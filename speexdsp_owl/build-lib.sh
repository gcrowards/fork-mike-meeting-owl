#!/bin/bash

echo "Do you want to rebuild Speex? [Default: No]"
select yn in "Yes" "No"; do
	case $yn in
		Yes ) break;;
		No )  exit;;
		* )   exit;;
	esac
done

if [ "$ANDROID_BUILD_ROOT" == "" ]; then
	echo "Please enter your Android Build root with no trailing slash"
	echo "Example: /home/user/Owl-Android"
	echo "(Run 'export ANDROID_BUILD_ROOT=<your path>' to avoid this step)"
	read ANDROID_BUILD_ROOT
fi

TOP=$ANDROID_BUILD_ROOT
echo "android root directory set to $TOP"

echo "sourcing build/envsetup.sh..."
. $TOP/build/envsetup.sh

echo "running lunch..."
lunch meetingowl-userdebug

echo "removing old build..."
rm -f  $OUT/obj_arm/lib/libspeexdsp_owl.so
rm -rf $OUT/obj_arm/SHARED_LIBRARIES/libspeexdsp_owl_intermediates/
rm -f  build/*.so

echo "running mm..."
mm

echo "copying library to build directory..."
cp $OUT/obj_arm/lib/libspeexdsp_owl.so build/
echo ""

echo "copying library to ../android/jni/libspeexdsp_owl directory..."
cp build/libspeexdsp_owl.so ../android/jni/libspeexdsp_owl/
echo ""
