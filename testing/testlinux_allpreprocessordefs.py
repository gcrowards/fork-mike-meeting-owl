#!/usr/bin/python

from owltestcase import OwlLinuxTestCase
skip = OwlLinuxTestCase.skip


class TestLinux_AllPreprocessorDefs (OwlLinuxTestCase):
    """
    This class tests the linux build with all preprocessor macros
    defined (forced active) except for LOG_TO_FILE.
    """

    # list active and inactive preprocessor macros
    active = ["MOCK_STAGE_PANEL"]
    inactive = []


    def setUp(self):
        # make sure we have a new build for this test case
        assert(self.build_once(self.active, self.inactive))
    
    def test_frame_rate(self):
        # run application and check FPS
        self.assert_frame_rate(29.0)
