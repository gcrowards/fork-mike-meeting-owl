#!/usr/bin/python

from owltestcase import OwlAndroidTestCase
skip = OwlAndroidTestCase.skip


class TestAndroid_AllPreprocessorDefs (OwlAndroidTestCase):
    """
    This class tests the android build with all preprocessor macros
    defined (forced active) except for LOG_TO_FILE. Note that this
    does not actually test INPUT_3456 since its effects are overrided
    by MOCK_INPUT.
    """

    # list active and inactive preprocessor macros
    active = ["INPUT_3456", "MOCK_INPUT", "MOCK_STAGE_PANEL", "OPENCV_FACES"]
    inactive = []


    def setUp(self):
        # make sure we have a new build for this test case
        assert(self.build_once(self.active, self.inactive))

    def test_frame_rate(self):
        # run application and check FPS
        self.assert_frame_rate(29.0)
