#!/usr/bin/python

from owltestcase import OwlLinuxTestCase
skip = OwlLinuxTestCase.skip


class TestLinux_DefaultPreprocessorDefs (OwlLinuxTestCase):
    """
    This class tests the linux build with default settings for the
    preprocessor macros, as defined in Makefile. Be sure to examine
    this test case for correctness if the default settings ever change
    in Makefile.
    """
    
    # list active and inactive preprocessor macros
    active = []
    inactive = []


    def setUp(self):
        # make sure we have a new build for this test case
        assert(self.build_once(self.active, self.inactive))
    
    def test_frame_rate(self):
        # run application and check FPS
        self.assert_frame_rate(29.0)
