#!/usr/bin/python

import os
import signal
import sys
import time
from subprocess import Popen, PIPE

# terminal color escape sequences
COLOR_RESET   = "\033[0m"
COLOR_RED     = "\033[91m"
COLOR_GREEN   = "\033[92m"
COLOR_YELLOW  = "\033[93m"
COLOR_BLUE    = "\033[94m"
COLOR_MAGENTA = "\033[95m"
COLOR_CYAN    = "\033[96m"

# timeouts in seconds
DEFAULT_TIMEOUT  = 10
STARTUP_TIMEOUT  = 20
SHUTDOWN_TIMEOUT = 30

# string colorize functions
def colorize(s, color):
    """
    Return a modified string that includes terminal escape sequences
    to colorize it.
    """
    return color + s + COLOR_RESET
def colorize_pass(s):
    return colorize(s, COLOR_GREEN)
def colorize_fail(s):
    return colorize(s, COLOR_RED)
def colorize_skip(s):
    return colorize(s, COLOR_YELLOW)


class OwlTestCase:
    """
    This class encapulates most of the functionality needed to run
    various test cases. It is inspired by the TestCase class in the
    unittest standard module.

    Notes
    -----
    * testRunner.py expects unit tests to be defined in subclasses of
      OwlTestCase that have names starting with 'Test'.
    * testRunner.py imports the default test cases that it will
      run. Import more test cases there, as needed, or comment out
      existing ones that you want to ignore. You can also pass
      command-line arguments to testRunner.py if you want to run or
      skip a small number of test cases. (See './testRunner.py -h'
      output.)
    * Individual tests are defined as methods that have names starting
      with 'test'.
    * The setUp() and tearDown() methods can be overridden to provide
      common initialization and cleanup for every test in a test case
      subclass.
    * Use the @skip decorator to mark an individual test as one to skip.
    """

    # member variables
    target = ""
    clean_build = False
    verbose = False
    proc_meetingOwl = None

    def assert_frame_rate(self, desired_fps, n_fps_readings=5, n_ignore=1):
        """
        Method that executes the application and checks for a sufficiently
        fast frame rate. The test gathers n_fps_readings FPS values
        and throws out the n_ignore lowest values before calculating
        the average FPS.
        """
        # set a timeout time
        fps_period = 5 # add margin to expected 3 seconds between FPS messages
        t_timeout = time.time() + fps_period * n_fps_readings + STARTUP_TIMEOUT
        # start up the application
        self.meetingOwl_run()
        # run until n_fps_readings frame rate log messages
        count = 0
        frame_rates = []
        while count < n_fps_readings:
            # consume the next line of output to stdout
            line = self.cmd_readline(self.proc_meetingOwl)
            if not line:
                # took too long to get a line of output
                self.assert_process_timeout()
            if line.startswith("ERROR"):
                # meetingOwl logged an error
                assert(False), line
            if time.time() > t_timeout:
                # the test as a whole took too long
                self.assert_process_timeout("test timed out")
            if "frame rate" in line.lower():
                # extract the frame rate and increment count
                frame_rates.append(float(line.split()[-2]))
                count += 1
        # stop the process and gather remaining results
        assert(self.meetingOwl_clean_exit()), "meetingOwl did not exit cleanly"
        # sort and throw out lowest values (default is to throw out 1 low value)
        frame_rates.sort()
        frame_rates = frame_rates[n_ignore:]
        # check the average rate
        if len(frame_rates) > 1:
            ave_fps = sum(frame_rates) / len(frame_rates)
        else:
            ave_fps = 0.
        err_msg = "frame rate too low: %.2f < %.2f" % (ave_fps, desired_fps)
        assert(ave_fps >= desired_fps), err_msg

    def assert_process_timeout(self, msg=None, proc=None):
        """
        Method that asserts False with a standard message about a timeout.
        """
        # if no proc specified, assume meetingOwl
        if proc is None:
            proc = self.proc_meetingOwl
        self.cmd_kill(proc) # ensure that application isn't running
        if not msg:
            msg = "no response from process"
        assert(False), msg
        
    def build_once(self, active_macros, inactive_macros):
        """
        Method that builds the default target once.
        """
        return self.build_target_once(self.target, active_macros, inactive_macros)

    def build_target_once(self, target, active_macros, inactive_macros):
        """
        Method that builds a target only once if repeatedly called,
        usually in setUp().
        """
        if not self.clean_build:
            # attempt new build
            if self.verbose:
                print "building new executable"
            if self.compile_target(target, active_macros, inactive_macros):
                self.clean_build = "pass"
                return True
            else:
                self.clean_build = "fail"
                return False
        elif self.clean_build == "pass":
            if self.verbose:
                print "using previous build"
            return True
        else:
            self.print_error("previous build failed")
            return False

    def compile(self, active_macros, inactive_macros):
        """
        Method that compiles code for the default target.
        """
        return self.compile_target(self.target, active_macros, inactive_macros)

    def compile_target(self, target, active_macros, inactive_macros):
        """
        Method that compiles code for a given target.
        """
        # make temporary Makefile
        self.create_makefile(active_macros, inactive_macros)
        # extract defined macros and print
        macro_defs = self.get_macro_defs()
        if self.verbose:
            print "preprocessor macro defs: %s" % macro_defs
        # clean and then make the target
        self.make_target("clean")
        return self.make_target(target)

    def create_makefile(self, active_macros, inactive_macros,
                        src="Makefile", dst="Makefile.tmp"):
        """
        Method to create a makefile given a source makefile and list of
        macro definitions to force either active or inactive.
        """
        # read the source makefile
        f = open(src, "r")
        lines = f.readlines()
        f.close()
        # write the destination makefile
        f = open(dst, "w")
        for line in lines:
            # check for a macro definition
            if "-D" in line:
                # extract the macro
                macro = line.split("-D")[-1].strip()
                # modify the line if necessary
                if macro in active_macros and line.startswith("#"):
                    line = line[1:]   # uncomment the line
                if macro in inactive_macros and not line.startswith("#"):
                    line = "#" + line # comment out the line
            f.write(line)
        f.close()

    def cmd_exec(self, cmd, show_msg=True):
        """
        Method that executes a system command and returns the output.
        """
        # print a standard message if desired
        if self.verbose and show_msg:
            print "'%s'" % cmd
        # run the command and gather the results
        proc = Popen(cmd.split(), stdin=PIPE, stdout=PIPE, stderr=PIPE)
        output, err = proc.communicate()
        return output.strip(), err.strip(), proc.returncode

    def cmd_exec_nowait(self, cmd, show_msg=True):
        """
        Method that executes a system command but immediately returns the
        Popen object rather than the command's output.
        """
        # print a standard message if desired
        if self.verbose and show_msg:
            print "'%s'" % cmd
        # run the command and gather the results
        p = Popen(cmd.split(), stdin=PIPE, stdout=PIPE, stderr=PIPE)
        return p

    def cmd_interrupt(self, proc):
        """
        Method to interrupt a process created with cmd_exec_nowait() and
        return the output.
        """
        proc.send_signal(signal.SIGINT)
        output, err = proc.communicate()
        return output.strip(), err.strip(), proc.returncode

    def cmd_kill(self, proc):
        """
        Method that kills a process and doesn't bother collecting any output.
        """
        proc.kill()

    def cmd_readline(self, proc, timeout=DEFAULT_TIMEOUT):
        """
        Method to read a line from stdout of a process created with
        cmd_exec_nowait(). Set timeout to a positive value (in
        seconds) to have call timeout after a while.
        """
        # setup an alarm signal handler
        class Alarm(Exception):
            pass
        def alarm_handler(signum, frame):
            raise Alarm
        if timeout >= 0:
            signal.signal(signal.SIGALRM, alarm_handler)
            signal.alarm(timeout)
        try:
            # read from stdout
            line = proc.stdout.readline().strip()
            # cancel the alarm
            if timeout >= 0:
                signal.alarm(0)
        except Alarm:
            line = ''
        if self.verbose and line:
            print line
        return line

    def get_macro_defs(self, makefile="Makefile.tmp"):
        """
        Method that extracts the defined preprocessor macro definitions
        from a makefile given the target ('linux' or 'android').
        """
        # override in child class
        print "unable to parse macro defs for target '%s'" % self.target
        print "aborting..."
        sys.exit()

    def get_num_tests(self):
        """
        Method that returns the number of defined tests.
        """
        return len(self.get_tests())
    
    def get_tests(self):
        """
        Method that extracts the names of all methdods with a name
        starting with 'test'.
        """
        # get the test methods
        tests = [method for method in dir(self)
                 if ( callable(getattr(self, method))
                      and method.startswith("test") )]
        tests.sort() # sort so the order is consistent
        return tests

    def make_target(self, target, makefile="Makefile.tmp"):
        """
        Method to execute a 'make <target>' or 'make -f <makefile> <target>'
        command.
        """
        # set cmd with makefile option
        if makefile:
            cmd = "make -f %s %s" % (makefile, target)
        else:
            cmd = "make %s" % target
        # build linux with multiple threads
        if target == "linux":
            cmd = cmd + " -j4"
        # run command and check output
        output, err, returncode = self.cmd_exec(cmd)
        if returncode:
            self.print_error(err)
            return False
        else:
            return True

    def meetingOwl_clean_exit(self):
        """
        Method that checks if the meetingOwl application exited cleanly.
        """
        # override in child class
        return False

    def meetingOwl_readline(self, timeout=DEFAULT_TIMEOUT):
        """
        Method to read a line from stdout of meetingOwl process. Set
        timeout to a positive value (in seconds) to have call timeout
        after a while.
        """
        return self.cmd_readline(self.proc_meetingOwl, timeout)

    def meetingOwl_run(self):
        """
        Method that runs the meetingOwl application return the associated
        Popen object.
        """
        # override in child class
        self.proc_meetingOwl = None

    def print_error(self, err_msg):
        """
        Method to print errors in a standard format.
        """
        divider = 70 * "-"
        print "%s\n%s\n%s" % (divider, err_msg, divider)

    def run(self, count_start, total, verbose):
        """
        Method for running all the tests in a test case.
        """
        # set verbose flag
        self.verbose = verbose
        # ensure clean build flag is cleared
        self.clean_build = False
        # extract the class name
        cls = self.__class__.__name__
        # get the test methods
        tests = self.get_tests()
        # make colorized passed / failed messages
        msg_passed = colorize_pass("PASSED")
        msg_failed = colorize_fail("FAILED")
        msg_skipped = colorize_skip("SKIPPED")
        # keep track of result of each test
        passed = []
        failed = []
        skipped = []
        # run the tests
        count = count_start
        for test in tests:
            count += 1
            test_name = "%s:%s" % (cls, test)
            if self.verbose:
                print 70 * "="
            print "%d / %d %s" % (count, total, test_name)
            try:
                test_fn = getattr(self, test)
                if hasattr(test_fn, "skip"):
                    print msg_skipped
                    skipped.append(test_name)
                else:
                    self.setUp()
                    ret = test_fn()
                    self.tearDown()
                    print msg_passed
                    passed.append(test_name)
            except AssertionError, err:
                err = str(err) # convert to a string
                if err:
                    self.print_error(err)
                print msg_failed
                failed.append(test_name)
        return passed, failed, skipped

    def setUp(self):
        """
        Method that gets called right before each test method. Override in
        a subclass as needed.
        """
        pass

    @staticmethod
    def skip(test_fn):
        """
        Decorator for marking a test as one to skip.
        """
        test_fn.skip = True # set an attribute on the function
        return test_fn      # return function as is (except for new attribute)

    def tearDown(self):
        """
        Method that gets called right after each test method. Override in
        a subclass as needed.
        """
        pass


class OwlAndroidTestCase (OwlTestCase):
    """
    This is a subclass of OwlTestCase with Android-specific details.
    """

    # set the target
    target = "android"


    def compile_target(self, target, active_macros, inactive_macros):
        """
        Method that compiles code for a given target.
        """
        # call parent method
        ret = OwlTestCase.compile_target(
            self, target, active_macros, inactive_macros)
        # install on android
        if ret:
            ret = self.make_target("install")
        return ret

    def get_macro_defs(self, makefile="Makefile.tmp"):
        """
        Method that extracts the defined preprocessor macro definitions
        from a makefile for the default target.
        """
        # use the Makefile 'echo' target to help extract the defined preprocessor macros
        # set cmd with makefile option
        if makefile:
            cmd = "make -f %s echo" % makefile
        else:
            cmd = "make echo"
        # run command and check output
        output, err, returncode = self.cmd_exec(cmd, False)
        # extract macro defs
        macro_defs = [line for line in output.split("\n")
                      if "JNI compile defs" in line]
        # clean up results
        macro_defs = macro_defs[0].split(":")[-1]
        return macro_defs

    def meetingOwl_clean_exit(self):
        """
        Method that checks if the meetingOwl application exited cleanly.
        """
        # set a timeout time
        t_timeout = time.time() + SHUTDOWN_TIMEOUT
        # send Ctrl-C ASCII code to meetingOwl adb shell
        self.proc_meetingOwl.stdin.write("\x03")
        while True:
            line = self.cmd_readline(self.proc_meetingOwl)
            if not line or "Say good night" in line or time.time() > t_timeout:
                break
        # now interrupt adb process and gather output
        output, err, returncode = self.cmd_interrupt(self.proc_meetingOwl)
        self.proc_meetingOwl = None # clear the Popen object
        if self.verbose:
            print output
        # use log message to test for clean exit rather than
        # returncode because with android we get the adb
        # returncode, not the meetingOwl returncode
        if "Good night." in line:
            ret = True
        else:
            ret = False
        # return the result
        return ret

    def meetingOwl_run(self):
        """
        Method that runs the meetingOwl application and sets the associated
        Popen object.
        """
        # NOTE: we get the **adb** processes not the actual android processes

        # start adb shell for meetingOwl and send the command
        if self.verbose:
            print "running meetingOwl..."
        self.proc_meetingOwl = self.cmd_exec_nowait("adb shell", show_msg=True)
        self.proc_meetingOwl.stdin.write("meetingOwl\n")


class OwlLinuxTestCase (OwlTestCase):
    """
    This is a subclass of OwlTestCase with Linux-specific details.
    """

    # set the target
    target = "linux"

    def get_macro_defs(self, makefile="Makefile.tmp"):
        """
        Method that extracts the defined preprocessor macro definitions
        from a makefile for the target.
        """
        # use the Makefile 'echo' target to help extract the defined preprocessor macros
        # set cmd with makefile option
        if makefile:
            cmd = "make -f %s echo" % makefile
        else:
            cmd = "make echo"
        # run command and check output
        output, err, returncode = self.cmd_exec(cmd, False)
        # extract macro defs
        macro_defs = [line for line in output.split("\n")
                      if "linux compile defs" in line]
        # clean up results
        macro_defs = macro_defs[0].split(":")[-1]
        return macro_defs

    def meetingOwl_clean_exit(self):
        """
        Method that checks if the meetingOwl application exited cleanly.
        """
        output, err, returncode = self.cmd_interrupt(self.proc_meetingOwl)
        self.proc_meetingOwl = None # clear the Popen object
        if self.verbose:
            print output
        if returncode:
            return False
        else:
            return True

    def meetingOwl_run(self):
        """
        Method that runs the meetingOwl application and sets the associated
        Popen object.
        """
        self.proc_meetingOwl = self.cmd_exec_nowait("./meetingOwl")
