#!/usr/bin/python
'''
This program runs various tests on the meetingOwl application.
'''

import inspect
import os
import sys
import time

from owltestcase import colorize_pass, colorize_fail, colorize_skip

# import desired test modules here
# can use command line to list specific test cases to run
# TODO: make testRunner find the modules and test cases automatically
from testlinux_defaultpreprocessordefs import TestLinux_DefaultPreprocessorDefs
from testlinux_allpreprocessordefs import TestLinux_AllPreprocessorDefs
from testandroid_defaultpreprocessordefs import TestAndroid_DefaultPreprocessorDefs
from testandroid_allpreprocessordefs import TestAndroid_AllPreprocessorDefs
from testandroid_opencvfaces import TestAndroid_OpenCvFaces


if __name__=="__main__":
    # ensure that the working directory is one up from the testing directory
    os.chdir(os.path.dirname(os.path.realpath(__file__)) + "/..")
    # set default flags
    verbose = True
    skip_test_cases = False
    android_filter = False
    linux_filter = False
    # check usage
    if len(sys.argv) > 1:
        if "-h" in sys.argv:
            print "usage: testRunner.py [options] [[-s] list of test cases]"
            print  "   -h        show this help info"
            print  "   --quiet   run with quiet (not verbose) output"
            print  "   --android filter out any test cases without 'Android' in its name"
            print  "   --linux   filter out any test cases without 'Linux' in its name"
            print  "   -s        skip listed test cases and run remaining default cases"
            sys.exit()
        if "--quiet" in sys.argv:
            verbose = False
        if "--android" in sys.argv:
            android_filter = True
        if "--linux" in sys.argv:
            linux_filter = True
        if "-s" in sys.argv:
            skip_test_cases = True
    # get the default list of test cases from the imported test classes
    test_classes = [(n, o) for (n, o) in inspect.getmembers(sys.modules[__name__])
                    if inspect.isclass(o) and n.startswith("Test")]
    test_classes.sort() # sort so the order is consistent
    # remove android and linux test cases as specified by flags
    if android_filter:
        test_classes = [(n, o) for (n, o) in test_classes if 'Android' in n]
    if linux_filter:
        test_classes = [(n, o) for (n, o) in test_classes if 'Linux' in n]
    # extract any test cases listed on command line
    test_case_args = [arg for arg in sys.argv[1:] if arg.startswith("Test")]
    # filter test cases list as command-line arguments
    if test_case_args:
        if skip_test_cases:
            # throw out listed test cases
            test_classes = [(n, o) for (n, o) in test_classes if n not in test_case_args]
        else:
            # keep only listed test cases
            test_classes = [(n, o) for (n, o) in test_classes if n in test_case_args]
    # keep track of result of each test
    passed = []
    failed = []
    skipped = []
    # init counters
    count = total = 0
    # get total number of tests
    for (name, obj) in test_classes:
        total += obj().get_num_tests()
    # run each test class
    t_start = time.time()
    user_break = False
    for (name, obj) in test_classes:
        try:
            ret_passed,ret_failed,ret_skipped = obj().run(count, total, verbose)
            passed += ret_passed
            failed += ret_failed
            skipped += ret_skipped
            count += len(ret_passed) + len(ret_failed) + len(ret_skipped)
        except KeyboardInterrupt:
            user_break = True
            break
    t_end = time.time()
    # summarize results
    if user_break:
        print "\nUser break\n"
    else:
        print 70 * "="
        print "\nRan %d tests in %.1fs" % (len(passed) + len(failed),
                                           t_end - t_start)
        if len(failed) == 0:
            if len(skipped):
                print colorize_skip("PASSED (skipped=%d)\n" % len(skipped))
            else:
                print colorize_pass("PASSED\n")
        else:
            if len(skipped):
                print colorize_fail("FAILED (failures=%d, skipped=%d)\n" \
                                    % (len(failed), len(skipped)))
            else:
                print colorize_fail("FAILED (failures=%d)\n" % len(failed))
