#!/usr/bin/python

from owltestcase import OwlAndroidTestCase
skip = OwlAndroidTestCase.skip


class TestAndroid_OpenCvFaces (OwlAndroidTestCase):
    """
    This class tests the android build with OPENCV_FACES defined
    (forced active).
    """

    # list active and inactive preprocessor macros
    active = ["OPENCV_FACES"]
    inactive = []


    def setUp(self):
        # make sure we have a new build for this test case
        assert(self.build_once(self.active, self.inactive))

    def test_frame_rate(self):
        # run application and check FPS
        self.assert_frame_rate(29.0)
