## Automated Testing

The "testing/" directory contains a number of python scripts that
support automated testing. **testRunner.py** is the one that actually
runs the various test cases, and most of the underlying infrastructure
for testing is in owltestcase.py. The remaining scripts implement the
various test cases.


### Caveats

* It's possible that changing the code base can affect the way the
automated testing works. For example, we currently use the format of
the closing "Say good night Owl" message to detect a successful exit
of the application. Getting familiar with the code in owltestcase.py
is recommended.

* Automated testing with the Android-based hardware will not work well
until the A/V pipeline is made more robust. For instance, the pipeline
does not always run properly if there is no host application consuming
UVC image frames. Similarly, the pipeline will report errors if there
is no host UAC endpoint connected.


### Using The Test Runner

* Run `python testRunner.py -h` to see the test runner options.

* "--android" and "--linux" are especially convenient options to use;
they restrict the testing to one platform or the other (based on test
case names). This gives a user the chance to re-configure their
testing environment for each platform before running the tests.

* The "--quiet" option will suppress most of the logging messages.

* Individual test cases can be listed as command-line arguments. This
is convenient when a user needs to run only a small number of test
cases. The "-s" option reverses the role of the listed test cases,
instead causing them to be skipped.


### Creating Test Cases

* The automated testing framework is inspired by the Python unittest
module. Tutorials on that module could be helpful for understanding
how the meetingOwl testing works. The examples below will also make
this section of the document clearer.

* Test cases are subclasses of OwlAndroidTestCase or OwlLinuxTestCase
and must have a class name beginning with, respectively,
"TestAndroid_" or "TestLinux_". By convention, the module file names
begin with "testandroid_" or "testlinux_". So, the class names use
camel case whereas the file names are all lower case. See the existing
scripts in the "testing/" directory.

* Currently, the test cases are imported near the top of
testRunner.py.  Update the imports as needed. A nice future capability
of the test runner would be to detect test cases automatically, based
on the module file name.

* Individual tests are defined as methods with a name that begins with
"test".

* Individual tests can be marked for skipping with the "@skip"
decorator.

* Test cases have setUp() and tearDown() methods that can be
overridden to do common initialization and cleanup for every test in a
test case.

* A good way to organize a test case is by common build options. That
way, only one time-consuming build is needed for all the tests in a
test case. The build should be done in the setUp() method rather than
in an individual test. The build_once() method will manage the
building so that a previous build will be used where possible.


### Examples

Test cases contain a group of individual tests that belong together in
some way. The following is a minimal test:

```
   def test_always_pass(self):
       assert(True)
```

This trivial example always passes, but it illustrates a couple
things. First, tests are just Python class methods, but the method
name must begin with "test". Second, the test should make one or more
assertions that represent whatever is being tested. This example
always asserts True, so the test always passes. We could also make a
test fail all the time by always asserting False:

```
   def test_always_fail(self):
       assert(False), "This test is forced to FAIL all the time"
```

Notice that the previous example includes a message about the failure
that will be printed by the test runner.

Typically, a test will do more interesting things to start up and
exercise the application. Then it will assert some comparisons on
various variables, where these assertions are just Python code that
evaluate to `True` or `False`.

The `@skip` decorator can be used to tell the test runner that a
specific test should be skipped, usually as part of some short-term
debugging effort. To do so, just add the decorator to its own line
before the definition of the test:

```
   @skip
   def test_always_fail(self):
       assert(False), "This is a dumb test that should be skipped"
```


The following is a complete example of a test case:

```
#!/usr/bin/python

from owltestcase import OwlAndroidTestCase
skip = OwlAndroidTestCase.skip


class TestAndroid_OpenCvFaces (OwlAndroidTestCase):
    """
    This class tests the android build with OPENCV_FACES defined
    (forced active).
    """

    # list active and inactive preprocessor macros
    active = ["OPENCV_FACES"]
    inactive = []


    def setUp(self):
        # make sure we have a new build for this test case
        assert(self.build_once(self.active, self.inactive))

    def test_frame_rate(self):
        # run application and check FPS
        self.assert_frame_rate(29.0)
```

Above, the first thing to do is import the parent Owl test case for
use when defining the new test case class.

The next line is for convenience; it assigns the skip decorator to a
short local variable so it can be used as illustrated previously.

Next the test case is defined, and then two member variables are
set. `active` is a list of preprocessor macros to turn on, even if
they are commented out near the top of Makefile. Similarly, `inactive`
is a list of preprocessor macros to turn off, even if they are
"commented in" in Makefile. In the example above, all the default
preprocessor definitions are used, except that `OPENCV_FACES` is
forced on.

Next, `setUp()` provides a place to run common code at the beginning
of each test in the test case. This example just builds the
application, although wrapping the build in an `assert()` call will
cause a test to fail if there are any build errors. Not shown in the
above is the `tearDown()` method which can be used to run common code
at the end of each test in the test case.

Finally, `test_frame_rate()` illustrates a useful test that checks if
the frame rate of the system is fast enough. The `assert_frame_rate()`
method is defined in OwlTestCase and provides a good illustration of
how to run the application and check the feedback contained in the
logging messages.
