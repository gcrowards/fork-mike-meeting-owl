LOCAL_PATH := $(call my-dir)

LOCAL_ARM_MODE := arm

# libOwlPG
include $(CLEAR_VARS)
LOCAL_CFLAGS := \
	-I ./hardware/libhardware/include/ \
	-I ./system/core/include/ \
	-I ./system/media/camera/include/ \
	-I ./frameworks/native/include/ \
	-I ./frameworks/native/include/media/openmax/ \
	-I ./frameworks/av/include/media/stagefright/ \
	-I ./external/tinyalsa/include \
	-I $(LOCAL_PATH)/../speexdsp_owl/include/speex \
	-I $(LOCAL_PATH)/source/libaudioPlatform/include/ \
	-I $(LOCAL_PATH)/source/ \
	-I $(LOCAL_PATH)/../android/jni/libfacialproc/ \
	-I $(LOCAL_PATH)/../ \
	-I ./include
#LOCAL_CFLAGS += -I ./external/jpeg
LOCAL_CFLAGS += -DGL_GLEXT_PROTOTYPES -DEGL_EGLEXT_PROTOTYPES
LOCAL_CFLAGS += -g # include debugging info
LOCAL_32_BIT_ONLY := true
LOCAL_MODULE    := libOwlPG
LOCAL_SHARED_LIBRARIES := \
    libEGL \
    libGLESv2 \
    libui \
    libgui \
    libutils \
    libbinder \
    libdl \
    libstagefright \
    libstagefright_foundation \
    libtinyalsa \
    libspeexdsp_owl \
    libcutils
#LOCAL_SHARED_LIBRARIES += libjpeg
LOCAL_LDLIBS := -L$(LOCAL_PATH)/../android/jni/libfacialproc -lfacialproc
LOCAL_SRC_FILES := \
    source/libOwlPgVersion.cpp \
    source/CameraHandler.cpp \
    source/CodecHandler.cpp \
    source/AudioHandler.cpp \
    source/sx1508.c \
    source/lp5562.c \
    source/I2cHandler.c \
    source/LedHandler.c \
    source/ButtonHandler.c \
    source/SpeakerHandler.c \
    source/audioframework.c \
    source/SampleRateProcessing.c \
    source/AudioProcessing.c \
    source/UVCHandler.c \
    source/DirectorHandler.c \
    source/WindowSurface.cpp
#LOCAL_SRC_FILES += source/GraphicsHandler.cpp
LOCAL_SRC_FILES += source/GpuSupport.cpp
include $(BUILD_SHARED_LIBRARY)
