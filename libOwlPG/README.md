## libOwlPG

"libOwlPG" refers to a project that builds a library used to hide
lower-level details of the Android platform. Currently, these details
deal with the following:

* low-level support of the camera with the GPU

* low-level support of MJPEG, H.264 and UVC output

* low-level support of microphone input and speaker output

* low-level support of Android graphics buffers so that images can be
  transferred from GPU to CPU without a call to glReadPixels()

* low-level support of LEDs, GPIO and I2C

This library enables a "full native pathway" to get camera images into
the GPU and the subsequent GPU-rendered images out to a USB host (same
sort of thing for audio, sans the GPU piece). The library also
facilitates easier integration of ongoing work by the Playground team.

The nice thing about libOwlPG is that it provides a welcome level of
modularity for Android development. In particular, a Meeting Owl
developer can use the library with the ndk-build tool without having
to install the entire Android code base and learn how to develop for
the platform. For those who need to modify or compile the library,
read the section on *Building libOwlPG* below.


### A/V Pipeline

The audio-visual (A/V) pipeline -- aka "full native pathway" -- is a
lower-level architecture for dealing with sound and images on the
Android platform and interacting with the main application, called
**meetingOwl** or often referred to as **owl-core**. The pipeline
is configured in `owl-core/src/main.cpp` for the main application,
with a similar configuration done in `libOwlPG/source/ConfSample.cpp`
for the corresponding reference application. This reference
application was provided by Playground as a working example of the A/V
pipeline.

Some pieces of the A/V pipeline -- called **handlers** -- run
independently of others, but some pieces interact with each other via
callbacks. Together, they form three separate pipelines: one for
camera/GPU images, one for microphone input, and one for loudspeaker
output. Note that these pipelines are not totally separate, since they
share CPU resources and all interact with the USB driver in the
kernel.


#### Camera Pipeline

The camera pipeline (see diagram) is driven by the camera hardware,
running at approximately 30 FPS. When a new image is ready to be
consumed, the **Camera Handler** calls the frame callback to make the
raw image available to the GPU. The rendering of the output frame is
handled by the `cGpu` class in owl-core or by the **Graphics Handler**
(not shown) in the reference application. Note that the reference
application can be compiled with either `libOwlPG/source/gpu.cpp` or
`libOwlPG/source/GraphicsHandler.cpp`. The latter is the original
handler from Playground, whereas the former is a replacement that is
structured similarly to `cGpu` in owl-core.

Both the GPU and the codec are bound to the same Android native
window. This allows the **Codec Handler** to "receive" the rendered
frame once the GPU is finished with it. After the frame is compressed,
the UVC frame callback passes the result on to the **UVC Handler**
which then interacts with the USB driver in the kernel.

![Camera Pipeline](diagrams/camera-pipeline.svg "Camera Pipeline")


#### Microphone Pipeline

For the microphone pipeline (see diagram), the **Audio Handler**
receives and processes audio data from the kernel. On a
**high-priority** thread, the beamform callback is used to expose the
PCM mic data to owl-core, which uses a delay-and-sum algorithm to
compute the two-channel audio output. This output is stored in a PCM
buffer, also exposed by the beamform callback. The Audio Handler then
pipes the output to the UAC handler in the kernel.

The **Director Handler** is Playground's stand-in for owl-core, and it
does several things needed to make the reference application work
properly. For meetingOwl, we still need the Director Handler because
it manages a **low-priority** thread that supports localization. In
particular, the localize callback exposes PCM microphone data to
owl-core so it can run the cross-correlations needed to localize sound
sources. This setup allows the localization to run as fast as we
enable it, yet not disrupt the quality of the sound passed back to the
host application.

![Microphone Pipeline](diagrams/mic-pipeline.svg "Microphone Pipeline")


#### Loudspeaker Pipeline

The loudspeaker pipeline (see diagram) is relatively straightforward,
where the **Speaker Handler** primarily acts as a bridge between the
host UAC data and the hardware. However, the Speaker Handler also uses
a couple callbacks to interact with owl-core. First, the speaker gain
callback routinely polls owl-core for volume information, which is
passed back as a real-valued gain. Second, the speaker active callback
provides estimates of the loudspeaker signal energy, so owl-core can
"lock-out" the sound localization algorithm when too much sound is
coming out of the loudspeaker.

![Loudspeaker Pipeline](diagrams/speaker-pipeline.svg "Loudspeaker Pipeline")


### Role of The GPU

Because of the GPU's close connection with the A/V pipeline, this is a
good place to describe how the GPU fits into the bigger view of the
application architecture (see diagram). In `owl-core/src/main.cpp`
instances are created of both `cOwlHook` and `cGpu`. These instances
are used to make regular updates of owl-core, as driven by the camera
frame callback.

![GPU Role](diagrams/gpu-role.svg "GPU Role")

Notice in the diagram that much of the GPU setup must not occur when
the `cGpu` instance is created. Instead, it must happen after the
camera is ready and on the same thread as the frame callback. This is
why the diagram shows a decision point for the first call of the frame
callback.

With each call of the frame callback, `cOwlHook::updateGpu()` is
called, passing the raw camera image to owl-core. This allows the GPU
to render the sub-frames and overlays to a native window for the codec
to process. The GPU also renders a separate panoramic strip that gets
copied to the CPU for computer vision purposes. Additionally,
`cOwl::update()` is called to update the rest of the core application
and retrieve the sub-frames for rendering on the next call of the
frame callback.


### Building libOwlPG

Detailed steps on how to set up a development environment are
below. This section covers how to build libOwlPG and the reference
application.

* Use the "build-lib.sh" script for convenience. For it to work, you must
  first set the $ANDROID_BUILD_ROOT environment variable to point to
  the location of *your* android distribution, e.g.,

```
export ANDROID_BUILD_ROOT=$HOME/Documents/owl/third-party/Qualcomm/codeaurora/APQ8016_410C_LA.BR.1.2.4-01810-8x16.0_5.1.1_Lollipop_P2
```

* The build script will source the envsetup.sh script and run lunch
  with the currently used target (`meetingowl-userdebug`).

* If the build is successful, the script will copy the new library to
  the build/ directory. The script will also "install" it by copying
  the library to `../android/jni/libOwlPG/`. Later, running "make setup"
  for the meetingOwl application will push the library to the
  device. You can also push the library manually with `adb push
  build/libOwlPG.so /system/lib/`.

* The build script also calls `../scripts/autoRevision.py`, which
  updates the header `owlcore/inc/version.hpp` with a new
  timestamp. This timestamp is used to helped detect mismatches
  between the versions of meetingOwl and libOwlPG installed on a
  device.

* Use the "build-app.sh" script to build the PGReferenceApp
  executable. Note that this app is not actively maintained. It might
  not work as expected, but it could be useful for debugging down the
  road.

* ConfSample.cpp has the macro INTEGRATE near the top. Comment it out
  to compile a version of PGReferenceApp that is nearly identical to
  the original ConfSample app. Leave the macro defined to compile a
  version that integrates with GPU code similar to owl-core. **See the
  INTEGRATE comments in ConfSample.cpp for additional instructions.**


### Setting Up The Android Platform Build Environment

Note that the steps below are likely obsolete, as they refer to the
publicly available codeaurora repository and we now use a private
version maintained by Playground. However, these instructions might
still be useful for debugging problems with future setups.

* Install the "repo" tool as described in the [Android Release Notes
  document](https://developer.qualcomm.com/download/db410c/android-release-notes-build-labr124-01810-8x160.pdf). The
  document has its own setup instructions that might be helpful if
  anything below goes wrong.

* Create a directory (e.g., "codeaurora") to download and build
  everything. Make sure you have lots of free space, about 60GB for
  the download and at least 40GB for the build (120GB total to be
  safe).

* Download the Android Board Support Package from the [Qualcomm
  Developer Network web
  site](https://developer.qualcomm.com/hardware/dragonboard-410c/tools). You
  probably want the most recent version (currently, the one that ends
  in "vLA.BR.1.2.4-01810-8x16.0-3"). Check with the Playground team in
  case we need to use a more recent version.

* Unzip the board support package and copy the contents to your source
  directory ("codeaurora" above).

* Make the build script executable with "chmod a+x DB410c_build.sh".

* Before running the build script, check for a mismatch in the name of
  the proprietary tgz file. Compare the file name with the one listed
  in the build script, near the end of the script. Currently, one has
  a couple "-" characters where the other has "_" characters. Change
  the name of the tgz file so they match.

* Run the build script. Wait and answer a few prompts about the user
  name and email. Then wait a *very* long time, probably upwards of 8
  hours (roughly 4-5 hours to download and 4-6 hours to build,
  depending on your setup).

* The build will probably end with a message about changes to the
  API. To deal with this, change into the newly created distribution
  directory ("APQ...") and enter "make update-api".

* Here's where it gets really annoying: Check the $OUT/obj_arm/lib/
  directory for missing libraries such as libgui.so, libGLESv2.so, and
  libstagefright_foundation.so. If any of these are missing, then do a
  clean rebuild without the build script. This will take a very long
  time again, and there's probably a better solution that hasn't been
  figured out yet. Feel free to try and update these instructions. ;-)

```
source build/envsetup.sh
lunch msm8916_64-userdebug
rm -rf out/
mv log.txt log.txt.0  # save the original log file
make -j4 | tee log.txt
```

* Confirm that the missing libraries are now there.
