LOCAL_PATH := $(call my-dir)

# -- libOwlPG
include $(CLEAR_VARS)
LOCAL_MODULE := libOwlPG-prebuilt
#LOCAL_SRC_FILES := ../../android/jni/libOwlPG/libOwlPG.so
LOCAL_SRC_FILES := ../build/libOwlPG.so
include $(PREBUILT_SHARED_LIBRARY)

# -- libjpeg
include $(CLEAR_VARS)
LOCAL_MODULE := libjpeg-prebuilt
LOCAL_SRC_FILES := ../../../OpenCV-android-sdk/sdk/native/3rdparty/libs/armeabi-v7a/liblibjpeg.a
include $(PREBUILT_STATIC_LIBRARY)

# -- Qualcomm facial processing
# Note that this library causes the following message to be printed to
# the adb shell: "WARNING: linker: libmmcamera_faceproc.so has text
# relocations. This is wasting memory and prevents security
# hardening. Please fix." We appear to be stuck with the warning until
# the library gets recompiled with a more recent ndk release (see
# stackoverflow.com/questions/20141538/).
include $(CLEAR_VARS)
LOCAL_MODULE := libfacialproc-prebuilt
LOCAL_SRC_FILES := ../../android/jni/libfacialproc/libfacialproc.so
include $(PREBUILT_SHARED_LIBRARY)

# -- reference app
include $(CLEAR_VARS)
LOCAL_CFLAGS := -I $(LOCAL_PATH)/../../android/jni/libjpeg/
LOCAL_CFLAGS += -I $(LOCAL_PATH)/../../owlcore/inc/
LOCAL_CFLAGS += -I $(LOCAL_PATH)/../../android/jni/libfacialproc/
LOCAL_CFLAGS += -DGL_GLEXT_PROTOTYPES -DEGL_EGLEXT_PROTOTYPES
LOCAL_LDLIBS := -lEGL -lGLESv2
LOCAL_SHARED_LIBRARIES := libOwlPG-prebuilt libjpeg-prebuilt libfacialproc-prebuilt
LOCAL_32_BIT_ONLY := true
LOCAL_MODULE    := PGReferenceApp
LOCAL_SRC_FILES := \
	../source/owlHook.cpp \
	../../owlcore/src/AudioEqualizer.cpp \
	../source/ConfSample.cpp
LOCAL_SRC_FILES += ../source/gpu.cpp
include $(BUILD_EXECUTABLE)
