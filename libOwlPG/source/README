Conference Sample Application

This application provides end-to-end example implementation of Conference
use case, from camera capture all the way to UVC output.

To Build:
 Sync to Android PDK build location, set the environment, then run "mm"
  - source build/envsetup.sh
  - lunch
  - cd vendor/owllabs/sample/ConferenceSampleApp
  - mm

To Run:
 Copy ConfSample application and libfacialproc.so file to system and run
 as root
  - adb root
  - adb remount
  - adb push $OUT/system/bin/ConfSample /system/bin
  - adb push libfacialproc/libfacialproc.so /system/lib
  - adb shell
  - setprop sys.usb.config audio_source,uvc
  - ConfSample
  - Ctrl+C to Quit

To Run Automatically at boot:
 Update sys.usb.config to default to audio_source,uvc and symlink to pg_start
  - adb root
  - adb remount
  - adb shell
  - setprop persist.sys.usb.config audio_source,uvc
  - cd /system/bin
  - ln -s ConfSample pg_start
  - reboot

Enabled Features:
 - Accept PDM microphone array, decode data, and send 2 streams to UAC to host
 - Accept UAC input from host, play data on speakers
 - Compose UVC output frames using camera, dewarping it into a strip and up to
   2 ROI
 - Output to UVC as MJPEG encoded frames
 - Localization on microphone array (~4FPS) and face detection on frames (~3FPS)
 - On the display, left frame will show detected face and right frame will show
   where the localized microphone source is
 - If only face or microphone detection is enabled, the display will show single
   frame.
 - No dropped audio microphone packets in steady-state.  Some packets may dropped
   while cameras / microphones / speakers are being enabled or disabled.

Software Architecture:
 - Each major "piece" of system is handled in its own source file with own
   header file.  Care has been taken to ensure that only known / required APIs
   are exposed across the different system handlers.
 - Entry point is ConfSample.cpp, which simply initializes and starts each of
   the handlers
 - Handlers can expose a callback as an entry point into the system, and a
   callback registration method which is called as an exit point from the
   system.  Idea is that the application is composed as series of components
   that in turn calls the next in line after processing is done until the
   data is sent out the UVC/UAC pipe
 - "Director" is slow running thread that samples audio and video information
   and decides what the next operation should be.  Directions are sent to the
   GraphicsHandler to compose the scene.
 - Each "Handler" should be trivially wrappable as shared library if required

Software Handlers:
 - AudioHandler: read data from SPI, process as PCM, and send it over UAC
   to the host computer.  Runs at highest thread priority
 - SpeakerHandler: read data from UAC and send it to speaker amp.  The
   handler will read up to 8 samples more or less per buffer to deal with
   clock drift issues between host computer's 48kHz clock and system's
   48kHz clock.  Runs at second highest thread priority
 - CameraHandler: starts and reads data from Camera and sends it to
   registered callback handlers, defaulting to GraphicsHandler
 - GraphicsHandler: accepts frames from CameraHandler and compose the final
   image and output it to registered NativeWindow
 - CodecHandler: accepts frames from GraphicsHandler and encode the frames to
   MJPEG stream and call the registered callbacks
 - UVCHandler: registers a callback to CodecHandler and accepts encoded JPEG
   frames and sends it over UVC
 - DirectorHandler: registers a callback to CodecHandler and AudioHandler and
   sample data (1 of 10 for CodecHandler, 1 of 50 for AudioHandler) and make a
   copy which will be processed by non-real-time priority director thread which
   runs face detection and localization routines

Known Issues:
 - UVC may not come up if system is booted up with USB cable plugged in.  To
   work around, unplug / plug in the USB port after system has booted
 - Switching between adb mode and uvc/uac mode may cause system instability
 - For UVC, first time connection will fail.  To work around, simply open the
   UVC port / webcam application again
 - On shutdown, CameraHandler sometimes will cause segmentation fault
