/* copyright Owl labs 2016 -

    audioframework.c - This file contains a number of core implementations for the audio processing frame work.


*/


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <signal.h>
#include <string.h>
#include <pthread.h>
#include <fcntl.h>
#include <math.h>

#include <linux/ioctl.h>
#include <cutils/properties.h>
#include "audioframework.h"
#include "AudioProcessing.h"
#include "SampleRateProcessing.h"

#define USE_DEBUG_BUFF


#if defined(__cplusplus)
extern "C" {
#endif
extern unsigned long long LastMicUSBWriteTime;
#if defined(__cplusplus)
}  /* extern "C" */
#endif

extern unsigned long long LastSpkrUSBReadTime;


#define DEBUG_WRITE_SIZE 48000*8*2
#define WRITE_DEBUG_CB_SIZE (DEBUG_WRITE_SIZE*4)
int micClips = 0;
int micMax = 0;
int micMin = 0;
int totalMax = 0;
int totalMin = 0;
char debugAudioBuffer[DEBUG_WRITE_SIZE];
CIRC_BUFF writeDebugCB;
DEBUG_BUFF fwDebugBuff;
extern DEBUG_BUFF micDebugBuff;
static pthread_t audioMgrThread;
int runAudioMgr = 1;
static int gMuteMic = 0;
static int gMicUsbActive = USB_CHANNEL_INACTIVE;
static int gSpkrUsbActive = 0;

AUDIO_IF_STATUS audioIfStatus;

// interfaces for file playback
static pthread_mutex_t filePlayMutex = PTHREAD_MUTEX_INITIALIZER;
int filePlayInited = 0;
int filePlayCBInUse[FILE_CB_COUNT];
CIRC_BUFF filePlayCB[FILE_CB_COUNT];

/** This function returns the number of nanoseconds since device start.
*
*   @return number of nanoseconds since device start
*/

// NOTE: prop names can only be 32 characters max
static pthread_mutex_t propMutex = PTHREAD_MUTEX_INITIALIZER;
//static pthread_cond_t  propCond = PTHREAD_COND_INITIALIZER;

AUDIO_PROPERTIES MasterProperties;
static int propertiesRead = 0;
AUDIO_PROP_RECORD audioProps[] =
{
  {"mowl.audio.miclocalgen", 0, &MasterProperties.micLocalGen },
  {"mowl.audio.spkrlocalgen", 0, &MasterProperties.spkrLocalGen },
  {"mowl.audio.prtfsest", 0, &MasterProperties.printFsEstimate },
  {"mowl.audio.closedebug", 0, &MasterProperties.closeDebugFiles },
  {"mowl.audio.spkrtrim", 16384, &MasterProperties.spkrTrim },
  {"mowl.audio.mictrim", 32767, &MasterProperties.micTrim },
  {"mowl.audio.disablevol", 0, &MasterProperties.disableVolume },
  {"mowl.audio.cicgain", 8192, &MasterProperties.CICGain },
  {"mowl.audio.wda", 0, &MasterProperties.writeDebugAudio },
  {"mowl.audio.cur", 90, &MasterProperties.audioCursor },
  {"mowl.audio.micchans", -1, &MasterProperties.micChans },
  {"mowl.audio.enableeq", 1, &MasterProperties.enableEq },
  {"mowl.audio.enablebeam", 0, &MasterProperties.enableBeam },
  {"mowl.audio.enableproc", 1, &MasterProperties.enableMicProc },
  {"mowl.audio.enableaec", 1, &MasterProperties.enableAEC },
  {"mowl.audio.enablenr", 1, &MasterProperties.enableNR },
  {"mowl.audio.enablelm", 1, &MasterProperties.enableLM },
  {"mowl.audio.mutemic", -1, &MasterProperties.micMute },
  {"mowl.audio.aecdebug", 0, &MasterProperties.aecDebug },
  {"persist.mowl.audio.aecpost", 3, &MasterProperties.aecPost },
  {"persist.mowl.audio.dtdthreshold", 30, &MasterProperties.dtdThreshold },
  {"mowl.audio.enabletidsp", 1, &MasterProperties.enableTiDsp },
  {"mowl.audio.localdebug", 0, &MasterProperties.localDebug },
  {"mowl.audio.localnum", -1, &MasterProperties.localNum },
  {"mowl.audio.locallog", 0, &MasterProperties.localLog },
  {"mowl.audio.localratio", 5, &MasterProperties.localRatio },
  {"persist.mowl.audio.msa", -50, &MasterProperties.minSpkrActive },
  {"persist.mowl.audio.nramount", 0, &MasterProperties.nrAmount },
  {"mowl.audio.ada", 0, &MasterProperties.aecDebugAudio },
  {"mowl.audio.micgain0", -16, &MasterProperties.micGainsDb[0] },
  {"mowl.audio.micgain1", -16, &MasterProperties.micGainsDb[1] },
  {"mowl.audio.micgain2", -16, &MasterProperties.micGainsDb[2] },
  {"mowl.audio.micgain3", -16, &MasterProperties.micGainsDb[3] },
  {"mowl.audio.micgain4", -16, &MasterProperties.micGainsDb[4] },
  {"mowl.audio.micgain5", -16, &MasterProperties.micGainsDb[5] },
  {"mowl.audio.micgain6", -16, &MasterProperties.micGainsDb[6] },
  {"mowl.audio.micgain7", -16, &MasterProperties.micGainsDb[7] },
};

/** gets a circular buffer to play a file data through
*
* @return ptr to playback circular buff if one is available; otherwise null
*/

CIRC_BUFF *GetPlayCB()
{
  CIRC_BUFF *tempCB;
  int i;
  pthread_mutex_lock(&filePlayMutex);
  tempCB = NULL;
  for(i = 0; i < FILE_CB_COUNT; ++i)
  {
    if(filePlayCBInUse[i] == 0)
    {
      tempCB = &filePlayCB[i];
      filePlayCBInUse[i]  = 1;
      break;
    }
  }
  pthread_mutex_unlock(&filePlayMutex);
  return tempCB;

}

/** Frees a circular buffer for file playback
*
* @param cbPtr - pointer to the cb to release
*
* @return nothing
*/
void FreePlayCB(CIRC_BUFF *cbPtr)
{
  int i;
  pthread_mutex_lock(&filePlayMutex);
  for(i = 0; i < FILE_CB_COUNT; ++i)
  {
    if(cbPtr == &filePlayCB[i])
    {
      if(filePlayCBInUse[i] == 0)
      {
        printf("ERROR - releaseing a play cb that was active \n");
      }
      else
      {
        filePlayCBInUse[i] = 0;
        CircBuffRestart(cbPtr);
      }
      break;
    }
  }
  pthread_mutex_unlock(&filePlayMutex);


}
/** Sets the mute status for the mic
*
* @param muteState - 0 is unmuted; 1 is muted
*
* @return void
*/
void SetMicMute(int inMuteState)
{
  static int lastMuteState = -1;
  if(inMuteState != lastMuteState)
  {
    if(AUDIO_SYSTEM_DEBUG)
    {printf("Mic Mute set to %d \n", inMuteState);}
  }
  lastMuteState = inMuteState;
  gMuteMic = inMuteState;
}

/** Get the current state of the mic mute
*
* @return gMicMute
*/
int GetMicMute()
{
  return gMuteMic;
}

/** Sets the state for the mic usb channel
*
* @param inMicActive - 0 if inactive, 1 if active
*
* @return void
*/
void SetUsbMicActive(int inMicActive)
{
  static int lastMicActive = -1;
  if(inMicActive != lastMicActive)
  {
    if(AUDIO_SYSTEM_DEBUG)
    {printf("Mic Usb set to %d \n", inMicActive);}
  }
  lastMicActive = inMicActive;
  gMicUsbActive = inMicActive;

}

/** Gets the current state of the mic usb channel
*
* @return 0 if mic usb channel inactive, 1 if channel active
*/
int GetUsbMicActive()
{
  return gMicUsbActive;
}

/** Sets the state for the spkr usb channel
*
* @param inSpkrActive - 0 if inactive, 1 if active
*
* @return void
*/
void SetUsbSpkrActive(int inSpkrActive)
{
  static int lastSpkrActive = -1;
  if(inSpkrActive != lastSpkrActive)
  {
    if(AUDIO_SYSTEM_DEBUG)
    {printf("Spkr Usb set to %d \n", inSpkrActive);}
  }
  lastSpkrActive = inSpkrActive;
  gSpkrUsbActive = inSpkrActive;

}

/** Gets the current state of the spkr usb channel
*
* @return 0 if speaker usb channel inactive, 1 if channel active
*/
int GetUsbSpkrActive()
{
  return gSpkrUsbActive;
}

/** Returns the configuration of the audio framework
*
* @param pConfig - pointer to the structure that will hold the config
*
* @return 0
*/
int GetAudioConfig(AUDIO_CONFIG *pConfig)
{
  pConfig->procFrameCount = PROC_FRAME_SIZE;
  pConfig->micCount = 8;
  return 0;
}


/** returns a 1.15 value from an integer property value
*
* If the value is positive, it is returned directly,
* if negative, the value is assumed to be db and result db2mag
*
* @param property value
*
* @return magnitude in 1.15
*/
int PropertyTo1_15(int propValue)
{
  if(propValue <= 0)
  {
    propValue = db2mag((double)propValue);
  }
  return propValue;

}

/** read the audio properties from the property store into a local structure.
*
* @return 0
*/
int ReadProperties()
{
  int items;
  int i;
  int temp;
  static int firstTime = 1;
  static AUDIO_PROPERTIES compProps;

  items = sizeof(audioProps) / sizeof(AUDIO_PROP_RECORD);

  for(i = 0; i < items; ++i)
  {
    temp = property_get_int32(audioProps[i].propName, audioProps[i].propDefault);
    if((temp != *audioProps[i].propDest) && (firstTime == 0))
    {
      printf("Property %s changed %d->%d \n", audioProps[i].propName,
             *audioProps[i].propDest, temp);
    }
    *audioProps[i].propDest = temp;

    if(firstTime)
    {
      if(*audioProps[i].propDest != audioProps[i].propDefault)
      {
        printf("Property %s not default - default %d; value %d \n",
               audioProps[i].propName, audioProps[i].propDefault, *audioProps[i].propDest);
      }
    }
  }
  firstTime = 0;
  // now do value translations
  MasterProperties.CICGain     =  PropertyTo1_15(MasterProperties.CICGain);

  if( MasterProperties.micChans < 0)
  {
    MasterProperties.micRightChan = -1;
    MasterProperties.micLeftChan = -1;
  }
  else
  {
    MasterProperties.micRightChan = MasterProperties.micChans % 10;
    if(MasterProperties.micRightChan > 7)
    { MasterProperties.micRightChan = -1; }
    MasterProperties.micLeftChan = MasterProperties.micChans / 10;
    if(MasterProperties.micLeftChan > 7)
    {MasterProperties.micLeftChan = -1;}
  }

  if(MasterProperties.micMute >= 0)
  {SetMicMute(MasterProperties.micMute);}

  compProps = MasterProperties;
  return 0;

}

/** prints the contents of master properties
*
* @return 0
*/
int PrintProperties()
{
  int i = 0;
  int items;

  items = sizeof(audioProps) / sizeof(AUDIO_PROP_RECORD);

  printf(" Properties to control audio behaviour \n");
  for(i = 0; i < items; ++i)
  {
    printf("     %-32s = %d\n", audioProps[i].propName, *audioProps[i].propDest);
  }
  return 0;
}

/** sets the reference available flag in the audioIfStatus
*
* @param - status
*
* @return void
*/
void AudioIfStatusRefAvail(int avail)
{
  audioIfStatus.refAvail = avail;
}

/** copies the audio interface status structure to the input
*
* @param - pAis - pointer to the destination of the status structure
*
* @return 0 if successful, -1 if not
*/
int AudioIfStatusGet(AUDIO_IF_STATUS *pAis)
{

  memcpy(pAis, &audioIfStatus, sizeof(audioIfStatus));
  return 0;
}

/** Invalidates the property cache by setting propertiesRead to 0
*
*/
void InvalidatePropCache()
{
  pthread_mutex_lock(&propMutex);
  propertiesRead = 0;
  pthread_mutex_unlock(&propMutex);

}

/** Get a copy of the audio properties; read them from the property store if
* necessary.
*
* @param pProps - ptr to destination for the copy
*
* @return 0
*/
int GetAudioProperties(AUDIO_PROPERTIES *pProps)
{
  pthread_mutex_lock(&propMutex);
  if(propertiesRead == 0)
  {
    ReadProperties();
    propertiesRead = 1;
  }
  memcpy(pProps, &MasterProperties, sizeof(MasterProperties));
  pthread_mutex_unlock(&propMutex);
  return 0;
}

/*** shim interface between owl app and setting property
*
* @param key - the string representing the property name to write
* @param value - the value to be written to the property key
*
* @return 0
*/
int AudioPropertiesSet(const char *key, int value)
{
  char valueString[64];
  int status;

  sprintf(valueString, "%d", value);
  status = property_set(key, (const char *)&valueString);
  printf(" Property set %d for %s %s \n", status, key, valueString);
  InvalidatePropCache();

  return 0;
}

/** function to get monotonic clock and convert it to unsigned long long number
* of nanoseconds since startup.
*
* @return void
*/

unsigned long long getNsecs()
{
  unsigned long long temp;
  struct timespec get_time;
  int status;
  status = clock_gettime(CLOCK_MONOTONIC, &get_time);
  if(status != 0)
  { return 0; }
  temp = get_time.tv_sec; // convert seconds to a unsigned long long
  temp = temp * ONE_BILLION + get_time.tv_nsec;
  return temp;
}
/** function returns number of millseconds since startup
*
*/
unsigned long long getMsecs()
{

  return getNsecs() / 1000000;
}

/** PollTimers that indicate when a certain amount of time has passed.
*   Need to be polled by applcation.
*/
//
// Helper for managing a polled "timer"
//
/** Initialize a Poll timer.
*
* @param pTimer - pointer to a poll timer descriptor.
* @param interval - number of nanoseconds between poll time indications.
*
* @return 0
*/
int PollTimerInit(POLL_TIMER *pTimer, unsigned long long interval)
{
  if(pTimer == NULL)
  { return -1; }
  pTimer->interval = interval;
  pTimer->nextTime = 0;
  return 0;
}
/** Indicates whether a polled timer has expired, sets to next time if yes.
*
* @param pTimer - pointer to poll timer descriptor.
*
* @return 1 if the polled timer has expired; 0 otherwise.
*/
int PollTimerPollTimer(POLL_TIMER *pTimer)
{
  unsigned long long currTime;

  if(pTimer->nextTime == 0)
  {
    pTimer->nextTime = getNsecs() + pTimer->interval;
  }

  currTime = getNsecs();
  if(currTime > pTimer->nextTime)
  {
    pTimer->nextTime = getNsecs() + pTimer->interval;
    return 1;
  }

  return 0;
}

/** Indicates whether a polled timer has expired, does not reset.
*
* @param pTimer - pointer to poll timer descriptor.
*
* @return 1 if the polled timer has expired; 0 otherwise.
*/
int PollTimerPollTimerOnce(POLL_TIMER *pTimer)
{
  unsigned long long currTime;

  if(pTimer->nextTime == 0)
  {
    pTimer->nextTime = getNsecs() + pTimer->interval;
  }

  currTime = getNsecs();
  if(currTime > pTimer->nextTime)
  {
    return 1;
  }

  return 0;
}

/** Indicates whether a polled timer has expired, sets to next time if yes.
*
* @param pTimer - pointer to poll timer descriptor.
*
* @return 1 if the polled timer has expired; 0 otherwise.
*/
int PollTimerReset(POLL_TIMER *pTimer, unsigned long long interval)
{
  pTimer->interval = interval;
  pTimer->nextTime = getNsecs() + pTimer->interval;
  return 0;
}

/// Interval timers are used to keep track of time between events.

/** Initialize an interval timer.
*
* @param pTimer - pointer to interval descriptor
*
* @return 0
*/

int IntervalInit(INTERVAL *pTimer)
{
  pTimer->startTime = 0;
  pTimer->currTime = 0;
  return 0;
}

/** Sets the current time for an interval.
*
* If the interval start time has not been set yet, it will be set by the first
* call to this function.
*
* @param pTimer - pointer to interval descriptor
*
* @return return 0 if the interval is initialized else will return the amount
* of time since interval start
*/
unsigned long long IntervalCurrTime(INTERVAL *pTimer)
{
  if(pTimer->startTime == 0)
  {
    pTimer->startTime = getNsecs();
    pTimer->currTime = pTimer->startTime;
  }
  else
  {
    pTimer->currTime = getNsecs();
  }
  return pTimer->currTime - pTimer->startTime;
}

/** Returns the length of the current interval.
*
* Used to keep track of such things as total amount of time that an interface
* has been producing data.
*
* @param pTimer - pointer to interval timer descriptor
*
* @return The amount of time in nanoseconds between the start for the interval
* and the last time the current time was set.
*/

unsigned long long IntervalGetInterval(INTERVAL *pTimer)
{
  return pTimer->currTime - pTimer->startTime;
}


/// Circulare buffer routines that pass data between input devices and output
/// devices and encapsulate the signalling mechanism.

/** Cleans the stale data out of a circular buffer.
*
* @param pBufDesc - pointer to circular buffer descriptor
*
* @return 0
*/
int CircBuffClean(CIRC_BUFF *pBufDesc)
{
  pthread_mutex_lock(&pBufDesc->bufMutex);
  pBufDesc->readIndex = 0;
  pBufDesc->writeIndex = 0;
  pthread_mutex_unlock(&pBufDesc->bufMutex);
  return 0;
}

/** Calculates the amount of data currently in the circular buffer.
*
* used in internal circular buffer routines. Not mutex protected.
*
* @param pBufDesc - pointer to circular buffer descriptor
*
* @return The number of items in the circular buffer
*/
static int localFilled(CIRC_BUFF *pBufDesc)
{
  int amount;

  if(pBufDesc->readIndex <= pBufDesc->writeIndex)
  {
    amount =  pBufDesc->writeIndex - pBufDesc->readIndex;
  }
  else
  {
    amount = (pBufDesc->maxIndex - pBufDesc->readIndex) + pBufDesc->writeIndex;
  }
  return amount;

}

/** Circular buffer metric is a measure of how much of the buffer is occupied
* for how long.
*
* Everytime data is added to or removed from the buffer, the amount of data is
* multiplied by the length of time sine the last operation. The amounts are
* summed together to create the metric. This metric gives an idea of the rate
* mismatch between the writer and the reader.
*
* if the metric is increasing, then the read slower than the writer.
* if the metric is decreasing, then the writer is slower than the reader.
*/

/** Add data to circular buffer metric.
*
* expects the caller to hold lock.
*
* @param pBufDesc - pointer to circular buffer descriptor
*
* @return 0
*/

static int localAddMetric(CIRC_BUFF *pBufDesc)
{
  unsigned long long currTime;

  int currAmount;
  currAmount = localFilled(pBufDesc);
  currTime = getNsecs();
  pBufDesc->itemSecs += (currTime - pBufDesc->lastTime) * currAmount;
  pBufDesc->lastTime = currTime;
  return 0;
}

/** Returns the current buffer metric.
*
* The buffer metric is the number of item nanoseconds are occupied in the buffer.
*
* @param pBufDesc - pointer to circular buffer descriptor
* @param metric - pointer to a buffer to receive the metric structure.
*
* @return 0.
*/


int CircBuffGetMetric(CIRC_BUFF *pBufDesc, CIRC_BUFF_METRIC *pMetric)
{
  pthread_mutex_lock(&pBufDesc->bufMutex);
  localAddMetric(pBufDesc);
  pMetric->itemSecs =  pBufDesc->itemSecs;
  pMetric->measureTime = pBufDesc->lastTime - pBufDesc->restartMetricTime;
  pthread_mutex_unlock(&pBufDesc->bufMutex);
  return 0;
}

/** Resets the measurements of the cicular buffer metric.
*
* @param pBufDesc - pointer to circular buffer descriptor
*
* @return 0;
*/
int CircBuffResetMetric(CIRC_BUFF *pBufDesc)
{
  pthread_mutex_lock(&pBufDesc->bufMutex);
  pBufDesc->itemSecs = 0;
  pBufDesc->lastTime = getNsecs();
  pBufDesc->restartMetricTime = pBufDesc->lastTime;
  pthread_mutex_unlock(&pBufDesc->bufMutex);
  return 0;
}

/** Returns the amount of data in a circular buffer.
*
* External routine - protected by mutex
*
* @param pBufDesc - pointer to circular buffer descriptor
*
* @return The number of items in the circular buffer
*/
int CircBuffFilled(CIRC_BUFF *pBufDesc)
{
  int amount;
  pthread_mutex_lock(&pBufDesc->bufMutex);
  amount = localFilled(pBufDesc);
  pthread_mutex_unlock(&pBufDesc->bufMutex);
  return amount;
}

/** Returns the time stampe of the last write to the buffer and
* the amount of data in the buffer. Allows calculation of timestamp at
* statrt of buffer
*
* @param pBufDesc - pointer to circular buffer descriptor
* @param pTsDesc - pointer to timestamp to be filled in
*
* @return 0 if successful, -1 if not
*/

int CircBuffTimestamp(CIRC_BUFF *pBufDesc, CIRC_BUFF_TIMESTAMP *pTsDesc)
{

  pthread_mutex_lock(&pBufDesc->bufMutex);
  pTsDesc->totalWrites = pBufDesc->itemsIn;
  pTsDesc->amount = localFilled(pBufDesc);
  pTsDesc->lastWriteTime = pBufDesc->lastWriteTime;
  pTsDesc->overruns = pBufDesc->overrunCount;
  pthread_mutex_unlock(&pBufDesc->bufMutex);
  return 0;

}

/** Returns the amount of empty space in a circular buffer.
*
* External routine - uses protected filled routine
*
* @param pBufDesc - pointer to circular buffer descriptor
*
* @return The number of empty items in the circular buffer
*/
int CircBuffEmpty(CIRC_BUFF *pBufDesc)
{
  return pBufDesc->maxIndex - CircBuffFilled(pBufDesc);
}

/** Initialize a circular buffer.
*
* @param pBufDesc  - pointer to circular buffer descriptor
* @param maxIndex  - The maximum index allowed in the queue. Defines queue size
* @param itemSize  - The size of each item. Used to
* create structures over ITEM_DEF basic type.
* @param name      - pointer to a string containg a name for this circular buffer.
*
* @return -1 of malloc of underlying memory fails, 0 otherwise.
*/
int CircBuffInit(CIRC_BUFF *pBufDesc, INDEX maxIndex, int itemSize,
                 char *pName)
{
  int namelen;
  pBufDesc->maxIndex = maxIndex;
  pBufDesc->pBufPtr = malloc(maxIndex);
  if(pBufDesc->pBufPtr == NULL)
  { return -1; }
  pBufDesc->readIndex = 0;
  pBufDesc->writeIndex = 0;
  pthread_mutex_init(&pBufDesc->bufMutex, NULL);
  pthread_cond_init(&pBufDesc->bufCond, NULL);
  pBufDesc->restartCount = 0;
  pBufDesc->waitCount = 0;
  pBufDesc->totalWaitTime = 0;
  pBufDesc->itemsIn = 0;
  pBufDesc->itemsOut = 0;
  pBufDesc->itemSecs = 0;
  pBufDesc->lastTime = getNsecs();
  pBufDesc->lastWriteTime = 0;
  pBufDesc->overrunCount = 0;
  pBufDesc->itemSize = itemSize;
  namelen = strlen(pName);
  if(namelen > 15)
  { namelen = 15; }
  memcpy(&pBufDesc->name, pName, namelen);
  pBufDesc->name[16] = 0;
  if(SHOW_CB_DEBUG)
  {
    printf("Init CB %s - %d \n", pBufDesc->name, maxIndex);
  }

  return 0;
}

/** Returns the number of times this Circular buffer has been restarted.
*
* @param pBufDesc - pointer to circular buffer descriptor
*
* @return The number of times circular buffer has been restarted.
*/

int CircBuffRestartCount(CIRC_BUFF *pBufDesc)
{
  int status;
  pthread_mutex_lock(&pBufDesc->bufMutex);
  status = pBufDesc->restartCount;
  pthread_mutex_unlock(&pBufDesc->bufMutex);

  return status;
}
/** Restart the circular buffer.
*
* Restart the circular buffer by resetting all the buffer parameters.
* Does not released memory. Does signal any waiting threads.
*
* @param pBufDesc - pointer to circular buffer descriptor
*
* @return 0
*/

int CircBuffRestart(CIRC_BUFF *pBufDesc)
{
  int i;
  pthread_mutex_lock(&pBufDesc->bufMutex);
  pBufDesc->readIndex = 0;
  pBufDesc->writeIndex = 0;
  pBufDesc->restartCount++;
  for(i = 0; i < pBufDesc->maxIndex; ++i)
  { pBufDesc->pBufPtr[i] = 0;}
  pthread_cond_signal(&pBufDesc->bufCond);
  pthread_mutex_unlock(&pBufDesc->bufMutex);
  pBufDesc->waitCount = 0;
  pBufDesc->totalWaitTime = 0;
  pBufDesc->itemsIn = 0;
  pBufDesc->itemsOut = 0;
  CircBuffResetMetric(pBufDesc);
  return 0;

}

/** Wait for another thread to put something on this circular buffer.
*
* Each time a thread writes something to the circular buffer, the reader
* can be notified if waiting. useful if a reading thread needs to wait for
* an amount of dat to be available.
*
* @param pBufDesc - pointer to circular buffer descriptor
*
* @return The numebr of items in the queue
*/

int CircBuffWait(CIRC_BUFF *pBufDesc)
{
  unsigned long long startTime;
  unsigned long long endTime;

  pBufDesc->waitCount++;
  startTime = getNsecs();
  pthread_mutex_lock(&pBufDesc->bufMutex);
  pthread_cond_wait(&pBufDesc->bufCond, &pBufDesc->bufMutex);
  pthread_mutex_unlock(&pBufDesc->bufMutex);
  endTime = getNsecs();
  pBufDesc->totalWaitTime += (endTime - startTime);
  return CircBuffFilled(pBufDesc);
}

/** Timed Wait for another thread to put something on this circular buffer.
*
* Each time a thread writes something to the circular buffer, the reader
* can be notified if waiting. useful if a reading thread needs to wait for
* an amount of dat to be available. Can timeout
*
* @param pBufDesc - pointer to circular buffer descriptor
* @param timeInMs - time to wait
*
* @return if successfule, The numebr of items in the queue, -1 otherwise
*/

int CircBuffTimedWait(CIRC_BUFF *pBufDesc, int timeInMs)
{
  unsigned long long startTime;
  unsigned long long endTime;
  int rt;


  struct timeval tv;
  struct timespec ts;

  gettimeofday(&tv, NULL);
  ts.tv_sec = time(NULL) + timeInMs / 1000;
  ts.tv_nsec = tv.tv_usec * 1000 + 1000 * 1000 * (timeInMs % 1000);
  ts.tv_sec += ts.tv_nsec / (1000 * 1000 * 1000);
  ts.tv_nsec %= (1000 * 1000 * 1000);



  pBufDesc->waitCount++;
  startTime = getNsecs();
  pthread_mutex_lock(&pBufDesc->bufMutex);
  rt = pthread_cond_timedwait(&pBufDesc->bufCond, &pBufDesc->bufMutex, &ts);
  pthread_mutex_unlock(&pBufDesc->bufMutex);
  endTime = getNsecs();
  pBufDesc->totalWaitTime += (endTime - startTime);

  if(rt != 0)
  {return -1;}
  else
  {return CircBuffFilled(pBufDesc);}
}


/** Write data to a circular buffer
*
* @param pBufDesc - pointer to circular buffer descriptor
* @param pInData  - pointer to the data to put in circular buffer
* @param count    - count of number of items to put in queue
*
* @return depends on implemntation: if overrunable, 0; if not overrunnable,
* 0 if space to take write; -1 otherwise.
*/

int CircBuffWrite(CIRC_BUFF *pBufDesc, ITEM_DEF *pInData, int count)
{
  int i;
  int status;
  INDEX localWriteIndex; // user local write index to allow for
  // atomic indication of write complete
  ITEM_DEF *localpBufPtr = pBufDesc->pBufPtr;
  int overrunFlag = 0;
//
// Two implemenations: overrunable and not overrunable.
//  Not overrunable will refuse writes if there is not sufficient space.
//  Overrunable - will always accept data, overwriting oldest data items.
//#define NOT_OVERRUNABLE
#ifdef NOT_OVERRUNABLE

  if(count <= CircBuffEmpty(pBufDesc))
  {
    localAddMetric(pBufDesc);
    pthread_mutex_lock(&pBufDesc->bufMutex);
    localWriteIndex = pBufDesc->writeIndex;
    pthread_mutex_unlock(&pBufDesc->bufMutex);

    for(i = 0; i < count; ++i)
    {
      localpBufPtr[localWriteIndex] =  pInData[i];
      localWriteIndex++;
      if(localWriteIndex >= pBufDesc->maxIndex)
      { localWriteIndex = 0; }
    }
    pthread_mutex_lock(&pBufDesc->bufMutex);
    pBufDesc->writeIndex = localWriteIndex;
    pBufDesc->itemsIn += count;
    pthread_mutex_unlock(&pBufDesc->bufMutex);
    status = 0;
  }
  else
  {
    printf("CB Overrun \n");
    status = -1;
  }

  pthread_cond_signal(&pBufDesc->bufCond);
#else
  // set a flag if this operation wil overrun the buffer.
  if(count < CircBuffEmpty(pBufDesc))
  {
    overrunFlag = 0;
  }
  else
  {
    overrunFlag = 1;
    pBufDesc->overrunCount++;
  }

  localAddMetric(pBufDesc);
  pthread_mutex_lock(&pBufDesc->bufMutex);
  localWriteIndex = pBufDesc->writeIndex;
  pthread_mutex_unlock(&pBufDesc->bufMutex);

  // copy the data
  for(i = 0; i < count; ++i)
  {
    localpBufPtr[localWriteIndex] =  pInData[i];
    localWriteIndex++;
    if(localWriteIndex >= pBufDesc->maxIndex)
    { localWriteIndex = 0; }
  }
  pthread_mutex_lock(&pBufDesc->bufMutex);
  pBufDesc->writeIndex = localWriteIndex;
  pBufDesc->itemsIn += count;
  if(overrunFlag == 1) // if the q overruns then move read index to keep last data
  {
    pBufDesc->readIndex = pBufDesc->writeIndex + pBufDesc->itemSize;
    if( pBufDesc->readIndex >= pBufDesc->maxIndex)
    { pBufDesc->readIndex =  pBufDesc->readIndex - pBufDesc->maxIndex; }
  }
  pBufDesc->lastWriteTime = getNsecs();
  pthread_mutex_unlock(&pBufDesc->bufMutex);
  if(overrunFlag == 1)
  { status = 1; }
  else
  { status = 0; }
  // indicate write to waiting reader.
  pthread_cond_signal(&pBufDesc->bufCond);
#endif

  return status;

}

/** Read data from circular buffer.
*
* @param pBufDesc - pointer to circular buffer descriptor
* @param outData  - pointer to destination of the data.
* @param count    - number of items to read.
*
* @return 0 of data available; -1 if not.
*/


int CircBuffRead(CIRC_BUFF *pBufDesc, ITEM_DEF *pOutData, int count)
{
  int i;
  INDEX localReadIndex;
  ITEM_DEF *localpBufPtr = pBufDesc->pBufPtr;


  if(count > CircBuffFilled(pBufDesc))
  { return -1; }

  localAddMetric(pBufDesc);
  pthread_mutex_lock(&pBufDesc->bufMutex);
  localReadIndex = pBufDesc->readIndex;
  pthread_mutex_unlock(&pBufDesc->bufMutex);

  for(i = 0; i < count; ++i)
  {
    pOutData[i] = localpBufPtr[localReadIndex];
    localReadIndex++;
    if(localReadIndex >= pBufDesc->maxIndex)
    { localReadIndex = 0; }
  }
  pthread_mutex_lock(&pBufDesc->bufMutex);
  pBufDesc->readIndex = localReadIndex;
  pBufDesc->itemsOut += count;
  pthread_mutex_unlock(&pBufDesc->bufMutex);

  return 0;
}

/** Returns statistics about this circular buffer.
*
* @param pBufDesc - pointer to circular buffer descriptor
* @param pStats   - pointer to where to write the pStats.
*
* @return 0;
*/


int CircBuffpStats(CIRC_BUFF *pBufDesc, CIRC_BUFF_STATS *pStats)
{

  pthread_mutex_lock(&pBufDesc->bufMutex);
  pStats->itemsOut = pBufDesc->itemsOut;
  pStats->itemsIn = pBufDesc->itemsIn;
  pStats->totalWaitTime = pBufDesc->totalWaitTime;
  pStats->waitCount = pBufDesc->waitCount;
  pthread_mutex_unlock(&pBufDesc->bufMutex);

  return 0;

}

/// Helper routines for manipulating multichannel audio streams

/** Extracts a single channel from a multi-channel buffer.
*
* @param pInBuff - pointer to input buffer
* @param pOutBuff - pointer to the output buffer
* @param frameCount - number of frames in buffer
* @param channel - channel index to extract
* @param channelCount - total channels in buffer
*
* @return 0 if successful; -1 if not
*/

int ExtractChannel(short *pInBuff, short *pOutBuff, int frameCount,
                   int channel, int channelCount)
{
  int i;

  if(channel >= channelCount)
  {return -1;}

  for(i = 0; i < frameCount; ++i)
  {
    pOutBuff[i] = pInBuff[i * channelCount + channel];
  }


  return 0;
}

/** Inserts a single channel into a multi-channel buffer.
*
* @param pInBuff - pointer to input buffer
* @param pOutBuff - pointer to the output buffer
* @param frameCount - number of frames in buffer
* @param channel - channel index to extract
* @param channelCount - total channels in buffer
*
* @return 0 if successful; -1 if not
*/

int InsertChannel(short *pInBuff, short *pOutBuff, int frameCount,
                  int channel, int channelCount)
{
  int i;

  if(channel >= channelCount)
  {return 0;}

  for(i = 0; i < frameCount; ++i)
  {
    pOutBuff[i * channelCount + channel] = pInBuff[i];
  }


  return 0;
}




/** Initialize in-memory debug area
*
* @param pBuff - pointer to in-memory desciptor block.
* @param size - size of the array to access
*
* @return 0 if malloc succeeds; -1 otherwise
*/
int DBInit(DEBUG_BUFF *pBuff, int size)
{
  if(pBuff->pBuffer != NULL)
  { return 0; } // if already initialized ignore
  pBuff->pBuffer = malloc(size);
  if(pBuff->pBuffer == NULL)
  { return -1; }

  pBuff->pCurr = pBuff->pBuffer;
  pBuff->pEnd = pBuff->pBuffer + size;
  return 0;
}

/** Resets in-memory debug area
*
* @param pBuff - pointer to in-memory desciptor block.
*
* @return 0
*/
int DBReset(DEBUG_BUFF *pBuff)
{
  pBuff->pCurr = pBuff->pBuffer;
  memset(pBuff->pBuffer, 0, (pBuff->pEnd - pBuff->pBuffer));
  return 0;
}



/** wrapper to allow printf like access to in memory debug buffer.
*
* @param pDb - pointer to debug buffer to use.
* @param fmt - rest of printf like variable arguments.
*
* @return 0
*/

int DBPrintf(DEBUG_BUFF *pDb, char *fmt, ...)
{
  int len;
  if(pDb->pBuffer == NULL)
  {
    return 0;   // no buffer silently go away
  }
  va_list args;
  va_start(args, fmt);
  vsprintf(pDb->pCurr, fmt, args);
  va_end(args);

  len = strlen(pDb->pCurr);
  if((pDb->pCurr + len) >= pDb->pEnd)
  { pDb->pCurr = pDb->pBuffer; }
  pDb->pCurr += len;
  return 0;

}

/** close out a debug buffer structure and write it to a file.
*
* @param pDb - pointer to the debug buffer descriptor.
* @param filename -  name of file to write the dat to.
*
* @return 0
*/

int DBClose(DEBUG_BUFF *pDb, char *filename)
{
  FILE *fp;
  if(pDb->pBuffer == NULL)
  {
    return 0;   // no buffer silently go away
  }
  int amount = pDb->pCurr - pDb->pBuffer;
  fp = fopen(filename, "w+");
  if(fp != NULL)
  {
    fwrite(pDb->pBuffer, amount, 1, fp);
    printf("Writing %d bytes to %s \n", amount, filename);
    fclose(fp);
  }
  free(pDb->pBuffer);
  pDb->pBuffer = NULL;
  return 0;
}



/** Initialize the audio framework.
*
* Initializes anything required by the framework before the components are
* started. Reads the audio configuration from the property store.
* Required before initaudio or initspeaker.
*
* @return 0
*/
int InitAudioFramework()
{
  ReadProperties();
  return 0;
}

/// Audio Support thread - manages property and writing

/** Writes audio data to circular buffer to be picked up by audio manager thread.
*
* @param pWrite - pointer to the data to write
* @param count - number of bytes to write
*
* @return 0;
*/
int ASWriteDebugAudio(ITEM_DEF *pWrite, int count)
{
  int overrun = 0;

  overrun = CircBuffWrite(&writeDebugCB, pWrite, count);
  if(overrun == 1)
  { printf("Audio Debug write overran \n  ");}
  return 0;
}

/** Checks to see if anything needs to be done with the writing debug
* audio to a file
*
* @return void
*/
static void CheckUSBIfs()
{
#define HEARTBEAT_TIME 100000000  // 100ms
  static int lastSpkrTooLongFlag = 0;
  static int lastMicTooLongFlag = 0;
  static int micState = 0;
  static int spkrState = 0;
  unsigned long long currTime;
  unsigned long long diff;

  currTime = getNsecs();

  // check the microphone
  diff = currTime - LastMicUSBWriteTime;
  if(diff > HEARTBEAT_TIME)
  {
    if((lastMicTooLongFlag == 1) && (micState == 1))
    {
      printf("%llu - Mic USB inactive \n", getNsecs() / 1000000);
      SetUsbMicActive(USB_CHANNEL_INACTIVE);
      micState = 0;
    }
    else
    {
      lastMicTooLongFlag = 1;
    }
  }
  else
  {
    if((lastMicTooLongFlag == 0) && (micState == 0))
    {
      printf("%llu - Mic USB active \n", getNsecs() / 1000000);
      SetUsbMicActive(USB_CHANNEL_ACTIVE);
      micState = 1;
    }
    else
    {
      lastMicTooLongFlag = 0;
    }

  }

  diff = currTime - LastSpkrUSBReadTime;

  if(diff > HEARTBEAT_TIME)
  {
    if((lastSpkrTooLongFlag == 1) && (spkrState == 1))
    {
      printf("%llu - Speaker USB inactive \n", getNsecs() / 1000000);
      SetUsbSpkrActive(USB_CHANNEL_INACTIVE);
      spkrState = 0;
    }
    else
    {
      lastSpkrTooLongFlag = 1;
    }
  }
  else
  {
    if((lastSpkrTooLongFlag == 0) && (spkrState == 0))
    {
      printf("%llu - Speaker USB active \n", getNsecs() / 1000000);
      SetUsbSpkrActive(USB_CHANNEL_ACTIVE);
      spkrState = 1;
    }
    else
    {
      lastSpkrTooLongFlag = 0;
    }

  }

}
/** Check to see if there is any audio to write to the debug audio file
*
* @return void
*/

static void CheckWriteDebugAudio()
{
  static FILE *debugWriteFD = NULL;
  static int currAudioFileIndex = 0;
  static char filename[512];
  static AUDIO_PROPERTIES mgrProps;
  AUDIO_PROPERTIES oldMgrProps;
  int finalread;

  memcpy(&oldMgrProps, &mgrProps, sizeof(mgrProps));
  GetAudioProperties(&mgrProps);
  if((mgrProps.writeDebugAudio == 1) && (oldMgrProps.writeDebugAudio == 0))
  {
    sprintf(filename, "/data/audio/debug_audio_%d.raw", currAudioFileIndex);
    debugWriteFD = fopen(filename, "wb");
    printf("Opened debug audio file %s %p \n", filename, debugWriteFD);
  }
  else if((mgrProps.writeDebugAudio == 0) && (oldMgrProps.writeDebugAudio == 1))
  {
    finalread = CircBuffFilled(&writeDebugCB);

    CircBuffRead(&writeDebugCB, &debugAudioBuffer[0], finalread);
    if(debugWriteFD != NULL)
    {
      printf("Final write %d to file %s \n", finalread, filename);
      fwrite(debugAudioBuffer, finalread, 1, debugWriteFD);
    }

    if(debugWriteFD != NULL)
    {fclose(debugWriteFD);}
    printf("Closed %s \n", filename);
    debugWriteFD = NULL;
    currAudioFileIndex++;
  }
  while((CircBuffFilled(&writeDebugCB) > DEBUG_WRITE_SIZE))
  {
    CircBuffRead(&writeDebugCB, &debugAudioBuffer[0], DEBUG_WRITE_SIZE);
    // if writing to the file then write else drop
    if((mgrProps.writeDebugAudio == 1) && (debugWriteFD != NULL))
    {
      printf("Wrote %d to file %s \n", DEBUG_WRITE_SIZE, filename);
      fwrite(debugAudioBuffer, DEBUG_WRITE_SIZE, 1, debugWriteFD);
    }

  }
}

/** checks to see if the close file property has been set. If so, then close
* debug files (replaces close file on kill);
*
* @return void
*/
static void CheckDebugFiles()
{

  extern FILE *tousb_fd;
  extern FILE *frommic_fd;
  extern FILE *fromusb_fd;
  extern FILE *tospkr_fd;
  extern DEBUG_BUFF procMicDebugBuff;
  extern DEBUG_BUFF micDebugBuff;
  extern DEBUG_BUFF spkrDebugBuff;
  extern int FsEstDebugClose();

  static AUDIO_PROPERTIES mgrProps;
  static int filesOpen = 1;
  GetAudioProperties(&mgrProps);

  if((mgrProps.closeDebugFiles == 1) && (filesOpen == 1))
  {
    printf("Closing any open debug files \n");
    if((TO_USB_FILE_OUT) && (tousb_fd != NULL))
    {
      printf("Wrote usb to file \n");
      fclose(tousb_fd);
      tousb_fd = NULL;
    }
    if((FROM_MIC_FILE_OUT) && (frommic_fd != NULL))
    {
      printf("Wrote mic data to file \n");
      fclose(frommic_fd);
      frommic_fd = NULL;
    }
    if((FROM_USB_FILE_OUT) && (fromusb_fd != NULL))
    {
      printf("Wrote from usb to file \n");
      fclose(fromusb_fd);
      fromusb_fd = NULL;
    }
    if((TO_SPKR_FILE_OUT) && (tospkr_fd != NULL))
    {
      printf("Wrote spkr to file \n");
      fclose(tospkr_fd);
      tospkr_fd = NULL;
    }
    DBClose(&procMicDebugBuff, (char *)"/data/proc_mic.txt");
    DBClose(&micDebugBuff, (char *)"/data/microphone.txt");
    FsEstDebugClose();
    DBClose(&spkrDebugBuff, (char *)"/data/speaker.txt");
    filesOpen = 0;

  }


}

/** The audio manager - manages various audio things including properties andG
* audio debug write
*
* @return  - void
*/

static void *AudioSupportThread(void *arg)
{
  (void)arg;
  int i;
  static int loopCount = 0;
  static int lastmicClips;
  int tempMax;
  int tempMin;

  pthread_mutex_lock(&propMutex);
  ReadProperties();
  propertiesRead = 1;
  pthread_mutex_unlock(&propMutex);
  CircBuffInit(&writeDebugCB, WRITE_DEBUG_CB_SIZE, 8 * 2, "Write Debug");

  pthread_mutex_lock(&filePlayMutex);
  for(i = 0; i < FILE_CB_COUNT; ++i)
  {
    CircBuffInit(&filePlayCB[i], FILE_PLAY_CB_SIZE, 4, "FilePlay");
    filePlayCBInUse[i] = 0;
  }
  filePlayInited = 1;
  pthread_mutex_unlock(&filePlayMutex);



  while(runAudioMgr)
  {

    pthread_mutex_lock(&propMutex);
    ReadProperties();
    pthread_mutex_unlock(&propMutex);

    CheckWriteDebugAudio();
    CheckUSBIfs();
    CheckDebugFiles();

    usleep(100000);
    loopCount++;
    //
    // compute the clipping status of the mic processing
    // and print the results
    //
    if(AUDIO_SYSTEM_DEBUG)
    {
      if(loopCount > 10)
      {
        tempMin = micMin;
        tempMax = micMax;
        micMax = 0;
        micMin = 0;
        if(tempMax > totalMax)
        {
          totalMax = tempMax;
        }
        if(tempMin < totalMin)
        {
          totalMin = tempMin;
        }
        if(micClips != lastmicClips)
        {
          printf("Mic Stats clips %d; curr limits %d %d; session %d %d \n",
                 micClips, tempMax, tempMin, totalMax, totalMin);
        }
        lastmicClips = micClips;
        loopCount = 0;
      }
    }

  }


  return NULL;

}


/** Initialize the audio manager thread
*
* @return NULL
*/
void  ASInit()
{
  int result;
  struct sched_param params;

  runAudioMgr = 1;
  ReadProperties();
  PrintProperties();

  result = pthread_create(&audioMgrThread, NULL, AudioSupportThread, NULL);
  if (result == 0)
  {
    pthread_setname_np(audioMgrThread, "AudioSupport");
  }
  params.sched_priority = AUDIO_SUPPORT_PRIORITY;
  pthread_setschedparam(audioMgrThread, SCHED_FIFO, &params);

  return;

}

/** Stop the audio manager this_thread
*
* @return NULL
*/
void ASStop()
{

  runAudioMgr = 0;
  CircBuffRestart(&writeDebugCB); // let the manager go
  pthread_join(audioMgrThread, NULL);

  return;


}


#ifdef __cplusplus
}
#endif

