/* Copyright Playground Global 2016 */

#ifndef UVCHANDLER_H
#define UVCHANDLER_H

#ifdef __cplusplus
extern "C" {
#endif

/* UVC 1.0 Probe Control */
struct uvc_1p0_streaming_control {
	__u16 bmHint;
	__u8  bFormatIndex;
	__u8  bFrameIndex;
	__u32 dwFrameInterval;
	__u16 wKeyFrameRate;
	__u16 wPFrameRate;
	__u16 wCompQuality;
	__u16 wCompWindowSize;
	__u16 wDelay;
	__u32 dwMaxVideoFrameSize;
	__u32 dwMaxPayloadTransferSize;
} __attribute__((__packed__));

void UVCInputFrameCallback(uint8_t *buf, uint32_t len, int64_t ptsUsec, uint32_t flags);

int initUVC();

int startUVC();

int stopUVC();

/* Owlcore will call this to retrieve information on the USB connection */
int getUsbConnectedState(int *pUsbConnected, int *pUsbStateChanged);
/* Owlcore will call this to retrieve information on the UVC connection */
int getUvcStreamState(int *pUvcStreaming, int *pUvcStateChanged);
/* Owlcore will call this to retrieve information on the UVC connection */
int getUsbStateFixup(int *pUsbStateFixup);

int  getOwlCoreMeetingInProgress(int *pInMeeting, int *pInMeetingChanged);
void setOwlCoreMeetingInProgress(int inMeeting);

extern int gUvcHandlerStreaming;
extern int gOwlCoreMeetingInProgress;

#ifdef __cplusplus
}
#endif

#endif
