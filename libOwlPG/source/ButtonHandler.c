/* Copyright 2016 Owl Labs, Inc.

   Encapsulate Button interface
*/

#include <stdio.h>

#include "ButtonHandler.h"
#include "sx1508.h"

static int previous_state;

int testButtons(struct buttonStatus *buf)
{
	int ret;
	int events, current_state;

	if (buf == NULL) {
		fprintf(stderr, "%s: Error NULL buffer received!\n", __func__);
		return -1;
	}

	ret = sx1508_button_status(&events, &current_state);
	if (ret < 0) {
		fprintf(stderr, "%s: Err retrieving button state!\n", __func__);
		return ret;
	}

	/* sanity check the event reporting */
	if (previous_state != current_state && !events) {
		events = previous_state ^ current_state;
	}

	buf->previous = (uint8_t) previous_state;
	buf->current = (uint8_t) current_state;
	buf->events = (uint8_t) events;

	previous_state = current_state;

	return events;
}

int initButtons()
{
	int ret;

	previous_state = 0;

	ret = sx1508_button_init();
	if (ret < 0)
		fprintf(stderr, "%s: Error initializing SX1508!\n", __func__);

	return ret;
}

