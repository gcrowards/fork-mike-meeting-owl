/* MediaCodec Abstraction Library
   Copyright Playground Global 2016 */

#ifndef CODECHANDLER_H
#define CODECHANDLER_H

/* Codec Configuration */
#define CODEC_MJPEG
//#define CODEC_H264

#define ENCODER_WIDTH 1280
#define ENCODER_HEIGHT 720
#define ENCODER_FRAMERATE 30.0f

#ifdef CODEC_MJPEG
    #define ENCODER_FORMAT "video/x-motion-jpeg"
    #define ENCODER_BITRATE 1000000
#elif defined CODEC_H264
    #define ENCODER_FORMAT "video/avc"
    #define ENCODER_BITRATE 2000000
    #define ENCODER_IFRAME_INTERVAL 1
#endif

#include <unistd.h>
#include <EGL/egl.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Callback when encoded frame is available
   Note: Called in a different thread! */
typedef void (*codecOutputFrameCB)(uint8_t *buf, uint32_t size, int64_t ptsUsec, uint32_t flags);

/* Returns 0 on success, < 0 on error */

/* Initialize the MediaCodec with callback */
int initCodec();

/* Register a callback to handle codec outputs */
int registerCodecCallback(codecOutputFrameCB cb);

/* Get EGLNativeWindowType to render the frames to */
EGLNativeWindowType getCodecNativeWindow();

/* Start Encoding */
int startCodec();

/* Stop Encoding */
int stopCodec();

#ifdef __cplusplus
}
#endif

#endif
