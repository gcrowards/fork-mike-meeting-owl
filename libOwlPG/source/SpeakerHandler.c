/* Copyright (c) 2015-2016 Playground Global LLC. All rights reserved.
** capPlay.c
**
** based on tinycap.c from Android TinyALSA
**
** Copyright 2011, The Android Open Source Project
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of The Android Open Source Project nor the names of
**       its contributors may be used to endorse or promote products derived
**       from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY The Android Open Source Project ``AS IS'' AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL The Android Open Source Project BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
** OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
** DAMAGE.
*/



#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <signal.h>
#include <string.h>
#include <pthread.h>
#include <fcntl.h>

#include <linux/ioctl.h>
#include <tinyalsa/asoundlib.h>
#include "I2cHandler.h"


#include "audioframework.h"
#include "SampleRateProcessing.h"
#include "AudioProcessing.h"
#include "SpeakerHandler.h"

#define SOC_CARD 0


// externing tinyalsa function not in asoundlib.h
extern int pcm_state(struct pcm *pcm);

// Configuration params
#define PERIOD_SIZE     MASTER_PERIOD_SIZE
#define PERIOD_COUNT    4

#define MIN_SPKR_START ( 2*PERIOD_SIZE*BYTES_PER_FRAME)
#define USB_SPKR_BUFF_SIZE (MIN_SPKR_START*8)

#define WAIT_TOO_LONG (250000000LL)

#define SPKR_WRITE_TIMEOUT 100

#define SPKR_RMS_SMOOTHED_LEVEL_START -96.0
#define SPKR_RMS_SMOOTHED_LEVEL_UP_COEFF 0.9 // how fast the RMS level can rise
#define SPKR_RMS_SMOOTHED_LEVEL_DOWN_COEFF 0.01 // how fast the RMS level drops


// debug file descriptors
FILE *tospkr_fd;
FILE *fromusb_fd;

// copy of audio properties from android property store.
static AUDIO_PROPERTIES spkrProps;

unsigned long long peakOutTime = 0;

// the timestamp for the last successful read from USB
unsigned long long LastSpkrUSBReadTime;

static owlSpeakerActiveCB owlSpeakerActiveCallback = NULL;
static owlSpeakerGainCB   owlSpeakerGainCallback = NULL;

static int capturing = 1;
// threads for the write to speaker and read from usb
static pthread_t speakerThread;
static pthread_t usbReadThread;

static int spkrRead = 0;
static int spkrWritten = 0;


// dbug buffer for the speaker side;
double spkrLevel = -96.0;

DEBUG_BUFF spkrDebugBuff;

// circular buffer between usb read and speaker write
CIRC_BUFF usb_to_spkr;
// circular buffer to send speaker to reference.
extern CIRC_BUFF usb_to_ref;

// Sample rate converter descriptor for SRC between usb read and speaker write
SRC_DESC usb_to_spkr_src;

// sample rate estimator for usb reading.
FS_EST usb_read_fs_est;

// sample rate estimator for speaker output
// used in local gen mode to determine the FS of the speaker output
FS_EST spkr_write_fs_est;

// reference to mic sample rate estimator, used as output estimator for speaker writer
extern FS_EST mic_read_fs_est;
// The file playback circular buffers
extern int filePlayCBInUse[FILE_CB_COUNT];
extern CIRC_BUFF filePlayCB[FILE_CB_COUNT];



// forward declarations

static unsigned int read_from_usb(FILE *file, unsigned int card,
                                  unsigned int device,
                                  unsigned int channels, unsigned int rate,
                                  enum pcm_format format, unsigned int period_size,
                                  unsigned int period_count, unsigned int bits);
static unsigned int send_to_spkr(FILE *file, unsigned int card,
                                 unsigned int device,
                                 unsigned int channels, unsigned int rate,
                                 enum pcm_format format, unsigned int period_size,
                                 unsigned int period_count, unsigned int bits);

/** Provides access to the speaker level
*
* @return db of RMS calculation of last audio buffer sent to speaker
*/
double GetSpeakerRMSLevel()
{
  static double smoothedLevel = SPKR_RMS_SMOOTHED_LEVEL_START;
  static double upCoeff = SPKR_RMS_SMOOTHED_LEVEL_UP_COEFF;
  static double downCoeff = SPKR_RMS_SMOOTHED_LEVEL_DOWN_COEFF;
  double currCoeff;

  if(spkrLevel > smoothedLevel)
  { currCoeff = upCoeff;}
  else
  { currCoeff = downCoeff;}

  smoothedLevel = (currCoeff * spkrLevel) + ((1.0 - currCoeff) * smoothedLevel );

  return smoothedLevel;
}


/**  Wrapper for read_from_usb function.
*
* Initializes usb read alsa configuration parameters; initializes framework components; Calls read_from_usb
*
* @param arg - not used;
*
* @return void
*/
static void *ReadFromUsb(void *arg)
{
  unsigned int card = 1;
  unsigned int device = 0;
  unsigned int channels = 2;
  unsigned int rate = 48000;
  unsigned int bits = 16;
  unsigned int period_size = PERIOD_SIZE;
  unsigned int period_count = PERIOD_COUNT;

  enum pcm_format format;
  struct pcm_params *usb_params;
  static char param_string[4096];
  (void) arg;

  switch (bits)
  {
  case 32:
    format = PCM_FORMAT_S32_LE;
    break;
  case 24:
    format = PCM_FORMAT_S24_LE;
    break;
  case 16:
    format = PCM_FORMAT_S16_LE;
    break;
  default:
    fprintf(stderr, "Speaker: %d bits is not supported.\n", bits);
    return NULL;
  }


  if(SHOW_ALSA_DEBUG)
  {
    usb_params = pcm_params_get(card, device, 0);
    pcm_params_to_string(usb_params, param_string, 4096);
    printf("USB  IN driver params\n %s \n", param_string);
    pcm_params_free(usb_params);
  }
  /* begin capturing */
  while (capturing)
  {
    read_from_usb(NULL, card, device, channels, rate, format,
                  period_size, period_count, bits);
  }

  return NULL;
}

/** Wrapper for send_to_spkr routine.
*
* Initializes usb read alsa configuration parameters; initializes framework components; Calls send_to_spkr
*
* @param arg - not used
*
* @return void
*/
static void *WriteToSpeaker(void *arg)
{
  unsigned int card = 1;
  unsigned int device = 0;
  unsigned int channels = 2;
  unsigned int rate = 48000;
  unsigned int bits = 16;
  unsigned int period_size = PERIOD_SIZE;
  unsigned int period_count = PERIOD_COUNT;
  enum pcm_format format;
  (void) arg;

  switch (bits)
  {
  case 32:
    format = PCM_FORMAT_S32_LE;
    break;
  case 24:
    format = PCM_FORMAT_S24_LE;
    break;
  case 16:
    format = PCM_FORMAT_S16_LE;
    break;
  default:
    fprintf(stderr, "Speaker: %d bits is not supported.\n", bits);
    return NULL;
  }

  // initialize speaker sample rate estimator. used for debug.

  FsEstInit(&spkr_write_fs_est, DEFAULT_SAMPLE_RATE, "spkr wrt");
  /* begin capturing */
  while (capturing)
  {
    send_to_spkr(NULL, card, device, channels, rate, format,
                 period_size, period_count, bits);
  }

  return NULL;
}

/** Wrapper for tiny alsa parameter bounds cheecking
*
* @param params - ptr to pcm_parms structure. Obtained from tinyalsa interface
* @param param  - selects which parameter to check
* @param value - value to be checked for validity
* @param param_name  - name of parameter to be checked. used in debug output if check fails;
* @param param_unit - units of the parameter under check; used in debug output if check fails
*
* @return returns 1 if parameter is within bounds; 0 otherwise
*/
static int check_param(struct pcm_params *params, unsigned int param,
                       unsigned int value,
                       char *param_name, char *param_unit)
{
  unsigned int min;
  unsigned int max;
  int is_within_bounds = 1;

  min = pcm_params_get_min(params, param);
  if (value < min)
  {
    fprintf(stderr, "Speaker: %s is %u%s, device only supports >= %u%s\n",
            param_name, value,
            param_unit, min, param_unit);
    is_within_bounds = 0;
  }

  max = pcm_params_get_max(params, param);
  if (value > max)
  {
    fprintf(stderr, "Speaker: %s is %u%s, device only supports <= %u%s\n",
            param_name, value,
            param_unit, max, param_unit);
    is_within_bounds = 0;
  }

  return is_within_bounds;
}

/** checks to see if the selected device can play the proposed audio format.
*
* @param card      - alsa card ID for the selected interface
* @param device    - alsa device on the selected card on the selected device.
* @param channels  - requested number of channels to be played back.
* @param rate      - requested sample rate for playback
* @param bits      - requested number of bits per sample
* @param period_size - requested ALSA periodsize
* @param period_count - requested ALSA period count
*
* @return 1 if the sample format can be played on device. 0 otherwise
*/

static int sample_is_playable(unsigned int card, unsigned int device,
                              unsigned int channels,
                              unsigned int rate, unsigned int bits, unsigned int period_size,
                              unsigned int period_count)
{
  struct pcm_params *params;
  int can_play;

  params = pcm_params_get(card, device, PCM_OUT);
  if (params == NULL)
  {
    fprintf(stderr, "Speaker: Unable to open PCM device %u.\n", device);
    return 0;
  }

  can_play = check_param(params, PCM_PARAM_RATE, rate, "Sample rate", "Hz");
  can_play &= check_param(params, PCM_PARAM_CHANNELS, channels, "Sample",
                          " channels");
  can_play &= check_param(params, PCM_PARAM_SAMPLE_BITS, bits, "Bitrate",
                          " bits");
  can_play &= check_param(params, PCM_PARAM_PERIOD_SIZE, period_size,
                          "Period size", "Hz");
  can_play &= check_param(params, PCM_PARAM_PERIODS, period_count,
                          "Period count", "Hz");

  pcm_params_free(params);

  return can_play;
}


/** main loop for reading audio from usb and send it to the speaker.
*
* @param file      - not used
* @param card      - alsa card ID for the selected interface
* @param device    - alsa device on the selected card on the selected device.
* @param channels  - requested number of channels to be played back.
* @param rate      - requested sample rate for playback
* @param bits      - requested number of bits per sample
* @param period_size - requested ALSA periodsize
* @param period_count - requested ALSA period count
* @param bits      - number of bits per sample, not used embedded in format
*
* @return 0
*/


static unsigned int read_from_usb(FILE *file, unsigned int card,
                                  unsigned int device,
                                  unsigned int channels, unsigned int rate,
                                  enum pcm_format format, unsigned int period_size,
                                  unsigned int period_count, unsigned int bits)
{
  struct pcm_config config;
  struct pcm *pcm;
  struct pcm_debug *pcm_debug;
  char *buffer;
  unsigned int size;
  int readstatus = 0;
  unsigned long long startTime;
  unsigned long long endTime;
  int restart_flag  = 0;
  static int frame_count = 0;
  int curr_read_size = 0;
  static int totalRead = 0;
  int srcFramesOut = 0;
  int src_read_size = 0;
  int totalFrames = 0;
  int lastUnderruns = 0;


  (void) file;
  (void) bits;

  // Used to write samples read from USB to local file


  if(FROM_USB_FILE_OUT)
  {
    fromusb_fd = fopen("/data/audio/from_usb.raw", "wb");
  }

  // copy input paramters into alsa configuration structure.
  config.channels = channels;
  config.rate = rate;
  config.period_size = period_size;
  config.period_count = period_count;
  config.format = format;
  config.start_threshold = 0;
  config.stop_threshold = 0;
  config.silence_threshold = 0;

  // open the usb reader
  pcm = pcm_open(card, device, PCM_IN, &config);
  if (!pcm || !pcm_is_ready(pcm))
  {
    fprintf(stderr, "Speaker: Unable to open PCM device (%s)\n",
            pcm_get_error(pcm));
    return 0;
  }
  pcm_debug = (struct pcm_debug *)pcm;

  // allocate the receive buffer

  size = pcm_frames_to_bytes(pcm, PERIOD_SIZE);
  buffer = malloc(size + pcm_frames_to_bytes(pcm, SRC_ALLOWANCE));
  if (!buffer)
  {
    fprintf(stderr, "Speaker: Unable to allocate %d bytes\n", size);
    free(buffer);
    pcm_close(pcm);
    return 0;
  }

  if(SHOW_ALSA_DEBUG)
  {
    printf("Capturing sample: %u ch, %u hz, %u bit, buf_size %d\n", channels,
           rate, pcm_format_to_bits(format), size);
  }

  curr_read_size = size;

  // main read loop
  while (capturing)
  {
    startTime = getNsecs();
    readstatus = pcm_read(pcm, buffer, curr_read_size);
    endTime = getNsecs();

    if(readstatus == 0)
    {
      // send the data to the speaker output
      if(FROM_USB_FILE_OUT)
      {

        fwrite(buffer, src_read_size, 1, fromusb_fd);
      }

      DBPrintf(&spkrDebugBuff, "%llu,%llu,\n", getNsecs() - LastSpkrUSBReadTime,
               endTime - startTime);
      LastSpkrUSBReadTime = getNsecs();

    }

    // if the read fails
    if (readstatus || ((endTime - startTime) > READ_TIME_OUT))
    {
      // if the read fails then the stream from the host is closed; clean out the circular buffer to the speaker.
//            DBPrintf(&spkrDebugBuff,"Speaker: Error reading sample %d %d %d %d %llu \n",curr_read_size,readstatus,errno,usb_pcm_state,getNsecs());
      if(restart_flag == 0)
      {
        CircBuffRestart(&usb_to_spkr);
        CircBuffRestart(&usb_to_ref);
        restart_flag = 1;
        FsEstReset(&usb_read_fs_est, DEFAULT_SAMPLE_RATE);
      }
      // there's no stream from the host, yet we returned rather than
      // timing out - make sure we aren't hogging CPU by adding a sleep
      if (endTime - startTime < READ_TIME_OUT)
      {
        usleep(1000); // Delay 1 mSec to avoid being a CPU hog
      }
    }
    else
    {

      spkrRead += pcm_bytes_to_frames(pcm, curr_read_size);
      totalRead += curr_read_size;

      restart_flag = 0;
      if(USB_LOOPBACK)
      {src_read_size = curr_read_size;}
      else
      {
        frame_count = pcm_bytes_to_frames(pcm, curr_read_size);
        // update sample rate for usb estimate
        FsEstAddFrames(&usb_read_fs_est, frame_count);
        totalFrames += frame_count;

#ifdef ENABLE_SRC
        // sample rate conver the buffer of the dat.
        srcFramesOut = SRCBuff(&usb_to_spkr_src, buffer, frame_count);
        src_read_size = pcm_frames_to_bytes(pcm, srcFramesOut);
#else
        src_read_size = pcm_frames_to_bytes(pcm, frame_count);
#endif
      }
      // send the data to the speaker output
      if((FROM_USB_FILE_OUT) && false)
      {
        fwrite(buffer, src_read_size, 1, fromusb_fd);
      }

      CircBuffWrite(&usb_to_spkr, buffer, src_read_size);
//        CircBuffWrite(&usb_to_ref, buffer, src_read_size);

    }

    if(pcm_debug->underruns != lastUnderruns)
    {
      printf("Speaker USB XRUN error %llu %d \n", getNsecs(), pcm_debug->underruns);
      lastUnderruns = pcm_debug->underruns;
    }
  }
  if (buffer)
  { free(buffer); }
  pcm_close(pcm);
  return 0;
}





/** Mixes two buffers together. Assumed they are 16 bit 2 channel 48Khz
*
* @param buffer - the buffer beinng mixed into
* @param mixBuffer - the buffer bing mixed from
* @param amp - Amplitude in 1.15 to apply to data being mixed in
* @param length - Length in BYTES! - will be converted by routine.
*
* @return 0 if successful, -1 if not
*/
int MixBuffer(char *buffer, char * mixBuffer, short amp, int length)
{
  short *tempBuff = (short *) buffer;
  short *tempMix  = (short *) mixBuffer;
  int mix;
  int count = length/sizeof(short);
  int i;
  
  for(i = 0; i < count; ++i)
  {
    mix = ((int)*tempMix * (int)amp) >> 15;
    *tempBuff = (short) mix + *tempBuff;
    tempBuff++;
    tempMix++;
  }
  
  return 0;

}

/** main loop for reading audio from circular buffer and send it to the speaker
*
* @param file      - not used
* @param card      - alsa card ID for the selected interface
* @param device    - alsa device on the selected card on the selected device.
* @param channels  - requested number of channels to be played back.
* @param rate      - requested sample rate for playback
* @param bits      - requested number of bits per sample
* @param period_size - requested ALSA periodsize
* @param period_count - requested ALSA period count
* @param bits      - number of bits per sample, not used embedded in format
*
* @return 0
*/

static unsigned int send_to_spkr(FILE *file, unsigned int card,
                                 unsigned int device,
                                 unsigned int channels, unsigned int rate,
                                 enum pcm_format format, unsigned int period_size,
                                 unsigned int period_count, unsigned int bits)
{
  char *buffer;
  char *mixBuffer;
  short *tempShort;
  short *tempShortMix;
  int size;
  unsigned int bytes_read = 0;
  int i;
  struct pcm_config out_config;
  struct pcm *out_pcm = NULL;
  struct pcm_debug *out_pcm_debug;
  int status = 0;
  struct pcm_params*spkr_params;
  static char param_string[4096];
  int ii;
  int lastUnderruns = 0;
  int spkrRMS = 0;
  int starting = 1;
  int spkrDataAvail = 0;
  
#define PCM_CLOSED    0
#define PCM_READY     1
#define PCM_USED      2
  static int pcmState = PCM_CLOSED;


  (void) file;
  (void) card;

  if(TO_SPKR_FILE_OUT)
  {
    tospkr_fd = fopen("/data/audio/tospkr.raw", "wb");
  }
  // if requested print out the speaker alsa propereties
  if(SHOW_ALSA_DEBUG)
  {
    spkr_params = pcm_params_get(0, device, 0);
    pcm_params_to_string(spkr_params, param_string, 4096);
    printf("Spkr Out driver params\n %s \n", param_string);
    pcm_params_free(spkr_params);
  }


  // copy device configuration to alsa structure
  out_config.channels = channels;
  out_config.rate = rate;
  out_config.period_size = period_size / 2;
  out_config.period_count = period_count;
  out_config.format = format;
  out_config.start_threshold = 1;
  out_config.stop_threshold = 0;
  out_config.silence_threshold = 0;


  // ensure that we service the speaker active callback even when
  // there's a problem capturing a sample for loudspeaker playback
  if (owlSpeakerActiveCallback != NULL)
  { owlSpeakerActiveCallback(0, 0); }


  if (!sample_is_playable(0, device, channels, rate, bits, period_size,
                          period_count))
  {
    return 0;
  }

  // open the speaker alsa device
  out_pcm = pcm_open(0, device, PCM_OUT | PCM_MONOTONIC, &out_config);

  if(SHOW_ALSA_DEBUG)
  {printf("Spkr Alsa device ptr = %p \n", out_pcm);}

  if (!out_pcm || !pcm_is_ready(out_pcm))
  {
    fprintf(stderr, "Speaker: Unable to open PCM device %u (%s)\n",
            device, pcm_get_error(out_pcm));
    return 0;
  }

  pcmState = PCM_READY;
  out_pcm_debug = (struct pcm_debug *)out_pcm;

  size = pcm_frames_to_bytes(out_pcm, PERIOD_SIZE);
  buffer = malloc(size);
  if (!buffer)
  {
    fprintf(stderr, "Speaker: Unable to allocate %d bytes\n", size);
    if(out_pcm != NULL)
    { pcm_close(out_pcm); }

    return 0;
  }
  mixBuffer = malloc(size);
  if (!mixBuffer)
  {
    fprintf(stderr, "Speaker: Unable to allocate %d bytes for mix buffer\n", size);
    if(out_pcm != NULL)
    { pcm_close(out_pcm); }

    return 0;
  }

  if(SHOW_ALSA_DEBUG)
  
  {
    printf("To Spkr format: %u ch, %u hz, %u bit, buf_size %d\n", channels, rate,
           pcm_format_to_bits(format), size);
  }

  
  
  while (capturing)
  {
  
    static int lastgen = -1;
    GetAudioProperties(&spkrProps);    
    if(spkrProps.spkrLocalGen != lastgen)
    {
      printf("spkrlocalgen = %d \n",spkrProps.spkrLocalGen);
    }
    lastgen = spkrProps.spkrLocalGen;

    memset(mixBuffer, 0, size);
    
    if(spkrProps.spkrLocalGen != 0)
    {
        // send out know pattern while estimating sample rate.
        static int localFrameCount = 0;
        memset(buffer, 0, size);
        if(localFrameCount == 200)
        {
          int i;

          for(i = 1; i < 65; i += 4)
          {
            buffer[i] = 0x7f;
            buffer[i + 2] = 0x7f;
          }

          for(i = 65; i < 129; i += 4)
          {
            buffer[i] = 0x7f;
            buffer[i + 2] = 0x7f;
          }

          peakOutTime = getNsecs();
          localFrameCount = 0;
        }
        localFrameCount++;
        MixBuffer(mixBuffer,buffer,0x4000, size);
        spkrDataAvail = 1;
    }
       
        
    if(CircBuffFilled(&usb_to_spkr) >= size)
    {
      CircBuffRead(&usb_to_spkr,buffer,size);
      MixBuffer(mixBuffer,buffer,0x7fff,size);   
      spkrDataAvail = 1;             
    }
    
    for(i = 0; i < FILE_CB_COUNT; ++i)
    {
      if(filePlayCBInUse[i] == 1)
      {
        int avail; 
        avail = CircBuffFilled(&filePlayCB[i]);
//        printf("avail %p %d  \n",&filePlayCB[i],avail);
        // if threr is not enough to fill buffer then 
        // just take what is available, assume end of file
        if(avail < size) 
        {
          memset(buffer,0,size);
        }
        else
        {
          avail = size;
        }
        CircBuffRead(&filePlayCB[i],buffer,avail);
        MixBuffer(mixBuffer,buffer,0x4000,size);  
        spkrDataAvail = 1;                
      }
    }

    int size_int16;    
    short *buffer_int16 = (short *)mixBuffer;
    GetAudioProperties(&spkrProps);
    size_int16 = size / sizeof(short);

    if (owlSpeakerGainCallback != NULL)
    {
      // query owl-core for the software gain to apply to the
      // signals in buffer_int16[]. The gain returned by the
      // callback is a floating point value expected to be in
      // [0, 1], although values outside this range are legal.
      // Consider rolling this into the next loop if there's
      // concern about wasting CPU time looping through the same
      // array again.
      if(spkrProps.disableVolume == 0)
      {

        int intGain = 0.5 + 1024 * owlSpeakerGainCallback();
        for (ii = 0; ii < size_int16; ii++)
        { buffer_int16[ii] = buffer_int16[ii] * intGain / 1024; }
      }
    }
    if (owlSpeakerActiveCallback != NULL)
    {
      // use an estimate of energy in the PCM data to decide if
      // the speaker is playing sound or is quiet; call the
      // callback with the energy estimate and let it decide if
      // the loudspeaker is active
      // NOTE: HMB - this is only used to support eLOudspeaker debug. 
      // speaker active is harmonized with double talk detector speaker active.
      int n, x0, x1;
      unsigned long sum0, sum1;
      n = size_int16 / 2; // samples per channel
      sum0 = sum1 = 0;
      for (ii = 0; ii < size_int16; ii += 2)
      {
        x0 = buffer_int16[ii];
        x1 = buffer_int16[ii + 1];
        sum0 += abs(x0);
        sum1 += abs(x1);
      }
      owlSpeakerActiveCallback(sum0 / n, sum1 / n);
    }


    spkrRMS = CalcRMS(buffer_int16, size_int16 / 2, 0, 2);
    spkrLevel = mag2db_i(spkrRMS);
    if((TO_SPKR_FILE_OUT) && (tospkr_fd != NULL))
    {
      fwrite(mixBuffer, size, 1, tospkr_fd);
    }
    

      CircBuffWrite(&usb_to_ref, mixBuffer, size);                 


    int intGain = spkrProps.spkrTrim;
    for (ii = 0; ii < size_int16; ii++)
    { buffer_int16[ii] = (short)(((int)buffer_int16[ii] * intGain) >> 15); }
    status = pcm_write(out_pcm, mixBuffer, size);
    pcmState = PCM_USED;
    spkrWritten += pcm_bytes_to_frames(out_pcm, size);
    if (status)
    {
      DBPrintf(&spkrDebugBuff, "Speaker error %d %d \n", status, errno);
    }
    bytes_read += size;
    if(out_pcm_debug->underruns != lastUnderruns)
    {
      printf("Speaker write XRUN error %llu %d \n", getNsecs(),
             out_pcm_debug->underruns);
      lastUnderruns = out_pcm_debug->underruns;
    }

  }
  if (buffer)
  { free(buffer); }
  if(out_pcm != NULL)
  { pcm_close(out_pcm); }

  return 0;
}
/** intialize the usb->spkr path.
*
* @param activeCb - pointer to callback to report if speaker is active.
* @param gainCb - pointer to callback to get USB gain setting.
*
* @return 0 if completed successfully, non-zero otherwise.
*/
int initSpeaker(owlSpeakerActiveCB activeCb, owlSpeakerGainCB gainCb)
{
  GetAudioProperties(&spkrProps);

  // assign callback that lets owl-core know if the speaker is active
  owlSpeakerActiveCallback = activeCb;
  // assign callback that SpeakerHandler query owl-core for the output gain
  owlSpeakerGainCallback = gainCb;

  // initialize the sample rate estimator for the USB
  if(FsEstInit(&usb_read_fs_est, DEFAULT_SAMPLE_RATE, "usb read") != 0)
  { return -1; }

  // initialize the circular buffer between the usb reader and the speaker writer.
  if(CircBuffInit(&usb_to_spkr, USB_SPKR_BUFF_SIZE, BYTES_PER_FRAME,
                  "usb->spkr") < 0)
  { return -1; }

  // initialize the sample rate converter between the usb reader and the speaker writer.
  if(SRCInit(&usb_to_spkr_src,  BYTES_PER_FRAME, PERIOD_SIZE * 4,
             &usb_read_fs_est, &mic_read_fs_est, "usb->spkr") < 0)
  { return -1; }

  return 0;
}

/** starts the usb->spkr threads.
*
* @return result of usbReadThread creation
*/
int startSpeaker()
{
  int result;
  capturing = 1;
  if(SPKR_DEBUG_BUFF)
  {
    DBInit(&spkrDebugBuff, DEF_DEBUG_BUFF_SIZE);
  }
//setup the proper threads for loop back or usb-> speaker

  if(USB_LOOPBACK)
  {
    result = pthread_create(&usbReadThread, NULL, ReadFromUsb, NULL);
    if (result == 0)
    {
      pthread_setname_np(usbReadThread, "ReadFromUsb");
    }
    struct sched_param params;
    params.sched_priority = LOOPBACK_PCM_PRIORITY;
    pthread_setschedparam(usbReadThread, SCHED_FIFO, &params);
  }
  else
  {
    result = pthread_create(&speakerThread, NULL, WriteToSpeaker, NULL);
    if (result == 0)
    {
      pthread_setname_np(speakerThread, "AudioWriteToSpeaker");
    }
    struct sched_param params;
    params.sched_priority = SPKR_WRITE_PRIORITY;
    pthread_setschedparam(speakerThread, SCHED_FIFO, &params);

    result = pthread_create(&usbReadThread, NULL, ReadFromUsb, NULL);
    if (result == 0)
    {
      pthread_setname_np(usbReadThread, "AudioReadFromUsb");
    }
    params.sched_priority = READ_USB_PRIORITY;
    pthread_setschedparam(usbReadThread, SCHED_FIFO, &params);
  }

  return result;

}

/** stop the usb-> speaker path.
*
* @return 0
*/
int stopSpeaker()
{

  // tell threads to exit loops
  capturing = 0;
  // let any waiting threads off the hook
  CircBuffRestart(&usb_to_spkr);

  // wait for threads to complete
  if(USB_LOOPBACK)
  {
    pthread_join(usbReadThread, NULL);
  }
  else
  {
    pthread_join(speakerThread, NULL);
    pthread_join(usbReadThread, NULL);
  }

  return 0;
}

