///
/// This file contains functionality related to measuring and converting sample
/// rates.
////

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <signal.h>
#include <string.h>
#include <pthread.h>
#include <fcntl.h>
#include <math.h>

#include <linux/ioctl.h>
#include <cutils/properties.h>
#ifdef USE_SPEEX_RESAMPLE
#include "speex_resampler.h"
#endif
#include "audioframework.h"
#include "SampleRateProcessing.h"



extern DEBUG_BUFF fwDebugBuff;
AUDIO_PROPERTIES srcProps;



/// Routines to support linear interpolator

/** Initialize a linear interpolator SRC
*
* @param pLinDesc - pointer to a linear interpolator descriptor
* @param channels - the number of channels to be processed in each buffer
*
* @return 0 if sucessful; -1 otherwise
*/
int LiInit(LI_DESC *pLiDesc, int channels)
{
  int i;

  pLiDesc->fsRatio = 1.0;
  pLiDesc->sampleIndex = 0.0;

  pLiDesc->channels = channels;
  pLiDesc->inFs = 0;
  pLiDesc->outFs = 0;

  for (i = 0; i < MAX_CHANNELS; ++i)
  {
    pLiDesc->leftSample[i] = 0;
  }

  if (channels > MAX_CHANNELS)
  {
    return -1;
  }
  else
  {
    return 0;
  }


}


/** Set the input and output sample rates for the linear interpolator
*
* @param inFs - input fs rate for sample rate converter
* @param outFs - output fs rate for SRC
*
* @return 0 if both rates are valid; -1 if not
*/
int LiSetRates(LI_DESC *pLiDesc, double inRate, double outRate)
{

  double ratio;
  ratio = inRate / outRate;

  if(inRate != pLiDesc->inFs)
  {
    pLiDesc->inFs = inRate;
  }

  if(outRate != pLiDesc->outFs)
  {
    pLiDesc->outFs = outRate;
  }

  pLiDesc->fsRatio = ratio;



  return 0;
}

/** Linear interpolate a buffer of samples
*
* @param pLiDesc - pointer to a linear interpolator descriptor
* @param pInBuff - pointer to a buffer of input samples
* @param pOutBuff - pointer to buffer to contain output samples
* @param len - length of the buffer of samples; in frames
* @param pEndBuff - Pointer to the end of the output sample buffer
*
* @return returns length of returned samples; if error returns < 0
*/
int LiBuffer(LI_DESC *pLiDesc, short *pInBuff, short *pOutBuff, int len,
             short *pEndBuff)
{
  int i;
  int index;
  double indexDouble;
  double currPos;
  double frac;
  short leftSample;
  short rightSample;
  short outSample;
  int outSampleCount = 0;
  double maxPos = len - 1;


  currPos = pLiDesc->sampleIndex;

  if(pLiDesc->fsRatio < 0)
  {
    printf("LiBuffer negative ratio %f \n", pLiDesc->fsRatio);
    pLiDesc->fsRatio = 1;
  }


  do
  {
    frac = modf(currPos, &indexDouble);
    index = (int)indexDouble;
    if(currPos == -1.0)
    {
      // correct for pos of exactly -1.0
      frac = -1.0; 
    }
    if (currPos < 0)
    {
      for (i = 0; i < pLiDesc->channels; ++i)
      {
        leftSample = pLiDesc->leftSample[i];
        rightSample = pInBuff[i];
        // This is different from the interpolation below because modf of
        // of a negative number returns a negative fraction.
        *pOutBuff++ = round((leftSample * -frac)) + (rightSample * (1.0 + frac));
      }
      outSampleCount++;
    }
    else
    {
      for (i = 0; i < pLiDesc->channels; ++i)
      {
        leftSample = pInBuff[(index * pLiDesc->channels) + i];
        rightSample = pInBuff[((index + 1) * pLiDesc->channels) + i];
        *pOutBuff++ = round((leftSample * (1.0 - frac)) + (rightSample * frac));
      }
      outSampleCount++;
    }
    if(pOutBuff >= pEndBuff)
    {
      // Don't overrun the buffer
      break;
    }
    currPos += pLiDesc->fsRatio;

  }
  while (currPos <= maxPos);

  for (i = 0; i < pLiDesc->channels; ++i)
  {
    pLiDesc->leftSample[i] = pInBuff[((len - 1) * pLiDesc->channels) + i];
  }


  pLiDesc->sampleIndex = -(len - currPos);
  return outSampleCount;

}

/// Routines that implementa wrapper for sample rate conversion routines

/** Initialize a sample rate converter.
*
* @param pSrcDesc      - pointer to Sample Rate Converter descriptor.
* @param framesize     - size of the expected frames to be processed by the SRC
* @param srcMaxFrames  - the maximum number of frames the caller is expecting
* the be returned by the SRC
* @param pInFsEst      - pointer to a sample rate estimation descriptor that
* will be estimating the sample rate of the input to this SRC.
* @param pOutFsEst     - pointer to a sample rate estimation descriptor that
* will be estimating the sample rate of the out to this SRC.
* @param name          - pointer to a name for this SRC.
*
* @return -1 if malloc of buffer for SRC fails, 0 otherwise.
*/

// Routines for estimating and performing SRC

int SRCInit(SRC_DESC *pSrcDesc,
            unsigned int framesize,
            unsigned int srcMaxFrames,
            FS_EST *pInFsEst,
            FS_EST *pOutFsEst,
            char *name)
{

//    DBInit(&fwDebugBuff,DEF_DEBUG_BUFF_SIZE);
  GetAudioProperties(&srcProps);

  pSrcDesc->pInFsEst = pInFsEst;
  pSrcDesc->pOutFsEst = pOutFsEst;

  pSrcDesc->inFs = FsEstGetFs(pSrcDesc->pInFsEst);
  pSrcDesc->outFs = FsEstGetFs(pSrcDesc->pOutFsEst);
  if(AUDIO_SYSTEM_DEBUG)
  {
    printf("Initializing SRC %p %s to %10.4f %10.4f \n", pSrcDesc, name,
           pSrcDesc->inFs, pSrcDesc->outFs);
  }
  DBPrintf(&fwDebugBuff, "Initializing SRC %p %s to %10.4f %10.4f \n", pSrcDesc,
           name,
           pSrcDesc->inFs, pSrcDesc->outFs);

  pSrcDesc->framesize = framesize;
  pSrcDesc->srcMaxFrames = srcMaxFrames;
  pSrcDesc->srcStartTime = 0;
  pSrcDesc->inFrames = 0;
  pSrcDesc->outFrames = 0;

  PollTimerInit(&pSrcDesc->srcTimer, SRC_POLL_TIME);
#ifdef USE_SPEEX_RESAMPLE
  pSrcDesc->speexState = speex_resampler_init(2, pSrcDesc->inFs,
                         pSrcDesc->outFs, 1, &err);
#endif
  pSrcDesc->pSrcBuff = malloc(srcMaxFrames * framesize);
  if(pSrcDesc->pSrcBuff == NULL)
  { return -1; }

  LiInit(&pSrcDesc->liDesc, 2);
  LiSetRates(&pSrcDesc->liDesc, 48000.0, 48000.0);
  pSrcDesc->startSRC = 1;
  return 0;

}
/** Sample Rate convert a buffer of samples.
*
* @param pSrcDesc     - pointer to Sample Rate Converter descriptor.
* @param buffPtr      - pointer to the data to SRC'd. assumes 16 bit stereo inp..
* @param frameCount   - number of frames of data to SRC.
*
* @return number of frames output by SRC
*/

int SRCBuff(SRC_DESC *pSrcDesc, char *buffPtr, int frameCount)
{
  int srcFrameCount;
  unsigned int outFrameCount;


  (void)buffPtr;
  (void)frameCount;



  if(pSrcDesc->srcStartTime == 0)
  { pSrcDesc->srcStartTime = getNsecs(); }

  // setup to output as much data as the input buffer will generate.
  pSrcDesc->srcFrameCount = frameCount + 32;
  pSrcDesc->inFrames += frameCount;

  srcFrameCount = pSrcDesc->srcFrameCount;

  // for the moment use the speex resampler
#ifdef USE_SPEEX_RESAMPLE
  status = speex_resampler_process_interleaved_int(pSrcDesc->speexState,
           (const spx_int16_t *)buffPtr,
           &localFrameCount,
           (spx_int16_t *)pSrcDesc->pSrcBuff,
           &srcFrameCount );
  memcpy(buffPtr, pSrcDesc->pSrcBuff, srcFrameCount * pSrcDesc->framesize);
#else

  if(FsEstIsStable(pSrcDesc->pInFsEst) && FsEstIsStable(pSrcDesc->pOutFsEst))
  {
    pSrcDesc->startSRC = 1;
    LiSetRates(&pSrcDesc->liDesc,
               FsEstGetFs(pSrcDesc->pInFsEst),
               FsEstGetFs(pSrcDesc->pOutFsEst));
    if(srcProps.printFsEstimate != 0)
    {
      /*
              printf("%s - %llu Starting src %10.4f %10.4f \n",
                     pSrcDesc->pInFsEst->name,getNsecs(),
                     FsEstGetFs( pSrcDesc->pInFsEst),FsEstGetFs( pSrcDesc->pOutFsEst));
      */
    }


  }

  if((pSrcDesc->startSRC))
  {
    short *pEndBuff = pSrcDesc->pSrcBuff +
                      ((pSrcDesc->srcMaxFrames * pSrcDesc->framesize) / sizeof(short));
    srcFrameCount = LiBuffer(&pSrcDesc->liDesc, (short *)buffPtr,
                             pSrcDesc->pSrcBuff, frameCount, pEndBuff);
    memcpy(buffPtr, pSrcDesc->pSrcBuff, srcFrameCount * pSrcDesc->framesize);
    if(srcFrameCount != frameCount)
    {
      if(srcProps.printFsEstimate != 0)
      {
        /*
                printf("%s - frame size change - %d %d; %10.4f %10.4f \n",
                       pSrcDesc->pInFsEst->name, frameCount, srcFrameCount,
                       FsEstGetFs( pSrcDesc->pInFsEst),FsEstGetFs( pSrcDesc->pOutFsEst));
        */
      }
    }
  }
  else
  {
    srcFrameCount = frameCount;
  }
#endif

  // Copy the data back to the caller buffer

  pSrcDesc->outFrames += srcFrameCount;
  outFrameCount = srcFrameCount;

  // Periodically update the input and output sample rates
  if(PollTimerPollTimer(&pSrcDesc->srcTimer))
  {
    pSrcDesc->inFs = FsEstGetFs(pSrcDesc->pInFsEst);
    pSrcDesc->outFs = FsEstGetFs(pSrcDesc->pOutFsEst);
#ifdef USE_SPEEX_RESAMPLE
    // Speex resample changing rate takes too long.
//  speex_resampler_set_rate(pSrcDesc->speexState,pSrcDesc->inFs ,pSrcDesc->outFs);
#endif
  }


  return outFrameCount;

}


/**
*
* The sample rate estimation works by calculating the number of frames of audio
* that read/written to a interface over time. It does this by updating the amount
* time since an interface started and the amount of data processed in that time.
*
*/

/** Initialize a sample rate estimation.
*
* @param pFs - pointer to a sample rate estimation descriptor
* @param baseFS - The expected sample rate.
* @param name - pointer to name for this SRC.
*
* @return 0
*/

// Routines for estimating the sample rate of an interface

int FsEstInit(FS_EST *pFs, int baseFS, char *name)
{
  pthread_mutex_init(&pFs->estMutex, NULL);
  IntervalInit(&pFs->fsInterval);
  PollTimerInit(&pFs->pollTime, FS_EST_POLL_TIME);
  pFs->frameCount  = 0;
  pFs->skipCount   = FS_EST_SKIP_COUNT;
  pFs->currFs  = (double)baseFS;
  memcpy(pFs->name, name, 16);
  pFs->name[15] = 0;
  if(srcProps.printFsEstimate != 0)
  {
    printf("fs estimator %s base rate %10.4f \n", pFs->name, pFs->currFs);
  }


  return 0;

}


/** Resets the fs estimator.
*
* Used when stream is interrupted.
*
* @param pFs - pointer to fs rate estimation descriptor
* @param baseFs - expected sample rate
*
* @return 0;
*/
int FsEstReset(FS_EST *pFs, int baseFS)
{
  pthread_mutex_lock(&pFs->estMutex);
  IntervalInit(&pFs->fsInterval);
  PollTimerInit(&pFs->pollTime, FS_EST_POLL_TIME);
  pFs->frameCount  = 0;
  pFs->skipCount   = FS_EST_SKIP_COUNT;
  pFs->currFs  = (double)baseFS;
  pFs->lastFs = 0;
  pthread_mutex_unlock(&pFs->estMutex);
  if(srcProps.printFsEstimate != 0)
  {
    printf("fs estimator reset %s base rate %10.4f \n", pFs->name, pFs->currFs);
  }


  return 0;

}



/** Adds a frames to the sample rate estimation.
*
* records the time when the data is written as wwll. Periodically,
* update estimate of sample rate.
*
* @param pFs - pointer to a sample rate estimation descriptor
* @param frames - number of frames that have been sent/recivied.
*
* @return 0; 1 if estimator reset
*/
int FsEstAddFrames(FS_EST *pFs, int frames)
{
  ULL thisTime;

  if(pFs->frameCount > 2 * ONE_BILLION)
  {
    FsEstReset(pFs, DEFAULT_SAMPLE_RATE );
    return 1;
  }

  if(pFs->skipCount == 0)
  {
    thisTime = IntervalCurrTime(&pFs->fsInterval);
    if(thisTime != 0) // don't add frames during initialization
    { pFs->frameCount += frames; }

    thisTime = getNsecs();
    if(PollTimerPollTimer(&pFs->pollTime))
    {
      pthread_mutex_lock(&pFs->estMutex);
      double int_time = (double)IntervalGetInterval(&pFs->fsInterval);
      double frames = (double) pFs->frameCount;
      double sampleTime = int_time / frames;

      pFs->lastFs = pFs->currFs;

      pFs->currFs = 1000000000.0 / sampleTime;
      // limit the fs to +- 1%
      if(pFs->currFs < (48000 - 48))
      { pFs->currFs = (48000 - 48); }
      else if(pFs->currFs > (48000 + 48))
      { pFs->currFs = (48000 + 48); }
      pthread_mutex_unlock(&pFs->estMutex);
      if(srcProps.printFsEstimate != 0)
      {
        printf("Updating fs est %s; %10.4f \n", pFs->name, pFs->currFs);
      }
      DBPrintf(&fwDebugBuff, "Updating est %s; %10.4f \n", pFs->name, pFs->currFs);


    }
  }
  else
  {
    pFs->skipCount--;
  }

  return 0;

}
/** return the latest estimate of the sample rate.
*
* @param pFs - pointer to a sample rate estimation descriptor
*
* @return latest estimate of the sample rate, rounded to the nearest integer.
*/

double FsEstGetFs(FS_EST *pFs)
{
  double tempFs;
  pthread_mutex_lock(&pFs->estMutex);
  tempFs = pFs->currFs;
  pthread_mutex_unlock(&pFs->estMutex);
  if(tempFs < 0)
  {
    printf("%llu: Bad fs %f from %s \n", getNsecs(), tempFs, pFs->name);
  }
  if(tempFs == 0) // if structure unitiailized return default
  { return FS_EST_DEFAULT_FS; }
  else
  { return tempFs; }
}
/** Determines if the estimated sample rate is stable;
*
* @param pFs - pointer to sample rate estimation descriptor.
*
* @return 1 if sample rate estimate is stable; 0 otherwise
*/
int FsEstIsStable(FS_EST *pFs)
{
  double variance;

  if(pFs->currFs == 0)
  { return 0;}
  pthread_mutex_lock(&pFs->estMutex);
  variance = pFs->currFs - pFs->lastFs;
  pthread_mutex_unlock(&pFs->estMutex);

  if(variance < FS_STABLE)
  { return 1; }
  else
  { return 0; }

}

/** Closes the debug interface  for the sample rate estimation process.
*
* uses the in-memory debug buffer approach.
*
* @return 0
*/

int FsEstDebugClose()
{

  DBClose(&fwDebugBuff, "/data/framework.txt");
  return 0;

}

