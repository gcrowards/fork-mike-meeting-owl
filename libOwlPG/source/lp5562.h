/* LP5562 Device Driver
   Copyright Owl Labs, Inc. 2016 */

#ifndef LP5562_H
#define LP5562_H

/* LP5562: this device controls the 'eye' LEDs */
#define LP5562_SLAVE_ADDR	0x30

/* LP5562 Control Registers */
#define LP5562_EN	0x00	/* Default 0x00 */
#define LP5562_OP_MODE	0x01	/* Default 0x00 */
#define LP5562_CONFIG	0x08	/* Default 0x00 */
#define LP5562_MAP	0x70	/* Default 0x39 */

/* LP5562 Control Registers */
#define LP5562_B_PWM	0x02	/* Default 0x00 */
#define LP5562_G_PWM	0x03	/* Default 0x00 */
#define LP5562_R_PWM	0x04	/* Default 0x00 */
#define LP5562_W_PWM	0x0E	/* Default 0x00 */
#define LP5562_B_CUR	0x05	/* Default 0xAF */
#define LP5562_G_CUR	0x06	/* Default 0xAF */
#define LP5562_R_CUR	0x07	/* Default 0xAF */
#define LP5562_W_CUR	0x0F	/* Default 0xAF */

/* LP5562 Enable Register Bits - LP5562_EN */
#define LP5562_CHIP_EN	(1 << 6)	/* Enables IC (allow 500us delay) */

/* LP5562 Op Mode Register Bits - LP5562_OP_MODE */
#define LP5562_ENG_DIS		0x00		/* Disable all engines */
#define LP5562_ENG1_EN		(1 << 5)	/* Enable Engine 1 */

/* LP5562 Config Register Bits - LP5562_CONFIG */
#define LP5562_CLK_EN		(1 << 0)	/* Enables internal clock */

/* LP5562 Map Register Bits - LP5562_MAP */
#define LP5562_MAP_EN		0x00		/* Enable bitmask */
#define LP5562_MAP_DIS		0xFF		/* Disable bitmask */

/* LP5562 PWM Control Bits */
#define LP5562_PWM_FULL	0xFF	/* Set to 100% duty cycle (on) */

int lp5562_init(void);
int lp5562_led_enable(int enable);

#endif	/* LP5562_H */
