/* SX1508 Device Driver
   Copyright Owl Labs, Inc. 2016 */

#include <stdio.h>
#include <fcntl.h>

#include "I2cHandler.h"
#include "sx1508.h"

int sx1508_led_init(void)
{
	int fd, read_reg, write_reg;

	fd = openI2C(I2C_PORT_0, SX1508_SLAVE_ADDR);
	if (fd < 0) {
		fprintf(stderr, "sx1508_led_init: Error opening i2c device\n");
		return fd;
	}

	/* Disable input buffer for LED output ports */
	read_reg = i2cReadReg8(fd, SX1508_REG_IN_DISABLE);
	write_reg = read_reg | SX1508_LED_ALL;
	i2cWriteReg8(fd, SX1508_REG_IN_DISABLE, write_reg);

	if (!clk_enabled) {
		/* Set CLK to use internal oscillator */
		read_reg = i2cReadReg8(fd, SX1508_REG_CLK);
		write_reg = read_reg & ~SX1508_REG_CLK_OSC_SRC;
		write_reg = read_reg | SX1508_REG_CLK_2M_OSC;
		i2cWriteReg8(fd, SX1508_REG_CLK, write_reg);
		clk_enabled = 1;
	}

	/* Set CLKX to OSC / 64 (~31kHz) */
	read_reg = i2cReadReg8(fd, SX1508_REG_MISC);
	write_reg = read_reg | SX1508_REG_MISC_LEDCLK;
	i2cWriteReg8(fd, SX1508_REG_MISC, write_reg);

	/* Enable LED driver for output IOs */
	write_reg = SX1508_LED_ALL;
	i2cWriteReg8(fd, SX1508_LED_DRV_EN, write_reg);

	/* Set default intensity for IO 3 (RED) */
	write_reg = SX1508_RED_INTENSITY;
	i2cWriteReg8(fd, SX1508_IO3_INTENSITY, write_reg);

	/* Set default intensity for IO 7 (BLUE) */
	write_reg = SX1508_BLUE_INTENSITY;
	i2cWriteReg8(fd, SX1508_IO7_INTENSITY, write_reg);

	/* Set LED IO data to 0; make sure we only set input IOs to 1 here */
	read_reg = i2cReadReg8(fd, SX1508_PORT_DIR);	/* 1: port is input */
	write_reg = read_reg & ~SX1508_LED_ALL;
	i2cWriteReg8(fd, SX1508_REG_DATA, write_reg);

	close(fd);

	return 0;
}

int sx1508_led_enable(int colorMask, int enable)
{
	int mask;
	int fd = openI2C(I2C_PORT_0, SX1508_SLAVE_ADDR);
	if (fd < 0) {
		fprintf(stderr, "LED Driver: Error opening i2c device\n");
		return fd;
	}

	mask = i2cReadReg8(fd, SX1508_PORT_DIR);
	if (mask < 0) {
		fprintf(stderr, "LED Driver: Error reading LED_DRV_EN reg!\n");
		return -1;
	}

	/* Preserve the direction of other ports, while setting LED ports to
	 * 1 for disable, or 0 for enable */
	if (enable)
		mask &= ~colorMask;
	else
		mask |= colorMask;

	i2cWriteReg8(fd, SX1508_PORT_DIR, mask);

	close(fd);

	return 0;
}

int sx1508_button_init(void)
{
	int fd, read_reg, write_reg;

	fd = openI2C(I2C_PORT_0, SX1508_SLAVE_ADDR);
	if (fd < 0) {
		fprintf(stderr, "%s: Error opening i2c device\n", __func__);
		return fd;
	}

	/* Preserve current input buffer state for non-button IOs */
	read_reg = i2cReadReg8(fd, SX1508_REG_IN_DISABLE);
	if (read_reg < 0) {
		fprintf(stderr, "%s:Error reading REG_IN_DISABLE!\n", __func__);
		return -1;
	}
	/* Enable input buffer for all connected buttons */
	write_reg = read_reg & ~SX1508_BUTTON_ALL;
	i2cWriteReg8(fd, SX1508_REG_IN_DISABLE, write_reg);

	/* Preserve current direction for non-button IOs */
	read_reg = i2cReadReg8(fd, SX1508_PORT_DIR);
	/* Set input direction for all connected buttons */
	write_reg = read_reg | SX1508_BUTTON_ALL;
	i2cWriteReg8(fd, SX1508_PORT_DIR, write_reg);

	/* Preserve current polarity for non-button IOs */
	read_reg = i2cReadReg8(fd, SX1508_PORT_POLARITY);
	/* Set inverted polarity for all connected buttons */
	write_reg = read_reg | SX1508_BUTTON_ALL;
	i2cWriteReg8(fd, SX1508_PORT_POLARITY, write_reg);

	/* Set rising + falling edge trigger for high button IOs */
	write_reg = SX1508_REG_SNS_HI_VOLDN | SX1508_REG_SNS_HI_BT;
	i2cWriteReg8(fd, SX1508_REG_SNS_HI, write_reg);

	/* Set rising + falling edge trigger for low button IOs */
	write_reg = SX1508_REG_SNS_LO_MUTE | SX1508_REG_SNS_LO_VOLUP;
	i2cWriteReg8(fd, SX1508_REG_SNS_LO, write_reg);

	if (!clk_enabled) {
		/* Set reg clock to internal 2MHz oscillator */
		read_reg = i2cReadReg8(fd, SX1508_REG_CLK);
		write_reg = read_reg & ~SX1508_REG_CLK_OSC_SRC;
		write_reg = read_reg | SX1508_REG_CLK_2M_OSC;
		i2cWriteReg8(fd, SX1508_REG_CLK, write_reg);
		clk_enabled = 1;
	}

	/* Set debounce time to 16ms */
	write_reg = SX1508_DBOUNCE_CFG_MSK & SX1508_DBOUNCE_CFG_16MS;
	i2cWriteReg8(fd, SX1508_DBOUNCE_CFG, write_reg);

	/* Enable debounce on all button IOs */
	write_reg = SX1508_BUTTON_ALL;
	i2cWriteReg8(fd, SX1508_DBOUNCE_EN, write_reg);

	/* Clear all button IO events */
	write_reg = SX1508_BUTTON_ALL;
	i2cWriteReg8(fd, SX1508_REG_EVT, write_reg);

	/* Unmask button IOs to allow button events */
	write_reg = ~SX1508_BUTTON_ALL;
	i2cWriteReg8(fd, SX1508_INT_MASK, write_reg);

	close(fd);

	return 0;
}

int sx1508_button_status(int *events, int *current)
{
	int read_reg, write_reg;
	int fd = openI2C(I2C_PORT_0, SX1508_SLAVE_ADDR);
	if (fd < 0) {
		fprintf(stderr, "%s: Error opening i2c device!\n", __func__);
		return fd;
	}

	/* read IO event register */
	read_reg = i2cReadReg8(fd, SX1508_REG_EVT);
	if (read_reg < 0) {
		fprintf(stderr, "%s: Error reading REG_EVT reg!\n", __func__);
		return read_reg;
	}
	/* mask out non-button events */
	read_reg &= SX1508_BUTTON_ALL;
	*events = read_reg;
	write_reg = read_reg;

	/* read IO data register */
	read_reg = i2cReadReg8(fd, SX1508_REG_DATA);
	if (read_reg < 0) {
		fprintf(stderr, "%s: Error reading REG_DATA reg!\n", __func__);
		return read_reg;
	}
	/* mask out non-button states */
	read_reg &= SX1508_BUTTON_ALL;
	*current = read_reg;

	/* clear button events before returning */
	i2cWriteReg8(fd, SX1508_REG_EVT, write_reg);

	close(fd);

	return 0;
}

