///
/// Includes for audio processing routines.

#ifndef AUDIOPROCESSING_H_INCLUDED
#define AUDIOPROCESSING_H_INCLUDED

#define DIVISOR_1_31 2147483648.0
#define DIVISOR_1_15 32768.0

#define MAX_16BIT 32767

#ifdef __cplusplus
extern "C" {
#endif

extern int CalcRMS(short *pBuff, int len, int channel, int stride);
extern double mag2db_i(int value);
extern double pow2db_i(int value);
extern int db2mag(double dbval);
extern int db2pow(double dbval);

#if defined(__cplusplus)
}  /* extern "C" */
#endif

#endif // AUDIOPROCESSING_H_INCLUDED
