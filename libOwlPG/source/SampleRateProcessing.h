#ifndef SAMPLERATEPROCESSING_H_INCLUDED
#define SAMPLERATEPROCESSING_H_INCLUDED


#ifdef __cplusplus
extern "C" {
#endif

//#define USE_SPEEX_RESAMPLE
#ifdef USE_SPEEX_RESAMPLE
#include "speex_resampler.h"
#endif

/// structures and declarations for sample rate related propcessing.

/// defintion of sample rate estimator descriptor;
typedef struct
{
  pthread_mutex_t estMutex;  ///< mutex to control access to this data strcuture
  POLL_TIMER pollTime;  ///< length of time between sample raet estimation updates.
  INTERVAL fsInterval;  ///< the time between the first buffer processed and
                        ///< the last buffer processed.
  ULL totalTime;        ///< will be used when estimations cross stream barriers.
  ULL thisTime;
  int frameCount;       ///< number of frames added during fs_interval time
  int totalFrameCount;  ///< will be used when estimations cross stream barriers.
  int skipCount;        ///< the number of events to skip to allow the startup
                        ///< timing to settle down.
  double currFs;        ///< the current estimate of the sample rate.
  double lastFs;        ///< the last estiamte of the sample rate, used for debug.
  char name[16];        ///< name of estimator.
} FS_EST;


/// Definintion for linear interpolator SRC engine
typedef struct
{
  double fsRatio; ///< ratio of the input sample rate to the output sample rate
  double sampleIndex; ///< index of the next sample; including fraction
  int channels;   ///< the number of channels to SRC
  double inFs;
  double outFs;
  short leftSample[MAX_CHANNELS];   ///< last left sample from previous buffer

}LI_DESC;

/// definition of the sample rate converter descriptor.
typedef struct
{
  POLL_TIMER srcTimer;  ///< timer for updating the sample rates for the SRC
  FS_EST *pInFsEst;     ///< pointer to the estimator for the input sample rate.
  FS_EST *pOutFsEst;    ///< pointer to the estimator for the output sample rate.
  ULL srcStartTime;     ///< time of the first buffer sent to SRC, used for debug.
  double inFs;          ///< current input sample rate
  double outFs;         ///< current output sample rate
  int startSRC;         ///< indicates that fs are stable and start SRC
  short *pSrcBuff;      ///< temp buffer to sample rate convert into.
  int framesize;        ///< expected number of frames to be sent to SRC.
  int srcMaxFrames;     ///< maximum number of output samples from SRC.
  int srcFrameCount;    ///< number of frames from last SRC invocation.
  int inFrames;         ///< the numebr of frames sent to SRC.
  int outFrames;        ///< number of frames returned by SRC
#ifdef USE_SPEEX_RESAMPLE
  SpeexResamplerState *speexState;    ///< state of speex resampler;
#endif
  LI_DESC liDesc;

} SRC_DESC;


// Sample rate conversion functions
extern int SRCInit(SRC_DESC *, unsigned int framesize,
                   unsigned int max_frames, FS_EST *inFs, FS_EST* outFs, char *);
extern int SRCBuff(SRC_DESC *, char *, int);

// sample rate estimation functions.
extern int FsEstInit(FS_EST *, int baseFs, char *);
extern int FsEstReset(FS_EST *, int baseFs);
extern int FsEstAddFrames(FS_EST *, int);
extern double FsEstGetFs(FS_EST *);
extern int FsEstIsStable(FS_EST *);
extern int FsEstDebugClose();

#if defined(__cplusplus)
}  /* extern "C" */
#endif

#endif // SAMPLERATEPROCESSING_H_INCLUDED
