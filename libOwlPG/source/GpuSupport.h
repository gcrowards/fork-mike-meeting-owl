#ifndef GPU_SUPPORT_H
#define GPU_SUPPORT_H

#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

extern "C" {
  /*****************************************************/
  /* pass-through functions not supported by ndk-build */
  /*****************************************************/

  void setAndroidPresentationTime(EGLDisplay display, EGLSurface surface);

  void  *grBufGetNativeBuffer(int width, int height);
  void  grBufCopyPixels(unsigned char *dst);

  bool lockGrBufForFrame(void *pInFrame, void **pOutFrame, int *pWidth,
                         int *pHeight, int *pStride);
  void unlockGrBufForFrame();
}

#endif // GPU_SUPPORT_H

