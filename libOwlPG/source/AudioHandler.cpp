/* Copyright Playground Global 2016 */

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>
#include "audioframework.h"
#include "SampleRateProcessing.h"

#include <linux/ioctl.h>
#include <tinyalsa/asoundlib.h>
#include <sound/asound.h>

#include "AudioHandler.h"
#include "SpeakerHandler.h"
#include "AudioProcessing.h"
#include "audioPacket.h"
#include "AudioWaveforms.h"


#define USB_WRITE_TIMEOUT 500000000

#define MAX_REGISTERED_CALLBACKS 2

#define UAC_CARD 1
#define UAC_DEVICE 0
#define UAC_CHANNELS 2
#define UAC_RATE 48000
#define UAC_BITS 16
#define UAC_PERIOD_SIZE MASTER_PERIOD_SIZE
#define UAC_PERIOD_COUNT 4
#define UAC_FORMAT PCM_FORMAT_S16_LE

#define OUT_BUF_SAMPLE_COUNT 8192
#define OUT_BUF_BUFFER_COUNT 3
#define CIC_OVERSAMPLE 32
#define R 32600  // hipass
#define SAMPLES_PER_MIC_BUFFER 2048

#define AUDIO_PACKET_SIZE (8192+32)

#define RECORD_START_SIZE (UAC_PERIOD_SIZE*1*4)
#define LOOPBACK_START    (UAC_PERIOD_SIZE*1*4)


#define MIC_PROC_SIZE (PROC_FRAME_SIZE*8*2) // 8 mic channels
#define REF_PROC_SIZE (PROC_FRAME_SIZE*2*2) // 2 ref channels

// Sizes for the microphone associated Circular Buffers;
#define MIC_PROC_BUFF_SIZE (MIC_PROC_SIZE*4)
#define REF_PROC_BUFF_SIZE (REF_PROC_SIZE*4+REF_DELAY_SIZE)
#define PROC_USB_BUFF_SIZE (REF_PROC_SIZE*4)
#define PROC_LOCALIZER_BUFF_SIZE (4096*8*2*4)
#define ACTIVE_MIC_HYS_COUNT 10

/// defines for the possible states for the USB writer
#define CLOSED  0
#define OPEN    1
#define CLOSING 2
#define OPENING 3
// buffer size used for testing the writabilty of the usb write interface
#define TEST_WRITE_SIZE 32


#if defined(__cplusplus)
extern "C" {
#endif
extern int pcm_state(struct pcm *pcm);
extern int micClips;
extern int micMax;
extern int micMin;
#if defined(__cplusplus)
}  /* extern "C" */
#endif
/// define internal calculation size for the CIC filters
typedef long long FILT_TYPE;
//#define MIC_DEBUG

// debug files
FILE *tousb_fd;
FILE *frommic_fd;


// copy of the audio properties
static AUDIO_PROPERTIES micprops;

extern unsigned long long peakOutTime;

// contains the time stamp for the last time a successful mic write occurred
unsigned long long LastMicUSBWriteTime;

// Set parameter defaults
static int batch = 1;

// Define some packet pointers1
static uint8_t* data_in;

// Audio Thread
static pthread_t audioThread;
static pthread_t pcmThread;
static pthread_t procThread;

// extern of usb to speaker cb, used for debug
extern CIRC_BUFF usb_to_spkr;

// Circular buffer declarations
CIRC_BUFF mic_to_proc;
CIRC_BUFF proc_to_usb;
CIRC_BUFF usb_to_ref;
CIRC_BUFF proc_to_localizer;

// SRC descriptor for sample rate conversion between mic rate and usb rate
SRC_DESC mic_to_usb_src;

// sample rate estimator for mic sample rate.
FS_EST mic_read_fs_est;

// sample rate estimator for reads from usb, used as rate for SRC output.
extern FS_EST usb_read_fs_est;

// descriptor for in memory debug buffer
DEBUG_BUFF micDebugBuff;
DEBUG_BUFF procMicDebugBuff;


static char mpName[] = "mic->proc ";
static char puName[] = "proc->usb ";
static char urName[] = "usb->ref";
static char plName[] = "proc->local";
// buffer to receive audio data from driver.
short outputbuffer[AUDIO_PACKET_SIZE];

typedef struct
{
  FILT_TYPE integrator[4];
  FILT_TYPE last_integrator_out;
  FILT_TYPE last2_integrator_out;
  FILT_TYPE last_comb1;
  FILT_TYPE last2_comb1;
  FILT_TYPE last_comb2;
  FILT_TYPE last2_comb2;
  FILT_TYPE last_comb3;
  FILT_TYPE last2_comb3;
  FILT_TYPE x;
  FILT_TYPE y;

} CICFilter;


static struct pcm_config
  out_config; ///< alsa configurataion for usb output driver

// pcm data buffer; contains output data from CIC filter operations.
static int16_t pcmData[SAMPLES_PER_MIC_BUFFER + SRC_ALLOWANCE * 8];

// pcm data buffer; contains data from audio processing
#define MAX_TO_PROC_SIZE 4096
static int16_t procData[(PROC_FRAME_SIZE * MIC_CHANS) + SRC_ALLOWANCE * 8];

// pcm data buffer; contains the reference data from speaker output
static int16_t refData[(PROC_FRAME_SIZE * REF_CHANS) + SRC_ALLOWANCE * 8];

// CIC filter state
static CICFilter filter[8];
// list of callbacks to process audio;
/// TODO: look at moving callbacks out of processSPI to make processSPI more robust.
static audioOutputCB callback[MAX_REGISTERED_CALLBACKS];
static owlAudioOutputCB owlCallback = NULL;

// Controls when to leave reading loop.
static int stop = 0;

static short *pRampTable = NULL;
static int rampCount = 0;

/** Provicde the address of the circulare buffer between processing and
*   the localizer
*
* @return Address of the circ buff between processing and localizer
*/
CIRC_BUFF *GetProcToLocalizerCB()
{
  return &proc_to_localizer;
}



/** Generates the ramp table for a particular buffer size
*
* @param - frameCount - number of frames in the ramp table
* @param - chanCount - number of channels in the buffer
*
* @return 0 if succesful; -1 if not
*/
int GenRampTable(int frameCount, int chanCount)
{
  int i;
  int j;
  double incr;
  double val;
  int rampValue;
  short *pTemp;

  if(pRampTable != NULL)
  { return -1; }

  rampCount = frameCount + SRC_ALLOWANCE;

  pRampTable = (short *)malloc((frameCount + SRC_ALLOWANCE) * chanCount *
                               SAMPLE_SIZE);
  if(pRampTable == NULL)
  {
    printf("Ramp Table alloc failed %d \n", frameCount * chanCount * SAMPLE_SIZE);
    return -1;
  }

  pTemp = pRampTable;

  incr = -90.0 / frameCount;
  for(i = 0; i < frameCount; ++i)
  {
    val = (double)i;
    val = val * incr;
    rampValue = db2mag  ((double)val);
    for(j = 0; j < chanCount; ++j)
    {
      *pTemp++ = (short)rampValue;
    }
  }
  // append zeros to ramp to account for frame sizes changed by src

  for(i = 0; i < SRC_ALLOWANCE; ++i)
  {
    for(j = 0; j < chanCount; ++j)
    {
      *pTemp++ = 0;
    }
  }


  return 0;

}

/** Processes the audio stream throught the mute processor
*
* @param - muteState - state of mute; 1 if muted; 0 if not
* @param - pAudio - pointer to the audio buffer;
* @param - frameCount - Number of frames in audio buffer
*
* @return 0 if sucessful; -1 if not
*/
int ProcessMicMute(int muteState, short *pAudio, short frameCount)
{
  int i;
  int j;
  static int lastMuteState = 0;
  short *pTable = pRampTable;

  if(frameCount > rampCount)
  { frameCount = rampCount;}


  if((lastMuteState == 0) && (muteState == 0))  // not muted
  {
    return 0;
  }
  else if((lastMuteState == 1) && (muteState == 1)) // were muted and still muted
  {
    for(i = 0; i < frameCount; ++i)
    {
      for(j = 0; j < 2; ++ j)
      {*pAudio++ = 0;}
    }
  }
  else if((lastMuteState == 0) && (muteState == 1)) // going muted
  {

    pTable = pRampTable;
    for(i = 0; i < frameCount; ++i)
    {
      for(j = 0; j < 2; ++ j)
      {
        *pAudio = (*pAudio * *pTable) >> 15;
        pAudio++;
        pTable++;
      }
    }

  }
  else if((lastMuteState == 1) && (muteState == 0)) // going unmuted
  {
    pTable = pRampTable + frameCount * 2;
    for(i = 0; i < frameCount; ++i)
    {
      for(j = 0; j < 2; ++ j)
      {
        *pAudio = (*pAudio * *pTable) >> 15;
        pAudio++;
        pTable--;
      }
    }

  }

  lastMuteState = muteState;

  return 0;

}


/** CIC filter implemenation to translate between mic PDM and PCM
*
* Filters all 8 channels of incoming PDM to create 8 channels of PCM
*
* @param src - pointer to buffer of 8 single pdm streams
* @param len - number of samples in the PDM buffer
* @param dst - pointer to the destination PCM buffer
* @param filters - pointer to the CIC filter state
*
* @return void
*/
static void filter_fast(const uint8_t* src, int len, int16_t* dst,
                        CICFilter* filters)
{
  const uint8_t* end = src + len;
  FILT_TYPE output;
  int cicGain =  micprops.CICGain;
  int sampleCount = 0;

  while (src < end)
  {
    for (int c = 0; c < 8; c++)
    {
      CICFilter& f = filters[c];

      FILT_TYPE i1 = f.integrator[0];
      FILT_TYPE i2 = f.integrator[1];
      FILT_TYPE i3 = f.integrator[2];
      FILT_TYPE i4 = f.integrator[3];
      int m = 0x80 >> c;
      int count = CIC_OVERSAMPLE;
      while (count--) // 32x, reg to reg
      {
        i1 += (*src++ & m) ? 1 : -1;
        i2 += i1;
        i3 += i2;
        i4 += i3;
      }
      src -= CIC_OVERSAMPLE;
      f.integrator[0] = i1;
      f.integrator[1] = i2;
      f.integrator[2] = i3;
      f.integrator[3] = i4;

      FILT_TYPE comb1 = i4 - f.last2_integrator_out;
      FILT_TYPE comb2 = comb1 - f.last2_comb1;
      FILT_TYPE comb3 = comb2 - f.last2_comb2;
      FILT_TYPE s = (comb3 - f.last2_comb3) >> 5;
      // Highpass
      f.y = (s - f.x) + (R * f.y >> 15);
      f.x = s;
      // compensate for the gain through the CIC filter.
      // TODO: get this right, this is just and empirical value
      sampleCount++;


      output = (f.y * cicGain) >> 15;
      if(output > micMax)
      {
        micMax = output;
      }
      if(output < micMin)
      {
        micMin = output;
      }
      // limit y
      if(output > 32767)
      {
        output = 32767;
        micClips++;
      }
      else if(output < -32767)
      {
        output = -32767;
        micClips++;
      }

      *dst++ = (short)output;

      f.last2_integrator_out = f.last_integrator_out;
      f.last_integrator_out = i4;
      f.last2_comb1 = f.last_comb1;
      f.last_comb1 = comb1;
      f.last2_comb2 = f.last_comb2;
      f.last_comb2 = comb2;
      f.last2_comb3 = f.last_comb3;
      f.last_comb3 = comb3;
    }
    src += CIC_OVERSAMPLE;
  }
}

/** reorders the bytes in a packet of pdm samples.
*
* I guess that it is possible for the byte order of the packets
* from the atmel to get messed up.
*
* @param packet - pointer to the PDM data packet recieived from the Atmel micro
* @param length - length of PDM dat packet
*
* @return 0 if packet is processable; -1 if not
*/

static int reorderPacket(audio_t* packet, int length)
{
  DBG_PRINTF(DBG_INFO, "%s:%s\n", __FILE__, __FUNCTION__);
  register int i;
  register uint32_t* p = (uint32_t*) packet;
  register uint32_t data;
  register uint32_t magic = packet->header.magicnumber;


  // reorder the packet
  switch (magic)
  {
  case 0x08020101:    // 8 bit swaps due to spi munge
    for (i = 0; i < length / 4; i++, p++)
    {
      data = *p;
      *p = ((data & 0xff00ff00) >> 8) | ((data & 0x00ff00ff) << 8);
    }
    break;
  case 0x01010802:    // 32 bit endian
    for (i = 0; i < length / 4; i++, p++)
    {
      data = *p;
      *p = ((data & 0xff000000) >> 24) | ((data & 0x00ff0000) >> 8) | ((
             data & 0x0000ff00) << 8) | ((data & 0x000000ff) << 24);
    }
    break;
  case 0x01010208:    // 32 bit and spi munge
    for (i = 0; i < length / 4; i++, p++)
    {
      data = *p;
      *p = (data >> 16) | (data << 16);
    }
    break;
  case 0x02080101:    // Correct nothing to do
    break;
  default:            // Unknown so error out
    return -1;
  }

  return 0;
}

/** Reads data from mic circular buffer and writes it to USB when USB stream is open
*
* @param arg - not used
*
* @return void
*/

static void *writePCM(void *arg)
{

  (void) arg;
  unsigned int size;
  char *buffer;
  int interface_state = 0;
  int last_interface_state = 0;
  int curr_state;
  unsigned long long startTime;
  unsigned long long endTime;
  static unsigned long long duration;
  struct pcm *out_pcm = NULL;
  struct pcm_debug *out_pcm_debug = NULL;
  int write_size = TEST_WRITE_SIZE;
  CIRC_BUFF *source_cb = &proc_to_usb;
  struct pcm_params*usb_params;
  static char param_string[4096];
  int pcm_buffer_size;
  int localByteCount;
  int srcFramesOut;
  int intGain;
  short *buffer_int16;
  int size_int16;
  int ii;
  int lastUnderruns = 0;
  int pcm_write_status = 0;
  struct timespec tstamp;
  unsigned int avail;
  int status;



  if(TO_USB_FILE_OUT)
  {
    tousb_fd = fopen("/data/audio/tousb.raw", "wb");
  }

  if(SHOW_ALSA_DEBUG)
  {
    usb_params = pcm_params_get(UAC_CARD, UAC_DEVICE, 0);
    pcm_params_to_string(usb_params, param_string, 4096);
    printf("USB OUT driver params\n %s \n", param_string);
    pcm_params_free(usb_params);
  }


  if (!out_pcm)
  {
    out_config.channels = UAC_CHANNELS;
    out_config.rate = UAC_RATE;
    out_config.period_size = UAC_PERIOD_SIZE / 2;
    out_config.period_count = UAC_PERIOD_COUNT;
    out_config.format = UAC_FORMAT;
    out_config.start_threshold = 0;
    out_config.stop_threshold = 0;
    out_config.silence_threshold = 0;

    out_pcm = pcm_open(UAC_CARD, UAC_DEVICE, PCM_OUT, &out_config);
    out_pcm_debug = (struct pcm_debug *)out_pcm;
    if (!out_pcm || !pcm_is_ready(out_pcm))
    {
      fprintf(stderr, "Microphone: Unable to open PCM device %u (%s)\n",
              UAC_DEVICE, pcm_get_error(out_pcm));
    }
    if(SHOW_ALSA_DEBUG)
    {
      printf("Opened mic alsa %d \n", out_pcm_debug->buffer_size);
    }

  }
  pcm_buffer_size = pcm_get_buffer_size(out_pcm);

  if(SHOW_ALSA_DEBUG)
  {
    printf("USB PCM buffer size %d \n", pcm_frames_to_bytes(out_pcm,
           pcm_buffer_size));
  }
  size = pcm_frames_to_bytes(out_pcm, UAC_PERIOD_SIZE);
  write_size = size;
  buffer = (char *) malloc(size);
  if (!buffer)
  {
    fprintf(stderr, "Speaker: Unable to allocate %d bytes\n", size);
    if(out_pcm != NULL)
    { pcm_close(out_pcm); }

    return 0;
  }

  while(!stop)
  {

    // 4 state state machine

    // if the last read failed and this read failed then the interface is closed and don't do anything
    if((last_interface_state == 0) && (interface_state == 0))
    {
      curr_state = CLOSED;
      status = pcm_get_htimestamp(out_pcm, &avail, &tstamp);
      usleep(1000);   // don't hog CPU
    }
    // if the last read failed but this read succeed, then the host just opened a stream to read mic data.
    else if((last_interface_state == 0) && (interface_state == 1))
    {
      curr_state = OPENING;
      while(((unsigned)CircBuffFilled(source_cb) < RECORD_START_SIZE) && (!stop))
      {
        CircBuffWait(source_cb);
      }
      CircBuffRead(source_cb, buffer, size);

    }
    // if the last read succeeded but this one failed, then the host has closed the stream.
    else if((last_interface_state == 1) && (interface_state == 0))
    {
      curr_state = CLOSING;
      if(out_pcm != NULL)
      {
        pcm_close(out_pcm);
        out_pcm = NULL;
        out_pcm_debug = (struct pcm_debug *)out_pcm;
      }
    }
    // if last read succeeded and this succeed then we are open and sending data.
    else if((last_interface_state == 1) && (interface_state == 1))
    {

      curr_state = OPEN;
      while(((unsigned)CircBuffFilled(source_cb) < size) && (!stop))
      {
        CircBuffWait(source_cb);
      }

      CircBuffRead(source_cb, buffer, size);
      write_size = size;
    }


    last_interface_state = interface_state;

    // reopen to usb interface if necessary.

    if (!out_pcm)
    {
      out_config.channels = UAC_CHANNELS;
      out_config.rate = UAC_RATE;
      out_config.period_size = UAC_PERIOD_SIZE;
      out_config.period_count = UAC_PERIOD_COUNT;
      out_config.format = UAC_FORMAT;
      out_config.start_threshold = 0;
      out_config.stop_threshold = 0;
      out_config.silence_threshold = 0;

      out_pcm = pcm_open(UAC_CARD, UAC_DEVICE, PCM_OUT, &out_config);
      out_pcm_debug = (struct pcm_debug *)out_pcm;
      if (!out_pcm || !pcm_is_ready(out_pcm))
      {
        fprintf(stderr, "Microphone: Unable to open PCM device %u (%s)\n",
                UAC_DEVICE, pcm_get_error(out_pcm));
      }
    }

    if((TO_USB_FILE_OUT) && (tousb_fd != NULL))
    {
      fwrite(buffer, write_size, 1, tousb_fd);
    }

    // Sampler rate convert the buffer to USB rate from the mic rate.
    if(curr_state == OPEN)
    {
#ifdef ENABLE_SRC
      srcFramesOut = SRCBuff(&mic_to_usb_src, (char *)buffer,
                             pcm_bytes_to_frames(out_pcm, write_size));
      localByteCount = pcm_frames_to_bytes(out_pcm, srcFramesOut);
#else
      localByteCount = write_size;

#endif
    }
    else
    {
      localByteCount = write_size;
    }

    buffer_int16 = (short *)buffer;
    intGain = micprops.micTrim;
    size_int16  = localByteCount / 2;
    for (ii = 0; ii < size_int16; ii++)
    {
      buffer_int16[ii] = (short)(((int)buffer_int16[ii] * intGain) >> 15);
    }

    ProcessMicMute(GetMicMute(), (short *)buffer, pcm_bytes_to_frames(out_pcm,
                   localByteCount));

    startTime = getNsecs();
    pcm_write_status = pcm_write(out_pcm, buffer, localByteCount);
    endTime = getNsecs();

    status = pcm_get_htimestamp(out_pcm, &avail, &tstamp);

    if (pcm_write_status)
    {
      interface_state = 0;
    }
    else
    {
      LastMicUSBWriteTime = getNsecs();

      if(status == 0)
      {interface_state = 1;}
      else
      {interface_state = 0;}
    }
    // even if the write succeeded but took a really long time then declare the interface down.

    duration = endTime - startTime;
    if(duration > USB_WRITE_TIMEOUT) // if the wait is longer than 100ms, say so
    {
      interface_state = 0;
    }


    if((out_pcm_debug != NULL) && (out_pcm_debug->underruns != lastUnderruns))
    {
      printf("Microphone XRUN error %llu %d \n", getNsecs(),
             out_pcm_debug->underruns);
      lastUnderruns = out_pcm_debug->underruns;
    }

  }

  if (out_pcm)
  {
    pcm_close(out_pcm);
    out_pcm = NULL;
    out_pcm_debug = (struct pcm_debug *)out_pcm;
  }
  return NULL;
}

/** USB output half of usb audio loopback.
*
* Takes the output of the USB reader (in speakerhandler.c) and sends it back to USB.
* Works pretty much the same as writePCM, doesn't need SRC since it is run at the USB rate.
*
* @param arg - not used
*
* @return void
*/

static void *usb_loopback(void *arg)
{
  (void) arg;
  unsigned int size;
  char *buffer;
  int interface_state = 0;
  int last_interface_state = 0;
  unsigned long long startTime;
  unsigned long long endTime;
  static unsigned long long duration;
  struct pcm *out_pcm = NULL;
  int write_size = TEST_WRITE_SIZE;
  CIRC_BUFF *source_cb = &usb_to_spkr;
  int pcm_write_status = 0;
  struct timespec tstamp;
  unsigned int avail;
  int curr_state;
  int status;



  if (!out_pcm)
  {
    out_config.channels = UAC_CHANNELS;
    out_config.rate = UAC_RATE;
    out_config.period_size = UAC_PERIOD_SIZE;
    out_config.period_count = UAC_PERIOD_COUNT;
    out_config.format = UAC_FORMAT;
    out_config.start_threshold = 0;
    out_config.stop_threshold = 0;
    out_config.silence_threshold = 0;

    out_pcm = pcm_open(UAC_CARD, UAC_DEVICE, PCM_OUT, &out_config);
    if (!out_pcm || !pcm_is_ready(out_pcm))
    {
      fprintf(stderr, "Loopback: Unable to open PCM device %u (%s)\n",
              UAC_DEVICE, pcm_get_error(out_pcm));
    }
  }

  size = pcm_frames_to_bytes(out_pcm, UAC_PERIOD_SIZE);
  buffer = (char *) malloc(size);
  if (!buffer)
  {
    fprintf(stderr, "Speaker: Unable to allocate %d bytes\n", size);
    if(out_pcm != NULL)
    { pcm_close(out_pcm); }

    return 0;
  }
  while(!stop)
  {

    if((last_interface_state == 0) && (interface_state == 0))
    {
      curr_state = CLOSED;
      write_size = TEST_WRITE_SIZE;
    }
    else if((last_interface_state == 0) && (interface_state == 1))
    {
      curr_state = OPENING;
//      pcm_ioctl(out_pcm, SNDRV_PCM_IOCTL_RESET);
      CircBuffRestart(source_cb);
      while(((unsigned)CircBuffFilled(source_cb) < LOOPBACK_START) && (!stop))
      {
        CircBuffWait(source_cb);
      }
      CircBuffRead(source_cb, buffer, size);
      write_size = size;

    }
    else if((last_interface_state == 1) && (interface_state == 0))
    {
      printf("Closing loopback to USB %d %llu\n", CircBuffFilled(source_cb),
             duration / 1000000);
      curr_state = CLOSING;
      if(out_pcm != NULL)
      {
        pcm_close(out_pcm);
        out_pcm = NULL;
        memset(buffer, 0x30, size);
      }

      write_size = TEST_WRITE_SIZE;
    }
    else if((last_interface_state == 1) && (interface_state == 1))
    {
      curr_state = OPEN;
      while(((unsigned)CircBuffFilled(source_cb) < size) && (!stop))
      {
        CircBuffWait(source_cb);
      }

      CircBuffRead(source_cb, buffer, size);
#ifdef LOOPBACK
      descPtr = (audio_frame_desc *)buffer;
      if(descPtr->mark == 0x12345678)
      {
        DBPrintf(&spkrDebugBuff, "Frame %d - delay %llu \n", descPtr->frame_number,
                 getNsecs() - descPtr->timestamp);
      }
#endif
      write_size = size;
    }

    last_interface_state = interface_state;

    if (!out_pcm)
    {
      fprintf(stderr, "Loopback: Reopening PCM device");
      out_config.channels = UAC_CHANNELS;
      out_config.rate = UAC_RATE;
      out_config.period_size = UAC_PERIOD_SIZE;
      out_config.period_count = UAC_PERIOD_COUNT;
      out_config.format = UAC_FORMAT;
      out_config.start_threshold = 0;
      out_config.stop_threshold = 0;
      out_config.silence_threshold = 0;

      out_pcm = pcm_open(UAC_CARD, UAC_DEVICE, PCM_OUT, &out_config);
      if (!out_pcm || !pcm_is_ready(out_pcm))
      {
        fprintf(stderr, "Loopback: Unable to open PCM device %u (%s)\n", UAC_DEVICE,
                pcm_get_error(out_pcm));
      }
    }

    startTime = getNsecs();
    //    printf("in %d \n", frameCount);
    pcm_write_status = pcm_write(out_pcm, buffer, write_size);
    endTime = getNsecs();
    status = pcm_get_htimestamp(out_pcm, &avail, &tstamp);

    startTime = getNsecs();
    if (pcm_write_status)
    {
      interface_state = 0;
    }
    else
    {
      if(status == 0)
      {interface_state = 1;}
      else
      {interface_state = 0;}
    }
    endTime = getNsecs();
    duration = endTime - startTime;
    if(duration > 40000000) // if the wait is longer than 100ms, say so
    {
      interface_state = 0;
    }
  }


  if (out_pcm)
  {
    pcm_close(out_pcm);
    out_pcm = NULL;
  }
  (void)curr_state;
  return NULL;
}


/** Reads and processes frames of PDM audio from Atmel microcontroller
*
* @param arg - not used
*
* @return void
*/

static void *processSPI(void* arg)
{
  DBG_PRINTF(DBG_INFO, "%s:%s\n", __FILE__, __FUNCTION__);
  audio_t* packetIn = (audio_t*) data_in;
  int result = 0;
  (void) arg;
  static int restartCount = 0;
  uint32_t currTimestamp = 0;
  uint32_t tempTimestamp = 0;
  int restart = 0;
  int frameCount = 0;


  while(!stop)
  {

    int fd = open("/dev/misc_spi_mic", O_RDONLY);
    if (fd < 0)
    {
      DBG_PRINTF(DBG_ERROR, "Error opening spi_mics (%d)%s\n", errno,
                 strerror(errno));
      return NULL;
    }
    else
    {
      DBG_PRINTF(DBG_INFO, "opened spi_mics\n");
    }

    restartCount++;
    restart = 0;
    frameCount = 0;

    while ((!stop) && (!restart))
    {

      GetAudioProperties(&micprops);
      result = read(fd, packetIn, AUDIO_PACKET_SIZE);
      if (AUDIO_PACKET_SIZE != result)
      {
        printf("Microphone: Error read %d expected %d %d \n", result,
               AUDIO_PACKET_SIZE, errno);
      }

      frameCount++;

      if (packetIn->header.magicnumber != MAGICNO_8PDM1P)
      {
        // Change the endianess of the packet
        result = reorderPacket(packetIn, AUDIO_PACKET_SIZE);
      }
      tempTimestamp = packetIn->header.timestamp;

      // Convert from uController packet with buffer number into SOC / Host
      // packet with 32bit count by zeroing bufnumber
      packetIn->header.bufnumber = 0;


      if(tempTimestamp > currTimestamp)
      {

        // zero the PCM output buffer (belt and suspenders);
        memset(pcmData, 0, sizeof(pcmData));

        filter_fast(packetIn->data8, 8192, pcmData, filter);
        int bufFilled = CircBuffFilled(&mic_to_proc);
        CircBuffWrite(&mic_to_proc, (ITEM_DEF *)pcmData, 4096);
//        ASWriteDebugAudio((ITEM_DEF *)pcmData, 4096);
        currTimestamp = tempTimestamp;
        FsEstAddFrames(&mic_read_fs_est, 256);
      }
      else
      {
        if(MIC_DRVR_DEBUG)
        {printf("Mic read bad timestamp %d %d \n", tempTimestamp, currTimestamp);}
      }

    }

  }


  return NULL;

}

/** processes audio and passes it to owl application for processing
* done in third thread to minimize latency impact on mic process and usb processing
*
* @param arg - not used
*
* @return void
*/
static void *ProcessMic(void *arg)
{

  int i = 0;
  DBG_PRINTF(DBG_INFO, "%s:%s\n", __FILE__, __FUNCTION__);
  (void) arg;
  int micsize;
  AUDIO_IF_STATUS audioStat;
  static int waitingOnDelay = 1;
  int micCbRestartCount = 0;
  int refCbRestartCount = 0;
  int status;
  static int lastStatus = 0;
  static int activeCount = 0;
  short *pSampleIn;
  short *pSampleOut;
  int frameCount;

  if(FROM_MIC_FILE_OUT)
  {
    frommic_fd = fopen("/data/audio/from_mic.raw", "wb");
  }

  if(PROC_MIC_DEBUG_BUFF)
  {
    DBInit(&procMicDebugBuff, (1024 * 1024 * 8));
  }


  while(!stop)
  {

    if(micCbRestartCount != CircBuffRestartCount(&mic_to_proc))
    {}
    if(refCbRestartCount != CircBuffRestartCount(&usb_to_ref))
    {
      waitingOnDelay = 1;
      refCbRestartCount = CircBuffRestartCount(&usb_to_ref);
    }


    GetAudioProperties(&micprops);
    while((CircBuffFilled(&mic_to_proc) < MIC_PROC_SIZE) && (!stop))
    {
      CircBuffWait(&mic_to_proc);
    }
    micsize = MIC_PROC_SIZE;
    CircBuffRead(&mic_to_proc, (ITEM_DEF *) procData, micsize);

    if((FROM_MIC_FILE_OUT) && (frommic_fd != NULL))
    {
      fwrite(procData, micsize, 1, frommic_fd);
    }



    if(BYPASS_MIC_PROCESSING)
    {
      pSampleIn = procData;
      pSampleOut = procData;
      frameCount = micsize / 16; // 8 channels and 2 bytes per samnple
      for (i = 0; i < frameCount; ++i)
      {
        if(micprops.micLeftChan < 0)
        {*pSampleOut++ = 0;}
        else
        {*pSampleOut++ =  pSampleIn[i * 8 + micprops.micLeftChan];}
        if(micprops.micRightChan < 0)
        {*pSampleOut++ = 0;}
        else
        {*pSampleOut++ =  pSampleIn[i * 8 + micprops.micRightChan];}

      }

      status = CircBuffWrite(&proc_to_usb, (char *)procData, micsize / 4);
    }
    else
    {

      if((waitingOnDelay == 1)
          && (CircBuffFilled(&usb_to_ref) < (REF_PROC_SIZE + REF_DELAY_SIZE)))
      {
        AudioIfStatusGet(&audioStat);
        if(audioStat.refAvail == 1)
        {
          if(AEC_DEBUG)
          {printf("refAvail 1->0\n");}
        }
        AudioIfStatusRefAvail(0);
      }
      else
      {
        waitingOnDelay = 0;
        CircBuffRead(&usb_to_ref, (ITEM_DEF *)refData, REF_PROC_SIZE);
        AudioIfStatusGet(&audioStat);
        if(audioStat.refAvail == 0)
        {
          if(AEC_DEBUG)
          {printf("refAvail 0->1\n");}
          DBReset(&procMicDebugBuff);
        }
        AudioIfStatusRefAvail(1);
      }
      CircBuffWrite(&proc_to_localizer,(ITEM_DEF *)procData, MIC_PROC_SIZE);
      for (i = 0; i < MAX_REGISTERED_CALLBACKS; i++)
      {
        if (callback[i])
        {
          callback[i](procData, refData, MIC_PROC_SIZE / 2);
        }
        else
        {
          break;
        }
      }

      if((FROM_MIC_FILE_OUT) && (frommic_fd != NULL))
      {
        fwrite(outputbuffer, REF_PROC_SIZE, 1, frommic_fd);
      }
      status = CircBuffWrite(&proc_to_usb, (char *)outputbuffer, REF_PROC_SIZE);
    }
    lastStatus = status;

  }


  return NULL;

}


/** Method to extract 2 of the 8 samples and simply stuff it into
*   UAC output thread to be sent to the PC.
*
*   Eventually, this will have to be integrated with the beam formed information.
*
* @param pcmIn - ptr to 8 channel interleaved buffer of 16 bit samples
* @param sample_count - count of number samples (8*number of frames)
*
* @return void
*/
static void twoChannelOutput(int16_t *pcmIn, int16_t *pRefIn,
                             uint32_t sample_count)
{
  uint32_t i;
  uint32_t j;
  int localByteCount;
  static int localFrameCount = 0;

  localFrameCount = sample_count / 8;

  if (owlCallback == NULL)
  {
    // copy mics 4 and 6 to the output channels
    j = 0;
    for (i = 0; i < sample_count; i += 8)
    {
      if(micprops.micLeftChan < 0)
      {outputbuffer[j] = 0;}
      else
      {outputbuffer[j] =  pcmIn[i + micprops.micLeftChan];}
      j++;
      if(micprops.micRightChan < 0)
      {outputbuffer[j] = 0;}
      else
      {outputbuffer[j] =  pcmIn[i + micprops.micRightChan];}

      j++;
    }
  }
  else
  {
    // have the callback assemble the data for the output channels
    owlCallback(outputbuffer, pcmIn, pRefIn, sample_count);
  }

  sample_count = sample_count / 4; // sample count is now 1/4 what it was.

  localByteCount = localFrameCount * BYTES_PER_FRAME;
  // if local gen set, then put in a pattern of known ticks.
  if(micprops.micLocalGen != 0)
  {
    if(micprops.micLocalGen == 1)
    {
      static int localGenFrameCount = 0;
      memset(outputbuffer, 0, sample_count * 2);
      if(localGenFrameCount == 200)
      {
        int i;

        for(i = 0; i < 16 ; i += 2)
        {
          outputbuffer[i] = 0x3f00;
          outputbuffer[i + 1] = 0x3f00;
        }

        for(i = 16; i < 32; i += 2)
        {
          outputbuffer[i] = -0x3f00;
          outputbuffer[i + 1] = -0x3f00;
        }

        localGenFrameCount = 0;
      }
      localGenFrameCount++;
    }
    else if(micprops.micLocalGen == 2)
    {
      memcpy(outputbuffer, localSine, localByteCount);
    }
  }
}

/** Register callbacks for 8 channel mic data.
*
* @param cb - ptr to callback routine
*
* @return 0 if callback registered, 1 if all callbacks are full.
*/
int registerAudioCallback(audioOutputCB cb)
{
  for (int i = 0; i < MAX_REGISTERED_CALLBACKS; i++)
  {
    if (callback[i] == NULL)
    {
      callback[i] = cb;
      return 0;
    }
  }
  return 1;
}

/** initialize the microphone to usb path.
*
* @param cb - ptr to callback routine
*
* @return 0 if no error, non-zero if error
*/
int initAudio(owlAudioOutputCB cb)
{
  int i, ret;
  stop = 0;

  GenRampTable(UAC_PERIOD_SIZE, 2);

  GetAudioProperties(&micprops);
  // assign the callback that lets owl-core compose the UAC audio output
  // NOTE: this is outside the other callback mechanism for some reason.
  owlCallback = cb;

  // Allocate some buffers
  data_in = (uint8_t*) calloc(batch, AUDIO_PACKET_SIZE);
  if (data_in == NULL)
  {
    fprintf(stderr, "Could not allocate memory for audio processing\n");
    return 1;
  }

  // clear out the callbacks
  for (i = 0; i < MAX_REGISTERED_CALLBACKS; i++)
  {
    callback[i] = NULL;
  }

  ret = registerAudioCallback(twoChannelOutput);
  if (ret)
  {
    fprintf(stderr, "Could not register audio callback for microphone output\n");
    return ret;
  }

  // initialize the circular buffer, sample rate estimator for the mic, and mic->usb SRC
  if(CircBuffInit(&mic_to_proc, MIC_PROC_BUFF_SIZE, MIC_BYTES_PER_FRAME,
                  mpName) < 0)
  { return 1; }

    if(CircBuffInit(&proc_to_localizer, PROC_LOCALIZER_BUFF_SIZE, MIC_BYTES_PER_FRAME,
                  plName) < 0)
  { return 1; }



  if(CircBuffInit(&proc_to_usb, PROC_USB_BUFF_SIZE, BYTES_PER_FRAME, puName) < 0)
  { return 1; }

  if(CircBuffInit(&usb_to_ref, REF_PROC_BUFF_SIZE, BYTES_PER_FRAME, urName) < 0)
  { return 1; }


  if(FsEstInit(&mic_read_fs_est, DEFAULT_SAMPLE_RATE, (char *)"mic read") != 0)
  { return 0; }
  if(SRCInit(&mic_to_usb_src, BYTES_PER_FRAME, UAC_PERIOD_SIZE * 4,
             &mic_read_fs_est, &usb_read_fs_est, (char *)"mic to usb") < 0)
  { return 0; }

  return 0;
}

/** Start the mic to usb audio path running.
*
* @return result of pcm thread creation.
*/

int startAudio()
{
  int result = 0;
  struct sched_param params;

  // setup an in-memory debug buffer for the mic path; if desired
  if(MIC_DEBUG_BUFF)
  {
    printf("Opening MIC_DEBUG_BUFF \n");
    DBInit(&micDebugBuff, DEF_DEBUG_BUFF_SIZE);
  }

  ASInit();

  if(USB_LOOPBACK)
  {
    // initialize the usb loopback writer if wanted.
    result = pthread_create(&pcmThread, NULL, usb_loopback, NULL);
    if (result == 0)
    { pthread_setname_np(pcmThread, "pcmThread"); }
    params.sched_priority = LOOPBACK_PCM_PRIORITY;
    pthread_setschedparam(pcmThread, SCHED_FIFO, &params);
  }
  else
  {
    // initialize the thread that reads mic data from the Atmel processor
    // Mic interface is sensitive to dropped frames so make this thread as high a priority as possible.
    params.sched_priority = MIC_SPI_PRIORITY;
    result = pthread_create(&audioThread, NULL, processSPI, NULL);
    if (result == 0)
    { pthread_setname_np(audioThread, "audioThread"); }
    pthread_setschedparam(audioThread, SCHED_FIFO, &params);

    // initialize the thread that writes to the usb
    result = pthread_create(&procThread, NULL, ProcessMic, NULL);
    if (result == 0)
    { pthread_setname_np(procThread, "procThread"); }
    params.sched_priority = MIC_PROC_PRIORITY;
    pthread_setschedparam(procThread, SCHED_FIFO, &params);

    // initialize the thread that writes to the usb
    result = pthread_create(&pcmThread, NULL, writePCM, NULL);
    if (result == 0)
    { pthread_setname_np(pcmThread, "pcmThread"); }
    params.sched_priority = PROC_TO_USB_PRIORITY;
    pthread_setschedparam(pcmThread, SCHED_FIFO, &params);
  }

  return result;
}

/** stop the mic->usb processing path
*
* @return 0
*/
int stopAudio()
{
  // set global variable that indicates to threads to exit their processing loops.
  stop = 1;

  // wait for them to finish
  if(USB_LOOPBACK)
  {
    CircBuffRestart(&usb_to_spkr);
    pthread_join(pcmThread, NULL);
  }
  else
  {
    pthread_join(pcmThread, NULL);
    pthread_join(audioThread, NULL);
    pthread_join(procThread, NULL);
  }



  ASStop();
  // free allocated memory.
  free(data_in);
  return 0;
}
