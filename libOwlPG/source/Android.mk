LOCAL_PATH := $(call my-dir)

# Conference Sample Application
include $(CLEAR_VARS)
LOCAL_CFLAGS := -DGL_GLEXT_PROTOTYPES -DEGL_EGLEXT_PROTOTYPES -g

LOCAL_C_INCLUDES += \
    frameworks/native/include/media/openmax \
    frameworks/av/include/media/stagefright \
    system/media/camera/include \
    $(LOCAL_PATH)/libaudioPlatform/include \
    external/tinyalsa/include \
    external/jpeg \
    $(LOCAL_PATH)/libfacialproc


LOCAL_SHARED_LIBRARIES := \
    libEGL \
    libGLESv2 \
    libui \
    libgui \
    libutils \
    libbinder \
    libdl \
    libstagefright \
    libstagefright_foundation \
    libjpeg \
    libtinyalsa

LOCAL_LDLIBS := -L$(LOCAL_PATH)/libfacialproc -lfacialproc

LOCAL_32_BIT_ONLY := true
LOCAL_MODULE    := ConfSample
LOCAL_SRC_FILES := ConfSample.cpp \
    WindowSurface.cpp \
    CameraHandler.cpp \
    CodecHandler.cpp \
    AudioHandler.cpp \
    SpeakerHandler.c \
    audioframework.c \
    UVCHandler.c \
    GraphicsHandler.cpp \
    DirectorHandler.c

include $(BUILD_EXECUTABLE)
