/* Copyright 2016 Playground Global

   Conference Sample Application */


// -- use a #define to turn integration testing on or off

// If INTEGRATE is turned off, i.e., commented out, change
// libOwlPG/Android.mk to compile GraphicsHandler.cpp rather than
// GpuSupport.cpp, and comment out the line in libOwlPG/jni/Android.mk
// that adds gpu.cpp to the build. You also need to enable use of JPEG
// by uncommenting the two jpeg lines in libOwlPG/Android.mk and by
// uncommenting the line with "#define USE_JPEG" in DirectorHandler.c.

// Be sure to push the correct libOwlPG.so to the device.
#define INTEGRATE


#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>

#include "CameraHandler.h"
#include "CodecHandler.h"
#include "audioframework.h"
#include "AudioHandler.h"
#include "SpeakerHandler.h"
#ifdef INTEGRATE
#include "gpu.hpp"
#else
#include "GraphicsHandler.h"
#endif
#include "UVCHandler.h"
#include "DirectorHandler.h"
#include "DirectorCommands.h"
#include "LogDebug.h"

#include "owlHook.hpp"
#include "AudioEqualizer.hpp"

// globals
EQUALIZER leftEq;
EQUALIZER rightEq;



cOwlHook *gpOwlHook; /**< Interface with core application. */
#ifdef INTEGRATE
cGpu     *gpGpu    ; /**< Interface with GPU. */
#endif
render_command gpRenderCmds[MAX_RENDER_COMMANDS+1]; /**< Render commands from
						       owl-core to GPU. +1 so
						       there's always room for
						       an end-of-array guard. */

static int terminate = 0;
static pthread_t signalThread;
static void *signalHandler(void *arg)
{
    sigset_t *set = (sigset_t *)arg;
    int sig;
    while (1) {
        sigwait(set, &sig);
        fprintf(stderr, "Received signal %d - initiate self destruct\n", sig);
        terminate = 1;
    }
    return NULL;
}

#define CHECK(func) do { fprintf(stderr, "Running " #func "\n"); if (func){ fprintf(stderr, #func " failed\n"); return -1;} } while(0)


render_command* owlGetRenderCommands(void)
{
  panel_command panel;
  shape_command shape;
  static int count=0;

  // load some dummy data
  panel = (panel_command) {48, 10, 1184, 148, 0.00, 6.28, 0.23, 0.48, 1, 0, 0};
  gpRenderCmds[0].renderType = CMD_RENDER_PANEL;
  gpRenderCmds[0].panel = panel;
  panel = (panel_command) {10, 168, 1260, 542, 0.50, 2.22, 0.25, 0.45, 1, 0, 0};
  gpRenderCmds[1].renderType = CMD_RENDER_PANEL;
  gpRenderCmds[1].panel = panel;
  shape = (shape_command) {0, 0, 0, 0, 100, 100, 200, 200, 0, 1, 0, 0, 1, 2};
  gpRenderCmds[2].renderType = CMD_RENDER_RECTANGLE;
  gpRenderCmds[2].shape = shape;
  gpRenderCmds[3].renderType = CMD_RENDER_NONE;

  //if (count % 100 == 0) { fprintf(stderr, "owlGetRenderCommands %8d\n", count); }
  count++;
  return gpRenderCmds;
}

void   owlUpdate(int16_t *pPcmBuf, int bufLen, int nMics)
{
  static int count=0;
  //if (count % 100 == 0) { fprintf(stderr, "owlUpdate %8d %8d %8d\n", count, bufLen, nMics); }
  count++;
}

#ifdef INTEGRATE
void   cameraFrameCallback(void *eglClientBuffer)
{
  gpGpu->update(eglClientBuffer);
}
#endif


int owlInitAudioProcess()
{
	EqInit(&leftEq, 48000);
	EqConfigure(&leftEq, 0, HPF, 200, 0, 2.0);
	EqActivate(&leftEq, 0, 1);
	EqConfigure(&leftEq, 1, HPF, 200, 0, 2.0);
	EqActivate(&leftEq, 1, 1);


	EqInit(&rightEq, 48000);
	EqConfigure(&rightEq, 0,  HPF, 200, 0, 2.0);
	EqActivate(&rightEq, 0, 1);
	EqConfigure(&rightEq, 1,  HPF, 200, 0, 2.0);
	EqActivate(&rightEq, 1, 1);
	return 0;


}

void   owlAudioOutput(int16_t *pcmOut, int16_t *pcmIn, uint32_t sample_count)
{
  // copy two of the interleaved inputs to the outputs
  int i, j=0;
  AUDIO_PROPERTIES micOutProps;

  GetAudioProperties(&micOutProps);
  j = 0;

  for (i = 0; i < sample_count; i += 8)
  {
    if(micOutProps.micLeftChan < 0)
        {pcmOut[j] = 0;}
    else
        {pcmOut[j] =  pcmIn[i + micOutProps.micLeftChan];}
    j++;
    if(micOutProps.micRightChan < 0)
        {pcmOut[j] = 0;}
    else
        {pcmOut[j] =  pcmIn[i + micOutProps.micRightChan];}

    j++;
  }

  if(micOutProps.enableEq == 1)
  {
    EqProcess(&leftEq, pcmOut, sample_count/8, 0, 2);
    EqProcess(&rightEq, pcmOut, sample_count/8, 1, 2);
  }


}

int main(void)
{
    sigset_t set;

    /* Handling signal handling for the entire application */
    sigemptyset(&set);
    sigaddset(&set, SIGQUIT);
    sigaddset(&set, SIGTERM);
    sigaddset(&set, SIGINT);
    sigaddset(&set, SIGABRT);
    sigaddset(&set, SIGFPE);
    CHECK(pthread_sigmask(SIG_BLOCK, &set, NULL));
    CHECK(pthread_create(&signalThread, NULL, &signalHandler, (void *) &set));

    // Initialize hook to core application that consumes image and audio data
    gpOwlHook = new cOwlHook();

    /* Initialize director thread which consumes JPEG data and "directs" the Graphics pipeline */
    CHECK(initDirector());

    // register a callback that updates owl-core with image and audio data
    CHECK(registerDirectorUpdateCallback(owlUpdate));

    /* Initialize UVC and get callback which should be called whenever new data is available */
    CHECK(initUVC());

    /* Initialize MediaCodec with a callback that returns encoded data
       Input to MediaCodec is a NativeWindow which is initialized as part of Graphics pipeline */
    CHECK(initCodec());
    CHECK(registerCodecCallback(UVCInputFrameCallback));
#ifdef INTEGRATE
    // Register the JPEG callback if we want the director to
    // decompress the upper portion of the output frame to extract the
    // panoramic strip. Comment out the next line if we plan to have
    // the GPU render the computer vision panoramic strip separately,
    // as in the past for meetingOwl.

    //CHECK(registerCodecCallback(DirectorJPEGFrameCallback));
#else
    CHECK(registerCodecCallback(DirectorJPEGFrameCallback));
#endif

    /* Initialize Graphics stack with CodecNativeWindow which output of Graphics Rendering and input
       to the MediaCodec bufferqueue.  Input to Graphics stack is the Camera Callback which delivers
       new frames at 30FPS
       GraphicsHandler can take in GraphicsDirector which directs what to display next */
#ifdef INTEGRATE
    gpGpu = new cGpu(getCodecNativeWindow, owlGetRenderCommands);
#else
    CHECK(initGraphics());
    CHECK(registerGraphicsNativeWindowCallback(getCodecNativeWindow));
    CHECK(setGraphicsDirector(getDirectorCommands));
#endif

    /* Initialize Camera with Graphics Callback which is output of the Camera pipeline */
    CHECK(initCamera(RES_1728x1728));
#ifdef INTEGRATE
    CHECK(registerCameraCallback(cameraFrameCallback));
#else
    CHECK(registerCameraCallback(GraphicsCameraFrameCallback));
#endif
    CHECK(owlInitAudioProcess());

    /* Initialize Speaker with i2c and mixer changes */
    CHECK(initSpeaker(NULL, NULL));

    /* Initialize Audio Path and register callbacks to director for audio localization */
    CHECK(initAudio(owlAudioOutput));
    CHECK(registerAudioCallback(DirectorPCMCallback));

    /* Start the pipeline from output to input */
    CHECK(startDirector());
    CHECK(startUVC());
    CHECK(startCodec());
#ifdef INTEGRATE
    // no need to start gpGpu
#else
    CHECK(startGraphics());
#endif
    CHECK(startCamera());
    CHECK(startSpeaker());
    CHECK(startAudio());

    while(!terminate){
        sleep(1);
    }

    /* Tear down the pipeline in reverse order */
    CHECK(stopDirector());
    delete gpOwlHook;
    CHECK(stopAudio());
    CHECK(stopSpeaker());
    CHECK(stopCamera());
#ifdef INTEGRATE
    delete gpGpu;
#else
    CHECK(stopGraphics());
#endif
    CHECK(stopCodec());
    CHECK(stopUVC());
    return 0;
}
