#ifndef _GPU_HPP_
#define _GPU_HPP_

#ifndef EGL_EGLEXT_PROTOTYPES
#define EGL_EGLEXT_PROTOTYPES
#endif

#ifndef GL_GLEXT_PROTOTYPES
#define GL_GLEXT_PROTOTYPES
#endif

#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

#include "DirectorCommands.h"

/** Typedef for the codec function that returns an EGLNativeWindow. */
typedef EGLNativeWindowType (*codecNativeWindowFnType)();

/* Function Pointer for owl-core render commands */
typedef render_command* (*owlRenderCommandsCallback)();


class cGpu
{
public:
  cGpu(codecNativeWindowFnType nativeWindowFn,
    owlRenderCommandsCallback cb);
  ~cGpu();

  void update(void *pEglClientBuffer);

private:
  EGLDisplay mDisplay;
  EGLSurface mSurface;
  EGLContext mContext;
  GLuint     mProgram;
  int        mOutWidth, mOutHeight;
  GLfloat    *mpVertexes;
  codecNativeWindowFnType   mGetNativeWindowFn = NULL;
  owlRenderCommandsCallback mOwlRenderCommandsCb = NULL;
  GLuint     mCvPanoFbo; /**< offscreen FBO for panoramic strip. */
  GLuint     mCameraTexId; /**< texture id of camera image. */
  GLuint     mCvPanoFboTexId; /**< texture id of panoramic strip FBO. */
  GLuint     mLogoTexId;   /**< texture id of logo RGBA image. */
  int        mLogoWidth, mLogoHeight;
  GLuint     mRgbaOverlayTexId; /**< texture id of RGBA overlay image. */
  int        mRgbaOverlayX, mRgbaOverlayY;
  int        mRgbaOverlayWidth, mRgbaOverlayHeight;
  int        mRgbaOverlayRenderWidth, mRgbaOverlayRenderHeight;
  float      mRgbaOverlayAlpha;
  
  int    checkEglError(const char *pMsg);
  int    checkGlError(const char *pMsg);
  void   createProgram(void);
  void   finish(void);
  int    loadShader(int shaderType, const GLchar *pShaderStr);
  void   prepareAttributes(void);
  void   prepareCvPanoFbo(void);
  void   prepareEgl(void);
  void   prepareTextures(void);
  void   prepareUniforms(void);
  void   prepareVertexes(void);
  void   prepareViewport(void);
  void   renderFrame(void);
  void   setBackgroundColor(void);
  void   setBlending(void);
  void   setupGraphics(void);
  
  // shape rendering methods
  void   renderEllipse(int x, int y, int width, int height,
		       float r, float g, float b, float alpha,
		       float lineWidth, bool filled);
  void   renderLine(int x0, int y0, int x1, int y1,
		    float r, float g, float b, float alpha,
		    float lineWidth);
  void   renderRectangle(int left, int top, int width, int height,
			 float r, float g, float b, float alpha,
			 float lineWidth, bool filled);

  // panel and RGBA overlay rendering methods
  void   renderRgba(int renderType, int left, int top,
		    int width, int height, float alpha);
  void   renderRgbaOverlay(int left, int top, int width, int height,
			   float alpha);
  void   renderRgbaLogo(int left, int top, int width, int height, float alpha);
  void   renderUnroll(int left, int top, int width, int height,
		      float theta0, float theta1, float r0, float r1,
		      float alpha, float stretch, float bend);
};
  
#endif // _GPU_HPP_
