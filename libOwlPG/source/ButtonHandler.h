/* Button Abstraction Library
   Copyright Owl Labs, Inc. 2016 */

#ifndef BUTTONHANDLER_H
#define BUTTONHANDLER_H

#ifdef __cplusplus
extern "C" {
#endif

struct buttonStatus {
	uint8_t	previous;
	uint8_t current;
	uint8_t events;
};

int testButtons(struct buttonStatus *buf);
int initButtons();

#ifdef __cplusplus
}
#endif

#endif	/* BUTTONHANDLER_H */
