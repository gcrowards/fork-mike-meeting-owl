/* SX1508 Device Driver
   Copyright Owl Labs, Inc. 2016 */

#ifndef SX1508_H
#define SX1508_H

/* SX1508: this device controls the buttons and 'skirt' LEDs */
#define SX1508_SLAVE_ADDR	0x20	/* slave address on I2C-0 */

/* SX1508 Per-Port Control Registers */
#define SX1508_REG_IN_DISABLE	0x00	/* 1=disable input buf */
#define SX1508_PORT_POLARITY	0x06	/* 1=invert polarity */
#define SX1508_PORT_DIR		0x07	/* 1=set as input */
#define SX1508_REG_DATA		0x08	/* read status or control output */
#define SX1508_INT_MASK		0x09	/* 1=event will not trigger INT */
#define SX1508_LED_DRV_EN	0x11	/* 1=LED driver enabled */
#define SX1508_DBOUNCE_EN	0x13	/* 1=enable debounce */

/* SX1508 Per-Port Control Register Bits
 * Note: the following bitmasks are valid for all registers that control
 * per-port functionality */
#define SX1508_BUTTON_MUTE	(1 << 0)	/* Mute button on IO[0] */
#define SX1508_BUTTON_VOLUP	(1 << 1)	/* Volume up button on IO[1] */
#define SX1508_LED_RED		(1 << 3)	/* Red LED on IO[3] */
#define SX1508_BUTTON_VOLDN	(1 << 4)	/* Volume dn button on IO[4] */
#define SX1508_BUTTON_BT	(1 << 5)	/* Bluetooth button on IO[5] */
#define SX1508_LED_BLUE		(1 << 7)	/* Blue LED on IO[7] */
#define SX1508_BUTTON_ALL	(SX1508_BUTTON_MUTE | SX1508_BUTTON_VOLUP |\
				 SX1508_BUTTON_VOLDN | SX1508_BUTTON_BT)
#define SX1508_LED_ALL		(SX1508_LED_RED | SX1508_LED_BLUE)

/* SX1508 Other Control Registers */
#define SX1508_REG_SNS_HI	0x0A	/* Default 0x00 */
#define SX1508_REG_SNS_LO	0x0B	/* Default 0x00 */
#define SX1508_REG_CLK		0x0F	/* Default 0x00 */
#define SX1508_REG_MISC		0x10	/* Default 0x00 */
#define SX1508_DBOUNCE_CFG	0x12	/* Default 0x00 */
#define	SX1508_IO3_INTENSITY	0x1C	/* Default 0xFF */
#define SX1508_IO7_INTENSITY	0x26	/* Default 0xFF */

/* SX1508_REG_SNS Control Bits */
#define SX1508_REG_SNS_HI_VOLDN	(3 << 0)
#define SX1508_REG_SNS_HI_BT	(3 << 2)
#define SX1508_REG_SNS_LO_MUTE	(3 << 0)
#define SX1508_REG_SNS_LO_VOLUP	(3 << 2)

/* SX1508_REG_CLK Control Bits */
#define SX1508_REG_CLK_OSC_SRC	(3 << 5)
#define SX1508_REG_CLK_2M_OSC	(1 << 6)

/* SX1508_REG_MISC Control Bits */
#define SX1508_REG_MISC_LEDCLK	(7 << 4)

/* SX1508_DBOUNCE_CFG Control Bits */
#define SX1508_DBOUNCE_CFG_1MS	(1 << 0)
#define SX1508_DBOUNCE_CFG_2MS	(2 << 0)
#define SX1508_DBOUNCE_CFG_4MS	(3 << 0)
#define SX1508_DBOUNCE_CFG_8MS	(4 << 0)
#define SX1508_DBOUNCE_CFG_16MS	(5 << 0)
#define SX1508_DBOUNCE_CFG_32MS	(6 << 0)
#define SX1508_DBOUNCE_CFG_MSK	(7 << 0)

/* Default intensity for blue and red LEDs */
#define SX1508_RED_INTENSITY	0x01
#define SX1508_BLUE_INTENSITY	0xDC

/* SX1508 Per-Port Status Registers */
#define SX1508_REG_EVT		0x0D	/* 1=event occured (write 1 to clear) */

static uint8_t clk_enabled = 0;

int sx1508_led_init(void);
int sx1508_led_enable(int mask, int enable);
int sx1508_button_init(void);
int sx1508_button_status(int *events, int *current);

#endif	/* SX1508_H */
