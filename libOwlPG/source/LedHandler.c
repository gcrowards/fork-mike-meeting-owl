/* Copyright 2016 Owl Labs, Inc.

   Encapsulate LED interface
*/

#include <stdio.h>

#include "LedHandler.h"
#include "sx1508.h"
#include "lp5562.h"

static int enableRedBlue(int colorMask, int enable)
{
	int mask = 0;
	int ret = 0;

	if ((colorMask & LED_HDL_BLUE) != 0) {
		mask |= SX1508_LED_BLUE;
	}
	if ((colorMask & LED_HDL_RED) != 0) {
		mask |= SX1508_LED_RED;
	}

	ret = sx1508_led_enable(mask, enable);

	return ret;
}

int ledEnable(int colorMask)
{
	int ret = 0;

	if ((colorMask & LED_HDL_WHITE) != 0) {
		ret = lp5562_led_enable(1);
	}
	if (((colorMask & LED_HDL_BLUE) != 0) ||
			((colorMask & LED_HDL_RED) != 0)) {
		ret = enableRedBlue(colorMask, 1);
	}

	return ret;
}

int ledDisable(int colorMask)
{
	int ret = 0;

	if ((colorMask & LED_HDL_WHITE) != 0) {
		ret = lp5562_led_enable(0);
	}
	if (((colorMask & LED_HDL_BLUE) != 0) ||
			((colorMask & LED_HDL_RED) != 0)) {
		ret = enableRedBlue(colorMask, 0);
	}

	return ret;
}

int initLeds()
{
	int ret, fd;
	int mask;

	/* Initialize SX1508 - controls 'skirt' LEDs */
	ret = sx1508_led_init();
	if (ret < 0) {
		fprintf(stderr, "initLeds: Error initializing SX1508 device\n");
		return ret;
	}

	/* Initialize LP5562 - controls 'eye' LEDs */
	ret = lp5562_init();
	if (ret < 0) {
		fprintf(stderr, "initLeds: Error initializing LP5562 device\n");
		return ret;
	}

	return 0;
}

