/* Copyright Playground Global 2016 */

#ifndef DIRECTORHANDLER_H
#define DIRECTORHANDLER_H

#include "DirectorCommands.h"

#ifdef __cplusplus
extern "C" {
#endif

#define BEAMS 32

typedef struct {
    int32_t correlation[BEAMS]; // Normalized power
    int32_t energy;             // pcm energy volume
    int32_t directionality;     // How pure is the direction metric (can be used to detect reverb/echos)
    int32_t angle;              // which beam best matches phase
} beam_info;

/* Input to the director, consumes JPEG to act upon the data
   at the rate that director would like the data.  This callback
   should not block as it's running within the main AV chain */
void DirectorJPEGFrameCallback(uint8_t *buf, uint32_t len, int64_t ptsUsec, uint32_t flags);

/* Input to the director, consumes PCM data to run the localizer
   This callback REALLY should not block as it's running at the highest
   real-time priority thread servicing SPI reads */
void DirectorPCMCallback(int16_t *buf, int16_t *ref, uint32_t count);

/* Function that returns director command struct to command
   Graphics Handler */
director_command getDirectorCommands();

/* Set up a callback for updating owl-core */
typedef void (*owlUpdateFunc)(int16_t *pPcmBuf, int bufLen, int nMics);
int registerDirectorUpdateCallback(owlUpdateFunc);

int initDirector();

int startDirector();

int stopDirector();

#ifdef __cplusplus
}
#endif

#endif
