#include <stdio.h>
#include <ui/GraphicBuffer.h>
#include <utils/Timers.h>

#include "GpuSupport.h"

using namespace android;
sp<GraphicBuffer> gGrBuf;


extern "C"
{
  /*****************************************************/
  /* pass-through functions not supported by ndk-build */
  /*****************************************************/

  void setAndroidPresentationTime(EGLDisplay display, EGLSurface surface)
  {
    eglPresentationTimeANDROID(display, surface, systemTime(CLOCK_MONOTONIC));
  }

  void *grBufGetNativeBuffer(int width, int height)
  {
    void *nativeBuf;

    gGrBuf = new GraphicBuffer(width, height,
			       PIXEL_FORMAT_RGBA_8888,
			       GraphicBuffer::USAGE_SW_READ_OFTEN
			       | GraphicBuffer::USAGE_HW_TEXTURE);
    status_t err = gGrBuf->initCheck();
    if (err != 0 || gGrBuf->handle == 0)
      {
	printf("ERROR:   unable to allocate graphic buffer\n");
	nativeBuf = NULL;
      }
    else
      {
	nativeBuf = gGrBuf->getNativeBuffer();
	//printf("allocated %dx%d graphic buffer\n", width, height);
      }
    return nativeBuf;
  }

  void  grBufCopyPixels(unsigned char *dst)
  {
    int    w, h;
    int    srcStride, dstStride;
    long   i, j, row;
    void   *src;

    w = gGrBuf->getWidth();
    h = gGrBuf->getHeight();
    srcStride = gGrBuf->getStride() * 4; // gGrBuf->getStride() is >= w
    dstStride = w * 4;

    gGrBuf->lock(GraphicBuffer::USAGE_SW_READ_OFTEN, &src);
    i = j = 0;
    for (row=0; row<h; row++)
      {
	memcpy(dst+i, (unsigned char *)(src)+j, srcStride);
	i += dstStride;
	j += srcStride;
      }
    gGrBuf->unlock();
  }


  // Global smart pointer to safely hold reference to fisheye buffer that's
  //   accessible from the CPU
  sp<GraphicBuffer>  g_pFisheyeGrBuf;

  /* Create a new fisheye buffer and lock it for CPU access.
   *
   * @param pInFrame points to image passed in frame available callback.
   * @param pOutFrame points to the locked fisheye buffer for CPU access.
   * @param pWidth points to location to store width of the buffer.
   * @param pHeight points to location to store height of the buffer.
   * @param pStride points to location to store stride of the buffer.
   *
   * @return true if returned buffer is valid, false otherwise.
   */
  bool lockGrBufForFrame(void *pInFrame, void **pOutFrame, int *pWidth,
                         int *pHeight, int *pStride)
  {
    ANativeWindowBuffer *anb = (ANativeWindowBuffer *)pInFrame;

    if (g_pFisheyeGrBuf != NULL)
      {
        g_pFisheyeGrBuf->unlock();
        g_pFisheyeGrBuf.clear();
      }
    g_pFisheyeGrBuf = new GraphicBuffer(anb, false);
    void *pMem;
    if (g_pFisheyeGrBuf != NULL)
      {
        if (NO_ERROR == g_pFisheyeGrBuf->lock(GRALLOC_USAGE_SW_READ_OFTEN, &pMem))
          {
            *pOutFrame = pMem;
            *pWidth = g_pFisheyeGrBuf->width;
            *pHeight = g_pFisheyeGrBuf->height;
            *pStride = g_pFisheyeGrBuf->stride;
            return true;
          }
      }
    return false;
  }

  /** Release a CPU accessible fisheye buffer after use
   *
   */
  void unlockGrBufForFrame()
  {
    if (g_pFisheyeGrBuf != NULL)
      {
        g_pFisheyeGrBuf->unlock();
        g_pFisheyeGrBuf.clear();
      }
  }

} // extern "C"
