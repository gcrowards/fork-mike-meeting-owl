//
/// AudioProcessing.c - Contains audio processing support routines
//

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <signal.h>
#include <string.h>
#include <pthread.h>
#include <fcntl.h>
#include <math.h>

#include "AudioProcessing.h"

/** Calculates the RMS value of a channel of audio
*
* @param pBuff - pointer to audio
* @param len - number of frames in audio buffer
* @param channel - channel to perform calculation on.
* @param stride - number of channels of audio
*
* @return rms value
*/
int CalcRMS(short *pBuff, int len, int channel, int stride)
{
  long long accum = 0;
  int idx = channel;
  int rms_val;
  int i;


  for(i = 0; i < len; ++i)
  {
    accum += pBuff[idx] * pBuff[idx];
    idx += stride;
  }
  accum = accum / len;

  rms_val = sqrt(accum);

  return rms_val;


}

/** Calculates a magnitude db value
* @param value - fixed point value 1.15
*
* @return decibel
*/
double mag2db_i(int value)
{
  double input;
  double output;

  if(value <= 0)
  { return -96.0; }

  input = value;
  input = input / DIVISOR_1_15;

  output = 20.0 * log10(input);
  return output;
}

/** Calculates a power db value
* @param value - fixed point value 1.15
*
* @return decibel
*/
double pow2db_i(int value)
{
  double input;
  double output;

  if(value <= 0)
  { return -96.0; }

  input = value;
  input = input / DIVISOR_1_15;

  output = 10.0 * log10(input);
  return output;
}

/** Calculates the mag from a db value
*
* @param - db value
*
* @return - magnitude in 1.15
*/
int db2mag(double dbval)
{
  double dVal;

  dVal = pow(10, dbval / 20.0);

  dVal = dVal * (double) MAX_16BIT;

  return (int)dVal;

}

/** Calculates the mag from a db value
*
* @param - db value
*
* @return - magnitude in 1.15
*/
int db2pow(double dbval)
{
  double dVal;

  dVal = pow(10, dbval / 10.0);

  dVal = dVal * (double) MAX_16BIT;

  return (int)dVal;

}


