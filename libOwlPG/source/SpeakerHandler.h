/* Copyright Playground Global 2016 */

#ifndef SPEAKERHANDLER_H
#define SPEAKERHANDLER_H

#ifdef __cplusplus
extern "C" {
#endif

typedef void  (*owlSpeakerActiveCB)(int chan0Val, int chan1Val);
typedef float (*owlSpeakerGainCB)(void);

extern int earlyAmplifierInit(void);

extern int initSpeaker(owlSpeakerActiveCB, owlSpeakerGainCB);

extern int startSpeaker();

extern int stopSpeaker();

extern double GetSpeakerRMSLevel();

#ifdef __cplusplus
}
#endif

#endif
