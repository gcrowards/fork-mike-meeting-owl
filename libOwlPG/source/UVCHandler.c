// Copyright (c) 2015-2016 Playground Global LLC. All rights reserved.
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>

#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <semaphore.h>

#include <linux/usb/ch9.h>
#include <linux/usb/video.h>
#include <linux/videodev2.h>
#include <stdbool.h>

#include "UVCHandler.h"
#include "SpeakerHandler.h"
#include "LedHandler.h"
#include "LogDebug.h"

#include "../../../kernel/drivers/usb/gadget/uvc.h"

#define MIN(A,B)    ({ __typeof__(A) __a = (A); __typeof__(B) __b = (B); __a < __b ? __a : __b; })
#define MAX(A,B)    ({ __typeof__(A) __a = (A); __typeof__(B) __b = (B); __a < __b ? __b : __a; })

#define CLAMP(x, low, high) ({\
  __typeof__(x) __x = (x); \
  __typeof__(low) __low = (low);\
  __typeof__(high) __high = (high);\
  __x > __high ? __high : (__x < __low ? __low : __x);\
  })

#define SIZE_OF_ARRAY(a)	((sizeof(a) / sizeof(a[0])))

#define FRAME_BUF_SIZE (1536*1024)  // Larger buffer for higher MJPEG quality
#define JPEG_SIZE_LIMIT (FRAME_BUF_SIZE / 4)
#define NUM_OF_AUDIO_INTERFACES (3) //control, out (speaker), in(mic)
#define MIN_VALID_FRAME 512 // Set generously low minimum valid frame size

//#define UVC_DEBUG

struct uvcg_device {
	int fd;
	fd_set fdset;

	unsigned int color_fmt;
	unsigned int width;
	unsigned int height;

	struct uvc_1p0_streaming_control probe_ctrl;
	struct uvc_1p0_streaming_control commit_ctrl;
	int control;

	unsigned char **frame_mem;
	unsigned int num_of_bufs;
	unsigned int bufsize;

	uint8_t color;
	unsigned int frame_size;
	unsigned int frame_count;
	unsigned int current_frame;
	unsigned char *imgdata;
	unsigned int stream_on;
	unsigned int first_frame;
	unsigned int first_iframe;

	unsigned char sps_pps_cache[100];
	unsigned int sps_pps_cache_len;

};

int gUvcHandlerStreaming = 0;
int gOwlCoreMeetingInProgress = 0;
static int sgUvcStreamingChanged = 0;
static int sgUsbConnected = 0;
static int sgUsbConnectedChanged = 0;
static int sgUsbStateFixup = 0;
static int gOwlCoreMeetingInProgressChanged = 0;
static struct uvcg_device *uvcg_dev;
static int terminate_signal = 0;

TIMELOG_INIT(uvc);

#define MAX_QUEUE_ENTRY    5

#define ENABLE_JITTER_COMPENSATION

#ifdef ENABLE_JITTER_COMPENSATION
#define MAX_JITTER_COMPENSATION_US 15000
#define NOMINAL_FRAME_US 33333
static struct timeval jitter_ctv, jitter_ptv;
static long long jitter_usec;
static long long jitter_usec_comp;
TIMELOG_INIT(uvc_jc);
#endif

// Timing Debug
//#define TIMING_DEBUG

#ifdef TIMING_DEBUG
static unsigned long long usec;
static unsigned long long usec_max = 0;
static unsigned long long usec_min = 999999999999L;
static unsigned long long usec_sum = 0;
static unsigned long time_counter = 0;
static static struct timeval ctv, ptv;
#endif

enum buf_data_type {
	BUF_EMPTY,
	BUF_FILLED
};
static int uvc_pp_read = 0;
static int uvc_pp_write = 1;
static uint8_t uvc_pp_buffer[2][JPEG_SIZE_LIMIT];
static int uvc_pp_buffer_size[2] = {0, 0};
static pthread_mutex_t uvc_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t uvc_cond = PTHREAD_COND_INITIALIZER;
static enum buf_data_type uvc_pp_status[2] = {BUF_EMPTY, BUF_EMPTY};

static void stream_off (void) {
	if (uvcg_dev) {
		uvcg_dev->stream_on=0;
		uvcg_dev->first_frame=0;
		uvcg_dev->first_iframe=0;
	}
	uvc_pp_read = 0;
	uvc_pp_write = 1;
	uvc_pp_status[0] = BUF_EMPTY;
	uvc_pp_status[1] = BUF_EMPTY;
}

static void fill_buffer(struct uvcg_device *dev, struct v4l2_buffer *buf) {
	unsigned int bytes_per_line;
	unsigned int i;
	static unsigned int count=0;

	switch (dev->color_fmt) {
	case V4L2_PIX_FMT_MJPEG:
		/* Fill the JPEG buffer with received video data*/
		if (dev->first_frame) {
#ifdef ENABLE_JITTER_COMPENSATION
			TIMELOG_LOG(uvc_jc);
			jitter_ptv = jitter_ctv;
			gettimeofday(&jitter_ctv, NULL);
			jitter_usec = jitter_ctv.tv_usec - jitter_ptv.tv_usec +
				(jitter_ctv.tv_sec - jitter_ptv.tv_sec) * 1000000;
			jitter_usec_comp = NOMINAL_FRAME_US - jitter_usec;
			if (jitter_usec_comp < 0) {
				jitter_usec_comp = (jitter_usec_comp <
						(MAX_JITTER_COMPENSATION_US * -1)) ?
						(MAX_JITTER_COMPENSATION_US * -1) : jitter_usec_comp;
			} else {
				jitter_usec_comp = (jitter_usec_comp >
						MAX_JITTER_COMPENSATION_US) ?
						MAX_JITTER_COMPENSATION_US : jitter_usec_comp;
			}
			usleep(jitter_usec_comp + MAX_JITTER_COMPENSATION_US);
#endif

			pthread_mutex_lock(&uvc_mutex);
			while (uvc_pp_status[uvc_pp_read] != BUF_FILLED)
				pthread_cond_wait(&uvc_cond, &uvc_mutex);
                        buf->bytesused = uvc_pp_buffer_size[uvc_pp_read];
			/* Only pass valid frames down to UVC */
			if (buf->bytesused > MIN_VALID_FRAME)
				memcpy(dev->frame_mem[buf->index],
						uvc_pp_buffer[uvc_pp_read],
								buf->bytesused);
			uvc_pp_status[uvc_pp_read] = BUF_EMPTY;
			uvc_pp_read = (uvc_pp_read == 0) ? 1 : 0;
			pthread_mutex_unlock(&uvc_mutex);
			TIMELOG_LOG(uvc);
#ifdef TIMING_DEBUG
			ptv = ctv;
			gettimeofday(&ctv, NULL);
			usec = ctv.tv_usec - ptv.tv_usec + (ctv.tv_sec - ptv.tv_sec) * 1000000;
			if (usec_max < usec)
				usec_max = usec;
			if (usec_min > usec)
				usec_min = usec;
			usec_sum += usec;
			if (time_counter % 150 == 0){
				printf("Avg: %llu Max: %llu Min: %llu\n", usec_sum / 150, usec_max, usec_min);
				usec_max = 0;
				usec_min = 999999999999L;
				usec_sum = 0;
			}
			time_counter++;
#endif
		}
		else {
			buf->bytesused = 0;
		}
		break;
	}
	count++;
}

static struct uvcg_device *uvcg_open(const char *devname) {
	struct uvcg_device *dev;
	struct v4l2_capability cap;
	int ret;
	int fd;

	fd = open(devname, O_RDWR | O_NONBLOCK);
	if (fd == -1) {
		fprintf(stderr,"v4l2 open failed: %s (%d)\n", strerror(errno), errno);
		return NULL;
	}

	fprintf(stderr,"v4l2 opened, file descriptor = %d\n", fd);

	ret = ioctl(fd, VIDIOC_QUERYCAP, &cap);
	if (ret < 0) {
		fprintf(stderr,"VIDIOC_QUERYCAP failed: %s (%d)\n", strerror(errno), errno);
		close(fd);
		return NULL;
	}

	dev = malloc(sizeof *dev);
	if (dev == NULL) {
		close(fd);
		return NULL;
	}

	memset(dev, 0, sizeof *dev);

	dev->fd = fd;

	return dev;
}

static void uvcg_close(struct uvcg_device *dev) {
#ifdef UVC_DEBUG
        fprintf(stderr,"closing\n");
#endif
	close(dev->fd);
	free(dev->frame_mem);
	free(dev);
}

static int request_bufs(struct uvcg_device *dev, int num_of_bufs) {
	struct v4l2_requestbuffers rb;
	struct v4l2_buffer buf;
	unsigned int i;
	int ret=0;

	for (i = 0; i < dev->num_of_bufs; ++i)
		munmap(dev->frame_mem[i], dev->bufsize);

	free(dev->frame_mem);
	dev->frame_mem = 0;
	dev->num_of_bufs = 0;

	memset(&rb, 0, sizeof rb);
	rb.count = num_of_bufs;
	rb.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
	rb.memory = V4L2_MEMORY_MMAP;

	if (num_of_bufs > 0)
		ret = ioctl(dev->fd, VIDIOC_REQBUFS, &rb);
	if (ret < 0) {
		fprintf(stderr,"error allocate buffers: %s (%d)\n", strerror(errno),
				errno);
		return ret;
	}

	fprintf(stderr,"%u buffers allocated\n", rb.count);

	dev->frame_mem = malloc(rb.count * sizeof dev->frame_mem[0]);

	for (i = 0; i < rb.count; ++i) {
		memset(&buf, 0, sizeof buf);
		buf.index = i;
		buf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
		buf.memory = V4L2_MEMORY_MMAP;
		ret = ioctl(dev->fd, VIDIOC_QUERYBUF, &buf);
		if (ret < 0) {
			fprintf(stderr,"failed query %u: %s (%d)\n", i, strerror(errno),
					errno);
			return -1;
		}
		fprintf(stderr,"len: %u offset: %u\n", buf.length, buf.m.offset);

		dev->frame_mem[i] = mmap(0, buf.length, PROT_READ | PROT_WRITE, MAP_SHARED,
				dev->fd, buf.m.offset);
		if (dev->frame_mem[i] == MAP_FAILED) {
			fprintf(stderr,"error map buffer %u: %s (%d)\n", i, strerror(errno),
					errno);
			return -1;
		}
		fprintf(stderr,"buffer %u at addr %p\n", i, dev->frame_mem[i]);
	}

	dev->bufsize = buf.length;
	dev->num_of_bufs = rb.count;

	return 0;
}

static int video_process(struct uvcg_device *dev) {
	struct v4l2_buffer buf;
	int ret;

	memset(&buf, 0, sizeof buf);
	buf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
	buf.memory = V4L2_MEMORY_MMAP;

	if ((ret = ioctl(dev->fd, VIDIOC_DQBUF, &buf)) < 0) {
		if (errno != 22 && errno != 19 && errno != 1) {
			fprintf(stderr,"error dequeue buffer: %s (%d)\n",
							strerror(errno), errno);
		}
		return ret;
	}

	dev->stream_on=1;
	fill_buffer(dev, &buf);
	if ((ret = ioctl(dev->fd, VIDIOC_QBUF, &buf)) < 0) {
		if (errno != 1) {
			fprintf(stderr,"error requeue buffer: %s (%d)\n",
							strerror(errno), errno);
		}
		return ret;
	}

	return 0;
}

static int video_stream(struct uvcg_device *dev, int enable) {
	struct v4l2_buffer buf;
	unsigned int i;
	int type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
	int ret;

	if (!enable) {
		fprintf(stderr,"stopping stream\n");
		ioctl(dev->fd, VIDIOC_STREAMOFF, &type);
		return 0;
	}

	fprintf(stderr,"starting stream\n");

	for (i = 0; i < dev->num_of_bufs; ++i) {
		memset(&buf, 0, sizeof buf);

		buf.index = i;
		buf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
		buf.memory = V4L2_MEMORY_MMAP;

		fill_buffer(dev, &buf);

		fprintf(stderr,"queueing %u\n", i);
		if ((ret = ioctl(dev->fd, VIDIOC_QBUF, &buf)) < 0) {
			fprintf(stderr,"error queue buffer: %s (%d)\n", strerror(errno),
					errno);
			break;
		}
	}

	ioctl(dev->fd, VIDIOC_STREAMON, &type);
	return ret;
}

static int video_set_format(struct uvcg_device *dev) {
	struct v4l2_format fmt;
	int ret;

	fprintf(stderr,"setting fmt to 0x%08x %ux%u\n", dev->color_fmt, dev->width,
			dev->height);

	memset(&fmt, 0, sizeof fmt);
	fmt.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
	fmt.fmt.pix.field = V4L2_FIELD_NONE;
	fmt.fmt.pix.width = dev->width;
	fmt.fmt.pix.height = dev->height;
	fmt.fmt.pix.pixelformat = dev->color_fmt;
	if (dev->color_fmt == V4L2_PIX_FMT_MJPEG)
		fmt.fmt.pix.sizeimage = FRAME_BUF_SIZE / 4;
	if (dev->color_fmt == V4L2_PIX_FMT_H264)
		fmt.fmt.pix.sizeimage = FRAME_BUF_SIZE / 4;

	if ((ret = ioctl(dev->fd, VIDIOC_S_FMT, &fmt)) < 0)
		fprintf(stderr,"error set fmt: %s (%d)\n", strerror(errno), errno);

	return ret;
}

struct uvc_frame_info {
	unsigned int width;
	unsigned int height;
	unsigned int intervals[2];
};

struct uvc_format_info {
	unsigned int color_fmt;
	const struct uvc_frame_info *frames;
};

static const struct uvc_frame_info uvc_frames_yuyv[] = { { 640, 360,
		{ 333333, 0 }, }, { 0, 0, { 0, }, }, };

static const struct uvc_frame_info uvc_frames_mjpeg[] = {
		{ 1280, 720, { 333333, 0 }, }, { 0, 0, { 0, }, }, };

#define USE_MJPEG
static const struct uvc_format_info uvc_formats[] = {
#ifdef USE_MJPEG
		{ V4L2_PIX_FMT_MJPEG, uvc_frames_mjpeg },
#endif
		};

static void events_process_standard(struct uvcg_device *dev,
		struct usb_ctrlrequest *ctrl, struct uvc_request_data *resp) {
	fprintf(stderr,"standard request\n");
	(void) dev;
	(void) ctrl;
	resp->length = 0;
}

static void events_process_data(struct uvcg_device *dev,
		struct uvc_request_data *data) {
	struct uvc_1p0_streaming_control *target;
	struct uvc_1p0_streaming_control *ctrl;
	const struct uvc_format_info *format;
	const struct uvc_frame_info *frame;
	const unsigned int *interval;
	unsigned int iformat, iframe;
	unsigned int nframes;
	void* temp;

	switch (dev->control) {
	case UVC_VS_PROBE_CONTROL:
		fprintf(stderr,"setting probe control, length = %d\n", data->length);
		target = &dev->probe_ctrl;
		stream_off();
		break;

	case UVC_VS_COMMIT_CONTROL:
		fprintf(stderr,"setting commit control, length = %d\n", data->length);
		target = &dev->commit_ctrl;
		break;

	default:
		fprintf(stderr,"setting unknown control, length = %d\n", data->length);
		return;
	}

	temp = (void*)&data->data;
	ctrl = (struct uvc_1p0_streaming_control *)temp;
	iformat = CLAMP((unsigned int )ctrl->bFormatIndex, 1U,
				(unsigned int)SIZE_OF_ARRAY(uvc_formats));
	format = &uvc_formats[iformat - 1];

	nframes = 0;
	while (format->frames[nframes].width != 0) {
		++nframes;
	}
	iframe = CLAMP((unsigned int )ctrl->bFrameIndex, 1U, nframes);
	frame = &format->frames[iframe - 1];
	interval = frame->intervals;
	while (interval[0] < ctrl->dwFrameInterval && interval[1]) {
		++interval;
	}
	target->bFormatIndex = iformat;
	target->bFrameIndex = iframe;
	switch (format->color_fmt) {
	case V4L2_PIX_FMT_YUYV:
		target->dwMaxVideoFrameSize = frame->width * frame->height * 2;
		break;
	case V4L2_PIX_FMT_MJPEG:
		target->dwMaxVideoFrameSize = FRAME_BUF_SIZE / 4;
		break;
	case V4L2_PIX_FMT_H264:
		target->dwMaxVideoFrameSize = FRAME_BUF_SIZE / 4;
		break;
	}
	target->dwFrameInterval = *interval;

	if (dev->control == UVC_VS_COMMIT_CONTROL) {
		dev->color_fmt = format->color_fmt;
		dev->width = frame->width;
		dev->height = frame->height;
		video_set_format(dev);
	}
}

static void events_process_streaming(struct uvcg_device *dev, uint8_t req,
		uint8_t cs, struct uvc_request_data *resp) {
	struct uvc_1p0_streaming_control *ctrl;
	void* temp;

	fprintf(stderr,"streaming request (req %02x cs %02x)\n", req, cs);

	if (cs != UVC_VS_PROBE_CONTROL && cs != UVC_VS_COMMIT_CONTROL)
		return;

	temp=(void*)&resp->data;
	ctrl = (struct uvc_1p0_streaming_control *) temp;
	resp->length = sizeof *ctrl;

	switch (req) {
	case UVC_SET_CUR:
		dev->control = cs;
		resp->length = 34;
		events_process_data(dev,resp);
		break;

	case UVC_GET_CUR:
		if (cs == UVC_VS_PROBE_CONTROL)
			memcpy(ctrl, &dev->probe_ctrl, sizeof *ctrl);
		else
			memcpy(ctrl, &dev->commit_ctrl, sizeof *ctrl);
		break;

	case UVC_GET_DEF:
	case UVC_GET_MIN:
	case UVC_GET_MAX:
		break;

	case UVC_GET_RES:
		memset(ctrl, 0, sizeof *ctrl);
		break;

	case UVC_GET_LEN:
		resp->data[0] = 0x00;
		resp->data[1] = 0x22;
		resp->length = 2;
		break;

	case UVC_GET_INFO:
		resp->data[0] = 0x03;
		resp->length = 1;
		break;
	}
}

static void fill_streaming_control(struct uvcg_device *dev,
		struct uvc_1p0_streaming_control *ctrl, int iframe, int iformat) {
	const struct uvc_format_info *format;
	const struct uvc_frame_info *frame;
	unsigned int nframes;
	(void)dev;

	if (iformat < 0)
		iformat = SIZE_OF_ARRAY(uvc_formats) + iformat;
	if (iformat < 0 || iformat >= (int) SIZE_OF_ARRAY(uvc_formats))
		return;
	format = &uvc_formats[iformat];

	nframes = 0;
	while (format->frames[nframes].width != 0)
		++nframes;

	if (iframe < 0)
		iframe = nframes + iframe;
	if (iframe < 0 || iframe >= (int) nframes)
		return;
	frame = &format->frames[iframe];

	memset(ctrl, 0, sizeof *ctrl);

	ctrl->bFormatIndex = iformat + 1;
	ctrl->bFrameIndex = iframe + 1;
	ctrl->dwFrameInterval = frame->intervals[0];
	ctrl->bmHint = 1;
	switch (format->color_fmt) {
	case V4L2_PIX_FMT_YUYV:
		ctrl->dwMaxVideoFrameSize = frame->width * frame->height * 2;
		break;
	case V4L2_PIX_FMT_MJPEG:
		ctrl->dwMaxVideoFrameSize = FRAME_BUF_SIZE / 4;
		break;
	case V4L2_PIX_FMT_H264:
		ctrl->dwMaxVideoFrameSize = FRAME_BUF_SIZE / 4;
		break;
	}
	ctrl->dwMaxPayloadTransferSize = 512; //this needs to come from driver
}

static void events_process_control(struct uvcg_device *dev, uint8_t req,
		uint8_t cs, struct uvc_request_data *resp, uint8_t length) {
	fprintf(stderr,"control request (req %02x cs %02x)\n", req, cs);
	(void) dev;
	resp->length = length;
	switch (req) {
	case UVC_GET_CUR:
	case UVC_GET_RES:
	case UVC_GET_INFO:
	case UVC_GET_MIN:
	case UVC_GET_MAX:
	case UVC_GET_DEF:
		events_process_streaming(dev, req, cs, resp);
	default:
		break;
	}
}

static void events_process_class(struct uvcg_device *dev,
		struct usb_ctrlrequest *ctrl, struct uvc_request_data *resp) {
	unsigned int i = (ctrl->wIndex & 0xff);

	i = i % NUM_OF_AUDIO_INTERFACES; //hack: when audio interface is present, adjust interface number dynamically 0=control 1=streaming

	fprintf(stderr,"ctrl->bRequestType=%x,ctrl->wIndex=%x, i=%x\n", ctrl->bRequestType,ctrl->wIndex, i);
	if ((ctrl->bRequestType & USB_RECIP_MASK) != USB_RECIP_INTERFACE)
		return;

	switch (i) {
	case UVC_INTF_CONTROL:
		events_process_control(dev, ctrl->bRequest, ctrl->wValue >> 8, resp,
				ctrl->wLength);
		break;

	case UVC_INTF_STREAMING:
		events_process_streaming(dev, ctrl->bRequest, ctrl->wValue >> 8,
				resp);
		break;

	default:
		break;
	}
}

static void events_process_setup(struct uvcg_device *dev,
		struct usb_ctrlrequest *ctrl, struct uvc_request_data *resp) {
	dev->control = 0;

	fprintf(stderr,"bRequestType %02x bRequest %02x wValue %04x wIndex %04x "
			"wLength %04x\n", ctrl->bRequestType, ctrl->bRequest, ctrl->wValue,
			ctrl->wIndex, ctrl->wLength);

	switch (ctrl->bRequestType & USB_TYPE_MASK) {
	case USB_TYPE_STANDARD:
		events_process_standard(dev, ctrl, resp);
		break;

	case USB_TYPE_CLASS:
		events_process_class(dev, ctrl, resp);
		break;

	default:
		break;
	}
}

static void events_process(struct uvcg_device *dev) {
	struct v4l2_event v4l2_event;
	struct uvc_event *uvc_event = (void *) &v4l2_event.u.data;
	struct uvc_request_data resp;
	int ret, mask;

	ret = ioctl(dev->fd, VIDIOC_DQEVENT, &v4l2_event);
	if (ret < 0) {
		fprintf(stderr,"VIDIOC_DQEVENT failed: %s (%d)\n", strerror(errno), errno);
		return;
	}

	memset(&resp, 0, sizeof resp);
	resp.length = -EL2HLT;

	switch (v4l2_event.type) {
	case UVC_EVENT_CONNECT:
#ifdef UVC_DEBUG
		fprintf(stderr,"###UVC_EVENT_CONNECT\n");
#endif
		if (sgUsbConnected == 0)
			sgUsbConnectedChanged = 1;
		sgUsbConnected = 1;
		return;

	case UVC_EVENT_DISCONNECT:
#ifdef UVC_DEBUG
		fprintf(stderr,"###UVC_EVENT_DISCONNECT\n");
#endif
		if (sgUsbConnected == 1)
			sgUsbConnectedChanged = 1;
		sgUsbConnected = 0;
		return;

	case UVC_EVENT_SETUP:
		events_process_setup(dev, &uvc_event->req, &resp);
		break;

	case UVC_EVENT_DATA:
		events_process_data(dev, &uvc_event->data);
		return;

	case UVC_EVENT_STREAMON:
#ifdef UVC_DEBUG
		fprintf(stderr,"###UVC_EVENT_STREAMON\n");
#endif
		/* If we receive a stream on request, but the UsbConnected
		 * flag is still 0, then we have missed the UVC Connect event
		 * and need to fix this here.
		 */
		if (sgUsbConnected == 0) {
			sgUsbConnectedChanged = 1;
			sgUsbConnected = 1;
			sgUsbStateFixup += 1;
		}
		uvc_pp_read = 0;
		uvc_pp_write = 1;
		uvc_pp_status[0] = BUF_EMPTY;
		uvc_pp_status[1] = BUF_EMPTY;
		request_bufs(dev, 4);
		video_stream(dev, 1);
		if (gUvcHandlerStreaming == 0)
			sgUvcStreamingChanged = 1;
		gUvcHandlerStreaming = 1;
		return;

	case UVC_EVENT_STREAMOFF:
		if (gUvcHandlerStreaming == 1)
			sgUvcStreamingChanged = 1;
		gUvcHandlerStreaming = 0;
#ifdef UVC_DEBUG
		fprintf(stderr,"###UVC_EVENT_STREAMOFF\n");
#endif
		video_stream(dev, 0);
		request_bufs(dev, 0);
		stream_off();
		return;
	}

	ret = ioctl(dev->fd, UVCIOC_SEND_RESPONSE, &resp);
	if (ret < 0) {
		fprintf(stderr,"UVCIOC_S_EVENT failed: %s (%d)\n", strerror(errno), errno);
		return;
	}
}

static void events_init(struct uvcg_device *dev) {
	struct v4l2_event_subscription sub;

	fill_streaming_control(dev, &dev->probe_ctrl, 0, 0);
	fill_streaming_control(dev, &dev->commit_ctrl, 0, 0);

	memset(&sub, 0, sizeof sub);
	sub.type = UVC_EVENT_CONNECT;
	ioctl(dev->fd, VIDIOC_SUBSCRIBE_EVENT, &sub);
	sub.type = UVC_EVENT_DISCONNECT;
	ioctl(dev->fd, VIDIOC_SUBSCRIBE_EVENT, &sub);
	sub.type = UVC_EVENT_SETUP;
	ioctl(dev->fd, VIDIOC_SUBSCRIBE_EVENT, &sub);
	sub.type = UVC_EVENT_DATA;
	ioctl(dev->fd, VIDIOC_SUBSCRIBE_EVENT, &sub);
	sub.type = UVC_EVENT_STREAMON;
	ioctl(dev->fd, VIDIOC_SUBSCRIBE_EVENT, &sub);
	sub.type = UVC_EVENT_STREAMOFF;
	ioctl(dev->fd, VIDIOC_SUBSCRIBE_EVENT, &sub);
}

static void terminate_function(int sig)
{
	(void)sig;
#ifdef UVC_DEBUG
	fprintf(stderr,"got termination signal\n");
#endif
	terminate_signal = 1;
}

static int counter = 0;
void UVCInputFrameCallback(uint8_t *buf, uint32_t len, int64_t ptsUsec, uint32_t flags) {
	(void) ptsUsec;
	struct uvcg_device *dev = uvcg_dev;
	char filename[64];

#ifdef UVC_DEBUG
	if ((counter % 30) == 0) {	/* Prints at approx. 1sec intervals */
		fprintf(stderr,"###[UVC %d] UVCInputFrameCallback %d\n",
								counter, len);
	}
	counter++;
#endif

#ifdef USE_MJPEG
	(void)flags;
	int buf_size;
	if (dev->stream_on==1) { //jpeg
		buf_size = len > JPEG_SIZE_LIMIT ? JPEG_SIZE_LIMIT : len;

		pthread_mutex_lock(&uvc_mutex);
		memcpy(uvc_pp_buffer[uvc_pp_write], buf, buf_size);
		uvc_pp_buffer_size[uvc_pp_write] = buf_size;
		uvc_pp_read = uvc_pp_write;
		uvc_pp_status[uvc_pp_read] = BUF_FILLED;
		uvc_pp_write = (uvc_pp_write == 0) ? 1 : 0;
		dev->first_frame = 1;
		pthread_cond_signal(&uvc_cond);
		pthread_mutex_unlock(&uvc_mutex);
	}
#endif
}

int initUVC() {
	int ret;
	int i;
	char device_name[20]; //uvc gadget video output device name
	pthread_t tcpserver_thread_handle;
	char name_file[40];
	int name_found=0;
	FILE* name_fp;

	strcpy(device_name,"/dev/video1");

	strcpy(name_file,"/sys/class/video4linux/video1/name");
	//                01234567890123456789012345678
	//find uvc gadget
	for (i='1'; i<'9'; i++) {
		char name_buf[30];
		int bytes_read;
		name_file[28]=i; //assuming single digit
		name_fp=fopen(name_file,"r");
		if (name_fp==NULL)
			break;
		bytes_read=fread(name_buf,1,sizeof(name_buf),name_fp);
		if (bytes_read>0) {
			if (strncmp(name_buf,"ci13xxx_msm",11)==0) {
				name_found=1;
				break;
			}
		}
	}
	if (name_fp) {
		fclose(name_fp);
	}

	if (name_found==0) {
		fprintf(stderr,"uvc device not found\n");
		return 1;
	}

	device_name[strlen(device_name)-1]=i;

	uvcg_dev = uvcg_open(device_name);
	if (uvcg_dev == NULL)
		return 1;

	events_init(uvcg_dev);
	FD_ZERO(&uvcg_dev->fdset);
	FD_SET(uvcg_dev->fd, &uvcg_dev->fdset);

	return 0;
}

static pthread_t uvcThread;

static void *processUVC(void *arg){
	(void) arg;
	int ret;
	do {
		fd_set except_fds = uvcg_dev->fdset;
		fd_set write_fds = uvcg_dev->fdset;
		if (gUvcHandlerStreaming) {
		    // System is streaming video, so wait for either data
		    // available for the fd to write to us, or for an
		    // exceptional condition.
		    ret = select(uvcg_dev->fd + 1, NULL, &write_fds, &except_fds, NULL);
		}
		else {
		    // System is NOT streaming right now, so wait only for
		    // exceptional conditions. (Otherwise, for some reason
		    // select returns immediately and the thread consumes
		    // a full CPU.)
                    ret = select(uvcg_dev->fd + 1, NULL, NULL, &except_fds, NULL);
		}
		if (FD_ISSET(uvcg_dev->fd, &except_fds))
			events_process(uvcg_dev);
		if (FD_ISSET(uvcg_dev->fd, &write_fds))
			video_process(uvcg_dev);
	} while (terminate_signal == 0);
	uvcg_close(uvcg_dev);
	return NULL;
}

int startUVC() {
	int result;
	result = pthread_create(&uvcThread, NULL, processUVC, NULL);
	if (result == 0) {
	    pthread_setname_np(uvcThread, "processUVC");
	}
	struct sched_param params;
	params.sched_priority = 70;
	pthread_setschedparam(uvcThread, SCHED_FIFO, &params);
	return result;
}

int stopUVC() {
	terminate_function(0);
        TIMELOG_END(uvc);
#ifdef ENABLE_JITTER_COMPENSATION
        TIMELOG_END(uvc_jc);
#endif
	return 0;
}

/* Owlcore will call this to retrieve information on the USB connection */
int getUsbConnectedState(int *pUsbConnected, int *pUsbStateChanged) {

	*pUsbConnected = sgUsbConnected;
	*pUsbStateChanged = sgUsbConnectedChanged;	

	sgUsbConnectedChanged = 0;

	return 0;
}

/* Owlcore will call this to retrieve information on the UVC connection */
int getUvcStreamState(int *pUvcStreaming, int *pUvcStateChanged) {

	*pUvcStreaming = gUvcHandlerStreaming;
	*pUvcStateChanged = sgUvcStreamingChanged;

	sgUvcStreamingChanged = 0;

	return 0;
}

/* Owlcore will call this to retrieve information on the USB connection */
int getUsbStateFixup(int *pUsbStateFixup) {

	*pUsbStateFixup = sgUsbStateFixup;

	sgUsbStateFixup = 0;

	return 0;
}

/** Returns current setting of the meeting-in-progress flag.
 *
 * This flag is controlled by OwlCore and is not changed by libOwlPG.
 * If the value has been changed
 * @param pInMeeting pointer to current in-meeting state to be filled in.
 * @param pInMeetingChanged pointer to changed state to be filled in.
 *
 * @returns 0.
 */
int  getOwlCoreMeetingInProgress(int *pInMeeting, int *pInMeetingChanged)
{
  *pInMeetingChanged = gOwlCoreMeetingInProgressChanged;
  *pInMeeting = gOwlCoreMeetingInProgress;

  gOwlCoreMeetingInProgressChanged = 0;

  return 0;
}

/** Sets the value of the meeting-in-progress flag.
 *
 * This flag is controlled by OwlCore and is not changed by libOwlPG.
 */
void setOwlCoreMeetingInProgress(int inMeeting)
{
  if (gOwlCoreMeetingInProgress != inMeeting)
    { gOwlCoreMeetingInProgressChanged = 1; }

  gOwlCoreMeetingInProgress = inMeeting;
}

