/* I2C Abstraction Library
   Copyright Owl Labs, Inc. 2016 */

#ifndef I2CHANDLER_H
#define I2CHANDLER_H

#define I2C_PORT_0	0
#define I2C_PORT_1	1
#define I2C_SLAVE       0x0703
#define I2C_SMBUS       0x0720
#define I2C_SMBUS_READ  1
#define I2C_SMBUS_WRITE 0
#define I2C_SMBUS_BLOCK_MAX	32
#define I2C_SMBUS_BYTE_DATA	2

#ifdef __cplusplus
extern "C" {
#endif

/* Function to write to an 8-bit register of an I2C device */
int i2cWriteReg8(int fd, int reg, int value);

/* Function to write to an 8-bit register of an I2C device */
int i2cReadReg8 (int fd, int reg);

/* Open the I2C device at the specified bus and slave address
 * Returns the open file descriptor
 */
int openI2C(int bus, int slaveAddr);

#ifdef __cplusplus
}
#endif

#endif	/* I2CHANDLER_H */
