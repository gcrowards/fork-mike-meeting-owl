// -- use a #define to turn JPEG use on or off
// See the comments for the INTEGRATE macro in ConfSample.cpp.
//#define USE_JPEG

#include <pthread.h>
#include <stdio.h>
#ifdef USE_JPEG
#include <jpeglib.h>
#endif

#include "qcff_native.h"
#include "DirectorCommands.h"
#include "DirectorHandler.h"

//#define QCFF_DEBUG

#define JPEG_SAMPLING_RATE 10
#define JPEG_BUFFER_COUNT 2
#define JPEG_MAX_SIZE (120*1024)

#define PCM_SAMPLING_RATE 1 // increase to slow down localizer thread
#define PCM_BUFFER_COUNT 2
#define PCM_MAX_SIZE (16*1024)

#define STRIP_WIDTH 1280
#define STRIP_HEIGHT 160

#define QCFF_MAX_FACES MAX_DETECTED_FACES
#define PI (3.141592653589793)

// Localization
#define MICS 8
#define BEAM_ENERGY_THRESHOLD 1500000
#define BEAM_ENERGY_FLOOR      250000
#define BEAM_ENERGY_DERATING        2
#define MIC_TO_PHASE_OFFSET(x) ((((float) x / (BEAMS-1)) ) * -2 * PI)

enum director_input {
    DIRECTOR_NONE,
    DIRECTOR_JPEG,
    DIRECTOR_PCM,
    DIRECTOR_DIE,
};

static pthread_t directorThread;
static int stop_director = 0;
static int director_running = 0;
static enum director_input director_process;

static int jpeg_sample_ptr = 0;
static int jpeg_sample_count = 0;
static int jpeg_buf_to_process = -1;
static uint8_t jpeg_sample_buffer[JPEG_BUFFER_COUNT][JPEG_MAX_SIZE];
static int jpeg_sample_buffer_len[JPEG_BUFFER_COUNT];

static int pcm_sample_count = 0;
static int pcm_sample_ptr = 0;
static int pcm_buf_to_process = -1;
static int16_t pcm_sample_buffer[PCM_BUFFER_COUNT][PCM_MAX_SIZE];
static int pcm_sample_buffer_len[PCM_BUFFER_COUNT];

static pthread_mutex_t dir_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t dir_cond = PTHREAD_COND_INITIALIZER;

static uint8_t grayscale_buf[STRIP_WIDTH * STRIP_HEIGHT];
static qcff_handle_t qcff_handle;
static qcff_complete_face_info_t qcff_info;
static qcff_face_rect_t qcff_faces[QCFF_MAX_FACES];
static uint32_t qcff_face_indices[QCFF_MAX_FACES];

static director_command current_command;
static director_state current_state;

static owlUpdateFunc owlUpdateCB = NULL;

static beam_info beam_info_data;

// TODO: point source / wave propagation
static const int _beams_32[] = {
    6,0,-6,0,4,4,-4,-4,		// BEAM 0
    6,1,-6,-1,5,3,-5,-3,	// BEAM 1
    6,2,-6,-2,6,2,-6,-2,	// BEAM 2
    5,3,-5,-3,6,1,-6,-1,	// BEAM 3
    4,4,-4,-4,6,0,-6,0,		// BEAM 4
    3,5,-3,-5,6,-1,-6,1,	// BEAM 5
    2,6,-2,-6,6,-2,-6,2,	// BEAM 6
    1,6,-1,-6,5,-3,-5,3,	// BEAM 7
    0,6,0,-6,4,-4,-4,4,		// BEAM 8
    -1,6,1,-6,3,-5,-3,5,	// BEAM 9
    -2,6,2,-6,2,-6,-2,6,	// BEAM 10
    -3,5,3,-5,1,-6,-1,6,	// BEAM 11
    -4,4,4,-4,0,-6,0,6,		// BEAM 12
    -5,3,5,-3,-1,-6,1,6,	// BEAM 13
    -6,2,6,-2,-2,-6,2,6,	// BEAM 14
    -6,1,6,-1,-3,-5,3,5,	// BEAM 15
    -6,0,6,0,-4,-4,4,4,		// BEAM 16
    -6,-1,6,1,-5,-3,5,3,	// BEAM 17
    -6,-2,6,2,-6,-2,6,2,	// BEAM 18
    -5,-3,5,3,-6,-1,6,1,	// BEAM 19
    -4,-4,4,4,-6,0,6,0,		// BEAM 20
    -3,-5,3,5,-6,1,6,-1,	// BEAM 21
    -2,-6,2,6,-6,2,6,-2,	// BEAM 22
    -1,-6,1,6,-5,3,5,-3,	// BEAM 23
    0,-6,0,6,-4,4,4,-4,		// BEAM 24
    1,-6,-1,6,-3,5,3,-5,	// BEAM 25
    2,-6,-2,6,-2,6,2,-6,	// BEAM 26
    3,-5,-3,5,-1,6,1,-6,	// BEAM 27
    4,-4,-4,4,0,6,0,-6,		// BEAM 28
    5,-3,-5,3,1,6,-1,-6,	// BEAM 29
    6,-2,-6,2,2,6,-2,-6,	// BEAM 30
    6,-1,-6,1,3,5,-3,-5,	// BEAM 31
};

static const int _cos8[] = {
    255,250,236,212,180,142,98,50,
    0,-50,-98,-142,-180,-212,-236,-250,
    -255,-250,-236,-212,-180,-142,-98,-50,
    0,50,98,142,180,212,236,250,
};

// really should do this with a fft, could match to any resolution (64,128,256....)
static int match_phase(beam_info* info)
{
    int32_t* s = info->correlation;
    int32_t cc[BEAMS];
    int maxcc = 0;
    int b, i;
    for (b = 0; b < BEAMS; b++)
    {
        int c = 0;
        for (i = 0; i < BEAMS; i++)
            c += s[i]*_cos8[(b+i) & 0x1F];
        cc[b] = c;
        if (cc[maxcc] < c)
        {
            maxcc = b;
            info->directionality = c;
        }
    }
    info->angle = maxcc;
    return maxcc;
}

// Run on >= ~5ms chunks for best results
// pcm is 8 channels
// returns 0..BEAMS-1 for angle
static int get_angle(int16_t* pcm, int samples, beam_info* info)
{
    const int PAD = 8;                  // skip 8 samples at start and end
    const int* beam_tab = _beams_32;
    int correlation[BEAMS];
    int beam, m;

    // TODO: check power first
    int energy = 0;
    for (beam = 0; beam < BEAMS; beam++) {
        int cc = 0;
        for (m = 0; m < MICS; m++) {
            int nextm = ((m+1)&3);
            int m0 = PAD*8 + beam_tab[m + beam*8]*8 + m;         // measure adjacent mics
            int m1 = PAD*8 + beam_tab[nextm + beam*8]*8 + nextm;
            int i = samples - PAD*2;
            while (i--)                                 // samples*mics
            {
                int s0 = pcm[m0];
                int s1 = pcm[m1];
                cc += s0*s1 >> 16;
                energy += (s0*s0 + s1*s1) >> 16;
                m0 += 8;
                m1 += 8;
            }
        }
        correlation[beam] = cc;
    }
    info->energy = energy;

    // subtract opposite beam to make it into a +-sin
    for (beam = 0; beam < BEAMS; beam++)
        info->correlation[beam] = correlation[beam]-correlation[(beam+(BEAMS>>1)) & (BEAMS-1)];

    // Do phase correlation
    return match_phase(info);
}

/* Input to the director, consumes JPEG to act upon the data
   at the rate that director would like the data.  This callback
   should not block as it's running within the main AV chain */
void DirectorJPEGFrameCallback(uint8_t *buf, uint32_t len, int64_t ptsUsec, uint32_t flags)
{
    (void) ptsUsec;
    (void) flags;

    jpeg_sample_count++;
    if ((jpeg_sample_count % JPEG_SAMPLING_RATE) != 0 || director_running)
        return;

    pthread_mutex_lock(&dir_mutex);
    len = (len > JPEG_MAX_SIZE) ? JPEG_MAX_SIZE : len;
    memcpy(jpeg_sample_buffer[jpeg_sample_ptr], buf, len);
    jpeg_sample_buffer_len[jpeg_sample_ptr] = len;
    jpeg_buf_to_process = jpeg_sample_ptr;
    jpeg_sample_ptr = (jpeg_sample_ptr) ? 0 : 1;
    director_process = DIRECTOR_JPEG;
    pthread_cond_signal(&dir_cond);
    pthread_mutex_unlock(&dir_mutex);
    return;
}

/* Input to the director, consumes PCM data to run the localizer
   This callback REALLY should not block as it's running at the highest
   real-time priority thread servicing SPI reads */
void DirectorPCMCallback(int16_t *buf, int16_t *ref, uint32_t count)
{
    (void)ref;
    pcm_sample_count++;
    if ((pcm_sample_count % PCM_SAMPLING_RATE) != 0 || director_running)
        return;

    pthread_mutex_lock(&dir_mutex);
    memcpy(pcm_sample_buffer[pcm_sample_ptr], buf, count * sizeof(int16_t));
    pcm_sample_buffer_len[pcm_sample_ptr] = count;
    pcm_buf_to_process = pcm_sample_ptr;
    pcm_sample_ptr = (pcm_sample_ptr) ? 0 : 1;
    director_process = DIRECTOR_PCM;
    pthread_cond_signal(&dir_cond);
    pthread_mutex_unlock(&dir_mutex);
    return;
}

static void *runDirector(void *arg)
{
    int buf_index = 0;
    uint8_t *jpeg_buf_ptr = NULL;
    int jpeg_buf_len = 0;
    int16_t *pcm_buf_ptr = NULL;
    int pcm_buf_len = 0;
    int lines;
    int ret;
    (void) arg;
    uint32_t i, num_faces;
    int beam_valid = 0;
    enum director_input process = DIRECTOR_NONE;

#ifdef USE_JPEG
    struct jpeg_decompress_struct cinfo = {0};
    struct jpeg_error_mgr jerr;
    cinfo.err = jpeg_std_error(&jerr);
    jpeg_create_decompress(&cinfo);
#endif

    while (!stop_director) {
        pthread_mutex_lock(&dir_mutex);
        while (director_process == DIRECTOR_NONE)
            pthread_cond_wait(&dir_cond, &dir_mutex);
        process = director_process;
        switch (process) {
        case DIRECTOR_JPEG:
            jpeg_buf_ptr = jpeg_sample_buffer[jpeg_buf_to_process];
            jpeg_buf_len = jpeg_sample_buffer_len[jpeg_buf_to_process];
            jpeg_buf_to_process = -1;
            break;
        case DIRECTOR_PCM:
            pcm_buf_ptr = pcm_sample_buffer[pcm_buf_to_process];
            pcm_buf_len = pcm_sample_buffer_len[pcm_buf_to_process];
            pcm_buf_to_process = -1;
            break;
        default:
            break;
        }
        director_running = 1;
        director_process = DIRECTOR_NONE;
        pthread_mutex_unlock(&dir_mutex);

        // Check stop signal
        if (stop_director || process == DIRECTOR_DIE)
            break;

        switch (process) {
        /* Handle JPEG / Face Detection */
        case DIRECTOR_JPEG:
#ifdef USE_JPEG
            // Decode JPEG as grayscale and extra strip portion of data
            jpeg_mem_src(&cinfo, jpeg_buf_ptr, jpeg_buf_len);
            ret = jpeg_read_header(&cinfo, TRUE);
            if (ret != JPEG_HEADER_OK || cinfo.image_width != STRIP_WIDTH || cinfo.image_height < STRIP_HEIGHT) {
                printf("Invalid JPEG image or header found: %dx%d\n", cinfo.image_width, cinfo.image_height);
                jpeg_abort_decompress(&cinfo);
                director_running = 0;
                continue;
            }

            // Valid image, start reading out processed grayscale data
            cinfo.out_color_space = JCS_GRAYSCALE;
            jpeg_start_decompress(&cinfo);
            for (lines = 0; lines < STRIP_HEIGHT; lines++) {
                uint8_t *rowbuf[1];
                rowbuf[0] = grayscale_buf + (lines * STRIP_WIDTH);
                ret = jpeg_read_scanlines(&cinfo, rowbuf, 1);
            }
            jpeg_abort_decompress(&cinfo);

            // Run the strip through face detection routine
            qcff_set_frame(qcff_handle, grayscale_buf);
            ret = qcff_get_complete_info(qcff_handle, QCFF_MAX_FACES, qcff_face_indices, &num_faces, &qcff_info);
            if (QCFF_FAILED(ret)) {
                fprintf(stderr, "Error running face detection: %d\n", ret);
            } else {
#ifdef QCFF_DEBUG
                printf("%d faces found\n", num_faces);
                for (i = 0; i < num_faces; i++) {
                    printf(" %d %d %d %d\n", qcff_faces[i].bounding_box.x, qcff_faces[i].bounding_box.y, qcff_faces[i].bounding_box.dx, qcff_faces[i].bounding_box.dy);
                }
#endif
                current_state.num_faces = num_faces;
                for (i = 0; i < num_faces; i++) {
                    current_state.face_phase[i] = ((float) (qcff_faces[i].bounding_box.x + (qcff_faces[i].bounding_box.dx >>1 )) - 960) / -640 * PI;
                }
            }
#endif // USE_JPEG
            break;

        /* Handle PCM Localization */
        case DIRECTOR_PCM:
	    if (owlUpdateCB == NULL)
	      {
		get_angle(pcm_buf_ptr, pcm_buf_len, &beam_info_data);
		current_state.beam_id = beam_info_data.angle;
		current_state.beam_energy = beam_info_data.energy;
	      }
	    else
	      {
		// let owl-core process PCM data
		owlUpdateCB(pcm_buf_ptr, pcm_buf_len, MICS);
	      }

            beam_valid = 0;
            current_state.selected_beam_energy /= BEAM_ENERGY_DERATING;
            if (current_state.beam_energy > BEAM_ENERGY_THRESHOLD && current_state.beam_energy > current_state.selected_beam_energy) {
                beam_valid = 1;
                current_state.selected_beam_id = current_state.beam_id;
                current_state.selected_beam_energy = current_state.beam_energy;
                current_state.selected_beam_phase = MIC_TO_PHASE_OFFSET(current_state.beam_id);
            } else if (current_state.selected_beam_energy > BEAM_ENERGY_FLOOR) {
                beam_valid = 1;
            }

            break;
        default:
            fprintf(stderr, "How did you get here?!?\n");
            break;
        }

        if (current_state.num_faces > 0) {
#if 0
// For now, always display microphone on left and face detection on right
            if (current_state.num_faces == 1) {
#endif
                if (beam_valid) {
                    current_command.command[0].target_phase = current_state.face_phase[0];
                    current_command.command[0].target_frame_count = 10;
                    current_command.command[0].command_valid = 1;
                    current_command.command[0].command_source = CMD_SRC_CAMERA;
                    current_command.command[1].target_phase = current_state.selected_beam_phase;
                    current_command.command[1].target_frame_count = 10;
                    current_command.command[1].command_valid = 1;
                    current_command.command[1].command_source = CMD_SRC_MIC;
                } else {
                    current_command.command[0].target_phase = current_state.face_phase[0] + (PI * 0.25f);
                    current_command.command[0].target_frame_count = 10;
                    current_command.command[0].command_valid = 1;
                    current_command.command[0].command_source = CMD_SRC_CAMERA;
                    current_command.command[1].target_phase = current_state.face_phase[0] - (PI * 0.25f);
                    current_command.command[1].target_frame_count = 10;
                    current_command.command[1].command_valid = 1;
                    current_command.command[1].command_source = CMD_SRC_CAMERA;
                }
#if 0
            } else {
                if (beam_valid) {
                    current_command.command[0].target_phase = current_state.face_phase[0];
                    current_command.command[0].target_frame_count = 10;
                    current_command.command[0].command_valid = 1;
                    current_command.command[0].command_source = CMD_SRC_CAMERA;
                    current_command.command[1].target_phase = current_state.selected_beam_phase;
                    current_command.command[1].target_frame_count = 10;
                    current_command.command[1].command_valid = 1;
                    current_command.command[1].command_source = CMD_SRC_MIC;
                } else {
                    current_command.command[0].target_phase = current_state.face_phase[0];
                    current_command.command[0].target_frame_count = 10;
                    current_command.command[0].command_valid = 1;
                    current_command.command[0].command_source = CMD_SRC_CAMERA;
                    current_command.command[1].target_phase = current_state.face_phase[1];
                    current_command.command[1].target_frame_count = 10;
                    current_command.command[1].command_valid = 1;
                    current_command.command[1].command_source = CMD_SRC_CAMERA;
                }
            }
#endif
            current_command.command_id++;
        } else if (beam_valid) {
            current_command.command[0].target_phase = current_state.selected_beam_phase + (PI * 0.25f);
            current_command.command[0].target_frame_count = 10;
            current_command.command[0].command_valid = 1;
            current_command.command[0].command_source = CMD_SRC_MIC;
            current_command.command[1].target_phase = current_state.selected_beam_phase - (PI * 0.25f);
            current_command.command[1].target_frame_count = 10;
            current_command.command[1].command_valid = 1;
            current_command.command[1].command_source = CMD_SRC_MIC;
            current_command.command_id++;
        }

        director_running = 0;
    }
#ifdef USE_JPEG
    jpeg_destroy_decompress(&cinfo);
#endif
    return NULL;
}

/* Todo: need to protect current_command... but for now, rely on the fact that
   command_id is incremented at the end */
director_command getDirectorCommands()
{
    return current_command;
}

int initDirector()
{
    int rc, i;
    current_command.command_id = 0;
    for (i = 0; i < MAX_ROI; i++){
        current_command.command[i].command_valid = 0;
    }

    qcff_config_t p_cfg = {STRIP_WIDTH, STRIP_HEIGHT, 0};

    stop_director = 0;
    rc = qcff_create(&qcff_handle);
    if (QCFF_FAILED(rc)) {
        fprintf(stderr, "Could not create face detection library: %d\n", rc);
        return 1;
    }
    rc = qcff_config(qcff_handle, &p_cfg);
    if (QCFF_FAILED(rc)) {
        fprintf(stderr, "Could not configure face detection library: %d\n", rc);
        return 1;
    }
    rc = qcff_set_mode(qcff_handle, QCFF_MODE_STILL);
    if (QCFF_FAILED(rc)) {
        fprintf(stderr, "Could not set face detection library mode: %d\n", rc);
        return 1;
    }

    // Initialize facialproc library data structures
    for (i = 0; i < QCFF_MAX_FACES; i++){
        qcff_face_indices[i] = i;
    }

    memset(&qcff_info, 0, sizeof(qcff_info));
    qcff_info.p_rects = qcff_faces;
    return 0;
}

int startDirector()
{
    int result = pthread_create(&directorThread, NULL, runDirector, NULL);
    if (result == 0) {
        pthread_setname_np(directorThread, "runDirector");
    }
    return result;
}

int stopDirector()
{
    pthread_mutex_lock(&dir_mutex);
    director_running = 0;
    stop_director = 1;
    director_process = DIRECTOR_DIE;
    pthread_cond_signal(&dir_cond);
    pthread_mutex_unlock(&dir_mutex);
    qcff_destroy(&qcff_handle);
    pthread_join(directorThread, NULL);
    return 0;
}

int registerDirectorUpdateCallback(owlUpdateFunc cb)
{
    owlUpdateCB = cb;
    return 0;
}
