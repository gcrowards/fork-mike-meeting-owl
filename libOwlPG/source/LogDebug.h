/* Copyright Playground Global 2016 */

#ifndef LOGDEBUG_H
#define LOGDEBUG_H

#include <stdio.h>
#include <errno.h>

/* Enable Timing Logs - this will generate /data/framelog_* files
   which basically just contain an array of struct timevals which
   can be unpacked to see the relative timing.  Due to the callback
   nature, the indicies should match all the way to UVC.  See /host
   for a simple program that can unpack the information */
//#define ENABLE_FRAME_TIMELOG

#ifdef ENABLE_FRAME_TIMELOG
#define FRAME_LOG_MAX 512

typedef struct {
    uint32_t counter;
    struct timeval data[FRAME_LOG_MAX];
} frame_log_data;

#define TIMELOG_INIT(x) static frame_log_data _log_data_##x; \
                        static FILE *_##x##_fp = NULL;

#define TIMELOG_LOG(x) do { gettimeofday(&_log_data_##x.data[_log_data_##x.counter++], NULL); \
                            if (_log_data_##x.counter >= FRAME_LOG_MAX) { \
                                if ( !_##x##_fp ) \
                                    _##x##_fp = fopen("/data/framelog_" #x ".data", "wb"); \
                                if ( _##x##_fp ) { \
                                    fwrite(_log_data_##x.data, sizeof(struct timeval), FRAME_LOG_MAX, _##x##_fp); \
                                    fflush(_##x##_fp); \
                                } else { printf("LogError " #x ": %d\n", errno); } \
                                _log_data_##x.counter = 0; \
                            } \
                       } while (0); \

#define TIMELOG_END(x) do { if ( !_##x##_fp) fclose(_##x##_fp); _##x##_fp = NULL; } while (0);

#else

#define TIMELOG_INIT(x)
#define TIMELOG_LOG(x)
#define TIMELOG_END(x)

#endif

#endif
