/* Copyright Playground Global 2016 */

#ifndef DIRECTORCOMMANDS_H
#define DIRECTORCOMMANDS_H

#define MAX_DETECTED_FACES 5
#define MAX_ROI 2
#define MAX_HISTORY 2

enum cmd_src {
    CMD_SRC_CAMERA,
    CMD_SRC_MIC,
    CMD_SRC_NONE,
};

/* Internal State information used by Director */
typedef struct {
    int num_faces;
    float face_phase[MAX_DETECTED_FACES];
    int beam_id;
    int beam_energy;
    int selected_beam_id;
    int selected_beam_energy;
    float selected_beam_phase;
} director_state;

/* Internal State information used by Graphics Handler*/
typedef struct {
    float target_phase;
    float current_phase;
    float phase_delta;
    int target_frame_count;
    int current_frame_count;
    enum cmd_src command_source;
} roi_state;

/* ROI Sub-command sent to Graphics Handler */
typedef struct {
    /* Target phase to point after transition */
    float target_phase;
    /* Number of frames at 30FPS to get to target phase */
    int target_frame_count;
    /* Command source - from Camera or Microphone */
    enum cmd_src command_source;
    /* Is command valid? */
    uint8_t command_valid;
} roi_command;

/* Director commands */
typedef struct {
    uint32_t command_id;    
    roi_command command[MAX_ROI];
} director_command;


// ---------------
// RENDER COMMANDS
// ---------------

#define MAX_RENDER_COMMANDS  200

enum render_type
  {
    CMD_RENDER_PANEL,
    CMD_RENDER_ELLIPSE,
    CMD_RENDER_LINE,
    CMD_RENDER_RECTANGLE,
    CMD_RENDER_NONE,
  };

typedef struct
{
  int    left, top, width, height;
  float  theta0, theta1, r0, r1;
  float  alpha, stretch, bend;
} panel_command;

typedef struct
{
  int    x0, y0, x1, y1;
  int    left, top, width, height;
  int    filled;
  float  r, g, b, alpha;
  float  lineWidth;
} shape_command;

typedef struct
{
  enum render_type renderType;
  panel_command    panel;
  shape_command    shape;
} render_command;


#endif // DIRECTORCOMMANDS_H
