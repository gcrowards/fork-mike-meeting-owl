/* Copyright 2016 Playground Global

   Encapsulate MediaCodec interface for native handling
   of H264 or other MediaCodec components
*/

#include "CodecHandler.h"
#include "LogDebug.h"

#include <binder/IPCThreadState.h>
#include <gui/Surface.h>
#include <media/ICrypto.h>
#include <OMX_IVCommon.h>
#include <MediaCodec.h>
#include <MediaErrors.h>
#include <foundation/AMessage.h>
#include <foundation/ABuffer.h>

#define MAX_REGISTERED_CALLBACKS 4

//#define CODEC_DEBUG

using namespace android;

static sp<MediaCodec> codec;
static sp<IGraphicBufferProducer> codecBufferProducer;
static sp<ANativeWindow> codecWindow;
static Vector<sp<ABuffer> > codecBuffers;
static pthread_t codecThread;
static bool processBuffers;

static codecOutputFrameCB callback[MAX_REGISTERED_CALLBACKS];

TIMELOG_INIT(codec);

static void *codecFunc(void *)
{
    int ret;
    size_t bufIndex, offset, size;
    int64_t ptsUsec;
    uint32_t flags;
    processBuffers = true;

    ret = codec->getOutputBuffers(&codecBuffers);
    if ( ret != NO_ERROR ){
        fprintf(stderr, "Could not get output buffers - %d\n", ret);
        return NULL;
    }

    while (processBuffers) {
        int l = 0;
        unsigned int s;
        ret = codec->dequeueOutputBuffer(&bufIndex, &offset, &size, &ptsUsec, &flags, 1000000 /* 1s timeout */);
        switch (ret) {
        case NO_ERROR:
            // There are special cases depending on flags...  but for now, send everything
            for (int i = 0; i < MAX_REGISTERED_CALLBACKS; i++){
                if (callback[i]) {
                    callback[i](codecBuffers[bufIndex]->data() + offset, size, ptsUsec, flags);
                } else {
                    break;
                }
            }
            TIMELOG_LOG(codec);
            ret = codec->releaseOutputBuffer(bufIndex);
            break;
        case -EAGAIN:
#ifdef CODEC_DEBUG
	    fprintf(stderr, "###CODEC THREAD -EAGAIN - %d\n", ret);
#endif
            break;
        case INFO_FORMAT_CHANGED:
#ifdef CODEC_DEBUG
	    fprintf(stderr, "###CODEC THREAD FORMAT_CHANGED - %d\n", ret);
#endif
            break;
        case INFO_OUTPUT_BUFFERS_CHANGED:
#ifdef CODEC_DEBUG
	    fprintf(stderr, "###CODEC THREAD OUTPUT_BUFFERS_CHANGED - %d\n", ret);
#endif
            break;
        case INVALID_OPERATION:
#ifdef CODEC_DEBUG
	    fprintf(stderr, "###CODEC THREAD INVALID_OPERATION - %d\n", ret);
#endif
            processBuffers = false;
            break;
        default:
#ifdef CODEC_DEBUG
	    fprintf(stderr, "###CODEC THREAD ERROR! - %d\n", ret);
#endif
            break;
        }
    }

    return NULL;
}

int registerCodecCallback(codecOutputFrameCB cb){
    for (int i = 0; i < MAX_REGISTERED_CALLBACKS; i++) {
        if (callback[i] == NULL){
            callback[i] = cb;
            return 0;
        }
    }
    return 1;
}

int initCodec(){
    int i, ret = 0;

    for (i = 0; i < MAX_REGISTERED_CALLBACKS; i++) {
        callback[i] = NULL;
    }

    /* Create Binder Threadpool */
    sp<ProcessState> self = ProcessState::self();
    self->startThreadPool();

    /* Create Looper Thread */
    sp<ALooper> looper = new ALooper;
    looper->setName("encoder_looper");
    looper->start();

    /* Create Configuration Message */
    sp<AMessage> msg = new AMessage();
    msg->setInt32("width", ENCODER_WIDTH);
    msg->setInt32("height", ENCODER_HEIGHT);
    msg->setString("mime", ENCODER_FORMAT);
    msg->setFloat("frame-rate", ENCODER_FRAMERATE);
    msg->setInt32("color-format", OMX_COLOR_FormatAndroidOpaque);
    msg->setInt32("bitrate", ENCODER_BITRATE);
#ifdef CODEC_H264
    msg->setInt32("i-frame-interval", ENCODER_IFRAME_INTERVAL);
#endif

    /* Create MediaCodec */
    codec = MediaCodec::CreateByType(looper, ENCODER_FORMAT, true);
    if (codec == NULL) {
        fprintf(stderr, "Could not create MediaCodec\n");
        return -1;
    }

    ret = codec->configure(msg, NULL, NULL, MediaCodec::CONFIGURE_FLAG_ENCODE);
    if (ret != 0) {
        codec->release();
        codec.clear();
        fprintf(stderr, "Could not configure codec: %d\n", ret);
        return -2;
    }

    /* Get InputSurface on which GL should render */
    ret = codec->createInputSurface(&codecBufferProducer);
    if (ret != 0) {
        codec->release();
        codec.clear();
        fprintf(stderr, "Could not create input surface: %d\n", ret);
        return -3;
    }

    /* Start the codec */
    ret = codec->start();
    if (ret != 0) {
        codec->release();
        codec.clear();
        fprintf(stderr, "Could not start codec: %d\n", ret);
        return -4;
    }

    codecWindow = new Surface(codecBufferProducer);

    return 0;
}

EGLNativeWindowType getCodecNativeWindow() {
    return codecWindow.get();
}

int startCodec() {
    int ret;
    struct sched_param params;

    /* Create Encoder Thread */
    pthread_attr_t attr;
    pthread_attr_init(&attr);

    ret = pthread_create(&codecThread, &attr, codecFunc, NULL);
    if (ret == 0)
      { pthread_setname_np(codecThread, "codecFunc"); }
    pthread_attr_destroy(&attr);

    params.sched_priority = 70;
    pthread_setschedparam(codecThread, SCHED_FIFO, &params);

    return ret;
}

int stopCodec() {
    TIMELOG_END(codec);
#ifdef CODEC_DEBUG
    fprintf(stdout, "Stopping Codec... takes couple of seconds...\n");
#endif
    processBuffers = false;
    if (codec != NULL) {
        codec->release();
        codec.clear();
    }
    codecWindow = NULL;
    codecBufferProducer = NULL;
    pthread_join(codecThread, NULL);
#ifdef CODEC_DEBUG
    fprintf(stdout, "... done.\n");
#endif
    return 0;
}
