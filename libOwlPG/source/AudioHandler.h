/* Copyright Playground Global 2016 */

#ifndef AUDIOHANDLER_H
#define AUDIOHANDLER_H

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*audioOutputCB)(int16_t *buf, int16_t *pRefIn,
                              uint32_t sample_count);
typedef void (*owlAudioOutputCB)(int16_t *dstBuf, int16_t *srcIn,
                                 int16_t *refIn, uint32_t sample_count);
int initAudio(owlAudioOutputCB);
/* Note that the registered callbacks are run at the highest priority
   thread doing the SPI reads - if you do anything silly in the callbacks,
   you _WILL_ drop packets. */
int registerAudioCallback(audioOutputCB);

int startAudio();

int stopAudio();

#ifdef __cplusplus
}
#endif

#endif
