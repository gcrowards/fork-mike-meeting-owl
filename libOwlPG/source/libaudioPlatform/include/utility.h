#ifndef UTILITY_H
#define UTILITY_H

#include <time.h>
#include <stdio.h>

#define DEBUG 10

#ifdef __cplusplus
extern "C" {
#endif

#define str(x)          #x
#define xstr(x)         str(x)

/** These DBG_xxx macros provide different levels of debug 
 *  based on the above xxx_PRINTF macros    */
#define DBG_DBG     (99)    /* Overly verbose */
#define DBG_INFO    (50)    /* Used to track execution and provide information debug */
#define DBG_WARN    (30)    /* Used for things that are not errors but should be handled */
#define DBG_CRIT    (20)    /* Used for criticall things that are not errors */
#define DBG_ERROR   (10)    /* Error condition */
#define DBG_NONE    (0)     /* Effectively print always if DBG_PRINTF is enabled */

#ifdef DEBUG

    #ifndef DBG_PREAMBLE
        #define DBG_PREAMBLE        "%s:%s(%d) "
        #define DBG_PREAMBLE_PARAMS __FILE__, __FUNCTION__, __LINE__
    #endif

    #if DEBUG < 0
        extern int ____debugLevel;
        #define DBG_TEST(level)     if (level <= ____debugLevel)
        #ifndef setDebugLevel
            #define setDebugLevel(level)    ____debugLevel = level
            #define getDebugLevel()         ____debugLevel
        #endif // setDebugLevel
    #else
        #define DBG_TEST(level)
        #ifndef setDebugLevel
            #define setDebugLevel(level)
            #define getDebugLevel()         (DEBUG)
        #endif // setDebugLevel
    #endif

    #ifndef _DBG_PRINTF
        #define _DBG_PRINTF(level, ...) \
            DBG_TEST(level) { \
                fprintf(stderr, ## __VA_ARGS__); \
                fflush(stderr); \
            }
    #endif // _DBG_PRINTF

    #ifndef _DBG_CALL
        #define _DBG_CALL(level, ...) \
            DBG_TEST(level) { \
                __VA_ARGS__; \
            }
    #endif // DBG_CALL

    #if DEBUG >= DBG_DBG || DEBUG <= -DBG_DBG
        #define _DBG_PRINTF_DBG_DBG(level, ...)     _DBG_PRINTF(level, __VA_ARGS__)
        #define _DBG_CALL_DBG_DBG(level, ...)               _DBG_CALL(level, __VA_ARGS__)
    #else
        #define _DBG_PRINTF_DBG_DBG(level, ...)
        #define _DBG_CALL_DBG_DBG(level, ...)
    #endif

    #if DEBUG >= DBG_INFO || DEBUG <= -DBG_INFO
        #define _DBG_PRINTF_DBG_INFO(level, ...)    _DBG_PRINTF(level, __VA_ARGS__)
        #define _DBG_CALL_DBG_INFO(level, ...)              _DBG_CALL(level, __VA_ARGS__)
    #else
        #define _DBG_PRINTF_DBG_INFO(level, ...)
        #define _DBG_CALL_DBG_INFO(level, ...)
    #endif

    #if DEBUG >= DBG_WARN || DEBUG <= -DBG_WARN
        #define _DBG_PRINTF_DBG_WARN(level, ...)    _DBG_PRINTF(level, __VA_ARGS__)
        #define _DBG_CALL_DBG_WARN(level, ...)              _DBG_CALL(level, __VA_ARGS__)
    #else
        #define _DBG_PRINTF_DBG_WARN(level, ...)
        #define _DBG_CALL_DBG_WARN(level, ...)
    #endif

    #if DEBUG >= DBG_CRIT || DEBUG <= -DBG_CRIT
        #define _DBG_PRINTF_DBG_CRIT(level, ...)    _DBG_PRINTF(level, __VA_ARGS__)
        #define _DBG_CALL_DBG_CRIT(level, ...)              _DBG_CALL(level, __VA_ARGS__)
    #else
        #define _DBG_PRINTF_DBG_CRIT(level, ...)
        #define _DBG_CALL_DBG_CRIT(level, ...)
    #endif

    #if DEBUG >= DBG_ERROR || DEBUG <= -DBG_ERROR
        #define _DBG_PRINTF_DBG_ERROR(level, ...)   _DBG_PRINTF(level, __VA_ARGS__)
        #define _DBG_CALL_DBG_ERROR(level, ...)             _DBG_CALL(level, __VA_ARGS__)
    #else
        #define _DBG_PRINTF_DBG_ERROR(level, ...)
        #define _DBG_CALL_DBG_ERROR(level, ...)
    #endif

    #define _DBG_PRINTF_DBG_NONE(level, ...)        _DBG_PRINTF(level, __VA_ARGS__)
    #define _DBG_CALL_DBG_NONE(level, ...)                  _DBG_CALL(level, __VA_ARGS__)

    /** These xxx_PRINTF macros provide for adding debug
     *  and diagnostic messages to the code, easy disabling
     *  and redirection if required   */
    #ifndef PRINTF
        /** This is just a regular printf and ignores the level which
         *  is present for compatibility or future capability
         *  basically the same as DBG_PRINTF(DBG_NONE, ... */
        #define PRINTF(level, ...) _DBG_PRINTF_DBG_NONE(level, __VA_ARGS__)
    #endif // PRINTF

    #ifndef DBG_PRINTF
        /** DBG_PRINTF will dependant on the compiled debug level
         *  enable or disable various debug messages */
        #define DBG_PRINTF(level, ...) _DBG_PRINTF_##level(level, __VA_ARGS__)
    #endif // DBG_PRINTF

    #ifndef NDBG_PRINTF
        /** NDBG_PRINTF is normally empy and just provides an easy
         *  way to disable a given DBG_PRINTF by prepending an "N" */
        #define NDBG_PRINTF(level, ...)
    #endif // NDBG_PRINTF

    /** The DBG_CALL macro allows for wrapping of a function call 
     *  which will be enabled or disable dependant on the debug level */
    #ifndef DBG_CALL
        #define DBG_CALL(level, ...) _DBG_CALL_##level(level, __VA_ARGS__)
    #endif // DBG_CALL

    /** PERF_TIME_... macros allow a piece of code to be wrapped
     *  and performance analyzed.  The parameter "x" uniquely
     *  identifies one PERF_TIME from another allow multiple to
     *  be used within a given section of code. */
    #define PERF_TIME_START(level,x)    clock_t perfTimeStart##x = clock()
    #define PERF_TIME_STOP(level,x)     _DBG_PRINTF(level, DBG_PREAMBLE " %s elapsed time %f\n", DBG_PREAMBLE_PARAMS, #x, ((double) (clock() - perfTimeStart##x)) / CLOCKS_PER_SEC)

#else // !defined(DEBUG)

    #define DBG_PREAMBLE        "%s:%s(%d) "
    #define DBG_PREAMBLE_PARAMS __FILE__, __FUNCTION__, __LINE__

    /** These xxx_PRINTF macros provide for adding debug
     *  and diagnostic messages to the code, easy disabling
     *  and redirection if required   */
    #ifndef PRINTF
        /** This is just a regular printf and ignores the level which
         *  is present for compatibility or future capability
         *  basically the same as DBG_PRINTF(DBG_NONE, ... */
        #define PRINTF(level, ...)          printf(__VA_ARGS__)
    #endif // PRINTF

    #ifndef DBG_PRINTF
        /** DBG_PRINTF will dependant on the compiled debug level
         *  enable or disable various debug messages */
        #define DBG_PRINTF(level, ...)
    #endif // DBG_PRINTF

    #ifndef NDBG_PRINTF
        /** NDBG_PRINTF is normally empy and just provides an easy
         *  way to disable a given DBG_PRINTF by prepending an "N" */
        #define NDBG_PRINTF(level, ...)
    #endif // NDBG_PRINTF

    /** The DBG_CALL macro allows for wrapping of a function call 
     *  which will be enabled or disable dependant on the debug level */
    #ifndef DBG_CALL
        #define DBG_CALL(level, ...)
    #endif // DBG_CALL

    #ifndef setDebugLevel
        #define setDebugLevel(level)
        #define getDebugLevel()
    #endif // setDebugLevel

    /** PERF_TIME_... macros allow a piece of code to be wrapped
     *  and performance analyzed.  The parameter "x" uniquely
     *  identifies one PERF_TIME from another allow multiple to
     *  be used within a given section of code. */
    #define PERF_TIME_START(level,x)
    #define PERF_TIME_STOP(level,x)

#endif // defined(DEBUG)

/** DUMP_FLAG_LINE_MASK masks off the portion of the flags
 *  that tells dumpBuffer howmany bytes to display on a line. */
#define DUMP_FLAG_LINE_MASK (0x000000FF)

/** These DUMP_FLAG_... defines tell dumpBuffer the size of
 *  the buffer contents and thus how to iterate on them. */
#define DUMP_FLAG_SIZE_MASK (0x00030000) /* Mask for the size portion of flags */
#define DUMP_FLAG_BYTE      (0x00000000) /* 8-bit This is the default */
#define DUMP_FLAG_HWORD     (0x00010000) /* 16-bit */
#define DUMP_FLAG_WORD      (0x00020000) /* 32-bit */
#define DUMP_FLAG_DWORD     (0x00030000) /* 64-bit */

/** These DUMP_FLAG_... defines tell dumpBuffer how to display 
 *  buffer contents. */
#define DUMP_FLAG_TYPE_MASK (0x000C0000) /* Mask for the display type portion of flags */
#define DUMP_FLAG_HEX       (0x00000000) /* hex */
#define DUMP_FLAG_INT       (0x00040000) /* signed int */
#define DUMP_FLAG_UINT      (0x00080000) /* unsigned int */
#define DUMP_FLAG_FLOAT     (0x000C0000) /* float (Only valid for DUMP_FLAG_WORD (float) and DUMP_FLAG_DWORD (double) */

/** These DUMP_FLAG_... defines provide guidance to dumpBuffer 
 *  as to how to iterate and display buffer contents. */
#define DUMP_FLAG_uint8     (DUMP_FLAG_BYTE | DUMP_FLAG_UINT)
#define DUMP_FLAG_int8      (DUMP_FLAG_BYTE | DUMP_FLAG_INT)
#define DUMP_FLAG_uint16    (DUMP_FLAG_HWORD | DUMP_FLAG_UINT)
#define DUMP_FLAG_int16     (DUMP_FLAG_HWORD | DUMP_FLAG_INT)
#define DUMP_FLAG_uint32    (DUMP_FLAG_WORD | DUMP_FLAG_UINT)
#define DUMP_FLAG_int32     (DUMP_FLAG_WORD | DUMP_FLAG_INT)
#define DUMP_FLAG_uint64    (DUMP_FLAG_DWORD | DUMP_FLAG_UINT)
#define DUMP_FLAG_int64     (DUMP_FLAG_DWORD | DUMP_FLAG_INT)
#define DUMP_FLAG_float     (DUMP_FLAG_WORD | DUMP_FLAG_FLOAT)
#define DUMP_FLAG_double    (DUMP_FLAG_DWORD | DUMP_FLAG_FLOAT)

/** DUMP_FLAG_ASCII tells dumpBuffer to add the ASCII version
 *  of the data to the end of each dump line. */
#define DUMP_FLAG_ASCII     (0x1000)

/** dumpBuffer provides a generic function for dumpping
 *  buffer/memory contents in a variety of formats. */
extern void dumpBuffer(void* buffer, uint32_t length, int flags);

/** countBits returns the count of 1 bits in value. */
extern int countBits(uint64_t value);

/** createLocalStatic defines a locally scoped static
 *  variable of type "type" called "object" using the
 *  "creator" and will force the current function to return
 *  -1 on failure.  If the object has previously been
 *  created it leaves it alone.  The variable args parameter
 *  "..." allows entry of additional initialization code that
 *  is executed on object creation only. */
#define createLocalStatic(type, object, creator, ...) \
    static type* object = NULL;\
    if (!object) {\
        object = creator;\
        if (!object) return -1;\
        {\
            __VA_ARGS__;\
        }\
    }

/** A union of various type pointers to simlify multi format
 *  process loops.  The field name is p... where the ... is
 *  the standard type name to avoid name conflicts */
typedef union {
    void*       pvoid;
    uint8_t*    puint8_t;
    uint16_t*   puint16_t;
    uint32_t*   puint32_t;
    uint64_t*   puint64_t;
    int8_t*     pint8_t;
    int16_t*    pint16_t;
    int32_t*    pint32_t;
    int64_t*    pint64_t;
    float*      pfloat;
    double*     pdouble;
} multiPtr_t;

#ifdef __cplusplus
}
#endif

#endif // UTILITY_H
