#ifndef AUDIOPACKET_H
#define AUDIOPACKET_H

#include "utility.h"

#ifdef __cplusplus
extern "C" {
#endif

/** DEF_AUDIO_PAYLOAD_SZ is the size in bytes of the payload.
 *  Default defined here but can be overridden before
 *  including this file. */
#ifndef DEF_AUDIO_PAYLOAD_SZ
    #define DEF_AUDIO_PAYLOAD_SZ    (8192)      /* Default number of bytes in the payload part of the packet */
#endif // DEF_AUDIO_PAYLOAD_SZ

/** Magic number defines */
#define MAGICNO_MASK_TYPE       (0xFF000000)    /* Mask for the type */
#define MAGICNO_MASK_CHCNT      (0x00FF0000)    /* Mask for the channel count */
#define MAGICNO_MASK_CHSIZ      (0x0000FF00)    /* Mask for the channel size in bits (storage size) */
#define MAGICNO_MASK_SASIZ      (0x000000FF)    /* Mask for the sample size in bits */
#define MAGICNO_MASK_NOTSASIZ   (0xFFFFFF00)    /* Mask everything except the sample size */

#define MAGICNO_EMPTY           (0x00000000)    /* Empty buffer */
#define MAGICNO_NOTRDY          (0xFFFFFFFF)    /* Not ready all 1's so assuming uController not ready */
#define MAGICNO_FIRST           (0xdead0000)    /* Test pattern number for first packet from device */
#define MAGICNO_DEV             (0xdeadbeef)    /* Test pattern number for development / data from device */
#define MAGICNO_4PDM1P          (0x02040101)    /* uint PDM  1 bit packed as 2x 4 channels of 1 bit samples in 1 byte */
#define MAGICNO_8PDM1P          (0x02080101)    /* uint PDM  1 bit packed as  8 channels of 1 bit samples in 1 byte */
#define MAGICNO_8CNT4P          (0x02080404)    /* uint 2x PDM  4 bit packed as  8 channels of 4 bit counts in 4 bytes */
#define MAGICNO_8PCM1           (0x01080801)    /* int PCM  2 bit in 1 byte  8 channels First decimation by 8 */
#define MAGICNO_8PCM4           (0x01080804)    /* int PCM  4 bit in 1 byte  8 channels First decimation by 8 */
#define MAGICNO_8PCM4P          (0x01080404)    /* int 2x PCM  4 bit packed as  8 channels of 4 bit pcm in 4 bytes First decimation by 8 */
#define MAGICNO_8PCM8           (0x01080808)    /* int PCM  8 bit in 1 byte  8 channels */
#define MAGICNO_8PCM16          (0x01081010)    /* int PCM 16 bit in 2 bytes 8 channels */
#define MAGICNO_8PCM24          (0x01081818)    /* int PCM 24 bit in 3 bytes 8 channels */
#define MAGICNO_8PCM32          (0x01082020)    /* int PCM 32 bit in 4 bytes 8 channels */

#define MAGICNO_8PDM8           (0x04080101)    /* 8 channels of 8x pdm samples as a byte in 8 bytes */

/** Packet Types match QAudioFormat settings from Qt to make packet conversion easier */
#define PACKET_TYPE_UNKNOWN		(0)			    /* Unknown type */
#define PACKET_TYPE_SIGNED		(1)			    /* Signed integers */
#define PACKET_TYPE_UNSIGNED	(2)			    /* Unsigned integers */
#define PACKET_TYPE_FLOAT		(3)			    /* Float */
#define PACKET_TYPE_PDM         (4)             /* Magic type for special PDM formats */
#define PACKET_TYPE_TURNED      (0x80)          /* Data has been turned and so is in channel order instead of sample order */

/** Function error returns */                   
#define PACKET_ERROR            (-1)            /* Refer to errno for details */
#define PACKET_GOOD             (0)             
#define PACKET_MISSED           (1)             /* Packet timestamp was out of sequence one or more missing */
#define PACKET_DISCARD          (2)             /* This is a duplicate of the last packet */
#define PACKET_NOTRDY           (3)             /* result packet not yet ready/full */

/** audioPacketProcess flags */
#define PACKET_PROCESS_VALIDATED    (1)         /* input packet already validated */

/** Helper macro's to fix byte swap due to 16 bit transfer */
#define host2spi32(data)      ((((data) & 0xff00ff00) >> 8) | (((data) & 0x00ff00ff) << 8))
#define host2spi16(data)      ((((data) & 0xff00) >> 8) | (((data) & 0x00ff) << 8))

/** Local types */
/** Mic data buffer */
/************************************************************************/
/* The first three fields are typically static and can be used as a     */
/* sync mark to re-sync packets of data                                 */
/************************************************************************/
typedef struct audioHeader_s {
    union {
        uint32_t	magicnumber;        // Magic number combines sampleBits, channelBits , 
                                        // channelCnt and type into single word for compares
        struct {                        
            uint8_t sampleBits;         // Number of significant bits per sample
            uint8_t channelBits;        // Number of bits per channel allocated
            uint8_t channelCnt;         // Channels in the packet
            uint8_t type;               // 0 = Unknown, 1 = Signed Int, 2 = Unsigned Int, 3 = Float
        };                              
    };                                  
    uint32_t	rate;                   // Sampling rate in Hz
    union {                             
        uint32_t    sampleCnt32;        // Large 32-bit sample count used by SOC and Hosts
        struct {                        // Small sample count used by uController with buffer mgmt
            uint16_t	samples;        // Number of samples in the buffer from uController
            uint16_t	bufnumber;      // buffer number for tracking and debug from uController
        };                              
    };                                  
    uint32_t    bufSize;                // Data buffer size in memory for tracking
    uint32_t    reserved;               // Reserved field to pad header to 64 bit boundary.
    uint32_t	timestamp;              // timestamp
    // Followed by data
} audioHeader_t;

/** audio_t is the standard type for audio packets actual
 *  length may vary vs. the DEF_AUDIO_PAYLOAD_SZ and is
 *  dependant on the header.bufSize variable */
typedef struct audio_s {
    // To make things easy the header must be a multiple of 8 bytes (64 bits).
    audioHeader_t   header;
    // This union and thus the actual sample/channel data must go
    // last as this allows a generic interface to different types and
    // sized packets.  As defined here provides the default as
    // delivered from the hardware over SPI.
	union {
        uint8_t		data8[DEF_AUDIO_PAYLOAD_SZ   / sizeof(uint8_t)];
        uint16_t	data16[DEF_AUDIO_PAYLOAD_SZ  / sizeof(uint16_t)];
        uint32_t	data32[DEF_AUDIO_PAYLOAD_SZ  / sizeof(uint32_t)];
        int8_t		sdata8[DEF_AUDIO_PAYLOAD_SZ  / sizeof(int8_t)];
        int16_t	    sdata16[DEF_AUDIO_PAYLOAD_SZ / sizeof(int16_t)];
        int32_t	    sdata32[DEF_AUDIO_PAYLOAD_SZ / sizeof(int32_t)];
        float       fdata[DEF_AUDIO_PAYLOAD_SZ   / sizeof(float)];
        double      ddata[DEF_AUDIO_PAYLOAD_SZ   / sizeof(double)];
	};
} audio_t;

/** audioDataIterator_t is a helper type defined to allow
 *  typeless access to data within a packet */
typedef struct audioDataIterator_s {
    multiPtr_t      ptrs;       // Pointer union to simplify loops
    uint8_t         iterInc;    // Number to increment the pointer by usually channelCnt
} audioDataIterator_t;

/** audioPacketNewIterator creates an iterator for
 *  accessing a given data/ channel in a buffer so each call is
 *  access to the successive sample for that channel.
 *  By setting channels to 1 and channel to 0 this will
 *  iterate every element in the buffer */
#define audioPacketNewIterator(packet, sample, channel, channels, ...)  \
    audioDataIterator_t packet##_iter##__VA_ARGS__;\
    uint8_t packet##_iterInc##__VA_ARGS__ = channels;\
    packet##_iter##__VA_ARGS__.ptrs.pvoid = packet->data8 + (sample * channels + channel) * packet->header.channelBits / 8;

/** audioPacketGetNextSample... gets the next element in the
 *  channel and pointer is post incremented by the channelCnt */
#define audioPacketGetNextSample(type, packet, ...) ({\
    type data = *packet##_iter##__VA_ARGS__.ptrs.p##type;\
    packet##_iter##__VA_ARGS__.ptrs.p##type += packet##_iterInc##__VA_ARGS__;\
    data;\
})
#define audioPacketGetNextSample_uint32(packet, ...)        audioPacketGetNextSample(uint32_t, packet, __VA_ARGS__)
#define audioPacketGetNextSample_uint16(packet, ...)        audioPacketGetNextSample(uint16_t, packet, __VA_ARGS__)
#define audioPacketGetNextSample_uint8(packet, ...)         audioPacketGetNextSample(uint8_t,  packet, __VA_ARGS__)
#define audioPacketGetNextSample_int32(packet, ...)         audioPacketGetNextSample(int32_t,  packet, __VA_ARGS__)
#define audioPacketGetNextSample_int16(packet, ...)         audioPacketGetNextSample(int16_t,  packet, __VA_ARGS__)
#define audioPacketGetNextSample_int8(packet, ...)          audioPacketGetNextSample(int8_t,   packet, __VA_ARGS__)
#define audioPacketGetNextSample_double(packet, ...)        audioPacketGetNextSample(double,   packet, __VA_ARGS__)
#define audioPacketGetNextSample_float(packet, ...)         audioPacketGetNextSample(float,    packet, __VA_ARGS__)

/** audioPacketSetNextSample... sets the next element in the
 *  channel and pointer is post incremented by
 *  the channelCnt */
#define audioPacketSetNextSample(data, type, packet, ...) ({\
    *packet##_iter##__VA_ARGS__.ptrs.p##type = data;\
    packet##_iter##__VA_ARGS__.ptrs.p##type += packet##_iterInc##__VA_ARGS__;\
    data;\
})
#define audioPacketSetNextSample_uint32(data, packet, ...)  audioPacketSetNextSample(data, uint32_t, packet, __VA_ARGS__)
#define audioPacketSetNextSample_uint16(data, packet, ...)  audioPacketSetNextSample(data, uint16_t, packet, __VA_ARGS__)
#define audioPacketSetNextSample_uint8(data, packet, ...)   audioPacketSetNextSample(data, uint8_t,  packet, __VA_ARGS__)
#define audioPacketSetNextSample_int32(data, packet, ...)   audioPacketSetNextSample(data, int32_t,  packet, __VA_ARGS__)
#define audioPacketSetNextSample_int16(data, packet, ...)   audioPacketSetNextSample(data, int16_t,  packet, __VA_ARGS__)
#define audioPacketSetNextSample_int8(data, packet, ...)    audioPacketSetNextSample(data, int8_t,   packet, __VA_ARGS__)
#define audioPacketSetNextSample_double(data, packet, ...)  audioPacketSetNextSample(data, double,   packet, __VA_ARGS__)
#define audioPacketSetNextSample_float(data, packet, ...)   audioPacketSetNextSample(data, float,    packet, __VA_ARGS__)

#define audioPacketGetNextData(packet, ...) ({                                  \
    switch (packet->header.type) {                                              \
    case PACKET_TYPE_UNKNOWN:                                                   \
    case PACKET_TYPE_UNSIGNED:                                                  \
        if (packet->header.channelBits > 16) {                                  \
            data = audioPacketGetNextSample_uint32(packet, __VA_ARGS__);        \
        } else if (packet->header.channelBits > 8) {                            \
            data = audioPacketGetNextSample_uint16(packet, __VA_ARGS__);        \
        } else {                                                                \
            data = audioPacketGetNextSample_uint8(packet, __VA_ARGS__);         \
        }                                                                       \
        break;                                                                  \
    case PACKET_TYPE_SIGNED:                                                    \
        if (packet->header.channelBits > 16) {                                  \
            data = audioPacketGetNextSample_int32(packet, __VA_ARGS__);         \
        } else if (packet->header.channelBits > 8) {                            \
            data = audioPacketGetNextSample_int16(packet, __VA_ARGS__);         \
        } else {                                                                \
            data = audioPacketGetNextSample_int8(packet, __VA_ARGS__);          \
        }                                                                       \
        break;                                                                  \
    case PACKET_TYPE_FLOAT:                                                     \
        if (packet->header.channelBits > 32) {                                  \
            data = audioPacketGetNextSample_double(packet, __VA_ARGS__);        \
        } else {                                                                \
            data = audioPacketGetNextSample_float(packet, __VA_ARGS__);         \
        }                                                                       \
        break;                                                                  \
    case PACKET_TYPE_PDM:                                                       \
    default:                                                                    \
        DBG_PRINTF(DBG_ERROR, "%s:%s iterator unsupported type\n", __FILE__, __FUNCTION__);    \
        return PACKET_ERROR;                                                    \
    }                                                                           \
    data; })

#define audioPacketGetNextDataSIGNED(packet, ...) ({                        \
    if (packet->header.channelBits > 16) {                                  \
        data = audioPacketGetNextSample_int32(packet, __VA_ARGS__);         \
    } else if (packet->header.channelBits > 8) {                            \
        data = audioPacketGetNextSample_int16(packet, __VA_ARGS__);         \
    } else {                                                                \
        data = audioPacketGetNextSample_int8(packet, __VA_ARGS__);          \
    }                                                                       \
    data; })

#define audioPacketSetNextData(data, packet, ...)                               \
    switch (packet->header.type) {                                              \
    case PACKET_TYPE_UNKNOWN:                                                   \
    case PACKET_TYPE_UNSIGNED:                                                  \
        if (packet->header.channelBits > 16) {                                  \
            audioPacketSetNextSample_uint32(data, packet, __VA_ARGS__);         \
        } else if (packet->header.channelBits > 8) {                            \
            audioPacketSetNextSample_uint16(data, packet, __VA_ARGS__);         \
        } else {                                                                \
            audioPacketSetNextSample_uint8(data, packet, __VA_ARGS__);          \
        }                                                                       \
        break;                                                                  \
    case PACKET_TYPE_SIGNED:                                                    \
        if (packet->header.channelBits > 16) {                                  \
            audioPacketSetNextSample_int32(data, packet, __VA_ARGS__);          \
        } else if (packet->header.channelBits > 8) {                            \
            audioPacketSetNextSample_int16(data, packet, __VA_ARGS__);          \
        } else {                                                                \
            audioPacketSetNextSample_int8(data, packet, __VA_ARGS__);           \
        }                                                                       \
        break;                                                                  \
    case PACKET_TYPE_FLOAT:                                                     \
        if (packet->header.channelBits > 32) {                                  \
            audioPacketSetNextSample_double(data, packet, __VA_ARGS__);         \
        } else {                                                                \
            audioPacketSetNextSample_float(data, packet, __VA_ARGS__);          \
        }                                                                       \
        break;                                                                  \
    case PACKET_TYPE_PDM:                                                       \
    default:                                                                    \
        DBG_PRINTF(DBG_ERROR, "%s:%s iterator unsupported type\n", __FILE__, __FUNCTION__);    \
        return PACKET_ERROR;                                                    \
    }

#define audioPacketSetNextDataSIGNED(data, packet, ...)                     \
    if (packet->header.channelBits > 16) {                                  \
        audioPacketSetNextSample_int32(data, packet, __VA_ARGS__);          \
    } else if (packet->header.channelBits > 8) {                            \
        audioPacketSetNextSample_int16(data, packet, __VA_ARGS__);          \
    } else {                                                                \
        audioPacketSetNextSample_int8(data, packet, __VA_ARGS__);           \
    }

/** audioPacketProcess_PREAMBLE_notInPlace
 *  provides the preamble that almost all functions that process
 *  packets need. This version specifically dissallows in
 *  place packet manipulation so if packetIn and packetOut match
 *  it will generate an error. It also provides these variables
 *  to the function...
 *  sampleCntIn     number of samples in packetIn,
 *  sampleCntOut    number of samples already in packetOut,
 *  addSampleCnt    number of samples that will be added   */
#define audioPacketProcess_PREAMBLE_notInPlace(packetIn, packetOut, decimate, interpolate) \
    if (packetIn == packetOut) goto error;\
    uint32_t sampleCntIn = packetIn->header.sampleCnt32;\
    uint32_t addSampleCnt = sampleCntIn * (interpolate) / (decimate);\
    if (audioPacketFull(packetOut, addSampleCnt)) audioPacketClear(packetOut);\
    if (audioPacketSamplesFit(addSampleCnt, packetOut)) goto error;\
    uint32_t sampleCntOut = packetOut->header.sampleCnt32;

/** audioPacketProcess_PREAMBLE_inPlace
 *  provides the preamble that almost all functions that process
 *  packets need. This version allows in place packet
 *  manipulation so if packetIn and packetOut match it will
 *  still work. It also provides these variables to the
 *  function...
 *  sampleCntIn     number of samples in packetIn,
 *  sampleCntOut    number of samples already in packetOut,
 *  addSampleCnt    number of samples that will be added   */
#define audioPacketProcess_PREAMBLE_inPlace(packetIn, packetOut, decimate, interpolate) \
    uint32_t sampleCntIn = packetIn->header.sampleCnt32;\
    uint32_t addSampleCnt = sampleCntIn * (interpolate) / (decimate);\
    if (packetIn == packetOut) {\
        audioPacketClear(packetOut);\
    } else {\
        if (audioPacketFull(packetOut, addSampleCnt)) audioPacketClear(packetOut);\
    }\
    if (audioPacketSamplesFit(addSampleCnt, packetOut)) goto error;\
    uint32_t sampleCntOut = packetOut->header.sampleCnt32;

/** audioPacketProcess_POSTAMBLE
 *  provides the postamble and error label that almost all
 *  functions that process packets need.  If the packetOut is
 *  full it will return PACKET_GOOD if it is not full
 *  PACKET_NOTRDY and if the error label is called
 *  PACKET_ERROR */
#define audioPacketProcess_POSTAMBLE(packetIn, packetOut) \
    if (audioPacketFull(packetOut, addSampleCnt)) return PACKET_GOOD;\
    return PACKET_NOTRDY;\
error:\
    DBG_PRINTF(DBG_ERROR, "%s:%s Error:\n", __FILE__, __FUNCTION__);\
    DBG_CALL(DBG_ERROR, audioPacketDumpHeaderCompare(packetIn, packetOut));\
    errno = EINVAL;\
    return PACKET_ERROR;

// Equivalent of the constructor to setup any internals
extern int audioPacketInit(void);

// Management functions
extern audio_t* audioPacketAlloc(uint32_t samples, uint8_t channels, uint8_t sampleSize, uint8_t channelSize, uint8_t type);
                                                // Create / Allocate a packet in memory

/** audioPacketAllocFromPacket is a helper function to create an equivalently 
 *  sized buffer to the packet parameter */
extern audio_t* audioPacketAllocFromPacket(audio_t* packet);
#define createLocalStatic_Packet(packet, basis, ...) \
    createLocalStatic(audio_t, packet, audioPacketAllocFromPacket(basis), __VA_ARGS__)

extern void audioPacketFree(audio_t* packet);   // Frees the packet from memory
extern int  audioPacketClear(audio_t* packet);    // Marks the packet as empty
extern int  audioPacketInvalid(audio_t *packet);  // Checks if packet valid and checks if duplicate of last packet
extern int  audioPacketFull(audio_t *packet, uint32_t addSampleCnt);    // Checks if the buffer in the packet is full
extern int  audioPacketEmpty(audio_t *packet);    // Checks if the buffer in the packet is empty

// Helper functions

// This set of functions deprecated should use new macros above.  So commented out to force errors till verified all is working
//extern int32_t audioPacketGetSample(audio_t* packet, uint16_t sample, uint8_t channel, int dataFracBits);
//extern double audioPacketGetSampleDouble(audio_t* packet, uint16_t sample, uint8_t channel);
//extern int audioPacketSetSample(audio_t* packet, uint16_t sample, uint8_t channel, int32_t data, int dataFracBits);
//extern int audioPacketSetSampleDouble(audio_t* packet, uint16_t sample, uint8_t channel, double data);

extern int audioPacketDeinterleave(audio_t* packetIn, audio_t* packetOut);
extern int audioPacketInterleave(audio_t* packetIn, audio_t* packetOut);

extern void audioPacketDumpHeader(audio_t* packet);
extern void audioPacketDumpHeaderCompare(audio_t* packet1, audio_t* packet2);
extern void audioPacketDumpData(audio_t* packet);

extern int audioPacketSamplesFit(uint32_t addSampleCnt, audio_t *packet);// Will addSampleCnt additional samples fit in the packet
extern int audioPacketSampleFits(uint32_t sample, audio_t *packet);      // Is sample a valid index into packet

extern int audioPacketCompareHeader(audio_t *packet1, audio_t *packet2); // 100% match of headers of the two packets
extern int audioPacketCompareData(audio_t *packet1, audio_t *packet2);   // 100% match of data of the two packets, doesn't require the headers to match
extern int audioPacketCompare(audio_t *packet1, audio_t *packet2);       // 100% match of both headers and data

// Functions to do specific conversions
extern int audioPacket_4PDM1P_to_8PDM8(audio_t *packetIn, audio_t *packetOut);
extern int audioPacket_8PDM1P_to_8PDM8(audio_t* packetIn, audio_t* packetOut);
extern int audioPacket_4PDM1P_to_8PCM1(audio_t *packetIn, audio_t *packetOut);
extern int audioPacket_8CNT4P_to_8PCM4(audio_t *packetIn, audio_t *packetOut);
extern int audioPacket_8PDM1P_to_8CNT4P_to_8PCM4(audio_t *packetIn, audio_t *packetOut);
extern int audioPacket_8PDM1P_to_8CNT4P(audio_t *packetIn, audio_t *packetOut);

// Packet processes
extern int audioPacketCopy(audio_t* packetIn, audio_t* packetOut, uint32_t channelMask, uint32_t gain, uint8_t gainFracBits, uint32_t flags);

// Processes the whole chain of conversions and/or filters
extern int audioPacketProcess(audio_t *packetIn, audio_t *packetOut, uint32_t flags);

#ifdef __cplusplus
}
#endif

#endif // AUDIOPACKET_H
