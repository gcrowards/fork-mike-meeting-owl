/* LP5562 Device Driver
   Copyright Owl Labs, Inc. 2016 */

#include <stdio.h>
#include <fcntl.h>

#include "I2cHandler.h"
#include "lp5562.h"

int lp5562_init(void)
{
	int fd = openI2C(I2C_PORT_1, LP5562_SLAVE_ADDR);
	if (fd < 0) {
		fprintf(stderr, "lp5562_init: Error opening I2C device!\n");
		return fd;
	}

	/* Write chip enable */
	i2cWriteReg8(fd, LP5562_EN, LP5562_CHIP_EN);
	usleep(1000);

	/* Disable all engines */
	i2cWriteReg8(fd, LP5562_OP_MODE, LP5562_ENG_DIS);

	/* Enable internal clock */
	i2cWriteReg8(fd, LP5562_CONFIG, LP5562_CLK_EN);

	/* Disable engine mapping */
	i2cWriteReg8(fd, LP5562_MAP, LP5562_MAP_DIS);

	/* Set PWM cycle to FULL for all LED sources */
	i2cWriteReg8(fd, LP5562_B_PWM, LP5562_PWM_FULL);
	i2cWriteReg8(fd, LP5562_G_PWM, LP5562_PWM_FULL);
	i2cWriteReg8(fd, LP5562_R_PWM, LP5562_PWM_FULL);
	i2cWriteReg8(fd, LP5562_W_PWM, LP5562_PWM_FULL);

	close(fd);

	return 0;
}

int lp5562_led_enable(int enable)
{
	int write_reg;
	int fd = openI2C(I2C_PORT_1, LP5562_SLAVE_ADDR);
	if (fd < 0) {
		fprintf(stderr, "lp5562_led_enable: Err opening I2C device!\n");
		return fd;
	}

	if (enable)
		write_reg = LP5562_MAP_EN;
	else
		write_reg = LP5562_MAP_DIS;

	i2cWriteReg8(fd, LP5562_MAP, write_reg);

	close(fd);

	return 0;
}

