/* Copyright 2016 Owl Labs, Inc.

   Encapsulate I2C interface for native handling
   of connected components
*/

#include <stdio.h>
#include <fcntl.h>

#include "I2cHandler.h"

union i2c_data
{
    uint8_t  byte;
    uint16_t word;
    uint8_t  block[I2C_SMBUS_BLOCK_MAX + 2];    // block [0] is used for length + one more for PEC
};

struct i2c_ioctl_data
{
    char read_write;
    uint8_t command;
    int size;
    union i2c_data *data;
};

static int i2c_access(int fd, char rw, uint8_t command, int size, union i2c_data *data)
{
    struct i2c_ioctl_data args;

    args.read_write = rw;
    args.command    = command;
    args.size       = size;
    args.data       = data;
    return ioctl(fd, I2C_SMBUS, &args);
}

int i2cReadReg8 (int fd, int reg)
{
    union i2c_data data;

    if (i2c_access(fd, I2C_SMBUS_READ, reg, I2C_SMBUS_BYTE_DATA, &data)) {
        return -1;
    } else {
        return data.byte & 0xFF;
    }
}

int i2cWriteReg8(int fd, int reg, int value)
{
    union i2c_data data;
    data.byte = value;
    return i2c_access(fd, I2C_SMBUS_WRITE, reg, I2C_SMBUS_BYTE_DATA, &data);
}

int openI2C(int bus, int slaveAddr)
{
    int fd;

    if (bus == 0) {
        fd = open("/dev/i2c-0", O_RDWR);
    } else if (bus == 1) {
	fd = open("/dev/i2c-1", O_RDWR);
    } else {
	fprintf(stderr, "i2c-%d: Invalid bus number\n", bus);
	return -1;
    }

    if (fd < 0) {
        fprintf(stderr, "/dev/i2c-%d: Could not open i2c device\n", bus);
        return fd;
    }

    if (ioctl(fd, I2C_SLAVE, slaveAddr) < 0) {
        fprintf(stderr, "0x%02x: Could not select slave device\n", slaveAddr);
        return -1;
    }

    return fd;
}

