/* Copyright 2016 Playground Global */

#include <stdio.h>
#include <string.h>

#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <utils/Timers.h>

#include "DirectorCommands.h"
#include "GraphicsHandler.h"
#include "LogDebug.h"

#define PI (3.141592653589793)
#define MESH_DENSITY (20)
#define OUTPUT_WIDTH 1280
#define OUTPUT_HEIGHT 720
#define PREVIEW_SIZE 1728

#define STRIP_WIDTH OUTPUT_WIDTH
#define STRIP_HEIGHT 160

#define ROI_WIDTH (OUTPUT_WIDTH/2)
#define ROI_HEIGHT (OUTPUT_HEIGHT - STRIP_HEIGHT)

#define FPS_COUNT_INTERVAL 900L

TIMELOG_INIT(gfx);

// Enable or disable 4x CPU box filter which can be used for strip dewarping
//#define ENABLE_CPU_FILTER

#ifdef ENABLE_CPU_FILTER
#include <ui/GraphicBuffer.h>
#include <ui/GraphicBufferAllocator.h>
#include <ui/GraphicBufferMapper.h>

#include <arm_neon.h>
#define FILTER_BUF_COUNT 2
#define FILTER_PIX_FORMAT 0x109
#define PREVIEW_UV_NV21
using namespace android;
static int filterCurFrame = 0;
static int filterRenderFrame = 1;
static android::sp<android::GraphicBuffer> filterBuf[FILTER_BUF_COUNT];

static int cpu_done = 0;
static int cpu_start = 0;
static pthread_t cpu_workthread;
static pthread_cond_t cpu_start_cond = PTHREAD_COND_INITIALIZER;
static pthread_cond_t cpu_done_cond = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t cpu_mutex = PTHREAD_MUTEX_INITIALIZER;

static uint8_t *raw_frame_dataptr;
#endif

static EGLContext mEglContext;
static EGLSurface mEglSurface;
static EGLDisplay mEglDisplay;
static GLuint tex_strip;
static GLuint tex_roi;
static GLuint vbo_strip;
static GLuint vbo_roi1;
static GLuint vbo_roi2;
static GLuint vao_strip;
static GLuint vao_roi1;
static GLuint vao_roi2;

static int strip_mesh_count = 0;
static int roi1_mesh_count = 0;
static int roi2_mesh_count = 0;

static codecNativeWindowFunc getCodecNW = NULL;

static directorCallback directorCB = NULL;

static int meshX, meshY;
static float * mROI1MeshVertices;
static float * mROI2MeshVertices;
static float * mStripMeshVertices;

static uint32_t last_command_id = 0;
static director_command current_command;
static roi_state current_roi[MAX_ROI];

#ifdef ENABLE_CPU_FILTER

void shrink2x(uint8_t* __restrict src1, uint8_t* __restrict src2, uint8_t* __restrict dest, int width)
{
    width >>= 4;
    while (width--)
    {
        asm (
            "pld [%[line1], #0xc00]     \n"
            "pld [%[line2], #0xc00]     \n"
            "vldm %[line1]!, {d0,d1}  \n"
            "vldm %[line2]!, {d2,d3}  \n"
            "vpaddl.u8 q0, q0         \n"
            "vpaddl.u8 q1, q1         \n"
            "vadd.u16  q0, q1         \n"
            "vshrn.u16 d0, q0, #2     \n"
            "vst1.8 {d0}, [%[dst]]! \n"
            :
            : [line1] "r"(src1), [line2] "r"(src2), [dst] "r"(dest)
            : "q0", "q1", "memory"
        );
    }
}

// do a 4x neon box filter shrink
void shrink4x(uint8_t* __restrict src, int srcStride, uint8_t* __restrict dst, int dstStride, int width, int height)
{
    uint8_t line0[2048];
    uint8_t line1[2048];    // max 3.5k * 3.5k
    height >>= 2;
    while (height--) {
        shrink2x(src,src+srcStride,line0,width);
        src += srcStride*2;
        shrink2x(src,src+srcStride,line1,width);
        src += srcStride*2;
        shrink2x(line0,line1,dst,width>>1);
        dst += dstStride;
    }
}

#define NV21

void shrink2x_uv(uint8_t* __restrict src1, uint8_t* __restrict src2, uint8_t* __restrict dest, int width)
{
    width >>= 4;
    while (width--)
    {
        asm (
            "pld [%[line1], #0x800]     \n"
            "pld [%[line2], #0x800]     \n"
            "vldm %[line1]!, {d0,d1}  \n"
            "vldm %[line2]!, {d2,d3}  \n"
#ifdef NV21
            "vuzp.u8 d1, d0  \n"
            "vuzp.u8 d3, d2  \n"
#else
            "vuzp.u8 d0, d1  \n"
            "vuzp.u8 d2, d3  \n"
#endif
            "vpaddl.u8 q0, q0         \n"
            "vpaddl.u8 q1, q1         \n"
            "vadd.u16  q0, q1         \n"
            "vshrn.u16 d0, q0, #2     \n"
            "vst1.8 {d0}, [%[dst]]!   \n"
            :
            : [line1] "r"(src1), [line2] "r"(src2), [dst] "r"(dest)
            : "q0", "q1", "memory"
        );
    }
}

void shrink2x_uv_fixup(uint8_t* __restrict src1, uint8_t* __restrict src2, uint8_t* __restrict dest, int width)
{
    width >>= 4;
    while (width--)
    {
        asm (
            "pld [%[line1], #0x800]     \n"
            "pld [%[line2], #0x800]     \n"
            "vldm %[line1]!, {d0,d1}  \n"
            "vldm %[line2]!, {d2,d3}  \n"
            "vpaddl.u8 q0, q0         \n"
            "vpaddl.u8 q1, q1         \n"
            "vadd.u16  q0, q1         \n"
            "vuzp.u16  d0, d1  \n"
#ifdef NV21
            "vswp  d0, d1  \n"
#endif
            "vshrn.u16 d0, q0, #2     \n"
            "vst1.8 {d0}, [%[dst]]!   \n"
            :
            : [line1] "r"(src1), [line2] "r"(src2), [dst] "r"(dest)
            : "q0", "q1", "memory"
        );
    }
}

// do a 4x neon box filter shrink
void shrink4x_uv(uint8_t* __restrict src, int srcStride, uint8_t* __restrict dst, int dstStride, int width, int height)
{
    uint8_t line0[2048];
    uint8_t line1[2048]; // max 3.5k * 3.5k
    height >>= 2;
    while (height--) {
        shrink2x_uv(src,src+srcStride,line0,width);
        src += srcStride*2;
        shrink2x_uv(src,src+srcStride,line1,width);
        src += srcStride*2;
        shrink2x_uv_fixup(line0,line1,dst,width>>1);
        dst += dstStride;
    }
}

static void *cpu_workhandler(void *arg){
    (void) arg;
    uint8_t *filter_dataptr;

    while(1){
        pthread_mutex_lock(&cpu_mutex);
        while (cpu_start == 0)
            pthread_cond_wait(&cpu_start_cond, &cpu_mutex);
        cpu_start = 0;
        pthread_mutex_unlock(&cpu_mutex);

        // Apply 4x shrink to Y and UV planes
        filterBuf[filterCurFrame]->lock(GRALLOC_USAGE_SW_WRITE_OFTEN | GRALLOC_USAGE_SW_READ_NEVER, (void**)&filter_dataptr);
        shrink4x(raw_frame_dataptr, PREVIEW_SIZE, filter_dataptr, PREVIEW_SIZE >> 2, PREVIEW_SIZE, PREVIEW_SIZE);
        shrink4x_uv(raw_frame_dataptr + (PREVIEW_SIZE * PREVIEW_SIZE), PREVIEW_SIZE, filter_dataptr + (PREVIEW_SIZE * PREVIEW_SIZE >> 4), PREVIEW_SIZE >> 2, PREVIEW_SIZE, PREVIEW_SIZE >> 1);
        filterBuf[filterCurFrame]->unlock();

        pthread_mutex_lock(&cpu_mutex);
        cpu_done = 1;
        pthread_cond_signal(&cpu_done_cond);
        pthread_mutex_unlock(&cpu_mutex);
    }
    return NULL;
}

#endif


// App responsible for cleaning up afterwards
static float * makeMesh(int h, int v, int width, int height, int height_offset, int width_offset){
    float *f = (float *)malloc(sizeof(float) * h * v * 12);
    float h_off = (((float) height_offset) / OUTPUT_HEIGHT);
    float w_off = (((float) width_offset) / OUTPUT_WIDTH);
    float h_slope = (((float) height) / OUTPUT_HEIGHT);
    float w_slope = (((float) width) / OUTPUT_WIDTH);
    int x, y, i = 0;
    for (y = 0; y < v; y++) {
        for (x = 0; x < h; x++) {
            f[i++] = ((float) x)/h*w_slope + w_off; 	f[i++] = (((float) y)/v*h_slope ) + h_off;
            f[i++] = ((float) x+1)/h*w_slope + w_off;	f[i++] = (((float) y)/v*h_slope ) + h_off;
            f[i++] = ((float) x)/h*w_slope + w_off;	f[i++] = (((float) y+1)/v*h_slope ) + h_off;
            f[i++] = ((float) x)/h*w_slope + w_off;	f[i++] = (((float) y+1)/v*h_slope ) + h_off;
            f[i++] = ((float) x+1)/h*w_slope + w_off;	f[i++] = (((float) y)/v*h_slope ) + h_off;
            f[i++] = ((float) x+1)/h*w_slope + w_off;	f[i++] = (((float) y+1)/v*h_slope ) + h_off;
        }
    }
    return f;
}

static const char *eglStrError(unsigned err)
{
    switch(err){
        case EGL_SUCCESS: return "SUCCESS";
        case EGL_NOT_INITIALIZED: return "NOT INITIALIZED";
        case EGL_BAD_ACCESS: return "BAD ACCESS";
        case EGL_BAD_ALLOC: return "BAD ALLOC";
        case EGL_BAD_ATTRIBUTE: return "BAD_ATTRIBUTE";
        case EGL_BAD_CONFIG: return "BAD CONFIG";
        case EGL_BAD_CONTEXT: return "BAD CONTEXT";
        case EGL_BAD_CURRENT_SURFACE: return "BAD CURRENT SURFACE";
        case EGL_BAD_DISPLAY: return "BAD DISPLAY";
        case EGL_BAD_MATCH: return "BAD MATCH";
        case EGL_BAD_NATIVE_PIXMAP: return "BAD NATIVE PIXMAP";
        case EGL_BAD_NATIVE_WINDOW: return "BAD NATIVE WINDOW";
        case EGL_BAD_PARAMETER: return "BAD PARAMETER";
        case EGL_BAD_SURFACE: return "BAD_SURFACE";
        //    case EGL_CONTEXT_LOST: return "CONTEXT LOST";
        default: return "UNKNOWN";
    }
}

static void printGLString(const char *name, GLenum s) {
    const char *v = (const char *) glGetString(s);
    fprintf(stderr, "GL %s = %s\n", name, v);
}

static void checkEglError(const char* op, EGLBoolean returnVal) {
    EGLint error;
    if (returnVal != EGL_TRUE) {
        fprintf(stderr, "%s() returned %d\n", op, returnVal);
    }

    for (error = eglGetError(); error != EGL_SUCCESS; error = eglGetError()) {
        fprintf(stderr, "after %s() eglError %s (0x%x)\n", op, eglStrError(error), error);
    }
}

static void checkEglError(const char* op) {
    checkEglError(op, EGL_TRUE);
}

static void checkGlError(const char* op) {
    GLint error;
    for (error = glGetError(); error; error = glGetError()) {
        fprintf(stderr, "after %s() glError (0x%x)\n", op, error);
    }
}

static const char gVertexShader[] = 
    "attribute vec2 a_position;\n"
    "uniform float u_phase;\n"
    "uniform float u_ycompress;\n"
    "uniform float u_xcompress;\n"
    "uniform float u_yoffset;\n"
    "uniform float u_xoffset;\n"
    "uniform float u_arc;\n"
    "uniform float u_r0;\n"
    "uniform float u_r1;\n"
    "varying vec2 v_uv;\n"
    "void main() {\n"
    "	float r = u_r0 + a_position.y*(u_r1)/u_ycompress-(u_yoffset/2.0);\n"
    "	float angle = (a_position.x*u_arc/u_xcompress-(u_xoffset)) + u_phase;\n"
    "	v_uv = vec2(0.5 + r*cos(angle),0.5 + r*sin(angle));\n"
    "  	gl_Position = vec4(1.0 - a_position.x * 2.0, 1.0 - a_position.y * 2.0, 0, 1);\n"
    "}\n";

static const char gFragmentShader[] = 
    "#extension GL_OES_EGL_image_external : require\n"
    "uniform samplerExternalOES sampler;\n"
    "precision lowp float;\n"
    "varying vec2 v_uv;\n"
    "void main() {\n"
    "   gl_FragColor = texture2D(sampler, v_uv);\n"
    "}\n";

static GLuint loadShader(GLenum shaderType, const char* pSource) {
    GLuint shader = glCreateShader(shaderType);
    if (shader) {
        glShaderSource(shader, 1, &pSource, NULL);
        glCompileShader(shader);
        GLint compiled = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
        if (!compiled) {
            GLint infoLen = 0;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
            if (infoLen) {
                char* buf = (char*) malloc(infoLen);
                if (buf) {
                    glGetShaderInfoLog(shader, infoLen, NULL, buf);
                    fprintf(stderr, "Could not compile shader %d:\n%s\n",
                            shaderType, buf);
                    free(buf);
                }
            } else {
                fprintf(stderr, "Guessing at GL_INFO_LOG_LENGTH size\n");
                char* buf = (char*) malloc(0x1000);
                if (buf) {
                    glGetShaderInfoLog(shader, 0x1000, NULL, buf);
                    fprintf(stderr, "Could not compile shader %d:\n%s\n",
                            shaderType, buf);
                    free(buf);
                }
            }
            glDeleteShader(shader);
            shader = 0;
        }
    }
    return shader;
}

static GLuint createProgram(const char* pVertexSource, const char* pFragmentSource) {
    GLuint vertexShader = loadShader(GL_VERTEX_SHADER, pVertexSource);
    if (!vertexShader) {
        return 0;
    }

    GLuint pixelShader = loadShader(GL_FRAGMENT_SHADER, pFragmentSource);
    if (!pixelShader) {
        return 0;
    }

    GLuint program = glCreateProgram();
    if (program) {
        glAttachShader(program, vertexShader);
        checkGlError("glAttachShader");
        glAttachShader(program, pixelShader);
        checkGlError("glAttachShader");
        glLinkProgram(program);
        GLint linkStatus = GL_FALSE;
        glGetProgramiv(program, GL_LINK_STATUS, &linkStatus);
        if (linkStatus != GL_TRUE) {
            GLint bufLength = 0;
            glGetProgramiv(program, GL_INFO_LOG_LENGTH, &bufLength);
            if (bufLength) {
                char* buf = (char*) malloc(bufLength);
                if (buf) {
                    glGetProgramInfoLog(program, bufLength, NULL, buf);
                    fprintf(stderr, "Could not link program:\n%s\n", buf);
                    free(buf);
                }
            }
            glDeleteProgram(program);
            program = 0;
        }
    }
    return program;
}

static GLuint gProgram;
static GLint gaPositionHandle;
static GLint guYCompressHandle;
static GLint guXCompressHandle;
static GLint guYOffsetHandle;
static GLint guXOffsetHandle;
static GLint guPhaseHandle;
static GLint guR0Handle;
static GLint guR1Handle;
static GLint guArcHandle;

static float uROI1Phase = 0.0f;
static float uROI2Phase = 0.0f;
static float uStripPhase = 0.0f;
static float uYCompress = 1.0f;
static float uXCompress = 1.0f;
static float uYOffset = 0.0f;
static float uXOffset = 0.0f;

static bool setupGraphics(int w, int h) {
    gProgram = createProgram(gVertexShader, gFragmentShader);
    if (!gProgram) {
        return false;
    }
    gaPositionHandle = glGetAttribLocation(gProgram, "a_position");
    checkGlError("glGetAttribLocation");
    fprintf(stderr, "glGetAttribLocation(\"a_position\") = %d\n",
            gaPositionHandle);

    guPhaseHandle = glGetUniformLocation(gProgram, "u_phase");
    checkGlError("glGetUniformLocation");
    fprintf(stderr, "glGetUniformLocation(\"u_phase\") = %d\n",
            guPhaseHandle);

    guYOffsetHandle = glGetUniformLocation(gProgram, "u_yoffset");
    checkGlError("glGetUniformLocation");
    fprintf(stderr, "glGetUniformLocation(\"u_yoffset\") = %d\n",
            guYOffsetHandle);

    guXOffsetHandle = glGetUniformLocation(gProgram, "u_xoffset");
    checkGlError("glGetUniformLocation");
    fprintf(stderr, "glGetUniformLocation(\"u_xoffset\") = %d\n",
            guXOffsetHandle);

    guYCompressHandle = glGetUniformLocation(gProgram, "u_ycompress");
    checkGlError("glGetUniformLocation");
    fprintf(stderr, "glGetUniformLocation(\"u_ycompress\") = %d\n",
            guYCompressHandle);

    guXCompressHandle = glGetUniformLocation(gProgram, "u_xcompress");
    checkGlError("glGetUniformLocation");
    fprintf(stderr, "glGetUniformLocation(\"u_xcompress\") = %d\n",
            guXCompressHandle);

    guR0Handle = glGetUniformLocation(gProgram, "u_r0");
    checkGlError("glGetUniformLocation");
    fprintf(stderr, "glGetUniformLocation(\"u_r0\") = %d\n",
            guR0Handle);

    guR1Handle = glGetUniformLocation(gProgram, "u_r1");
    checkGlError("glGetUniformLocation");
    fprintf(stderr, "glGetUniformLocation(\"u_r1\") = %d\n",
            guR1Handle);

    guArcHandle = glGetUniformLocation(gProgram, "u_arc");
    checkGlError("glGetUniformLocation");
    fprintf(stderr, "glGetUniformLocation(\"u_arc\") = %d\n",
            guArcHandle);

    glViewport(0, 0, w, h);
    checkGlError("glViewport");

/* Strip */
    meshX = STRIP_WIDTH / MESH_DENSITY;
    meshY = STRIP_HEIGHT / MESH_DENSITY;
    mStripMeshVertices = makeMesh(meshX, meshY, STRIP_WIDTH, STRIP_HEIGHT, 0, 0);

    glGenVertexArraysOES(1, &vao_strip);
    checkGlError("glGenVertexArrays");
    glBindVertexArrayOES(vao_strip);
    checkGlError("glBindVertexArray");
    glGenBuffers(1, &vbo_strip);
    checkGlError("glGenBuffers");
    glBindBuffer(GL_ARRAY_BUFFER, vbo_strip);
    checkGlError("glBindBuffer");
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * meshX * meshY * 12, mStripMeshVertices, GL_STATIC_DRAW); 
    checkGlError("glBufferData");
    strip_mesh_count = meshX * meshY * 6;

    glVertexAttribPointer(gaPositionHandle, 2, GL_FLOAT, GL_FALSE, 0, 0);
    checkGlError("glVertexAttribPointer");
    glEnableVertexAttribArray(gaPositionHandle);
    checkGlError("glEnableVertexAttribArray");

/* ROI 1 */
    meshX = ROI_WIDTH / MESH_DENSITY;
    meshY = ROI_HEIGHT / MESH_DENSITY;
    mROI1MeshVertices = makeMesh(meshX, meshY, ROI_WIDTH, ROI_HEIGHT, STRIP_HEIGHT, ROI_WIDTH);

    glGenVertexArraysOES(1, &vao_roi1);
    checkGlError("glGenVertexArrays");
    glBindVertexArrayOES(vao_roi1);
    checkGlError("glBindVertexArray");
    glGenBuffers(1, &vbo_roi1);
    checkGlError("glGenBuffers");
    glBindBuffer(GL_ARRAY_BUFFER, vbo_roi1);
    checkGlError("glBindBuffer");
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * meshX * meshY * 12, mROI1MeshVertices, GL_STATIC_DRAW);
    checkGlError("glBufferData");
    roi1_mesh_count = meshX * meshY * 6;

    glVertexAttribPointer(gaPositionHandle, 2, GL_FLOAT, GL_FALSE, 0, 0);
    checkGlError("glVertexAttribPointer");
    glEnableVertexAttribArray(gaPositionHandle);
    checkGlError("glEnableVertexAttribArray");

/* ROI 2 */
    meshX = ROI_WIDTH / MESH_DENSITY;
    meshY = ROI_HEIGHT / MESH_DENSITY;
    mROI2MeshVertices = makeMesh(meshX, meshY, ROI_WIDTH, ROI_HEIGHT, STRIP_HEIGHT, 0);

    glGenVertexArraysOES(1, &vao_roi2);
    checkGlError("glGenVertexArrays");
    glBindVertexArrayOES(vao_roi2);
    checkGlError("glBindVertexArray");
    glGenBuffers(1, &vbo_roi2);
    checkGlError("glGenBuffers");
    glBindBuffer(GL_ARRAY_BUFFER, vbo_roi2);
    checkGlError("glBindBuffer");
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * meshX * meshY * 12, mROI2MeshVertices, GL_STATIC_DRAW);
    checkGlError("glBufferData");
    roi2_mesh_count = meshX * meshY * 6;

    glGenTextures(1, &tex_roi);
    checkGlError("glGenTextures");
    glGenTextures(1, &tex_strip);
    checkGlError("glGenTextures");

    glVertexAttribPointer(gaPositionHandle, 2, GL_FLOAT, GL_FALSE, 0, 0);
    checkGlError("glVertexAttribPointer");
    glEnableVertexAttribArray(gaPositionHandle);
    checkGlError("glEnableVertexAttribArray");

    return true;
}

#define STRIP_R0_VAL (0.25f)
#define STRIP_R1_VAL (0.20f)
#define R0_VAL (1.0f/5.0f)
#define R1_VAL (1.0f/2.0f - R0_VAL)

static void renderFrame() {
    glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
    checkGlError("glClearColor");
    glClear( GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
    checkGlError("glClear");

    glUseProgram(gProgram);
    checkGlError("glUseProgram");

/* strip */
    glUniform1f(guR0Handle, STRIP_R0_VAL);
    checkGlError("glUniform1f");
    glUniform1f(guR1Handle, STRIP_R1_VAL);
    checkGlError("glUniform1f");

    glUniform1f(guArcHandle, PI * 2.0f);
    checkGlError("glUniform1f");

    uStripPhase = 0.0f;
    uYCompress = ((float) STRIP_HEIGHT) / OUTPUT_HEIGHT;
    uXCompress = ((float) STRIP_WIDTH) / OUTPUT_WIDTH;
    uYOffset = 0.0f;
    uXOffset = 0.0f;

    glUniform1f(guPhaseHandle, uStripPhase);
    checkGlError("glUniform1f");
    glUniform1f(guYCompressHandle, uYCompress);
    checkGlError("glUniform1f");
    glUniform1f(guXCompressHandle, uXCompress);
    checkGlError("glUniform1f");
    glUniform1f(guYOffsetHandle, uYOffset);
    checkGlError("glUniform1f");
    glUniform1f(guXOffsetHandle, uXOffset);
    checkGlError("glUniform1f");
    glBindVertexArrayOES(vao_strip);

    checkGlError("glBindVertexArray");
    glBindTexture(GL_TEXTURE_EXTERNAL_OES, tex_strip);
    checkGlError("glBindTexture");
    glTexParameteri(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glDrawArrays(GL_TRIANGLES, 0, strip_mesh_count);
    checkGlError("glDrawArrays");

/* roi 1 */
    glUniform1f(guR0Handle, R0_VAL);
    checkGlError("glUniform1f");
    glUniform1f(guR1Handle, R1_VAL);
    checkGlError("glUniform1f");

    glUniform1f(guArcHandle, PI * 0.5f);
    checkGlError("glUniform1f");

    uYCompress = ((float) ROI_HEIGHT) / OUTPUT_HEIGHT;
    uXCompress = ((float) ROI_WIDTH) / OUTPUT_WIDTH;
    uYOffset = ((float) STRIP_HEIGHT) / OUTPUT_HEIGHT;
    uXOffset = PI * 0.25f;

    glUniform1f(guPhaseHandle, uROI1Phase);
    checkGlError("glUniform1f");
    glUniform1f(guYCompressHandle, uYCompress);
    checkGlError("glUniform1f");
    glUniform1f(guXCompressHandle, uXCompress);
    checkGlError("glUniform1f");
    glUniform1f(guYOffsetHandle, uYOffset);
    checkGlError("glUniform1f");
    glUniform1f(guXOffsetHandle, uXOffset);
    checkGlError("glUniform1f");

    glBindVertexArrayOES(vao_roi1);
    checkGlError("glBindVertexArray");
    glBindTexture(GL_TEXTURE_EXTERNAL_OES, tex_roi);
    checkGlError("glBindTexture");
    glTexParameteri(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glDrawArrays(GL_TRIANGLES, 0, roi1_mesh_count);
    checkGlError("glDrawArrays");

/* roi 2 */
    glUniform1f(guR0Handle, R0_VAL);
    checkGlError("glUniform1f");
    glUniform1f(guR1Handle, R1_VAL);
    checkGlError("glUniform1f");

    glUniform1f(guArcHandle, PI * 0.5f);
    checkGlError("glUniform1f");

    uYCompress = ((float) ROI_HEIGHT) / OUTPUT_HEIGHT;
    uXCompress = ((float) ROI_WIDTH) / OUTPUT_WIDTH;
    uYOffset = ((float) STRIP_HEIGHT) / OUTPUT_HEIGHT;
    uXOffset = PI * -0.25f;

    glUniform1f(guPhaseHandle, uROI2Phase);
    checkGlError("glUniform1f");
    glUniform1f(guYCompressHandle, uYCompress);
    checkGlError("glUniform1f");
    glUniform1f(guXCompressHandle, uXCompress);
    checkGlError("glUniform1f");
    glUniform1f(guYOffsetHandle, uYOffset);
    checkGlError("glUniform1f");
    glUniform1f(guXOffsetHandle, uXOffset);
    checkGlError("glUniform1f");

    glBindVertexArrayOES(vao_roi2);
    checkGlError("glBindVertexArray");
    glBindTexture(GL_TEXTURE_EXTERNAL_OES, tex_roi);
    checkGlError("glBindTexture");
    glTexParameteri(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glDrawArrays(GL_TRIANGLES, 0, roi2_mesh_count);
    checkGlError("glDrawArrays");
}

static EGLNativeWindowType window;
static int initOpenGL()
{
    EGLBoolean returnValue;
    EGLConfig myConfig = {0};

    EGLint context_attribs[] = { EGL_CONTEXT_CLIENT_VERSION, 2, EGL_NONE };
    EGLint majorVersion;
    EGLint minorVersion;
    EGLint w, h;
    EGLint numConfigs = 1, n=0;

    mEglDisplay = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    checkEglError("eglGetDisplay");
    if (mEglDisplay == EGL_NO_DISPLAY) {
        printf("eglGetDisplay returned EGL_NO_DISPLAY.\n");
        return 1;
    }

    returnValue = eglInitialize(mEglDisplay, &majorVersion, &minorVersion);
    checkEglError("eglInitialize", returnValue);
    fprintf(stderr, "EGL version %d.%d\n", majorVersion, minorVersion);
    if (returnValue != EGL_TRUE) {
        printf("eglInitialize failed\n");
        return 1;
    }

    /* Setup EGL for Codec buffers */
    EGLint configAttribs[] = {
            EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
            EGL_RED_SIZE, 8,
            EGL_GREEN_SIZE, 8,
            EGL_BLUE_SIZE, 8,
            EGL_ALPHA_SIZE, 8,
            EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
            EGL_RECORDABLE_ANDROID, 1,
            EGL_NONE };

    EGLConfig* const configs = (EGLConfig*)malloc(sizeof(EGLConfig)*numConfigs);
    if (eglChooseConfig(mEglDisplay, configAttribs, configs, numConfigs, &n) == EGL_FALSE) {
        free(configs);
        return 1;
    }

    myConfig = configs[0];
    free(configs);

    mEglContext = eglCreateContext(mEglDisplay, myConfig, EGL_NO_CONTEXT, context_attribs);
    checkEglError("eglCreateContext");
    if (mEglContext == EGL_NO_CONTEXT) {
        printf("eglCreateContext failed\n");
        return 1;
    }

    window = getCodecNW();
    mEglSurface = eglCreateWindowSurface(mEglDisplay, myConfig, window, NULL);
    checkEglError("eglCreateWindowSurface");
    if (mEglSurface == EGL_NO_SURFACE) {
        printf("eglCreateWindowSurface failed.\n");
        return 1;
    }

    returnValue = eglMakeCurrent(mEglDisplay, mEglSurface, mEglSurface, mEglContext);
    checkEglError("eglMakeCurrent", returnValue);
    if (returnValue != EGL_TRUE) {
        return 1;
    }

    eglQuerySurface(mEglDisplay, mEglSurface, EGL_WIDTH, &w);
    checkEglError("eglQuerySurface");
    eglQuerySurface(mEglDisplay, mEglSurface, EGL_HEIGHT, &h);
    checkEglError("eglQuerySurface");
    GLint dim = w < h ? w : h;

    fprintf(stderr, "Window dimensions: %d x %d\n", w, h);

    printGLString("Version", GL_VERSION);
    printGLString("Vendor", GL_VENDOR);
    printGLString("Renderer", GL_RENDERER);
    printGLString("Extensions", GL_EXTENSIONS);

    if(!setupGraphics(OUTPUT_WIDTH, OUTPUT_HEIGHT)) {
        fprintf(stderr, "Could not set up graphics.\n");
        return 1;
    }
    return 0;
}

static int stop = 0;
static int initialized = 0;
static int frame_counter = 0;
static long long usec_tmp;
static struct timeval ptv, ctv;
void GraphicsCameraFrameCallback(void *eglClientBuffer)
{
    int i, ret;

    if (stop == 1) {
        return;
    }
    if (initialized == 0){
        ret = initOpenGL();
        initialized = 1;
        gettimeofday(&ptv, NULL);
    }

#ifdef ENABLE_CPU_FILTER
    const Rect rect(PREVIEW_SIZE, PREVIEW_SIZE);
    ANativeWindowBuffer *anb = (ANativeWindowBuffer *)eglClientBuffer;
    GraphicBufferMapper::get().lock(anb->handle, GRALLOC_USAGE_SW_READ_OFTEN | GRALLOC_USAGE_SW_WRITE_NEVER, rect, (void**)&raw_frame_dataptr);

    filterRenderFrame = filterCurFrame;
    filterCurFrame = (filterCurFrame + 1) % FILTER_BUF_COUNT;
    pthread_mutex_lock(&cpu_mutex);
    cpu_start = 1;
    pthread_cond_signal(&cpu_start_cond);
    pthread_mutex_unlock(&cpu_mutex);
#endif

    EGLImageKHR full_img = eglCreateImageKHR(mEglDisplay, EGL_NO_CONTEXT, EGL_NATIVE_BUFFER_ANDROID, (EGLClientBuffer) eglClientBuffer, 0);
    checkEglError("eglCreateImageKHR");
    if (full_img == EGL_NO_IMAGE_KHR) {
        return;
    }

    glBindTexture(GL_TEXTURE_EXTERNAL_OES, tex_roi);
    checkGlError("glBindTexture");
    glEGLImageTargetTexture2DOES(GL_TEXTURE_EXTERNAL_OES, (GLeglImageOES)full_img);
    checkGlError("glEGLImageTargetTexture2DOES");

#ifdef ENABLE_CPU_FILTER
    EGLImageKHR filter_img = eglCreateImageKHR(mEglDisplay, EGL_NO_CONTEXT, EGL_NATIVE_BUFFER_ANDROID, (EGLClientBuffer) filterBuf[filterRenderFrame]->getNativeBuffer(), 0);
    checkEglError("eglCreateImageKHR");
    if (filter_img == EGL_NO_IMAGE_KHR) {
        return;
    }

    glBindTexture(GL_TEXTURE_EXTERNAL_OES, tex_strip);
    checkGlError("glBindTexture");
    glEGLImageTargetTexture2DOES(GL_TEXTURE_EXTERNAL_OES, (GLeglImageOES)filter_img);
    checkGlError("glEGLImageTargetTexture2DOES");
#else
    tex_strip = tex_roi;
#endif

    // Set the next phase angle for ROI 1 and 2
    uROI1Phase = current_roi[0].current_phase;
    uROI2Phase = current_roi[1].current_phase;
    renderFrame();

    // Use the time for render to get the next direction command
    if (directorCB) {
        current_command = directorCB();
        if (last_command_id != current_command.command_id) {
            last_command_id = current_command.command_id;
            for (i = 0; i < MAX_ROI; i++) {
                if (current_command.command[i].command_valid) {
                    current_roi[i].target_phase = current_command.command[i].target_phase;
                    current_roi[i].target_frame_count = current_command.command[i].target_frame_count;
                    current_roi[i].current_frame_count = 0;
                    current_roi[i].command_source = current_command.command[i].command_source;
                    current_roi[i].phase_delta = (current_roi[i].target_phase - current_roi[i].current_phase) / current_roi[i].target_frame_count;
                }
            }
        }
    }

    // Process the director command for next cycle
    // For now, just linearly pan across
    for (i = 0; i < MAX_ROI; i++) {
        int frame_diff = current_roi[i].target_frame_count - current_roi[i].current_frame_count;
        // Only process if we differ in the frame_count
        if (frame_diff >= 0) {
            current_roi[i].current_phase += current_roi[i].phase_delta;
            current_roi[i].current_frame_count++;
        }
    }

    glFinish();
    checkGlError("glFinish");

    eglPresentationTimeANDROID(mEglDisplay, mEglSurface, systemTime(CLOCK_MONOTONIC));
    checkEglError("eglPresentationTimeANDROID");

    TIMELOG_LOG(gfx);

    eglSwapBuffers(mEglDisplay, mEglSurface);
    checkEglError("eglSwapBuffers");

    eglDestroyImageKHR(mEglDisplay, full_img);
    checkEglError("eglDestroyImageKHR");

#ifdef ENABLE_CPU_FILTER
    eglDestroyImageKHR(mEglDisplay, filter_img);
    checkEglError("eglDestroyImageKHR");

    pthread_mutex_lock(&cpu_mutex);
    while (cpu_done == 0)
        pthread_cond_wait(&cpu_done_cond, &cpu_mutex);
    cpu_done = 0;
    pthread_mutex_unlock(&cpu_mutex);

    GraphicBufferMapper::get().unlock(anb->handle);
#endif

    if ( frame_counter == FPS_COUNT_INTERVAL ){
        gettimeofday(&ctv, NULL);
        usec_tmp = ctv.tv_usec - ptv.tv_usec + (ctv.tv_sec - ptv.tv_sec) * 1000000;
        fprintf(stdout, "Elapsed: %lld FPS: %f\n", usec_tmp, FPS_COUNT_INTERVAL * 1000000.0f / usec_tmp);
        ptv = ctv;
        frame_counter = 0;
    }
    frame_counter++;
}

int setGraphicsDirector(directorCallback cb)
{
    directorCB = cb;
    return 0;
}

int registerGraphicsNativeWindowCallback(codecNativeWindowFunc nwFunc)
{
    getCodecNW = nwFunc;
    return 0;
}

int initGraphics()
{
    int i;
    stop = 0;
    directorCB = NULL;
    getCodecNW = NULL;

    for (i = 0; i < MAX_ROI; i++) {
        current_roi[i].target_phase = 0.0f;
        current_roi[i].current_phase = 0.0f;
        current_roi[i].phase_delta = 0.0f;
        current_roi[i].target_frame_count = 0;
        current_roi[i].current_frame_count = 0;
        current_roi[i].command_source = CMD_SRC_NONE;
    }


#ifdef ENABLE_CPU_FILTER
    int rc;
    rc = pthread_create(&cpu_workthread, NULL, cpu_workhandler, NULL);
    if (rc) {
        fprintf(stderr, "Could not start cpu workthread\n");
        return 1;
    }
    else {
        pthread_setname_np(cpu_workthread, "cpu_workhandler");
    }

    for (int i = 0; i < FILTER_BUF_COUNT; i++){
        filterBuf[i] = new android::GraphicBuffer(PREVIEW_SIZE>>2, PREVIEW_SIZE>>2, FILTER_PIX_FORMAT, GRALLOC_USAGE_SW_READ_NEVER | GRALLOC_USAGE_SW_WRITE_OFTEN | GRALLOC_USAGE_HW_TEXTURE);
        if (filterBuf[i] == NULL) {
            fprintf(stderr, "Could not allocate CPU box filter buffer\n");
            return 1;
        }
    }
    filterCurFrame = 0;
    filterRenderFrame = 1;
#endif

    return 0;
}

int startGraphics()
{
    // Graphics Initialization happens when callback is called the first time
    if (getCodecNW == NULL) {
        fprintf(stderr, "Codec Native Window required for OpenGL output\n");
        return 1;
    }
    return 0;
}

int stopGraphics()
{
    stop = 1;
    if (mROI1MeshVertices)
        free(mROI1MeshVertices);
    if (mROI2MeshVertices)
        free(mROI2MeshVertices);
    if (mStripMeshVertices)
        free(mStripMeshVertices);
#ifdef ENABLE_CPU_FILTER
    pthread_kill(cpu_workthread, SIGABRT);
#endif
    TIMELOG_END(gfx);
    return 0;
}
