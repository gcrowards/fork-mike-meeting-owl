#ifndef CAMERAHANDLER_H
#define CAMERAHANDLER_H

/* Camera Abstraction Library
   Copyright Playground Global 2016 */
#include <EGL/egl.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Two supported Camera Resolutions */
enum Resolution {
	RES_3456x3456,
	RES_1728x1728,
};

/** Structure used for defining new camera parameter values. */
typedef struct cameraParams_s {
  const char * const pKey;    /**< Parameter key identifier */
  const char * const pValue;  /**< Value to assign to the key */
} cameraParams_t;

/* Callback when frame is available
   Note: Called in a different thread! */
typedef void (*frameAvailableCB)(void *eglClientBuffer);

/* Returns 0 on success, < 0 on error */

/* Get a valid NativeWindow */
int getNativeWindowConfig(EGLDisplay dpy, EGLNativeWindowType *window, EGLConfig *config);

/* Initialize the camera with resolution */
int initCamera(enum Resolution res);

/* Register a callback that should be called with received frame */
int registerCameraCallback(frameAvailableCB);

/* Start the Camera Frame Capture */
int startCamera();

/* Stop the Camera Frame Capture */
int stopCamera();

/* Set camera control parameters */
bool setCameraParameters(cameraParams_t paramList[], size_t numParams);

/* Retrieve value of one camera control parameter */
char *getCameraParameter(const char *paramKey);

#ifdef __cplusplus
}
#endif

#endif
