/* LED Abstraction Library
   Copyright Owl Labs, Inc. 2016 */

#ifndef LEDHANDLER_H
#define LEDHANDLER_H

/* Application-usable color masks */
#define LED_HDL_WHITE		(1 << 0)
#define LED_HDL_BLUE		(1 << 1)
#define LED_HDL_RED		(1 << 2)
#define LED_HDL_ALL		(LED_HDL_WHITE | LED_HDL_BLUE | LED_HDL_RED)

#ifdef __cplusplus
extern "C" {
#endif

int initLeds();
int ledEnable(int colorMask);
int ledDisable(int colorMask);

#ifdef __cplusplus
}
#endif

#endif	/* LEDHANDLER_H */
