#include <stdio.h>
#include <string.h>

#include <cmath>

#include "DirectorCommands.h"
#include "gpu.hpp"
#include "GpuSupport.h"


// TODO: get params from owlcore/inc/params.h
#define OUTPUT_WIDTH 1280
#define OUTPUT_HEIGHT 720
#define CV_PANO_RES_X          1280 /**< computer vision panorama size. */
#define CV_PANO_RES_Y           161 /**< computer vision panorama size. */


#define PREVIEW_SIZE 1728

#define STRIP_WIDTH OUTPUT_WIDTH
#define STRIP_HEIGHT 160

// fake the logging macros used by owl-core
#define LOGI   printf
#define LOGW   printf
#define LOGE   printf

// render constants hard-coded in the fragment shader and used by
// symbolic name in the C++ code below; be sure to update the fragment
// shader if any of these change
#define RENDER_UNROLL                0
#define RENDER_LOGO                  1
#define RENDER_RGBA_OVERLAY          2
#define RENDER_SHAPE                 3
#define RENDER_SHAPE_ELLIPSE         4


cGpu::cGpu(codecNativeWindowFnType nativeWindowFn,
	   owlRenderCommandsCallback cb)
{
  // store the external function pointers for later use
  mGetNativeWindowFn = nativeWindowFn;
  mOwlRenderCommandsCb = cb;
}


cGpu::~cGpu()
{
  finish();
}


// ----------
// shaders
const GLchar *gpVertexShaderStr =
  "attribute vec2 a_position;\n"
  "uniform   bool  u_flipped;\n"
  "uniform   float u_x0;\n"
  "uniform   float u_x1;\n"
  "uniform   float u_y0;\n"
  "uniform   float u_y1;\n"
  "varying   vec2  v_texPos;\n"
  "void main() {\n"
  "   float x = u_x0 + a_position.x * (u_x1 - u_x0);\n"
  "   float y = u_y0 + a_position.y * (u_y1 - u_y0);\n"
  "   v_texPos = vec2(a_position.x, a_position.y);\n"
  "   vec2 clipPos = 2.0*vec2(x, y) - vec2(1.0, 1.0);\n"
  "   if (!u_flipped)\n"
  "   { // properly orient screen preview\n"
  "     clipPos.y = -clipPos.y;\n"
  "   }\n"
  "   gl_Position = vec4(clipPos, 0.0, 1.0);\n"
  "}\n";

const GLchar *gpFragmentShaderStr =
#if defined(__ANDROID__) && !defined(MOCK_INPUT)
  "#extension GL_OES_EGL_image_external : require\n"
  "uniform samplerExternalOES cameraSampler;\n"
#else // linux or MOCK_INPUT
  "uniform sampler2D cameraSampler;\n"
#endif
  "uniform sampler2D logoSampler;\n"
  "uniform sampler2D rgbaOverlaySampler;\n"
  "precision highp float;\n"
  "uniform   int   u_render;\n"
  "uniform   vec3  u_color;\n"
  "uniform   float u_alpha;\n"
  "uniform   float u_r0;\n"
  "uniform   float u_r1;\n"
  "uniform   float u_cx;\n"
  "uniform   float u_cy;\n"
  "uniform   float u_theta0;\n"
  "uniform   float u_theta1;\n"
  "uniform   float u_stretch;\n"
  "uniform   float u_bend;\n"
  "varying   vec2 v_texPos;\n"
  "void main() {\n"
  "   float r, rnorm;\n"
  "   float theta;\n"
  "   vec2 pos;\n"
  "   vec4 color;\n"
  "   if (u_render == 0)\n"
  "   { // ===== RENDER_UNROLL =====\n"
  "     rnorm = v_texPos.y;\n"
  "     rnorm = rnorm - u_stretch * (1.0 - rnorm) * rnorm;\n"
  "     rnorm = rnorm + u_bend * (1.0 - rnorm) * pow(v_texPos.x - 0.5, 2.);\n"
  "     r =  clamp(u_r0 + rnorm * (u_r1 - u_r0), 0.0, 0.5);\n"
  "     theta = -u_theta0 - v_texPos.x * (u_theta1 - u_theta0);\n"
  "     pos = vec2(u_cx + r*cos(theta), u_cy + r*sin(theta));\n"
  "     color = texture2D(cameraSampler, pos);\n"
  "     color.a = u_alpha;\n"
  "   }\n"
  "   else if (u_render == 1)\n"
  "   { // ===== RENDER_LOGO =====\n"
  "     color = texture2D(logoSampler, v_texPos);\n"
  "     color.a = u_alpha * color.a;\n"
  "   }\n"
  "   else if (u_render == 2)\n"
  "   { // ===== RENDER_RGBA_OVERLAY =====\n"
  "     color = texture2D(rgbaOverlaySampler, v_texPos);\n"
  "     color.a = u_alpha * color.a;\n"
  "   }\n"
  "   else if (u_render >= 3)\n"
  "   { // ===== RENDER_SHAPE =====\n"
  "     color = vec4(u_color, u_alpha);\n"
  "     if (u_render == 4)\n"
  "     { // ----- RENDER_SHAPE_ELLIPSE -----\n"
  "       pos = v_texPos - vec2(0.5, 0.5);\n"
  "       r = length(pos);\n"
  "       if (r > 0.5 || r < u_r0) color.a = 0.0;\n"
  "     }\n"
  "   }\n"
  "   gl_FragColor = color;\n"
  "}\n";

// NOTE 1: Above, samplerExternalOES is needed for camera-based input
// textures on android; the usual sampler2D is used on linux

// NOTE 2: When unrolling, rnorm is used to control the vertical
// warping as the output image y-coordinate ranges from 0 to 1. The
// three equations above implement, respectively, a linear mapping
// with no extra warping, a vertical expansion that favors the top
// half of the output image, and a bending that increases towards the
// top of the output image and with greater horizontal deviation from
// the middle of the output image. Different equations could be used
// to add some nuance to how the warping is done, but the ones above
// are relatively simple and capture the main qualities of the warping
// with just two parameters to adjust.

// NOTE 3: The fragment shader above is a monolithic program with a
// series of if-else statements. Consider separating this into
// multiple programs if performance is a concern. Testing with the
// Adreno profiler showed no noticeable improvement when the if-else
// statements were removed and only the panoramic strip was rendered.

// ----------


int  cGpu::checkEglError(const char *pMsg)
{
  EGLint err = eglGetError();
  if (err != EGL_SUCCESS)
    { LOGE("EGL ERROR 0x%04x: %s\n", err, pMsg); }
  return err;
}

int  cGpu::checkGlError(const char *pMsg)
{
  EGLint err = glGetError();
  if (err != GL_NO_ERROR)
    { LOGE("GL ERROR 0x%04x: %s\n", err, pMsg); }
  return err;
}

void cGpu::createProgram(void)
{
  GLuint fShader, vShader, program;

  // load shaders
  vShader = loadShader(GL_VERTEX_SHADER,   gpVertexShaderStr);
  fShader = loadShader(GL_FRAGMENT_SHADER, gpFragmentShaderStr);

  // create the program with shaders
  mProgram = glCreateProgram();
  checkGlError("glCreateProgram");
  glAttachShader(mProgram, fShader);
  checkGlError("glAttachShader (fragment)");
  glAttachShader(mProgram, vShader);
  checkGlError("glAttachShader (vertex)");

  // link and use the program
  glLinkProgram(mProgram);
  checkGlError("glLinkProgram");
  glUseProgram(mProgram);
  checkGlError("glUseProgram");

  // detach and delete shaders
  glDetachShader(mProgram, vShader);
  checkGlError("glDetachShader vShader");
  glDetachShader(mProgram, fShader);
  checkGlError("glDetachShader fShader");
  glDeleteShader(vShader);
  checkGlError("glDeleteShader vShader");
  glDeleteShader(fShader);
  checkGlError("glDeleteShader fShader");
}

void cGpu::finish(void)
{
  free(mpVertexes);

  // release EGL resources
  eglMakeCurrent(mDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
  checkEglError("eglMakeCurrent");
  eglDestroyContext(mDisplay, mContext);
  checkEglError("eglDestroyContext");
  eglDestroySurface(mDisplay, mSurface);
  checkEglError("eglDestroySurface");
  eglTerminate(mDisplay);
  checkEglError("eglTerminate");
}

int  cGpu::loadShader(int shaderType, const GLchar *pShaderStr)
{
  GLint  status;
  GLuint shader;

  shader = glCreateShader(shaderType);
  glShaderSource(shader, 1, &pShaderStr, NULL);
  glCompileShader(shader);
  checkGlError("glCompileShader");
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  if (!status)
    {
      switch (shaderType)
	{
	case GL_VERTEX_SHADER:
	  LOGE("Error: unable to compile vertex shader\n");
	  break;
	case GL_FRAGMENT_SHADER:
	  LOGE("Error: unable to compile fragment shader\n");
	  break;
	}
    }
  return shader;
}

void cGpu::prepareAttributes(void)
{
  // set attributes
  glBindAttribLocation(mProgram, 0, "a_position");
  checkGlError("glBindAttribLocation");
}

void cGpu::prepareCvPanoFbo(void)
{
  // create texture for panoramic strip
  glGenTextures(1, &mCvPanoFboTexId);
  glBindTexture(GL_TEXTURE_2D, mCvPanoFboTexId);
  checkGlError("glBindTexture CvPanoFbo");
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  checkGlError("glTexParameter CvPanoFbo");
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  checkGlError("glTexParameter CvPanoFbo");

#ifdef __ANDROID__
  // get a native buffer
  EGLClientBuffer buffer = NULL;
  // MTR EGLClientBuffer buffer =                                               \
  // MTR  (EGLClientBuffer) grBufGetNativeBuffer(CV_PANO_RES_X, CV_PANO_RES_Y);
  // map a KHR image to the buffer
  EGLint pAttrs[] = {
    EGL_WIDTH,               CV_PANO_RES_X,
    EGL_HEIGHT,              CV_PANO_RES_Y,
    EGL_MATCH_FORMAT_KHR,    EGL_FORMAT_RGBA_8888_KHR,
    EGL_IMAGE_PRESERVED_KHR, EGL_TRUE,
        EGL_NONE
  };
  EGLImageKHR image = eglCreateImageKHR(eglGetCurrentDisplay(),
					EGL_NO_CONTEXT,
					EGL_NATIVE_BUFFER_ANDROID,
					buffer, pAttrs);
  checkEglError("eglCreateImageKHR");
  // bind image to texture
  glEGLImageTargetTexture2DOES(GL_TEXTURE_2D, image);
  checkGlError("glEGLImageTargetTexture2DOES");
#else
  // set up the texture images
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, CV_PANO_RES_X, CV_PANO_RES_Y, 0,
	       GL_RGBA, GL_UNSIGNED_BYTE, 0);
#endif

  // create framebuffer objects
  glGenFramebuffers(1, &mCvPanoFbo);
  glBindFramebuffer(GL_FRAMEBUFFER, mCvPanoFbo);

  // attach texture to FBO
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
			 mCvPanoFboTexId, 0);

  // check FBO status
  GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if(status != GL_FRAMEBUFFER_COMPLETE)
    {LOGE("GL ERROR 0x%04x: %s\n", status,
	  "glCheckFramebufferStatus CvPano");}

  // restore defaults
  glBindTexture(GL_TEXTURE_2D, 0);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void cGpu::prepareEgl(void)
{
    EGLint major, minor;
    EGLint numConfigs;
    EGLConfig config;
    EGLint width, height;
    EGLint format;

    // open connection to the display
    mDisplay = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    checkEglError("eglGetDisplay");
    
    // initialize display connection
    eglInitialize(mDisplay, &major, &minor);
    checkEglError("eglInitialize");
    LOGI("Display initialized, version %d.%d\n", major, minor);

    // find a suitable configuration
    EGLint  pConfigAttrs [] =
      {
	EGL_RENDERABLE_TYPE,       EGL_OPENGL_ES2_BIT,
	EGL_SURFACE_TYPE,          EGL_WINDOW_BIT,
	EGL_RED_SIZE,              8,
	EGL_GREEN_SIZE,            8,
	EGL_BLUE_SIZE,             8,
	EGL_ALPHA_SIZE,            8,
#if defined(__ANDROID__)
	EGL_RECORDABLE_ANDROID,    1,
#endif
	EGL_NONE
      };
    eglChooseConfig(mDisplay, pConfigAttrs, &config, 1, &numConfigs);
    checkEglError("eglChooseConfig");
      
    // get native window 
    EGLNativeWindowType nativeWindow;
    nativeWindow = mGetNativeWindowFn();

    // create surface from native window
    mSurface = eglCreateWindowSurface(mDisplay, config, nativeWindow, NULL);
    checkEglError("eglCreateWindowSurface");

    // create rendering context
    EGLint  pContextAttrs [] =
      {
	EGL_CONTEXT_CLIENT_VERSION, 2,
	EGL_NONE,
      };
    mContext = eglCreateContext(mDisplay, config, EGL_NO_CONTEXT, pContextAttrs);
    checkEglError("eglCreateContext");
    eglMakeCurrent(mDisplay, mSurface, mSurface, mContext);
    checkEglError("eglMakeCurrent");

    // check surface size
    eglQuerySurface(mDisplay, mSurface, EGL_WIDTH, &width);
    eglQuerySurface(mDisplay, mSurface, EGL_HEIGHT, &height);
    checkEglError("eglQuerySurface width/height");
    LOGI("Created %dx%d surface\n", width, height);
}

void cGpu::prepareTextures(void)
{
  // create camera image texture and set properties
  glGenTextures(1, &mCameraTexId);
#if defined(__ANDROID__) && !defined(MOCK_INPUT)
  glBindTexture(GL_TEXTURE_EXTERNAL_OES, mCameraTexId);
  checkGlError("glBindTexture camera");
  glTexParameterf(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  checkGlError("glTexParameterf");
  glTexParameterf(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  checkGlError("glTexParameterf");
#else // linux or MOCK_INPUT
  glBindTexture(GL_TEXTURE_2D, mCameraTexId);
  checkGlError("glBindTexture camera");
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  checkGlError("glTexParameterf");
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  checkGlError("glTexParameterf");
#endif

  // create logo texture and set properties
  glGenTextures(1, &mLogoTexId);
  glBindTexture(GL_TEXTURE_2D, mLogoTexId);
  checkGlError("glBindTexture logo");
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  checkGlError("glTexParameterf");
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  checkGlError("glTexParameterf");

  // create image overlay texture and set properties
  glGenTextures(1, &mRgbaOverlayTexId);
  glBindTexture(GL_TEXTURE_2D, mRgbaOverlayTexId);
  checkGlError("glBindTexture image overlay");
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  checkGlError("glTexParameterf");
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  checkGlError("glTexParameterf");

  // assign textures to texture units so we can use more than one sampler
  glActiveTexture(GL_TEXTURE0); // camera uses texture unit 0
#if defined(__ANDROID__) && !defined(MOCK_INPUT)
  glBindTexture(GL_TEXTURE_EXTERNAL_OES, mCameraTexId);
#else // linux or MOCK_INPUT
  glBindTexture(GL_TEXTURE_2D, mCameraTexId);
#endif
  glActiveTexture(GL_TEXTURE1); // logo uses texture unit 1
  glBindTexture(GL_TEXTURE_2D, mLogoTexId);
  glActiveTexture(GL_TEXTURE2); // general-purpose overlay uses texture unit 2
  glBindTexture(GL_TEXTURE_2D, mRgbaOverlayTexId);
  glActiveTexture(GL_TEXTURE3); // CvPano FBO uses texture unit 3
  // must call prepareCvPanoFbo before next line
  glBindTexture(GL_TEXTURE_2D, mCvPanoFboTexId);

  // restore default
  glActiveTexture(GL_TEXTURE0);
}

void cGpu::prepareUniforms(void)
{
  // set up uniforms with reasonable initial values to be overridden later
  GLuint u;
  u = glGetUniformLocation(mProgram, "u_render");
  glUniform1i(u, 0);
  u = glGetUniformLocation(mProgram, "u_flipped");
  glUniform1i(u, 0);
  u = glGetUniformLocation(mProgram, "u_r0");
  glUniform1f(u, 0.25);
  u = glGetUniformLocation(mProgram, "u_r1");
  glUniform1f(u, 0.50);
  u = glGetUniformLocation(mProgram, "u_cx");
  glUniform1f(u, 0.50);
  u = glGetUniformLocation(mProgram, "u_cy");
  glUniform1f(u, 0.50);
  u = glGetUniformLocation(mProgram, "u_theta0");
  glUniform1f(u, 0.0);
  u = glGetUniformLocation(mProgram, "u_theta1");
  glUniform1f(u, M_PI * 2.0);
  u = glGetUniformLocation(mProgram, "u_x0");
  glUniform1f(u, 0.0);
  u = glGetUniformLocation(mProgram, "u_x1");
  glUniform1f(u, 1.0);
  u = glGetUniformLocation(mProgram, "u_y0");
  glUniform1f(u, 0.0);
  u = glGetUniformLocation(mProgram, "u_y1");
  glUniform1f(u, 1.0);
  u = glGetUniformLocation(mProgram, "u_color");
  glUniform3f(u, 0.0, 0.0, 0.0);
  u = glGetUniformLocation(mProgram, "u_alpha");
  glUniform1f(u, 1.0);
  u = glGetUniformLocation(mProgram, "u_stretch");
  glUniform1f(u, 0.0);
  u = glGetUniformLocation(mProgram, "u_bend");
  glUniform1f(u, 0.0);

  checkGlError("uniforms");

  // assign samplers to texture units
  u = glGetUniformLocation(mProgram, "cameraSampler");
  glUniform1i(u, 0);
  u = glGetUniformLocation(mProgram, "logoSampler");
  glUniform1i(u, 1);
  u = glGetUniformLocation(mProgram, "rgbaOverlaySampler");
  glUniform1i(u, 1);
  checkGlError("uniforms samplers");
}

void cGpu::prepareVertexes(void)
{
  GLuint i, nVertexes, nBytes, pVboIds[1];

  // set number of vertexes
  // we need 6 vertexes for a two-triangle quad, plus
  // 4 more vertexes for rendering a rectangle, plus
  // 2 more vertexes for rendering an arbitrary line
  nVertexes = 6 + 4 + 2;

  // allocate vertexes, accounting for 2 coordinates per vertex
  mpVertexes = (GLfloat *)  malloc(nVertexes * 2 * sizeof(GLfloat));
  if (mpVertexes == NULL)
    { LOGE("unable to allocate quads\n"); }

  // init the array index
  i = 0;

  // construct vertexes for the quad
  mpVertexes[i++] = 0; mpVertexes[i++] = 0;
  mpVertexes[i++] = 1; mpVertexes[i++] = 0;
  mpVertexes[i++] = 0; mpVertexes[i++] = 1;
  mpVertexes[i++] = 0; mpVertexes[i++] = 1;
  mpVertexes[i++] = 1; mpVertexes[i++] = 0;
  mpVertexes[i++] = 1; mpVertexes[i++] = 1;

  // construct vertexes for the rectangle
  mpVertexes[i++] = 0; mpVertexes[i++] = 0;
  mpVertexes[i++] = 1; mpVertexes[i++] = 0;
  mpVertexes[i++] = 1; mpVertexes[i++] = 1;
  mpVertexes[i++] = 0; mpVertexes[i++] = 1;

  // construct vertexes for the line
  mpVertexes[i++] = 0; mpVertexes[i++] = 0;
  mpVertexes[i++] = 1; mpVertexes[i++] = 1;

  // set up the vertex buffer
  glGenBuffers(1, pVboIds);
  glBindBuffer(GL_ARRAY_BUFFER, pVboIds[0]);
  nBytes = nVertexes * 2 * sizeof(GLfloat); // two coords per vertex
  glBufferData(GL_ARRAY_BUFFER, nBytes, mpVertexes, GL_STATIC_DRAW);
  checkGlError("VBO");

  // enable the vertex attribute array and set the pointer
  GLint a_position;
  a_position = glGetAttribLocation(mProgram, "a_position");
  checkGlError("glGetAttribLocation");
  glEnableVertexAttribArray(a_position);
  checkGlError("glEnableVertexAttribArray");
  glVertexAttribPointer(a_position, 2, GL_FLOAT, GL_FALSE, 0, 0);
  checkGlError("glVertexAttribPointer");
}

void cGpu::prepareViewport(void)
{
  // set viewport, check it, and store width and height
  int    pCoords[4];
  glViewport(0, 0, OUTPUT_WIDTH, OUTPUT_HEIGHT);
  checkGlError("glViewport");
  glGetIntegerv(GL_VIEWPORT, pCoords);
  LOGI("Viewport Coords: %d, %d, %d, %d\n",
       pCoords[0], pCoords[1], pCoords[2], pCoords[3]);
  mOutWidth = pCoords[2];
  mOutHeight = pCoords[3];
}

void cGpu::renderFrame()
{
  render_command *pRenderCmds;
  panel_command  p;
  shape_command  s;

  // clear frame
  glClear(GL_COLOR_BUFFER_BIT);
  checkGlError("glClear");

  // get the render commands from owl-core
  pRenderCmds = mOwlRenderCommandsCb();

  // iterate until there's a "none" command
  for (int i=0; i<MAX_RENDER_COMMANDS; i++)
    {
      if (pRenderCmds[i].renderType == CMD_RENDER_NONE)
	{ break; }

      // extract components with short variable names for convenience
      p = pRenderCmds[i].panel;
      s = pRenderCmds[i].shape;

      // process command
      switch (pRenderCmds[i].renderType)
	{
	case CMD_RENDER_PANEL:
	  renderUnroll(p.left, p.top, p.width, p.height,
		       p.theta0, p.theta1, p.r0, p.r1,
		       p.alpha, p.stretch, p.bend);
	  break;

	case CMD_RENDER_ELLIPSE:
	  break;
	  
	case CMD_RENDER_LINE:
	  break;
	  
	case CMD_RENDER_RECTANGLE:
	  renderRectangle(s.left, s.top, s.width, s.height,
			  s.r, s.g, s.b, s.alpha, s.lineWidth, s.filled);
	  break;
	  
	case CMD_RENDER_NONE:
	default:
	  // shouldn't get here
	  break;
	}
    }
}

void cGpu::setBackgroundColor(void)
{
  const float gray = 0.0;
  glClearColor(gray, gray, gray, 1.0);
}

void cGpu::setBlending(void)
{
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void cGpu::setupGraphics(void)
{
  prepareEgl();
  setBackgroundColor();
  setBlending();
  prepareCvPanoFbo();   // must call before prepareTextures()
  prepareViewport();
  prepareTextures();
  createProgram();
  prepareAttributes();
  prepareUniforms();
  prepareVertexes();
}

void cGpu::update(void *pEglClientBuffer)
{
    int i, ret;
    static bool init=true;
    
    // wait for camera frame callback to trigger the first update
    // before setting up the graphics system
    if (init)
      {
	setupGraphics();
	init = false;
      }

    // ensure default framebuffer is active
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // transfer camera image to texture
    EGLImageKHR img = eglCreateImageKHR(mDisplay, EGL_NO_CONTEXT, EGL_NATIVE_BUFFER_ANDROID,
					(EGLClientBuffer) pEglClientBuffer, 0);
    checkEglError("eglCreateImageKHR");
    glEGLImageTargetTexture2DOES(GL_TEXTURE_EXTERNAL_OES, (GLeglImageOES)img);
    checkGlError("glEGLImageTargetTexture2DOES");
    
    /*
EGLImageKHR full_img = eglCreateImageKHR(mDisplay, EGL_NO_CONTEXT, EGL_NATIVE_BUFFER_ANDROID, (EGLClientBuffer) eglClientBuffer, 0);
    checkEglError("eglCreateImageKHR");
    if (full_img == EGL_NO_IMAGE_KHR) {
        return;
    }

    glEGLImageTargetTexture2DOES(GL_TEXTURE_EXTERNAL_OES, (GLeglImageOES)full_img);
    checkGlError("glEGLImageTargetTexture2DOES");
    */
    
    renderFrame();


    // finish the rendering and swap buffers
    glFinish();
    checkGlError("glFinish");
#ifdef __ANDROID__    
    setAndroidPresentationTime(mDisplay, mSurface);
    checkEglError("eglPresentationTimeANDROID");
#endif
    eglSwapBuffers(mDisplay, mSurface);
    checkEglError("eglSwapBuffers");

    // clean up camera buffer
    eglDestroyImageKHR(mDisplay, img);
    checkEglError("eglDestroyImageKHR");
}


// -----------------------
// shape rendering methods
// -----------------------

void cGpu::renderEllipse(int x, int y, int width, int height,
			 float r, float g, float b, float alpha,
			 float lineWidth, bool filled)
{
  GLint u;

  // set the render value
  u = glGetUniformLocation(mProgram, "u_render");
  glUniform1i(u, RENDER_SHAPE_ELLIPSE);

  // set the bounding box, converting from pixels to normalized values
  u = glGetUniformLocation(mProgram, "u_x0");
  glUniform1f(u, float(x - width/2) / OUTPUT_WIDTH);
  u = glGetUniformLocation(mProgram, "u_y0");
  glUniform1f(u, float(y - height/2) / OUTPUT_HEIGHT);
  u = glGetUniformLocation(mProgram, "u_x1");
  glUniform1f(u, float(x + width/2) / OUTPUT_WIDTH);
  u = glGetUniformLocation(mProgram, "u_y1");
  glUniform1f(u, float(y + height/2) / OUTPUT_HEIGHT);

  // set color and transparency
  u = glGetUniformLocation(mProgram, "u_color");
  glUniform3f(u, r, g, b);
  u = glGetUniformLocation(mProgram, "u_alpha");
  glUniform1f(u, alpha);

  // use u_r0 to control line width
  u = glGetUniformLocation(mProgram, "u_r0");
  if (filled)
    {
      // u_r0 == 0.0 will fill the whole ellipse
      glUniform1f(u, 0.0);
    }
  else
    {
      // u_r0 sets the inner radius of an annulus; note that the rendered
      // line width will be non-uniform if the ellipse is not a circle
      glUniform1f(u, 0.5 - lineWidth/width);
    }
	
  // draw!
  glDrawArrays(GL_TRIANGLES, 0, 6);
  // 0 is start of vertex array; 6 is number of quad vertexes
}

void cGpu::renderLine(int x0, int y0, int x1, int y1,
		      float r, float g, float b, float alpha,
		      float lineWidth)
{
  GLint u;

  // set the render value
  u = glGetUniformLocation(mProgram, "u_render");
  glUniform1i(u, RENDER_SHAPE);

  // set the endpoints, converting from pixels to normalized values
  u = glGetUniformLocation(mProgram, "u_x0");
  glUniform1f(u, float(x0) / OUTPUT_WIDTH);
  u = glGetUniformLocation(mProgram, "u_y0");
  glUniform1f(u, float(y0) / OUTPUT_HEIGHT);
  u = glGetUniformLocation(mProgram, "u_x1");
  glUniform1f(u, float(x1) / OUTPUT_WIDTH);
  u = glGetUniformLocation(mProgram, "u_y1");
  glUniform1f(u, float(y1) / OUTPUT_HEIGHT);

  // set color and transparency
  u = glGetUniformLocation(mProgram, "u_color");
  glUniform3f(u, r, g, b);
  u = glGetUniformLocation(mProgram, "u_alpha");
  glUniform1f(u, alpha);

  // set line width; note the GPU drivers impose a limit
  glLineWidth(lineWidth);

  // draw!
  glDrawArrays(GL_LINES, 10, 2);
  // 10 is offset from quad+rect vertexes; 2 is number of line vertexes
}

void cGpu::renderRectangle(int left, int top, int width, int height,
			   float r, float g, float b, float alpha,
			   float lineWidth, bool filled)
{
  GLint u;

  // set the render value
  u = glGetUniformLocation(mProgram, "u_render");
  glUniform1i(u, RENDER_SHAPE);

  // set the bounding box, converting from pixels to normalized values
  u = glGetUniformLocation(mProgram, "u_x0");
  glUniform1f(u, float(left) / OUTPUT_WIDTH);
  u = glGetUniformLocation(mProgram, "u_y0");
  glUniform1f(u, float(top) / OUTPUT_HEIGHT);
  u = glGetUniformLocation(mProgram, "u_x1");
  glUniform1f(u, float(left + width) / OUTPUT_WIDTH);
  u = glGetUniformLocation(mProgram, "u_y1");
  glUniform1f(u, float(top + height) / OUTPUT_HEIGHT);

  // set color and transparency
  u = glGetUniformLocation(mProgram, "u_color");
  glUniform3f(u, r, g, b);
  u = glGetUniformLocation(mProgram, "u_alpha");
  glUniform1f(u, alpha);

  // draw!
  if (filled)
    {
      glDrawArrays(GL_TRIANGLES, 0, 6);
      // 0 is start of vertex array; 6 is number of quad vertexes
    }
  else
    {
      glLineWidth(lineWidth);
      glDrawArrays(GL_LINE_LOOP, 6, 4);
      // 6 is offset from quad vertexes; 4 is number of rectangle vertexes
    }
    
}


// ----------------------------------------
// panel and RGBA overlay rendering methods
// ----------------------------------------

/** This function renders an RGBA overlay image.
 *
 * @param renderType determines which texture unit is used, the one
 * for the logo or the one for the general-purpose overlay.
 * @param left gives the x-coordinate of the top-left corner of image.
 * @param top gives the y-coordinate of the top-left corner of image.
 * @param width gives the width of the onscreen, rendered image, which
 * can be different from the width of the loaded image.
 * @param height gives the height of the onscreen, rendered image, which
 * can be different from the height of the loaded image.
 * @param alpha is the rendered transparency of the image in [0.,1.].
 */
void cGpu::renderRgba(int renderType, int left, int top,
		      int width, int height, float alpha)
{
  GLint u;

  if (renderType != RENDER_LOGO && renderType != RENDER_RGBA_OVERLAY)
    {
      LOGW("unknown image render type: %d\n", renderType);
      return;
    }
  
  // set the render value
  u = glGetUniformLocation(mProgram, "u_render");
  glUniform1i(u, renderType);

  // set the bounding box, converting from pixels to normalized values
  u = glGetUniformLocation(mProgram, "u_x0");
  glUniform1f(u, float(left) / OUTPUT_WIDTH);
  u = glGetUniformLocation(mProgram, "u_y0");
  glUniform1f(u, float(top) / OUTPUT_HEIGHT);
  u = glGetUniformLocation(mProgram, "u_x1");
  glUniform1f(u, float(left + width) / OUTPUT_WIDTH);
  u = glGetUniformLocation(mProgram, "u_y1");
  glUniform1f(u, float(top + height) / OUTPUT_HEIGHT);

  // set transparency
  u = glGetUniformLocation(mProgram, "u_alpha");
  glUniform1f(u, alpha);

  // draw!
  glDrawArrays(GL_TRIANGLES, 0, 6);
  // 0 is start of vertex array; 6 is number of quad vertexes
}

/** This function renders the general-purpose RGBA overlay.
 * 
 * @param left gives the x-coordinate of the top-left corner of image.
 * @param top gives the y-coordinate of the top-left corner of image.
 * @param width gives the width of the onscreen, rendered image, which
 * can be different from the width of the loaded image.
 * @param height gives the height of the onscreen, rendered image, which
 * can be different from the height of the loaded image.
 * @param alpha is the rendered transparency of the image in [0.,1.].
 */
void cGpu::renderRgbaOverlay(int left, int top, int width, int height, float alpha)
{
  renderRgba(RENDER_RGBA_OVERLAY, left, top, width, height, alpha);
}

/** This function renders the RGBA logo overlay.
 * 
 * @param left gives the x-coordinate of the top-left corner of image.
 * @param top gives the y-coordinate of the top-left corner of image.
 * @param width gives the width of the onscreen, rendered image, which
 * can be different from the width of the loaded image.
 * @param height gives the height of the onscreen, rendered image, which
 * can be different from the height of the loaded image.
 * @param alpha is the rendered transparency of the image in [0.,1.].
 */
void cGpu::renderRgbaLogo(int left, int top, int width, int height, float alpha)
{
  renderRgba(RENDER_LOGO, left, top, width, height, alpha);
}

/** This function unrolls a portion of the raw camera image to a
 * rectangular pane.
 *
 * This function can be called multiple times per frame to render
 * multiple panes with different parameter values. The order of the
 * calls determines the layering of the panes, with the first pane
 * drawn on the bottom and the last pane drawn on top.
 *
 * @param left is the x-coordinate of the upper left corner of the pane.
 * @param top is the y-coordinate of the upper left corner of the pane.
 * @param width is the width of the pane.
 * @param height is the height of the pane.
 * @param r0 is the inner radius of the raw camera image.
 * @param r1 is the outer radius of the raw camera image.
 * @param theta0 is the start angle of the raw camera image.
 * @param theta1 is the stop angle of the raw camera image.
 * @param alpha is the overall transparency of the pane.
 * @param stretch is the vertical stretching coefficient (0 == no stretching).
 * @param bend is the vertical bending coefficient (0 == no bending).
 */
void cGpu::renderUnroll(int left, int top, int width, int height,
			float theta0, float theta1, float r0, float r1,
			float alpha, float stretch, float bend)
{
  GLint u;

  // set the render value
  u = glGetUniformLocation(mProgram, "u_render");
  glUniform1i(u, RENDER_UNROLL);

  // set the arc angles
  u = glGetUniformLocation(mProgram, "u_theta0");
  glUniform1f(u, theta0);
  u = glGetUniformLocation(mProgram, "u_theta1");
  glUniform1f(u, theta1);

  // set the radii
  u = glGetUniformLocation(mProgram, "u_r0");
  glUniform1f(u, r0);
  u = glGetUniformLocation(mProgram, "u_r1");
  glUniform1f(u, r1);

  // set the warping coefficients
  u = glGetUniformLocation(mProgram, "u_stretch");
  glUniform1f(u, stretch);
  u = glGetUniformLocation(mProgram, "u_bend");
  glUniform1f(u, bend);

  // set the bounding box, converting from pixels to normalized values
  u = glGetUniformLocation(mProgram, "u_x0");
  glUniform1f(u, float(left) / OUTPUT_WIDTH);
  u = glGetUniformLocation(mProgram, "u_y0");
  glUniform1f(u, float(top) / OUTPUT_HEIGHT);
  u = glGetUniformLocation(mProgram, "u_x1");
  glUniform1f(u, float(left + width) / OUTPUT_WIDTH);
  u = glGetUniformLocation(mProgram, "u_y1");
  glUniform1f(u, float(top + height) / OUTPUT_HEIGHT);

  // set transparency
  u = glGetUniformLocation(mProgram, "u_alpha");
  glUniform1f(u, alpha);

  // draw!
  glDrawArrays(GL_TRIANGLES, 0, 6);
  // 0 is start of vertex array; 6 is number of quad vertexes
}
