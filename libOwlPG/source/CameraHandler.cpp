/* Copyright Playground Global 2016

  Camera Access library that encapsulates camera access
  through Android HAL from userspace
*/

#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <hardware/hardware.h>
#include <hardware/camera.h>
#include <sys/time.h>
#include <system/window.h>
#include <binder/MemoryBase.h>
#include <binder/MemoryHeapBase.h>
#include <utils/Log.h>

#include "CameraHandler.h"
#include "WindowSurface.h"
#include "LogDebug.h"
#include "UVCHandler.h"

// Debug output enable / disable macros
// #define DEBUG_CAMERA_PARAMETERS
#ifdef DEBUG_CAMERA_PARAMETERS
#define DEBUG_OUT(...) (fprintf(stderr, __VA_ARGS__))
#else
#define DEBUG_OUT(...) {}
#endif
#undef DEBUG_CAMERA_PARAMETERS

#define DEBUG_CAMERA_PARAMETER_ERRORS
#ifdef DEBUG_CAMERA_PARAMETER_ERRORS
#define ERROR_OUT(...) (fprintf(stderr, __VA_ARGS__))
#else
#define ERROR_OUT(...) {}
#endif
#undef DEBUG_CAMERA_PARAMETER_ERRORS


#define container_of(ptr, type, member) ({                      \
        const typeof(((type *) 0)->member) *__mptr = (ptr);     \
        (type *) ((char *) __mptr - (char *)(&((type *)0)->member)); })

TIMELOG_INIT(camera);

// thread support for making stop_preview() timeout (see below)
#ifdef __cplusplus
extern "C" {
#endif
static pthread_t timeout_thread;
static pthread_mutex_t timeout_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t timeout_cond = PTHREAD_COND_INITIALIZER;
#ifdef __cplusplus
}
#endif

using namespace android;

static WindowSurface windowSurface;
static WindowSurface glWindowSurface;
static camera_device_t *dev = NULL;
static preview_stream_ops_t nw;
static void __put_memory(camera_memory_t *data);
static frameAvailableCB frameCB = NULL;
static char cameraParams[10240];

class CameraHeapMemory : public RefBase {
    public:
        CameraHeapMemory(int fd, size_t buf_size, uint_t num_buffers = 1) :
                         mBufSize(buf_size),
                         mNumBufs(num_buffers)
        {
            mHeap = new MemoryHeapBase(fd, buf_size * num_buffers);
            commonInitialization();
        }

        CameraHeapMemory(size_t buf_size, uint_t num_buffers = 1) :
                         mBufSize(buf_size),
                         mNumBufs(num_buffers)
        {
            mHeap = new MemoryHeapBase(buf_size * num_buffers);
            commonInitialization();
        }

        void commonInitialization()
        {
            handle.data = mHeap->base();
            handle.size = mBufSize * mNumBufs;
            handle.handle = this;

            mBuffers = new sp<MemoryBase>[mNumBufs];
            for (uint_t i = 0; i < mNumBufs; i++)
                mBuffers[i] = new MemoryBase(mHeap,
                                             i * mBufSize,
                                             mBufSize);

            handle.release = __put_memory;
        }

        virtual ~CameraHeapMemory()
        {
            delete [] mBuffers;
        }

        size_t mBufSize;
        uint_t mNumBufs;
        sp<MemoryHeapBase> mHeap;
        sp<MemoryBase> *mBuffers;

        camera_memory_t handle;
};

static void __put_memory(camera_memory_t *data)
{
    CameraHeapMemory *mem = static_cast<CameraHeapMemory *>(data->handle);
    mem->decStrong(mem);
}

static int __lock_buffer(struct preview_stream_ops*,
                      buffer_handle_t*)
{
    return 0;
}

static int __enqueue_buffer(struct preview_stream_ops*,
                      buffer_handle_t* buffer)
{
    EGLNativeWindowType window = windowSurface.getSurface();
    return window->queueBuffer(window, container_of(buffer, ANativeWindowBuffer, handle), -1);
}

static int __cancel_buffer(struct preview_stream_ops*,
                      buffer_handle_t* buffer)
{
    EGLNativeWindowType window = windowSurface.getSurface();
    return window->cancelBuffer(window, container_of(buffer, ANativeWindowBuffer, handle), -1);
}

static int __set_buffer_count(struct preview_stream_ops*, int count)
{
    EGLNativeWindowType window = windowSurface.getSurface();
    return native_window_set_buffer_count(window, count);
}

static int __set_buffers_geometry(struct preview_stream_ops*,
                      int width, int height, int format)
{
    EGLNativeWindowType window = windowSurface.getSurface();
    int rc = native_window_set_buffers_dimensions(window, width, height);
    if(!rc){
        rc = native_window_set_buffers_format(window, format);
    }
    return rc;
}

static int __set_crop(struct preview_stream_ops *,
                      int left, int top, int right, int bottom)
{
    EGLNativeWindowType window = windowSurface.getSurface();
    android_native_rect_t crop;
    crop.left = left;
    crop.right = right;
    crop.top = top;
    crop.bottom = bottom;
    return native_window_set_crop(window, &crop);
}

static int __set_timestamp(struct preview_stream_ops *,
                           int64_t timestamp) {
    EGLNativeWindowType window = windowSurface.getSurface();
    return native_window_set_buffers_timestamp(window, timestamp);
}

static int __set_usage(struct preview_stream_ops*, int usage)
{
    EGLNativeWindowType window = windowSurface.getSurface();
    return native_window_set_usage(window, usage);
}

static int __set_swap_interval(struct preview_stream_ops *, int interval)
{
    EGLNativeWindowType window = windowSurface.getSurface();
    return window->setSwapInterval(window, interval);
}

static int __get_min_undequeued_buffer_count(
                  const struct preview_stream_ops *,
                  int *count)
{
    EGLNativeWindowType window = windowSurface.getSurface();
    return window->query(window, NATIVE_WINDOW_MIN_UNDEQUEUED_BUFFERS, count);
}

static int counter = 0;
static int __dequeue_buffer(struct preview_stream_ops*,
                            buffer_handle_t** buffer, int *stride)
{
    EGLNativeWindowType window = windowSurface.getSurface();
    ANativeWindowBuffer *anb;
    int rc = native_window_dequeue_buffer_and_wait(window, &anb);
    if(!rc){
        *buffer = &anb->handle;
        *stride = anb->stride;
        // Throw away first 10 buffers
        if (counter < 10){
            counter++;
        }else if (gUvcHandlerStreaming | gOwlCoreMeetingInProgress) {
            if (frameCB)
                frameCB(anb);
            TIMELOG_LOG(camera);
        }
    }
    return rc;
}

static camera_memory_t* __get_memory(int fd, size_t buf_size, uint_t num_bufs,
                                     void *user __attribute__((unused)))
{
    CameraHeapMemory *mem;
    if (fd < 0)
        mem = new CameraHeapMemory(buf_size, num_bufs);
    else
        mem = new CameraHeapMemory(fd, buf_size, num_bufs);
    mem->incStrong(mem);
    return &mem->handle;
}

static void __notify_cb(int32_t, int32_t,
                            int32_t, void *)
{
    return;
}

static void __data_cb(int32_t,
                          const camera_memory_t *, unsigned int,
                          camera_frame_metadata_t *,
                          void *)
{
    return;
}

static void __data_cb_timestamp(nsecs_t, int32_t,
                             const camera_memory_t *, unsigned,
                             void *)
{
    return;
}

static status_t selectConfigForPixelFormat(
        EGLDisplay dpy,
        EGLint const* attrs,
        int32_t format,
        EGLConfig* outConfig)
{
    EGLint numConfigs = -1, n=0;

    if (!attrs)
        return BAD_VALUE;

    if (outConfig == NULL)
        return BAD_VALUE;

    // Get all the "potential match" configs...
    if (eglGetConfigs(dpy, NULL, 0, &numConfigs) == EGL_FALSE)
        return BAD_VALUE;

    EGLConfig* const configs = (EGLConfig*)malloc(sizeof(EGLConfig)*numConfigs);
    if (eglChooseConfig(dpy, attrs, configs, numConfigs, &n) == EGL_FALSE) {
        free(configs);
        return BAD_VALUE;
    }

    int i;
    EGLConfig config = NULL;
    for (i=0 ; i<n ; i++) {
        EGLint nativeVisualId = 0;
        eglGetConfigAttrib(dpy, configs[i], EGL_NATIVE_VISUAL_ID, &nativeVisualId);
        if (nativeVisualId>0 && format == nativeVisualId) {
            config = configs[i];
            break;
        }
    }

    free(configs);

    if (i<n) {
        *outConfig = config;
        return NO_ERROR;
    }

    return NAME_NOT_FOUND;
}

static status_t selectConfigForNativeWindow(
        EGLDisplay dpy,
        EGLint const* attrs,
        EGLNativeWindowType window,
        EGLConfig* outConfig)
{
    int err;
    int format;

    if (!window)
        return BAD_VALUE;

    if ((err = window->query(window, NATIVE_WINDOW_FORMAT, &format)) < 0) {
        return err;
    }

    return selectConfigForPixelFormat(dpy, attrs, format, outConfig);
}

extern "C"
{

int getNativeWindowConfig(EGLDisplay dpy, EGLNativeWindowType *window, EGLConfig *config)
{
    EGLBoolean returnValue;
    EGLint s_configAttribs[] = {
            EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
            EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
            EGL_NONE };

    *window = glWindowSurface.getSurface();
    returnValue = selectConfigForNativeWindow(dpy, s_configAttribs, *window, config);
    if (returnValue) {
        printf("selectConfigForNativeWindow() returned %d", returnValue);
        return 1;
    }
    return 0;
}

int startCamera()
{
    if ( dev == NULL )
    {
        fprintf(stderr, "Camera is not initialized\n");
        return -1;
    }
    dev->ops->start_preview(dev);
    return 0;
}

static void *stopPreviewWrapper(void *arg)
{
  (void) arg;
  // set frameCB to NULL so __dequeue_buffer() will stop calling it
  frameCB = NULL;
  dev->ops->stop_preview(dev);
  return NULL;
}

static void stopPreviewTimeout(int seconds)
{
  // This function is used to wrap stop_preview() in a thread that
  // times out. This is to mitigate problems where the call to
  // stop_preview() hangs. Note that this is still a bit experimental
  // and that the call to stop_preview() seems to always time out when
  // called from a separate thread. See below in stopCamera() where we
  // have available three options.
  int err;
  struct timespec wait_time;
  struct timeval now;
  int rt;

  // contruct the absolute time for the timeout
  gettimeofday(&now, NULL);
  wait_time.tv_sec = now.tv_sec + seconds;
  wait_time.tv_nsec = now.tv_usec * 1000;

  // use a thread to kill stop_preview() after a timeout
  pthread_mutex_lock(&timeout_mutex);
  rt = pthread_create(&timeout_thread, NULL, stopPreviewWrapper, NULL);
  if (rt == 0)
    { pthread_setname_np(timeout_thread, "stopPreviewTmr"); }
  err = pthread_cond_timedwait(&timeout_cond, &timeout_mutex, &wait_time);
  if (err == ETIMEDOUT)
    { printf("stop_preview() timed out after %ds\n", seconds); }
  pthread_mutex_unlock(&timeout_mutex);
}

int stopCamera()
{
    if ( dev == NULL )
    {
        fprintf(stderr, "Camera is not initialized\n");
        return -1;
    }
    TIMELOG_END(camera);

    // TODO: further experiments to decide the best way to deal with
    // calls to stop_preview() that hang. For now, we have three
    // options: 1) don't call stop_preview() at all, 2) call it
    // normally, or 3) call stopPreviewTimeout().
#if 1
    // OPTION 1: don't call stop_preview() at all, but set frameCB to
    // NULL and give previous calls to frameCB() a chance to return.
    // Setting frameCB to NULL causes __dequeue_buffer() to stop
    // calling it. Keep an eye out for a possible memory leak. Here
    // are a couple useful commands for checking memory: "dumpsys
    // meminfo" and "cat /d/ino/heaps/system"
    frameCB = NULL;
    sleep(1);
#elif 1
    // OPTION 2: call stop_preview() normally; this hangs occasionally.
    dev->ops->stop_preview(dev);
#else
    // OPTION 3: use a thread to make stop_preview() time out; this
    // seems to always time out.
    stopPreviewTimeout(5);
#endif
    dev->ops->release(dev);
    return 0;
}

int registerCameraCallback(frameAvailableCB cb)
{
    frameCB = cb;
    return 0;
}

/** Find first instance of the paramKey string in camera parameters.
 *
 * @param paramKey is the key string parameter name.
 * @param params is the pointer to the entire parameters string.
 *
 * @return Pointer to the value following the found parameter in the
 * global cameraParams buffer.
 */
char * findParameter(const char *paramKey, char *params)
{
  char *pReturn = NULL;
  char keyEquals[256];
  char *pSemiColon;

  // find ;paramKey= in the string
  strncpy(keyEquals, ";", sizeof(keyEquals) - 1);
  strncat(keyEquals, paramKey, sizeof(keyEquals) - 1);
  strncat(keyEquals, "=", sizeof(keyEquals) - 1);

  if ((pReturn = strstr(params, keyEquals)) != NULL)
    {
      // return pointer to first char of value associated with this key
      pReturn += strlen(keyEquals);
    }

  return pReturn;
}

/** In-place replacement of a key value with a new value.
 *
 * @param param is the parameter key and value to replace.
 * @param paramString is the list of all parameter keys and values.
 * @param paramStringLen is the length of paramString.
 *
 * @return true on successful replacement, false if key is not found.
 */
bool replaceParameter(cameraParams_t param, char *paramString,
                      int paramStringLen)
{
  bool bReturn = true;
  char keyEquals[256];
  char *pKeyStart;
  char *pValueStart;
  char *pDelimiterOrig;
  char *pDelimiterNew;
  int remainderLen = 0;
  int valueLenOrig;
  int valueLenNew;

  strncpy(keyEquals, ";", sizeof(keyEquals) - 1);
  strncat(keyEquals, param.pKey, sizeof(keyEquals) - 1);
  strncat(keyEquals, "=", sizeof(keyEquals) - 1);

  if ((pKeyStart = strstr(paramString, keyEquals)) != NULL)
    {
      pValueStart = pKeyStart + strlen(keyEquals);
      pDelimiterOrig = strstr(pValueStart, ";");
      if (pDelimiterOrig == NULL)
        {  // At end of list of items, so no ';'
          valueLenOrig = strlen(pValueStart);
        }
      else
        {
          valueLenOrig = pDelimiterOrig - pValueStart;
          remainderLen = strlen(paramString) - (pDelimiterOrig - paramString);
        }
      valueLenNew = strlen(param.pValue);
      if (valueLenNew == valueLenOrig)
        {  // Just copy new value over old value
          memcpy(pValueStart, param.pValue, valueLenNew);
        }
      else
        {  // Copy (move) everything after and including delimiter leaving
           // space for the new value, then copy the new value
          if (strlen(paramString) + (valueLenNew - valueLenOrig) <=
              (size_t)paramStringLen)
            {  // Enough space for the new string
              pDelimiterNew = pValueStart + valueLenNew;
              memmove(pDelimiterNew, pDelimiterOrig, remainderLen + 1 /*EOS*/);
              memcpy(pValueStart, param.pValue, valueLenNew);
            }
          else
            {  // Not enough space for the new string
              bReturn = false;
            }
        }
    }
  else
    { bReturn = false; }

  return bReturn;
}

/** Modify camera control parameters.
 *
 * This routine retrieves the current set of camera control parameters from
 * the camera driver, updates the control string by replacing the value
 * fields of matching keys, then instantiates the new parameter set string.
 *
 * @param paramList is a list of key - value pairs to replace.
 * @param numParams is the number of key - value pairs in the paramList.
 *
 * @return true if all parameters are successfully set, false otherwise.
 */
bool setCameraParameters(cameraParams_t paramList[], size_t numParams)
{
  bool bSuccess = true;

  // retrieve the camera parameters from the kernel
  char *params;
  params = dev->ops->get_parameters(dev);
  DEBUG_OUT("Camera parameters 0:\n%s\n", params);

  // test to be sure they can be written exactly as retrieved
  if (dev->ops->set_parameters(dev, params) == BAD_VALUE)
    { DEBUG_OUT("dev->ops->set_parameters() 0 failed\n"); }
  else
    { DEBUG_OUT("set_parameters() 0 WORKED!!!\n"); }

  // copy into local memory before modification
  strncpy(cameraParams, params, sizeof(cameraParams) - 1);

  // free the params returned from the kernel
  if (dev->ops->put_parameters)
    { dev->ops->put_parameters(dev, params); }
  else
    { free(params); }

  // go through paramList replacing values
  for (size_t param = 0; param < numParams; param++)
    {
      if (findParameter(paramList[param].pKey, cameraParams) != NULL)
        {
          replaceParameter(paramList[param],
                           cameraParams,
                           sizeof(cameraParams));
        }
      else
        {
          bSuccess = false;
          ERROR_OUT("Could not set camera parameter %s to %s\n",
                  paramList[param].pKey, paramList[param].pValue);
          break;
        }
    }

  if (bSuccess)
    {
      if (dev->ops->set_parameters(dev, cameraParams) == BAD_VALUE)
        { ERROR_OUT("dev->ops->set_parameters() 1 failed, params: \n%s\n", cameraParams); }
      else
        { DEBUG_OUT("dev->ops->set_parameters() New parameters WORKED!!!\n"); }

      params = dev->ops->get_parameters(dev);
      DEBUG_OUT("Camera parameters (after):\n%s\n", params);
      if (dev->ops->put_parameters)
        { dev->ops->put_parameters(dev, params); }
      else
        { free(params); }
    }

  return bSuccess;
}

/** Retrieve value of one camera control parameter.
 *
 * Note, the returned string points to the global buffer used by libOwlPG
 * to hold camera parameters and only remains valid until another call to
 * this function, or a call to setCameraParameters().
 *
 * @param paramKey is the key whose value should be retrieved.
 *
 * @return string pointer to global memory where the value is stored.
 */
char *getCameraParameter(const char *paramKey)
{
  char *pReturn;

  // retrieve the camera parameters from the kernel
  char *params;
  params = dev->ops->get_parameters(dev);
  DEBUG_OUT("Camera parameters 0:\n%s\n", params);

  // copy into local memory before modification
  strncpy(cameraParams, params, sizeof(cameraParams) - 1);

  // free the params returned from the kernel
  if (dev->ops->put_parameters)
    { dev->ops->put_parameters(dev, params); }
  else
    { free(params); }

  pReturn = findParameter(paramKey, cameraParams);

  if (pReturn != NULL)
    {
      char *pSemiColon;

      // return pointer to first char of value associated with this key
      if ((pSemiColon = strstr(pReturn, ";")) != NULL)
        {
          *pSemiColon = 0; // terminate the string at end of value
        }
    }

  return pReturn;
}

/** Initialize the camera
 *
 */
int initCamera(Resolution res)
{
    int rc;
    char camera_device_name[] = "0";
    const char *id = "camera";
    void *handle = NULL;
    hw_module_t *hmi;
    camera_module_t *cam = NULL;
    frameCB = NULL;

    memset(&nw, 0x00, sizeof(struct preview_stream_ops));
    nw.cancel_buffer = __cancel_buffer;
    nw.lock_buffer =__lock_buffer;
    nw.dequeue_buffer =__dequeue_buffer;
    nw.enqueue_buffer = __enqueue_buffer;
    nw.set_buffer_count = __set_buffer_count;
    nw.set_buffers_geometry = __set_buffers_geometry;
    nw.set_crop = __set_crop;
    nw.set_timestamp = __set_timestamp;
    nw.set_usage = __set_usage;
    nw.set_swap_interval = __set_swap_interval;
    nw.get_min_undequeued_buffer_count = __get_min_undequeued_buffer_count;

    /*
     * load the symbols resolving undefined symbols before
     * dlopen returns. Since RTLD_GLOBAL is not or'd in with
     * RTLD_NOW the external symbols will not be global
     */
    handle = dlopen("/system/lib/hw/camera.msm8916.so", RTLD_NOW);
    if (handle == NULL) {
        char const *err_str = dlerror();
        fprintf(stderr, "load: module=\n%s", err_str ? err_str : "unknown");
	return -1;
    }

    /* Get the address of the struct hal_module_info. */
    const char *sym = HAL_MODULE_INFO_SYM_AS_STR;
    hmi = (hw_module_t *)dlsym(handle, sym);
    if (hmi == NULL) {
        fprintf(stderr, "load: couldn't find symbol %s", sym);
	return -2;
    }

    /* Check that the id matches */
    if (strcmp(id, hmi->id) != 0) {
        fprintf(stderr, "load: id=%s != hmi->id=%s", id, hmi->id);
	return -3;
    }

    hmi->dso = handle;
    cam = (camera_module_t*)hmi;
    fprintf(stderr, "loaded %s\n", cam->common.name);

    camera_info_t info;
    int cameraId = 0;
    if( 0 != cam->get_camera_info(0, &info)){
	fprintf(stderr, "invalid camera info\n");
	return -4;
    }

    /* Print the Camera API version */
    fprintf(stderr, "camera device version api: %d hal: %d\n", hmi->module_api_version, hmi->hal_api_version);

    rc = hmi->methods->open(hmi,camera_device_name,(hw_device_t**)&dev);
    if(dev){
	fprintf(stderr, "camera open\n");
    }
    else{
	fprintf(stderr, "could not open camera %u\n", rc);
	return -5;
    }
    dev->ops->set_callbacks(dev, __notify_cb, __data_cb, __data_cb_timestamp, __get_memory, NULL );
    if(dev->ops->set_preview_window){
        dev->ops->set_preview_window(dev, &nw);
    }
    else
    {
	fprintf(stderr, "preview methods not available");
        return -6;
    }

    cameraParams_t picture_size_1728[] = {
        /* Key,                   New value,           Default value */
        {"preview-size",          "1728x1728"},
//      {"picture-size",          "1728x1728"},
//      {"video-size",            "1728x1728"},
//      {"preferred-preview-size-for-video", "1728x1728"},
        {"antibanding",           "auto"},            // "auto"
        {"auto-exposure",         "center-weighted"}, // "center-weighted"
        {"denoise",               "denoise-off"},     // "denoise-off"
        {"lensshade",             "enable"},          // "enable"
        {"exposure-compensation", "0"},               // "0"
        {"jpeg-quality",          "85"},              // "85"
        {"jpeg-thumbnail-quality","85"},              // "85"
    };
    cameraParams_t picture_size_3456[] = {
        /* Key,                   New value,           Default value */
        {"preview-size",          "3456x3456"},
//      {"picture-size",          "3456x3456"},
//      {"video-size",            "3456x3456"},
//      {"preferred-preview-size-for-video", "3456x3456"},
        {"antibanding",           "auto"},            // "auto"
        {"auto-exposure",         "center-weighted"}, // "center-weighted"
        {"denoise",               "denoise-off"},     // "denoise-off"
        {"lensshade",             "enable"},          // "enable"
        {"exposure-compensation", "0"},               // "0"
        {"jpeg-quality",          "85"},              // "85"
        {"jpeg-thumbnail-quality","85"},              // "85"
    };

    if (res == RES_3456x3456)
      {
        setCameraParameters(picture_size_3456, sizeof(picture_size_3456) /
                            sizeof(picture_size_3456[0]));
      }
    else
      {
        setCameraParameters(picture_size_1728, sizeof(picture_size_1728) /
                            sizeof(picture_size_1728[0]));
      }

    return 0;
}

}
