#ifndef AUDIOFRAMEWORK_H
#define AUDIOFRAMEWORK_H

//#include <sound/asound.h>

#ifdef __cplusplus
extern "C" {
#else
#define true 1
#define false 0
#endif

///< Debug configuration
// if AUDIO_DEBUG NOT set then all debug will be off regardless
//  Else the debug settings will come from the debug flags
//#define AUDIO_DEBUG
#ifdef AUDIO_DEBUG
#define DEBUG_ON  true
#define DEBUG_OFF false
#else
#define DEBUG_ON  false
#define DEBUG_OFF false
#endif


// use these defines if debugging
#define AUDIO_SYSTEM_DEBUG    DEBUG_ON
#define TO_USB_FILE_OUT       DEBUG_OFF
#define FROM_MIC_FILE_OUT     DEBUG_OFF
#define FROM_USB_FILE_OUT     DEBUG_OFF
#define TO_SPKR_FILE_OUT      DEBUG_OFF
#define APP_OUT_FILE_OUT      DEBUG_OFF

#define BYPASS_MIC_PROC       DEBUG_OFF
#define USB_LOOPBACK          DEBUG_OFF
#define PROC_MIC_DEBUG_BUFF   DEBUG_OFF
#define MIC_DEBUG_BUFF        DEBUG_OFF
#define FRAMEWORK_DEBUG_BUFF  DEBUG_OFF
#define SPKR_DEBUG_BUFF       DEBUG_OFF

#define SHOW_SPKR_STATE       DEBUG_OFF
#define SHOW_CB_DEBUG         DEBUG_OFF
#define SHOW_ALSA_DEBUG       DEBUG_OFF
#define BYPASS_MIC_PROCESSING DEBUG_OFF
#define MIC_DRVR_DEBUG        DEBUG_OFF
#define ALM_DEBUG             DEBUG_OFF
#define AEC_DEBUG             DEBUG_ON
#define LOCALIZER_DEBUG       DEBUG_OFF
#define AUDIOPLAYER_DEBUG     DEBUG_OFF
#define ENABLE_SRC
#define DEFAULT_SAMPLE_RATE 48000
// amount of delay that will be added to the ref channel (in bytes) must be
// divisible by  frame size e.g. 1ms = 192
#define REF_DELAY_MS    54
#define REF_DELAY_SIZE  (REF_DELAY_MS*192)
//#define LOOPBACK
#define MIC_SPI_PRIORITY      90
#define LOOPBACK_PCM_PRIORITY 90
#define MIC_PROC_PRIORITY     85
#define PROC_TO_USB_PRIORITY  80
#define SPKR_WRITE_PRIORITY   80
#define READ_USB_PRIORITY     85
#define AUDIO_SUPPORT_PRIORITY 75

//helpful macro to make ensure proper number of zeros.
#define ONE_BILLION (1000000000LL)

#define MAX_CHANNELS  8

#define SAMPLE_SIZE 2
#define MIC_BYTES_PER_FRAME 16
#define BYTES_PER_FRAME 4

// the master number of frames to process for each path at a time
#define MASTER_PERIOD_SIZE 240
#define PROC_FRAME_SIZE    960
#define MIC_CHANS       8
#define REF_CHANS       2
#define SRC_ALLOWANCE 32
#define FILE_CB_COUNT 4
#define FILE_PLAY_CB_SIZE 96000
#define SRC_POLL_TIME ONE_BILLION

#define READ_TIME_OUT ((ONE_BILLION/DEFAULT_SAMPLE_RATE)*MASTER_PERIOD_SIZE*4)

/// configuration parameters for the sample rate estimator.
#define FS_EST_POLL_TIME (1000000000LL)
#define FS_EST_DEFAULT_FS 48000.0
#define FS_EST_SKIP_COUNT 100

#define DEF_DEBUG_BUFF_SIZE (1024*1024*8)

#define FS_STABLE (0.05)
#define SPKR_NOT_ACTIVE   0
#define SPKR_ACTIVE       1
#define RAMP_SAMPLES 480

#define ACTIVE_SPKR_LEVEL -24.0
#define ACTIVE_SPKR_HYS    250000000 // 250 ms hyst time to catch the end of the echo

#define USB_CHANNEL_ACTIVE 1
#define USB_CHANNEL_INACTIVE 0

///  Definiton of structure holding values for reading properties
typedef struct
{
  char propName[64];
  int propDefault;
  int *propDest;
} AUDIO_PROP_RECORD;

/// Defintion of the structure contains values read from the andriod property store.
typedef struct
{
  int micLocalGen;        ///< if == 1 causes the mic thread to throw away
  ///< mic data and replace with known pattern.
  int spkrLocalGen;       ///< if set to 1 causes the speaker thread to igonre
  ///< audio from usb and ouptut a known pattern
  int printFsEstimate;    ///< if set to 1 peridocally print the sample rate
  ///< estimates for the microphone input and usb from host
  int closeDebugFiles;         ///< if set to one close all debug files;
  int spkrTrim;           ///< if exists used as a 1.15 number mulitplied to
  ///< output speaker samples to trim output level
  int micTrim;            ///< if exists used as a 1.15 number mulitplied to
  ///< input mic samples to trim input level
  int disableVolume;      ///< if set to 1 will disable the volume control.
  int CICGain;            ///< if exists will be used for the attenuation in
  ///< the CIC filter. is 1.15 value, default is 16384.
  int writeDebugAudio;    ///< if 1 write debug audio to file; if 0 stop
  int audioCursor;        ///< angle to draw cursor for measuring angles
  int micChans;
  int micLeftChan;        ///< mic channel to send on the left channel to host
  int micRightChan;       ///< mic channel to send on the right channel to host
  int enableMicProc;      ///< if not set will bypass all mic processing
  int enableEq;           ///< if set, enables the microphone eq.
  int enableBeam;         ///< if set enable beamforming on the output
  int enableAEC;          ///< if set will enable acoustic echo cancellation
  int enableNR;           ///< if set will enable noise reduction
  int enableLM;           ///< if set will enable level manager
  int micMute;            ///< property tied to mic mute
  int aecDebug;           ///< property to control output from AEC
  int aecPost;            ///< configures which algorithm to use after AEC
  int altThresh;           ///< The threshold for the alternate threshold
  int altEnable;          ///< Set the enable level of the alternate limiter
  int enableTiDsp;        ///< allows control of TI dsp in smart amp
  int localDebug;         ///< controls what debug info printed from localizer
  int localNum;           ///< controls what beam number is used debug localizer
  int localLog;           ///< controls logging of localizer info to file
  int localRatio;           ///< localizer pre/post ratio * 10.
  int minSpkrActive;      /// minimum Level to declare speaker active  
  int nrAmount;           ///< amount of noise reduction to apply
  int dtdThreshold;       ///< Threshold for declaring double talk. 
  int aecDebugAudio;      ///< controls writing of aec audio to local files
  int micGainsDb[8];      ///< gains for each of the microphones for the downmix

} AUDIO_PROPERTIES;

/// Structure contains information about the state of the audio interfaces
typedef struct
{
  int refAvail;

} AUDIO_IF_STATUS;
//#define SRC_POLL_TIME ((unsigned long long)ONE_BILLION*3)

// typedefs for circular buffer types.
typedef char ITEM_DEF;
typedef int INDEX;
typedef unsigned long long ULL;

///< structure taken from pcm.c  for debug purposes
///< NOTE: this is not complete, it has enough to get to underruns
struct pcm_debug
{
  int fd;
  unsigned int flags;
  int running: 1;
  int prepared: 1;
  int underruns;
  unsigned int buffer_size;
  unsigned int boundary;
};



/// Definintion of circular buffer descriptor

typedef struct
{

  pthread_mutex_t bufMutex;  ///< mutex to control access to this data strcuture
  pthread_cond_t  bufCond;   ///< wait condition to wait upon when waiting for
  ///< data to be written.
  INDEX readIndex;           ///< index of next item to be read.
  INDEX writeIndex;          ///< index of next item to be written.
  INDEX maxIndex;            ///< the maximum item index allwed in the buffer
  int restartCount;          ///< The number of times the buffer has been reset,
  ///< allow clients to see if buffer has been reset
  ///< from under them.
  ITEM_DEF *pBufPtr;         ///< Pointer to the underlying buffer memory,
  unsigned long long totalWaitTime;   ///< total amopunt of time that reader was
  ///< waiting for data.
  int waitCount;                ///< number of times the reader waited.
  int itemsIn;                  ///< Number of items written to buffer
  int itemsOut;                 ///< Number of items read from buffer
  unsigned int overrunCount;             ///< number of times the CB has overrun
  unsigned long long lastTime;   ///< used by metric calculation
  unsigned long long itemSecs;   ///< buffer metric - measure of how much of the
  ///< buffer has been used for how long.
  unsigned long long
  restartMetricTime; ///< The last time the metric was restarted.
  unsigned long long lastWriteTime;
  int itemSize;             ///< size of each each structure imposed over basic
  ///< ITEM_DEF type; used in OVERRUNALBEL calculation
  char name[17];            ///< name of buffer

} CIRC_BUFF;

/// Defintion of circular buffer statistics available to user.
// see same variables in circular buffer descriptor above.
typedef struct
{
  int itemsIn;  ///<
  int itemsOut;
  unsigned long long totalWaitTime;
  int waitCount;
} CIRC_BUFF_STATS;
/// Defintion of buffer timestamp
typedef struct
{
  unsigned int amount;
  unsigned int totalWrites;
  unsigned int overruns;
  unsigned long long lastWriteTime;
} CIRC_BUFF_TIMESTAMP;

/// Defintion of the circular buffer metric returned to user.
typedef struct
{
  unsigned long long itemSecs;            ///< buffer occupancy measure.
  unsigned long long
  measureTime;         ///< length of time the metric was measured across.
} CIRC_BUFF_METRIC;

/// structure used on usb loopback to indicate the beginning of a buffer.
typedef struct
{
  int mark;
  int frameNumber;
  unsigned long long timestamp;

} audio_frame_desc;


/// defition of a poll timer descriptor used for determing time in polling loop.
typedef struct
{
  unsigned long long nextTime; ///< the next time the timer will expire
  unsigned long long interval; ///< how long between expirations.

} POLL_TIMER;


/// used for measuring a interval  of time, particularly in the circ buff metric
typedef struct
{
  unsigned long long startTime;
  unsigned long long currTime;
} INTERVAL;


/// defitnion of a in-memory debug buffer
typedef struct
{
  char *pBuffer;  ///< pointer to memory block
  int length;     ///< length of in-memory buffer
  char *pCurr;    ///< pointer to curr write position in buffer
  char *pEnd;     ///< pointer to end of buffer
} DEBUG_BUFF;


/// externs for functions in the audioframework.c

typedef struct
{
  int procFrameCount;
  int micCount;
} AUDIO_CONFIG;

/// Returns the configuration of the audio path
extern int GetAudioConfig(AUDIO_CONFIG *pConfig);
/// returns number of nanoseconds since system start.
extern unsigned long long getNsecs();
/// returns number of milliseconds since system start.
extern unsigned long long getMsecs();

// polled "timer" functions
extern int PollTimerInit(POLL_TIMER *timer, unsigned long long interval);
extern int PollTimerPollTimer(POLL_TIMER *timer);
extern int PollTimerPollTimerOnce(POLL_TIMER *timer);
extern int PollTimerReset(POLL_TIMER *timer, unsigned long long interval);

// interval measurement functions
extern int IntervalInit(INTERVAL *timer);
unsigned long long IntervalCurrTime(INTERVAL *timer);
unsigned long long IntervalGetInterval(INTERVAL *timer);

// routines for manipulating multichannel buffers
int ExtractChannel(short *pInBuff, short *pOutBuff, int frameCount,
                   int channel, int channelCount);
int InsertChannel(short *pInBuff, short *pOutBuff, int frameCount,
                  int channel, int channelCount);

// thread safe circular buffer routines -
extern int CircBuffFilled(CIRC_BUFF *bufDesc);
extern int CircBuffEmpty(CIRC_BUFF *bufDesc);
extern int CircBuffInit(CIRC_BUFF *bufDesc, INDEX maxIndex, int item_size,
                        char *name);
extern int CircBuffRestartCount(CIRC_BUFF *bufDesc);
extern int CircBuffRestart(CIRC_BUFF *bufDesc);
extern int CircBuffWait(CIRC_BUFF *bufDesc);
extern int CircBuffTimedWait(CIRC_BUFF *pBufDesc, int timeInMs);
extern int CircBuffWrite(CIRC_BUFF *bufDesc, ITEM_DEF *inData, int count);
extern int CircBuffRead(CIRC_BUFF *bufDesc, ITEM_DEF *outData, int count);
extern int CircBuffClean(CIRC_BUFF *bufDesc);
extern int CircBuffStats(CIRC_BUFF *bufDesc, CIRC_BUFF_STATS *stats);
extern int CircBuffGetMetric(CIRC_BUFF *bufDesc, CIRC_BUFF_METRIC *metric);
extern int CircBuffResetMetric(CIRC_BUFF *bufDesc);
extern int CircBuffTimestamp(CIRC_BUFF *pBufDesc, CIRC_BUFF_TIMESTAMP *pTsDesc);

extern int DBInit(DEBUG_BUFF *pBuff, int size);
extern int DBReset(DEBUG_BUFF *pBuff);
extern int DBPrintf(DEBUG_BUFF *pDb, char *fmt, ...);
extern int DBClose(DEBUG_BUFF *pDb, char *filename);

/// initialize the framework itself
extern int InitAudioFramework();

/// Audio Support Thread functions

extern void ASInit();
extern void ASStop();
extern int  ASWriteDebugAudio(ITEM_DEF *, int);
// get audio properties from property store
extern int GetAudioProperties(AUDIO_PROPERTIES *props);
// invalidate the property cache to cause re read of properties
extern void InvalidatePropCache();
// shim to allow app to write properties; since not available from ndk
extern int AudioPropertiesSet(const char *key, int value);
extern void AudioIfStatusRefAvail(int avail);
extern int AudioIfStatusGet(AUDIO_IF_STATUS *pAis);

/// COntrols mic mute
extern void SetMicMute(int);
extern int GetMicMute();

/// Indicators of state of usb channelks
extern void SetUsbMicActive(int inMicActive);
extern int GetUsbMicActive();
extern void SetUsbSpkrActive(int inSpkrActive);
extern int GetUsbSpkrActive();
extern double GetSpeakerRMSLevel();

// Circular buffer allocation routines for file playback
extern CIRC_BUFF *GetPlayCB();
extern void FreePlayCB(CIRC_BUFF *cbPtr);


#if defined(__cplusplus)
}  /* extern "C" */
#endif

#endif
