#include <stdio.h>

#include "libOwlPgVersion.hpp"
#include "owlcore/inc/version.hpp"

/** @return char pointer to a version string for libOwlPG.so. */
char *getLibOwlPgVersion(void)
{
  static char pVersion[256];

  sprintf(pVersion, "%s.%s.%s.%s",
	  LIBOWLPG_VERSION_MAJOR,
	  LIBOWLPG_VERSION_MINOR,
	  LIBOWLPG_VERSION_BUILD,
	  LIBOWLPG_VERSION_TIMESTAMP);
  return pVersion;
}
