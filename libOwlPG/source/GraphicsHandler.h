#ifndef GRAPHICSHANDLER_H
#define GRAPHICSHANDLER_H

/* Graphics Abstraction Library
   Copyright Playground Global 2016 */
#include <EGL/egl.h>
#include "DirectorCommands.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Function Pointer that returns EGLNativeWindow to
   render to for connection to MediaCodec stack */
typedef EGLNativeWindowType (*codecNativeWindowFunc)();

/* Function Pointer for director commands */
typedef director_command (*directorCallback)();

/* Initialize the graphics with target window */
int initGraphics();

/* Initialize callback that provides CodecNativeWindow that OpenGL will draw on */
int registerGraphicsNativeWindowCallback(codecNativeWindowFunc);

/* Add a Director method which will be called to get directions
   This call should not block as it's called within the core draw methods */
int setGraphicsDirector(directorCallback);

/* Camera Frame Callback - to be registered with Camera to get new preview image buffers */
void GraphicsCameraFrameCallback(void *eglClientBuffer);

/* Start the Graphics Processing */
int startGraphics();

/* Stop the Graphics Processing */
int stopGraphics();

#ifdef __cplusplus
}
#endif

#endif
