#!/bin/bash

rm -f jni/PGReferenceApp
ndk-build clean
ndk-build -B
cp obj/local/armeabi-v7a/PGReferenceApp jni/
rm -rf obj
rm -rf libs
