# Uses FlameGraph from https://github.com/brendangregg/FlameGraph.git
./perfhost script -k $OUT/obj/KERNEL_OBJ/vmlinux --symfs $OUT/symbols/ -i perf.data | FlameGraph/stackcollapse-perf.pl | FlameGraph/flamegraph.pl > flamegraph.html
