### Profiling

* Daniel at Playground provided a version of
  [perf](https://perf.wiki.kernel.org/index.php/Main_Page) for
  Android. He also provided a couple scripts to make it easier to run
  perf and analyze the results. From Daniel:

        adb root
        adb remount
        adb push perf /system/bin
        adb push perf.sh /system/bin
        adb shell
        # <run workload>
        # perf.sh
        <... wait>

        To analyze the data (on the host):
        adb root
        adb pull /data/perf.data perf.data
        ./process.sh (uses FlameGraph and Android build environment)

* Here are the contents of perf.sh:

        perf record -g -a -v -o /data/perf.data sleep 30

Note that "-a" records data for the entire system; the "-p <pid>"
option could be used instead to record for a specific process or
thread.

* Here are the contents of process.sh:

        ./perfhost script -k $OUT/obj/KERNEL_OBJ/vmlinux --symfs $OUT/symbols/ -i perf.data | FlameGraph/stackcollapse-perf.pl | FlameGraph/flamegraph.pl > flamegraph.html

Note that [FlameGraph](https://github.com/brendangregg/FlameGraph)
should be installed locally and that $OUT points to a sub-directory of
the Android platform build environment, something like
".../APQ.../out/target/product/msm8916_64".

* Instead of viewing the perf output with FlameGraph, one can run
  `perf report > results.txt` *on the target* to create a text-only,
  human-readable callgraph. This is useful for searching for specific
  function calls, although the callgraph could be very large and
  unwieldy.

* Note that when compiling with ndk-build, the version of the
  application with debug symbols is in
  "android/obj/local/armeabi-v7a/".
