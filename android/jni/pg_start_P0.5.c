#include <unistd.h>
#include <sys/wait.h>
#include "i2crw.c"

// sets up loudspeaker amp
int initAmp(void)
{
  int fd;

  // get handle to amp
  fd = open_i2c(0x4C);

  // send commands to amp
  i2cWriteReg8(fd, 0x0, 0x0);
  i2cWriteReg8(fd, 0xD, 0x10);
  i2cWriteReg8(fd, 0x15, 0x10);
  i2cWriteReg8(fd, 0x18, 0x1);
  i2cWriteReg8(fd, 0x28, 0x0);
  i2cWriteReg8(fd, 0x25, 0x8);  
  i2cWriteReg8(fd, 0x0, 0x1);
  i2cWriteReg8(fd, 0x2, 0x11);
  i2cWriteReg8(fd, 0x0, 0x0);

  // start tinymix
  pid_t pid;

  pid = fork();
  if (pid == 0)
    {
      // Child process
      return execl("/system/bin/tinymix",
		   "/system/bin/tinymix",
		   "578",
		   "1",
		   (char *) NULL );
    }
  else if (pid < 0)
    {return 1;}
  else
    {wait(NULL);}

  sleep(5);

  pid = fork();
  if (pid == 0)
    {
      // Child process
      return execl("/system/bin/tinymix",
		   "/system/bin/tinymix",
		   "474",
		   "1",
		   (char *) NULL );
    }
  else if (pid < 0)
    {return 1;}
  else
    {wait(NULL);}

  sleep(5);

  return 0;
}

int main(void)
{
  pid_t pid;

  // initialize loudspeaker amp
  if(initAmp() != 0)
    { return 1;}

  // setup USB port for UVC & Audio Source
  pid = fork();
  if (pid == 0) 
    {
      // Child process
      return execl("/system/bin/setprop", 
		   "/system/bin/setprop", 
		   "sys.usb.config", 
		   "audio_source,uvc", 
		   (char *) NULL );
    } 
  else if (pid < 0) 
    {return 1;} 
  else 
    {wait(NULL);}
  
  sleep(5);

  // start meetingOwl program
  pid = fork();
  if (pid == 0) 
    {
      // Child process
      return execl("/system/bin/meetingOwl", 
		   "/system/bin/meetingOwl", 
		   (char *) NULL );
    } 
  else if (pid < 0) 
    {return 1;} 
  else 
    {
      // No need to wait
    }
  
  sleep(5);
  
  // start owlAudio program
  pid = fork();
  if (pid == 0) 
    {
      // Child process
      return execl("/system/bin/owlAudio", 
		   "/system/bin/owlAudio", 
		   (char *) NULL );
    } 
  else if (pid < 0) 
    {return 1;} 
  else 
    {wait(NULL);}
  
  return 0;
}
