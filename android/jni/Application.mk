#APP_STL := c++_static  # this makes std::stoi work (but nothing else likes it)
APP_STL := gnustl_static # NOTE: look into using gnustl_shared to eliminate compiler warnings

# Enable c++11 extentions in source code (and more)
APP_CPPFLAGS += -std=c++11 -frtti

# Target hardware(s) to build for
APP_ABI := $(JNI_APP_ABI) # exported by Makefile

