LOCAL_PATH := $(call my-dir)

#--- OpenCV Libs

include $(CLEAR_VARS)
LOCAL_MODULE := libopencv-core-prebuilt
LOCAL_SRC_FILES := ../../../OpenCV-android-sdk/sdk/native/libs/armeabi-v7a/libopencv_core.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libopencv-objdetect-prebuilt
LOCAL_SRC_FILES := ../../../OpenCV-android-sdk/sdk/native/libs/armeabi-v7a/libopencv_objdetect.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libopencv-videoio-prebuilt
LOCAL_SRC_FILES := ../../../OpenCV-android-sdk/sdk/native/libs/armeabi-v7a/libopencv_videoio.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libopencv-imgcodecs-prebuilt
LOCAL_SRC_FILES := ../../../OpenCV-android-sdk/sdk/native/libs/armeabi-v7a/libopencv_imgcodecs.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libopencv-imgproc-prebuilt
LOCAL_SRC_FILES := ../../../OpenCV-android-sdk/sdk/native/libs/armeabi-v7a/libopencv_imgproc.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libopencv-highgui-prebuilt
LOCAL_SRC_FILES := ../../../OpenCV-android-sdk/sdk/native/libs/armeabi-v7a/libopencv_highgui.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libopencv-hal-prebuilt
LOCAL_SRC_FILES := ../../../OpenCV-android-sdk/sdk/native/libs/armeabi-v7a/libopencv_hal.a
include $(PREBUILT_STATIC_LIBRARY)

#--- 3rd Party Libs Needed For OpenCV

include $(CLEAR_VARS)
LOCAL_MODULE := libtbb-prebuilt
LOCAL_SRC_FILES := ../../../OpenCV-android-sdk/sdk/native/3rdparty/libs/armeabi-v7a/libtbb.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libjpeg-prebuilt
LOCAL_SRC_FILES := ../../../OpenCV-android-sdk/sdk/native/3rdparty/libs/armeabi-v7a/liblibjpeg.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libpng-prebuilt
LOCAL_SRC_FILES := ../../../OpenCV-android-sdk/sdk/native/3rdparty/libs/armeabi-v7a/liblibpng.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libtiff-prebuilt
LOCAL_SRC_FILES := ../../../OpenCV-android-sdk/sdk/native/3rdparty/libs/armeabi-v7a/liblibtiff.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libilmImf-prebuilt
LOCAL_SRC_FILES := ../../../OpenCV-android-sdk/sdk/native/3rdparty/libs/armeabi-v7a/libIlmImf.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libjasper-prebuilt
LOCAL_SRC_FILES := ../../../OpenCV-android-sdk/sdk/native/3rdparty/libs/armeabi-v7a/liblibjasper.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libwebp-prebuilt
LOCAL_SRC_FILES := ../../../OpenCV-android-sdk/sdk/native/3rdparty/libs/armeabi-v7a/liblibwebp.a
include $(PREBUILT_STATIC_LIBRARY)

#--- Qualcomm facial processing
# Note that this library causes the following message to be printed to
# the adb shell: "WARNING: linker: libmmcamera_faceproc.so has text
# relocations. This is wasting memory and prevents security
# hardening. Please fix." We appear to be stuck with the warning until
# the library gets recompiled with a more recent ndk release (see
# stackoverflow.com/questions/20141538/).
include $(CLEAR_VARS)
LOCAL_MODULE := libfacialproc-prebuilt
LOCAL_SRC_FILES := libfacialproc/libfacialproc.so
include $(PREBUILT_SHARED_LIBRARY)

#--- libOwlPG
include $(CLEAR_VARS)
LOCAL_MODULE := libOwlPG-prebuilt
LOCAL_SRC_FILES := libOwlPG/libOwlPG.so
include $(PREBUILT_SHARED_LIBRARY)

#--- libspeexdsp_owl
include $(CLEAR_VARS)
LOCAL_MODULE := libspeexdsp_owl-prebuilt
LOCAL_SRC_FILES := libspeexdsp_owl/libspeexdsp_owl.so
include $(PREBUILT_SHARED_LIBRARY)


#--- Owl Core
include $(CLEAR_VARS)
LOCAL_MODULE    := owlCommon
LOCAL_SRC_FILES := $(JNI_SRC_FILES) # JNI_SRC_FILES exported by Makefile
LOCAL_CFLAGS += -I../owlcore/inc/ -Ijni/libjpeg/ -Ijni/libpng/ -Ijni/libOwlPG/ -I../../OpenCV-android-sdk/sdk/native/jni/include/
LOCAL_CFLAGS += -Ijni/libfacialproc
LOCAL_CFLAGS += -I../libOwlPG/source/ -I../speexdsp_owl/include/speex
LOCAL_CFLAGS += -I $(JNI_INCD) # exported by Makefile
LOCAL_CFLAGS += $(JNI_COMPILE_DEFS) # exported by Makefile
LOCAL_STATIC_LIBRARIES := libopencv-videoio-prebuilt libopencv-imgcodecs-prebuilt libopencv-objdetect-prebuilt libopencv-highgui-prebuilt libilmImf-prebuilt libwebp-prebuilt libopencv-imgproc-prebuilt libopencv-core-prebuilt libopencv-hal-prebuilt libtbb-prebuilt libjpeg-prebuilt libtiff-prebuilt libjasper-prebuilt libpng-prebuilt
LOCAL_SHARED_LIBRARIES := libOwlPG-prebuilt
LOCAL_SHARED_LIBRARIES := libspeexdsp_owl-prebuilt
LOCAL_SHARED_LIBRARIES += libfacialproc-prebuilt
include $(BUILD_STATIC_LIBRARY)

#--- Owl Native Application

include $(CLEAR_VARS)
LOCAL_MODULE    := $(JNI_APP) # exported by Makefile
APP_PLATFORM    := $(JNI_APP_PLATORM) # exported by Makefile
LOCAL_SRC_FILES := $(JNI_APP_MAIN)    # ../../owlcore/src/main.cpp
LOCAL_CFLAGS += -I../owlcore/inc/ -Ijni/libOwlPG/ -Ijni/libfacialproc -I../speexdsp_owl/include/speex
LOCAL_CFLAGS += -I../libOwlPG/source/
LOCAL_CFLAGS += -I $(JNI_INCD) # exported by Makefile
LOCAL_CFLAGS += $(JNI_COMPILE_DEFS) # exported by Makefile
LOCAL_CFLAGS += $(JNI_CFLAGS) # exported by Makefile
LOCAL_LDLIBS := -L $(JNI_LIBD) # exported by Makefile
LOCAL_LDLIBS += -lEGL -lGLESv2 -llog -lm -lz -pthread
LOCAL_STATIC_LIBRARIES := libowlCommon
LOCAL_SHARED_LIBRARIES := libOwlPG-prebuilt
LOCAL_32_BIT_ONLY := true # required by libOwlCam (for now)
LOCAL_CFLAGS    += -fPIE
LOCAL_LDFLAGS   += -fPIE -pie
include $(BUILD_EXECUTABLE)

#--- P0.5 Auto-start launcher

include $(CLEAR_VARS)
LOCAL_MODULE    := pg_start_P0.5
LOCAL_SRC_FILES := pg_start_P0.5.c
LOCAL_CFLAGS := -O2
LOCAL_SHARED_LIBRARIES := liblog
LOCAL_32_BIT_ONLY := true
include $(BUILD_EXECUTABLE)

#--- P1 Auto-start launcher

include $(CLEAR_VARS)
LOCAL_MODULE    := pg_start_P1
LOCAL_SRC_FILES := pg_start_P1.c
LOCAL_CFLAGS := -O2
LOCAL_SHARED_LIBRARIES := liblog
LOCAL_32_BIT_ONLY := true
include $(BUILD_EXECUTABLE)
