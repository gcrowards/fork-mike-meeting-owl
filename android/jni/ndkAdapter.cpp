#include <jni.h>
#include <android/log.h>
#include <android/bitmap.h>

#include "logging.hpp"
#include "owlHook.hpp"
#include "params.h"

// native function prototypes (not using a header file)
void ndkInit(JNIEnv* env, jclass clazz, jint textureId);
void ndkUpdate(JNIEnv* env, jclass clazz);
void ndkFinish(JNIEnv* env, jclass clazz);
void ndkCopyGpuOutput(JNIEnv* env, jclass clazz);
int  ndkGetOutputWidth(JNIEnv* env, jclass clazz);
int  ndkGetOutputHeight(JNIEnv* env, jclass clazz);
int  ndkGetCameraWidth(JNIEnv* env, jclass clazz);
int  ndkGetCameraHeight(JNIEnv* env, jclass clazz);

// global hook to Owl application
cOwlHook *gpOwlHook;

jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
  JNIEnv* env;
  if (vm->GetEnv((void **)&env, JNI_VERSION_1_6) != JNI_OK) {
    return -1;
  }
  JNINativeMethod nm[8];
  nm[0].name = "ndkInit";
  nm[0].signature = "(I)V";
  nm[0].fnPtr = (void*)ndkInit;
  nm[1].name = "ndkUpdate";
  nm[1].signature = "()V";
  nm[1].fnPtr = (void*)ndkUpdate;
  nm[2].name = "ndkFinish";
  nm[2].signature = "()V";
  nm[2].fnPtr = (void*)ndkFinish;
  nm[3].name = "ndkCopyGpuOutput";
  nm[3].signature = "()V";
  nm[3].fnPtr = (void*)ndkCopyGpuOutput;
  nm[4].name = "ndkGetOutputWidth";
  nm[4].signature = "()I";
  nm[4].fnPtr = (void*)ndkGetOutputWidth;
  nm[5].name = "ndkGetOutputHeight";
  nm[5].signature = "()I";
  nm[5].fnPtr = (void*)ndkGetOutputHeight;
  nm[6].name = "ndkGetCameraWidth";
  nm[6].signature = "()I";
  nm[6].fnPtr = (void*)ndkGetCameraWidth;
  nm[7].name = "ndkGetCameraHeight";
  nm[7].signature = "()I";
  nm[7].fnPtr = (void*)ndkGetCameraHeight;
  jclass cls = env->FindClass("com/owllabs/meetingOwl/MainView");
  env->RegisterNatives(cls, nm, 8);
  return JNI_VERSION_1_6;
}

void ndkInit(JNIEnv* env, jclass clazz, jint textureId)
{
  // create hook into Owl application
  gpOwlHook = new cOwlHook(textureId);
}

void ndkUpdate(JNIEnv* env, jclass clazz)
{
  // update owl application
  gpOwlHook->update();
}

void ndkFinish(JNIEnv* env, jclass clazz)
{
  gpOwlHook->finish();
}

void ndkCopyGpuOutput(JNIEnv* env, jclass clazz)
{
  // tell owl application it's safe to transfer the GPU image
//  gpOwlHook->copyGpuOutput();
//  TODO: currently, gpOwlHook.update() is doing this copy itself, maybe this
//        is a bad idea?
}

int  ndkGetOutputWidth(JNIEnv* env, jclass clazz)  { return OUTPUT_WIDTH;  }
int  ndkGetOutputHeight(JNIEnv* env, jclass clazz) { return OUTPUT_HEIGHT; }
int  ndkGetCameraWidth(JNIEnv* env, jclass clazz)  { return CAMERA_WIDTH;  }
int  ndkGetCameraHeight(JNIEnv* env, jclass clazz) { return CAMERA_HEIGHT; }
