#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <fcntl.h>

#define MAX_BUF 100
#define TEST_SZ 17

int main(void)
{
  pid_t pid;
  char usb_config[MAX_BUF];
  char test_string[TEST_SZ];
  FILE *fd;

  // Configure speaker amplifier
  pid = fork();
  if (pid == 0)
    {
      // Child process
      return execl("/system/bin/sh",
		   "/system/bin/sh",
		   "-c",
		   "/system/bin/initAudioAmp.sh", /* Note: the script plays the
		                                   * hoot sound, but that has
		                                   * been moved inside
		                                   * meetingOwl, so the owlHoot
		                                   * wav has been deleted to
		                                   * avoid double hoots.
		                                   */
		   (char *) NULL );
    }
  else if (pid < 0)
    {return 1;}
  else
    {wait(NULL);}

  // determine current USB port configuration
  fd = fopen("/sys/class/android_usb/android0/functions", "r");
  if (!fd) {
    return -1;
  }

  fscanf(fd, "%s", usb_config);
  fclose(fd);

  sprintf(test_string, "uvc,audio_source");

  // setup USB port for UVC & Audio Source (only on first execution)
  if (strcmp(usb_config, test_string) != 0) {
    printf("pg_start: usb_config != %s\n", test_string);
    pid = fork();
    if (pid == 0)
      {
        // Child process
        return execl("/system/bin/setprop",
                     "/system/bin/setprop",
                     "sys.usb.config",
                     "uvc,audio_source",
                     (char *) NULL );
      }
    else if (pid < 0)
      {return 1;}
    else
      {wait(NULL);}
  }

  // TEMP HACK
  // redirect stderr to printk because messages are causing SPI comms to miss
  // interrupts, which results in bad mic data
  pid = fork();
  if (pid == 0)
    {
      // Child process
      return execl("/system/bin/sh",
                   "/system/bin/sh",
                   "-c",
                   "echo 2 > /proc/sys/kernel/printk",
                   (char *) NULL );
    }
  else if (pid < 0)
    {return 1;}
  else
    {wait(NULL);}

  // start meetingOwl program
  pid = fork();
  if (pid == 0)
    {
      // Child process
      return execl("/system/bin/meetingOwl",
                   "/system/bin/meetingOwl",
                   (char *) NULL );
    }
  else if (pid < 0)
    {return 1;}
  else
    {
      // wait forever, otherwise Android will restart pg_start!
      wait(NULL);
    }

  return 0;
}

