#ifndef _LIBOWLPG_H_
#define _LIBOWLPG_H_

// list of includes used by libOwlPG
#include "libOwlPgVersion.hpp"
#include "CameraHandler.h"
#include "CodecHandler.h"
#include "AudioHandler.h"
#include "SpeakerHandler.h"
#include "GpuSupport.h"
#include "UVCHandler.h"
#include "DirectorHandler.h"
#include "DirectorCommands.h"
#include "LedHandler.h"
#include "I2cHandler.h"
#include "UVCHandler.h"
#include "ButtonHandler.h"

#endif // _LIBOWLPG_H_
