// Copyright (c) 2015-2016 Playground Global LLC. All rights reserved.
// /dev/i2c-0 read write byte tool
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

#define I2C_SLAVE       0x0703
#define I2C_SMBUS       0x0720
#define I2C_SMBUS_READ  1
#define I2C_SMBUS_WRITE 0
#define I2C_SMBUS_BLOCK_MAX	32
#define I2C_SMBUS_BYTE_DATA	2

union i2c_data
{
  uint8_t  byte;
  uint16_t word;
  uint8_t  block[I2C_SMBUS_BLOCK_MAX + 2];    // block [0] is used for length + one more for PEC
};

struct i2c_ioctl_data
{
  char read_write;
  uint8_t command;
  int size;
  union i2c_data *data;
};

static int i2c_access (int fd, char rw, uint8_t command, int size, union i2c_data *data)
{
  struct i2c_ioctl_data args ;

  args.read_write = rw ;
  args.command    = command ;
  args.size       = size ;
  args.data       = data ;
  return ioctl (fd, I2C_SMBUS, &args) ;
}

static int htoi(char *p)
{
       if ((p[1] == 'x')  || (p[1] == 'X'))
               return(strtol(&p[2], (char **)0, 16));
       else
               return(strtol(p, (char **)0, 16));
}

static int i2cReadReg8 (int fd, int reg)
{
  union i2c_data data;

  if (i2c_access (fd, I2C_SMBUS_READ, reg, I2C_SMBUS_BYTE_DATA, &data))
    return -1;
  else
    return data.byte & 0xFF;
}

static int i2cWriteReg8 (int fd, int reg, int value)
{
  union i2c_data data;

  data.byte = value;
  return i2c_access (fd, I2C_SMBUS_WRITE, reg, I2C_SMBUS_BYTE_DATA, &data);
}

static int open_i2c(const int address) {
	int fd;
	fd = open ("/dev/i2c-0",O_RDWR);
	if (fd<0)
		return fd;
	if (ioctl(fd,I2C_SLAVE,address)<0) {
		fprintf(stderr,"i2crw: not able to select address=%x\n",address);
	}
	return fd;
}

static void usage(void) {
	fprintf(stderr,"i2crw: usage (numbers in hex): i2crw <address> <'R' or 'W'> <reg address> <write byte>\n");
}

/*
int main (int argc, char** argv) {
	int address=0;
	char rw='R';
	int reg;
	int fd;
	int ret;
	if (argc<4) {
		usage();
		return -1;
	}
	rw=argv[2][0]&0x5f;
	if (rw=='W' && argc<5) {
		usage();
		return -1;
	}
	address=htoi(argv[1]);
	reg=htoi(argv[3]);
	fd=open_i2c(address);
	if (fd<0) {
		return -1;
	}
	switch (rw) {
	case 'R':
		ret=i2cReadReg8 (fd, reg);
		fprintf(stdout,"%x:%x=%x\n",address,reg,ret);
		break;
	case 'W':
	{
		int value=htoi(argv[4]);
		i2cWriteReg8 (fd,reg,value);
		break;
	}
	default:
		usage();
		close(fd);
		return -1;
	}
	close(fd);
	return 0;
}

*/
