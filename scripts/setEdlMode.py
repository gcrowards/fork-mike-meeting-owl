#!/usr/bin/python
'''
This script is used to update the EDL flag as part of the meetingOwl Android
build process.
'''

import datetime
import os
import sys

# extract the script path
SCRIPT_PATH = os.path.dirname(os.path.realpath(__file__))

# path to source file
SOURCE_PATH = sys.argv[1] + '/kernel/drivers/power/reset/msm-poweroff.c'

# try to open the source file
try:
    f = open(SOURCE_PATH)
except IOError:
    print 'ERROR: setEdlMode.py unable to open "%s"' % SOURCE_PATH
    sys.exit(-1)

# read the source file contents
lines = f.readlines()
f.close()

# generate true or false revision string based on input argument
if sys.argv[2] == "1":
    revision = "1;	/* 1 enables EDL mode on panic */"
else:
    revision = "0;	/* 1 enables EDL mode on panic */"

# re-open the header for writing; then copy the lines, except update
# the line containing the dowload_mode declaration
f = open(SOURCE_PATH, 'w')
for line in lines:
    if (line.startswith('static int download_mode')):
        # revise the defined portion of the string
        revised_line = line.split('=')
        revised_line[0] += ' '
        revised_line[1] = ' ' + revision
        line = '='.join(revised_line) + '\n'
    f.write(line)
f.close()
