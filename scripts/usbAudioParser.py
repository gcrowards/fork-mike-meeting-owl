import os

class cUSBAudio:
    def __init__(self):
        
        #get all of the USB sound device port names (pactl list sources)
        tCmd = 'pactl list sources | grep sysfs.path | grep usb'
        #tCmd = 'more pactlListSourcesDoubleHub.txt | grep sysfs.path | grep usb'
        fUSBPorts = os.popen(tCmd)
        #create a key list
        self.mKey = []
        
        for line in fUSBPorts:
            s = line.split(':')[-2]
            s = s.split('/')[-1]
            #check if this is a unique address
            if not self.mKey or self.mKey[-1] != s:
                self.mKey.append(s)
                
        fUSBPorts.close()
                
        #get all of the input and output device names
        tCmd = 'pactl list sources | grep "Name:" | grep usb | grep input'
        #tCmd = 'more pactlListSourcesDoubleHub.txt | grep "Name:" | grep usb | grep input'
        fInDevNames = os.popen(tCmd)
        self.mInName = []
        self.mDeviceName = []
        for line in fInDevNames:
            s = line.split('Name: ')
            sDeviceName = (s[1].split('-')[-2]).split('.analog')[0]
##            print sDeviceName
            self.mInName.append(s[1][:-1])
            self.mDeviceName.append(sDeviceName)
        fInDevNames.close()

        #TODO: implement an output sink search function
        #tCmd = 'pactl list sources | grep "Name:" | grep usb | grep output'

        #associate the USB device port names with Alsa names
        i = 0
        self.mdDevSource = {}
        self.mdDevSink = {}
        for p in self.mKey:
            self.mdDevSource[p] = self.mDeviceName[i]
            #self.mdDevSink[p] = self.mOutName[i]
            i+=1
    
    def sGetSources(self):
        strOut = ''
        for key, val in self.mdDevSource.items():
            strOut += (key + ' ' +  val + '\n')
        return strOut

    def sGetSinks(self):
        strOut = ''
        for key, val in self.mdDevSinks.items():
            strOut += (key + ' ' +  val + '\n')
        return strOut

    def sGetSource(self, sName):
        return self.mdDevSource[sName]
        
    def sGetSink(self, sName):
        return self.mdDevSink[sName]


'''USAGE EXAMPLE
usbAudio = cUSBAudio()

for keys, values in usbAudio.mdDevSource.items():
    print keys, values

lSpeaker = lMic

F = open('init.txt','w')

# the file output is a space-deliminted string as follows: 
# <PORT_ADDRESS DEVICE_NAME\n>
F.write(usbAudio.sGetSources())
F.close()

for keys, values in usbAudio.mdDevSource.items():
    print keys, values

print usbAudio.sGetSource('3-1.4.4')
print usbAudio.sGetSink('3-1.4.4')
'''

