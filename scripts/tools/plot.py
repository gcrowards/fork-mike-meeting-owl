#!/usr/bin/python

import csv
from pylab import *
import keyPress
import dsp

# Parameters
MIC_SIGNAL_IDX = 1         # column in CSV file
RAW_SAMPLE_FREQ = 44100    # Hz
SUB_SAMPLE_FREQ = 100      # Hz
RMS_PERIOD = 1.000         # seconds

# Constants
TIMESCALE = 1000 * (1. / RAW_SAMPLE_FREQ)  # ms

PLOT_W = 10
PLOT_H = 7.5

FILENAME = '../../log.csv'

# Read in data
fh = open(FILENAME, 'r')
rawdata = csv.reader(fh, delimiter = ',')

time = []
signal = []

samples = -1
for line in rawdata:
    try:
        signal.append(int(line[MIC_SIGNAL_IDX]))
        time.append(samples * TIMESCALE)
        samples += 1
    except:
        pass

fh.close()

# Process data
subSampledRms = dsp.Root_Mean_Square(size = (SUB_SAMPLE_FREQ * RMS_PERIOD))
subSampleTimes = []
subSampledRmsVals = []

sample = 0
t0 = time[sample]

for rawSignal in signal:
    if(((time[sample] - t0) / 1000.) >= (1. / SUB_SAMPLE_FREQ)):

        t0 = time[sample]

        subSampledRms.AddDataPoint(rawSignal)

        if(subSampledRms.isFull()):
            subSampleTimes.append(time[sample])
            subSampledRmsVals.append(subSampledRms.CalcRms())

    sample += 1

print len(subSampledRmsVals)

# Plot output

# create plotting windows
fig = figure(figsize = (PLOT_W, PLOT_H))

sp1 = subplot(211)
plot(time, signal, label = 'Signal')
grid(b = True, which = 'major')
legend()
title('Raw Signal - ' + FILENAME)
xlabel('time (ms)')
ylabel('value')

sp2 = subplot(212)
plot(subSampleTimes, subSampledRmsVals, label = 'Sub-sampled RMS')
grid(b = True, which = 'major')
legend()
title('Sub-sampled RMS - ' + FILENAME)
xlabel('time (ms)')
ylabel('value')

# Setup key capture

# create function to capture keyboard hits and link to plotting window
key = keyPress.cKeyCapture((sp1, sp2))
fig.canvas.mpl_connect('key_press_event', key.press)

# Display
show()

