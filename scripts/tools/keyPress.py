from pylab import *

class cKeyCapture():

    def __init__(self, plots):
        self.command_active = False
        self.key_buffer = ''
        self.plots = plots

    def press(self, event):

        if(event.key == 'x' or event.key == 'y'):
            self.command_active = True
            self.key_buffer = ''
            self.key_buffer += event.key

        elif(event.key == 'backspace'):
            self.key_buffer = self.key_buffer[:-1]

        elif(event.key == 'enter'):
            self.command_active = False

        elif(self.command_active == True):
            self.key_buffer += event.key

        if(event.key == 'enter'):
            self.execute()

        print self.key_buffer
    
    def execute(self):
        
        command = self.key_buffer[0]
        self.key_buffer = self.key_buffer[1:]

        if(command == 'x'):
            self.resize_axis('x')
        elif(command == 'y'):
            self.resize_axis('y')

    def resize_axis(self, axis):
                                             
        value = self.key_buffer.partition(' ')
        limits = (float(value[0]), float(value[2]))

        for plot in self.plots:
            if(axis == 'x'):
                setp(plot, xlim = limits)
            elif(axis == 'y'):
                setp(plot, ylim = limits)














##            setp(sp1, xlim=(3,4))
##            setp(sp2, xlim=(3,4))
