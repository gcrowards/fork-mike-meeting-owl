#!/bin/bash
# This script adds audio to a video file. 
#
# Video files that are created with OpenCV contain no audio and reading an
# audioless video file can cause errors with OpenCV can sometime cause errors. 
# This script helps fix that problem.
#
# Run this script with two arguments. First, the file name of the audio file you
# want to add to the video. Second, the file name of the audioless video file. 
# The input files should be in your current folder and the output file will
# appear there too with 'wAudio' prepended to the original video file name.
# 
# Note: The audio file should be longer than the video since the output file will
# be the length of the shorter of two input files.  
#
# Example usage: ./addAudioToVideo.sh AudioFile.mp3 VideoFile.avi
# Outputs: new file called wAudioVideoFile.avi in your current folder.

if [ "$#" -ne 2 ]; then
    echo "Illegal number of parameters"
    echo "Example Usage: ./addAudioToVideo.sh AudioFile.mp3 VideoFile.avi"
    echo "Example Output: new file wAudioVideoFile.avi in your current folder."
    exit 1
fi

ffmpeg -i $2 -i $1 -codec copy -shortest wAudio$2
