import collections
from math import sqrt

#####################
# A Low Pass Filter #
#####################
class LPF:
    def __init__(self, filter_constant = 50):
        self.LPF_constant = filter_constant
        self.filtered_data = []

    # Run low pass filter
    def LowPassFilter(self, new_point):
        if(new_point != None):
            if(self.filtered_data != []):
                prev_point = self.filtered_data[-1]
                filtered = (((self.LPF_constant-1)*prev_point + new_point)/self.LPF_constant)
            else:
                filtered = new_point
            
            return(filtered)
        else:
            return(None)
        
    # Clear filtered data
    def Clear(self):
        self.filtered_data = []
        
    # Filter and entire set of data
    def FilterDataSet(self, data_list): 
        # clear out any exisiting filtered data
        self.Clear()
        
        # filter data
        for data in data_list:
            filtered = self.LowPassFilter(data)
            
            if(filtered != None):
                self.filtered_data.append(filtered)
    
    # Filter a single point of data (assumes it's part of the existing data set)
    def FilterDataPoint(self, point):
        filtered = self.LowPassFilter(point)
            
        if(filtered != None):
                self.filtered_data.append(filtered)
            

####################################
# Discrete Difference (Derivative) #
####################################
class Discrete_Diff:
    def __init__(self, data = []):
        self.raw_data = data
        self.derivative = []
        
    def SetRawData(self, data):
        self.raw_data = data
    
    def Append(self, point):
        self.raw_data.append(point)
    
    def Clear(self):
        self.derivative = []
    
    def Differentiate(self, from_index, to_index, greater_than = None, less_than = None):
        # empty previous derivative
        self.Clear()
        
        # set indicies if full differention is requested
        if(from_index == 0 and to_index == 0):
            to_index = len(self.raw_data) - 1
            
        # calculate new derivative
        for i in range(from_index, to_index + 1):
            desc_diff = self.raw_data[i] - self.raw_data[i-1]
            self.derivative.append(desc_diff)
    
        # tests if any of the values are exceede the test limits
        if(greater_than != None and less_than == None):
            for desc_diff in self.derivative:
                if(desc_diff > greater_than):
                    return(True)
        elif(less_than != None and greater_than == None):
            for desc_diff in self.derivative:
                if(desc_diff < less_than):
                    return(True)
        elif(less_than != None and greater_than != None):
            for desc_diff in self.derivative:
                if(desc_diff < less_than or desc_diff > greater_than):
                    return(True)
        else:
            return(True)
        
        return(False)


####################
# Root Mean Square #
####################
class Root_Mean_Square:
    def __init__(self, size = 10):
        self.size = size
        self.data = collections.deque(maxlen = self.size)
        self.rms = 0.

        self.sumOfSquares = 0.

    # Clear filtered data
    def Clear(self):
        self.data = []

    # Add datapoint
    def AddDataPoint(self, dataPoint):
        dSquared = dataPoint * dataPoint

        if(self.isFull()):
            self.sumOfSquares -= self.data[0]

        self.sumOfSquares += dSquared

        self.data.append(dSquared)

    # Get current RMS value
    def CalcRms(self):
        sum = self.sumOfSquares / self.size
        self.rms = sqrt(sum)

        return self.rms

    # See if buffer is fully loaded
    def isFull(self):
        return len(self.data) == self.size
