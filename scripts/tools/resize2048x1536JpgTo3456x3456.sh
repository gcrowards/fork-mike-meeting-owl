#!/bin/bash
# This script resizes a 2048x1536 .jpg input image to a final 3456x3456 for use
# as a MOCK_INPUT image for the Meeting Owl program.
#
# The orig image will be captured from the Chinese microscope imagers used in the
# fedglings but needs to be resized to 3456square while putting the center of the
# fisheye image in center of the output 3456square image in the x-direction. The
# y-direction will be left uncentered. The new center will be calculated and
# saved in the file name.
#
# Example usage: ./resize2048x1536JpgTo3456x3456.sh orig.jpg 980 797
# ...where 980 is the original center in the x-direction 
# ...where 797 is the original center in the y-direction
# Outputs: new file called "center-x1728-y(newCalculatedY).jpg"
#
# Note: the original image should be in the folder that you are currently in when
# you run this command.

if [ "$#" -ne 3 ]; then
    echo "Illegal number of parameters, there should be 3"
    echo "Example Usage: ./resize2048x1536JpgTo3456x3456.sh orig.jpg 980 797"
    echo "...where (for example) orig.jpg is the 2048x1536 input image"
    echo "...where (for example) 980 is the center in the x direction"
    echo "...where (for example) 797 is the center in the y direction"
    exit 1
fi

floatNewY=$(echo "$3*3456/1536" | bc -l)
intNewY=${floatNewY%.*}
outputName="center-x1728-y""$intNewY"".jpg"

ffmpeg -i $1 -filter:v "crop=1536:1536:""$(expr  $2 - 767)"":0" -c:a copy tempOut.jpg
ffmpeg -i tempOut.jpg -vf "scale=3456:3456" "$outputName"

rm tempOut.jpg
