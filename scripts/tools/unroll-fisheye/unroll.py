##################
# This script will unroll a fisheye taken from an Owl camera into a panorama,
#  using the same algorithm and parameters as the meetingOwl program.
#
# Requires installing the PIL library for python.
#  On Ubuntu:
#
#   sudo apt-get install libjpeg libjpeg-dev libfreetype6 libfreetype6-dev zlib1g-dev python-pip
#   pip install PIL
##################
import sys, argparse, math
from PIL import Image
from unrollUtils import *

# parameters (from meetingOwl 3/28/2017)
PANO_SIZE = (4388, 552)              # size of the output panorama
INTERPOLATE = True                   # whether to interpolate when sampling fisheye
THETA_RANGE = (0, 2 * math.pi)       # theta range (panorama horizontal)
RADIUS_WIDTH = 0.25                  # difference between R1 and R0
RADIUS_PADDING = 0.02                # distance from R1 to edge of fisheye image
STRETCH = 0                          # stretch factor
BEND = 0                             # bend factor

if __name__ == "__main__":
    
    # parse command-line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', dest='fisheyePath', type=str,
                        metavar="INPUT_FISHEYE_PATH", default='./fisheye.jpg',
                        help='Specify the path to the fisheye image to unroll.' )
    parser.add_argument('-o', '--output', dest='panoOutPath', type=str,
                        metavar="OUTPUT_PANORAMA_PATH", default='./panorama.png',
                        help='Path to store the output panorama image.' )
    parser.add_argument('centerX', metavar='CENTER_X', type=int,
                        help='Camera X center (in pixels)' )
    parser.add_argument('centerY', metavar='CENTER_Y', type=int,
                        help='Camera Y center (in pixels)' )
    params = parser.parse_args()

    try: # try to open fisheye
        print "Opening fisheye '%s'..." % (params.fisheyePath)
        fisheyeImage = Image.open(params.fisheyePath)
    except IOError:
        # file not found
        print "ERROR: Could not load fisheye image."
        exit()
    
    # calculate other params
    fisheyeSize = fisheyeImage.size
    fisheyeCenter = (params.centerX, params.centerY)
    imageCenter = (fisheyeSize[0] / 2.0, fisheyeSize[1] / 2.0)

    # get minimum distance to an edge of the fisheye
    minRadiusX = min(fisheyeCenter[0], fisheyeSize[0] - fisheyeCenter[0])
    minRadiusY = min(fisheyeCenter[1], fisheyeSize[1] - fisheyeCenter[1])
    minRadius = min(minRadiusX, minRadiusY)

    # calculate the radius range
    r1 = float(minRadius) / float(fisheyeSize[0]) - RADIUS_PADDING;
    r0 = r1 - RADIUS_WIDTH;

    # create the panorama image
    panoImage = Image.new("RGB", PANO_SIZE, 0x00FFFF)
    
    # unroll the fisheye
    print "Unrolling fisheye...\n"
    print "------------------------------"
    print "| Resolution: %dx%d" % PANO_SIZE
    print "| Camera center: (%d, %d)" % fisheyeCenter
    print "| T0: %f, T1: %f" % THETA_RANGE
    print "| R0: %f, R1: %f" % (r0, r1)
    print "| Stretch: %f, Bend: %f" % (STRETCH, BEND)
    print "------------------------------\n"
    unrollFisheye(list(fisheyeImage.getdata()), fisheyeSize,
                  panoImage.load(), PANO_SIZE, fisheyeCenter,
                  THETA_RANGE[0], THETA_RANGE[1], r0, r1,
                  STRETCH, BEND, INTERPOLATE )
   
    try: # try to save the unrolled panorama
        print "Saving unrolled panorama as '%s'..." % (params.panoOutPath)
        panoImage.save(params.panoOutPath)
    except IOError:
        print "Error: Could not save panorama image."
        exit()

    print "Done."
