import math

ERROR_COLOR = 0xFF0FF

# linear interpolation
def lerp(x0, x1, a):
    return (1.0 - a) * float(x0) + a * float(x1)

# linear interpolation for colors (tuples)
def lerpColor(c0, c1, a):
    return (lerp(c0[0], c1[0], a),
            lerp(c0[1], c1[1], a),
            lerp(c0[2], c1[2], a) )

# bilinear interpolation for colors (tuples)
def bilinearInterpColor(tlC, trC, blC, brC, p):
    cOut = lerpColor(lerpColor(tlC, blC, p[1]),
                     lerpColor(trC, brC, p[1]),
                     p[0] )
    return (int(cOut[0]), int(cOut[1]), int(cOut[2]))

# clamps a value into the given range
def clamp(x, low, high):
    return min(high, max(low, x))

# samples an image at the given texture coordinates
def sampleImage(imageArr, imageSize, texPos, interpolate):
    if texPos[0] < 0.0 or texPos[0] > 1.0 or texPos[1] < 0.0 or texPos[1] > 1.0 :
        print("Error (sampleImage()): texPos must be in the range [0.0, 1.0].")
        return ERROR_COLOR

    pixPosF = ((texPos[0] * imageSize[0]), (texPos[1] * imageSize[1]))
    pixPos = (int(texPos[0] * imageSize[0]), int(texPos[1] * imageSize[1]))
    
    index = (pixPos[1] * imageSize[0] +
             int(texPos[0] * imageSize[0]) )
    
    if interpolate:
        # use bilinear interpolation sampling
        tl = imageArr[index]
        tr = imageArr[index + 1]
        bl = imageArr[index + imageSize[0]]
        br = imageArr[index + imageSize[0] + 1]
        return bilinearInterpColor(tl, tr, bl, br,
                                   (pixPosF[0] - pixPos[0],
                                   pixPosF[1] - pixPos[1] ))
    else:
        # use nearest-point sampling
        return imageArr[index]

# unrolls a fisheye image into a panorama using the same algorithm as meetingOwl
def unrollFisheye(fisheye, fisheyeSize, panoOut, panoSize, cameraCenter,
                  theta0, theta1, r0, r1, stretch, bend, interpolate ):
    cameraCenter = (float(cameraCenter[0]) / float(fisheyeSize[0]),
                    float(cameraCenter[1]) / float(fisheyeSize[1]) )

    for fX in range(panoSize[0]):
        texX = fX / float(panoSize[0])
        for fY in range(panoSize[1]):
            texY = fY / float(panoSize[1])

            # the following was copy/pasted from meetingOwl's unroll shader code
            #  and modified to run in python.
            rnorm = texY
            rnorm = rnorm - stretch * (1.0 - rnorm) * rnorm
            rnorm = rnorm + bend * (1.0 - rnorm) * pow(texX - 0.5, 2.0)
            r =  clamp(r0 + rnorm * (r1 - r0), 0.0, 0.5)
            theta = -theta0 - texX * (theta1 - theta0)
            pos = (cameraCenter[0] + r * math.cos(theta),
                   cameraCenter[1] + r * math.sin(theta) )
            
            panoOut[fX, fY] = sampleImage(fisheye, fisheyeSize, pos, interpolate)
