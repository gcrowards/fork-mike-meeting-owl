#!/usr/bin/python

import zmq
import time
import sys
from numbers import Number

# ZMQ comms stuff
ZMQ_IPC_LINK = "ipc:///tmp/micData"

class cMicComms:

    def __init__(self, socketLink):
        self.socketLink = socketLink

        self.context = zmq.Context()
        self.socket =  self.context.socket(zmq.PAIR)

        self.socket.bind(self.socketLink)

    def sendDirection(self, direction):
        data = str(direction) + '\n'

#        try:
        print("sending message %s" % data)
        self.socket.send(data)#, zmq.NOBLOCK)
        print "message sent"
 #       except:
 #           pass


####### Start Main #######

micComms = cMicComms(ZMQ_IPC_LINK)

keyboardData = None

while(keyboardData != "q"):
    keyboardData = raw_input('Sound source angle (0 - 360): ')

    try:
        soundSourceAngle = int(keyboardData)
        
        # WARNING: This line is here becaue there's a hack in micComms.cpp
        # that inverts the angle received. This is because the angle that is
        # sent from the Arduino is in a different coordinate system than what
        # owl-core uses.  The mics have theta increasing CCW.  owl-core 
        # increases CW now (it used to not).  Eventually the hack should be
        # removed and then this line can also be removed.
        soundSourceAngle = 360 - soundSourceAngle
        # WARNING

        micComms.sendDirection(soundSourceAngle)
    except:
        continue

    if(keyboardData == 'q'):
        sys.exit(0)
