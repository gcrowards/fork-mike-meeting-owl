#!/usr/bin/python

import csv
from pylab import *
import keyPress
import dsp

# Parameters
MIC_DIRECTION_IDX = 0      # column in CSV file
MIC_SIGNAL_IDX = 1         # 
MIC_HPF_IDX = 2            #

RAW_SAMPLE_FREQ = 44100    # Hz
SUB_SAMPLE_FREQ = 100      # Hz
RMS_PERIOD = 1.000         # seconds

# Constants
#TIMESCALE = 1000 * (1. / RAW_SAMPLE_FREQ)  # ms
TIMESCALE = 2

PLOT_W = 10
PLOT_H = 7.5

FILENAME = '../../log.csv'

# Read in data
fh = open(FILENAME, 'r')
rawdata = csv.reader(fh, delimiter = ',')

time0 = []
time90 = []
time180 = []
time270 = []

mic0 = []
mic90 = []
mic180 = []
mic270 = []

hpf0 = []
hpf90 = []
hpf180 = []
hpf270 = []

maxVal = 0;

samples = -1
for line in rawdata:
    try:
        if(line[MIC_DIRECTION_IDX] == '0'):
            if(len(line) != 3): break

            mic0.append(pow(int(line[MIC_SIGNAL_IDX]), 2))

            if(mic0[-1] > maxVal): maxVal = mic0[-1]

            hpf0.append(pow(float(line[MIC_HPF_IDX]), 2))

            time0.append(samples * TIMESCALE)    
            samples += 1
        elif(line[MIC_DIRECTION_IDX] == '90'):
            if(len(line) != 3): break

            mic90.append(pow(int(line[MIC_SIGNAL_IDX]), 2))

            if(mic90[-1] > maxVal): maxVal = mic90[-1]

            hpf90.append(pow(float(line[MIC_HPF_IDX]), 2))

            time90.append(samples * TIMESCALE)
            samples += 1
        elif(line[MIC_DIRECTION_IDX] == '180'):
            if(len(line) != 3): break

            mic180.append(pow(int(line[MIC_SIGNAL_IDX]), 2))

            if(mic180[-1] > maxVal): maxVal = mic180[-1]

            hpf180.append(pow(float(line[MIC_HPF_IDX]), 2))

            time180.append(samples * TIMESCALE)
            samples += 1
        elif(line[MIC_DIRECTION_IDX] == '270'):
            if(len(line) != 3): break

            mic270.append(pow(int(line[MIC_SIGNAL_IDX]), 2))

            if(mic270[-1] > maxVal): maxVal = mic270[-1]

            hpf270.append(pow(float(line[MIC_HPF_IDX]), 2))

            time270.append(samples * TIMESCALE)
            samples += 1
    except:
        pass

maxVal *= 1.25

fh.close()

# Plot output

# create plotting windows
fig = figure(figsize = (PLOT_W, PLOT_H))

# ALL ON ONE PLOT
##sp1 = plot()
##plot(time0, mic0, label = '0 degree mic')
##grid(b = True, which = 'major')
##
##plot(time90, mic90, label = '90 degree mic')
##grid(b = True, which = 'major')
##
##plot(time180, mic180, label = '180 degree mic')
##grid(b = True, which = 'major')
##
##plot(time270, mic270, label = '270 degree mic')
##grid(b = True, which = 'major')
##
##legend()

# INDIVIDUAL SUB-PLOTS
sp1 = subplot(411)
plot(time0, mic0, label = '0 degree mic')
plot(time0, hpf0)
xlim([0, time0[-1]])
ylim([0, maxVal])
grid(b = True, which = 'major')
legend()

sp2 = subplot(412)
plot(time90, mic90, label = '90 degree mic')
plot(time90, hpf90)
xlim([0, time90[-1]])
ylim([0, maxVal])
grid(b = True, which = 'major')
legend()

sp3 = subplot(413)
plot(time180, mic180, label = '180 degree mic')
plot(time180, hpf180)
xlim([0, time180[-1]])
ylim([0, maxVal])
grid(b = True, which = 'major')
legend()

sp4 = subplot(414)
plot(time270, mic270, label = '270 degree mic')
plot(time270, hpf270)
xlim([0, time270[-1]])
ylim([0, maxVal])
grid(b = True, which = 'major')
legend()

title('Mic Data ' + FILENAME)
xlabel('time (ms)')
ylabel('value')

# Setup key capture

# create function to capture keyboard hits and link to plotting window
key = keyPress.cKeyCapture((sp1, sp2, sp3, sp4))
fig.canvas.mpl_connect('key_press_event', key.press)

# Display
show()

