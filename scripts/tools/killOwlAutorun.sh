# Run this script right after plugging in device

echo "Waiting for device..."
adb wait-for-device root
echo "Remounting..."
adb wait-for-device remount
echo "Waiting for device remount..."
echo "Once adb-shell is opened, run the following commands:"
echo "# cd system/bin"
echo "# mv pg_start pg_startX"
echo "# sync"
echo "# exit"
adb wait-for-device shell
echo "Rebooting device..."
adb reboot
echo "Complete!"

