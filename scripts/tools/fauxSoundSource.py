#!/usr/bin/python

import os
import subprocess
import sys

keyboardData = None


# check for linux or adb
linux = adb = False
if len(sys.argv) == 2:
    if sys.argv[1] == '--linux':
        linux = True
    elif sys.argv[1] == '--adb':
        adb = True

# usage
if linux == False and adb == False:
    print '\nUsage: ./fauxSoundSource.py <platform>'
    print '   platform is either --linux or --adb\n'
    sys.exit(0)

# if linux, do a little work to locate the sim file more robustly
if linux:
    linux_path = os.path.dirname(os.path.realpath(__file__))
    if '/tools' in linux_path:
        linux_path = linux_path + '/..'
    if '/scripts' in linux_path:
        linux_path = linux_path + '/..'

# prep the system command, assuming the filepath corresponds to what
# was set in owlcore/inc/params.h; THIS WILL BREAK IF THE VALUE IN
# params.h CHANGES
# TODO: parse params.h to find the filepath
if adb:
    CMD = 'adb shell "echo %d > /data/simPcmAngle.txt"'
else:
    CMD = 'echo %s > %s/simPcmAngle.txt' % ("%d", linux_path)
print "using shell command '%s'" % CMD

# loop until quit
while(keyboardData != "q"):
    keyboardData = raw_input('Sound source angle (0 - 360; negative for silence; "q" to quit): ')

    try:
        soundSourceAngle = int(keyboardData)
        subprocess.call(CMD % soundSourceAngle, shell=True)
    except:
        continue

    if(keyboardData == 'q'):
        sys.exit(0)
