#!/usr/bin/python

import commandLineHelper
from usbAudioParser import cUSBAudio

# this is where all the magic happens
usbAudio = cUSBAudio()

# find out what physical port the user is interested in
commandLineParser = commandLineHelper.commandLineProcessor()
port = commandLineParser.busAndPortLocation

# write result to file
FILE = open('micName.txt','w')
FILE.write("hw:CARD=" + usbAudio.sGetSource(port))
FILE.close()

