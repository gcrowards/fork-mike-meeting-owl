import argparse

def commandLineProcessor():
    # command line parser
    parser = argparse.ArgumentParser(description = 'command line parser')

    # positional args
    parser.add_argument('busAndPortLocation', action='store', 
                        help='USB bus/port location of the device in question: ex: "1-2.3"')
    
    # parse
    args = parser.parse_args()

    return args
