#!/usr/bin/python
'''
This script is used to update the version header file as part of
the libOwlPG build process.
'''

import datetime
import os
import sys


# extract the script path
SCRIPT_PATH = os.path.dirname(os.path.realpath(__file__))

# path to header file
HEADER_PATH = SCRIPT_PATH + '/../' + 'owlcore/inc/version.hpp'

# try to open the header file
try:
    f = open(HEADER_PATH)
except IOError:
    print 'ERROR: autoRevision.py unable to open "%s"' % HEADER_PATH
    sys.exit(-1)

# read the header contents
lines = f.readlines()
f.close()

# generate revision timestamp in UTC
revision = datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
    
# re-open the header for writing; then copy the lines, except update
# any line with a timestamp
f = open(HEADER_PATH, 'w')
for line in lines:
    if line.startswith('#define') and 'TIMESTAMP' in line:
        # revise the portion between double quotes
        revised_line = line.split('"')
        if len(revised_line) == 3:
            revised_line[1] = revision
            line = '"'.join(revised_line)
            print 'autoRevision TIMESTAMP set to %s' % revision
    f.write(line)
f.close()
