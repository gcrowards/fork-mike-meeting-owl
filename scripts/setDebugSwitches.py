#!/usr/bin/python
'''
This script is used to update the debug switches as part of the meetingOwl
build process.
'''

import datetime
import os
import sys

# extract the script path
SCRIPT_PATH = os.path.dirname(os.path.realpath(__file__))

# path to header file
HEADER_PATH = SCRIPT_PATH + '/../' + 'owlcore/inc/debugSwitches.h'

# try to open the header file
try:
    f = open(HEADER_PATH)
except IOError:
    print 'ERROR: setDebugSwitches.py unable to open "%s"' % HEADER_PATH
    sys.exit(-1)

# read the header contents
lines = f.readlines()
f.close()

# generate true or false revision string based on input argument
if sys.argv[1] == "1":
    revision = "true"
else:
    revision = "false"

# re-open the header for writing; then copy the lines, except update
# any line with a timestamp
f = open(HEADER_PATH, 'w')
for line in lines:
    if (line.startswith('#define GENERAL')):
        # revise the defined portion of the string
        revised_line = line.split()
        if len(revised_line) == 3:
            offset = 29 - (len(revised_line[0]) + len(revised_line[1]))
            revised_line[1] += ' ' * offset
            revised_line[2] = revision
            line = ' '.join(revised_line) + '\n'
    f.write(line)
f.close()
