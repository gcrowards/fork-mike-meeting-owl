import os
import re

class cUDEVParser:
    def __init__(self):
        
        #get all of the USB sound device port names (pactl list sources)
        
        tCmd = 'ls /dev/ | grep video'
        #tCmd = 'more lsusb-tDoubleHub.txt'
        
        fVideoNames = os.popen(tCmd)
        self.mlVideoSources = []
        self.mdVidAddr = {}
        
        #find all of the device names, strip off everything other than the name
        for l in fVideoNames:
            self.mlVideoSources.append(l.split('\n')[0])
            
        fVideoNames.close()
        tCmd = 'udevadm info -a -p $(udevadm info -q path -n /dev/'
        for v in self.mlVideoSources:
            fUdevInfo = os.popen(tCmd + v + ")")
            
            for l in fUdevInfo:
                #walk the device's udev tree and make sure it's conncted to USB
                if "looking at device" in l and "usb" in l:
                    #print l.split("'")[1]
                    s = l.split("'")[1]
                    s = s.split(":")
                    s = s[-2].split("/")[-1]

                    #performr regex search for a digit
                    matchObject = re.search("\d", v)

                    #create a device dictionary {USB addr:X}, where X is the
                    #numeric portion of 'videoX' (X represents a number)
                    self.mdVidAddr[s] = v[matchObject.start():]
            fUdevInfo.close()

''' Usage example
udev = cUDEVParser()

for l in udev.mlVideoSources:
    print l

print udev.mdVidAddr
'''
