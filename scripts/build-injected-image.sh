#!/bin/bash

if [ "$ANDROID_BUILD_ROOT" == "" ]; then
	echo "Please enter your Android Build root with no trailing slash"
	echo "Example: /home/user/Owl-Android"
	echo "(Run 'export ANDROID_BUILD_ROOT=<your path>' to avoid this step)"
	read ANDROID_BUILD_ROOT
fi

TOP=$ANDROID_BUILD_ROOT
HOME=$PWD
OUT_TGT=$TOP/out/target/product/meetingowl
echo "ANDROID_BUILD_ROOT=$TOP; HOME=$HOME; OUT_TGT=$OUT_TGT"

echo "Creating system/media/OwlLabs directory in OUT_TGT..."
mkdir $OUT_TGT/system/media/OwlLabs/

echo "Copying Meeting Owl components to Android output directory..."
echo "gitHash.txt.../system/media/OwlLabs/"
cp $HOME/../gitHash.txt $OUT_TGT/system/media/OwlLabs/

# Use 'copyTargetAssets' script to copy all the assets (shared with Makefile)
./copyTargetAssets.sh "cp" "$HOME/.." "$OUT_TGT"

echo "meetingOwl.../system/bin/"
cp $HOME/../android/libs/armeabi-v7a/meetingOwl $OUT_TGT/system/bin/

echo "Meeting Owl OS: enable application auto-run on boot? [Default: Yes]"
select yn in "Yes" "No"; do
	case $yn in
		Yes ) echo "pg_start_P1.../system/bin/pg_start";
		      rm $OUT_TGT/system/bin/pg_startX;
		      cp $HOME/../android/libs/armeabi-v7a/pg_start_P1 $OUT_TGT/system/bin/pg_start;
		      break;;
		No )  echo "pg_start_P1.../system/bin/pg_startX";
		      rm $OUT_TGT/system/bin/pg_start;
		      cp $HOME/../android/libs/armeabi-v7a/pg_start_P1 $OUT_TGT/system/bin/pg_startX;
		      break;;
		* )   echo "pg_start_P1.../system/bin/pg_start";
		      rm $OUT_TGT/system/bin/pg_startX;
		      cp $HOME/../android/libs/armeabi-v7a/pg_start_P1 $OUT_TGT/system/bin/pg_start;
		      break;;
	esac
done

echo "Do you need to update the build ID? [Default: No]"
select yn in "Yes" "No"; do
	case $yn in
		Yes ) rm $OUT_TGT/system/build.prop*;
		      python setBuildID.py $ANDROID_BUILD_ROOT;

		      break;;
		No )  break;;
		* )   break;;
	esac
done

echo "Enable EDL mode on kernel panic (No = auto-reboot on kernel crash)? [Default: No]"
select yn in "Yes" "No"; do
	case $yn in
		Yes ) python setEdlMode.py $ANDROID_BUILD_ROOT 1;
		      break;;
		No )  python setEdlMode.py $ANDROID_BUILD_ROOT 0;
		      break;;
		* )   python setEdlMode.py $ANDROID_BUILD_ROOT 0;
		      break;;
	esac
done

echo "Enable ADB mode? [Default: No]"
select yn in "Yes" "No"; do
	case $yn in
		Yes ) python setAdbMode.py $ANDROID_BUILD_ROOT 1;
		      break;;
		No )  python setAdbMode.py $ANDROID_BUILD_ROOT 0;
		      break;;
		* )   python setAdbMode.py $ANDROID_BUILD_ROOT 0;
		      break;;
	esac
done

echo "Enable serial console? [Default: No]"
select yn in "Yes" "No"; do
	case $yn in
		Yes ) python setSerialMode.py $ANDROID_BUILD_ROOT 1;
		      break;;
		No )  python setSerialMode.py $ANDROID_BUILD_ROOT 0;
		      break;;
		* )   python setSerialMode.py $ANDROID_BUILD_ROOT 0;
		      break;;
	esac
done

echo "Copying build-owl script to Android build root..."
cp build-owl.sh $TOP/

echo "Executing build-owl script..."
cd $TOP && ./build-owl.sh

echo "Copying image files to build directory..."
cp $OUT_TGT/boot.img $HOME/../android/build/images/
cp $OUT_TGT/cache.img $HOME/../android/build/images/
cp $OUT_TGT/userdata.img $HOME/../android/build/images/
cp $OUT_TGT/system.img $HOME/../android/build/images/
cp $OUT_TGT/persist.img $HOME/../android/build/images/
cp $OUT_TGT/recovery.img $HOME/../android/build/images/
echo ""

