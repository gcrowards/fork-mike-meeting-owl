#!/usr/bin/python
'''
This script is used to update the build ID as part of the meetingOwl system
build process.
'''

import datetime
import os
import sys

# extract the script path
SCRIPT_PATH = os.path.dirname(os.path.realpath(__file__))

# path to header file
HEADER_PATH = sys.argv[1] + '/build/core/build_id.mk'

# try to open the header file
try:
    f = open(HEADER_PATH)
except IOError:
    print 'ERROR: setBuildID.py unable to open "%s"' % HEADER_PATH
    sys.exit(-1)

# read the header contents
lines = f.readlines()
f.close()

# re-open the header for writing; then copy the lines, except update
# any line with a timestamp
f = open(HEADER_PATH, 'w')
for line in lines:
    if line.startswith('export BUILD_ID'):
        # revise the defined portion of the string
        revised_line = line.split('=')
        if len(revised_line) == 2:
            print 'Current BUILD_ID = %s' % revised_line[1]
            # Add logic to avoid accidentally writing a NULL string to
            # build_id.mk
            while True:
                try:
                    revision = int(input('Enter new BUILD_ID: '))
                except NameError:
                    # Non-integer entries call a new prompt
                    print("Sorry, BUILD_ID must be an integer.")
                    continue
                except SyntaxError:
                    # Un-intelligle entries need not apply
                    print("Please enter an integer greater than 0.")
                    continue
                except TypeError:
                    # Data types can cause another type of error
                    print("Please enter an integer greater than 0.")
                    continue
                except KeyboardInterrupt:
                    # Break out and use existing value
                    break
                if revision < 1:
                    # Negative or 0 entries call a new prompt
                    print("Sorry, BUILD_ID must be greater than 0.")
                    continue
                else:
                    # Compose the revised line only when we get the right value
                    revised_line[1] = str(revision)
                    break
            # If no entry was received, we should just use the original BUILD_ID
            line = '='.join(revised_line) + '\n'
    f.write(line)
f.close()
