#!/system/bin/sh

#####################################################################################
##
##  This script prints the version number stored in /persist/WCNSS_qcom_wlan_nv.bin
##
##  Format:  XX.XX.XX.XX
##
#####################################################################################

VERSION_OFFSET=1281   # HEX offset of version
VERSION_SIZE=4        # number of bytes in version

# print version, formatted with a period every 2 characters
VERSION_NUM=$(busybox hexdump -v -s $VERSION_OFFSET -n $VERSION_SIZE -e '/1 "%02x"' /persist/WCNSS_qcom_wlan_nv.bin)
VERSION_STR=$(echo $VERSION_NUM | busybox sed 's/../&./g;s/.$//')
echo "$VERSION_STR"
