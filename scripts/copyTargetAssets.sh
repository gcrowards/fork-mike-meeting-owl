#!/bin/bash

# Copy files listed in input text file
# Call this script with 3 arguments: copyList.sh command src_root dst_root
#   'command' is the command to execute on each line of the file list.
#   'src_root' is the relative path to get to the meeting owl root directory.
#   'dst_root' is the path to prepend on the destination paths.
# Each line of FILE_LIST includes the filename to copy and the directory 
# to copy into.

# Script parameters
CMD=$1      # Command to apply to each line in FILE_LIST
HOME=$2     # Directory prefix to attach to the source file name
OUT_TGT=$3  # Directory prefix to attach to the destination path

# Set the global input field separator to a newline for parsing file list
IFS="
"

# The following list contains files to copy to the target
# Each line contains the file and its target directory
# Each file contains the path from the meeting-owl directory
# Each target directory is relative to the root on the target
FILE_LIST="\
android/jni/libOwlPG/libOwlPG.so system/lib/ 
android/jni/libfacialproc/libfacialproc.so system/lib/
android/jni/libspeexdsp_owl/libspeexdsp_owl.so system/lib/
assets/panoramic3456-01.jpg system/media/OwlLabs/
assets/panoramic3456-02.jpg system/media/OwlLabs/
assets/panoramic1728-01.jpg system/media/OwlLabs/
assets/panoramic1728-02.jpg system/media/OwlLabs/
assets/owlHaloIcon.png system/media/OwlLabs/
assets/owlStartupIcon.png system/media/OwlLabs/
assets/owlPinnedAoi.png system/media/OwlLabs/
assets/micOnIcon.png system/media/OwlLabs/
assets/micOffIcon.png system/media/OwlLabs/
assets/micMuteIcon.png system/media/OwlLabs/
assets/videoMuteIcon.png system/media/OwlLabs/
assets/speakerOnIcon.png system/media/OwlLabs/
assets/speakerOffIcon.png system/media/OwlLabs/
assets/bannerLine.png system/media/OwlLabs/
assets/startupBackground.png system/media/OwlLabs/
assets/startupFirstTime.png system/media/OwlLabs/
assets/startupNextTime.png system/media/OwlLabs/
assets/owlFirstPowerUp.wav system/media/OwlLabs
assets/owlWiFiUnconfig.wav system/media/OwlLabs
assets/volumeBlip.wav system/media/OwlLabs/
assets/owlHoot-48kHz.wav system/media/OwlLabs/
assets/owlHoot-NewVer-48kHz.wav system/media/OwlLabs/
haarcascade_frontalface_alt.xml system/media/OwlLabs/
inputParams.cfg system/media/OwlLabs/
system-test/config/micTest-system.conf system/media/OwlLabs/
system-test/assets/PinkFilt3min48k-3dB.wav system/media/OwlLabs/
system-test/assets/PinkFilt3min48k-6dB.wav system/media/OwlLabs/
system-test/assets/WhiteFilt3min48k-3dB.wav system/media/OwlLabs/
system-test/assets/WhiteFilt3min48k-6dB.wav system/media/OwlLabs/
scripts/nv-version.sh system/bin/"

# This loop formats the command, prints it to the termainal and
# executes it, as well
for line in $FILE_LIST
do
	SRC=$(echo $line | cut -f1 -d \ )  # get the file name from line
	DST=$(echo $line | cut -f2 -d \ )  # get the dest path from line
	# paste together the command line from pieces
	CMDLINE=$(eval echo "$1 ${HOME}/${SRC} ${OUT_TGT}/${DST}")
	echo $CMDLINE    # show what we're going to do for this line
	eval $CMDLINE    # and actually do it
done


