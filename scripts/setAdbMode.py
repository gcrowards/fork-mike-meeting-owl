#!/usr/bin/python
'''
This script is used to update the ADB mode as part of the meetingOwl Android
build process.
'''

import datetime
import os
import sys

# extract the script path
SCRIPT_PATH = os.path.dirname(os.path.realpath(__file__))

# path to source files
OWL_PATH = sys.argv[1] + '/device/owllabs/meetingowl/init.usb.rc'
QCOM_PATH = sys.argv[1] + '/device/qcom/common/rootdir/etc/init.qcom.usb.sh'

# Modify the source file in device/owllabs first
try:
    f = open(OWL_PATH)
except IOError:
    print 'ERROR: setAdbMode.py unable to open "%s"' % OWL_PATH
    sys.exit(-1)

# read the source file contents
lines = f.readlines()
f.close()

# generate true or false revision string based on input argument
if sys.argv[2] == "1":
    revision = "import /init.owl.adb.rc"
else:
    revision = "# import /init.owl.adb.rc"

# re-open the init file for writing; then copy the lines, except update
# the line containing the import of init.owl.adb.rc
f = open(OWL_PATH, 'w')
for line in lines:
    if ("import /init.owl.adb.rc" in line):
        # replace line
        line = revision + '\n'
    f.write(line)
f.close()

# Now modify the source file in device/qcom
try:
    f = open(QCOM_PATH)
except IOError:
    print 'ERROR: setAdbMode.py unable to open "%s"' % QCOM_PATH
    sys.exit(-1)

# read the source file contents
lines = f.readlines()
f.close()

# generate true or false revision string based on input argument
if sys.argv[2] == "1":
    revision = "                            setprop persist.sys.usb.config diag,serial_smd,rmnet_bam,adb"
    CMP_REV = '    "" | "adb" | "none" | "uvc,audio_source") #USB persist config not set, select default configuration'
else:
    revision = "                            setprop persist.sys.usb.config none"
    CMP_REV = '    "" | "adb" | "diag,serial_smd,rmnet_bam,adb" | "uvc,audio_source") #USB persist config not set, select default configuration'

# re-open the init file for writing; then copy the lines, except update
# the line containing the persistent usb state
f = open(QCOM_PATH, 'w')
for line in lines:
    if ("setprop persist.sys.usb.config diag,serial_smd,rmnet_bam,adb" in line):
        # replace line
        line = revision + '\n'
    elif ("setprop persist.sys.usb.config none" in line):
        # replace line
        line = revision + '\n'
    elif ('    "" | "adb"' in line):
	line = CMP_REV + '\n'
    f.write(line)
f.close()
