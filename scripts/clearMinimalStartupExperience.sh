#!/bin/bash

echo "Waiting for Owl to enter adb mode; if Owl is currently running, please disconnect power, wait 10 seconds, and reconnect power..."
adb wait-for-device root && adb wait-for-device remount && adb wait-for-device shell "mkdir -p /data/OwlLabs/Params/StartupExp" && adb wait-for-device shell "rm /data/OwlLabs/Params/StartupExp/Minimize" 

