#!/usr/bin/python
'''
This script uses adb to pull the latest log file and then does a
diff with the one specified as a command-line argument.
'''

import sys
import time
from subprocess import Popen, PIPE

def do_diff(ref_log_file, dev_log_file):
    print "----- diff %s %s -----" % (ref_log_file, dev_log_file)
    cmd = 'diff %s %s' % (ref_log_file, dev_log_file)
    p = Popen(cmd.split(), stdin=PIPE, stdout=PIPE, stderr=PIPE)
    output, err = p.communicate()
    print output
    
def get_device_log_file_name():
    # list /sdcard/ directory
    cmd = 'adb shell ls /sdcard/'
    p = Popen(cmd.split(), stdin=PIPE, stdout=PIPE, stderr=PIPE)
    output, err = p.communicate()
    output = output.split()
    # pull out log files
    logs = [s for s in output if s.startswith('owl-') and s.endswith('.log')]
    logs.sort() # ensure list is sorted by timestamp
    return logs[-1] # last one is most recent

def pull_log_file(src_file, dst_file):
    print "pulling '%s' from device..." % src_file
    cmd = 'adb pull /sdcard/%s %s' % (src_file, dst_file)
    p = Popen(cmd.split(), stdin=PIPE, stdout=PIPE, stderr=PIPE)
    output, err = p.communicate()
    
if __name__=='__main__':
    # check usage
    if len(sys.argv) != 2:
        print 'usage: adbLogDiff <reference log file>'
        sys.exit()
    # set the reference log file
    ref_log_file = sys.argv[1]
    # get name of most recent log file on device
    dev_log_file = get_device_log_file_name()
    if not dev_log_file:
        print 'no log file found on device'
        sys.exit()
    # pull the file to a local temporary file
    pull_log_file(dev_log_file, dev_log_file)
    # do the diff
    do_diff(ref_log_file, dev_log_file)
