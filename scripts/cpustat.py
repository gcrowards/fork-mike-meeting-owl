#!/usr/bin/python
'''
This script is based on info at the following links:
http://askubuntu.com/questions/120953/exact-field-meaning-of-proc-stat
http://stackoverflow.com/questions/23367857
'''

import sys
import time
from subprocess import Popen, PIPE

ANDROID = True

if ANDROID:
    CMD_PROC_STAT = 'adb shell cat /proc/stat'
    CMD_PID_STAT  = 'adb shell dumpsys cpuinfo'
else:
    CMD_PROC_STAT = 'cat /proc/stat'
    CMD_PID_STAT  = ''


def get_proc_stat():
    # get contents of system stat file
    p = Popen(CMD_PROC_STAT.split(), stdin=PIPE, stdout=PIPE, stderr=PIPE)
    output, err = p.communicate()
    output = output.split('\n')
    # make a dictionary, one key per cpu core plus totals
    proc_stat = {}
    for o in output:
        if not o.startswith('cpu'):
            continue
        o = o.split()
        # set the cpu name
        cpu = o[0]
        if cpu == 'cpu':
            cpu = 'all'   # rename
        else:
            cpu = cpu[-1] # just grab the core number
        # make a dictionary of field values
        proc_stat[cpu] = {}
        proc_stat[cpu]['usr']     = int(o[1])
        proc_stat[cpu]['nice']    = int(o[2])
        proc_stat[cpu]['sys']     = int(o[3])
        proc_stat[cpu]['idle']    = int(o[4])
        proc_stat[cpu]['iowait']  = int(o[5])
        proc_stat[cpu]['irq']     = int(o[6])
        proc_stat[cpu]['soft']    = int(o[7])
        proc_stat[cpu]['steal']   = int(o[8])
        proc_stat[cpu]['guest']   = int(o[9])
        proc_stat[cpu]['gnice']   = int(o[10])
    return proc_stat

def show_cpu_stats(before, after, label='--'):
    # construct list of cpus
    n_cpus = len(before.keys()) - 1
    cpus = ['all'] + ['%d' % i for i in range(n_cpus)]
    # list the field names in desired order (same order as mpstat)
    fields = ['usr', 'nice', 'sys', 'iowait', 'irq', 'soft', 'steal', 'guest', 'gnice', 'idle']
    # print header
    print '%-12s CPU' % label,
    for field in fields:
        field_label = '%%%s' % field
        print '%7s' % field_label,
    print ''
    # print stats per cpu
    for cpu in cpus:
        print '%-12s %3s' % (label, cpu),
        total = 0
        for field in fields:
            value = after[cpu][field] - before[cpu][field]
            total += value
        for field in fields:
            value = after[cpu][field] - before[cpu][field]
            print '%7.2f' % (100. * value / total),
        print ''
    print ''

def show_pid_stats(n_cpus, n=10):
    # get output of "dumpsys cpuinfo"
    p = Popen(CMD_PID_STAT.split(), stdin=PIPE, stdout=PIPE, stderr=PIPE)
    output, err = p.communicate()
    output = output.split('\n')
    # print header line
    print output[1]
    # loop over processes
    total = 0.0
    for o in output[2:(2+n)]:
        o = o.strip().split()
        percent = float(o[0].strip('%')) / n_cpus
        total += percent
        process = o[1].split('/')
        pid = process[0]
        command = process[1].strip(':')
        print '   %5.1f%% %6s %s' % (percent, pid, command)
    print '---------\n   %5.1f%%' % total


if __name__=='__main__':
    # check usage
    if len(sys.argv) != 3:
        print 'usage: cpustat <interval> <count>'
        sys.exit()
    # set interval and count
    interval = int(sys.argv[1])
    count = int(sys.argv[2])
    # initialize raw stats
    print 'gathering cpu stats...'
    start = get_proc_stat()
    if 'all' not in start:
        print 'ERROR: unable to parse /proc/stat'
        if ANDROID:
            print 'check device connection and adb server'
        sys.exit(0)
    before = start.copy()
    # main loop
    for i in range(count):
        time.sleep(interval)
        label = time.strftime('%H:%M:%S %p')
        after = get_proc_stat()
        show_cpu_stats(before, after, label)
        before = after
    show_cpu_stats(start, after, 'Average:')
    if ANDROID:
        n_cpus = len(before.keys()) - 1
        show_pid_stats(n_cpus)
