#!/bin/bash

# Configuration Parameters
#VIEWER="/usr/bin/guvcview -d "
VIEWER="/usr/bin/vlc v4l://"
MEETING_OWL_PROG="/system/bin/pg_startX"

# Internal Global Script Variables
AW="[a-z,A-Z,0-9]*"     # Any Word including numbers
DT="[0-9]{1,3}"         # Any one to three digit number
IP_SED_CMD="s/$AW\s*$AW\s*(($DT\.){3}$DT).*/\1/p" # 2 words then IP Addr
VIEWER_APP=`echo $VIEWER | sed -nr "s/.*\/(.*) .*/\1/p"`


# Function returns Owl IP address in first argument if found, otherwise null string
function GetOwlIP() {  # ARG1 is the output variable
  local _outvar=$1
  local _ip=""
  local _WLAN0=""
  while [ -z $_ip ]; do
    adb wait-for-device shell netcfg > ./netInterfaces.tmp
    _WLAN0=`grep wlan0 ./netInterfaces.tmp`
    rm ./netInterfaces.tmp
    _ip=`echo $_WLAN0 | sed -nr $IP_SED_CMD`
    # echo $_ip
  done
  eval $_outvar=\$_ip
}

# Function returns Owl IP or times out
function GetOwlIPWithTimeout() {
  # First parameter, $1, is timeout value
  # Second parameter, $2 is returned IP string
  local _timeout=$1
  local _outvar=$2
  local _time_cnt=0
  local _lip=""
  GetOwlIP _lip
  while [ -z "$_lip" -o "$_lip" = "0.0.0.0" ]; do
    let "_time_cnt = _time_cnt + 1"
    if [ $_time_cnt -gt $_timeout ]; then
      eval $_outvar=\$_lip
      return 1
    fi
    sleep 1
    GetOwlIP _lip
    echo "IP:"$_lip
  done

  eval $_outvar=\$_lip
  return 0
}


# Function finds the device number of the Meeting Owl camera device
function FindMeetingOwlDevice() {
  # First parameter, $1, is returned device name string
  local _outvar=$1
  local _device=""
  local _dev_num=""

  for dev in /sys/class/video4linux/*; do
    _dev_name=`cat $dev/name`
    echo "Checking dev: $dev, name: $_dev_name"
    if [ "$_dev_name" = "Meeting Owl" ]; then
      _dev_num=`echo $dev | sed -nr "s/.*(.)/\1/p"`
    fi
  done

  if [ -n "$_dev_num" ]; then
    _device="/dev/video$_dev_num"
    eval $_outvar=\$_device
    echo "FindMeetingOwlDevice: "$_device
    return 0
  else
    return 1
  fi
}


# Function returns Owl device number or times out
function FindMeetingOwlDeviceWithTimeout() {
  # First parameter, $1, is timeout value
  # Second parameter, $2 is returned device number string
  local _timeout=$1
  local _outvar=$2
  local _time_cnt=0
  local _ldev=""
  FindMeetingOwlDevice _ldev
  while [ $? -ne 0 ]; do
    let "_time_cnt = _time_cnt + 1"
    if [ $_time_cnt -gt $_timeout ]; then
      eval $_outvar=\$_ldev
      return 1
    fi
    sleep 1
    FindMeetingOwlDevice _ldev
  done

  eval $_outvar=\$_ldev
  return 0
}


# Function returns PID of the ADB process used to run Owl
function FindAdbPid() {
  # First parameter, $1, is returned PID
  local _outvar=$1
  local _ladb_pid=0
  local _grep_out

  _grep_out=`ps aux | grep bash | grep adb | grep pg_start`
  _ladb_pid=`echo $_grep_out | tr -s " " | cut -d " " -f 2`
  # echo "_ladb_pid:"$_ladb_pid      # Debug

  eval $_outvar=\$_ladb_pid
  return 0
}


# Run Owl for testing:
#  -Create network adb connection to Owl
#  -Start a terminal window showing output of running pg_startX on Owl
#  -Start switchboardTest connected to Owl in another terminal window
#  -Start guvcview connected to Owl video
#  -Wait for user to kill the Owl adb window
#  -Then kill the other two windows
#  -Finally reboot the Owl
function RunMeetingOwl() {
  local _return=0

  adb kill-server
  adb start-server
  echo "Waiting for Owl..."
  GetOwlIPWithTimeout 30 IP
  if [ $? -ne 0 ]; then
    echo "Failed to find the Meeting Owl IP address...Provision Owl!"
    return 1
  fi
  adb wait-for-device root
  sleep 2
  adb tcpip 5555
  adb wait-for-device connect $IP
  ADB_CMD="/usr/bin/adb -s $IP:5555 wait-for-device shell $MEETING_OWL_PROG"
  DONE_PRMT="\nPress <Enter> to close this shell. "
  # echo "ADB_CMD:"$ADB_CMD   # Debug
  # Run pg_startX on the owl and show in its own terminal
  x-terminal-emulator -e /bin/bash -c "$ADB_CMD; printf \"$DONE_PRMT\"; read" &

  sleep 1  # Allow time for Owl to start up meetingOwl program
  _pwd=`pwd`
  _cmd=$_pwd/../switchboardTest
  if [ ! -a "$_cmd" ]; then
    _cmd=$_pwd/switchboardTest
  fi
  # Run switchboardTest connected to Owl and show in another terminal
  x-terminal-emulator -e "$_cmd -c $IP" &

  FindMeetingOwlDeviceWithTimeout 60 MEETING_OWL_DEV
  if [ $? -ne 0 ]; then
    echo "Failed to find the Meeting Owl camera...quitting!"
    FindAdbPid _adb_pid
    if [ -n "$_adb_pid" ]; then
      kill -n 9 $_adb_pid
    fi
    _return=2
  fi
  echo "Meeting Owl device:"$MEETING_OWL_DEV
  # Start up video viewer application
  # Note: the two shell variables are intentionally directly abutting;
  #  this is needed to compose the vlc command line properly!
  $VIEWER$MEETING_OWL_DEV >> /dev/null 2>&1 &

  sleep 1
  _adb_pid="1"
  echo "Press <Ctrl>-c in the Meeting Owl shell window to end test."
  # Wait for user to kill the meetingOwl window with <Ctrl>-c
  while [ -n "$_adb_pid" ]; do
    FindAdbPid _adb_pid
    sleep 1
  done

  # Kill switchboardTest and the viewer
  _viewer_pid=`pgrep $VIEWER_APP`
  if [ -n "$_viewer_pid" ]; then
    kill -n 9 $_viewer_pid
  fi
  _switchboardTest_pid=`pgrep switchboardTest`
  if [ -n "$_switchboardTest_pid" ]; then
    kill -n 15 $_switchboardTest_pid
  fi

  # Reboot the owl to prepare for next test
  echo "Rebooting Owl"
  adb -s $IP:5555 wait-for-device shell reboot &
  sleep 2

  # Reset ADB for the next round
  adb kill-server
  adb start-server

  echo "debugOwl.sh done!"
  return $_return
}


# Run it!
RunMeetingOwl

