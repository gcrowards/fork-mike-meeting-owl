#!/bin/bash

echo "Waiting for Owl to enter adb mode; if Owl is currently running, please disconnect power, wait 10 seconds, and reconnect power..."
adb wait-for-device root && adb wait-for-device remount && adb wait-for-device shell "cd /system/bin && mv pg_start pg_start_dev"
