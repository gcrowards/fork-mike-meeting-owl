#!/system/bin/sh

############################################################################
#                                                                          #
# Update ssid and psk to match your router (possibly proto and key_mgmt as #
# well) and push this script to Android device.                            #
#                                                                          #
# NOTE: You must run this script upon each boot.                           #
############################################################################

# make sure WiFi is enabled
WIFI_ON="$(settings get global wifi_on)"

if [ ${WIFI_ON} != 1 ]
    then
    echo "enabling wifi"
    svc wifi enable
    sleep 2
fi

# issue wpa_cli commands to get on the network
WPACLI="wpa_cli -p /data/misc/wifi/sockets/ -i wlan0"

# Add new WiFi network
NETWORK=$($WPACLI add_network)

$WPACLI set_network $NETWORK auth_alg OPEN 
$WPACLI set_network $NETWORK key_mgmt WPA-PSK 
$WPACLI set_network $NETWORK ssid '"26"'
$WPACLI set_network $NETWORK proto WPA
$WPACLI set_network $NETWORK mode 0
$WPACLI set_network $NETWORK psk '"MochiBean"'

# Connect to it
$WPACLI select_network $NETWORK
$WPACLI enable_network $NETWORK
$WPACLI reassociate 
sleep 1
$WPACLI reassociate 

# Check the status
sleep 2
$WPACLI status

