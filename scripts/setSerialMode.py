#!/usr/bin/python
'''
This script is used to update the serial mode as part of the meetingOwl Android
build process.
'''

import datetime
import os
import sys

# extract the script path
SCRIPT_PATH = os.path.dirname(os.path.realpath(__file__))

# path to device tree source
DT_PATH = sys.argv[1] + '/kernel/arch/arm/boot/dts/qcom/msm8916-pinctrl.dtsi'

# Modify the pin control file
try:
    f = open(DT_PATH)
except IOError:
    print 'ERROR: setSerialMode.py unable to open "%s"' % DT_PATH
    sys.exit(-1)

# read the source file contents
lines = f.readlines()
f.close()

# generate true or false revision string based on input argument
if sys.argv[2] == "1":
    # set pin mode to UART
    revision = "			qcom,pin-func = <2>;"
else:
    # set pin mode to GPIO
    revision = "			qcom,pin-func = <0>;"

trigger = 0

# re-open the device tree file for writing; then copy the lines, except update
# the line containing the uart pin mode declaration
f = open(DT_PATH, 'w')
for line in lines:
    if ("pmx-uartconsole {" in line):
        trigger = 1
    if (trigger):
        if ("qcom,pin-func" in line):
            line = revision + '\n'
            trigger = 0
    f.write(line)
f.close()

