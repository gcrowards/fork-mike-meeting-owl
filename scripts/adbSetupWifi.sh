#!/bin/bash

####
# This script sets up an adb connection to an owl over wifi.
#
# Make sure the owl is connected to the wifi network via the Meeting Owl app
#  before running this script for a newly-flashed owl.
####

# parameters
TCP_PORT=5555 # the port to connect over
IP_ATTEMPTS=2 # number of times to try getting the owl's IP address.

# disconnect adb over wifi if already running
adb -e disconnect &>/dev/null

# start adb in root mode.
echo "Waiting for an adb device to be connected..."
adb wait-for-device && adb root
adb wait-for-device && adb remount
adb wait-for-device

# get the owl's IP address
attempt=1
until [ $IP_ATTEMPTS -le 0 ]; do
    DEVICEIP=`adb shell "netcfg" | grep wlan0`
    DEVICEIP=`echo "$DEVICEIP" | awk -v col=3 '{print $col}' | cut -f1 -d"/"`
    
    echo "Attempting to connect to the owl (attempt #$attempt)..."
    echo "     IP address: $DEVICEIP"

    if [ "$DEVICEIP" == "0.0.0.0" -o "$DEVICEIP" == "" ]; then
	echo "     Failed -- $IP_ATTEMPTS attempts left"
	IP_ATTEMPTS=$((IP_ATTEMPTS - 1))
	attempt=$((attempt + 1))
	sleep 2
    else
	echo "     Success."
	break
    fi
done

# make sure the IP address is valid
if [ "$DEVICEIP" == "" ]; then
    echo "Error: Failed to get the owl's IP address."
    echo "       Have you set up wifi with the Meeting Owl app?."
    exit 1
elif [ "$DEVICEIP" == "0.0.0.0" ]; then
    echo "Error: The owl doesn't appear to be connected to wifi. "
    echo "       Have you set up wifi with the Meeting Owl app?."
     exit 1
fi

# set adb to use TCP port 5555.
adb shell "setprop service.adb.tcp.port $TCP_PORT"

# restart the adb daemon on the device.
adb shell "start adbd"

# connect to the device over TCP.
adb wait-for-device
adb tcpip $TCP_PORT
adb connect "$DEVICEIP"

# open an adb shell over wifi
adb -e wait-for-device
adb -e shell
