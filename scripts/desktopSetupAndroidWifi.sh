#!/bin/bash

# WARNING: This script is not usable because their is an issue with sending
#          the network's password to wpa_cli via the adb shell command (it
#          works if you're actually in the Android device's console).  It
#          seems like a bug in wpa_cli, and there maybe a work-around, but
#          I haven't found it yet. -mark

############################################################################
#                                                                          #
# Update ssid and psk to match your router (possibly proto and key_mgmt as #
# well) and run this script from your desktop machine to get the Android   #
# device on your wireless router                                           #
#                                                                          #
# NOTE: You must run this script upon each boot.                           #
############################################################################

# make sure WiFi is enabled
WIFI_ON=$(adb shell settings get global wifi_on)

if [ ${WIFI_ON} != 1 ]
    then
    echo "enabling wifi"
    adb shell svc wifi enable
    sleep 2
fi

WPACLI="adb shell wpa_cli -p /data/misc/wifi/sockets/ -i wlan0"

# Add new WiFi network
NETWORK=$($WPACLI add_network)

$WPACLI set_network $NETWORK auth_alg OPEN 
$WPACLI set_network $NETWORK key_mgmt WPA-PSK 
$WPACLI set_network $NETWORK ssid '"26"'
$WPACLI set_network $NETWORK proto WPA
$WPACLI set_network $NETWORK mode 0
$WPACLI set_network $NETWORK psk '"MochiBean"'

# Connect to it
$WPACLI select_network $NETWORK
$WPACLI enable_network $NETWORK
$WPACLI reassociate 
$WPACLI reassociate 

# Check the status
sleep 2
$WPACLI status

