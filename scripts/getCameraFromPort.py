#!/usr/bin/python

'''
This script will find all of the video devices and their associated USB
address by initializing videoDevices with the cUDEVParser class.  It
will then accept a command line address (BUS-Port.Port, ex: 3-2.1) and
return the video device name to a text file called videoName.txt

USAGE EXAMPLE: python getUSBVideoDevNumber.py 3-2
'''


import commandLineHelper
from udevParser import cUDEVParser

#find all of the videoX devices in /dev/ & their USB port addresses
videoDevices = cUDEVParser() 
commandLineParser = commandLineHelper.commandLineProcessor()

port = commandLineParser.busAndPortLocation

if 1:
    FILE = open('cameraIndex.txt','w')
    FILE.write(videoDevices.mdVidAddr[port])
    FILE.close()

