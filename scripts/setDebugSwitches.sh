#!/bin/bash

echo "Meeting Owl: enable debug switches? [Default: No]"
select yn in "Yes" "No"; do
	case $yn in
		Yes ) python setDebugSwitches.py 1; break;;
		No )  python setDebugSwitches.py 0; break;;
		* )   python setDebugSwitches.py 0; break;;
	esac
done

