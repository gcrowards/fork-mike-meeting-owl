**To build full Meeting Owl documentation clone this repo and run doxygen in root 
of the clone.  Be sure to install graphviz first, so that you can view the 
collaboration charts, which are very helpful.**

/** \mainpage Meeting Owl P0 Application Documentation
 *
 * \section intro_sec Introduction
 *
 * The basic concept behind Meeting Owl (for this rev) is that it can see and
 * hear all around the room, and that it is intelligent enough to automatically
 * choose what section, or sections, of the room to focus it's attention on. The
 * output of this focus are sequention frames of video and audio that enable the
 * viewer to undertand what's happening in the room.
 *
 * For this P0 prototype the Owl focuses its attention on the people that it
 * believes are active, or recent, conversation participants.  As such, the
 * trigger mechanism for focus is a sound source that is believed to be a person
 * speaking.  When such a source is detected, the Owl searches in the direction
 * of the source an attempts to find a person's face.  If a face is found, that
 * becomes the center of attention.  If no face can be found then the direction
 * of the sound source is used as the center of attention. When there are 
 * multiple sound sources (found sequentially in time) the output video is 
 * subdivided so that multiple participants can be seen at once.  This is called
 * "Stage Sharing."  Participants that have not spoken for some time cease to be
 * considered active and will be removed from the display.  When there are no
 * active speakers the panormaic view of the room is shown.
 *
 * Currently, all of the above is implemented in a very rudimentary fashion.
 *
 * In the future, the system will be made to be smarter about what it focuses
 * on.  For example, if there are only two or three people in the room it may
 * choose to always display all of the people.  Or, if two people are sitting in
 * close proximity it may choose to display them together with one wider angle
 * shot, instead of cropping out the individuals and displaying them side-by-
 * side.  As another example, the system may notice that there is one person in
 * the room who is running the meeting, so Owl may choose to always display that
 * individual, even if he/she hasn't spoken in a while.  Eventually, it is
 * intended that Owl will be able to focus on more things that people, for 
 * example, whiteboards, television display, and objects that are being 
 * referenced by a speaker in the meeting.
 *
 * \section debug_sec Debug output control
 *
 * Please use /inc/debugSwitches.h to control debugging output.  debugSwitches.h 
 * contains instruction on usage.
 *
 * When checking in code to "shared" branches (e.g. develop) please be sure to
 * wrap your debug code with DEBUG() macro and turn off the flag(s) that enable
 * your debug code.
 *
 * \section git_sec Git workflow explanation
 *
 * There are two core branches:
 *
 * "Master" contains the lastest stable "release."  At this point code is merged 
 * into master when very significant and broad improvements have been made and 
 * are well tested.  Generally these denote demo-able milestones and the commits
 * are tagged as demo-able and with the date of the first demonstration.
 *
 * "Develop" contains the latest "cutting egde" version of stable code.  It 
 * includes new features are they are implemented and working.  It's not quite
 * as reliable as master, but it should always compile and run properly, and
 * usually be reliable.
 *
 * Feature branches should be created off of develop and merged back into
 * develop when they are implemented and working reaonably well and reliably. 
 * These branches should be deleted by the engineer who created them, once the
 * branch is merged back into develop and/or is no longer needed.
 *
 * \subsection Sanctuary 
 *
 * The Owl Sanctuary is a repository where individual engineers can mess around
 * with whatever they want, and leave behind indefinitely whatever mess they 
 * make.
 *
 * \section Logging
 *
 * In logging.hpp, several macros are defined to provide an interface
 * to the application's logging functions. These macros are LOGV(),
 * LOGD(), LOGI(), LOGW(), and LOGE(). They respectively correspond
 * to the logging levels used by Android: verbose, debug, info,
 * warning, and error. Also in logging.hpp, LOG_LEVEL is defined to
 * control which logging levels appear in the log output. Be sure to
 * run 'make clean' if you ever change the value assigned to
 * LOG_LEVEL.
 *
 * One more macro, LOGF(), is used to direct log messages to a
 * timestamped file only. Note that LOGV() only directs log messages
 * to stdout, whereas all of the other macros direct log messages to
 * both stdout and the file, assuming LOG_LEVEL is set
 * accordingly. Also note that the compile option LOG_TO_FILE must be
 * defined in Makefile to enable logging to a file.
 *
 * The log file supports a basic form of software testing since a
 * developer can run a diff on two log files and look for unexpected
 * changes. The script adbLogDiff.py makes this a little easier by
 * pulling the most recent log file off an adb device and running the
 * diff with a given reference log file.
 *
 * At the time of this writing, it is not yet clear what kinds of
 * testing will be done. Additional infrastruture might be needed to
 * make testing easier. Possibilities include a global mechanism to
 * trigger event-based logging and some sort of scripts that automate
 * testing with different compile flags or test data files.
 *
 * \section Automated Testing
 *
 * See the separate documentation in testing/README.md.
 *
 */
