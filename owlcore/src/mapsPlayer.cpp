#include "common.hpp"
#include "logging.hpp"
#include "mapsPlayer.hpp"

cMapsPlayer::cMapsPlayer(void)
{
  init("", "");
}

cMapsPlayer::cMapsPlayer(std::string personDataFilename,
			 std::string voiceDataFilename )
{
  init(personDataFilename, voiceDataFilename);
}

cMapsPlayer::~cMapsPlayer()
{
}

/** Do basic initialization.
 *
 * @param personDataFilename is the path to the CSV file containing time series 
 * person map data.
 * @param voiceDataFilename is the path to the CSV file containing time series 
 * voice map data.
 */
void cMapsPlayer::init(std::string personDataFilename,
		       std::string voiceDataFilename )
{
  mPlayEveryFrame = false;
  // default is to playback based on the real elasped time (which may mean some 
  // data is skipped, if the playback system is not fast enough)
  
  setPersonDataFilename(personDataFilename);
  setVoiceDataFilename(voiceDataFilename);  

  reset();
}

/** This function kicks off the Player.
 *
 * If *not* playing back every frame, once this function is called the clock
 * is running.
 *
 * @param time is the point in time (seconds, assumed) in the data set to start 
 * the Player from (zero would typically be the beginning).
 */
void cMapsPlayer::start(float time)
{
  // keep it clean
  reset();

  // note the wall time right now
  currentTimeSpec(&mStartTime);

  // read in all data that the Player will need
  loadData();

  mPersonTableIterator = mPersonMapTable.begin();
  mVoiceTableIterator = mVoiceMapTable.begin();

  // move to the commanded start time in the playback data
  jumpTo(time);
}

/** Move to the given time in the loaded data set.
 *
 * @param time is the point in time (seconds, assumed) in the data set.
 */ 
void cMapsPlayer::jumpTo(float time)
{
  struct timespec now;
  currentTimeSpec(&now);

  // fudge mStartTime so that the next elpased-time-since calculation will 
  // return the time that was passed into this function)
  time_t sec = (time_t)time;
  long nsec = (time - sec) * 10E9L;

  mStartTime.tv_sec = now.tv_sec - sec;
  mStartTime.tv_nsec = now.tv_nsec - nsec;

  if(mPlayEveryFrame)
    { playEveryFrame(); } // this moves iterators to new time
}

/** Reset the Player. */
void cMapsPlayer::reset(void)
{
  clearData();
}

/** Switch Player into mode where every entry of data is played.
 *
 * This mode ensures that every frame of data in the source files is played
 * back, but it means that the playback time may not match the originally 
 * intended playback time (e.g. if the playback system is too slow).
 */
void cMapsPlayer::playEveryFrame(void)
{
  mPlayEveryFrame = true;  

  // set iterators to the current time-based location in the data set
  float time = (float)elapsedTimeSinceSec(mStartTime);

  mPersonTableIterator = mPersonMapTable.lower_bound(time);
  mVoiceTableIterator = mVoiceMapTable.lower_bound(time);
}

/** Switch Player into mode where the elapsed wall time dictates which frame
 * of data is provided by the Player at any given moment.
 */
void cMapsPlayer::playRealTime(void)
{
  mPlayEveryFrame = false;

  // get the current time from the current frame
  float time = (*mPersonTableIterator).first;
  
  jumpTo(time);
}

/** Get current playback time. 
 * 
 * The specific meaning of the playback time is dependent on whether the 
 * player is playing back in real-time or forcing each data frame to be played
 * back.
 *
 * @return the current time into the playback.
 */
double cMapsPlayer::getTime(void)
{
  if(mPlayEveryFrame)
    { // time according to data set
      return (*mPersonTableIterator).first;
    }
  else
    { // real time since playback began
      return elapsedTimeSinceSec(mStartTime);
    }
}

/* @return Person Map at the current playback time or position. */
std::vector<cItem> cMapsPlayer::getPersonMap(void)
{
  std::vector<cItem> emptyMap = std::vector<cItem> {};

  // if there's no data to work with return empty vectory
  if(mPersonMapTable.size() == 0) { return emptyMap; }

  if(!mPlayEveryFrame)
    {
      float time = (float)elapsedTimeSinceSec(mStartTime);
      
      LOGV("%4.3f s (playback time)\n", time);

      itemTable_t::iterator iPersonMap = mPersonMapTable.lower_bound(time);
      
      if(iPersonMap != mPersonMapTable.begin())
	{ --iPersonMap; }
      
      return (*iPersonMap).second;
    }
  else // play every frame
    {
      mPersonTableIterator++;
      
      LOGV("%4.3f s (playback time)\n", (*mPersonTableIterator).first);
      
      if(mPersonTableIterator != mPersonMapTable.end())
	{ return (*mPersonTableIterator).second; }
      else
	{ return emptyMap; }
    }
}

/* @return Voice Map at the current playback time or position. */
std::vector<cVoice> cMapsPlayer::getVoiceMap(void)
{
  std::vector<cVoice> emptyMap = std::vector<cVoice> {};

  // if there's no data to work with return empty vectory
  if(mVoiceMapTable.size() == 0) { return emptyMap; }

  if(!mPlayEveryFrame)
    {
      float time = (float)elapsedTimeSinceSec(mStartTime);
            
      voiceTable_t::iterator iVoiceMap = mVoiceMapTable.lower_bound(time);
      
      if(iVoiceMap != mVoiceMapTable.begin())
	{ --iVoiceMap; }
      
      return (*iVoiceMap).second;
    }
  else // play every frame
    {    
      mVoiceTableIterator++;
            
      if(mVoiceTableIterator != mVoiceMapTable.end())
	{ return (*mVoiceTableIterator).second; }
      else
	{ return emptyMap; }
    }
}

/** Set the full path of the Person Map CSV file.
 *
 * @param personDataFilename is the full path of the CSV data file.
 */ 
void cMapsPlayer::setPersonDataFilename(std::string personDataFilename)
{
  mPersonDataFilename = personDataFilename;
}

/** Set the full path of the Voice Map CSV file.
 *
 * @param voiceDataFilename is the full path of the CSV data file.
 */ 
void cMapsPlayer::setVoiceDataFilename(std::string voiceDataFilename)
{
  mVoiceDataFilename = voiceDataFilename;
}

/** Parse input data and generate Person and Voice Map tables.
 *
 * Parse CSV files into table of string data, and then convert that to a
 * table of numerical data.  The numerical data is used to generate a map 
 * table where each entry represents the map state at a specific time.
 *
 * @return true if person and voice maps are both successfully generated.
 */
bool cMapsPlayer::loadData(void)
{
  clearData();

  owl::numericalTable_t<float> personMapRawTable;
  owl::numericalTable_t<float> voiceMapRawTable;

  // parse person data CSV file
  cCsvParser parser(mPersonDataFilename);

  if(!parser.parse())
    {
      LOGE("ERROR: Couldn't parse mock person map data file.\n");
      return false; 
    }
   
  // convert string data to numerical data
  parser.fillNumericalTable(&personMapRawTable);
  
  // parse voice data CSV file
  parser.setFilename(mVoiceDataFilename);

  if(!parser.parse())
    {
      LOGE("ERROR: Couldn't parse mock voice map data file.\n");
      return false; 
    }

  // convert string data to numerical data
  parser.fillNumericalTable(&voiceMapRawTable);

  // turn numerical data tables into Person & Voice Map tables
  if(createPersonMapTable(personMapRawTable) &&
     createVoiceMapTable(voiceMapRawTable) )
    { return true; }
  else
    { return false; }
}

/** Clear Person and Voice Map Tables. */
void cMapsPlayer::clearData(void)
{
  mPersonMapTable.clear();
  mVoiceMapTable.clear();
}

/** Generate Person Map table.
 *
 * This table is a map that uses time as the key and Person Map state as the
 * associated value.
 *
 * @param rawTable is a list of numerical data that the map table is to be
 * built from.
 *
 * @return true the map table was built successfully.
 */
bool cMapsPlayer::createPersonMapTable(owl::numericalTable_t<float> rawTable)
{
  // indices into the data (these names reflect what is expected in each field)
  typedef enum {TIME, BEARING, WIDTH} eDataField_t;
  
  eDataField_t field;

  // convert data into a map of Person Map states
  for(auto row : rawTable)
    {
      std::vector<cItem> personMap;
      field = TIME;
      
      // data that is used to build each entry into the C++ map
      float time, bearing, width;

      // each row contains a time (key) followed by an arbitrary number of
      // bearings and width pairs (each of which represents a person on the
      // map)
      for(auto data : row)
	{
	  switch(field)
	    {
	    case TIME:
	      time = data;
	      field = BEARING;
	      break;
	      
	    case BEARING:
	      bearing = data;
	      field = WIDTH;
	      break;
	      
	    case WIDTH:
	      width = data;
	      field = BEARING;
	     
	      // add to list of people on the map at this time
	      cItem person(bearing, width);
	      personMap.push_back(person);
	      
	      break;
	    }
	}

      if(!(mPersonMapTable.insert(std::make_pair(time, personMap))).second)
	{ // "second" member of the returned std::pair is false if the 
	  // provided key duplicates a key already in the map
	  LOGE("ERROR: Duplicate time (%f) key in mock person map data!\n",
	       time );

	  return false;
	}
    }

  return true;
}

/** Generate Voice Map table.
 *
 * This table is a map that uses time as the key and Voice Map state as the
 * associated value.
 *
 * @param rawTable is a list of numerical data that the map table is to be
 * built from.
 *
 * @return true the map table was built successfully.
 */
bool cMapsPlayer::createVoiceMapTable(owl::numericalTable_t<float> rawTable)
{
  // indices into the data (these names reflect what is expected in each field)
  typedef enum {TIME, BEARING} eDataField_t;
  
  eDataField_t field;

  // convert data into a map of Voice Map states
  for(auto row : rawTable)
    {
      std::vector<cVoice> voiceMap;
      field = TIME;
      
      // data that is used to build each entry into the C++ map
      float time, bearing;
      
      // each row contains a time (key) followed by an arbitrary number of
      // bearings (though there should be only one or zero), representing the 
      // Voice Map state at the given time. 
      for(auto data : row)
	{
	  switch(field)
	    {
	    case TIME:
	      time = data;

	      if(row.size() > 1)
		{ field = BEARING; }
	      else
		{ /* do nothing, there's no sound source */ }

	      break;
	      
	    case BEARING:
	      bearing = data;
	      field = BEARING;

	      float fauxScore = 10; // arbitrary value
	      std::vector<int> fauxAudioData;

	      // add to list of people on the map at this time
	      cVoice voice(bearing, fauxScore, fauxAudioData);
	      voiceMap.push_back(voice);
	      
	      break;
	    }
	}
	 
      if(!(mVoiceMapTable.insert(std::make_pair(time, voiceMap))).second)
	{ // "second" member of the returned std::pair is false if the 
	  // provided key duplicates a key already in the map
	  LOGE("ERROR: Duplicate time key in mock voice map data!\n");

	  return false;
	}
    }
  
  return true;
}
