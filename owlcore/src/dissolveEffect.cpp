#include "dissolveEffect.hpp"

#include <cmath>
#include <cstddef>

cDissolveEffect::cDissolveEffect(double duration)
{
  mDuration = duration;

  init();
}

cDissolveEffect::cDissolveEffect(cv::Mat *pImageA, cv::Mat *pImageB, 
				 double duration )
{
  mpImageA = pImageA;
  mpImageB = mpImageB;
  mDuration = duration;

  init();
}

void cDissolveEffect::init(void)
{
  mDone = false;
  mAlpha = 1.;
  mBeta = 0.;
  mPrevTime = std::clock();
  setSpeed();  
}

cDissolveEffect::~cDissolveEffect()
{
}

bool cDissolveEffect::update(cv::Mat *pResultImage, 
			     cv::Mat *pImageA,
			     cv::Mat *pImageB )
{
  clock_t now = std::clock();
  double dt = ((double)(now - mPrevTime)) / CLOCKS_PER_SEC;
  mPrevTime = now;

  mAlpha -= (dt * mSpeed);
  mBeta += (dt * mSpeed);

  if(mAlpha < 0. || mBeta > 1.)
    {
      mAlpha = 0.;
      mBeta = 1.;
      mDone = true;
    }

  if(pImageA != NULL)
    {mpImageA = pImageA;}
  if(pImageB != NULL)
    {mpImageB = pImageB;}

  cv::addWeighted(*mpImageA, mAlpha, *mpImageB, mBeta, 0.0, *pResultImage); 
  
  return mDone;
}

void cDissolveEffect::setSpeed(void)
{
  mSpeed = (mAlpha - mBeta) / mDuration;
}

bool cDissolveEffect::getDone(void)
{
  return mDone;
}
