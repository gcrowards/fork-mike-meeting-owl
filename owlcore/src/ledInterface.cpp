/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  ledInterface.cpp
 *  This is the implementation file for cLedInterface.
 *   --> High-level interface for controlling the Owl's LEDs.
 */

#include "ledContext.hpp"
#include "ledInterface.hpp"
#include "logging.hpp"

#include <unistd.h>

#ifdef __ANDROID__
#include "LedHandler.h"
/*  NOTE: Values duplicated from preprocessor defines in LedHandler.h (libOwlPG).
 *        Renamed by their main use to match their function and protect unsafe
 *         access of libOwlPG. */
const ledType_t cLedInterface::mkMuteLedMask      = LED_HDL_RED;
const ledType_t cLedInterface::mkBluetoothLedMask = LED_HDL_BLUE;
const ledType_t cLedInterface::mkEyeLedMask       = LED_HDL_WHITE;
const ledType_t cLedInterface::mkAllLedMask       = LED_HDL_ALL;
#else // Linux
// placeholders -- no LEDs on Linux
const ledType_t cLedInterface::mkMuteLedMask      = 0x01;
const ledType_t cLedInterface::mkBluetoothLedMask = 0x02;
const ledType_t cLedInterface::mkEyeLedMask       = 0x04;
const ledType_t cLedInterface::mkAllLedMask       = 0x07;
#endif

cLedInterface::cLedInterface()
  : mBlinkThread(std::bind(&cLedInterface::blinkLoop, this),
		 "ledBlinkThread", DEFAULT_THREAD_PRIORITY )
{
#ifdef __ANDROID__
  initLeds();   // calls libOwlPG LedHandler initialization
#endif
}

cLedInterface::~cLedInterface()
{
  if(mBlinkThread.isRunning())
    { // stop the blink thread
      mBlinkThread.join();
    }
  
  setContext(nullptr); // disconnect the LED context
  hwDisable(LEDS_ALL);   // turn off all LEDs.
}

/** Turns the specified LEDs on.
 *
 * @param leds the LED to enable.
 */
void cLedInterface::enable(ledType_t leds)
{
  ledType_t nonBlinking = leds & (~mBlinkingLeds);
  hwEnable(nonBlinking);
  
  if(mpActiveContext)
    { // update the context's states
      mpActiveContext->mLedStates |= leds;
    }
}

/** Turns the specified LED off.
 *
 * @param leds the LED to disable.
 */
void cLedInterface::disable(ledType_t leds)
{
  ledType_t nonBlinking = leds & (~mBlinkingLeds);
  hwDisable(nonBlinking);

  if(mpActiveContext)
    { // update the context's states
      mpActiveContext->mLedStates &= (~leds);
    }
}

/** Turns the specified LEDs on or off.
 *
 * @param leds a bitmask of the LEDs to set.
 * @param setEnabled true to enable, or false to disable.
 */
void cLedInterface::setLeds(ledType_t leds, bool setEnabled)
{
  if(setEnabled)
    { enable(leds); }
  else
    { disable(leds); }
}

/** Enables the specified LEDs without updating the active context's
 *   states. */
void cLedInterface::hwEnable(ledType_t leds)
{
#ifdef __ANDROID__
  // disable LEDs through libOwlPG interface
  if(ledEnable(leds))
    { LOGE("cLedInterface --> Failed to turn off LED.\n"); }
#endif
  // update the hardware LED states
  mLedStates |= leds;
}

/** Disables the specified LEDs without updating the active context's
 *   states. */
void cLedInterface::hwDisable(ledType_t leds)
{
#ifdef __ANDROID__
  // disable LEDs through libOwlPG interface
  if(ledDisable(leds))
    { LOGE("cLedInterface --> Failed to turn off LED.\n"); }
#endif
  // update the hardware LED states
  mLedStates &= (~leds);
}

/** Sets the active LED context, and sets the LED states to match the new context.
 *
 * @param pContext a pointer to the new context to set. nullptr disconnects the
 *         active context. */
void cLedInterface::setContext(cLedContext *pContext)
{
  if(mpActiveContext)
    { // deactivate previous context
      mpActiveContext->mpLedInterface = nullptr;
      mpActiveContext = nullptr;
    }

  if(pContext)
    { // activate new context
      ledType_t newStates = pContext->getStates();
      pContext->mpLedInterface = this;
      
      // only change the LEDs that need to change
      ledType_t changed = newStates ^ mLedStates;
      ledType_t enabledLeds = changed & newStates;
      ledType_t disabledLeds = changed & (~newStates);
      
      enable(enabledLeds);
      disable(disabledLeds);
    }
  else
    { // no context -- turn off all LEDs
      disable(LEDS_ALL);
    }
  mpActiveContext = pContext;
}

/** @return the current active context. */
cLedContext* cLedInterface::getContext()
{
  return mpActiveContext;
}

/** Starts blinking the specified LEDs with the given timing.
 *
 * @param leds the LEDs to blink.
 * @param onTimeMs the time the LEDs will stay on.
 * @param offTimeMs the time the LEDs will stay off.
 */
void cLedInterface::startBlinking(ledType_t leds, int onTimeMs, int offTimeMs)
{
  mBlinkOnTimeUs = onTimeMs * 1000;
  mBlinkOffTimeUs = offTimeMs * 1000;
  mBlinkingLeds = leds;
  
  LOGD("Started blinking --> states: 0x%X, blinking: 0x%X\n",
       (int)mLedStates, (int)leds );

  if(mLedStates & mBlinkingLeds)
    { // some of the blinking LEDs are already on -- turn off first
      hwDisable(mBlinkingLeds);
      usleep(mBlinkOffTimeUs);
    }
  mBlinkThread.start();
}

/** Stops blinking the LEDs. */
void cLedInterface::stopBlinking()
{
  mBlinkThread.join();
  mBlinkingLeds = LEDS_NONE;

  if(mpActiveContext)
    { hwEnable(mpActiveContext->mLedStates); }
}

/** Returns whether any LEDs are blinking. */
bool cLedInterface::isBlinking() const
{
  return (mBlinkingLeds != LEDS_NONE);
}

/** Turns blinking LEDs on, then off with the timing set via startBlinking(). */
void cLedInterface::blinkLoop()
{
  // turn LEDs on
  hwEnable(mBlinkingLeds);
  usleep(mBlinkOnTimeUs);  // sleep for onTime

  // turn LEDs off
  hwDisable(mBlinkingLeds);
  usleep(mBlinkOffTimeUs); // sleep for offTime
}
