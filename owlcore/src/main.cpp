#include <fcntl.h>
#include <iostream>
#include <fstream>
#include <pthread.h>
#include <sys/resource.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <sys/time.h>
#include <unistd.h>

#ifdef __ANDROID__
#include "libOwlPG.h"
#endif

#include "beamMath.hpp"
#include "gpu.hpp"
#include "logging.hpp"
#include "main.hpp"
#include "owlHook.hpp"
#include "params.h"
#include "thread.hpp"
#include "version.hpp"


#define CHECK(func) do { LOGD("==> %s\n", #func); if (func){ LOGE("%s\n", #func); return -1;} } while(0)


// constants
const int kMainLoopSleepTime = 5000; /**< sleep tim between main loop cycles. */


// globals
cOwlHook *gpOwlHook; /**< interface with core application. */
cGpu     *gpGpu;     /**< interface with GPU. */
#ifndef __ANDROID__
Display  *gpX11Display; /**< native X11 display for Linux. */
#endif
static bool gTerminate = false; /**< flag for exiting main loop. */

bool MicStreaming = true;
bool SpeakerStreaming = true;


#ifdef __ANDROID__ // Android --------------------------------------------------

/** Signal handler to catch termination events like Ctrl-C. */
static void *signalHandler(void *arg)
{
    sigset_t *set = (sigset_t *)arg;
    int sig;
    while (1)
      {
	sigwait(set, &sig);
	LOGD("Received signal %d -- %s\n", sig, strsignal(sig));
	gTerminate = true;
      }
    return NULL;
}

#else // Linux -----------------------------------------------------------------

/** Signal handler to catch termination events like Ctrl-C. */
void signalHandler(int sig)
{
  gTerminate = true;
}

#endif // Android / Linux ------------------------------------------------------

/** Prints git commit version info. */
void printGitCommitInfo(void)
{
  std::ifstream gitVersionFile;
  gitVersionFile.open(GIT_VERSION_PATH);
  // Iterate stream contents into string s
  std::string s(std::istreambuf_iterator<char>(gitVersionFile), {});

  LOGI("Git Hash Info: %s", s.c_str());
}

/** Prints the command-line help message. */
static void usage()
{
  //Print the message to stdout
  printf("Usage:  meetingOwl [OPTIONS]\n\n");
  printf("-h                 Print this message\n");
#if defined(SIMULATE_PCM_DATA) || (defined(__ANDROID__) && !defined(MOCK_INPUT))
  // help info pertaining to real input or simulated PCM input
#else // linux or MOCK_INPUT
  // help info pertaining to mock input
  printf("-t testDirectory   Run with test input from the directory\n");
#endif
}

/** Callback for transferring PCM audio data from libOwlPG to owl-core
    for localization on a low-priority thread. */
void   owlUpdateLocalize(int16_t *pPcmBuf, int bufLen, int nMics)
{
  // validate nMics, which comes from libOwlPG, to ensure that it
  // agrees with N_MICS, which the rest of owl-core expects
  if (nMics != N_MICS)
    {
      LOGE("N_MICS not equal to value passed from libOwlPG; %d != %d\n",
	   N_MICS, nMics);
      return;
    }

  // pass through to cOwlHook instance
  gpOwlHook->updateLocalize(pPcmBuf, bufLen);
}

/** Callback for transferring PCM audio data from libOwlPG to owl-core
    for beamforming on a high-priority thread. */
void   owlProcessAudio(int16_t *pPcmOut, int16_t *pPcmIn, int16_t *pRefIn, unsigned int bufLen)
{
#ifdef SIMULATE_PCM_DATA
  // overwrite actual microphone data with simulated sound
  simulatePcmData(pPcmIn, bufLen);
#endif

  // pass through to cOwlHook instance
  gpOwlHook->processAudio(pPcmOut, pPcmIn, pRefIn, bufLen); // must return quickly
}

/** Callback for notifying owl-core when the loudspeaker is active. */
void   owlUpdateSpeakerActive(int leftChanLevel, int rightChanLevel)
{
  // pass through to cOwlHook instance; must return quickly
  gpOwlHook->updateSpeakerActive(leftChanLevel, rightChanLevel);
}

/** Callback for notifying SpeakerHandler of the loudspeaker gain. */
float  owlUpdateSpeakerGain(void)
{
  // NOTE: this function must return quickly
  int volume;
  float gain;

  if (gpOwlHook->mpSysfs->getMute())
    { volume = 0; }
  else
    { volume = gpOwlHook->mpSysfs->getVolume(); }

  // convert volume as an integer in [0, 100] to a real-valued gain
  // expected to be in [0, 1]
  // TODO: consider changing the mapping from linear to logarithmic
  gain = 0.01 * volume;

  return gain;
}

/** Callback for transferring fisheye images from libOwlPG to GPU. */
void   cameraFrameCallback(void *eglClientBuffer)
{
  static bool first=true;

  if (first)
    {
      // The first call to cGpu::update() should finish setting up the
      // graphics system. At that point, it's safe to connect the
      // cOwlHook instance to the cGpu instance. From then on, we let
      // cOwlHook::update() manage the GPU update.
      gpGpu->update(eglClientBuffer);
      gpOwlHook->setGpu(gpGpu);
      first = false;
    }
  gpOwlHook->updateGpu(eglClientBuffer); // must not block or take a long time
}

/** Callback for notifying the application that video started/stopped
 * streaming. */
void uvcConnectedCallback(bool connected)
{
  return;
}

/** Configure the stdin stream to unbuffered, non-blocking mode.
 *
 * @param enable when true turns on unbuffered, non-blocking, otherwise off.
 */
void stdinUnbuffered(int enable)
{
  struct termios t;
  int stdinFlags;
  int stdinFd = fileno(stdin);

  tcgetattr(stdinFd, &t);
  stdinFlags = fcntl(stdinFd, F_GETFL);
  switch (enable) {
    case 0:
      t.c_lflag |= (ICANON | ECHO);
      stdinFlags &= ~O_NONBLOCK;
      break;
    default:
      t.c_lflag &= ~(ICANON | ECHO);
      stdinFlags |= O_NONBLOCK;
      break;
  }
  tcsetattr(stdinFd, 0, &t);
  fcntl(stdinFd, F_SETFL, stdinFlags);
}

/** Wait for keyboard input until timeout.
 *
 * Waits for up to 'timeoutUsec' microseconds for keyboard input to become
 * available. Otherwise return 0.
 *
 * @returns a typed character, or 0 if timeout.
 */
char kbWaitForChar(int timeoutUsec)
{
  char c[256];
  int numRead;
  int stdinFd = fileno(stdin);
  struct timeval tv;
  static bool kbInitialized = false;
  const int kOneMillion = 1000000;

  if (DEBUG(KEYBOARD_INPUT))
    {
      if (!kbInitialized)
        {
          // Set up
          stdinUnbuffered(1);
          kbInitialized = true;
        }
      fd_set fds;
      tv.tv_sec = timeoutUsec / kOneMillion;
      tv.tv_usec = timeoutUsec % kOneMillion;
      FD_ZERO(&fds);
      FD_SET(stdinFd, &fds);
      select(stdinFd+1, &fds, NULL, NULL, &tv);
      if (FD_ISSET(stdinFd, &fds))
        {
        numRead = read(stdinFd, c, sizeof(c));
        if (numRead > 0)
          {
            c[numRead] = 0; // Terminate the input
            LOGI("read returned %s\n", c);
          }
        }
      else
        {
          c[0] = 0;
        }

      return c[0];
    }
  else
    {
      usleep (timeoutUsec);

      return 0;
    }
}

/** Receives debugging command characters and takes action accordingly.
 *
 * This function replaces a simple sleep() in the main loop and provides
 * a means for inserting artificial events for debug and test. When a
 * character is received on stdin, it causes kbWaitForChar() to return
 * immediately. This function can then take action based on the character
 * value. The action runs in the context of the main application thread.
 */
void handleDebugCommands(int timeoutUsec)
{
  switch (kbWaitForChar(5000))
  {
    case 'h':
    case 'H':
    case '?':
      printf("Debugging commands:\n");
      printf("  'M': Toggle mic streaming\n");
      printf("  'S': Toggle speaker streaming\n");
      printf("  'Q': Quit the meetingOwl application\n");
#ifdef __ANDROID__
      printf("  'R': Reboot the system\n");
#endif
      printf("  'C': Crash (segmentation fault)\n");
      break;
    case 'm':
    case 'M':
      MicStreaming = !MicStreaming;
      LOGI("%s\n", (MicStreaming?"Mic streaming":"Mic NOT streaming"));
      break;
    case 's':
    case 'S':
      SpeakerStreaming = !SpeakerStreaming;
      LOGI("%s\n", (SpeakerStreaming?"Speaker streaming":"Speaker NOT streaming"));
      break;
    case 'q':
    case 'Q':
      LOGD("Terminating\n");
      gTerminate = true;
      break;
    case 'r':
    case 'R':
      {
#ifdef __ANDROID__   // Don't reboot host systems!
        int rc = system("reboot");
#endif
      }
      break;
    case 'c':
    case 'C':
      {
        char *pcSegFault = (char *)0x00000000;
        *pcSegFault = 0;  // Cause segmentation fault!
      }
      break;
    default:
      break;
  }
}

/** main entry point for the meetingOwl program.
 *
 */
int  main(int argc, char **argv)
{
  int ret;
  sigset_t set;
  pthread_t signalThread;
  struct rlimit core_limit;
  core_limit.rlim_cur = RLIM_INFINITY;
  core_limit.rlim_max = RLIM_INFINITY;

  // print git hash
  printGitCommitInfo();

  // Configure the application to dump core on fatal errors
  if (setrlimit(RLIMIT_CORE, &core_limit) < 0)
      fprintf(stderr, "setrlimit: %s\nWarning: core dumps may be truncated or non-existant\n", strerror(errno));
#ifdef __ANDROID__
  system("echo \"/data/OwlLabs/core_%e\" > /proc/sys/kernel/core_pattern");
#endif

#ifdef __ANDROID__ // Android --------------------------------------------------
  // initialize UVC as early as possible to be ready for host control requests
  CHECK(initUVC());
#endif

  // check version info removed HMB
 // if ( checkVersion() )
//    { return 0; }

#ifdef __ANDROID__ // Android --------------------------------------------------
  // set signals to be caught
  sigemptyset(&set);
  sigaddset(&set, SIGQUIT);
  sigaddset(&set, SIGTERM);
  sigaddset(&set, SIGINT);
  sigaddset(&set, SIGABRT);
  sigaddset(&set, SIGFPE);
  // start a thread for catching terminate events
  CHECK(pthread_sigmask(SIG_BLOCK, &set, NULL));
  CHECK(pthread_create(&signalThread, NULL, &signalHandler, (void *) &set));
  pthread_setname_np(signalThread, "signalHandler");
#else // Linux -----------------------------------------------------------------
  // set up signal handler
  struct sigaction signalAction;
  signalAction.sa_handler = signalHandler;
  sigaction(SIGINT, &signalAction, 0);
#endif // Android / Linux ------------------------------------------------------

  std::string testPath = "";

  // handle command-line arguments
  for(int i = 1; i < argc; i++)
    {
      if(argv[i][0] == '-')
	{
	  switch(argv[i][1])
	    {
#if defined(SIMULATE_PCM_DATA) || (defined(__ANDROID__) && !defined(MOCK_INPUT))
	      // arguments pertaining to real input or simulated PCM input
#else // linux or MOCK_INPUT
	      // arguments pertaining to mock input
	    case 't': // set path of the test directory
	      testPath = argv[++i];
	      // remove any trailing slashes
	      while(testPath.back() == '/')
		{ testPath.pop_back(); }
	      break;
#endif // linux or MOCK_INPUT
	    case 'h': // show help
	    default:
	      usage();
	      return 0;
	    }
	}
    }

  // initialize hook to core application that consumes image and audio data
#if defined(__ANDROID__) && !defined(MOCK_INPUT)
  gpOwlHook = new cOwlHook();
#else // linux or MOCK_INPUT
  gpOwlHook = new cOwlHook(testPath);
#endif

  // do platform-specific initialization
  ret = init();
  if (ret) { return ret; }

  // print thread count after initialization
  // NOTE: This only includes the threads created with cThread.
  LOGV("THREADING ==> Number of cThread threads: %d\n", cThread::mNumThreads);

#if defined(__ANDROID__) && !defined(MOCK_INPUT)
  // need a dummy loop while CameraHandler services cameraFrameCallback()
  while(!gTerminate) { handleDebugCommands(kMainLoopSleepTime); }
#else // linux or MOCK_INPUT

  LOGI("running with MOCK camera input\n");

#ifdef SIMULATE_PCM_DATA
  // use a counter to call callbacks at different rates
  // TODO: consider setting up separate threads
  int     cameraCallbackCounter = 0;
  // create buffers for simulated PCM data
  const int kPcmChunkSize = 256;
  const int kPcmBufLen = N_MICS * kPcmChunkSize;
  int16_t pPcmOut[2 * kPcmChunkSize];
  int16_t pPcmIn[N_MICS * kPcmChunkSize];

  // need a loop to service callbacks
  while(!gTerminate)
    {
      // use a counter so we update video more slowly than audio
      if (cameraCallbackCounter % 5 == 0)
	{
	  // for mock camera, must explictly call cameraFrameCallback()
	  cameraFrameCallback(gpOwlHook->getMockCameraImage());
	}
      cameraCallbackCounter++;
      // with simulated PCM data, must explictly call
      // owlProcessAudio() and owlUpdateLocalize()
      owlProcessAudio(pPcmOut, pPcmIn, NULL, kPcmBufLen);
      owlUpdateLocalize(pPcmIn, kPcmBufLen, N_MICS);

      usleep(kMainLoopSleepTime);
      // check for quit keypress
      if (quitKey()) { gTerminate = true; }
    }
#else // !SIMULATE_PCM_DATA
  // need a loop to service callbacks
  while(!gTerminate)
    {
      // for mock camera, must explictly call cameraFrameCallback()
      cameraFrameCallback(gpOwlHook->getMockCameraImage());
      // with mock source angles, must explictly call owlUpdateLocalize()
      // Note that the PCM buffer is unused, hence the null pointer below
      owlUpdateLocalize(nullptr, 0, N_MICS);
      
      handleDebugCommands(kMainLoopSleepTime);
      if (!DEBUG(KEYBOARD_INPUT))
        {
          // check for quit keypress
          if (quitKey()) { gTerminate = true; }
        }

      if (DEBUG(ANALYTICS))
	{
	  static int connected = 0;
	  static int count = 0;
	  count++;
	  if (count >= 200)
	    {
	      count = 0;
	      connected = !connected;
	      uvcConnectedCallback(connected);
	    }
	}
    }
#endif // SIMULATE_PCM_DATA

#endif // linux or MOCK_INPUT

  // reset console buffering
  stdinUnbuffered(0);

  // do platform-specific cleanup
  ret = finish();
  if (ret) { return ret; }

  // release the owl-core resources
  delete gpOwlHook;
  delete gpGpu;

  // NOTE: the following message is used to detect clean exiting of
  // the application by the unit testing scripts when running the
  // Android version via adb.  Make sure any changes to it have a
  // corresponding change in OwlTesCase:meetingOwl_clean_exit().
  LOGV("Say good night, Owl... Good night.\n");

  return 0;
}

/** Perform platform-specific initialization. */
int  init(void)
{
#ifdef __ANDROID__ // Android --------------------------------------------------
  // initialize director thread that "directs" the graphics pipeline
  CHECK(initDirector());

  // register a callback that updates owl-core with image and audio data
  CHECK(registerDirectorUpdateCallback(owlUpdateLocalize));

  // initialize MediaCodec with callback that encodes data from a NativeWindow
  CHECK(initCodec());
  CHECK(registerCodecCallback(UVCInputFrameCallback));

  // initialize GPU with function to get NativeWindow
  gpGpu = new cGpu(getCodecNativeWindow);

#ifndef MOCK_INPUT
  // initialize Camera with callback that passes output of the Camera pipeline
#ifdef INPUT_3456
  CHECK(initCamera(RES_3456x3456));
#else
  CHECK(initCamera(RES_1728x1728));
#endif // INPUT_3456
  CHECK(registerCameraCallback(cameraFrameCallback));
#endif // MOCK_INPUT

  // initialize Speaker with i2c and mixer changes
  CHECK(initSpeaker(owlUpdateSpeakerActive, owlUpdateSpeakerGain));

  // initialize Audio Path and register callbacks to director for audio localization
  CHECK(initAudio(owlProcessAudio));
  CHECK(registerAudioCallback(DirectorPCMCallback));

  // Start the pipeline from output to input (no need to explicitly start GPU)
  CHECK(startDirector());
  CHECK(startUVC());
  CHECK(startCodec());
#ifndef MOCK_INPUT
  CHECK(startCamera());
#endif
  CHECK(startSpeaker());
  CHECK(startAudio());

#else // Linux -----------------------------------------------------------------

  Window root, x11Window;

  // get X11 display
  gpX11Display = XOpenDisplay(NULL);
  if(!gpX11Display)
    { LOGE("Error: unable to open display\n"); }

  // create X11 Window
  root = DefaultRootWindow(gpX11Display);
  x11Window = XCreateWindow(gpX11Display, root,
			    0, 0, OUTPUT_WIDTH, OUTPUT_HEIGHT,
			    0, CopyFromParent, InputOutput, CopyFromParent, 0, NULL);
  XMapWindow(gpX11Display, x11Window);

  // select keypress events
  XSelectInput(gpX11Display, x11Window, KeyPressMask);

  // initialize GPU with native display and window
  gpGpu = new cGpu(gpX11Display, x11Window);

#endif // Android / Linux ------------------------------------------------------

  return 0; // success
}

int  finish(void)
{
#ifdef __ANDROID__ // Android --------------------------------------------------

  // tear down the pipeline in reverse order
  CHECK(stopAudio());
  CHECK(stopSpeaker());
#ifndef MOCK_INPUT
  CHECK(stopCamera());
#endif
  CHECK(stopCodec());
  CHECK(stopUVC());
  CHECK(stopDirector());

#else // Linux -----------------------------------------------------------------

  XCloseDisplay(gpX11Display);

#endif // Android / Linux ------------------------------------------------------

  return 0; // success
}

/** Check for a press of one of the quit keys; not supported with Android. */
bool quitKey(void)
{
  bool quit = false;

#ifdef __ANDROID__
  // keypress events not supported; must use Ctrl-C
#else // Linux
  XEvent event;
  KeySym key;
  char   keyBuf[255];

  if ( XPending(gpX11Display) > 0 )
    {
      XNextEvent(gpX11Display, &event);
      if ( event.type == KeyPress
	   && XLookupString(&event.xkey, keyBuf, 255, &key, 0)==1 )
	{
	  // check for quit key
	  if (keyBuf[0] == 27 || keyBuf[0] == 'q' || keyBuf[0] == 'Q')
	    quit = true;
	}
    }
#endif
  return quit;
}

/** This function is used to overwrite the PCM microphone data with
 * simulated data coming from a given sound bearing.
 *
 * The intention is that this function will be used for debugging
 * purposes, to overwrite the real data with what we expect from a
 * single, possibly moving, sound source.
 *
 * The file referred to by SIMULATED_PCM_FILEPATH is repeatedly polled
 * for the sound source direction, in degrees. The contents of the
 * file can be updated with something like "echo DEGREES > FILEPATH",
 * with a negative value used to indicate silence.
 *
 * @param pPcmIn is a pointer to the PCM sound data transferred via
 * the owlUpdate callback in main.cpp.
 * @param bufLen is the data buffer length.
 */
void simulatePcmData(int16_t *pPcmIn, unsigned int bufLen)
{
  int  value, ret;
  FILE *fp;
  const float kDegToBeam = (float) N_BEAMS / 360;
  static int beam = -1; // initialized for silence
  static int count = 0;
  static bool init = true;

  if (init)
    {
      // initialize the sound source direction file
      fp = fopen(SIMULATE_PCM_FILEPATH, "w");
      if (fp == NULL)
	{
	  LOGW("unable to open simulated PCM file '%s'\n",
	       SIMULATE_PCM_FILEPATH);
	}
      else
	{
	  // write a negative value, which indicates silence
	  ret = fprintf(fp, "-1");
	  fclose(fp);
	}
      init = false;
    }

  // poll the file system for a sound source direcction
  if  (count % 200 == 0) // 200 corresponds to ~1s
    {
      // open the filepath for reading the sound location
      fp = fopen(SIMULATE_PCM_FILEPATH, "r");
      if (fp != NULL)
	{
	  // read the value, expected to be in degrees [0, 360]
	  ret = fscanf(fp, "%d", &value);
	  fclose(fp);
	  if (ret == 1 && value >=0 && value <= 360)
	    {
	      // convert to a beam number, accounting for wraparound
	      beam = 0.5 + kDegToBeam * value;
	      if (beam >= N_BEAMS)
		{ beam -= N_BEAMS; }
	    }
	  else
	    { beam = -1; } // silence
	}
    }

  // overwrite the PCM data and increment the counter
  beamMath::simulatePcmData(pPcmIn, bufLen, beam);
  count++;
}
