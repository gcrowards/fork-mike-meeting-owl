///
/// AudioLevelManager.cpp - contains code used to manage level in the microphone path.
/// - Initially it will contain a compressor and a limiter. Eventually it will also support
///   an AGC and possibly other algorithms.
///

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "AudioMgr.hpp"
#include "AudioLevelManager.hpp"

extern FILE *dbgFile;

#define COMP_THRESHOLD_MIN   -82.0
#define COMP_THRESHOLD_MAX   0.0
#define COMP_RATIO_MIN     -8.0
#define COMP_RATIO_MAX     40.0
#define TC_MIN         0.0
#define TC_MAX         10.0
#define MAKEUP_MIN       0.0
#define MAKEUP_MAX       60.0
#define DUCK_MIN         -60.0
#define DUCK_MAX       0.0



/** CalcTimeConst - calculate and exponential time constant from a time value
*
* @param time_val - The amount of time for the time constant
* @param fs - the sample rate of the system
*
* @return the time constant associated with this time
*/
FLOAT CalcTimeConst(FLOAT varTime, int fs)
{

  return (FLOAT)(1.0 - exp((-2.2 / fs) / varTime));
}

/** verifyRange -  Verifys if an input value is in range.
*
* @param in - value to be verified
* @param maxValue - maximum value of the input
* @param minValue - minimum value of the range
*
* @return returned a pinned value
*/

FLOAT verifyRange(FLOAT in, FLOAT maxValue, FLOAT minValue)
{
  if (in > maxValue)
  { return maxValue; }
  if (in < minValue)
  { return minValue; }
  return in;
}

/** InitLevel - Initializes the state of the level calculations
*
* @param pLs - Pointer to the level state structure
*
* return void
*/
void InitLevel(LEVEL_STATE *pLevel)
{
  int i;
  pLevel->buffIndex = 0;
  pLevel->maxSum = 0;
  pLevel->rmsSum = 0;
  for (i = 0; i < RMS_BUFF_SIZE; ++i)
  { pLevel->rmsBuffer[i] = 0; }

  pLevel->last = 0;
  pLevel->lastPeak = 0;
  pLevel->peakDecay = 0.99f;
  pLevel->lastgain = 0;
  pLevel->peakHoldCount = PEAK_HOLD_COUNT;

}


/** RMSCalc - Calculate the RMS value of this input value and previous values
*
* @param input - the input sample
* @param pLevel - Pointer to a level state structure.
* @param reset - If this is set, then reset the level structure
*
* @return rms level o
*/
FLOAT RMSCalc(FLOAT input, LEVEL_STATE *pLevel, bool reset)
{
  FLOAT in2;
  FLOAT rmsVal;
  int i;

  if (reset)
  {
    pLevel->buffIndex = 0;
    pLevel->maxSum = 0;
    pLevel->rmsSum = 0;
    for (int i = 0; i < RMS_BUFF_SIZE; ++i)
    { pLevel->rmsBuffer[i] = 0; }
  }

  in2 = input * input;
  pLevel->rmsSum = pLevel->rmsSum - pLevel->rmsBuffer[pLevel->buffIndex] + in2;
  pLevel->rmsBuffer[pLevel->buffIndex] = in2;
  pLevel->buffIndex++;
  if (pLevel->buffIndex >= RMS_BUFF_SIZE)
  {
    pLevel->buffIndex = 0;
  }
  if (pLevel->rmsSum < 0)
  {
    rmsVal = 0;
    for (i = 0; i < RMS_BUFF_SIZE; ++i)
    {
      rmsVal += pLevel->rmsBuffer[i];
    }
    pLevel->rmsSum = rmsVal;
  }

  rmsVal = (FLOAT)sqrt(pLevel->rmsSum / RMS_BUFF_SIZE);

  return rmsVal;
}

/** CalcLevel - Calculates the RMS and peaks levels of the audio
*
* @param input - input sample
* @param pLevel - pointer to the level state
* @param pLevelsOut - pointer to calculated levels (out)
*
* @return 0 if successful -1 if note
*/
int CalcLevels(FLOAT input, LEVEL_STATE *pLevel, LEVELS *pLevelsOut)
{
  pLevelsOut->rmsLevel = RMSCalc(input, pLevel, false);
  pLevelsOut->rmsLevel = mag2db(pLevelsOut->rmsLevel);

  input = (FLOAT)fabsf(input);

  if (pLevel->peakHoldCount > 0)
  {
    if (input > pLevel->lastPeak)
    {
      pLevel->lastPeak = input;
      pLevel->last = input;
      pLevel->peakHoldCount = PEAK_HOLD_COUNT;
    }
  }
  else
  {
    pLevel->last *= pLevel->peakDecay;
    if (input > pLevel->last)
    {
      pLevel->lastPeak = input;
      pLevel->last = input;
      pLevel->peakHoldCount = PEAK_HOLD_COUNT;

    }
    else
    {
      pLevel->peakHoldCount = 1;
    }
  }

  pLevel->peakHoldCount--;

  pLevelsOut->peakLevel = mag2db(pLevel->lastPeak);

  return 0;
}
/** ALMWritePredelay - writes a sample into the predelay buffer
*
* @param pAlmState - pointer to the state of this level manager
* @param sample - sample to write into the buffer
*
* @return void
*/
void ALMWritePredelay(ALM_STATE *pAlmState, FLOAT sample)
{
  pAlmState->predelay_buffer[pAlmState->writeIndex] = sample;
  pAlmState->writeIndex++;
  if(pAlmState->writeIndex >= MAX_PREDELAY)
  { pAlmState->writeIndex = 0;}
}

/**ALMReadPredelay - reads the next sample in the predelay buffer
*
* @param pAlmState - pointer to the state of this level manager
*
* @return sample
*/
FLOAT ALMReadPreday(ALM_STATE *pAlmState)
{
  FLOAT retVal;

  retVal =  pAlmState->predelay_buffer[pAlmState->readIndex];
  pAlmState->readIndex++;
  if(pAlmState->readIndex >= MAX_PREDELAY)
  { pAlmState->readIndex = 0;}

  return retVal;
}

/** ALMMeasureLevels - Measures the levels and puts the results in an internal
* buffer. Designed for measuring in different poiont in chain from where gain
* is applied.
*
* @param pAlmState - pointer to the state of this level manager
* @param pAudioIn - pointer to the input audio
* @param frameCount - number of frames of audio in input buffer
* @param procChan - Which channel to process in a multichannel buffer
* @param totalChans - The number of channels in each frame.
*
* @return 0 if successful, -1 if not
*/
int ALMMeasureLevels(ALM_STATE *pAlmState, short *pAudioIn,
                     int frameCount, int procChan, int totalChans)
{
  int i;
  FLOAT sample;
  LEVELS levels;

  for (i = 0; i < frameCount; ++i)
  {
    sample = ((float)pAudioIn[i * totalChans + procChan]) / 32767.0;
    CalcLevels(sample, &pAlmState->levelState, &levels);
    pAlmState->levelState.rmsLevelBuffer[i] = levels.rmsLevel;
    pAlmState->levelState.peakLevelBuffer[i] = levels.peakLevel;
  }

  return 0;

}
/** CalcGain - Heart of ALM - calculates gains based on level and config
*
* @param pLs - pointer to the  state
* @param pLevels - pointer to the level structure
* @param dtdAvail - Double talk has been detected. 
*
* @return gain value to apply to the audio
*/

FLOAT CalcGain(ALM_STATE *pAlmState, LEVELS *pLevels, float dtdAvail)
{
  FLOAT levelDb;
  FLOAT limitLevel;
  FLOAT limitGain;
  FLOAT gain;
  FLOAT interpCoeff = 1.0;
#define LINEAR 0
#define COMPRESS 1
#define LIMIT 2
  static int lastState = LINEAR;
  static int lastLimit = 0;
  int limiting = 0;
  int compressing = 0;
  int linear = 0;
  int temp;

  levelDb = pLevels->rmsLevel;
  gain = 0;

  pAlmState->mLastState = LINEAR;
  if ((pAlmState->enables & ALM_ENABLE_COMPRESSION)
      && (levelDb >= pAlmState->compThreshold) && (dtdAvail <= 0)) // we are in compression
  {
	pAlmState->mLastState = COMPRESS;
    if(lastState != COMPRESS)
    {
      if(ALM_DEBUG)
      {printf("ALM %llu - %d->%d \n", getMsecs(), lastState, COMPRESS);}
      lastState = COMPRESS;
    }
    gain = -(levelDb - pAlmState->compThreshold) * pAlmState->oneOverRatio;
	pAlmState->lastCompressGain = gain;
    if (levelDb >= pAlmState->lastLevels.rmsLevel)
    {
      interpCoeff = pAlmState->compAtCoeff;
    }
    else
    {
      interpCoeff = pAlmState->compDecCoeff;
    }
    pAlmState->retToLinCoeff = pAlmState->compDecCoeff;
  }
  else
  {
	  if (lastState == COMPRESS)
	  {
		  pAlmState->delayTimeStamp = getMsecs() + pAlmState->delayTime;
	  }
	  if (getMsecs() < pAlmState->delayTimeStamp)
	  {
		  gain = pAlmState->lastCompressGain;
		  interpCoeff = 0; //HMBT - just hang out here
	  }
	  else
	  {
		  // we are in linear range
		  gain = 0;
		  interpCoeff = pAlmState->retToLinCoeff;
	  }
	if (lastState != LINEAR)
	{
		if (ALM_DEBUG)
		{
			printf("ALM %llu - %d->%d  \n", getMsecs(), lastState, LINEAR);
		}
		lastState = LINEAR;
	}
  }

  if (dtdAvail < 0)
  {
	  gain -= pAlmState->duck;
  }
  else
  {
	  gain += pAlmState->makeup;
  }


  // now do the limiter
  // see if the new gain has caused the audio to exceed limit theshold
  limitLevel = pLevels->peakLevel + gain;

  if ((pAlmState->enables & ALM_ENABLE_LIMIT) &&
      (limitLevel > pAlmState->limitThreshold))
  {
    if(lastLimit == 0)
    {
      if(ALM_DEBUG)
      {printf("ALM %llu - Limit On \n", getMsecs());}
      lastLimit = 1;
    }

    // we are in limiting
    limitGain = -(limitLevel - pAlmState->limitThreshold) *
                pAlmState->limitOneOverRatio;
    gain += limitGain;
    if (limitLevel >= pAlmState->lastLevels.peakLevel)
    {
      interpCoeff = pAlmState->limitAtCoeff;
    }
    else
    {
      interpCoeff = pAlmState->limitDecCoeff;
    }
    pAlmState->retToLinCoeff = pAlmState->limitDecCoeff;
  }
  else
  {
    if(lastLimit == 1)
    {
      if(ALM_DEBUG)
      {printf("ALM %llu - Limit  Off \n", getMsecs());}
      lastLimit = 0;
    }
  }

//  pAlmState->debugVal = (short)GetSpeakerRMSLevel();

  gain = (1.0f - interpCoeff) * pAlmState->lastGain + interpCoeff * gain;
  if(gain < pAlmState->maxAtten)
  { gain = pAlmState->maxAtten; }
  pAlmState->lastGain = gain;
  pAlmState->lastLevels.rmsLevel = pLevels->rmsLevel;
  pAlmState->lastLevels.peakLevel = pLevels->peakLevel;

  gain = db2mag(gain);

  return gain;
}
/** ALMInit - not used; here for parallelism with EQ
*
* @param pAlmState - ALM state to initialize
*
* @return 0
*/
int ALMInit(ALM_STATE *pAlmState)
{
  InitLevel(&pAlmState->levelState);
  return 0;
}

/** ALMConfigure - Initialize ALM state from the input parameters
*
* @param pLs - pointer to the ALM state
* @param pLc - pointer to the ALM config
*
* @return 0 if successful; -1 if note
*/
int ALMConfigure(ALM_STATE *pLs, ALM_CONFIG *pLc)
{
  pLs->Fs = 48000;
  pLs->enables = pLc->enables;

  pLs->compThreshold = verifyRange(pLc->compThreshold, COMP_THRESHOLD_MAX,
                                   COMP_THRESHOLD_MIN);
  pLs->compRatio = verifyRange(pLc->compRatio, COMP_RATIO_MAX, COMP_RATIO_MIN);
  pLs->compAttack = verifyRange(pLc->compAttack, TC_MAX, TC_MIN);
  pLs->compDecay = verifyRange(pLc->compDecay, TC_MAX, TC_MIN);
  pLs->limitThreshold = verifyRange(pLc->limitThreshold, COMP_THRESHOLD_MAX,
                                    COMP_THRESHOLD_MIN);
  pLs->limitRatio = verifyRange(pLc->limitRatio, COMP_RATIO_MAX, COMP_RATIO_MIN);
  pLs->limitAttack = verifyRange(pLc->limitAttack, TC_MAX, TC_MIN);
  pLs->limitDecay = verifyRange(pLc->limitDecay, TC_MAX, TC_MIN);

  pLs->makeup = verifyRange(pLc->makeup, MAKEUP_MAX, MAKEUP_MIN);
  pLs->duck = pLc->duck;
  pLs->lastGain = pLs->makeup;
  pLs->maxAtten = pLc->maxAtten;
  pLs->delayTime = pLc->delayTime;
  pLs->delayTimeStamp = 0;
  pLs->lastCompressGain = 0;
  // now calculate the attack and decay coefficients from the times

  pLs->compAtCoeff = CalcTimeConst(pLs->compAttack, pLs->Fs);
  pLs->compDecCoeff = CalcTimeConst(pLs->compDecay, pLs->Fs);
  pLs->limitAtCoeff = CalcTimeConst(pLs->limitAttack, pLs->Fs);
  pLs->limitDecCoeff = CalcTimeConst(pLs->limitDecay, pLs->Fs);
  pLs->spkrActiveCoeff = CalcTimeConst(0.01, pLs->Fs);

  pLs->oneOverRatio = 1.0f - (1.0f / pLs->compRatio);
  pLs->limitOneOverRatio = 1.0f - (1.0f / pLs->limitRatio);

  pLs->readIndex = 0;
  if(pLc->predelay >= MAX_PREDELAY)
  {pLs->writeIndex = MAX_PREDELAY - 1;}
  else
  {pLs->writeIndex = pLc->predelay; }


  InitLevel(&pLs->levelState);

  return 0;

}

/** ALMCheckProperties - Checks the properties for new values of certain params.
*
* @param pAlmState - ALM state to reconfigure
*
* @return 0
*/
int ALMCheckProperties(ALM_STATE *pLs)
{
  FLOAT temp;
  return 0;

}


/** ALMProcess - Processes a buffer of audio through the level manager
*
* @param pAlmState - pointer to the state of this level manager
* @param pAudioIn - pointer to the input audio
* @param pAudioOut - pointer to the output audio buffer (can be same as input)
* @param frameCount - number of frames of audio in input buffer
* @param procChan - Which channel to process in a multichannel buffer
* @param totalChans - The number of channels in each frame.
* @param applyGain - Indicates to apply the makeup gain vs. compression gain
*
* @return 0 if successful, -1 if not
*/
int ALMProcess(ALM_STATE *pAlmState, short *pAudioIn, short *pAudioOut,
               int frameCount, int procChan, int totalChans, float dtdAvail)
{
  int i;
  FLOAT gain;
  FLOAT sample;
  LEVELS levels;
  static FLOAT lastMakeup = -1;
  static int mlMesg = 1;
  FLOAT tempGain;
  FLOAT aveGain = 0;

  ALMCheckProperties(pAlmState);
  if((pAlmState->enables & ALM_ALTERNATE_MEASURE_POINT) == 0)
  {
    ALMMeasureLevels(pAlmState, pAudioIn, frameCount, procChan, totalChans);
  }


  for (i = 0; i < frameCount; ++i)
  {
    sample = ((float)pAudioIn[i * totalChans + procChan]) / 32767.0;
    ALMWritePredelay(pAlmState, sample);
    sample = ALMReadPreday(pAlmState);
    levels.rmsLevel = pAlmState->levelState.rmsLevelBuffer[i];
    levels.peakLevel = pAlmState->levelState.peakLevelBuffer[i];
    gain = CalcGain(pAlmState, &levels, dtdAvail);
    sample = sample * gain;
    if (sample > 1.0)
    {
      sample = 1.0;
    }
    else if(sample < -1.0)
    {
      sample = -1.0;
    }

    pAudioOut[i * totalChans + procChan] = (short)(sample * 32767.0);

  }

  return 0;

}

