/*
 * pinnedAoiArgs.cpp
 *
 *  Created on: Mar 2, 2017
 *
 *  Class for managing pinned AOI arguments. Includes the ability to
 *  serialize and deserialize data as JSON strings.
 */
#include "circleMath.hpp"
#include "logging.hpp"
#include "rapidjson/document.h"
#include "rapidjson/pointer.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "switchboard/pinnedAoiReply.hpp"
#include "switchboard/switchboardMessages.hpp"

// #define DEBUG_PINNED_AOI_ARGS
#ifdef  DEBUG_PINNED_AOI_ARGS
#define LOGID LOGI
#else
#define LOGID(...) {}
#endif

using namespace rapidjson;


/* *** cPinnedAoiReply *** */

/** Template for creating PinnedAoiReply messages. */
const char pcPinnedAoiReplyDOM[] =
    "{\"" PINNED_AOI_REPLY_STR "\":"                   \
       "{\"" PINNED_AOI_REPLY_CMD_STR "\":\"\","       \
        "\"" PINNED_AOI_REPLY_ID_STR "\":0,"           \
        "\"" PINNED_AOI_REPLY_SIZE_STR "\":0,"         \
        "\"" PINNED_AOI_REPLY_ANGLE_STR "\":0,"        \
        "\"" PINNED_AOI_REPLY_RETURNVAL_STR "\":0,"    \
        "\"" PINNED_AOI_REPLY_MESSAGE_STR "\":\"\"}}";


/** Create an object with default values. */
cPinnedAoiReply::cPinnedAoiReply()
  : cmd(ePinnedAoiCmd_t::NONE), id(0), size(0), angle(0), returnVal(0), message("")
{}

/** Create an object with command and id specified. */
cPinnedAoiReply::cPinnedAoiReply(ePinnedAoiCmd_t cmd, int id)
: cmd(cmd), id(id), size(0), angle(0), returnVal(0), message("")
{}

/** Create an object with command, id, size and angle all specfified. */
cPinnedAoiReply::cPinnedAoiReply(ePinnedAoiCmd_t cmd, int id, int retVal, std::string msg)
: cmd(cmd), id(id), size(0), angle(0), returnVal(retVal), message(msg)
{}

/** Create an object with command, id, size and angle all specfified. */
cPinnedAoiReply::cPinnedAoiReply(ePinnedAoiCmd_t cmd, int id, int size, int angle, int retVal, std::string msg)
: cmd(cmd), id(id), size(size), angle(angle), returnVal(retVal), message(msg)
{}

/** Destroy an object. */
cPinnedAoiReply::~cPinnedAoiReply()
{}

/** Get the command. */
ePinnedAoiCmd_t cPinnedAoiReply::getCommand(void)
{
  return cmd;
}

/** Get the ID. */
int cPinnedAoiReply::getId(void)
{
  return id;
}

/** Get the size. */
int cPinnedAoiReply::getSize(void)
{
  return size;
}

/** Get the angle. */
int cPinnedAoiReply::getAngle(void)
{
  return angle;
}

/** Get the return value. */
int cPinnedAoiReply::getReturnVal(void)
{
  return returnVal;
}

/** Get the message. */
std::string cPinnedAoiReply::getMessage(void)
{
  return message;
}

/** Parse the given JSON message into the fields of this args class.
 *
 * @param pcMsg is the buffer containing the JSON message to parse.
 *
 * @return true if a valid message was parsed.
 */
bool cPinnedAoiReply::parseMsg(const char *pcMsg)
{
  bool parsed = false;
  Document doc;
  doc.Parse(pcMsg);
  bool isPinnedAoiMsg = doc.HasMember(PINNED_AOI_REPLY_STR);
  Value *cmdP = Pointer(PINNED_AOI_REPLY_CMD_PTR).Get(doc);
  Value *idP = Pointer(PINNED_AOI_REPLY_ID_PTR).Get(doc);
  Value *sizeP = Pointer(PINNED_AOI_REPLY_SIZE_PTR).Get(doc);
  Value *angleP = Pointer(PINNED_AOI_REPLY_ANGLE_PTR).Get(doc);
  Value &retValR =
      Pointer(PINNED_AOI_REPLY_RETURNVAL_PTR).GetWithDefault(doc, 0);
  Value *retValP = &retValR;
  Value &messageR =
      Pointer(PINNED_AOI_REPLY_MESSAGE_PTR).GetWithDefault(doc, "");
  Value *messageP = &messageR;

  if (isPinnedAoiMsg && (cmdP != nullptr) && (idP != nullptr))
    {
      const char *cmdStr;
      IF_VERIFY_STRING(cmdP)
        {
          cmdStr = cmdP->GetString();
          cmd = ePinnedAoiCmd_t::fromString(cmdStr);
        }
      IF_VERIFY_INT(idP)
        {
          id  = idP->GetInt();
        }
      IF_VERIFY_INT(sizeP)
        {
          size = sizeP->GetInt();
        }
      IF_VERIFY_INT(angleP)
        {
          angle = angleP->GetInt();
        }
      IF_VERIFY_INT(retValP)
        {
          returnVal = retValP->GetInt();
        }
      IF_VERIFY_STRING(messageP)
        {
          message = messageP->GetString();
        }
      parsed = true;
      LOGID("cPinnedAoiReply::parseMsg: cmd = %d, id = %d, size = %d, "
            "angle = %d, retVal = %d, message = %s\n",
            cmd, id, size, angle, returnVal, message.c_str());
    }
  else if (isPinnedAoiMsg)
    {
      LOGW("cPinnedAoiReply::parseMsg: Bad PinnedAoiReply message \"%s\"\n",
           pcMsg);
    }
  else
    {
      LOGW("cPinnedAoiReply::parseMsg: Not a PinnedAoiReply message \"%s\"\n",
           pcMsg);
    }

  return parsed;
}

/** Construct a JSON message in the supplied buffer for these args.
 *
 * @param pMsgBuf is the buffer in which to construct the JSON message.
 * @param bufLen is the length of the buffer.
 *
 * @return true if the message fit in the buffer.
 */
bool cPinnedAoiReply::buildMsg(char *pMsgBuf, int bufLen)
{
  bool retVal = false;

  // Create document and load our document model
  Document doc;
  doc.Parse(pcPinnedAoiReplyDOM);

  // Verify existence of the 'cmd' member under the 'PinnedAoi' member
  if (Value *cmdP = Pointer(PINNED_AOI_REPLY_CMD_PTR).Get(doc))
    {
      // Exists, so set it
      cmdP->SetString(cmd.toString(), doc.GetAllocator());
      // Set the id, size, and angle values, too
      Value *idP = Pointer(PINNED_AOI_REPLY_ID_PTR).Get(doc);
      Value *sizeP = Pointer(PINNED_AOI_REPLY_SIZE_PTR).Get(doc);
      Value *angleP = Pointer(PINNED_AOI_REPLY_ANGLE_PTR).Get(doc);
      if (idP != nullptr) { idP->SetInt(id); }
      if (sizeP != nullptr) { sizeP->SetInt(size); }
      if (angleP != nullptr)
        {
          // Rotate to align with the world coordinate system, and
          // mirror in X to match perspective of phone
          angle = circleMath::intAddAngs(angle, CAMERA_ALIGNMENT_ANGLE);
          angle = circleMath::intSubAngs(360, angle);
          angleP->SetInt(angle);
        }
      Value &retValP =
          Pointer(PINNED_AOI_REPLY_RETURNVAL_PTR).GetWithDefault(doc, 0);
      retValP.SetInt(returnVal);
      Value &messageP =
          Pointer(PINNED_AOI_REPLY_MESSAGE_PTR).GetWithDefault(doc, "");
      messageP.SetString(message.c_str(), doc.GetAllocator());
      LOGID("cPinnedAoiReply::buildMsg: cmd = %d, id = %d, size = %d, "
            "angle = %d, retVal = %d, message = %s\n",
            cmd, id, size, angle, returnVal, message.c_str());
    }

  // Create stream writer to access doc as a string
  StringBuffer buf;
  Writer<StringBuffer> writer(buf);
  doc.Accept(writer);
  const char *pinnedAoiMsg = buf.GetString();

  // As long as it will fit, copy the JSON into the supplied buffer
  if (strlen(pinnedAoiMsg) <= bufLen)
    {
      strncpy(pMsgBuf, pinnedAoiMsg, bufLen);
      retVal = true;
    }

  return retVal;
}

