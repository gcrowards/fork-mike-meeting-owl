/*
 * switchboardServer.cpp
 *
 * Copyright (c) Owl Labs Inc. 2017.
 * All rights reserved.
 *
 *  Implements the OIPC (Owl Inter-Process Communications) server. The server
 *  allows clients to connect on the port 'TCP_PORT', subscribe to types
 *  of messages, and handle routing among clients. Each client writes messages
 *  to the server identified by type. Messages of a given type are then
 *  forwarded to every client that subscribed to that type.
 */
#include <netinet/in.h>
#include <pthread.h>
#include <sys/socket.h>
#include <stdio.h>
#include <string>
#include <string.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>

#include "logging.hpp"
#include "switchboard/switchboardServer.hpp"

// #define DEBUG_SWITCHBOARD_SERVER
#ifdef DEBUG_SWITCHBOARD_SERVER
#define LOGID LOGI
#else
#define LOGID(...) {}
#endif

/* *** cSwitchboardServer *** */

/** Construct an Owl IPC server object and start it running.
 *
 */
cSwitchboardServer::cSwitchboardServer() :
  mWriterThread(std::bind(&cSwitchboardServer::writeManager,
                          this), "serverWriteMgr"),
  mpServerConnectionFactoryThread(nullptr),
  mRestartThread(std::bind(&cSwitchboardServer::restartServer, this),
                 "restartServer"),
  mSockFd(-1)
{
  mWriterThread.start();
  mRestartThread.start();

  pthread_mutex_init(&mSubListMutex, NULL);
}

/** Tear down an Owl IPC server.
 *
 */
cSwitchboardServer::~cSwitchboardServer()
{
  mRestartThread.signalStop();
  mWriterThread.signalStop();
  mRestartThread.join();
  mWriterThread.join();
  LOGID("~cSwitchboardServer: Restart and Writer threads done\n");

  socketFinish();

  pthread_mutex_destroy(&mSubListMutex);
}

/** Shut down the server instances and the main server socket.
 *
 */
void cSwitchboardServer::socketFinish()
{
  mpServerConnectionFactoryThread->signalStop();

  lockSubscriptionList();
  for (std::vector<instanceSubscriptionList_t *>::iterator
      pList = mSubscriptionListsList.begin();
      pList != mSubscriptionListsList.end();
      pList++)
    {
      (*pList)->pInst->writeMessage(CLIENT_PLEASE_CLOSE, NULL, 0);
    }
  unlockSubscriptionList();

  /* This is a little scary: the delay is needed to allow the clients to
   * receive the message to close, actually close, and then inform the
   * server socket that they've closed. TODO: It might be possible to use a
   * select call here with a timeout to be more responsive and robust.
   */
  usleep(2000000);
  /* Polling loop waits for the subscription list to become
   * available and leaves the mutex locked once it is.
   */
  lockSubscriptionListExclusive(true);
  LOGID("socketFinish: list size = %d\n", mSubscriptionListsList.size());
  for (std::vector<instanceSubscriptionList_t *>::iterator
      pList = mSubscriptionListsList.begin();
      pList != mSubscriptionListsList.end();
      pList++)
    {
      LOGID("socketFinish: starting deletes\n");
      delete (*pList)->pInst;
      (*pList)->pInst = nullptr;
    }
  mSubscriptionListsList.clear();
  unlockSubscriptionListExclusive();

  LOGID("socketFinish: shutdown\n");
  if (shutdown(mSockFd, SHUT_RDWR) < 0)
    {
      LOGID("server: shutdown of socket failed.\n");
    }
  close(mSockFd);
  mSockFd = -1;

  mpServerConnectionFactoryThread->join();
  delete mpServerConnectionFactoryThread;
  mpServerConnectionFactoryThread = nullptr;
}

/** Starts up the server, or restarts it if it fails.
 *
 * This is the function called in a thread loop. It opens and connects
 * the socket, then starts the client factory thread that creates a new
 * server thread for each client that connects to the server. It retries
 * every 1/10 second.
 */
void cSwitchboardServer::restartServer(void)
{
  if ((mpServerConnectionFactoryThread != nullptr) &&
      !mpServerConnectionFactoryThread->isRunning())
    {
      socketFinish();
    }
  else if (mSockFd < 0)
    {
      LOGID("restartServer: socket unconnected\n");
      // Try to connect the socket
      if (connectSocket() >= 0)
        {
          // Socket is now good, create server factory thread
          mpServerConnectionFactoryThread =
              new cThread(std::bind(&cSwitchboardServer::serverFactory,
                                    this), "serverClientFty");
          mpServerConnectionFactoryThread->start();
          LOGID("restartServer: server factory started\n");
        }
    }
  else
    {
      pruneDeadInstances();
    }
  // Wait a while, then check again if socket needs startup
  usleep(100000);
}

/** Function called from the instance when a client disconnects.
 *
 */
void cSwitchboardServer::clientDyingCallback(cServerInstance *dyingClient,
                                             eDyingReason_t reason)
{
  LOGID("clientDyingCallback\n");

  dyingClient->setTerminationReason(reason); // Retry manager will take care
                                             // of actual deletion of the
                                             // instance.
}

/** Starts up the socket connection to prepare for connecting to clients.
 *
 */
int cSwitchboardServer::connectSocket(void)
{
  int sockFd;
  socklen_t  clilen;
  struct sockaddr_in cli_addr, serv_addr;
  struct linger lingerParams = { .l_onoff = 0, .l_linger = 1 };

  if ((sockFd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
      LOGID("server: can't open stream socket\n");
      if (mpServerConnectionFactoryThread != nullptr)
        { mpServerConnectionFactoryThread->signalStop(); }
      mSockFd = -1;
      return -1;
    }

  memset((char *) &serv_addr, 0, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  serv_addr.sin_port = htons(TCP_PORT);

  if (bind(sockFd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
    {
      LOGID("server: can't bind local address\n");
      if (mpServerConnectionFactoryThread != nullptr)
        { mpServerConnectionFactoryThread->signalStop(); }
      shutdown(sockFd, SHUT_WR);
      close(sockFd);
      mSockFd = -1;
      return -2;
    }

  listen(sockFd, 5);

  if (setsockopt(sockFd, SOL_SOCKET, SO_LINGER,
                 &lingerParams, sizeof(lingerParams)) < 0)
    {
      LOGID("serverFactory: setsockopt failed.\n");
    }

  mSockFd = sockFd;

  return 0;
}

/** Function returns true if a message type is in the subscribed list.
 *
 * @param list Vector containing all types to which this client has subscribed.
 * @param type The message type being sought.
 *
 * @return true if the type is in the list, false otherwise.
 */
bool cSwitchboardServer::isSubscribed(std::vector<eOipcMessageType>list,
                                      eOipcMessageType type)
{
  for (std::vector<eOipcMessageType>::iterator pT = list.begin();
      pT != list.end();
      pT++)
    {
      if ((*pT) == type)
        { return true; }
    }
  return false;
}

/** Internal function that sends provided message to all subscribed clients.
 *
 * @param pHeader Pointer to message header to be sent.
 * @param pcMsg Pointer to message payload to be sent.
 */
void cSwitchboardServer::sendToClients(oipcMessageHeader *pHeader, char *pcMsg)
{
  lockSubscriptionList();
  for (std::vector<instanceSubscriptionList_t *>::iterator
      pList  = mSubscriptionListsList.begin();
      pList != mSubscriptionListsList.end();
      pList++)
    {
      if ((!(*pList)->pInst->isTerminated()) &&
          (isSubscribed((*pList)->typeList, (eOipcMessageType)pHeader->type)))
        {
          (*pList)->pInst->writeMessage((eOipcMessageType)pHeader->type,
                                 pcMsg,
                                 pHeader->length);
        }
    }
  unlockSubscriptionList();
}

/** Internal function that deletes one terminated server instance per call.
 *
 * This should be called periodically to keep the server instance list clear
 * of dead client connections. (Called by retry thread that also restarts
 * the server if it fails.)
 */
void cSwitchboardServer::pruneDeadInstances()
{
  for (std::vector<instanceSubscriptionList_t *>::iterator
      pList  = mSubscriptionListsList.begin();
      pList != mSubscriptionListsList.end();
      pList++)
    {
      if ((*pList)->pInst->isTerminated())
        {
          LOGID("pruneDeadInstances: Removing terminated thread %d\n",
               (*pList)->pInst->getSockFd());
          pthread_mutex_lock(&mSubListMutex);
          if (mSubscriptionListLooperCount == 0)
            {
              delete (*pList)->pInst;
              delete (*pList);
              mSubscriptionListsList.erase(pList);
            }
          // else wait until next iteration to delete entry
          pthread_mutex_unlock(&mSubListMutex);
          break;
        }
    }
}

/** Locks out subscription list changers while using the list.
 *
 */
void cSwitchboardServer::lockSubscriptionList(void)
{
  pthread_mutex_lock(&mSubListMutex);
  mSubscriptionListLooperCount++;
  pthread_mutex_unlock(&mSubListMutex);
}

/** Unlocks subscription list allowing access to change the list.
 *
 */
void cSwitchboardServer::unlockSubscriptionList(void)
{
  pthread_mutex_lock(&mSubListMutex);
  mSubscriptionListLooperCount--;
  pthread_mutex_unlock(&mSubListMutex);
}

/** Locks subscription list guaranteeing no other thread is using it.
 *
 * Note: This leaves the mSubListMutex locked if it succeeds, and it
 * will always succeed if the optional argument wait is true (default).
 *
 * @param wait (optional; default = true) causes the function to retry
 * until success if true, otherwise tries once and returns result.
 *
 * @return true if the lock was acquired, false if not.
 */
bool cSwitchboardServer::lockSubscriptionListExclusive(bool wait)
{
  bool locked = false;

  /* Polling loop waits for the subscription list to become
   * available and leaves the mutex locked once it is.
   */
  do {
      pthread_mutex_lock(&mSubListMutex);
      if (mSubscriptionListLooperCount > 0)
        {
          pthread_mutex_unlock(&mSubListMutex);
          if (wait) { usleep(1000); }
        }
      else
        {
          locked = true;
        }
  } while (!locked && wait);

  return locked;
}

/** Unlock the subscription list acquired exclusively.
 *
 */
void cSwitchboardServer::unlockSubscriptionListExclusive(void)
{
  pthread_mutex_unlock(&mSubListMutex);
}

/** Function forwards received messages from reader to subscribed clients.
 *
 * Checks each client connection for a pending message. If a message is
 * present, then it is forwarded out to each client that has subscribed to
 * that message type.
 *
 * This function also intercepts messages used for server management
 * for clients to subscribe to and unsubscribe from message types.
 *
 * After a message has been handled, it is released so the client can
 * accept additional messages.
 *
 * TODO: This function is currently polled at regular intervals, but
 * could use select to wait for activity on the server instances to be
 * more responsive and support faster throughput.
 */
void cSwitchboardServer::writeManager(void)
{
  oipcMessageHeader *pHeader;
  char *pMsg;
  bool msgFound;
  bool restart;

  do {
      msgFound = false;
      restart = false;
      lockSubscriptionList();
      for (std::vector<instanceSubscriptionList_t *>::iterator
          pList =  mSubscriptionListsList.begin();
          (pList != mSubscriptionListsList.end()) && !restart;
          pList++)
        {
          if ((*pList)->pInst->hasMessage())
            {
              (*pList)->pInst->getMessage(&pHeader, &pMsg);
              switch (pHeader->type)
              {
                case CLIENT_PLEASE_CLOSE:
                  // Make sure not to pass these along
                  break;
                case SUBSCRIPTION_ADD:
                  {
                    // Client to server message, handle here
                    oipcSubscriptionMessage *pSub =
                        (oipcSubscriptionMessage *)pMsg;
                    pSub->type = ntohl(pSub->type);
                    LOGID("writeManager: Subscribe to %d\n", pSub->type);
                    unlockSubscriptionList();
                    addSubscription((*pList)->pInst,
                                    (eOipcMessageType)pSub->type);
                    restart = true;
                    lockSubscriptionList();
                  }
                  break;
                case SUBSCRIPTION_REMOVE:
                  {
                    // Client to server message, handle here
                    oipcSubscriptionMessage *pSub =
                        (oipcSubscriptionMessage *)pMsg;
                    pSub->type = ntohl(pSub->type);
                    LOGID("writeManager: Unsubscribe from %d\n", pSub->type);
                    unlockSubscriptionList();
                    removeSubscription((*pList)->pInst,
                                       (eOipcMessageType)pSub->type);
                    restart = true;
                    lockSubscriptionList();
                  }
                  break;
                default:
                  LOGID("writeManager: Msg(type:%d, len:%d): %s\n",
                       pHeader->type,
                       pHeader->length,
                       pMsg);
                  sendToClients(pHeader, pMsg);
                  break;
              }
              (*pList)->pInst->releaseMessage();
              msgFound = true;
              break;
            }
          if (restart)
            { break; }
        }
      unlockSubscriptionList();
    } while ((msgFound || restart) && mWriterThread.isRunning());

  // Wait before polling again (but don't if thread has been stopped)
  if (mWriterThread.isRunning())
    { usleep(10000); }
}

/** Internal thread function that creates new server instances per client.
 *
 * This function will typically spend almost all its time waiting in
 * accept(), and only returns when 1) a new client wants to connect, or 2)
 * the socket has been closed. When a new client wants to connect, this
 * creates a new server instance object to manage the connection.
 */
void cSwitchboardServer::serverFactory(void)
{
  int        newSockFd;
  socklen_t  clilen;
  struct sockaddr_in cli_addr;

  if (mSockFd >= 0)
    {
      clilen = sizeof(cli_addr);
      newSockFd = accept(mSockFd, (struct sockaddr *) &cli_addr, &clilen);

      if (newSockFd < 0)
        {
          LOGID("server: accept error\n");
          // Stop the factory - the socket has been closed
          mpServerConnectionFactoryThread->signalStop();
          return;
        }
      else
        { LOGID("serverFactory: accept returned successfully\n"); }

      /* Create a new thread to process the incoming request */
      cSwitchboardServer::cServerInstance *pServer =
          new cSwitchboardServer::cServerInstance(newSockFd,
                             std::bind(&cSwitchboardServer::clientDyingCallback,
                                       this,
                                       std::placeholders::_1,
                                       std::placeholders::_2));
      instanceSubscriptionList_t *pNewList = new instanceSubscriptionList_t;
      pNewList->pInst = pServer;
      lockSubscriptionListExclusive(true);
      mSubscriptionListsList.push_back(pNewList);
      unlockSubscriptionListExclusive();
    }
  else
    {
      // Socket is not connected, so wait a bit before checking again.
      usleep(10000);
    }

  /* The server is now free to accept another socket request.
   * Allow the thread to loop and restart the factory.
   */
  return;
}

/** Internal function to add a message type subscription requested by a client.
 *
 * Checks first to see if the type is already subscribed, and if not, adds
 * the type to the subscription list.
 */
void cSwitchboardServer::addSubscription(
    cSwitchboardServer::cServerInstance *pServerInst,
    eOipcMessageType type)
{
  lockSubscriptionListExclusive(true);
  for (std::vector<instanceSubscriptionList_t *>::iterator
      pInst =  mSubscriptionListsList.begin();
      pInst != mSubscriptionListsList.end();
      pInst++)
    {
      if (pServerInst == (*pInst)->pInst)
        {
          bool found = false;

          for (std::vector<eOipcMessageType>::iterator
              pType =  (*pInst)->typeList.begin();
              pType != (*pInst)->typeList.end();
              pType++)
            {
              if ((*pType) == type)
                { found = true; break; }
            }
          if (!found)
            { (*pInst)->typeList.push_back(type); }
          break;  // All done
        }
    }
  unlockSubscriptionListExclusive();
}

/** Internal function to remove a subscription at a client's request.
 *
 */
void cSwitchboardServer::removeSubscription(
    cSwitchboardServer::cServerInstance *pServerInst,
    eOipcMessageType type)
{
  lockSubscriptionListExclusive(true);
  for (std::vector<instanceSubscriptionList_t *>::iterator
      pInst =  mSubscriptionListsList.begin();
      pInst != mSubscriptionListsList.end();
      pInst++)
    {
      if (pServerInst == (*pInst)->pInst)
        {
          for (std::vector<eOipcMessageType>::iterator
              pType =  (*pInst)->typeList.begin();
              pType != (*pInst)->typeList.end();
              pType++)
            {
              if ((*pType) == type)
                {
                  LOGID("removeSubscription: erasing type %d\n", type);
                  (*pInst)->typeList.erase(pType);
                  break;
                }
            }
        }
    }
  unlockSubscriptionListExclusive();
}



/* *** cServerInstance nested class of cSwitchboardServer *** */

/** Create a server instance object to manage connection to a client.
 *
 * @param sockFd A new socket file descriptor to communicate with client.
 * @param dyingCB Parent function to call when client dies.
 */
cSwitchboardServer::cServerInstance::cServerInstance(int sockFd, clientDyingCallback_t dyingCB)
  : mSockFd(sockFd),
    mClientDyingCB(dyingCB),
    mMsgValid(false),
    mTerminationReason(NONE)
{
  char name[128];

  snprintf(name, sizeof(name), "serverInst_%d", sockFd);
  std::string threadName = name;
  mpInstanceThread = new cThread(std::bind(&cServerInstance::serverInstanceFunc,
                                   this), threadName);
  mpInstanceThread->start();
}

/** Destroy a server instance that managed a connection to a client.
 *
 */
cSwitchboardServer::cServerInstance::~cServerInstance(void)
{
  LOGID("cServerInstance::~cServerInstance\n");
  terminate(mTerminationReason == NONE ? NORMAL_SHUTDOWN : mTerminationReason);
}

/** Tear down the socket and reader thread for a server instance.
 *
 * NOTE: All the protections for multiple calls to this function should
 * no longer be required because now only one function calls this.
 */
void cSwitchboardServer::cServerInstance::terminate(eDyingReason_t reason)
{
  if ((reason == VERSION_MISMATCH) && (mSockFd >= 0))
    {
      writeMessage(CLIENT_VERSION_ERROR, NULL, 0);
    }

  if (mSockFd >= 0)
    {
      LOGID("serverInstance::terminate: shutdown\n");
      shutdown(mSockFd, SHUT_WR);
      usleep(10000);
    }

  if (mpInstanceThread != nullptr)
    {
      mpInstanceThread->signalStop();
      mpInstanceThread->join();
      delete mpInstanceThread;
      mpInstanceThread = nullptr;
    }

  if (mSockFd >= 0)
    {
      LOGID("serverInstance::terminate: close %d\n", mSockFd);
      close(mSockFd);
      mSockFd = -1;
    }
}

/** Writes a message to the connected client of this instance.
 *
 * @param type Message type to write.
 * @param pcMsg Pointer to buffer containing message payload.
 * @param length Length of the message payload.
 */
void cSwitchboardServer::cServerInstance::writeMessage(eOipcMessageType type,
                                                       char *pcMsg,
                                                       uint32_t length)
{
  if (mSockFd < 0)
    {
      LOGID("writeMessage: mSockFd invalid!\n");
    }
  else
    {
      gWriteMessage(mSockFd, type, pcMsg, length);
    }
}

/** Returns true if a new message has been received from the connected client.
 *
 * After a new message is received, this function will continue to return
 * true until the releaseMessage() method is called on this instance.
 *
 * @return true if a new message is available, otherwise false
 */
bool cSwitchboardServer::cServerInstance::hasMessage(void)
{
  return mMsgValid;
}

/** Returns pointers to the header and message payload for this instance.
 *
 * This function is only used internally (so it is Ok to return pointers.)
 *
 * @param ppheader Pointer to set to point to the message header pointer.
 * @param ppcMsg Pointer to set to point to the message payload pointer.
 */
bool cSwitchboardServer::cServerInstance::getMessage(
    oipcMessageHeader **ppHeader, char **ppMsg)
{
  *ppHeader = &mHeader;
  *ppMsg = mMsg;
  return mMsgValid;
}

/** Marks the instance's message as no longer valid.
 *
 * When a message is valid, the server writer thread must read the message,
 * forward it to subscribed clients, then release the message.
 */
void cSwitchboardServer::cServerInstance::releaseMessage(void)
{
  mMsgValid = false;
}

/** Set reason flagging instance as ready for termination.
 *
 * @param reason indicates the circumstances of the instance's death.
 */
void cSwitchboardServer::cServerInstance::setTerminationReason(
    eDyingReason_t reason)
{
  mTerminationReason = reason;
}

/** Indicates if an instance thread has been terminated.
 *
 * @return true if the socket has closed terminating the thread, else false.
 */
bool cSwitchboardServer::cServerInstance::isTerminated()
{
  return ((mSockFd < 0) || (mTerminationReason != NONE));
}

/** Returns the socket file descriptor for this instance.
 *
 * @return mSockFd File descriptor of this server's socket connection.
 *
 */
int cSwitchboardServer::cServerInstance::getSockFd(void)
{
  return mSockFd;
}

/** Thread function that reads messages from a client for a server instance.
 *
 * Each instance buffers a single message from a client. The writer thread
 * polls all the instances and writes the message to any clients subscribed
 * to receive that message type. Once all the subscribed clients have received
 * the message, then the writer thread clears the message valid flag, and
 * the server instance can read the next message from its client.
 */
void cSwitchboardServer::cServerInstance::serverInstanceFunc(void)
{
  // Read a message mHeader
  int numRead;
  int readRet;

  if (mMsgValid)
    { // Server has a valid message, wait for it to be cleared
      usleep(10000);
      return;
    }
  else
    {
      readRet = gReadMessage(mSockFd, &mHeader, mMsg, sizeof(mMsg));
      if (readRet < 0)
        {
          LOGID("serverInstanceFunc: readMessage server returned error\n");
          mClientDyingCB(this,
                         (readRet == -5) ? VERSION_MISMATCH : NORMAL_SHUTDOWN);
        }
      else
        {
          // For now, the writer polls to see this has been set, later, signal!
          mMsgValid = true;
          LOGID("serverInstanceFunc: Got a message!\n");
        }
    }
}



/* *** Global shared socket comms functions *** */

/** General Owl IPC message reader function.
 *
 * This function reads from a socket looking for the magic number that
 * identifies the beginning of a message. Once the beginning of a message is
 * found, the message header is read, including the length of the following
 * message payload. The payload data is then read. Each read may return
 * less data than requested, so the reads are repeated until sufficient data
 * has been received.
 *
 * @param sockFd open socket file descriptor.
 * @param pHeader Pointer to the header structure to be populated.
 * @param pcMsg Pointer to buffer to fill with message payload.
 * @param length Length of the buffer for the payload.
 *
 * @return 0 on success, negative number on failure.
 */
int gReadMessage(int sockFd, oipcMessageHeader *pHeader,
                 char *pcMsg, size_t length)
{
  int val = 0;
  int hVal;
  char *pcVal = (char *)&val;
  char pHeaderBuf[sizeof(*pHeader)];
  bool magic = false;
  int numRead;
  int numToRead;
  int byteCount;

  // Read bytes from socket until 4 consecutive bytes make the magic number
  while (!magic)
    {
      byteCount = read(sockFd, (void *)&pcVal[3], 1);
      if (byteCount <= 0)
        {  // Error or exit
          LOGID("readMessage: byteCount1 = %d\n", byteCount);
          pHeader->type = MIN_OIPC_TYPE;
          return -1;
        }
      else
        {
          hVal = ntohl(val);  // Convert from network to host byte order
          LOGID("readMessage: got char 0x%02X, hVal = 0x%08X\n", pcVal[3], hVal);
          if (hVal == OIPC_MAGIC)
            {
              magic = true;
            }
          else
            {
              pcVal[0] = pcVal[1];
              pcVal[1] = pcVal[2];
              pcVal[2] = pcVal[3];
            }
        }
    }
  pHeader->magic = OIPC_MAGIC;
  *((int *)pHeaderBuf) = val;  // Set the magic number in the header buffer
  LOGID("readMessage: magic found!\n");

  // Read the remainder of the header from the socket
  numRead = sizeof(pHeader->magic);
  numToRead = sizeof(*pHeader) - sizeof(pHeader->magic);
  while (numToRead > 0)
    {
      byteCount = read(sockFd, (void *)&pHeaderBuf[numRead], numToRead);
      if (byteCount > 0)
        {
          numToRead -= byteCount;
          numRead += byteCount;
        }
      else
        {
          LOGID("readMessage: byteCount2 = %d\n", byteCount);
          pHeader->type = MIN_OIPC_TYPE;
         return -2;
        }
    }
  *pHeader = *(oipcMessageHeader *)pHeaderBuf;
  pHeader->magic = ntohl(pHeader->magic);
  pHeader->major = ntohs(pHeader->major);
  pHeader->minor = ntohs(pHeader->minor);
  pHeader->type = ntohl(pHeader->type);
  pHeader->length = ntohl(pHeader->length);
  LOGID("readMessage: header.magic=0x%X, .major=0x%X, .minor = 0x%X, .type=%d, .length=%d\n",
       pHeader->magic, pHeader->major, pHeader->minor,
       pHeader->type, pHeader->length);

  if (pHeader->major != OIPC_MAJOR_VERSION)
    {
      LOGID("readMessage: Error - major version mismatch (%d != %d)\n",
           pHeader->major, OIPC_MAJOR_VERSION);
      return -5;
    }
  if (pHeader->minor > OIPC_MINOR_VERSION)
    {
      LOGID("readMessage: Warning - minor version mismatch (%d > %d)\n",
           pHeader->minor, OIPC_MINOR_VERSION);
    }

  // Now read the payload
  if ((pHeader->magic == OIPC_MAGIC) &&
      (pHeader->length <= length) &&
      (pHeader->length > 0))
    {
      int numToRead = pHeader->length;
      int numRead = 0;

      LOGID("readMessage: Reading %d bytes...\n", numToRead);
      while (numToRead > 0)
        {
          byteCount = read(sockFd, (void *)&pcMsg[numRead], numToRead);
          if (byteCount >= 0)
            {
              numToRead -= byteCount;
              numRead += byteCount;
            }
          else
            {
              pHeader->type = MIN_OIPC_TYPE;
              return -3;
            }
        }
      LOGID("readMessage: Read %d bytes - %02X %02X %02X %02X...\n",
           numRead, pcMsg[0], pcMsg[1], pcMsg[2], pcMsg[3]);
      LOGID("Done.\n");
    }
  else if (pHeader->length > length)
    {
      LOGID("gReadMessage: Payload length %d too long (max: %d)\n",
           pHeader->length, length);
      pHeader->type = MIN_OIPC_TYPE;
      return -4;
    }

  return 0;
}

/** General Owl IPC message writer function.
 *
 * Writes the message header and payload to an open socket file descriptor.
 *
 * @param sockFd Open socket file descriptor.
 * @param type Type of message to be sent.
 * @param pcMsg Pointer to buffer containing the message payload to send.
 * @param length Length of the payload.
 *
 * @return 0 on success, negative number on failure.
 */
int gWriteMessage(int sockFd, eOipcMessageType type, char *pcMsg, size_t len)
{
  oipcMessageHeader header;
  int returnVal = 0;

  header.magic  = htonl(OIPC_MAGIC);
  header.major  = htons(OIPC_MAJOR_VERSION);
  header.minor  = htons(OIPC_MINOR_VERSION);
  header.type   = htonl(type);
  header.length = htonl(len);

  if (write(sockFd, (void *)&header, sizeof(header)) != sizeof(header))
    {
      LOGID("writeMessage: header failed\n");
      returnVal = -1;
    }
  else if ((pcMsg != nullptr) && (len > 0))
    {
      if (write(sockFd, (void *)pcMsg, len) != len)
        {
          LOGID("writeMessage: payload failed\n");
          returnVal = -2;
        }
    }

  return returnVal;
}

