/*
 * setupProg.cpp
 *
 *  Created on: May 7, 2017
 */

#include "logging.hpp"
#include "rapidjson/document.h"
#include "rapidjson/pointer.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "switchboard/setupProg.hpp"
#include "switchboard/switchboardMessages.hpp"


#define DEBUG_SETUP_PROGRESS_MSGS
#ifdef  DEBUG_SETUP_PROGRESS_MSGS
#define LOGID LOGI
#else
#define LOGID(...) {}
#endif


/** Templates for creating SetupProgressSet messages. */
const char pcSetupProgSetDOM[] =
    "{\"" SETUP_PROGRESS_SET_STR "\":"              \
        "{\"" SETUP_PROGRESS_STATE_STR "\":0}}";

const char pcSetupProgGetReplyDOM[] =
    "{\"" SETUP_PROGRESS_GET_REPLY_STR "\":"        \
        "{\"" SETUP_PROGRESS_STATE_STR "\":0}}";

using namespace rapidjson;

/**
 *
 */
cSetupProgressMsgs::cSetupProgressMsgs(std::string type)
  : mType(type),
    mState(eSetupProgress_t::NONE),
    mConsumed(false)
{
}

/**
 *
 */
cSetupProgressMsgs::cSetupProgressMsgs(std::string type, eSetupProgress_t state)
  : mType(type),
    mState(state),
    mConsumed(false)
{
}

/**
 *
 */
cSetupProgressMsgs::~cSetupProgressMsgs(void)
{
}

/**
 *
 */
eSetupProgress_t cSetupProgressMsgs::getState(void)
{
  return mState;
}

/**
 *
 */
bool cSetupProgressMsgs::getArgsConsumed(void)
{
  return mConsumed;
}

/**
 *
 */
void cSetupProgressMsgs::setArgsConsumed(void)
{
  mConsumed = true;
}

/**
 *
 */
void cSetupProgressMsgs::abortArgs(void)
{
  mConsumed = true;
}

/** Return the argument parameters contained in this class.
 *
 * @param state is the state value contained in the message.
 */
bool cSetupProgressMsgs::getArgs(eSetupProgress_t &state)
{
  bool previouslyConsumed = mConsumed;

  state = mState;

  return previouslyConsumed;
}

/** Parse the given JSON message into the fields of this class.
 *
 * @param pcMsg is the JSON message string to parse.
 *
 * @return true if the JSON message is well formed and parsed.
 */
bool cSetupProgressMsgs::parseMsg(const char *pcMsg)
{
  bool parsed = false;
  Document doc;
  doc.Parse(pcMsg);
  bool isProperMsg = doc.HasMember(mType.c_str());
  Value *stateP;
  int state;

  if (mType.compare(SETUP_PROGRESS_SET_STR) == 0)
    {
      stateP = Pointer(SETUP_PROGRESS_SET_STATE_PTR).Get(doc);
    }
  else
    {
      stateP = Pointer(SETUP_PROGRESS_GET_REPLY_STATE_PTR).Get(doc);
    }

  if (isProperMsg && (stateP != nullptr))
    {
      IF_VERIFY_INT(stateP)
        {
          state = stateP->GetInt();
          mState = eSetupProgress_t(state);
          mConsumed = false;
          parsed = true;
          LOGID("cSetupProgressMsgs::parseMsg: state = %d (%s)\n",
               mState, mState.toString());
        }
    }
  else if (isProperMsg)
    {
      LOGW("cSetupProgressMsgs::parseMsg: Bad SetupProgress message \"%s\"\n",
           pcMsg);
    }
  else
    {
      LOGW("cSetupProgressMsgs::parseMsg: Not a SetupProgress message \"%s\"\n",
           pcMsg);
    }

  return parsed;
}

/** Construct a JSON message in the supplied buffer for these args.
 *
 * @param pMsgBuf is the buffer to fill with the JSON message.
 * @param bufLen is the maximum capacity of the buffer.
 *
 * @return true if the message fits in the provided buffer.
 */
bool cSetupProgressMsgs::buildMsg(char *pMsgBuf, int bufLen)
{
  bool retVal = false;
  std::string stateAccessor;

  // Create document and load our document model
  Document doc;
  if (mType.compare(SETUP_PROGRESS_SET_STR) == 0)
    {
      doc.Parse(pcSetupProgSetDOM);
      stateAccessor = std::string(SETUP_PROGRESS_SET_STATE_PTR);
    }
  else
    {
      doc.Parse(pcSetupProgGetReplyDOM);
      stateAccessor = std::string(SETUP_PROGRESS_GET_REPLY_STATE_PTR);
    }

  // Verify existence of the 'cmd' member under the 'PinnedAoi' member
  if (Value *stateP = Pointer(stateAccessor.c_str()).Get(doc))
    {
      // Exists, so set it
      stateP->SetInt(mState);
    }

  // Create stream writer to access doc as a string
  StringBuffer buf;
  Writer<StringBuffer> writer(buf);
  doc.Accept(writer);
  const char *msg = buf.GetString();

  // As long as it will fit, copy the JSON into the supplied buffer
  if (strlen(msg) <= bufLen)
    {
      strncpy(pMsgBuf, msg, bufLen);
      retVal = true;
    }

  return retVal;
}

