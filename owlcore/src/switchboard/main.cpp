/** main.cpp
 *
 * Module that implements the switchboardTest interface for exchanging
 * messages to test the switchboard interface with the Owl.
 *
 * Copyright: Owl Labs Inc. 2017
 */

#include <algorithm>
#include <fcntl.h>
#include <pthread.h>
#include <sys/resource.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <sys/time.h>
#include <unistd.h>

#ifdef __ANDROID__
#include "libOwlPG.h"
#endif

#include "beamMath.hpp"
#include "debugInfo.hpp"
#include "gpu.hpp"
#include "logging.hpp"
#include "main.hpp"
#include "owlHook.hpp"
#include "params.h"
#include "rapidjson/document.h"
#include "rapidjson/pointer.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "switchboard/audioParams.hpp"
#include "switchboard/jsonListReader.hpp"
#include "switchboard/jsonListWriter.hpp"
#include "switchboard/meetingInfo.hpp"
#include "switchboard/pinnedAoiArgs.hpp"
#include "switchboard/pinnedAoiReply.hpp"
#include "switchboard/setupProg.hpp"
#include "switchboard/uiAction.hpp"
#include "switchboard/switchboardClient.hpp"
#include "switchboard/switchboardServer.hpp"
#include "switchboard/videoParams.hpp"
#include "thread.hpp"
#include "version.hpp"


#define CHECK(func) do { LOGD("==> %s\n", #func); if (func){ LOGE("%s\n", #func); return -1;} } while(0)

#define MAX_IP_LEN  64

static bool gTerminate = false; /**< flag for exiting main loop. */

const int kMainLoopSleepTime = 5000;

bool gDoClient = false;
bool gDoServer = true;

cSwitchboardServer *gpSwitchboardServer = nullptr;
cSwitchboardClient *gpSwitchboardClient = nullptr;

/* The following definition keeps the logging from being published to the
 * server. TODO: Find a better way!!!
 */
cSwitchboardClient *gpDebugSwitchboardClient = nullptr;

/** IP address in use for communications */
char gIpAddr[MAX_IP_LEN] = "";

/** Structure defining one menu entry */
typedef struct menuEntry_s {
  const char cmdChar;
  const char * const pPrompt;
  void *pMenuContext;
  union menuPtr_u {
    void (*pFunc)(void *);
    struct menuEntry_s *pSubMenu;
  } ptr;
  bool bLeaf;
} menuEntry_t;

/** Structure template for all menu contexts */
typedef struct genericMenuContext_s {
  menuEntry_t *pMenuItem;
} genericMenuContext_t;

/** Structure containing context used by top menu functions */
typedef struct topMenuContext_s {
  menuEntry_t *pMenuItem;
  bool bPinging;
  int minSpeakerLevel;
  int NRLevel;
} topMenuContext_t;
/** Instance of top menu context data structure */
topMenuContext_t topMenuContext = {
    NULL, false, -50, 0
};

/** Structure containing context used by audio menu functions */
typedef struct audioMenuContext_s {
  menuEntry_t *pMenuItem;
  int minSpeakerLevel;
  int NRLevel;
  int volume;
  bool mute;
  bool enableDTD;
} audioMenuContext_t;
/** Instance of top menu context data structure */
audioMenuContext_t audioMenuContext = {
    NULL, -50, 0, 90, false, false
};

/** Structure containing context used by debug menu functions */
typedef struct debugMenuContext_s {
  menuEntry_t *pMenuItem;
  bool bStreamDebug;    /**< If true stream Owl debug, else don't */
  bool bOwlSubscribedMessageDebug; /**< If true stream all messages that Owl
                                    * subscribes to */
} debugMenuContext_t;
/** Instance of debug menu context data structure */
debugMenuContext_t debugMenuContext = {
    NULL, true, false
};

/** Structure containing context used by pinned AOI menu functions */
typedef struct pinnedAoiContext_s {
  menuEntry_t *pMenuItem;
  int id;
  int size;
  int angle;
  int sizeIncrement;
  int angleIncrement;
  std::string msg;
} pinnedAoiContext_t;
/** Instance of pinnedAoi context data structure */
pinnedAoiContext_t pinnedAoiContext = {
    NULL, 0, 30, 0, 0, 5
};

/** Structure containing context used by camera parameters menu functions */
typedef struct camParamsContext_s {
  menuEntry_t *pMenuItem;
} camParamsContext_t;
/** Instance of camera parameters context data structure */
camParamsContext_t camParamsContext;

/** Auto-exposure mode context used by auto exposure menu functions */
typedef struct autoExpContext_s {
  menuEntry_t *pMenuItem;
  int expCompensation;
} autoExpContext_t;
/** Instance of auto exposure context data structure */
autoExpContext_t autoExpContext = {
    NULL, 0
};

/** Contrast context used by contrast menu functions */
typedef struct contrastContext_s {
  menuEntry_t *pMenuItem;
  int contrast;
} contrastContext_t;
/** Instance of auto exposure context data structure */
contrastContext_t contrastContext = {
    NULL, 5
};

/** Saturation context used by contrast menu functions */
typedef struct saturationContext_s {
  menuEntry_t *pMenuItem;
  int saturation;
} saturationContext_t;
/** Instance of saturation context data structure */
saturationContext_t saturationContext = {
    NULL, 5
};

/** Sharpness context used by sharpness menu functions */
typedef struct sharpnessContext_s {
  menuEntry_t *pMenuItem;
  int sharpness;
} sharpnessContext_t;
/** Instance of saturation context data structure */
sharpnessContext_t sharpnessContext = {
    NULL, 12
};

/** Preview frame rate context used by preview frame rate menu functions */
typedef struct previewFrameRateContext_s {
  menuEntry_t *pMenuItem;
  int rate;
} previewFrameRateContext_t;
/** Instance of saturation context data structure */
previewFrameRateContext_t previewFrameRateContext = {
    NULL, 30
};

/** Denoise context used by denoise state menu functions */
typedef struct denoiseContext_s {
  menuEntry_t *pMenuItem;
  bool denoise;
} denoiseContext_t;
/** Instance of saturation context data structure */
denoiseContext_t denoiseContext = {
    NULL, false
};

/** Load test context used in load test menu functions */
typedef struct loadTestContext_s {
  menuEntry_t *pMenuItem;
  int messageCount;
  struct timespec startTime;
  eOipcMessageType msgType;
  int uSecMessageDelay;
  std::string msg;
} loadTestContext_t;
/** Instance of load test context data structure */
loadTestContext_t loadTestContext = {
    NULL, 0, {0, 0}, PINNED_AOI_CMD, 1000000
};

/** Forward declaration of character reader function. */
char kbWaitForChar(int timeoutUsec);

/** Prompt user to enter a value, read and bounds check it.
 *
 * @param pPrompt points to the prompt string.
 * @param min is the minimum allowed value the user can enter.
 * @param max is the maximum allowed value the user can enter.
 * @param defaultVal is the value displayed and returned if the user only
 *     presses <Enter>.
 * @param newVal holds the new value on return.
 *
 * @return true if a value was chosen, false if user pressed <Esc>.
 */
bool getInt(const char *pPrompt, int min, int max, int defaultVal, int &newVal)
{
  char c = 0;
  int num = 0;
  bool showPrompt = true;
  int sign = 1;
  int charCount = 0;

  do {
      if (showPrompt)
        {   // prompt user to enter a number
          fprintf(stderr, "  %s (%d - %d)\n    (default: %d): ",
                  pPrompt, min, max, defaultVal);
          showPrompt = false;
        }
      c = kbWaitForChar(5000);
      if ((num == 0) && (c == '-'))
        {
          sign = -1;
          charCount++;
          fprintf(stderr, "-");
        }
      else if (isdigit(c))
        {
          num = (num * 10) + (c - '0');
          charCount++;
          fprintf(stderr, "%c", c);
        }
      else if (c == 0x1b /* <Esc> */ )
        {
          return false;
        }
      else if (c == 127)
        {   // Backspace
          charCount--;
          if (charCount == 0)
            { sign = 1; }
          num = num / 10;
          fprintf(stderr, "\b \b");
        }
      else if (c == '\n')
        {
          if (charCount == 0)
            {
              num = defaultVal;
              sign = 1;
            }
          num = num * sign;
          if ((num < min) || (num > max))
            {
              fprintf(stderr, "\nInteger %d is %s than allowed. Please try again.\n",
                      num, ((num < min) ? "smaller" : "larger"));
              num = 0;
              c = 0;
              sign = 1;
              charCount = 0;
              showPrompt = true;
            }
        }
  } while (c != '\n');
  fprintf(stderr, "\n");

  newVal = num;

  return true;
}

/** Prompt user to enter a string of text.
 *
 * @param pPrompt is the string to display to prompt user for input.
 * @param defaultStr is the default string to use if the user only presses
 *     <Enter>.
 * @param newStr on return contains the new string response to the prompt.
 * @param maxLen is the length of the newStr buffer.
 *
 * @return Length of the new string, or 0 if user cancels with Esc key.
 */
int getString(const char *pPrompt, char *defaultStr, char *newStr, int maxLen)
{
  char c;
  int len;

  fprintf(stderr, "  %s\n    (default: %s): ", pPrompt, defaultStr);
  len = 0;
  do {
      c = kbWaitForChar(5000);
      if (isprint(c) && (len < maxLen - 1))
        {
          newStr[len] = c;
          len++;
          newStr[len] = 0;
          fprintf(stderr, "%c", c);
        }
      else if (c == 127)
        {   // Backspace
          int lastIndex = len - 1;
          if (lastIndex < 0)
            { lastIndex = 0; }
          newStr[lastIndex] = 0;
          fprintf(stderr, "\b \b");
          len--;
        }
      else if (c == '\n')
        {
          if (len <= 0)
            {
              strncpy(newStr, defaultStr, maxLen);
              len = strlen(newStr);
            }
        }
  } while ((c != '\n') && (c != 0x1b /* Esc */ ));
  fprintf(stderr, "\n");

  if (c == 0x1b)
    { len = 0; }

  return len;
}

/** Prompt user to enter a boolean value.
 *
 * @param pPrompt is the string to display to prompt user for input.
 * @param bDefault is the default value to display in use if the user presses
 *     only <Enter>.
 * @param newVal is the selected boolean value on return.
 *
 * @return true if a new value was selected, false if the user pressed <Esc>.
 */
bool getBool(const char *pPrompt, bool bDefault, bool &newVal)
{
  static char stringSpace[256];
  char c;

  fprintf(stderr, "  %s\n    (default: %s): ", pPrompt,
          (bDefault) ? "T" : "F");
  stringSpace[0] = 0;
  do {
      c = kbWaitForChar(5000);
      if (((c == 'T') || (c == 'F') ||
           (c == 't') || (c == 'f') ||
           (c == '1') || (c == '0')) &&
          (strlen(stringSpace) < 1))
        {
          strncat(stringSpace, &c, sizeof(stringSpace) - 1);
          fprintf(stderr, "%c", c);
        }
      else if (c == 0x1b /* <Esc> */)
        {
          return false;
        }
      else if (c == 127)
        {   // Backspace
          int lastIndex = strlen(stringSpace) - 1;
          if (lastIndex < 0)
            { lastIndex = 0; }
          stringSpace[lastIndex] = 0;
          fprintf(stderr, "\b \b");
        }
      else if (c == '\n')
        {
          if (strlen(stringSpace) <= 0)
            {
              strncpy(stringSpace, (bDefault) ? "1" : "0",
                  sizeof(stringSpace) - 1);
            }
        }
  } while (c != '\n');
  fprintf(stderr, "\n");

  newVal = ((stringSpace[0] == 'T') ||
            (stringSpace[0] == 't') ||
            (stringSpace[0] == '1'));

  return true;
}




int  init(void);
int  finish(void);

#ifdef __ANDROID__ // Android --------------------------------------------------

/** Signal handler to catch termination events like Ctrl-C. */
static void *signalHandler(void *arg)
{
    sigset_t *set = (sigset_t *)arg;
    int sig;
    while (1)
      {
	sigwait(set, &sig);
	LOGD("Received signal %d -- %s\n", sig, strsignal(sig));
	gTerminate = true;
      }
    return NULL;
}

#else // Linux -----------------------------------------------------------------

/** Signal handler to catch termination events like Ctrl-C. */
void signalHandler(int sig)
{
  gTerminate = true;
}

#endif // Android / Linux ------------------------------------------------------


/** Prints the command-line help message. */
static void usage()
{
  //Print the message to stdout
  printf("Usage:  switchboardServerTest [OPTIONS]\n\n");
  printf("-c                 Run as switchboard client\n");
  printf("-s                 Run as switchboard server\n");
  printf("-h                 Print this message\n");
  // help info pertaining to mock input
  printf("-t testDirectory   Run with test input from the directory\n");
}

/** Configure the stdin stream to unbuffered, non-blocking mode.
 *
 * @param enable when true turns on unbuffered, non-blocking, otherwise off.
 */
void stdinUnbuffered(int enable)
{
  struct termios t;
  int stdinFlags;
  int stdinFd = fileno(stdin);

  tcgetattr(stdinFd, &t);
  stdinFlags = fcntl(stdinFd, F_GETFL);
  switch (enable) {
    case 0:
      t.c_lflag |= (ICANON | ECHO);
      stdinFlags &= ~O_NONBLOCK;
      break;
    default:
      t.c_lflag &= ~(ICANON | ECHO);
      stdinFlags |= O_NONBLOCK;
      break;
  }
  tcsetattr(stdinFd, 0, &t);
  fcntl(stdinFd, F_SETFL, stdinFlags);
}

/** Wait for keyboard input until timeout.
 *
 * Waits for up to 'timeoutUsec' microseconds for keyboard input to become
 * available. Otherwise return 0.
 *
 * @returns a typed character, or 0 if timeout.
 */
char kbWaitForChar(int timeoutUsec)
{
  char c[256];
  int numRead;
  int stdinFd = fileno(stdin);
  struct timeval tv;
  static bool kbInitialized = false;
  const int kOneMillion = 1000000;

  if (1 /* DEBUG(KEYBOARD_INPUT) */ )
    {
      if (!kbInitialized)
        {
          // Set up
          stdinUnbuffered(1);
          kbInitialized = true;
        }
      fd_set fds;
      tv.tv_sec = timeoutUsec / kOneMillion;
      tv.tv_usec = timeoutUsec % kOneMillion;
      FD_ZERO(&fds);
      FD_SET(stdinFd, &fds);
      select(stdinFd+1, &fds, NULL, NULL, &tv);
      if (FD_ISSET(stdinFd, &fds))
        {
        numRead = read(stdinFd, c, sizeof(c));
        if (numRead > 0)
          {
            c[numRead] = 0; // Terminate the input
            // LOGI("read returned %s\n", c);
          }
        }
      else
        {
          c[0] = 0;
        }

      return c[0];
    }
  else
    {
      usleep (timeoutUsec);

      return 0;
    }
}


/** Function that sends a message then waits; for use in load testing.
 *
 */
void loadTestFunc(void)
{
  LOGI("loadTestFunc(): msg='%s'\n", loadTestContext.msg.c_str());
  gpSwitchboardClient->writeMessage(loadTestContext.msgType,
                                    (char *)loadTestContext.msg.c_str(),
                                    loadTestContext.msg.length() + 1 /*terminate string*/);
  usleep(loadTestContext.uSecMessageDelay);
}


/* ******************************
 * Switchboard Callback Functions
 * These functions are called to handle messages on specific channels
 * received through the switchboard interface.
 */

/** Test callback function for testing with main().
 *
 * @param type is the type of the received message.
 * @param pcMsg is a pointer to the buffer containing the message.
 */
void showMessageCallback(eOipcMessageType type, const char *pcMsg)
{
  #define VERBOSE
  #ifdef VERBOSE
  #define LOGID LOGI
  #else
  #define LOGID(...) {}
  #endif

  LOGID("showMessage: Type: %d, Msg: %s\n", type, pcMsg);

  cJsonListReader reader;

  bool keepGoing = reader.setList(std::string(pcMsg));

  if (keepGoing)
    {
      std::string object;
      while (reader.getNextObject(std::string("PinnedAoi"), object))
        {
          cPinnedAoiArgs pinnedAoi;
          LOGID("showMessageCallback: parse args %s\n", object.c_str());
          bool parsed = pinnedAoi.parseMsg(object.c_str());
          LOGID("showMessageCallback: parsed = %s\n", ((parsed)?"true":"false"));
#if 0
          LOGID("showMessageCallback: sending response message %s\n",
                gMsg);
          gpSwitchboardClient->writeMessage((eOipcMessageType)gMsgType,
                                            gMsg,
                                            strlen(gMsg) + 1 /*terminate string*/);
#endif
          loadTestContext.messageCount++;
        }
      reader.resetList();
      while (reader.getNextObject(std::string("PinnedAoiReply"), object))
        {
          cPinnedAoiReply pinnedAoiReply;
          LOGID("showMessageCallback: parse reply %s\n", object.c_str());
          bool parsed = pinnedAoiReply.parseMsg(object.c_str());
          LOGID("showMessageCallback: parsed = %s\n", ((parsed)?"true":"false"));
          loadTestContext.messageCount++;
        }
    }
}

/** LOG display callback function for monitoring meetingOwl logs.
 *
 * @param type is the type of the received message.
 * @param pMsg is a pointer to the buffer containing the message.
 * @param msgLen is the length of the message in bytes.
 */
void showMessageOnlyCallback(eOipcMessageType type, const char *pcMsg, int msgLen)
{
  if (debugMenuContext.bOwlSubscribedMessageDebug)
    {
      fprintf(stderr, "Owl Sub: ");
      fwrite((const void *)pcMsg, sizeof(char), (size_t)strlen(pcMsg), stdout);
      fprintf(stderr, "\n");
    }
}

/** LOG display callback function for monitoring meetingOwl logs.
 *
 * @param type is the type of the received message.
 * @param pMsg is a pointer to the buffer containing the message.
 * @param msgLen is the length of the message in bytes.
 */
void showLOGCallback(eOipcMessageType type, const char *pcMsg, int msgLen)
{
  if (debugMenuContext.bStreamDebug)
    {
      fwrite((const void *)pcMsg, sizeof(char), (size_t)strlen(pcMsg), stdout);
    }
}

/** Callback to receive pinned AOI reply messages.
 *
 * Extracts the AOI ID value and stores in global for future messages.
 *
 * @param type is the type of the received message.
 * @param pMsg is a pointer to the buffer containing the message.
 * @param msgLen is the length of the message in bytes.
 */
void pinnedAoiReplyCallback(eOipcMessageType type, const char *pMsg, int msgLen)
{
  cJsonListReader reader;
  std::string object;
  cPinnedAoiReply reply;

  LOGI("pinnedAoiReplyCallback: \n%s\n\n", pMsg);

  if (reader.setList((std::string)pMsg, true))
    {
      for (int iObj = 0; iObj < reader.getObjectCount(); iObj++)
        {
          reader.getNextObject(std::string("PinnedAoiReply"), object);
          reply.parseMsg(object.c_str());
          LOGI("pinnedAoiReplyCallback: %s\n", object.c_str());
          loadTestContext.messageCount++;

          if (reply.getMessage().find("NOT") == std::string::npos)
            {   // This is a pinned AOI because "NOT" was not found
              pinnedAoiContext.id = reply.getId();
              LOGI("pinnedAoiReplyCallback: Active AOI: %d\n",
                   pinnedAoiContext.id);
            }
        }
    }
}

/** Callback to receive meeting info messages
 *
 * @param type is the type of the received message.
 * @param pMsg is a pointer to the buffer containing the message.
 * @param msgLen is the length of the message in bytes.
 */
void meetingInfoCallback(eOipcMessageType type, const char *pMsg, int msgLen)
{
  cJsonListReader reader;
  std::string object;
  cMeetingInfo info;

  if(reader.setList((std::string)pMsg, true))
    {
      for(int iObj = 0; iObj < reader.getObjectCount(); iObj++)
        {
          reader.getNextObject(std::string("MeetingInfo"), object);
          info.parseMsg(object.c_str());
          LOGI("MeetingInfoCallback: %s\n", object.c_str());

	  LOGI("\nRecieved meeting state: %s\n", (info.getState() ? "MEETING" : "NOT MEETING"));
        }
    }
}

/** Callback to receive app configured state get messages
 *
 * @param type is the type of the received message.
 * @param pMsg is a pointer to the buffer containing the message.
 * @param msgLen is the length of the message in bytes.
 */
void appConfiguredStateGetCallback(eOipcMessageType type, const char *pMsg, int msgLen)
{
  cJsonListReader reader;
  std::string object;
  cSetupProgressMsgs info(SETUP_PROGRESS_GET_REPLY_STR);

  if(reader.setList((std::string)pMsg, true))
    {
      for(int iObj = 0; iObj < reader.getObjectCount(); iObj++)
        {
          reader.getNextObject(std::string(SETUP_PROGRESS_GET_REPLY_STR), object);
          info.parseMsg(object.c_str());
          LOGI("SetupProgressGetReply: %s\n", object.c_str());

          LOGI("\nRecieved setupProgressState: %s\n", info.getState().toString());
        }
    }
}

/** Callback to receive audio parameters reply message
 *
 * @param type is the type of the received message.
 * @param pMsg is a pointer to the buffer containing the message.
 * @param msgLen is the length of the message in bytes.
 */
void audioParametersReplyCallback(eOipcMessageType type, const char *pMsg, int msgLen)
{
  cJsonListReader reader;
  std::string object;
  cAudioParameters params(eAudioParametersCmd_t::GET);

  if(reader.setList((std::string)pMsg, true))
    {
      for(int iObj = 0; iObj < reader.getObjectCount(); iObj++)
        {
          reader.getNextObject(std::string(AUDIO_PARAMETERS_STR), object);
          params.parseMsg(object.c_str());
          LOGI("AudioParameters GET: %s\n", object.c_str());

          int minSpeakerLevel;
          bool minSpeakerLevelValid;
          int volume;
          bool volumeValid;
          bool mute;
          bool muteValid;
          int  noiseSuppressionLevel;
          bool noiseSuppressionLevelValid;
          bool enableDTD;
          bool enableDTDValid;
          char pMsg[2048];
          params.getArgs(volume, volumeValid,
                         mute, muteValid,
                         minSpeakerLevel, minSpeakerLevelValid,
                         noiseSuppressionLevel, noiseSuppressionLevelValid,
                         enableDTD, enableDTDValid);
          sprintf(pMsg, "\nReceived audioParameters: %s",
                  params.getCommand().toString());
          if (minSpeakerLevelValid)
            {
              sprintf(&pMsg[strlen(pMsg)], ", minSpeakerLevel = %d",
                      minSpeakerLevel);
            }
          if (noiseSuppressionLevelValid)
            {
              sprintf(&pMsg[strlen(pMsg)], ", noiseSuppressionLevel = %d",
                      noiseSuppressionLevel);
            }
          if (volumeValid)
            { sprintf(&pMsg[strlen(pMsg)], ", volume = %d", volume); }
          if (muteValid)
            {
              sprintf(&pMsg[strlen(pMsg)], ", mute = %s",
                      (mute) ? "true" : "false");
            }
          if (enableDTDValid)
            {
              sprintf(&pMsg[strlen(pMsg)], ", enableDTD = %s",
                      (enableDTD) ? "true" : "false");
            }
          sprintf(&pMsg[strlen(pMsg)], "\n");
          LOGI(pMsg);
        }
    }
}

/** Callback to receive video parameters reply message
 *
 * @param type is the type of the received message.
 * @param pMsg is a pointer to the buffer containing the message.
 * @param msgLen is the length of the message in bytes.
 */
void videoParametersReplyCallback(eOipcMessageType type, const char *pMsg, int msgLen)
{
  cJsonListReader reader;
  std::string object;
  cVideoParameters params(eVideoParametersCmd_t::GET);

  if(reader.setList((std::string)pMsg, true))
    {
      for(int iObj = 0; iObj < reader.getObjectCount(); iObj++)
        {
          reader.getNextObject(std::string(VIDEO_PARAMETERS_STR), object);
          params.parseMsg(object.c_str());
          LOGI("VideoParameters GET: %s\n", object.c_str());

          LOGI("\nRecieved videoParameters: %s, key = %s, value = %s\n",
               params.getCommand().toString(),
               params.getKey().c_str(),
               params.getValue().c_str());
        }
    }
}

/** Callback to receive connection level error notifications.
 *
 * @param type is the type of the received message.
 * @param pMsg is a pointer to the buffer containing the message.
 * @param msgLen is the length of the message in bytes.
 */
void errorCallback(eOipcMessageType type, const char *pcMsg, int msgLen)
{
  // If the type is CLIENT_VERSION_ERROR, then there's a major version
  // mismatch and no communications are allowed.
  LOGI("Error Message: Type: %d, Msg: %s\n", type, pcMsg);
}

/** Data used in debugInfo data transfers. */
static const std::string kDebugInfoFileName = "./debugOutInfo.txt";

/** Structure to maintain state during debugInfo transfers. */
typedef struct {
  uint32_t bytesReceived;
  FILE *pFile;
  cDebugInfo::transferHeader_t header;
} debugInfo_t;
debugInfo_t debugInfo;

/** Called when header received at the beginning of debugInfo data transfer.
 *
 * @param type is the type of the received message.
 * @param pMsg is a pointer to the buffer containing the message.
 * @param len is the length of the message in bytes.
 */
void debugInfoHeaderCallback(eOipcMessageType type, const char *pcMsg, int len)
{
  debugInfo.bytesReceived = 0;
  debugInfo.pFile = fopen(kDebugInfoFileName.c_str(), "wb");
  memcpy(&debugInfo.header, pcMsg,
         std::min(len, (int)sizeof(debugInfo.header)));
  LOGI("debugInfoHeaderCallback(): received message, len = %d\n", len);
  LOGI("debugInfoHeaderCallback(): Got debugInfo length %d\n",
       debugInfo.header.mDebugInfoLength);
}

/** Called for each chunk of debugInfo data received from Owl.
 *
 * @param type is the type of the received message.
 * @param pMsg is a pointer to the buffer containing the message.
 * @param len is the length of the message in bytes.
 */
void debugInfoChunkCallback(eOipcMessageType type, const char *pcMsg, int len)
{
  LOGI("debugInfoChunkCallback(): Got chunk with length %d\n", len);
  if (debugInfo.pFile == nullptr)
    {
      LOGW("debugInfoChunkCallback(): Chunk received, but file not open!\n");
      return;
    }
  fwrite(pcMsg, len, 1, debugInfo.pFile);
  debugInfo.bytesReceived += len;
  if (debugInfo.bytesReceived >= debugInfo.header.mDebugInfoLength)
    {
      fclose(debugInfo.pFile);
      debugInfo.pFile = nullptr;
      LOGI("***** Wrote debug output to file %s *****\n",
           kDebugInfoFileName.c_str());
    }
}

/** **************
 *  Menu functions
 *  The following functions define actions taken when menu items are
 *  selected.
 */

/** Pinned AOI Sub-Menu */
/** Helper function to send a message on the PINNED_AOI_CMD channel.
 *
 * @param msg is string to fill with the JSON message.
 * @param args is the structure containing the arguments to send.
 */
void sendPinnedAoiMsg(std::string &msg, cPinnedAoiArgs *args)
{
  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = true;

  success = args->buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    {
      success = msgList.addObject(std::string(oneMsg));
    }
  if (success)
    {
      msgList.getList(msg);
      LOGI("message: %s\n", msg.c_str());
      gpSwitchboardClient->writeMessage((eOipcMessageType)PINNED_AOI_CMD,
                                    (char *)msg.c_str(),
                                    msg.length() + 1 /*terminate string*/);
    }
}

/** Create a pinned AOI.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void pinnedAoiCreate(void *pContext)
{
  pinnedAoiContext_t *pC = (pinnedAoiContext_t *)pContext;

  if (!getInt("AOI id", 0, 4, pC->id, pC->id)) { return; }
  if (!getInt("Aoi width (degrees)", 5, 120, pC->size, pC->size)) { return; }
  if (!getInt("Aoi central angle (degrees)", 0, 360, pC->angle, pC->angle))
    { return; }
  cPinnedAoiArgs aoiArgsMsg(ePinnedAoiCmd_t::CREATE,
                            pC->id, pC->size, pC->angle);
  sendPinnedAoiMsg(pC->msg, &aoiArgsMsg);
}

/** Move the given AOI to a new location (can change size, too).
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void pinnedAoiMove(void *pContext)
{
  pinnedAoiContext_t *pC = (pinnedAoiContext_t *)pContext;

  if (!getInt("AOI id", 0, 4, pC->id, pC->id)) { return; }
  if (!getInt("Aoi width (degrees)", 5, 120, pC->size, pC->size)) { return; }
  if (!getInt("Aoi central angle (degrees)", 0, 360, pC->angle, pC->angle))
    { return; }
  cPinnedAoiArgs aoiArgsMsg(ePinnedAoiCmd_t::MOVE,
                            pC->id, pC->size, pC->angle);
  sendPinnedAoiMsg(pC->msg, &aoiArgsMsg);
}

/** Delete the given AOI.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void pinnedAoiDelete(void *pContext)
{
  pinnedAoiContext_t *pC = (pinnedAoiContext_t *)pContext;

  if (!getInt("AOI id", 0, 4, pC->id, pC->id)) { return; }
  cPinnedAoiArgs aoiArgsMsg(ePinnedAoiCmd_t::DELETE, pC->id);
  sendPinnedAoiMsg(pC->msg, &aoiArgsMsg);
}

/** Pin the given AOI.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void pinnedAoiPin(void *pContext)
{
  pinnedAoiContext_t *pC = (pinnedAoiContext_t *)pContext;

  if (!getInt("AOI id", 0, 4, pC->id, pC->id)) { return; }
  cPinnedAoiArgs aoiArgsMsg(ePinnedAoiCmd_t::PIN, pC->id);
  sendPinnedAoiMsg(pC->msg, &aoiArgsMsg);
}

/** Get the status of the pinned AOI with given ID.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void pinnedAoiGet(void *pContext)
{
  pinnedAoiContext_t *pC = (pinnedAoiContext_t *)pContext;

  if (!getInt("AOI id", 0, 4, pC->id, pC->id)) { return; }
  cPinnedAoiArgs aoiArgsMsg(ePinnedAoiCmd_t::GET, pC->id);
  sendPinnedAoiMsg(pC->msg, &aoiArgsMsg);
}

/** Delete all the pinned AOIs on the Owl.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void pinnedAoiDeleteAll(void *pContext)
{
  pinnedAoiContext_t *pC = (pinnedAoiContext_t *)pContext;

  cPinnedAoiArgs aoiArgsMsg(ePinnedAoiCmd_t::DELETE_ALL, pC->id);
  sendPinnedAoiMsg(pC->msg, &aoiArgsMsg);
}

/** Get status of all the AOI's currently known to the Owl.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void pinnedAoiGetAll(void *pContext)
{
  pinnedAoiContext_t *pC = (pinnedAoiContext_t *)pContext;

  cPinnedAoiArgs aoiArgsMsg(ePinnedAoiCmd_t::GET_ALL, pC->id);
  sendPinnedAoiMsg(pC->msg, &aoiArgsMsg);
}

/** Send a pinned AOI move command with auto-incrementing of variables.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void pinnedAoiRepeatMove(void *pContext)
{
  pinnedAoiContext_t *pC = (pinnedAoiContext_t *)pContext;

  // pC->id = getInt("AOI id", 0, 4, pC->id);
  if (!getInt("Aoi width increment (degrees)", 0, 45, pC->sizeIncrement,
              pC->sizeIncrement)) { return; }
  if (!getInt("Aoi central angle increment (degrees)", 0, 360,
              pC->angleIncrement, pC->angleIncrement)) { return; }
  pC->size += pC->sizeIncrement;
  pC->angle += pC->angleIncrement;
  cPinnedAoiArgs aoiArgsMsg(ePinnedAoiCmd_t::MOVE,
                            pC->id, pC->size, pC->angle);
  sendPinnedAoiMsg(pC->msg, &aoiArgsMsg);
}

/** Top Level Menu */
/** Reset the Owl's internal state and setup to factory-fresh.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void topMenuFactoryReset(void *pContext)
{
  char msg[1024];

  LOGI("Factory Reset!\n");
  msg[0] = 0;
  gpSwitchboardClient->writeMessage(
      (eOipcMessageType)FACTORY_RESET_CMD, msg, 0);
}

/** Start or stop fast or slow eye flashing on the Owl.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void topMenuPing(void *pContext)
{
  topMenuContext_t *pC = (topMenuContext_t *)pContext;
  menuEntry_t *pMenuItem = pC->pMenuItem;
  bool pingFast = (pMenuItem->cmdChar == 'P');

  if (pC->bPinging)
    {   // pinging now, so stop
      cUiAction pingStopCmd(uiCommand_t::PING_STOP);
      cJsonListWriter msgList;
      char oneMsg[1024];
      bool success = pingStopCmd.buildMsg(oneMsg, sizeof(oneMsg));
      if (success)
        { success = msgList.addObject(std::string(oneMsg)); }

      std::string msg;
      msgList.getList(msg);
      strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
      LOGI("message: %s\n", msg.c_str());
      gpSwitchboardClient->writeMessage(UI_ACTION_CMD,
                                        oneMsg,
                                        strlen(oneMsg) + 1 );
      pC->bPinging = false;
    }
  else
    {   // Start pinging
      cUiAction *pPingStartCmd;
      if (!pingFast)   // Ping slowly
        { pPingStartCmd = new cUiAction(uiCommand_t::PING_START_SLOW); }
      else  // cmdChar == 'P', Ping fast
        { pPingStartCmd = new cUiAction(uiCommand_t::PING_START_FAST); }
      cJsonListWriter msgList;
      char oneMsg[1024];
      bool success = pPingStartCmd->buildMsg(oneMsg, sizeof(oneMsg));
      if (success)
        { success = msgList.addObject(std::string(oneMsg)); }

      std::string msg;
      msgList.getList(msg);
      strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
      LOGI("message: %s\n", msg.c_str());
      gpSwitchboardClient->writeMessage(UI_ACTION_CMD,
                                        oneMsg,
                                        strlen(oneMsg) + 1 );
      pC->bPinging = true;
    }
}

/** Query the owl for meeting state.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void topMenuInMeeting(void *pContext)
{
  gpSwitchboardClient->writeMessage(MEETING_INFO_QUERY,
                                    (char*)"", 1);
}

/** Get or set the state of the App configuration state variable.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void topMenuAppCnfg(void *pContext)
{
  topMenuContext_t *pC = (topMenuContext_t *)pContext;
  menuEntry_t *pMenuItem = pC->pMenuItem;

  if (pMenuItem->cmdChar == 'N')
    {   // Get the App configuration progress state value
      char msg[16];
      LOGI("Get app setup progress state\n");
      msg[0] = 0;
      gpSwitchboardClient->writeMessage(
          (eOipcMessageType)SETUP_PROGRESS_GET, msg, 0);
    }
  else   // cmdChar must be 'n'
    {   // Set the App configuration progress state value
      int num = 0;
      if (!getInt("Enter a number for the state value", 0, 7, num, num))
        { return; }
      cSetupProgressMsgs setupProg(SETUP_PROGRESS_SET_STR,
                                   eSetupProgress_t(num));
      cJsonListWriter msgList;
      char oneMsg[1024];
      bool success = setupProg.buildMsg(oneMsg, sizeof(oneMsg));
      if (success)
        { success = msgList.addObject(std::string(oneMsg)); }

      std::string msg;
      msgList.getList(msg);
      strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
      LOGI("message: %s\n", msg.c_str());
      gpSwitchboardClient->writeMessage(SETUP_PROGRESS_SET,
                                        oneMsg,
                                        strlen(oneMsg) + 1 );
    }
}

/** Quits this program gracefully.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void topMenuQuit(void *pContext)
{
  LOGD("Terminating\n");
  gTerminate = true;
}

/** Debug Sub-Menu */
/** Turn on or off display of log messages generated by meetingOwl.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void debugMenuStreamDebug(void *pContext)
{
  debugMenuContext_t *pC = (debugMenuContext_t *)pContext;

  if (!getBool("Stream Owl debug messages", pC->bStreamDebug, pC->bStreamDebug))
    { return; }
}

/** Turn on or off display of messages that Owl receives on the switchboard.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void debugMenuStreamOwlMessages(void *pContext)
{
  debugMenuContext_t *pC = (debugMenuContext_t *)pContext;

  if (!getBool("Stream Owl's subscribed messages",
               pC->bOwlSubscribedMessageDebug, pC->bOwlSubscribedMessageDebug))
    { return; }
}

/** Force a core dump of the process to test failure recovery.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void debugMenuCoreDump(void *pContext)
{
  char *pcSegFault = (char *)0x00000000;
  *pcSegFault = 0;  // Seg Fault happens here!
}

/** Signal Owl to send logcat dump.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void debugMenuLogcatDump(void *pContext)
{
  static char pMsg[1024] = "l\0BLEService:V BarnClientService:V *:S";

  if (getString("Extra parameters", &pMsg[2], &pMsg[2], sizeof(pMsg) - 2) > 0)
    {
      int len = strlen(&pMsg[2]) + 2 + 1;   // +1 for the string terminator

      gpSwitchboardClient->writeMessage(DEBUG_INFO_CAPTURE_CMD,
                                        pMsg, len);
    }
}

/** Signal Owl to send dmesg dump.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void debugMenuDmesgDump(void *pContext)
{
  static char pMsg[1024] = "d\0\0\0";

  int len = strlen(&pMsg[2]) + 2 + 1;   // +1 for the string terminator

  gpSwitchboardClient->writeMessage(DEBUG_INFO_CAPTURE_CMD,
                                    pMsg, len);
}

/** Tell Owl to do a check-in with the Barn.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void debugMenuBarnCheckin(void *pContext)
{
  char pMsg[1024] = "\0";

  gpSwitchboardClient->writeMessage(DEBUG_INFO_TRIGGER_UPDATE,
                                    pMsg, 0);
}

/** Camera Parameters Sub-Menu */
/** Get the exposure compensation level for the camera.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void camParamsGetExp(void *pContext)
{
  cVideoParameters paramsMsg(eVideoParametersCmd_t::GET,
                             std::string("exposure-compensation"));
  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = paramsMsg.buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    { success = msgList.addObject(std::string(oneMsg)); }

  std::string msg;
  msgList.getList(msg);
  strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
  LOGI("message: %s\n", msg.c_str());
  gpSwitchboardClient->writeMessage(VIDEO_PARAMETERS_CMD,
                                    oneMsg,
                                    strlen(oneMsg) + 1 );
}

/** Set the exposure compensation level for the camera.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void camParamsSetExp(void *pContext)
{
  autoExpContext_t *pC = (autoExpContext_t *)pContext;
  if (!getInt("Exposure compensation", -12, 12, pC->expCompensation,
              pC->expCompensation)) { return; }
  std::stringstream expCompStr;
  expCompStr << pC->expCompensation;
  cVideoParameters paramsMsg(eVideoParametersCmd_t::SET,
                             std::string("exposure-compensation"),
                             expCompStr.str());
  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = paramsMsg.buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    { success = msgList.addObject(std::string(oneMsg)); }

  std::string msg;
  msgList.getList(msg);
  strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
  LOGI("message: %s\n", msg.c_str());
  gpSwitchboardClient->writeMessage(VIDEO_PARAMETERS_CMD,
                                    oneMsg,
                                    strlen(oneMsg) + 1 );
}

/** Report the current auto-exposure mode.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void autoExposureGet(void *pContext)
{
  cVideoParameters paramsMsg(eVideoParametersCmd_t::GET,
                             std::string("auto-exposure"));
  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = paramsMsg.buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    { success = msgList.addObject(std::string(oneMsg)); }

  std::string msg;
  msgList.getList(msg);
  strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
  LOGI("message: %s\n", msg.c_str());
  gpSwitchboardClient->writeMessage(VIDEO_PARAMETERS_CMD,
                                    oneMsg,
                                    strlen(oneMsg) + 1 );
}

/** Set the auto-exposure mode to center-weighted or frame-average.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void autoExposureSet(void *pContext)
{
  std::string autoExposureType;
  autoExpContext_t *pC = (autoExpContext_t *)pContext;

  if (pC->pMenuItem->cmdChar == 'c')
    { autoExposureType = std::string("center-weighted"); }
  else
    { autoExposureType = std::string("frame-average"); }

  cVideoParameters paramsMsg(eVideoParametersCmd_t::SET,
                             std::string("auto-exposure"),
                             autoExposureType.c_str());
  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = paramsMsg.buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    { success = msgList.addObject(std::string(oneMsg)); }

  std::string msg;
  msgList.getList(msg);
  strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
  LOGI("message: %s\n", msg.c_str());
  gpSwitchboardClient->writeMessage(VIDEO_PARAMETERS_CMD,
                                    oneMsg,
                                    strlen(oneMsg) + 1 );
}

/** Increment or decrement exposure compensation value.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void incExposure(void *pContext)
{
  autoExpContext_t *pC = (autoExpContext_t *)pContext;

  if (pC->pMenuItem->cmdChar == '+')
    { (pC->expCompensation)++; }
  else
    { (pC->expCompensation)--; }

  if (pC->expCompensation > 12)
    { pC->expCompensation = 12; }
  if (pC->expCompensation < -12)
    { pC->expCompensation = -12; }

  std::stringstream expCompStr;
  expCompStr << pC->expCompensation;
  cVideoParameters paramsMsg(eVideoParametersCmd_t::SET,
                             std::string("exposure-compensation"),
                             expCompStr.str());

  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = paramsMsg.buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    { success = msgList.addObject(std::string(oneMsg)); }

  std::string msg;
  msgList.getList(msg);
  strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
  LOGI("message: %s\n", msg.c_str());
  gpSwitchboardClient->writeMessage(VIDEO_PARAMETERS_CMD,
                                    oneMsg,
                                    strlen(oneMsg) + 1 );
}

/** Get the contrast level for the camera.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void camParamsGetContrast(void *pContext)
{
  cVideoParameters paramsMsg(eVideoParametersCmd_t::GET,
                             std::string("contrast"));
  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = paramsMsg.buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    { success = msgList.addObject(std::string(oneMsg)); }

  std::string msg;
  msgList.getList(msg);
  strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
  LOGI("message: %s\n", msg.c_str());
  gpSwitchboardClient->writeMessage(VIDEO_PARAMETERS_CMD,
                                    oneMsg,
                                    strlen(oneMsg) + 1 );
}

/** Set the contrast level for the camera.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void camParamsSetContrast(void *pContext)
{
  contrastContext_t *pC = (contrastContext_t *)pContext;
  if (!getInt("Contrast", 0, 10, pC->contrast, pC->contrast)) { return; }
  std::stringstream contrastStr;
  contrastStr << pC->contrast;
  cVideoParameters paramsMsg(eVideoParametersCmd_t::SET,
                             std::string("contrast"),
                             contrastStr.str());
  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = paramsMsg.buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    { success = msgList.addObject(std::string(oneMsg)); }

  std::string msg;
  msgList.getList(msg);
  strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
  LOGI("message: %s\n", msg.c_str());
  gpSwitchboardClient->writeMessage(VIDEO_PARAMETERS_CMD,
                                    oneMsg,
                                    strlen(oneMsg) + 1 );
}

/** Increment or decrement contrast value.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void incContrast(void *pContext)
{
  contrastContext_t *pC = (contrastContext_t *)pContext;

  if (pC->pMenuItem->cmdChar == '+')
    { (pC->contrast)++; }
  else
    { (pC->contrast)--; }

  if (pC->contrast > 10)
    { pC->contrast = 10; }
  if (pC->contrast < 0)
    { pC->contrast = 0; }

  std::stringstream contrastStr;
  contrastStr << pC->contrast;
  cVideoParameters paramsMsg(eVideoParametersCmd_t::SET,
                             std::string("contrast"),
                             contrastStr.str());

  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = paramsMsg.buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    { success = msgList.addObject(std::string(oneMsg)); }

  std::string msg;
  msgList.getList(msg);
  strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
  LOGI("message: %s\n", msg.c_str());
  gpSwitchboardClient->writeMessage(VIDEO_PARAMETERS_CMD,
                                    oneMsg,
                                    strlen(oneMsg) + 1 );
}

/** Get the saturation level for the camera.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void camParamsGetSaturation(void *pContext)
{
  cVideoParameters paramsMsg(eVideoParametersCmd_t::GET,
                             std::string("saturation"));
  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = paramsMsg.buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    { success = msgList.addObject(std::string(oneMsg)); }

  std::string msg;
  msgList.getList(msg);
  strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
  LOGI("message: %s\n", msg.c_str());
  gpSwitchboardClient->writeMessage(VIDEO_PARAMETERS_CMD,
                                    oneMsg,
                                    strlen(oneMsg) + 1 );
}

/** Set the contrast level for the camera.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void camParamsSetSaturation(void *pContext)
{
  saturationContext_t *pC = (saturationContext_t *)pContext;
  if (!getInt("Saturation", 0, 10, pC->saturation, pC->saturation)) { return; }
  std::stringstream saturationStr;
  saturationStr << pC->saturation;
  cVideoParameters paramsMsg(eVideoParametersCmd_t::SET,
                             std::string("saturation"),
                             saturationStr.str());
  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = paramsMsg.buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    { success = msgList.addObject(std::string(oneMsg)); }

  std::string msg;
  msgList.getList(msg);
  strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
  LOGI("message: %s\n", msg.c_str());
  gpSwitchboardClient->writeMessage(VIDEO_PARAMETERS_CMD,
                                    oneMsg,
                                    strlen(oneMsg) + 1 );
}

/** Increment or decrement saturation value.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void incSaturation(void *pContext)
{
  saturationContext_t *pC = (saturationContext_t *)pContext;

  if (pC->pMenuItem->cmdChar == '+')
    { (pC->saturation)++; }
  else
    { (pC->saturation)--; }

  if (pC->saturation > 10)
    { pC->saturation = 10; }
  if (pC->saturation < 0)
    { pC->saturation = 0; }

  std::stringstream saturationStr;
  saturationStr << pC->saturation;
  cVideoParameters paramsMsg(eVideoParametersCmd_t::SET,
                             std::string("saturation"),
                             saturationStr.str());

  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = paramsMsg.buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    { success = msgList.addObject(std::string(oneMsg)); }

  std::string msg;
  msgList.getList(msg);
  strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
  LOGI("message: %s\n", msg.c_str());
  gpSwitchboardClient->writeMessage(VIDEO_PARAMETERS_CMD,
                                    oneMsg,
                                    strlen(oneMsg) + 1 );
}

/** Get the sharpness level for the camera.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void camParamsGetSharpness(void *pContext)
{
  cVideoParameters paramsMsg(eVideoParametersCmd_t::GET,
                             std::string("sharpness"));
  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = paramsMsg.buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    { success = msgList.addObject(std::string(oneMsg)); }

  std::string msg;
  msgList.getList(msg);
  strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
  LOGI("message: %s\n", msg.c_str());
  gpSwitchboardClient->writeMessage(VIDEO_PARAMETERS_CMD,
                                    oneMsg,
                                    strlen(oneMsg) + 1 );
}

/** Set the sharpness level for the camera.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void camParamsSetSharpness(void *pContext)
{
  sharpnessContext_t *pC = (sharpnessContext_t *)pContext;
  // note that valid settings are multiples of 6
  if (!getInt("Sharpness", 0, 36, pC->sharpness, pC->sharpness)) { return; }
  pC->sharpness = pC->sharpness / 6;
  pC->sharpness = pC->sharpness * 6;
  std::stringstream sharpnessStr;
  sharpnessStr << pC->sharpness;
  cVideoParameters paramsMsg(eVideoParametersCmd_t::SET,
                             std::string("sharpness"),
                             sharpnessStr.str());
  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = paramsMsg.buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    { success = msgList.addObject(std::string(oneMsg)); }

  std::string msg;
  msgList.getList(msg);
  strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
  LOGI("message: %s\n", msg.c_str());
  gpSwitchboardClient->writeMessage(VIDEO_PARAMETERS_CMD,
                                    oneMsg,
                                    strlen(oneMsg) + 1 );
}

/** Increment or decrement sharpness value.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void incSharpness(void *pContext)
{
  sharpnessContext_t *pC = (sharpnessContext_t *)pContext;

  if (pC->pMenuItem->cmdChar == '+')
    { (pC->sharpness) += 6; }
  else
    { (pC->sharpness) -= 6; }

  if (pC->sharpness > 36)
    { pC->sharpness = 36; }
  if (pC->sharpness < 0)
    { pC->sharpness = 0; }

  std::stringstream sharpnessStr;
  sharpnessStr << pC->sharpness;
  cVideoParameters paramsMsg(eVideoParametersCmd_t::SET,
                             std::string("sharpness"),
                             sharpnessStr.str());

  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = paramsMsg.buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    { success = msgList.addObject(std::string(oneMsg)); }

  std::string msg;
  msgList.getList(msg);
  strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
  LOGI("message: %s\n", msg.c_str());
  gpSwitchboardClient->writeMessage(VIDEO_PARAMETERS_CMD,
                                    oneMsg,
                                    strlen(oneMsg) + 1 );
}

/** Get the preview frame rate for the camera.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void camParamsGetPFR(void *pContext)
{
  cVideoParameters paramsMsg(eVideoParametersCmd_t::GET,
                             std::string("preview-frame-rate"));
  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = paramsMsg.buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    { success = msgList.addObject(std::string(oneMsg)); }

  std::string msg;
  msgList.getList(msg);
  strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
  LOGI("message: %s\n", msg.c_str());
  gpSwitchboardClient->writeMessage(VIDEO_PARAMETERS_CMD,
                                    oneMsg,
                                    strlen(oneMsg) + 1 );
}

/** Set the preview frame rate for the camera.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void camParamsSetPFR(void *pContext)
{
  previewFrameRateContext_t *pC = (previewFrameRateContext_t *)pContext;
  if (!getInt("Preview frame rate", 8, 30, pC->rate, pC->rate)) { return; }
  std::stringstream rateStr;
  rateStr << pC->rate;
  cVideoParameters paramsMsg(eVideoParametersCmd_t::SET,
                             std::string("preview-frame-rate"),
                             rateStr.str());
  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = paramsMsg.buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    { success = msgList.addObject(std::string(oneMsg)); }

  std::string msg;
  msgList.getList(msg);
  strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
  LOGI("message: %s\n", msg.c_str());
  gpSwitchboardClient->writeMessage(VIDEO_PARAMETERS_CMD,
                                    oneMsg,
                                    strlen(oneMsg) + 1 );
}

/** Increment or decrement preview frame rate.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void incPFR(void *pContext)
{
  previewFrameRateContext_t *pC = (previewFrameRateContext_t *)pContext;

  if (pC->pMenuItem->cmdChar == '+')
    { (pC->rate)++; }
  else
    { (pC->rate)--; }

  if (pC->rate > 30)
    { pC->rate = 30; }
  if (pC->rate < 8)
    { pC->rate = 8; }

  std::stringstream rateStr;
  rateStr << pC->rate;
  cVideoParameters paramsMsg(eVideoParametersCmd_t::SET,
                             std::string("preview-frame-rate"),
                             rateStr.str());

  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = paramsMsg.buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    { success = msgList.addObject(std::string(oneMsg)); }

  std::string msg;
  msgList.getList(msg);
  strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
  LOGI("message: %s\n", msg.c_str());
  gpSwitchboardClient->writeMessage(VIDEO_PARAMETERS_CMD,
                                    oneMsg,
                                    strlen(oneMsg) + 1 );
}

/****** denoise control *******/
/** Get the denoise state for the camera.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void camParamsGetDenoise(void *pContext)
{
  cVideoParameters paramsMsg(eVideoParametersCmd_t::GET,
                             std::string("denoise"));
  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = paramsMsg.buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    { success = msgList.addObject(std::string(oneMsg)); }

  std::string msg;
  msgList.getList(msg);
  strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
  LOGI("message: %s\n", msg.c_str());
  gpSwitchboardClient->writeMessage(VIDEO_PARAMETERS_CMD,
                                    oneMsg,
                                    strlen(oneMsg) + 1 );
}

/** Set the denoise state for the camera.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void camParamsSetDenoise(void *pContext)
{
  denoiseContext_t *pC = (denoiseContext_t *)pContext;
  if (!getBool("Denoise", pC->denoise, pC->denoise)) { return; }
  std::string denoiseStr =
      std::string(((pC->denoise) ? "denoise-on" : "denoise-off"));
  cVideoParameters paramsMsg(eVideoParametersCmd_t::SET,
                             std::string("denoise"),
                             denoiseStr.c_str());
  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = paramsMsg.buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    { success = msgList.addObject(std::string(oneMsg)); }

  std::string msg;
  msgList.getList(msg);
  strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
  LOGI("message: %s\n", msg.c_str());
  gpSwitchboardClient->writeMessage(VIDEO_PARAMETERS_CMD,
                                    oneMsg,
                                    strlen(oneMsg) + 1 );
}

/** Get or set the minimum speaker level threshold value.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void audioMenuMinSpkrLevel(void *pContext)
{
  audioMenuContext_t *pC = (audioMenuContext_t *)pContext;
  menuEntry_t *pMenuItem = pC->pMenuItem;
  cAudioParameters *pAudioParams;


  if (pMenuItem->cmdChar == 's')
    {
      if (!getInt("Minimum speaker level noise threshold",
                                   -60, -25,
                                   pC->minSpeakerLevel, pC->minSpeakerLevel))
        { return; }
      pAudioParams = new cAudioParameters(eAudioParametersCmd_t::SET);
      pAudioParams->setMinSpeakerLevel(pC->minSpeakerLevel);
    }
  else
    {
      pAudioParams = new cAudioParameters(eAudioParametersCmd_t::GET);
    }
  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = pAudioParams->buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    { success = msgList.addObject(std::string(oneMsg)); }

  std::string msg;
  msgList.getList(msg);
  strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
  LOGI("message: %s\n", msg.c_str());
  gpSwitchboardClient->writeMessage(AUDIO_PARAMETERS_CMD,
                                    oneMsg,
                                    strlen(oneMsg) + 1 );
  delete pAudioParams;
}

/** Set audio noise reduction level
 *
 */
void audioMenuSetNRLevel(void *pContext)
{
  audioMenuContext_t *pC = (audioMenuContext_t *)pContext;
  menuEntry_t *pMenuItem = pC->pMenuItem;
  cAudioParameters *pAudioParams;

  if (pMenuItem->cmdChar == 'N')
    {
      if (!getInt("Noise reduction value", 0, 20, pC->NRLevel, pC->NRLevel))
        { return; }
      pAudioParams = new cAudioParameters(eAudioParametersCmd_t::SET);
      pAudioParams->setNoiseSuppressionLevel(pC->NRLevel);
    }
  else
    {
      pAudioParams = new cAudioParameters(eAudioParametersCmd_t::GET);
    }
  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = pAudioParams->buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    { success = msgList.addObject(std::string(oneMsg)); }

  std::string msg;
  msgList.getList(msg);
  strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
  LOGI("message: %s\n", msg.c_str());
  gpSwitchboardClient->writeMessage(AUDIO_PARAMETERS_CMD,
                                    oneMsg,
                                    strlen(oneMsg) + 1 );
  delete pAudioParams;
}

/** Set audio mute state
 *
 */
void audioMenuSetMute(void *pContext)
{
  audioMenuContext_t *pC = (audioMenuContext_t *)pContext;
  menuEntry_t *pMenuItem = pC->pMenuItem;
  cAudioParameters *pAudioParams;

  if (pMenuItem->cmdChar == 'M')
    {
      if (!getBool("Mute", pC->mute, pC->mute)) { return; }
      pAudioParams = new cAudioParameters(eAudioParametersCmd_t::SET);
      pAudioParams->setMute(pC->mute);
    }
  else
    {
      pAudioParams = new cAudioParameters(eAudioParametersCmd_t::GET);
    }
  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = pAudioParams->buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    { success = msgList.addObject(std::string(oneMsg)); }

  std::string msg;
  msgList.getList(msg);
  strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
  LOGI("message: %s\n", msg.c_str());
  gpSwitchboardClient->writeMessage(AUDIO_PARAMETERS_CMD,
                                    oneMsg,
                                    strlen(oneMsg) + 1 );
  delete pAudioParams;
}

/** Set audio enable DTD (double talk detection) state
 *
 */
void audioMenuSetEnableDTD(void *pContext)
{
  audioMenuContext_t *pC = (audioMenuContext_t *)pContext;
  menuEntry_t *pMenuItem = pC->pMenuItem;
  cAudioParameters *pAudioParams = NULL;

  if (pMenuItem->cmdChar == 'D')
    {
      if (!getBool("Enable double talk detection", pC->enableDTD,
                   pC->enableDTD))
        { return; }
      pAudioParams = new cAudioParameters(eAudioParametersCmd_t::SET);
      pAudioParams->setEnableDTD(pC->enableDTD);
    }
  else
    {
      pAudioParams = new cAudioParameters(eAudioParametersCmd_t::GET);
    }
  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = pAudioParams->buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    { success = msgList.addObject(std::string(oneMsg)); }

  std::string msg;
  msgList.getList(msg);
  strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
  LOGI("message: %s\n", msg.c_str());
  gpSwitchboardClient->writeMessage(AUDIO_PARAMETERS_CMD,
                                    oneMsg,
                                    strlen(oneMsg) + 1 );
  if (pAudioParams != NULL)
    { delete pAudioParams; }
}

/** Set audio volume level
 *
 */
void audioMenuSetVolume(void *pContext)
{
  audioMenuContext_t *pC = (audioMenuContext_t *)pContext;
  menuEntry_t *pMenuItem = pC->pMenuItem;
  cAudioParameters *pAudioParams;

  if (pMenuItem->cmdChar == 'V')
    {
      if (!getInt("Volume", 0, 100, pC->volume, pC->volume)) { return; }
      pAudioParams = new cAudioParameters(eAudioParametersCmd_t::SET);
      pAudioParams->setVolume(pC->volume);
    }
  else
    {
      pAudioParams = new cAudioParameters(eAudioParametersCmd_t::GET);
    }
  cJsonListWriter msgList;
  char oneMsg[1024];
  bool success = pAudioParams->buildMsg(oneMsg, sizeof(oneMsg));
  if (success)
    { success = msgList.addObject(std::string(oneMsg)); }

  std::string msg;
  msgList.getList(msg);
  strncpy(oneMsg, msg.c_str(), sizeof(oneMsg));
  LOGI("message: %s\n", msg.c_str());
  gpSwitchboardClient->writeMessage(AUDIO_PARAMETERS_CMD,
                                    oneMsg,
                                    strlen(oneMsg) + 1 );
  delete pAudioParams;
}

/** Load Test Sub-Menu */
/* Start/stop a load test thread that spews messages.
 *
 * @param start if true means start a test, otherwise stop.
 */
void executeLoadTest(bool start)
{
  static cThread *pLoadTest = nullptr;

  if ((pLoadTest == nullptr) && start)
    {
      LOGI("Start load test (messages flowing)\n");
      loadTestContext.messageCount = 0;
      pLoadTest = new cThread(std::bind(&loadTestFunc), "loadTest");
      currentTimeSpec(&loadTestContext.startTime);
      pLoadTest->start();
    }
  else
    {
      pLoadTest->signalStop();
      pLoadTest->join();
      delete pLoadTest;
      pLoadTest = nullptr;
      LOGI("\n\n*******\nFinish load test, received %d messages %lf per second.\n*******\n\n",
           loadTestContext.messageCount,
           loadTestContext.messageCount /
               elapsedTimeSinceSec(loadTestContext.startTime));
    }
}

/** Run a load test with a 'move' pinned AOI command.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void loadTestAoi(void *pContext)
{
  loadTestContext_t *pC = (loadTestContext_t *)pContext;

  if (!getInt("Microseconds between messages", 0, 1000000,
              pC->uSecMessageDelay, pC->uSecMessageDelay))
    { return; }

  // create a pinned AOI
  cPinnedAoiArgs aoiArgsMsg(ePinnedAoiCmd_t::CREATE,
                            0, 30, 0);
  sendPinnedAoiMsg(pC->msg, &aoiArgsMsg);
  usleep(500000); // wait for response so next message has proper id

  // create a message to move the AOI
  cPinnedAoiArgs aoiArgsMsgM(ePinnedAoiCmd_t::MOVE,
                            pinnedAoiContext.id, 30, 180);
  sendPinnedAoiMsg(pC->msg, &aoiArgsMsgM);
  pC->msgType = PINNED_AOI_CMD;
  LOGI("loadTestAoi(): msg='%s'\n", pC->msg.c_str());
  usleep(500000); // wait for response so message counting is right

  // run the load test with that move message
  executeLoadTest(true);
}

/** Terminates a running load test operation.
 *
 * @param pContext is a pointer to the menu context for this command.
 */
void stopLoadTest(void *pContext)
{
  loadTestContext_t *pC = (loadTestContext_t *)pContext;

  executeLoadTest(false);

  // create a message to delete all AOIs
  cPinnedAoiArgs aoiArgsMsgM(ePinnedAoiCmd_t::DELETE_ALL,
                            pinnedAoiContext.id, 30, 180);
  sendPinnedAoiMsg(pC->msg, &aoiArgsMsgM);
}


/** ****************
 *  Menu definitions
 */

/** Pinned AOI Sub-Menu */
static menuEntry_t pinnedAoiMenu[] = {
    {'c', "Create", (void *)&pinnedAoiContext, pinnedAoiCreate, true},
    {'m', "Move", (void *)&pinnedAoiContext, pinnedAoiMove, true},
    {'d', "Delete", (void *)&pinnedAoiContext, pinnedAoiDelete, true},
    {'p', "Pin", (void *)&pinnedAoiContext, pinnedAoiPin, true},
    {'g', "Get", (void *)&pinnedAoiContext, pinnedAoiGet, true},
    {'D', "Delete all", (void *)&pinnedAoiContext, pinnedAoiDeleteAll, true},
    {'G', "Get all", (void *)&pinnedAoiContext, pinnedAoiGetAll, true},
    {'r', "Repeat Move", (void *)&pinnedAoiContext, pinnedAoiRepeatMove, true},
    {  0, NULL, NULL, NULL, true},
};
/** Debugging Sub-Menu */
static menuEntry_t debugMenu[] = {
    {'d', "Stream Owl Debug messages", (void *)&debugMenuContext, debugMenuStreamDebug, true},
    {'s', "Stream Owl's Subscribed messages", (void *)&debugMenuContext, debugMenuStreamOwlMessages, true},
    {'c', "Force core dump", (void *)&debugMenuContext, debugMenuCoreDump, true},
    {'l', "Logcat dump", (void *)&debugMenuContext, debugMenuLogcatDump, true},
    {'m', "dMesg dump", (void *)&debugMenuContext, debugMenuDmesgDump, true},
    {'b', "Barn check-in", (void *)&debugMenuContext, debugMenuBarnCheckin, true},
    {  0, NULL, NULL, NULL, true},
};
/** Auto exposure Sub-Menu */
static menuEntry_t autoExposureMenu[] = {
    {'g', "Get auto-exposure setting", (void *)&autoExpContext, autoExposureGet, true},
    {'c', "center-weighted", (void *)&autoExpContext, autoExposureSet, true},
    {'f', "frame-average", (void *)&autoExpContext, autoExposureSet, true},
    {'e', "Get Exposure compensation", (void *)&camParamsContext, camParamsGetExp, true},
    {'E', "Set Exposure compensation", (void *)&camParamsContext, camParamsSetExp, true},
    {'+', "Increment exposure compensation", (void *)&camParamsContext, incExposure, true},
    {'-', "Decrement exposure compensation", (void *)&camParamsContext, incExposure, true},
    {  0, NULL, NULL, NULL, true},
};
/** Contrast Sub-Menu */
static menuEntry_t contrastMenu[] = {
    {'c', "Get Contrast", (void *)&contrastContext, camParamsGetContrast, true},
    {'C', "Set Contrast", (void *)&contrastContext, camParamsSetContrast, true},
    {'+', "Increment contrast", (void *)&contrastContext, incContrast, true},
    {'-', "Decrement contrast", (void *)&contrastContext, incContrast, true},
    {  0, NULL, NULL, NULL, true},
};
/** Saturation Sub-Menu */
static menuEntry_t saturationMenu[] = {
    {'s', "Get Saturation", (void *)&saturationContext, camParamsGetSaturation, true},
    {'S', "Set Saturation", (void *)&saturationContext, camParamsSetSaturation, true},
    {'+', "Increment saturation", (void *)&saturationContext, incSaturation, true},
    {'-', "Decrement saturation", (void *)&saturationContext, incSaturation, true},
    {  0, NULL, NULL, NULL, true},
};
/** Sharpness Sub-Menu */
static menuEntry_t sharpnessMenu[] = {
    {'s', "Get Sharpness", (void *)&sharpnessContext, camParamsGetSharpness, true},
    {'S', "Set Sharpness", (void *)&sharpnessContext, camParamsSetSharpness, true},
    {'+', "Increment sharpness", (void *)&sharpnessContext, incSharpness, true},
    {'-', "Decrement sharpness", (void *)&sharpnessContext, incSharpness, true},
    {  0, NULL, NULL, NULL, true},
};
/** Preview frame rate Sub-Menu */
static menuEntry_t previewFrameRateMenu[] = {
    {'p', "Get Preview frame rate", (void *)&previewFrameRateContext, camParamsGetPFR, true},
    {'P', "Set Preview frame rate", (void *)&previewFrameRateContext, camParamsSetPFR, true},
    {'+', "Increment preview frame rate", (void *)&previewFrameRateContext, incPFR, true},
    {'-', "Decrement preview frame rate", (void *)&previewFrameRateContext, incPFR, true},
    {  0, NULL, NULL, NULL, true},
};
/** Denoise state Sub-Menu */
static menuEntry_t denoiseMenu[] = {
    {'d', "Get Denoise State", (void *)&denoiseContext, camParamsGetDenoise, true},
    {'D', "Set Denoise State", (void *)&denoiseContext, camParamsSetDenoise, true},
    {  0, NULL, NULL, NULL, true},
};
/** Camera Parameters Sub-Menu */
static menuEntry_t camParamsMenu[] = {
    {'e', "Exposure menu", (void *)&camParamsContext, {.pSubMenu = autoExposureMenu}, false},
    {'c', "Contrast menu", (void *)&camParamsContext, {.pSubMenu = contrastMenu}, false},
    {'s', "Saturation menu", (void *)&camParamsContext, {.pSubMenu = saturationMenu}, false},
    {'r', "shaRpness menu", (void *)&camParamsContext, {.pSubMenu = sharpnessMenu}, false},
    {'p', "Preview frame rate menu", (void *)&camParamsContext, {.pSubMenu = previewFrameRateMenu}, false},
    {'d', "Denoise state menu", (void *)&camParamsContext, {.pSubMenu = denoiseMenu}, false},
    {  0, NULL, NULL, NULL, true},
};
/** Load test Sub-Menu */
static menuEntry_t loadTestMenu[] = {
    {'a', "Select AOI load test", (void *)&loadTestContext, loadTestAoi, true},
    //{'c', "Select App Configuration load test", (void *)&loadTestContext, loadTestConf, true},
    {'s', "Stop load test", (void *)&loadTestContext, stopLoadTest, true},
    {  0, NULL, NULL, NULL, true},
};
/** Audio Parameters Sub-Menu */
static menuEntry_t audioParamsMenu[] = {
    {'s', "Set minSpeakerLevel", (void *)&audioMenuContext, audioMenuMinSpkrLevel, true},
    {'S', "Get minSpeakerLevel", (void *)&audioMenuContext, audioMenuMinSpkrLevel, true},
    {'n', "Set Noise reduction level", (void *)&audioMenuContext, audioMenuSetNRLevel, true},
    {'N', "Get Noise reduction level", (void *)&audioMenuContext, audioMenuSetNRLevel, true},
    {'m', "Get Mute State", (void *)&audioMenuContext, audioMenuSetMute, true},
    {'M', "Set Mute state", (void *)&audioMenuContext, audioMenuSetMute, true},
    {'v', "Get Volume", (void *)&audioMenuContext, audioMenuSetVolume, true},
    {'V', "Set Volume", (void *)&audioMenuContext, audioMenuSetVolume, true},
    {'d', "Get EnableDTD", (void *)&audioMenuContext, audioMenuSetEnableDTD, true},
    {'D', "Set EnableDTD", (void *)&audioMenuContext, audioMenuSetEnableDTD, true},
    {  0, NULL, NULL, NULL, true},
};
/** Top level menu definition */
static menuEntry_t topMenu[] = {
    {'f', "Factory reset", (void *)&topMenuContext, topMenuFactoryReset, true},
    {'p', "Ping owl eyes (slowly) toggle on/off", (void *)&topMenuContext, topMenuPing, true},
    {'P', "Ping owl eyes (fast) toggle on/off", (void *) &topMenuContext, topMenuPing, true},
    {'n', "Set App coNfiguration state", (void *)&topMenuContext, topMenuAppCnfg, true},
    {'N', "Get App coNfiguration state", (void *)&topMenuContext, topMenuAppCnfg, true},
    {'i', "Get Owl in-meeting state", (void *)&topMenuContext, topMenuInMeeting, true},
    {'l', "Camera Lock Menu", (void *)&topMenuContext, {.pSubMenu = pinnedAoiMenu}, false},
    {'c', "Camera parameters Menu", (void *)&topMenuContext, {.pSubMenu = camParamsMenu}, false},
    {'a', "Audio parameters Menu", (void *)&topMenuContext, {.pSubMenu = audioParamsMenu}, false},
    {'t', "Load Test Menu", (void *)&topMenuContext, {.pSubMenu = loadTestMenu}, false},
    {'d', "Debug Menu", (void *)&debugMenuContext, {.pSubMenu = debugMenu}, false},
    {'q', "Quit", (void *)&topMenuContext, topMenuQuit, true},
    {  0, NULL, NULL, NULL, true},
};

#define MAX_MENU_DEPTH   4

/** Structure for maintaining the menu stack */
typedef struct menuStack_s {
  menuEntry_t *stack[MAX_MENU_DEPTH]; /**< Stack of menu calls */
  int depth;                          /**< Current depth of stack */
} menuStack_t;


/** Prints the menu contents to prompt the user for an action.
 *
 * @param pMenu is a pointer to the menu structure defining the current menu.
 */
void printMenu(menuEntry_t *pMenu)
{
  bool isTopMenu = (pMenu == topMenu);

  fprintf(stderr, "Menu:\n");
  while (pMenu->cmdChar != 0)
    {
      fprintf(stderr, "  %c - %s\n", pMenu->cmdChar, pMenu->pPrompt);
      pMenu++;
    }
  if (!isTopMenu)
    { fprintf(stderr, "  <Enter> - Pop one menu level up\n"); }
  fprintf(stderr, "Choose an option : ");
}

/** Provides command menu functionality including prompting and execution.
 *
 * The pMenuStack must be initialized with one item which is the root
 * menu of the menu system whose state machine this function 'ticks' each
 * time it is called.
 *
 * @param pMenuStack is the stack of menus traversed to reach the current one.
 */
void menuHandler(menuStack_t *pMenuStack)
{
  char cmdChar = kbWaitForChar(5000);
  menuEntry_t *pActiveMenu;
  bool bCmdFound = false;

  // handle the cmdChar if one was received
  if (cmdChar == '\n')
    {     // pop a menu on <Enter>
      if (pMenuStack->depth > 0)
        {
          pMenuStack->stack[pMenuStack->depth] = NULL;
          (pMenuStack->depth)--;
          fprintf(stderr,
                  "Pop menu\nChoose an option: ");
        }
    }
  else if ((cmdChar == 'h') || (cmdChar == 'H') || (cmdChar == '?'))
    {   // Print out the menu
      pActiveMenu = pMenuStack->stack[pMenuStack->depth];
      fprintf(stderr, "%c\n", cmdChar);
      printMenu(pActiveMenu);
    }
  else if (cmdChar != 0)
    {   // Got some kind of command input
      pActiveMenu = pMenuStack->stack[pMenuStack->depth];
      while (pActiveMenu->cmdChar != 0)
        {
          if (pActiveMenu->cmdChar == cmdChar)
            {   // match! execute this menu entry
              fprintf(stderr, "%c\n", cmdChar);
              if (pActiveMenu->bLeaf)
                {   // runnable command, so run it
                  ((genericMenuContext_t *)pActiveMenu->pMenuContext)->pMenuItem =
                      pActiveMenu;
                  (*pActiveMenu->ptr.pFunc)(pActiveMenu->pMenuContext);
                }
              else
                {   // Sub-menu, so make it active
                  (pMenuStack->depth)++;
                  pMenuStack->stack[pMenuStack->depth] = pActiveMenu->ptr.pSubMenu;
                }
              bCmdFound = true;
              fprintf(stderr, "Choose an option: ");
              break;
            }
          pActiveMenu++;
        }
      if (!bCmdFound)
        {   // Unknown command - be helpful and print out the active menu
          fprintf (stderr, "\nUnknown command %c\n\n", cmdChar);
          printMenu(pMenuStack->stack[pMenuStack->depth]);
        }
    }
}

/** Receives debugging command characters and takes action accordingly.
 *
 * This function replaces a simple sleep() in the main loop and provides
 * a means for inserting artificial events for debug and test. When a
 * character is received on stdin, it causes kbWaitForChar() to return
 * immediately. This function can then take action based on the character
 * value. The action runs in the context of the main application thread.
 */
void handleDebugCommands(int timeoutUsec)
{
  static bool currentlyPinging = false;

  if (gDoServer)
    {
      // Note for the future: if more server side run time options are added,
      // then create menus and use the menuHandler().
      switch (kbWaitForChar(5000))
      {
        case 'h':
        case 'H':
        case '?':
          printf("Debugging commands:\n");
          break;
        case 'q':
        case 'Q':
          LOGD("Terminating\n");
          gTerminate = true;
          break;
        case 'c':
        case 'C':
          {
            char *pcSegFault = (char *)0x00000000;
            *pcSegFault = 0;  // Cause segmentation fault!
          }
          break;
        default:
          break;
      }
    }
  else if (gDoClient)
    {
      static menuStack_t clientMenuStack = {{topMenu}, 0}; // tracks current menu

      // tick the hierarchical menu state machine
      menuHandler(&clientMenuStack);
    }
 return;
}

/** main entry point for the switchboardTest program.
 *
 */
int  main(int argc, char **argv)
{
  int ret;
  sigset_t set;
  pthread_t signalThread;
  struct rlimit core_limit;
  core_limit.rlim_cur = RLIM_INFINITY;
  core_limit.rlim_max = RLIM_INFINITY;

  // Configure the application to dump core on fatal errors
  if (setrlimit(RLIMIT_CORE, &core_limit) < 0)
      fprintf(stderr, "setrlimit: %s\nWarning: core dumps may be truncated or non-existant\n", strerror(errno));
#ifdef __ANDROID__
  system("echo \"/data/OwlLabs/core_%e\" > /proc/sys/kernel/core_pattern");
#endif

#ifdef __ANDROID__ // Android --------------------------------------------------
  // set signals to be caught
  sigemptyset(&set);
  sigaddset(&set, SIGQUIT);
  sigaddset(&set, SIGTERM);
  sigaddset(&set, SIGINT);
  sigaddset(&set, SIGABRT);
  sigaddset(&set, SIGFPE);
  // start a thread for catching terminate events
  CHECK(pthread_sigmask(SIG_BLOCK, &set, NULL));
  CHECK(pthread_create(&signalThread, NULL, &signalHandler, (void *) &set));
  pthread_setname_np(signalThread, "signalHandler");
#else // Linux -----------------------------------------------------------------
  // set up signal handler
  struct sigaction signalAction;
  signalAction.sa_handler = signalHandler;
  sigaction(SIGINT, &signalAction, 0);
#endif // Android / Linux ------------------------------------------------------

  std::string testPath = "";

  // handle command-line arguments
  for(int i = 1; i < argc; i++)
    {
      if(argv[i][0] == '-')
	{
	  switch(argv[i][1])
	    {
#if defined(SIMULATE_PCM_DATA) || (defined(__ANDROID__) && !defined(MOCK_INPUT))
	      // arguments pertaining to real input or simulated PCM input
#else // linux or MOCK_INPUT
	      // arguments pertaining to mock input
	    case 't': // set path of the test directory
	      testPath = argv[++i];
	      // remove any trailing slashes
	      while(testPath.back() == '/')
		{ testPath.pop_back(); }
	      break;
#endif // linux or MOCK_INPUT
	    case 'c':
	    case 'C':
	      gDoClient = true;
	      gDoServer = false;
	      if (argc < 3)
	        {
	          strncpy(gIpAddr, "127.0.0.1", sizeof(gIpAddr));
                  LOGI("Client using loopback IP: %s\n", gIpAddr);
	        }
	      else
	        {
	          strncpy(gIpAddr, argv[2], sizeof(gIpAddr));
	          LOGI("Client using IP: %s\n", gIpAddr);
	        }
	      break;
	    case 's':
	    case 'S':
	      gDoClient = false;
	      gDoServer = true;
	      break;
	    case 'h': // show help
	    default:
	      usage();
	      return 0;
	    }
	}
    }

  init();

  while(!gTerminate) { handleDebugCommands(kMainLoopSleepTime); }

  finish();

  // reset console buffering
  stdinUnbuffered(0);

  LOGV("Say good night, switchboardTest... Good night.\n");

  return 0;
}

/** Perform platform-specific initialization.
 *
 */
int  init(void)
{
  if (gDoServer)
    {
      gpSwitchboardServer = new cSwitchboardServer();
      // gpSwitchboardServer->init();
    }
  else if (gDoClient)
    {
      gpSwitchboardClient = new cSwitchboardClient(std::string(gIpAddr),
                                                   errorCallback);
      // Subscribe to the debug log message type
      gpSwitchboardClient->subscribe(eOipcMessageType::MEETING_OWL_DEBUG_MSG,
                                     showLOGCallback);
      // Subscribe to pinned AOI reply messages
      gpSwitchboardClient->subscribe(eOipcMessageType::PINNED_AOI_REPLY,
                                     pinnedAoiReplyCallback);
      // Subscribe to meeting info notification messages
      gpSwitchboardClient->subscribe(eOipcMessageType::MEETING_INFO_NOTIFY,
                                     meetingInfoCallback);
      // Subscribe to setup progress get reply messages
      gpSwitchboardClient->subscribe(eOipcMessageType::SETUP_PROGRESS_GET_REPLY,
                                     appConfiguredStateGetCallback);
      gpSwitchboardClient->subscribe(eOipcMessageType::AUDIO_PARAMETERS_REPLY,
                                     audioParametersReplyCallback);
      gpSwitchboardClient->subscribe(eOipcMessageType::VIDEO_PARAMETERS_REPLY,
                                     videoParametersReplyCallback);

      // switchboard channels that meetingOwl subscribes to
      gpSwitchboardClient->subscribe(eOipcMessageType::MEETING_OWL_DEBUG_CMD,
                                     showMessageOnlyCallback);
      gpSwitchboardClient->subscribe(eOipcMessageType::PINNED_AOI_CMD,
                                     showMessageOnlyCallback);
      gpSwitchboardClient->subscribe(eOipcMessageType::UI_ACTION_CMD,
                                     showMessageOnlyCallback);
      gpSwitchboardClient->subscribe(eOipcMessageType::MEETING_INFO_QUERY,
                                     showMessageOnlyCallback);
      gpSwitchboardClient->subscribe(eOipcMessageType::SETUP_PROGRESS_SET,
                                     showMessageOnlyCallback);
      gpSwitchboardClient->subscribe(eOipcMessageType::SETUP_PROGRESS_GET,
                                     showMessageOnlyCallback);
      gpSwitchboardClient->subscribe(eOipcMessageType::FACTORY_RESET_CMD,
                                     showMessageOnlyCallback);
      gpSwitchboardClient->subscribe(eOipcMessageType::AUDIO_PARAMETERS_CMD,
                                     showMessageOnlyCallback);
      gpSwitchboardClient->subscribe(eOipcMessageType::VIDEO_PARAMETERS_CMD,
                                     showMessageOnlyCallback);
      gpSwitchboardClient->subscribe(eOipcMessageType::DEBUG_INFO_HEADER_MSG,
                                     debugInfoHeaderCallback);
      gpSwitchboardClient->subscribe(eOipcMessageType::DEBUG_INFO_CHUNK_MSG,
                                     debugInfoChunkCallback);
    }

  return 0; // success
}

/** Clean up when terminating the program.
 *
 */
int  finish(void)
{
  if (gDoServer)
    { delete gpSwitchboardServer; }
  else if (gDoClient)
    { delete gpSwitchboardClient; }

  return 0; // success
}

