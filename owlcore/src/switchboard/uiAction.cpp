/*
 * uiAction.cpp
 *
 *  Created on: Mar 30, 2017
 *
 *  Class defining action commands for Owl hardware UI (LEDs/buttons).
 */

#include <string.h>

#include "logging.hpp"
#include "rapidjson/document.h"
#include "rapidjson/pointer.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "switchboard/uiAction.hpp"
#include "switchboard/switchboardMessages.hpp"

/** Template for creating UiAction messages. */
const char cUiAction::pcUiActionDOM[] =
  "{\"" UI_ACTION_ARGS_STR "\":"           \
  "{\"" UI_ACTION_CMD_STR "\":\"\"}}";

const int cUiAction::mkSlowPingBlinkOnTimeMs = 800;
const int cUiAction::mkSlowPingBlinkOffTimeMs = 500;
const int cUiAction::mkFastPingBlinkOnTimeMs = 340;
const int cUiAction::mkFastPingBlinkOffTimeMs = 270;

/*** cUiAction ***/
using namespace rapidjson;

/** Create an object with default values. */
cUiAction::cUiAction()
  : mCmd(uiCommand_t::NONE), mConsumed(false)
{ }

cUiAction::cUiAction(uiCommand_t cmd)
  : mCmd(cmd), mConsumed(false)
{ }

/** Destroy an object. */
cUiAction::~cUiAction()
{ }

/** Get the command. */
uiCommand_t cUiAction::getCommand(void)
{
  return mCmd;
}

/** Parse the given JSON message into the fields of this args class.
 *
 * @param pcMsg is the JSON message string to parse.
 *
 * @return true if the JSON message is well formed and parsed.
 */
bool cUiAction::parseMsg(const char *pcMsg)
{
  bool parsed = false;
  Document doc;
  doc.Parse(pcMsg);
  bool isUiMsg = doc.HasMember(UI_ACTION_ARGS_STR);
  Value *cmdPtr = Pointer(UI_ACTION_CMD_PTR).Get(doc);

  if(isUiMsg && cmdPtr)
    {
      IF_VERIFY_STRING(cmdPtr)
        {
          const char *cmdStr = cmdPtr->GetString();
          mCmd = uiCommand_t::fromString(cmdStr);
          mConsumed = false;
          parsed = true;
          LOGD("cUiAction::parseMsg: cmd = '%s'\n", cmdStr);
        }
    }
  else if(isUiMsg)
    {
      LOGW("cUiAction::parseMsg: Bad Ui message \"%s\"\n", pcMsg);
    }
  else
    {
      LOGW("cUiAction::parseMsg: Not a Ui message \"%s\"\n", pcMsg);
    }

  return parsed;
}

/** Construct a JSON message in the supplied buffer for these args.
 *
 * @param pMsgBuf is the buffer to fill with the JSON message.
 * @param bufLen is the maximum capacity of the buffer.
 *
 * @return true if the message fits in the provided buffer.
 */
bool cUiAction::buildMsg(char *pMsgBuf, int bufLen)
{
  bool retVal = false;

  // Create document and load our document model
  Document doc;
  doc.Parse(pcUiActionDOM);
  Value *cmdPtr = Pointer(UI_ACTION_CMD_PTR).Get(doc);

  // Verify existence of the 'cmd' member under the 'UiComand' member
  if(cmdPtr)
    { // Exists, so set it
      cmdPtr->SetString(mCmd.toString(), doc.GetAllocator());
    }

  // Create stream writer to access doc as a string
  StringBuffer buf;
  Writer<StringBuffer> writer(buf);
  doc.Accept(writer);
  const char *uiMsg = buf.GetString();

  // As long as it will fit, copy the JSON into the supplied buffer
  if (strlen(uiMsg) <= bufLen)
    {
      strncpy(pMsgBuf, uiMsg, bufLen);
      retVal = true;
    }

  return retVal;
}

/** Return the argument parameters contained in this class.
 *
 * @param cmdArg is the command contained in the message.
 */
bool cUiAction::getArgs(uiCommand_t &cmdArg)
{
  bool previouslyConsumed = mConsumed;

  cmdArg = mCmd;
  mConsumed = true;

  return previouslyConsumed;
}

/** Return indication whether this instance's arguments have already been used.
 *
 * @return true if this object's arguments have been consumed via getArgs().
 */
bool cUiAction::argsConsumed()
{
  return mConsumed;
}

/** Mark arguments as already consumed effectively throwing them away. */
void cUiAction::abortArgs(void)
{
  mConsumed = true;
}
