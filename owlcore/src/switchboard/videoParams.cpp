/*
 * videoParams.cpp
 *
 * Class that defines JSON messages for exchanging video
 * parameters over a Switchboard connection.
 *
 * Copyright (c) Owl Labs Inc. 2017.
 * All rights reserved.
 *
 * Created on: Aug 14, 2017
 */

#include "logging.hpp"
#include "rapidjson/document.h"
#include "rapidjson/pointer.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "switchboard/videoParams.hpp"
#include "switchboard/switchboardMessages.hpp"


// #define DEBUG_AUDIO_PARAMS_MSGS
#ifdef  DEBUG_AUDIO_PARAMS_MSGS
// Enable module debugging messages
#define LOGID LOGI
#else
// Disable module debugging messages
#define LOGID(...) {}
#endif


/** Templates for creating videoParameters messages. */
const char pcVideoParamsDOM[] =
    "{\"" VIDEO_PARAMETERS_STR "\":"               \
        "{\"" VIDEO_PARAMETERS_CMD_STR "\":\"\","  \
         "\"" VIDEO_PARAMETERS_KEY_STR "\":\"\","  \
         "\"" VIDEO_PARAMETERS_VALUE_STR "\":\"\"" \
        "}"                                        \
    "}";


using namespace rapidjson;

/** Construct a default audio parameters object.
 *
 * This object is used for generating or unpacking JSON audio parameter
 * messages.
 */
cVideoParameters::cVideoParameters()
  : mCmd(eVideoParametersCmd_t::NONE),
    mKey(""),
    mValue("")
{
}

/** Construct an audio parameters object.
 *
 * This object is used for generating or unpacking JSON audio parameter
 * messages.
 *
 * @param cmd is the command field initializer.
 */
cVideoParameters::cVideoParameters(eVideoParametersCmd_t cmd)
  : mCmd(cmd),
    mKey(""),
    mValue("")
{
}

/** Construct an video parameters object.
 *
 * This object is used for generating or unpacking JSON video parameter
 * messages.
 *
 * @param cmd is the command field initializer.
 * @param key is the key to associate with these parameters.
 * @param value is the value to associate with these parameters
 */
cVideoParameters::cVideoParameters(eVideoParametersCmd_t cmd,
                                   std::string key)
  : mCmd(cmd),
    mKey(key),
    mValue("")
{
}

/** Construct an video parameters object.
 *
 * This object is used for generating or unpacking JSON video parameter
 * messages.
 *
 * @param cmd is the command field initializer.
 * @param key is the key to associate with these parameters.
 * @param value is the value to associate with these parameters
 */
cVideoParameters::cVideoParameters(eVideoParametersCmd_t cmd,
                                   std::string key, std::string value)
  : mCmd(cmd),
    mKey(key),
    mValue(value)
{
}

/** Destroy a video parameters object.
 *
 */
cVideoParameters::~cVideoParameters(void)
{
}

/** Return the argument parameters contained in this class.
 *
 * @param state is the state value contained in the message.
 *
 * @return true if the value is valid, false otherwise.
 */
bool cVideoParameters::getArgs(std::string &key, std::string &value)
{
  key = mKey;
  value = mValue;

  return true;
}

/** Return the command field associated with this message.
 *
 * @return the eAudioParametersCmd_t value contained in this message.
 */
eVideoParametersCmd_t cVideoParameters::getCommand(void)
{
  return mCmd;
}

/** Return the video parameter key value.
 *
 * @return video parameter key value string.
 */
std::string cVideoParameters::getKey(void)
{
  return mKey;
}

/** Return the video parameter value associated with the given key.
 *
 * @return the video parameter value as a string.
 */
std::string cVideoParameters::getValue(void)
{
  return mValue;
}

/** Parse the given JSON message into the fields of this class.
 *
 * @param pcMsg is the JSON message string to parse.
 *
 * @return true if the JSON message is well formed and parsed.
 */
bool cVideoParameters::parseMsg(const char *pcMsg)
{
  bool parsed = false;
  Document doc;
  doc.Parse(pcMsg);
  bool isProperMsg = doc.HasMember(VIDEO_PARAMETERS_STR);
  Value *pCmdP = Pointer(VIDEO_PARAMETERS_CMD_PTR).Get(doc);
  Value *pKeyP = Pointer(VIDEO_PARAMETERS_KEY_PTR).Get(doc);
  Value *pValueP = Pointer(VIDEO_PARAMETERS_VALUE_PTR).Get(doc);

  if (isProperMsg && (pCmdP != nullptr))
    {
      const char *cmdStr = pCmdP->GetString();
      mCmd = eVideoParametersCmd_t::fromString(cmdStr);
      IF_VERIFY_STRING(pKeyP)
        {
          mKey = std::string(pKeyP->GetString());
          IF_VERIFY_STRING(pValueP)
            {
              mValue = std::string(pValueP->GetString());
            }
          else
            {
              mValue = std::string("");
            }
          LOGID("cVideoParameters::parseMsg: cmd = %s, key = %s, value = %s\n",
                mCmd, mKey, mValue);
        }
      parsed = true;
    }
  else if (isProperMsg)
    {
      LOGW("cVideoParameters::parseMsg: Bad videoParamters message \"%s\"\n",
           pcMsg);
    }
  else
    {
      LOGW("cVideoParameters::parseMsg: Not a videoParamters message \"%s\"\n",
           pcMsg);
    }

  return parsed;
}

/** Construct a JSON message in the supplied buffer for these args.
 *
 * @param pMsgBuf is the buffer to fill with the JSON message.
 * @param bufLen is the maximum capacity of the buffer.
 *
 * @return true if the message fits in the provided buffer.
 */
bool cVideoParameters::buildMsg(char *pMsgBuf, int bufLen)
{
  bool retVal = false;

  // Create document and load our document model
  Document doc;
  doc.Parse(pcVideoParamsDOM);

  // Verify existence of the 'cmd' member under the 'videoParameters' member
  if (Value *pCmdP = Pointer(VIDEO_PARAMETERS_CMD_PTR).Get(doc))
    {
      // Exists, so set it
      pCmdP->SetString(mCmd.toString(), doc.GetAllocator());
      // Set the fields
      Value *pKeyP = Pointer(VIDEO_PARAMETERS_KEY_PTR).Get(doc);
      pKeyP->SetString(mKey.c_str(), doc.GetAllocator());
      Value *pValueP = Pointer(VIDEO_PARAMETERS_VALUE_PTR).Get(doc);
      pValueP->SetString(mValue.c_str(), doc.GetAllocator());
    }

  // Create stream writer to access doc as a string
  StringBuffer buf;
  Writer<StringBuffer> writer(buf);
  doc.Accept(writer);
  const char *videoParamsMsg = buf.GetString();

  // As long as it will fit, copy the JSON into the supplied buffer
  if (strlen(videoParamsMsg) <= bufLen)
    {
      strncpy(pMsgBuf, videoParamsMsg, bufLen);
      retVal = true;
    }

  return retVal;
}


