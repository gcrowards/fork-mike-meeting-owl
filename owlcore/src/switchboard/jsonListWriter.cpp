/*
 * jsonListWriter.cpp
 *
 *  Created on: Mar 5, 2017
 *
 * Class for composing a list of JSON objects.
  */
#include "logging.hpp"
#include "rapidjson/document.h"
#include "rapidjson/pointer.h"
#include "rapidjson/writer.h"
#include "rapidjson/prettywriter.h"
#include "rapidjson/stringbuffer.h"
#include "switchboard/jsonListWriter.hpp"

// #define LIST_WRITER_VERBOSE
#ifdef LIST_WRITER_VERBOSE
#define LOGID LOGI
#else
#define LOGID(...) {}
#endif

/* *** cJsonListWriter *** */

/** Constructs an empty JSON array management list object.
 *
 */
cJsonListWriter::cJsonListWriter()
  : mObjectCount(0)
{
  mDoc.SetArray();
}

/** Destroys the object.
 *
 */
cJsonListWriter::~cJsonListWriter()
{
}

/** Adds a JSON object to the array of objects.
 *
 * @param object is the JSON object to add to the array.
 *
 * @return true if the object is valid and can be added to the document,
 *         false otherwise.
 */
bool cJsonListWriter::addObject(std::string object)
{
  bool valid = false;

  // Create document and load the object into it
  rapidjson::Document newDoc;
  newDoc.Parse(object.c_str());

  if (mDoc.IsArray() && newDoc.IsObject())
    {
      // Create rapidjson object containing contents of 'object'
      rapidjson::Value obj(newDoc, mDoc.GetAllocator());
      // And add it to the array in mDoc
      mDoc.PushBack(obj, mDoc.GetAllocator());
      valid = true;
      mObjectCount++;
    }
  return valid;
}

/** Retrieves the contents of this object as a JSON array in a string.
 *
 * @param list is the string to fill with the JSON array.
 * @param pretty (optional) pretty-prints list if true.
 *
 * @return the number of elements in the array.
 */
int cJsonListWriter::getList(std::string &list, bool pretty)
{
  // Create stream writer to access doc as a string
  rapidjson::StringBuffer buf;
  if (pretty)
    {
      rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buf);
      mDoc.Accept(writer);
    }
  else
    {
      rapidjson::Writer<rapidjson::StringBuffer> writer(buf);
      mDoc.Accept(writer);
    }
  const char *newDocMsg = buf.GetString();

  list.assign(newDocMsg);

  return mDoc.Size();
}

/** @return the number of objects in the JSON array.
 *
 */
int cJsonListWriter::getObjectCount(void)
{
  return mObjectCount;
}

