/*
 * pinnedAoiArgs.cpp
 *
 *  Created on: Mar 2, 2017
 *
 *  Class for managing pinned AOI arguments. Includes the ability to
 *  serialize and deserialize data as JSON strings.
 */
#include "circleMath.hpp"
#include "logging.hpp"
#include "params.h"
#include "rapidjson/document.h"
#include "rapidjson/pointer.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "switchboard/pinnedAoiArgs.hpp"
#include "switchboard/switchboardMessages.hpp"

// #define DEBUG_PINNED_AOI_ARGS
#ifdef  DEBUG_PINNED_AOI_ARGS
#define LOGID LOGI
#else
#define LOGID(...) {}
#endif

/** Template for creating PinnedAoiArgs messages. */
const char pcPinnedAoiDOM[] =
    "{\"" PINNED_AOI_ARGS_STR "\":"              \
        "{\"" PINNED_AOI_CMD_STR "\":\"\","      \
         "\"" PINNED_AOI_ID_STR "\":0,"          \
         "\"" PINNED_AOI_SIZE_STR "\":0,"        \
         "\"" PINNED_AOI_ANGLE_STR "\":0}}";

using namespace rapidjson;

/* *** cPinnedAoiArgs *** */


/** Create an object with default values. */
cPinnedAoiArgs::cPinnedAoiArgs()
  : mCmd(ePinnedAoiCmd_t::NONE), mId(0), mSize(0), mAngle(0), mConsumed(false)
{}

/** Create an object with command and id specified. */
cPinnedAoiArgs::cPinnedAoiArgs(ePinnedAoiCmd_t cmd, int id)
: mCmd(cmd), mId(id), mSize(0), mAngle(0), mConsumed(false)
{}

/** Create an object with command, id, size and angle all specfified. */
cPinnedAoiArgs::cPinnedAoiArgs(ePinnedAoiCmd_t cmd, int id, int size, int angle)
: mCmd(cmd), mId(id), mSize(size), mAngle(angle), mConsumed(false)
{}

/** Destroy an object. */
cPinnedAoiArgs::~cPinnedAoiArgs()
{}

/** @param newCmd is the new pinning command.
 *
 * This setter triggers processing the command in the Attention System
 * (possibly other places), whereas setting the other parameter setters are
 * not expected to.
 */
void cPinnedAoiArgs::setCommand(ePinnedAoiCmd_t cmd)
{
  mCmd = cmd;
  mConsumed = false;  // Since the command just changed, make sure clients
                      // have a chance to respond
}

/** @param id is the new AOI id */
void cPinnedAoiArgs::setId(int id)
{
  mId = id;
}

/** @param size is the new AOI size */
void cPinnedAoiArgs::setSize(int size)
{
  mSize = size;
}

/** @param newAngle is the new AOI center angle. */
void cPinnedAoiArgs::setAngle(int angle)
{
  mAngle = angle;
}

/** Get the command. */
ePinnedAoiCmd_t cPinnedAoiArgs::getCommand(void)
{
  return mCmd;
}

/** Get the ID. */
int cPinnedAoiArgs::getId(void)
{
  return mId;
}

/** Get the size. */
int cPinnedAoiArgs::getSize(void)
{
  return mSize;
}

/** Get the angle. */
int cPinnedAoiArgs::getAngle(void)
{
  return mAngle;
}

/** Parse the given JSON message into the fields of this args class.
 *
 * @param pcMsg is the JSON message string to parse.
 *
 * @return true if the JSON message is well formed and parsed.
 */
bool cPinnedAoiArgs::parseMsg(const char *pcMsg)
{
  bool parsed = false;
  Document doc;
  doc.Parse(pcMsg);
  bool isPinnedAoiMsg = doc.HasMember(PINNED_AOI_ARGS_STR);
  Value *cmdP = Pointer(PINNED_AOI_CMD_PTR).Get(doc);
  Value *idP = Pointer(PINNED_AOI_ID_PTR).Get(doc);
  Value &sizeR = Pointer(PINNED_AOI_SIZE_PTR).GetWithDefault(doc, 0);
  Value *sizeP = &sizeR;
  Value &angleR = Pointer(PINNED_AOI_ANGLE_PTR).GetWithDefault(doc, 0);
  Value *angleP = &angleR;

  if (isPinnedAoiMsg && (cmdP != nullptr) && (idP != nullptr))
    {
      const char *cmdStr;

      IF_VERIFY_STRING(cmdP)
        {
          cmdStr = cmdP->GetString();
          mCmd = ePinnedAoiCmd_t::fromString(cmdStr);
        }
      IF_VERIFY_INT(idP)
        {
          mId  = idP->GetInt();
        }
      IF_VERIFY_INT(sizeP)
        {
          mSize = sizeP->GetInt();
        }
      IF_VERIFY_INT(angleP)
        {
          mAngle = angleP->GetInt();
	  
          if((mCmd == ePinnedAoiCmd_t::CREATE) ||
             (mCmd == ePinnedAoiCmd_t::MOVE))
	    { // if command is coming from mobile app, adjust coordinate system
	      
	      // Mirror in X to match perspective of phone, and rotate to align
	      // with Owl's coordinate system
	      mAngle = circleMath::intSubAngs(360, mAngle);
	      mAngle = circleMath::intSubAngs(mAngle, CAMERA_ALIGNMENT_ANGLE);
	    }
        }
      
      mConsumed = false;
      parsed = true;
      LOGID(
        "cPinnedAoiArgs::parseMsg: cmd = %d, id = %d, size = %d, angle = %d\n",
        mCmd, mId, mSize, mAngle);
    }
  else if (isPinnedAoiMsg)
    {
      LOGW("cPinnedAoiArgs::parseMsg: Bad PinnedAoi message \"%s\"\n", pcMsg);
    }
  else
    {
      LOGW("cPinnedAoiArgs::parseMsg: Not a PinnedAoi message \"%s\"\n", pcMsg);
    }

  return parsed;
}

/** Construct a JSON message in the supplied buffer for these args.
 *
 * @param pMsgBuf is the buffer to fill with the JSON message.
 * @param bufLen is the maximum capacity of the buffer.
 *
 * @return true if the message fits in the provided buffer.
 */
bool cPinnedAoiArgs::buildMsg(char *pMsgBuf, int bufLen)
{
  bool retVal = false;

  // Create document and load our document model
  Document doc;
  doc.Parse(pcPinnedAoiDOM);

  // Verify existence of the 'mCmd' member under the 'PinnedAoi' member
  if (Value *cmdP = Pointer(PINNED_AOI_CMD_PTR).Get(doc))
    {
      // Exists, so set it
      cmdP->SetString(mCmd.toString(), doc.GetAllocator());
      // Set the id, size, and angle values, too
      Value *idP = Pointer(PINNED_AOI_ID_PTR).Get(doc);
      if (idP != nullptr) { idP->SetInt(mId); }
      Value &sizeP = Pointer(PINNED_AOI_SIZE_PTR).GetWithDefault(doc, 0);
      sizeP.SetInt(mSize);
      Value &angleP = Pointer(PINNED_AOI_ANGLE_PTR).GetWithDefault(doc, 0);
      angleP.SetInt(mAngle);
    }

  // Create stream writer to access doc as a string
  StringBuffer buf;
  Writer<StringBuffer> writer(buf);
  doc.Accept(writer);
  const char *pinnedAoiMsg = buf.GetString();

  // As long as it will fit, copy the JSON into the supplied buffer
  if (strlen(pinnedAoiMsg) <= bufLen)
    {
      strncpy(pMsgBuf, pinnedAoiMsg, bufLen);
      retVal = true;
    }

  return retVal;
}

/** Return the argument parameters contained in this class.
 *
 * @param cmdArg is the command contained in the message.
 * @param idArg is the meetingOwl supplied ID that identifies the AOI.
 * @param sizeArg is the size of the AOI.
 * @param angleArg is the angle of the center of the AOI.
 */
bool cPinnedAoiArgs::getArgs(ePinnedAoiCmd_t &cmdArg, int &idArg,
                             int &sizeArg, int &angleArg)
{
  bool previouslyConsumed = mConsumed;

  cmdArg = mCmd;
  idArg = mId;
  sizeArg = mSize;
  angleArg = mAngle;

  return previouslyConsumed;
}

/** Return indication whether this instance's arguments have already been used.
 *
 * @return true if this object's arguments have been consumed via getArgs().
 */
bool cPinnedAoiArgs::getArgsConsumed()
{
  return mConsumed;
}

/** Return indication whether this instance's arguments have already been used.
 *
 * @return true if this object's arguments have been consumed via getArgs().
 */
void cPinnedAoiArgs::setArgsConsumed()
{
  mConsumed = true;
}

/** Mark arguments as already consumed effectively throwing them away. */
void cPinnedAoiArgs::abortArgs(void)
{
  mConsumed = true;
}


