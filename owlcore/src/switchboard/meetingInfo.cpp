/*
 * meetingInfo.cpp
 *
 *  Created on: Mar 30, 2017
 *
 *  Class defining packets for providing meeting information.
 */

#include <string.h>

#include "logging.hpp"
#include "rapidjson/document.h"
#include "rapidjson/pointer.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "switchboard/meetingInfo.hpp"
#include "switchboard/switchboardMessages.hpp"

/** Template for creating MeetingInfo messages. */
const char cMeetingInfo::pcMeetingInfoDOM[] =
  "{\"" MEETING_INFO_ARGS_STR "\":"           \
  "{\"" MEETING_INFO_STATE_STR "\":false}}";

/*** cMeetingInfo ***/
using namespace rapidjson;

/** Create an object with default values. */
cMeetingInfo::cMeetingInfo()
  : mState(false), mConsumed(false)
{ }

cMeetingInfo::cMeetingInfo(bool state)
  : mState(state), mConsumed(false)
{ }

/** Destroy an object. */
cMeetingInfo::~cMeetingInfo()
{ }

/** Get whether the owl is meeting. */
bool cMeetingInfo::getState() const
{
  return mState;
}

/** Parse the given JSON message into the fields of this args class.
 *
 * @param pcMsg is the JSON message string to parse.
 *
 * @return true if the JSON message is well formed and parsed.
 */
bool cMeetingInfo::parseMsg(const char *pcMsg)
{
  bool parsed = false;
  Document doc;
  doc.Parse(pcMsg);
  bool isMeetingMsg = doc.HasMember(MEETING_INFO_ARGS_STR);
  Value *statePtr = Pointer(MEETING_INFO_STATE_PTR).Get(doc);

  if(isMeetingMsg && statePtr)
    {
      IF_VERIFY_BOOL(statePtr)
        {
          mState = statePtr->GetBool();
          mConsumed = false;
          parsed = true;
          LOGD("cMeetingInfo::parseMsg: state = %s\n",
               (mState ? "true" : "false"));
        }
    }
  else if(isMeetingMsg)
    {
      LOGW("cMeetingInfo::parseMsg: Bad MeetingInfo message \"%s\"\n", pcMsg);
    }
  else
    {
      LOGW("cMeetingInfo::parseMsg: Not a MeetingInfo message \"%s\"\n", pcMsg);
    }

  return parsed;
}

/** Construct a JSON message in the supplied buffer for these args.
 *
 * @param pMsgBuf is the buffer to fill with the JSON message.
 * @param bufLen is the maximum capacity of the buffer.
 *
 * @return true if the message fits in the provided buffer.
 */
bool cMeetingInfo::buildMsg(char *pMsgBuf, int bufLen)
{
  bool retVal = false;

  // Create document and load our document model
  Document doc;
  doc.Parse(pcMeetingInfoDOM);

  // Verify existence of the 'state' member under the 'MeetingInfo' member
  Value *statePtr = Pointer(MEETING_INFO_STATE_PTR).Get(doc);
  if(statePtr)
    { // Exists, so set it
      statePtr->SetBool(mState);
    }

  // Create stream writer to access doc as a string
  StringBuffer buf;
  Writer<StringBuffer> writer(buf);
  doc.Accept(writer);
  const char *infoMsg = buf.GetString();

  // As long as it will fit, copy the JSON into the supplied buffer
  if(strlen(infoMsg) <= bufLen)
  {
      strncpy(pMsgBuf, infoMsg, bufLen);
      retVal = true;
  }

  return retVal;
}

/** Return the argument parameters contained in this class.
 *
 * @param state is the meeting state contained in the message.
 */
bool cMeetingInfo::getArgs(bool &state)
{
  bool previouslyConsumed = mConsumed;

  state = mState;
  mConsumed = true;

  return previouslyConsumed;
}

/** Return indication whether this instance's arguments have already been used.
 *
 * @return true if this object's arguments have been consumed via getArgs().
 */
bool cMeetingInfo::argsConsumed()
{
  return mConsumed;
}

/** Mark arguments as already consumed effectively throwing them away. */
void cMeetingInfo::abortArgs()
{
  mConsumed = true;
}
