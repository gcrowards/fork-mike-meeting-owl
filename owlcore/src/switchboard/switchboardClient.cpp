/*
 * switchboardClient.cpp
 *
 * Copyright (c) Owl Labs Inc. 2017.
 * All rights reserved.
 *
 *  Owl IPC (Owl Inter Process Communications) client implementation.
 */
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>

#include "logging.hpp"
#include "switchboard/switchboardClient.hpp"


#define DEBUG_SWITCHBOARD_CLIENT_ERRORS
#ifdef DEBUG_SWITCHBOARD_CLIENT_ERRORS
#define LOGED LOGE
#else
#define LOGED(...) {}
#endif

// #define DEBUG_SWITCHBOARD_CLIENT_WARNINGS
#ifdef DEBUG_SWITCHBOARD_CLIENT_WARNINGS
#define LOGWD LOGW
#else
#define LOGWD(...) {}
#endif

// #define DEBUG_SWITCHBOARD_CLIENT_INFO
#ifdef DEBUG_SWITCHBOARD_CLIENT_INFO
#define LOGID LOGI
#else
#define LOGID(...) {}
#endif

// Enable info messages concerning runtime message passing
// #define DEBUG_SWITCHBOARD_CLIENT_INFO_RUN
#ifdef DEBUG_SWITCHBOARD_CLIENT_INFO_RUN
#define LOGIDR LOGI
#else
#define LOGIDR(...) {}
#endif

/* *** cSwitchBoardClient *** */

/** Create an instance of a client for the Owl IPC server.
 *
 * @param ipAddr is the IP address of the server to which to connect.
 */
cSwitchboardClient::cSwitchboardClient(std::string ipAddr,
                                       oipcMessageCallback_t errorCB) :
  mSockFd(-1),
  mConnected(false),
  mpReaderThread(nullptr),
  mIpAddr(ipAddr),
  mRestartThread(std::bind(&cSwitchboardClient::retryConnectionFunc, this),
                  "clientRestart"),
  mErrorCB(errorCB),
  mError(false)
{
  mRestartThread.start();

  pthread_mutex_init(&mTypeListMutex, NULL);
}

/** Destroy an Owl IPC client.
 *
 */
cSwitchboardClient::~cSwitchboardClient()
{
  mRestartThread.signalStop();
  mRestartThread.join();

  finish();

  pthread_mutex_destroy(&mTypeListMutex);
}

/** Start or restart an Owl IPC client connection.
 *
 * When a client first connects, this routine, running in its own thread,
 * immediately initiates the client connection and in the process creates
 * the reader thread. After starting the connection, this thread runs
 * periodically to check that the connection is still up. If not, then
 * it tears down the previous connection, and the next time it runs,
 * attempts to reconnect. This allows a client to create a connection
 * once, and recover if the server goes down and then comes back up.
 */
void cSwitchboardClient::retryConnectionFunc(void)
{
  if (!mConnected && !mError)
    {  // mConnected is cleared when socket connection goes away
      if (mSockFd >= 0)
        {   // mSockFd is invalid when the socket connection has been torn down
          LOGID("retryConnectionFunc: Attempting cleanup (finish)\n");
          finish();
          usleep(2000000); // Give server time to shut down, if needed
        }
      else
        {
          LOGID("retryConnectionFunc: Attempting restart\n");
          init();
          usleep(500000);  // Sleep 1/2 second before checking connection
        }
    }
  else
    {  // Connection is fine or error state, sleep until the next check-up
      usleep(100000);  // Sleep 1/2 second before checking connection again
    }
}

/** Set up the socket for an Owl IPC client connection.
 * 
 * This routine opens and connects the socket to the server, then creates
 * a thread to read messages from the socket.
 */
int cSwitchboardClient::init(void)
{
  struct sockaddr_in servAddr;
  int sockFd;
  struct linger lingerParams = { .l_onoff = 0, .l_linger = 1 };
  int optval;

  if ((sockFd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
      LOGWD("init: Could not create socket \n");
      return -1;
    }

  if (setsockopt(sockFd, SOL_SOCKET, SO_LINGER,
                 &lingerParams, sizeof(lingerParams)) < 0)
    { LOGED("init: setsockopt failed.\n"); }

  /* Set the option active */
  optval = 1;
  if(setsockopt(sockFd, SOL_SOCKET, SO_KEEPALIVE,
                &optval, sizeof(optval)) < 0)
    { LOGED("init: setsockopt SO_KEEPALIVE failed.\n"); }

  /* Set the interval before a keep-alive is sent */
  optval = 60;
  if(setsockopt(sockFd, SOL_TCP, TCP_KEEPIDLE,
                &optval, sizeof(optval)) < 0)
    { LOGED("init: setsockopt SO_KEEPALIVE failed.\n"); }


  memset(&servAddr, '0', sizeof(servAddr));
  servAddr.sin_family = AF_INET;
  servAddr.sin_port = htons(TCP_PORT);

  if (inet_pton(AF_INET, mIpAddr.c_str(), (void *)&servAddr.sin_addr) <= 0)
    {
      LOGED("init: inet_pton error occurred\n");
      close(sockFd);
      return -2;
    }

  if (connect(sockFd, (struct sockaddr *)&servAddr, sizeof(servAddr)) < 0)
    {
      LOGWD("init: Connect Failed \n");
      close(sockFd);
      return -3;
    }

  mSockFd = sockFd;

  mpReaderThread = new cThread(std::bind(&cSwitchboardClient::readerThreadFunc, 
                                         this), "clientReader");
  mConnected = true;
  mpReaderThread->start();

  // Re-subscribe to messages, if restarting
  std::vector<oipcTypeCallback_t> listCopy = mTypeCallbackList;
  mTypeCallbackList.clear();
  for (std::vector<oipcTypeCallback_t>::iterator
      pItem = listCopy.begin();
      pItem != listCopy.end();
      pItem++)
    {
      subscribe((*pItem).mType, (*pItem).mCB);
    }

  return 0;
}

/** Tear down the client socket and terminate the reader thread.
 *
 */
int cSwitchboardClient::finish(void)
{
  if (mpReaderThread != nullptr)
    { mpReaderThread->signalStop(); }

  if (mSockFd >= 0)
    {
      LOGID("finish: attempting shutdown of client\n");
      if (shutdown(mSockFd, SHUT_WR) < 0)
        {
          LOGED("client: shutdown of socket failed.\n");
        }
      usleep(100000);
      close(mSockFd);
      mSockFd = -1;
    }

  if (mpReaderThread != nullptr)
    {
      mpReaderThread->join();
      delete mpReaderThread;
      mpReaderThread = nullptr;
    }

  return 0;
}

/** Subscribe to a 'type' of messages and register 'cb' as their callback.
 *
 * Callback 'cb' will be called every time a message of type 'type' is
 * received. This routine sends a message to the server requesting the
 * subscription.
 */
int cSwitchboardClient::subscribe(eOipcMessageType type,
                                  oipcMessageCallback_t cb)
{
  oipcSubscriptionMessage subscription;
  oipcTypeCallback_t tcb;
  int rc;

  tcb.mType = type;
  tcb.mCB = cb;
  pthread_mutex_lock(&mTypeListMutex);
  mTypeCallbackList.push_back(tcb);
  pthread_mutex_unlock(&mTypeListMutex);

  subscription.type = htonl(type);

  rc = writeMessage(SUBSCRIPTION_ADD, (char *)&subscription,
                    sizeof(subscription));

  return rc;
}

/** Cancel a subscription to messages of type 'type'.
 *
 * Sends a request to the Owl IPC server to no longer send messages of type
 * 'type' to this client.
 */
int cSwitchboardClient::unSubscribe(eOipcMessageType type)
{
  oipcSubscriptionMessage subscription;
  int rc;

  pthread_mutex_lock(&mTypeListMutex);
  for (std::vector<oipcTypeCallback_t>::iterator
      pItem = mTypeCallbackList.begin();
      pItem != mTypeCallbackList.end();
      pItem++)
    {
      if ((*pItem).mType == type)
        {
          mTypeCallbackList.erase(pItem);
          break;
        }
    }
  pthread_mutex_unlock(&mTypeListMutex);

  subscription.type = htonl(type);

  rc = writeMessage(SUBSCRIPTION_REMOVE, (char *)&subscription,
                    sizeof(subscription));

  return rc;
}

/** Returns state of the client's connection to the server.
 *
 * @return true if connected, false otherwise.
 */
bool cSwitchboardClient::isConnectionOpen(void)
{
  return (mConnected);
}

/** Function called in a loop by the reader thread to receive messages.
 *
 * This routine is typically waiting for an Owl IPC message to arrive from
 * the server. When a message arrives, if it matches a desired type, then
 * the associated callback is invoked with the message as its argument. If
 * the read fails, that indicates that the server has closed, in which case
 * the mConnected flag is cleared so that the retry thread can shutdown
 * the socket gracefully. However, the recommended method for shutting
 * down the socket requires that the server send the 'CLIENT_PLEASE_CLOSE'
 * message so the client can shut down before the server. As long as the
 * client shuts down first, the server can restart the socket immediately,
 * but if the server shuts down first, and the client shuts down due to a
 * broken connection, then the server will have to wait for a minute or
 * two for the OS to time out the old socket connection before the new one
 * can be initiated.
 */
void cSwitchboardClient::readerThreadFunc(void)
{
  int readRet;
  static char serverVerErr[] = SERVER_VER_ERR;

  if (mConnected)
    {
      readRet = gReadMessage(mSockFd, &mHeader, mMsg, sizeof(mMsg));
      if (readRet >= 0)
        {
          // Upon getting here, full message has been read
          eOipcMessageType msgType = (eOipcMessageType)mHeader.type;

          LOGIDR("client::readerThreadFunc: Got message (%d, %d, [%d, ...])\n",
               mHeader.type, mHeader.length,
               ((mHeader.length > 0) ? mMsg[0] : 0));
          switch (msgType)
          {
            case CLIENT_PLEASE_CLOSE:
              LOGID("readerThreadFunc: server asked me to close\n");
              // Clear the mConnected flag and let the retry thread shut down
              mConnected = false;
              break;
            case CLIENT_VERSION_ERROR:
              LOGID("readerThreadFunc: Client / Server VERSION MISMATCH!\n");
              if (mErrorCB != nullptr)
                {
                  mErrorCB(CLIENT_VERSION_ERROR, serverVerErr,
                           sizeof(serverVerErr));
                }
              mConnected = false;
              mError = true;
              break;
            default:
              break;
          }
          // Find type / callback entry(s) that match message type and invoke
          // the callback with the message and type for each.
          pthread_mutex_lock(&mTypeListMutex);
          for (std::vector<oipcTypeCallback_t>::iterator
              pItem = mTypeCallbackList.begin();
              pItem != mTypeCallbackList.end();
              pItem++)
            {
              if ((*pItem).mType == msgType)
                {
                  if ((*pItem).mCB != nullptr)
                    {
                      (*pItem).mCB(msgType, mMsg, mHeader.length);
                    }
                }
            }
          pthread_mutex_unlock(&mTypeListMutex);
        }
      else if (readRet == -5)
        {
          LOGI("readerThreadFunc: Client / Server VERSION MISMATCH!\n");
          if (mErrorCB != nullptr)
            {
              mErrorCB(CLIENT_VERSION_ERROR, serverVerErr,
                       sizeof(serverVerErr));
            }
          mConnected = false;
          mError = true;
        }
      else
        {
          LOGED("readerThreadFunc: readMessage returned error\n");
          // Clear the mConnected flag and let the retry thread shut down
          mConnected = false;
        }
    }
  else
    {
      usleep(10000);  // No socket, so just snooze and try again later
    }

  return;
}

/** Write a message to the Owl IPC server.
 *
 */
int cSwitchboardClient::writeMessage(eOipcMessageType type,
                                             char *pcMsg,
                                             size_t len)
{
  return gWriteMessage(mSockFd, type, pcMsg, len);
}


