/*
 * jsonListReader.cpp
 *
 *  Created on: Mar 5, 2017
 *
 * Class for unpacking a list of JSON objects.
  */
#include "logging.hpp"
#include "rapidjson/document.h"
#include "rapidjson/pointer.h"
#include "rapidjson/writer.h"
#include "rapidjson/prettywriter.h"
#include "rapidjson/stringbuffer.h"
#include "switchboard/jsonListReader.hpp"

// #define LIST_READER_VERBOSE
#ifdef LIST_READER_VERBOSE
#define LOGID LOGI
#else
#define LOGID(...) {}
#endif


/** Constructor builds an empty reader.
 *
 */
cJsonListReader::cJsonListReader() :
  mNextItem(0),
  mValid(false),
  mObjectCount(0)
{
}

/** Destructor finalizes a reader.
 *
 */
cJsonListReader::~cJsonListReader()
{
}

/** Pass a string to the reader to parse as a JSON list.
 *
 * This parses the JSON input into a RapidJson document and counts the
 * number of array elements in the list. The JSON is assumed to be in
 * the form of an array of objects, and this function confirms that to
 * be true.
 *
 * @param listMsg is a JSON array of objects in a string.
 * @param pretty (optional) pretty-print debug output.
 *
 * @return true if the JSON is a proper array, false otherwise.
 */
bool cJsonListReader::setList(std::string listMsg, bool pretty)
{
  // Parse supplied string into document
  rapidjson::ParseResult ok = mDoc.Parse(listMsg.c_str());
  if (pretty)
    {
      rapidjson::StringBuffer buf;
      rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buf);
      mDoc.Accept(writer);
      LOGID("setList: %s\n", buf.GetString());
    }
  else
    {
      LOGID("setList: listMsg: %s\n", listMsg.c_str());
    }

  mValid = (bool)ok;
  mNextItem = 0;

  if (mValid)
    {   // Parse succeeded, at least it is JSON
      mValid = mDoc.IsArray();
      mObjectCount = mDoc.Size();
    }
  if (mValid)
    {
      LOGID("setList: mDoc isArray true, size = %d\n",
           mDoc.Size());
    }
  else
    {
      LOGI("setList: mDoc is NOT an array!\n");
    }

  return mValid;
}

/** Return the number of items in the array.
 *
 * @return the number of objects.
 */
int cJsonListReader::getObjectCount(void)
{
  return mObjectCount;
}

/** Resets the read index back to the first element of the list.
 *
 */
void cJsonListReader::resetList(void)
{
  mNextItem = 0;
}

/** Get the next JSON object in the list and advance the read index.
 *
 * @param key is string used to find matching objects in the list array.
 * @param object is the string to fill with the found object
 *
 * @return true if a match is found, false if not.
 */
bool cJsonListReader::getNextObject(std::string key, std::string &object)
{
  bool found = false;

  if (mValid && (mDoc.Size() > 0))
    {
      for (rapidjson::SizeType item = mNextItem;
           item < mDoc.Size() && !found;
           item++)
        {
          char ptrStr[64];

          // create a RapidJson pointer to access the desired array element
          snprintf(ptrStr, sizeof(ptrStr), "/%d/%s", item, key.c_str());
          rapidjson::Value *thisObj = rapidjson::Pointer(ptrStr).Get(mDoc);
          if (thisObj != NULL)
            {
              snprintf(ptrStr, sizeof(ptrStr), "/%d", item);
              thisObj = rapidjson::Pointer(ptrStr).Get(mDoc);
              if (thisObj->IsObject())
                {
                  // Create stream writer to access doc as a string
                  rapidjson::StringBuffer buf;
                  rapidjson::Writer<rapidjson::StringBuffer> writer(buf);
                  thisObj->Accept(writer);
                  const char *itemMsg = buf.GetString();
                  object.assign(itemMsg);
                  LOGID("getNextObject: object = %s\n", object.c_str());
                  found = true;
                  mNextItem = item + 1;
                  break;
                }
            }
        }
    }
  if (!found)
    {
      LOGID("getNextObject: no object found\n");
    }
  return found;
}

