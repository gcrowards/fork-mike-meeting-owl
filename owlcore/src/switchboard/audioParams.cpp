/*
 * audioParams.cpp
 *
 * Class that defines JSON messages for exchanging audio
 * parameters over a Switchboard connection.
 *
 * Copyright (c) Owl Labs Inc. 2017.
 * All rights reserved.
 *
 * Created on: Jul 25, 2017
 */

#include "logging.hpp"
#include "rapidjson/document.h"
#include "rapidjson/pointer.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "switchboard/audioParams.hpp"
#include "switchboard/switchboardMessages.hpp"


// #define DEBUG_AUDIO_PARAMS_MSGS
#ifdef  DEBUG_AUDIO_PARAMS_MSGS
#define LOGID LOGI
#else
#define LOGID(...) {}
#endif


/** Templates for creating SetupProgressSet messages. */
const char pcAudioParamsDOM[] =
    "{\"" AUDIO_PARAMETERS_STR "\":"                                   \
        "{\"" AUDIO_PARAMETERS_CMD_STR "\":\"none\","                  \
         "\"" AUDIO_PARAMETERS_NOISE_LEVEL_THRESHOLD_STR "\":0,"       \
         "\"" AUDIO_PARAMETERS_VOLUME_STR "\":0,"                      \
         "\"" AUDIO_PARAMETERS_MUTE_STR "\":false,"                    \
         "\"" AUDIO_PARAMETERS_NOISE_SUPPRESSION_LEVEL_STR "\":false," \
         "\"" AUDIO_PARAMETERS_ENABLE_DTD_STR "\":false}}";


using namespace rapidjson;

/** Construct a default audio parameters object.
 *
 * This object is used for generating or unpacking JSON audio parameter
 * messages.
 */
cAudioParameters::cAudioParameters()
  : mCmd(eAudioParametersCmd_t::NONE),
    mMinSpeakerLevel(0),
    mMinSpeakerLevelValid(false),
    mVolume(0),
    mVolumeValid(false),
    mMute(false),
    mMuteValid(false),
    mNoiseSuppressionLevel(false),
    mNoiseSuppressionLevelValid(false),
    mEnableDTD(false),
    mEnableDTDValid(false)
{
}

/** Construct an audio parameters object.
 *
 * This object is used for generating or unpacking JSON audio parameter
 * messages.
 *
 * @param cmd is the command field initializer.
 */
cAudioParameters::cAudioParameters(eAudioParametersCmd_t cmd)
  : mCmd(cmd),
    mMinSpeakerLevel(0),
    mMinSpeakerLevelValid(false),
    mVolume(0),
    mVolumeValid(false),
    mMute(false),
    mMuteValid(false),
    mNoiseSuppressionLevel(false),
    mNoiseSuppressionLevelValid(false),
    mEnableDTD(false),
    mEnableDTDValid(false)
{
}

/** Construct an audio parameters object.
 *
 * This object is used for generating or unpacking JSON audio parameter
 * messages.
 *
 * @param cmd is the command field initializer.
 */
cAudioParameters::cAudioParameters(eAudioParametersCmd_t cmd,
                                   int minSpeakerLevel,
                                   int noiseSuppressionLevel)
  : mCmd(cmd),
    mMinSpeakerLevel(minSpeakerLevel),
    mMinSpeakerLevelValid(true),
    mVolume(0),
    mVolumeValid(false),
    mMute(false),
    mMuteValid(false),
    mNoiseSuppressionLevel(noiseSuppressionLevel),
    mNoiseSuppressionLevelValid(true),
    mEnableDTD(false),
    mEnableDTDValid(false)
{
}

/** Construct an audio parameters object.
 *
 * This object is used for generating or unpacking JSON audio parameter
 * messages.
 *
 * @param cmd is the command field initializer.
 * @param minSpeakerLevel is the noise threshold to apply to the audio
 *    input streams to account for Skype 4 Business injected speaker noise.
 */
cAudioParameters::cAudioParameters(eAudioParametersCmd_t cmd,
                                   int  volume,
                                   bool mute,
                                   int  minSpeakerLevel,
                                   int noiseSuppressionLevel)
  : mCmd(cmd),
    mMinSpeakerLevel(minSpeakerLevel),
    mMinSpeakerLevelValid(true),
    mVolume(volume),
    mVolumeValid(true),
    mMute(mute),
    mMuteValid(true),
    mNoiseSuppressionLevel(noiseSuppressionLevel),
    mNoiseSuppressionLevelValid(true),
    mEnableDTD(false),
    mEnableDTDValid(false)
{
}

/** Construct an audio parameters object.
 *
 * This object is used for generating or unpacking JSON audio parameter
 * messages.
 *
 * @param cmd is the command field initializer.
 * @param volume is the volume setting from 0 to 100.
 * @param mute is a boolean indicating if audio mute is on (true) else off.
 * @param minSpeakerLevel is the noise threshold to apply to the audio
 *    input streams to account for Skype 4 Business injected speaker noise.
 * @param noiseSupressionLevel
 * @param enableDTD is a boolean, true if double talk detection is on, else off.
 */
cAudioParameters::cAudioParameters(eAudioParametersCmd_t cmd,
                                   int  volume,
                                   bool mute,
                                   int  minSpeakerLevel,
                                   int  noiseSuppressionLevel,
                                   bool enableDTD)
  : mCmd(cmd),
    mMinSpeakerLevel(minSpeakerLevel),
    mMinSpeakerLevelValid(true),
    mVolume(volume),
    mVolumeValid(true),
    mMute(mute),
    mMuteValid(true),
    mNoiseSuppressionLevel(noiseSuppressionLevel),
    mNoiseSuppressionLevelValid(true),
    mEnableDTD(enableDTD),
    mEnableDTDValid(true)
{
}

/** Destroy an audio parameters object.
 *
 */
cAudioParameters::~cAudioParameters(void)
{
}

/** Return the argument parameters contained in this class.
 *
 * @param state is the state value contained in the message.
 *
 * @return true if the value is valid, false otherwise.
 */
bool cAudioParameters::getArgs(int  &volume,
                               bool &volumeValid,
                               bool &mute,
                               bool &muteValid,
                               int  &minSpeakerLevel,
                               bool &minSpeakerLevelValid,
                               int  &noiseSuppressionLevel,
                               bool &noiseSuppressionLevelValid,
                               bool &enableDTD,
                               bool &enableDTDValid)
{
  volume = mVolume;
  volumeValid = mVolumeValid;
  mute = mMute;
  muteValid = mMuteValid;
  minSpeakerLevel = mMinSpeakerLevel;
  minSpeakerLevelValid = mMinSpeakerLevelValid;
  noiseSuppressionLevel = mNoiseSuppressionLevel;
  noiseSuppressionLevelValid = mNoiseSuppressionLevelValid;
  enableDTD = mEnableDTD;
  enableDTDValid = mEnableDTDValid;

  return true;
}

/** Return the Minimum Speaker Level audio parameter
 *
 * @return the minimum speaker level parameter that determines the noise
 *    threshold to apply to mic audio streams to account for injected
 *    noise in the speaker stream generated by Skype 4 Business.
 */
bool cAudioParameters::getMinSpeakerLevel(int &minSpeakerLevel)
{
  minSpeakerLevel = mMinSpeakerLevel;
  return mMinSpeakerLevelValid;
}

/** Return the Volume audio parameter
 *
 * @return true if the speaker volume parameter is valid.
 */
bool cAudioParameters::getVolume(int &volume)
{
  volume = mVolume;
  return mVolumeValid;
}

/** Return the Mute audio parameter
 *
 * @return true if the mute parameter is valid.
 */
bool cAudioParameters::getMute(bool &mute)
{
  mute = mMute;
  return mMuteValid;
}

/** Return the Enable Noise Suppression audio parameter
 *
 * @return the state of the noise suppression flag.
 */
int cAudioParameters::getNoiseSuppressionLevel(int &noiseSuppressionLevel)
{
  noiseSuppressionLevel = mNoiseSuppressionLevel;
  return mNoiseSuppressionLevelValid;
}

/** Return the double talk detection (DTD) enabled audio parameter
 *
 * @param enableDTD is a reference to a flag indicating if DTD is enabled.
 * @return true if double talk detection is enabled parameter is valid.
 */
bool cAudioParameters::getEnableDTD(bool &enableDTD)
{
  enableDTD = mEnableDTD;
  return mEnableDTDValid;
}

/** Set the Minimum Speaker Level audio parameter
 *
 * @param minSpeakerLevel is the parameter that determines the noise
 *    threshold to apply to mic audio streams to account for injected
 *    noise in the speaker stream generated by Skype 4 Business.
 */
void cAudioParameters::setMinSpeakerLevel(int minSpeakerLevel)
{
  mMinSpeakerLevel = minSpeakerLevel;
  mMinSpeakerLevelValid = true;
}

/** Set the Volume audio parameter
 *
 * @param volume is the speaker volume for the Owl audio.
 */
void cAudioParameters::setVolume(int volume)
{
  mVolume = volume;
  mVolumeValid = true;
}

/** Set the Mute audio parameter
 *
 * @param mute is the microphone mute control for the Owl audio.
 */
void cAudioParameters::setMute(bool mute)
{
  mMute = mute;
  mMuteValid = true;
}

/** Set the Noise Suppression state audio parameter
 *
 * @param enableNoiseSuppression is the parameter that determines the
 * state of the noise suppression: true = on, false = off.
 */
void cAudioParameters::setNoiseSuppressionLevel(int level)
{
  mNoiseSuppressionLevel = level;
  mNoiseSuppressionLevelValid = true;
}

/** Set the double talk detection (DTD) enabled audio parameter
 *
 * @param enableDTD is the state of the DTD.
 */
void cAudioParameters::setEnableDTD(bool enableDTD)
{
  mEnableDTD = enableDTD;
  mEnableDTDValid = true;
}

/** Return the command field associated with this message.
 *
 * @return the eAudioParametersCmd_t value contained in this message.
 */
eAudioParametersCmd_t cAudioParameters::getCommand(void)
{
  return mCmd;
}

/** Parse the given JSON message into the fields of this class.
 *
 * @param pcMsg is the JSON message string to parse.
 *
 * @return true if the JSON message is well formed and parsed.
 */
bool cAudioParameters::parseMsg(const char *pcMsg)
{
  bool parsed = false;
  Document doc;
  doc.Parse(pcMsg);
  bool isProperMsg = doc.HasMember(AUDIO_PARAMETERS_STR);
  Value *pCmdP = Pointer(AUDIO_PARAMETERS_CMD_PTR).Get(doc);
  Value *pMinSpeakerLevelP =
      Pointer(AUDIO_PARAMETERS_NOISE_LEVEL_THRESHOLD_PTR).Get(doc);
  Value *pVolumeP =
      Pointer(AUDIO_PARAMETERS_VOLUME_PTR).Get(doc);
  Value *pMuteP =
      Pointer(AUDIO_PARAMETERS_MUTE_PTR).Get(doc);
  Value *pNoiseSuppressionLevelP =
      Pointer(AUDIO_PARAMETERS_NOISE_SUPPRESSION_LEVEL_PTR).Get(doc);
  Value *pEnableDTDP =
      Pointer(AUDIO_PARAMETERS_ENABLE_DTD_PTR).Get(doc);

  if (isProperMsg && (pCmdP != nullptr))
    {
      const char *cmdStr = pCmdP->GetString();
      mCmd = eAudioParametersCmd_t::fromString(cmdStr);
      if (pMinSpeakerLevelP != nullptr)
        {
          IF_VERIFY_INT(pMinSpeakerLevelP)
            {
              mMinSpeakerLevel = pMinSpeakerLevelP->GetInt();
              mMinSpeakerLevelValid = true;
              LOGID("cAudioParameters::parseMsg: minSpeakerLevel = %d\n",
                    mMinSpeakerLevel);
            }
        }
      if (pVolumeP != nullptr)
        {
          IF_VERIFY_INT(pVolumeP)
            {
              mVolume = pVolumeP->GetInt();
              mVolumeValid = true;
              LOGID("cAudioParameters::parseMsg: volume = %d\n",
                    mVolume);
            }
        }
      if (pMuteP != nullptr)
        {
          IF_VERIFY_BOOL(pMuteP)
            {
              mMute = pMuteP->GetBool();
              mMuteValid = true;
              LOGID("cAudioParameters::parseMsg: mute = %d\n",
                    mMute);
            }
        }
      if (pNoiseSuppressionLevelP != nullptr)
        {
          IF_VERIFY_INT(pNoiseSuppressionLevelP)
            {
              mNoiseSuppressionLevel = pNoiseSuppressionLevelP->GetInt();
              mNoiseSuppressionLevelValid = true;
              LOGID("cAudioParameters::parseMsg: enableNoiseSuppresion = %s\n",
                    (mEnableNoiseSuppression) ? "true" : "false");
            }
        }
      if (pEnableDTDP != nullptr)
        {
          IF_VERIFY_BOOL(pEnableDTDP)
            {
              mEnableDTD = pEnableDTDP->GetBool();
              mEnableDTDValid = true;
              LOGID("cAudioParameters::parseMsg: enableDTD = %s\n",
                    ((mEnableDTD) ? "true" : "false"));
            }
        }
      parsed = true;
    }
  else if (isProperMsg)
    {
      LOGW("cAudioParameters::parseMsg: Bad AudioParamters message \"%s\"\n",
           pcMsg);
    }
  else
    {
      LOGW("cAudioParameters::parseMsg: Not a AudioParamters message \"%s\"\n",
           pcMsg);
    }

  return parsed;
}

/** Construct a JSON message in the supplied buffer for these args.
 *
 * @param pMsgBuf is the buffer to fill with the JSON message.
 * @param bufLen is the maximum capacity of the buffer.
 *
 * @return true if the message fits in the provided buffer.
 */
bool cAudioParameters::buildMsg(char *pMsgBuf, int bufLen)
{
  bool retVal = false;

  // Create document and load our document model
  Document doc;
  doc.Parse(pcAudioParamsDOM);

  Value *pObjectP = Pointer(AUDIO_PARAMETERS_OBJECT_PTR).Get(doc);
  assert(pObjectP != nullptr);

  // Verify existence of the 'cmd' member under the 'audioParameters' member
  if (Value *pCmdP = Pointer(AUDIO_PARAMETERS_CMD_PTR).Get(doc))
    {
      // Exists, so set it
      pCmdP->SetString(mCmd.toString(), doc.GetAllocator());
      // Set the fields
      if (mMinSpeakerLevelValid)
        {
          Value *pNoiseThresholdP =
              Pointer(AUDIO_PARAMETERS_NOISE_LEVEL_THRESHOLD_PTR).Get(doc);
          pNoiseThresholdP->SetInt(mMinSpeakerLevel);
        }
      else
        {
          pObjectP->RemoveMember(AUDIO_PARAMETERS_NOISE_LEVEL_THRESHOLD_STR);
        }
      if (mVolumeValid)
        {
          Value *pVolumeP =
              Pointer(AUDIO_PARAMETERS_VOLUME_PTR).Get(doc);
          pVolumeP->SetInt(mVolume);
        }
      else
        {
          pObjectP->RemoveMember(AUDIO_PARAMETERS_VOLUME_STR);
        }
      if (mMuteValid)
        {
          Value *pMuteP =
              Pointer(AUDIO_PARAMETERS_MUTE_PTR).Get(doc);
          pMuteP->SetBool(mMute);
        }
      else
        {
          pObjectP->RemoveMember(AUDIO_PARAMETERS_MUTE_STR);
        }
      if (mNoiseSuppressionLevelValid)
        {
          Value *pNoiseSuppressionLevelP =
              Pointer(AUDIO_PARAMETERS_NOISE_SUPPRESSION_LEVEL_PTR).Get(doc);
          pNoiseSuppressionLevelP->SetInt(mNoiseSuppressionLevel);
        }
      else
        {
          pObjectP->RemoveMember(AUDIO_PARAMETERS_NOISE_SUPPRESSION_LEVEL_STR);
        }
      if (mEnableDTDValid)
        {
          Value *pEnableDTDP =
              Pointer(AUDIO_PARAMETERS_ENABLE_DTD_PTR).Get(doc);
          pEnableDTDP->SetBool(mEnableDTD);
        }
      else
        {
          pObjectP->RemoveMember(AUDIO_PARAMETERS_ENABLE_DTD_STR);
        }
    }

  // Create stream writer to access doc as a string
  StringBuffer buf;
  Writer<StringBuffer> writer(buf);
  doc.Accept(writer);
  const char *pinnedAoiMsg = buf.GetString();

  // As long as it will fit, copy the JSON into the supplied buffer
  if (strlen(pinnedAoiMsg) <= bufLen)
    {
      strncpy(pMsgBuf, pinnedAoiMsg, bufLen);
      retVal = true;
    }

  return retVal;
}


