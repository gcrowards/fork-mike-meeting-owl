#include "opencv2/imgproc/imgproc.hpp"

#include "aspectMath.hpp"
#include "circleMath.hpp"
#include "debugSwitches.h"
#include "logging.hpp"
#include "params.h"
#include "subframe.hpp"

#include <cmath>

#define DEFAULT_ANIMATION_TIME  1.0f                     // s
#define DEFAULT_FAST_PAN_TIME   0.5f                     // s
#define DEFAULT_SLOW_PAN_TIME   60.f                     // s

// initalize static member(s)
eAnimationMode_t cSubframe::sAnimationMode = eAnimationMode_t::SLIDING;


/** @param focus is a OpenCV point that specifies the pixel coordinate in the
 * image source that is to be used as the nominal center point of the subframe.
 * @param size is an OpenCV size that specifies the minimum required size (in
 * pixels) of the image source that can be used for the size of the subframe.
 * @param isPanorama indicates that the subframe is the panoramic strip.
 */
cSubframe::cSubframe(cv::Point focus, cv::Size size, bool isPanorama)
{
  mIsPanorama = isPanorama;
  init();
  setFocus(focus);
  setMinReqSize(size);
}

cSubframe::cSubframe(bool isPanorama)
{
  mIsPanorama = isPanorama;
  init();
}

cSubframe::~cSubframe()
{
}

/** Common code for constructors. */
void cSubframe::init(void)
{
  mAnimationEnabled = false;
  mDisplayState = STATIC;
  mTerminated = false;
  mLocked = false;
  mInLayout = false;
  mNewAnimation = false;
  mPosAnimationTime =   DEFAULT_ANIMATION_TIME;
  mSizeAnimationTime =  DEFAULT_ANIMATION_TIME;
  mFocusAnimationTime = DEFAULT_FAST_PAN_TIME;
  mMinReqSize = cv::Size(0., 0.);
  mNewMinReqSize = false;

  mPos   = (cv::Point2f)mEndPos   = cv::Point(0, 0);
  mSize  = (cv::Size2f) mEndSize  = cv::Size (0, 0);
  mFocus = (cv::Point2f)mEndFocus = cv::Point(0, 0);

  mVelPos   = cv::Point(0., 0.);
  mVelSize  = cv::Point(0., 0.);
  mVelFocus = cv::Point(0., 0.);

  mPanel = owl::cPanel(0, 0, 0, 0, 0, 0, 0, 0);
  mMovingRight =   true;  //
  mGrowing =       true;  // just to give a known default state
  mFocusingRight = true;  //

  mpPano = NULL;
  mMaxRadius = aspectMath::getMaxRadius();
}

/** This function cycles to the next available base animation mode, as defined
 * by the enum in the header file.
 */
void cSubframe::nextAnimationMode(void)
{
  sAnimationMode = (eAnimationMode_t)(sAnimationMode + 1);

  if(sAnimationMode >= eAnimationMode_t::ANIMATION_ENUM_END)
    {sAnimationMode = eAnimationMode_t::ANIMATION_ENUM_BEGIN + 1;}
}

/** This function ticks the subframe to take a step in its animation squence.
 * 
 * Call this function at regular intervals (more or less) to make the subfraame
 * update its size and position (within the coordinate system of the Layout). 
 * The details of the animation set is dictated by the end conditions that it
 * is targeting, and how much time it was told it had to reach them (as set via
 * other cSubframe functions).
 */
cv::Rect cSubframe::animate(void)
{
  if(mAnimationEnabled)
    {      
      struct timespec nowTimespec;

      if(mNewAnimation)
	{
	  // prime for new animation sequence
	  currentTimeSpec(&mPrevAnimationTime);
	  mNewAnimation = false;
	}

      // calc time step & spatial adders, and apply
      double dt = std::abs(elapsedTimeSinceSec(mPrevAnimationTime));

      // reset for next frame
      currentTimeSpec(&mPrevAnimationTime);

      if(DEBUG(SUBFRAME)) {
	LOGI("dt = %f seconds\n", dt); }

      float moveDist = mVelPos.x * dt;
      float growDist = mVelSize.x * dt;
      float focusDist = mVelFocus.x * dt;

      // animate subframe based on user-selected mode and the display state
      // as set by the Layout/Visual Editor
      if(sAnimationMode == eAnimationMode_t::STRAIGHT_CUT)
	{ // Straight-Cut Mode
	  if(mDisplayState == ADJUSTING)
	    { animateSliding(moveDist, growDist, focusDist); }
	  else
	    { animateStraightCut(focusDist); }
	}
      else if(sAnimationMode ==  eAnimationMode_t::SLIDING)
	{ // Sliding Mode
	  if(mDisplayState == STAGE_SWAP)
	    { animateStraightCut(focusDist); }
	  else
	    { animateSliding(moveDist, growDist, focusDist); }
	}
      
      if(DEBUG(SUBFRAME)) {
	LOGI("Pos.x: %3.1f/%d\n", mPos.x, mEndPos.x); }
      
      // test for completion/cap results
      bool isDoneTranslating = false;
      bool isDoneScaling = false;
      bool isDoneFocusing = false;

      if((mMovingRight && (mPos.x >= mEndPos.x)) ||
	 (mMovingLeft && (mPos.x <= mEndPos.x)) )
	{
	  isDoneTranslating = true;
	  mPos.x = mEndPos.x;
	  
	  if(DEBUG(SUBFRAME)) {
	    LOGI("Done translating, mPos.x = %3.1f\n", mPos.x); }
	}
      
      if((mGrowing && (mSize.width >= mEndSize.width)) ||
	 (mShrinking && (mSize.width <= mEndSize.width)) )
	{
	  isDoneScaling = true;
	  mSize.width = (float)mEndSize.width;

	  if(DEBUG(SUBFRAME)) {
	    LOGI("Done growing, mSize.width = %3.1f\n", mSize.width); }
	}

      // Calculates the most efficient approach direction for mFocus.x to get to
      // mEndFocus.x taking into account the wrap-around
      int focusingDirection = circleMath::intDiff(mEndFocus.x, (int)mFocus.x,
						  CV_PANO_RES_X );
      // will be positive if mFocus.x should approach mEndFocus.x from the left
      // will be negative if mFocus.x should approach mEndFocus.x from the right
      // will be zero if mFocus.x equals mEndFocus.x
      
      if((mFocusingRight && (focusingDirection <= 0)) ||
	 (mFocusingLeft && (focusingDirection >= 0)) )
	{ // Checks if the focusing direction required for mFocus.x to approach
	  // mEndFocus.x most efficiently has changed. If so, focusing is done.
	  isDoneFocusing = true;
	  mFocus.x = (float)mEndFocus.x;

	  if(DEBUG(SUBFRAME)) {
	    LOGI("Done focusing, mFocus.x = %3.1f\n", mFocus.x); }
	}
      
      if(isDoneTranslating && isDoneScaling && isDoneFocusing)
	{
	  disableAnimation();
	}
      
      // update region-of-interest rectangle
      mRoi.x = (int)(mFocus.x - (mSize.width / 2.));
      mRoi.y = 0; // <--adjust this # to crop from not-the-top of the raw image
      mRoi.width = (int)mSize.width;
      mRoi.height = (int)mSize.height;

      // update panel parameters
      setPanel();
    }

  return mRoi;
}

/** Calling this function is prevent the Subframe from animating (even if 
    animate() is called. */
void cSubframe::disableAnimation(void)
{
  mAnimationEnabled = false;
  mDisplayState = STATIC;

  // ensures that panel is up-to-date 
  setPanel();
}

/** Calling this function allows the Subframe to animate when animate() is 
    called. */
void cSubframe::enableAnimation(void)
{
  if(!mAnimationEnabled)
    {
      mAnimationEnabled = true;
      mNewAnimation = true;
    }
}

/** Return flag indicating the Subframe's boolean animation state. */
bool cSubframe::isAnimating(void)
{
  return mAnimationEnabled;
}

/** Calling this function sets the subframe up to be removed from cLayout.
 *
 * A terminated Subframe is one that is marked as ready to be removed from the
 * Layout, but setting it to "terminated" does not mean that the Subframe is
 * immediately destroyed.  
 *
 * For example, a Layout may wish to remove a Subframe and therefore mark it as 
 * terminated, but if that subframe is still in the process of animating (say, 
 * so that it can slide off the screen gracefully), then the Subframe will 
 * persist until it is done animating.  Then it will be destoryed.
 */
void cSubframe::terminate(void)
{
  mTerminated = true;
}

/** Return flag indicated the Subframe's boolean termination state. */
bool cSubframe::isTerminated(void)
{
  return mTerminated;
}

/** Change the point in the source image that is the center of the Subframe &
 * use current animation speed to get there.
 *
 * Externally facing convenience function to access interal adjustFocusCore().
 *
 * @param focus is an OpenCV point that holds the pixel coordinate of where in
 * the source image the Subframe should be built around.
 */
void cSubframe::adjustFocus(cv::Point focus)
{
  adjustFocusCore(focus);
}

/** Quickly change the point in the source image that is the center of the 
 * Subframe.
 *
 * Externally facing convenience function to access interal adjustFocusCore().
 *
 * @param focus is an OpenCV point that holds the pixel coordinate of where in
 * the source image the Subframe should be built around.
 */
void cSubframe::adjustFocusFast(cv::Point focus)
{
  mFocusAnimationTime = DEFAULT_FAST_PAN_TIME;
  adjustFocusCore(focus);
}

/** Slowly change the point in the source image that is the center of the 
 * Subframe.
 *
 * Externally facing convenience function to access interal adjustFocusCore().
 *
 * @param focus is an OpenCV point that holds the pixel coordinate of where in
 * the source image the Subframe should be built around.
 */
void cSubframe::adjustFocusSlow(cv::Point focus)
{
  mFocusAnimationTime = DEFAULT_SLOW_PAN_TIME;
  adjustFocusCore(focus);
}

/** Make the last good focus point for subframe the current focus point;
 *
 * This is the last focus point that could be used by cLayout to generate a valid
 * layout (one with no overlapping views).
 *
 * @param focus is a cv::Point that holds the (x, y) pixel coordinate that is
 * at the center of the valid subframe.
 */
void cSubframe::updateLastGoodFocus(void)
{
  mLastGoodFocus = mFocus;
}

/** Change Subframe's focus point back to the last focus point that resulted in
 * a valid Stage layout (i.e. one with no overlapping subframes).
 */
void cSubframe::revertFocus(void)
{
  adjustFocus(mLastGoodFocus);
}

/** Sets this subframe's state on the Stage, as requested, if the subframe is
 * not in the middle of another state transition.
 * 
 * The state (e.g. subframe is being added, subframe is having its size 
 * adjusted etc.) is needed because different animation may be required for 
 * different states.
 *
 * @param displayState is the new state.
 */
void cSubframe::setDisplayState(eDisplayState_t displayState)
{
  // This test prevents changing the state while an animation is currently in
  // progress
  if(mDisplayState == STATIC)
    {
      mDisplayState = displayState;
    }
}

/** Immediately sets this subframe's state on the Stage, as requested.
 * 
 * The state (e.g. subframe is being added, subframe is having its size 
 * adjusted, etc.) is needed because different animation may be required for 
 * different states.
 *
 * @param displayState is the new state.
 */
void cSubframe::setDisplayStateNow(eDisplayState_t displayState)
{
  mDisplayState = displayState;
}

/** This function indicates if this subframe is allowed to have its size/pos
 * adjusted (e.g. by the cLayout).
 *
 * @param state is the true/false value to set mLocked to.  If no state is
 * provided the default value of "false" is used.
 */
void cSubframe::setLocked(bool state)
{
  mLocked = state;
}

/** Assign the panorama source for the cVisualEditor.
 *
 * @param pPano is a pointer to a cPanorama that will be used as the
 * image source for the cVisualEditor. 
 */
void cSubframe::setPanorama(cPanorama *pPano)
{
  mpPano = pPano;
}

/** Assign the pixel location of the Subframe (within the source image).
 *
 *  @param pos is an OpenCV Point that contains center pixel coordinate for 
 *  the rectangle that defines the Subframe. 
 */
void cSubframe::setPos(cv::Point pos)
{
  if(mLocked) { return; }

  mPos = (cv::Point2f)pos;
  setEndPos(pos);
}

/** Assign the final pixel location of the Subframe (within the source image)
 * that should be reached by the time animation of the Subframe is complete.
 *
 * The velocity of the animation is updated in this function based on the new
 * end position and the previously set animation time.
 *
 * @param pos is an OpenCV Point that contrains the center pixel coordinate for
 * the rectange that defines the Subframe.
 */
void cSubframe::setEndPos(cv::Point pos)
{
  if(mLocked) { return; }

  mEndPos = pos;

  mVelPos.x = (mEndPos.x - mPos.x) / mPosAnimationTime;
  mVelPos.y = (mEndPos.y - mPos.y) / mPosAnimationTime;

  // TODO: It's lame that this is only dependent on the x-axis...
  if(mVelPos.x >= 0)
    {
      mMovingRight = true;
      mMovingLeft = false;
    }
  else
    {
      mMovingLeft = true;
      mMovingRight = false;
    }
}

/** Assign the size of the Subframe.
 *
 * @param size is an OpenCV Size that sets the size of the rectangle that 
 * defines the Subframe's boundaries. 
 */
void cSubframe::setSize(cv::Size size)
{
  if(mLocked) { return; }

  mSize = (cv::Size2f)size;
  setEndSize(size);
}

/** Assign the final size of the rectangle that defines the boundaries of the
 * Subframe, which should be reached by the time animation of the Subframe is 
 * complete.
 *
 * The velocity of the animation is updated in this function based on the new
 * end size and the previously set animation time.
 *
 * @param size is an OpenCV Size that that sets the size of the rectangle that 
 * defines the Subframe's boundaries. 
 */
void cSubframe::setEndSize(cv::Size size)
{
  if(mLocked) { return; }

  mEndSize = size;

  mVelSize.x = (mEndSize.width - mSize.width) / mSizeAnimationTime;
  mVelSize.y = (mEndSize.height - mSize.height) / mSizeAnimationTime;
  
  // TODO: It's lame that this is only dependent on the x-axis...
  if(mVelSize.x >= 0.)
    {
      mGrowing = true;
      mShrinking = false;
    }
  else
    {
      mShrinking = true;
      mGrowing = false;
    }

  // reset flag, since we've now updated based off new end size
  mNewMinReqSize = false;
}

/** Set new focus point for subframe.
 *
 * The focus is the point (in the unrolled image) that the subframe is centered
 * on.
 *
 * @param focus is a cv::Point that holds the (x, y) pixel coordinate that is
 * at the center of the subframe.
 */
void cSubframe::setFocus(cv::Point focus)
{
  mFocus = (cv::Point2f)focus;
  adjustFocusCore(focus);
}

/** This function lets the Subframe know the minimum size (in pixels) that is
 * necessary in order to capture what the attention system wants captured.
 *
 * Subframe doesn't actually do anything with this information, it just holds
 * onto it in case someone using the Subframe wants to know. Currently, this is
 * used by the Layout & cAreaOfInterest as a way of negotiating the end size of
 * a subframe (which due to limited screen space may not actually be able to be
 * the ideal size).
 *
 * @param minReqSize is an OpenCV Size that specifies the minimum necessary
 * boundaries that should be used for the subframe.
 */
void cSubframe::setMinReqSize(cv::Size minReqSize)
{
  if(minReqSize != mMinReqSize)
    {
      // keep subframe's size within bounds of display
      const int kMaxSubframeWidth = OUTPUT_WIDTH - SUBFRAME_BORDER_MARGIN;

      if(minReqSize.width > kMaxSubframeWidth)
	{minReqSize.width = kMaxSubframeWidth;}
      // TODO: Add height check

      mMinReqSize = minReqSize;
      mNewMinReqSize = true;
    }
}

/** This function lets the Subframe know how long it has to animate from its
 * current state to the elsewhere-specified end state.  
 *
 * Note: This function sets all animation times to the given value.
 *
 * @param time is the time in seconds that the subframe has to complete its
 * animation.
 */
void cSubframe::setAnimationTime(float time)
{
  mPosAnimationTime   = time;
  mSizeAnimationTime  = time;
  mFocusAnimationTime = time;
}

/** Set flag indicating whether or not the Subframe is part of the cLayout.
 *
 * @param inLayout is a flag indicating whether or not the Subrame is part of
 * the Layout.
 */ 
void cSubframe::setInLayout(bool inLayout)
{
  mInLayout = inLayout;
}

/** Sets the the nominal style of animation.
 *
 * The animation mode is a shared variable that enables all subframes to use the
 * same nomimal style when animating.
 *
 * @param animationMode is the new nominal animation style for all subframes.
 */
void cSubframe::setAnimationMode(eAnimationMode_t animationMode)
{
  if (((int)animationMode > (int)eAnimationMode_t::ANIMATION_ENUM_BEGIN) &&
      ((int)animationMode < (int)eAnimationMode_t::ANIMATION_ENUM_END))
    {
      sAnimationMode = animationMode;
    }
  else
    {
      LOGE("cSubframe::setAnimationMode: Illegal mode %d\n", animationMode);
    }
}

/** This function updates the panel values based the state set in the animation
 * function.
 *
 * The panel contains all the geometric information needed to copy a slice of
 * the input source image into the appropriate location in the output 
 * composite image.
 */
void cSubframe::setPanel(void)
{
  float phi;
  float theta0, theta1;
  float rInner, rOuter;
  const float k2Pi = M_PI * 2.0;
  const float kRadToDeg = 180.0 / M_PI;
  const float kDegToRad = M_PI / 180.0;
  const float kPhiMin = CAMERA_LENS_MIN_PHI_DEG * kDegToRad;

  // make sure the panorama is ready
  if(mpPano == NULL) { return; }
  
  // set the elevation angle, phi
  // Setting phi to the minimum will make computeRadiiFromHFovPhi() choose the
  // maximum allowed value for the the outer radius
  phi = kPhiMin;
  
  // convert focus from pixels to degrees; adjust for camera alignment
  float focusThetaRaw = mpPano->angleAt(mFocus.x);
  float focusThetaAlign = focusThetaRaw + CAMERA_ALIGNMENT_ANGLE;
  if(focusThetaAlign >= 360)
  { focusThetaAlign -= 360; }

  if (mIsPanorama)
    {
      // use full horizontal FOV
	  theta0 = kDegToRad * (focusThetaAlign - 180);
	  theta1 = kDegToRad * (focusThetaAlign + 180);
      // compute the proper radii, accounting for aspect ratio
      aspectMath::computeRadiiFromHFovPhi(&rInner, &rOuter, mMaxRadius,
					  k2Pi, phi,
					  mSize.width, mSize.height);
    }
  else
    {
      // convert panel width from pixels to degrees
      float dTheta = stagePixToDeg(mSize.width);
      // set horizontal FOV
      theta0 = kDegToRad * (focusThetaAlign - dTheta / 2);
      theta1 = kDegToRad * (focusThetaAlign + dTheta / 2);
      // compute the proper radii, accounting for aspect ratio
      aspectMath::computeRadiiFromHFovPhi(&rInner, &rOuter, mMaxRadius,
					  kDegToRad * dTheta, phi,
					  mSize.width, mSize.height);
    }

  // set angles and radii
  mPanel.theta0 = theta0;
  mPanel.theta1 = theta1;
  mPanel.radiusInner = rInner;
  mPanel.radiusOuter = rOuter;
  
  // set panel location
  mPanel.posX0 = (int)(mPos.x - (mSize.width / 2));
  mPanel.posY0 = (int)mPos.y;
  mPanel.posX1 = (int)(mPos.x + (mSize.width / 2));
  mPanel.posY1 = (int)(mPos.y + mSize.height);
}

/** Change the point in the source image that is the center of the Subframe.
 *
 * When the focus is changed via this function animation is automatically 
 * enabled so that the Subframe can move to the assigned position.  Velocities
 * for this animation are automatically calculated based on the Subframe's
 * exisiting mFocusAnimationTime parameter.
 *
 * @param focus is an OpenCV point that holds the pixel coordinate of where in
 * the source image the Subframe should be built around.
 */
void cSubframe::adjustFocusCore(cv::Point focus)
{
  if(mEndFocus != focus)
  {
    mEndFocus = focus;
  
    mVelFocus.x = (circleMath::floatDiff(mEndFocus.x, mFocus.x, CV_PANO_RES_X) / mFocusAnimationTime);
  
    if(mVelFocus.x >= 0.)
    {
      mFocusingRight = true;
      mFocusingLeft = false;
    }
    else
    {
      mFocusingLeft = true;
      mFocusingRight = false;
    }
  
    mDisplayState = PANNING;

    enableAnimation();
  }
}

/** This function updates the subframe's position & size.
 *
 * Position & size are set directly to end state to create an immediate display
 * of the final state.
 *
 * @param focusDist is the amount to change the focal point of  the center of
 * the subframe by.
 */
void cSubframe::animateStraightCut(float focusDist)
{
  mPos.x = mEndPos.x;
  mSize.width = mEndSize.width;
  mFocus.x = circleMath::floatAdd(mFocus.x, focusDist, CV_PANO_RES_X);
}

/** This fucntion updates the subframe's position, size, & focus.
 *
 * Position, size, and focus are adjusted incrementally based on the values of
 * the given arguments.  Over time, this creates a sliding expanding/contracting
 * effect.
 *
 * @param moveDist is the amount to change the position of the subframe by.
 * @param growDist is the amount to change the width of the subframe by.
 * @param focusDist is the amount to change the focal point of  the center of
 * the subframe by.
 */
void cSubframe::animateSliding(float moveDist, float growDist, float focusDist)
{
  mPos.x += moveDist;
  mSize.width += growDist;
  mFocus.x = circleMath::floatAdd(mFocus.x, focusDist, CV_PANO_RES_X);
}

/** Return true if the end size (width only, right now) is smaller than the 
    required size. */
bool cSubframe::isTooSmall(void)
{
  if(mEndSize.width < mMinReqSize.width)
    {return true;}
  
  return false;
}

/** Return true if the minimum-required-size has changed since the last time
    the end-size was updated. */
bool cSubframe::isMinReqSizeChanged(void)
{
  return mNewMinReqSize;
}

/** Return the state of the subframe on the Stage (e.g. if it's being added to
 * the stage. */
cSubframe::eDisplayState_t cSubframe::getDisplayState(void)
{
  return mDisplayState;
}

/** Return whether or not this subframe is allowed to have it's size/pos 
    modified. */
bool cSubframe::getLocked(void)
{
  return mLocked;
}

/** Return an OpenCV rectangle that countains the boundaries of Subframe. */ 
cv::Rect cSubframe::getRoi(void)
{
  return mRoi;
}

/** Return an OpenCV Point containing the pixel coordinates of the center
    corner of the rectangle that defines the Subframe. */
cv::Point cSubframe::getPos(void)
{
  return (cv::Point2i)mPos;
}

/** Return the OpenCV Point that holds the pixel coordinates of where the 
    Subframe rectangle will end when animation is complete. */
cv::Point cSubframe::getEndPos(void)
{
  return mEndPos;
}

/** Return the OpenCV Size that holds the current dimensions of the rectangle
    that defines the boundaries of the Subframe. */
cv::Size cSubframe::getSize(void)
{
  return (cv::Size2i)mSize;
}

/** Return the OpenCV size that holds the final dimensions of the rectnagle
    that will define the boundaries of the Subframe when animation is complete.*/
cv::Size cSubframe::getEndSize(void)
{
  return mEndSize;
}

/** Return the OpenCV Size that contains the minimum size of the subframe (in
 * pixels) necessary to capture what the attention system wants captured. */
cv::Size cSubframe::getMinReqSize(void)
{
  return mMinReqSize;
}

/** Return the (x, y) coordinates of the top-left corner of the subframe in
    the coordinate system of the cLayout. */
cv::Point cSubframe::getPosInLayout(void)
{
  cv::Point posInLayout = (cv::Point2i)mPos;

  posInLayout.x = (int)(mPos.x - (mSize.width / 2.));

  return posInLayout;
}

/** Return the nominal animation mode of all subframes (this is a static
 * variable!) */
eAnimationMode_t cSubframe::getAnimationMode(void)
{
  return sAnimationMode;
}

/** Return the OpenCV Point that defines the pixel coordinate in the input
    image's frame of reference that is the center of the Subframe. */
cv::Point cSubframe::getFocus(void)
{
  cv::Point2i focus = (cv::Point2i)mFocus;
  
  return focus;
}

/** Returns current focus X wrt pano focus and any shift thereof.
 *
 */
int cSubframe::getFocusXWrtPano(void)
{
  int focusX = fmod(mFocus.x + (CV_PANO_RES_X>>1) - mpPano->getFocusX(), CV_PANO_RES_X);
  if(focusX < 0)              { focusX += CV_PANO_RES_X; }
  if(focusX >= CV_PANO_RES_X) { focusX = 0;              }

  return focusX;
}

/* Return the panel that defines the input & output geometries needed to apply
   the subframe's underlying input image to the output composite. */
owl::cPanel cSubframe::getPanel(void)
{
  return mPanel;
}

/** Return the flag that indicates whether the Subframe is part the cLayout or
    not. */
bool cSubframe::isInLayout(void)
{
  return mInLayout;
}
