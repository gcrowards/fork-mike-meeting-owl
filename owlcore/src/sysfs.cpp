#include <sys/resource.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <unistd.h>

#include "logging.hpp"
#include "sysfs.hpp"
#include "params.h"


cSysfs::cSysfs()
{
  // initialize variables
  mMicMute = mMicVolume = SYSFS_BAD_VALUE;
  mMute = mVolume = SYSFS_BAD_VALUE;

  // initialize mutex and start thread
  if (pthread_mutex_init(&mMutex, NULL) != 0)
    { LOGE("unable to init sysfs mutex\n"); }
  start();
}

cSysfs::~cSysfs()
{
  // stop thread and destroy mutex
  join();
  pthread_mutex_destroy(&mMutex);
}

/** This is the PRIVATE function that reads and returns a sysfs
 * variable value.
 *
 * @param pValueName is a pointer to the variable name; SYSFS_PATH
 * will be preprended to get the full path for the sysfs node.
 * @return the variable value, currently expected to be in [0,100]
 * under normal conditions; the return value is SYSFS_BAD_VALUE
 * (currently -1) if something goes wrong.
 */
int  cSysfs::getValue(const char *pValueName)
{
  int  value, ret;
  char pFilepath[256];
  FILE *fp;

  // open the filepath for the specific sysfs variable
  sprintf(pFilepath, "%s/%s", SYSFS_PATH, pValueName);
  fp = fopen(pFilepath, "r");
  // read the value, setting SYSFS_BAD_VALUE if anything goes wrong
  if (fp == NULL)
    { value = SYSFS_BAD_VALUE; }
  else
    {
      ret = fscanf(fp, "%d", &value);
      if (ret != 1)
	{ value = SYSFS_BAD_VALUE; }
      fclose(fp);
    }
  return value;
}

/** @return the mic mute value. */
int  cSysfs::getMicMute(void)
{
  // return the most recently polled value; see cSysfs::update()
  return mMicMute;
}

/** @return the mic volume value. */
int  cSysfs::getMicVolume(void)
{
  // return the most recently polled value; see cSysfs::update()
  return mMicVolume;
}

/** @return the loudspeaker mute value. */
int  cSysfs::getMute(void)
{
  // return the most recently polled value; see cSysfs::update()
  return mMute;
}

/** @return the loudspeaker volume value. */
int  cSysfs::getVolume(void)
{
  // return the most recently polled value; see cSysfs::update()
  return mVolume;
}

/** This function waits for the thread to exit. */
void cSysfs::join(void)
{
  mKeepThreading = false;
  pthread_join(mThread, NULL);
}

/** This is the PRIVATE function writes a sysfs variable value.
 *
 * @param pValueName is a pointer to the variable name; SYSFS_PATH
 * will be preprended to get the full path for the sysfs node.
 * @param value is the value to write, currently expected to be in
 * [0,100].
 * @return a subsequent read of the value to confirm it was set; the
 * return value will be SYSFS_BAD_VALUE (currently -1) if something
 * goes wrong.
 */
int  cSysfs::setValue(const char *pValueName, int value)
{
  int  ret;
  char pFilepath[256];
  FILE *fp;

  // open the filepath for the specific sysfs variable
  sprintf(pFilepath, "%s/%s", SYSFS_PATH, pValueName);
  fp = fopen(pFilepath, "w");
  // write the value, setting SYSFS_BAD_VALUE if we can't open the file
  if (fp == NULL)
    { ret = SYSFS_BAD_VALUE; }
  else
    {
      ret = fprintf(fp, "%d", value);
      fclose(fp);
    }
  // validate the write, and set the corresponding return value
  if (ret <= 0)
    { ret = SYSFS_BAD_VALUE; }      // ensure consistent bad return value
  else
    { ret = getValue(pValueName); } // read back the written value
  return ret;
}

/** This is the PUBLIC function to set the mic mute value.
 *
 * @param value is the mute value to set, expected to be 0 or 1.
 * @return a subsequent read of the set value.
 */
int  cSysfs::setMicMute(int value)
{
  // set value; assign member variable w/o waiting for a poll
  mMicMute = setValue("mic_mute", value);
  return mMicMute;
}

/** This is the PUBLIC function to set the mic volume value.
 *
 * @param value is the volume value to set, expected to be in [0,100].
 * @return a subsequent read of the set value.
 */
int  cSysfs::setMicVolume(int value)
{
  // set value; assign member variable w/o waiting for a poll
  mMicVolume = setValue("mic_volume", value);
  return mMicVolume;
}

/** This is the PUBLIC function to set the loudspeaker mute value.
 *
 * @param value is the mute value to set, expected to be 0 or 1.
 * @return a subsequent read of the set value.
 */
int  cSysfs::setMute(int value)
{
  // set value; assign member variable w/o waiting for a poll
  mMute = setValue("mute", value);
  return mMute;
}

/** This is the PUBLIC function to set the loudspeaker volume value.
 *
 * @param value is the volume value to set, expected to be in [0,100].
 * @return a subsequent read of the set value.
 */
int  cSysfs::setVolume(int value)
{
  // set value; assign member variable w/o waiting for a poll
  mVolume = setValue("volume", value);
  return mVolume;
}

/** This function creates and starts the thread .*/
void cSysfs::start(void)
{
  mKeepThreading = true;
  if (pthread_create(&mThread, NULL, threadWrapper, this) != 0)
    { LOGE("unable to create sysfs thread\n"); }
  else
    { pthread_setname_np(mThread, "cSysfs"); }
}

/** This function implements the thread update loop. */
void cSysfs::threadUpdate(void)
{
  pid_t tid = syscall(SYS_gettid);
  int ret = setpriority(PRIO_PROCESS, tid, DEFAULT_THREAD_PRIORITY);
  LOGV("THREADING ==> sysfs thread has id %d and priority %d\n",
       tid, getpriority(PRIO_PROCESS, tid));
  while (mKeepThreading)
    {
      update();
      usleep(SYSFS_THREAD_USLEEP);
    }
  LOGV("THREADING ==> sysfs thread exiting\n");
}

/** This function provides a wrapper that's compatible with pthread_create(). */
void *cSysfs::threadWrapper(void *pContext)
{
  reinterpret_cast<cSysfs *>  (pContext)->threadUpdate();
  return (void *)NULL;
}

/** This function performs polling of the sysfs node. */
void cSysfs::update(void)
{
  // poll the values
  mMicMute     = getValue("mic_mute");
  mMicVolume   = getValue("mic_volume");
  mMute        = getValue("mute");
  mVolume      = getValue("volume");
}
