#include <sys/resource.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <unistd.h>

#include "common.hpp"
#include "debugSwitches.h"
#include "circleMath.hpp"
#include "logging.hpp"
#include "motionMapper.hpp"
#include "params.h"

#include <math.h>
#include <vector>

#define ANGULAR_RESOLUTION_DEG   20
#define MOTION_TEST_PERIOD_S     0.5f

#define MAP_ADD_VAL             400
#define MAP_LEAK_VAL             20
#define MAP_SPREAD_VAL            8 // degrees

cMotionMapper::cMotionMapper(cPanorama *pPano)
{
  mpPano = pPano;

  // create a place to copy the mpPano gray image
  mGrayImage = cv::Mat(CV_PANO_RES_Y, CV_PANO_RES_X, CV_8UC1);

  setupDetectors(ANGULAR_RESOLUTION_DEG);

  mMap.setAddVal(MAP_ADD_VAL);
  mMap.setLeakVal(MAP_LEAK_VAL);
  mMap.setSpreadVal(MAP_SPREAD_VAL);
  // using default MIN and MAX values (0 and 1000)

  // initialize mutex and start thread
  if (pthread_mutex_init(&mMutex, NULL) != 0)
    { LOGE("unable to init motionMapper mutex\n"); }
  start();
}

cMotionMapper::~cMotionMapper()
{
  // stop thread and destroy mutex
  join();
  pthread_mutex_destroy(&mMutex);

  // delete motion detectors
  for(std::vector<cMotionDetector *>::iterator 
	iDetector = mpMotionDetectors.begin();
      iDetector != mpMotionDetectors.end(); ++iDetector )
    {
      delete (*iDetector);
    }
  mpMotionDetectors.clear();
  // log the final map
  mMap.logMap("final motion map:");
}

/** This function waits for the thread to exit. */
void cMotionMapper::join(void)
{
  mKeepThreading = false;
  pthread_join(mThread, NULL);
}

/** This function creates and starts the thread .*/
void cMotionMapper::start(void)
{
  mKeepThreading = true;
  if (pthread_create(&mThread, NULL, threadWrapper, this) != 0)
    { LOGE("unable to create motionMapper thread\n"); }
  else
    { pthread_setname_np(mThread, "cMotionMapper"); }
}

/** This function implements the thread update loop. */
void cMotionMapper::threadUpdate(void)
{
  pid_t tid = syscall(SYS_gettid);
  int ret = setpriority(PRIO_PROCESS, tid, DEFAULT_THREAD_PRIORITY);
  LOGV("THREADING ==> motionMapper thread has id %d and priority %d\n",
       tid, getpriority(PRIO_PROCESS, tid));
  while (mKeepThreading)
    {
      update();
      usleep(MOTIONMAPPER_THREAD_USLEEP);
    }
  LOGV("THREADING ==> motionMapper thread exiting\n");
}

/** This function provides a wrapper that's compatible with pthread_create(). */
void *cMotionMapper::threadWrapper(void *pContext)
{
  reinterpret_cast<cMotionMapper *>  (pContext)->threadUpdate();
  return((void *)NULL);
}

/** This is the main externally facing function that is called to trigger the
 * cMotionMapper to perform an analysis cycle.
 *
 * Each time this function is called the whole panoramic image is search for
 * motion (assuming the time delta required by the underlying motion detectors
 * has been reached).  There are many separate motion detectors spread across
 * the panoramic image so that we can determing, with adeqare angular resolution
 * which bearings have motion.
 */
void cMotionMapper::update(void)
{
  // bearings at which motion was detected
  std::vector<int> motionBearings;

  // get deep copy of gray image
  if (mpMotionDetectors[0]->isTimeToUpdate())
    {
      mpPano->copyGrayImage(&mGrayImage);

      // update motion detectors
      for(std::vector<cMotionDetector *>::iterator
            iDetector = mpMotionDetectors.begin();
          iDetector != mpMotionDetectors.end(); ++iDetector )
        {
          bool motion = (*iDetector)->update(&mGrayImage);

          if(motion)
            { // find center of detector and store corresponding world bearing
              cv::Rect roi = (*iDetector)->getRoi();

              int pixel = roi.x + roi.width / 2;

              motionBearings.push_back((int)mpPano->angleAt(pixel));
            }
        }

      // update mBroadMotion flag
      updateBroadMotion();
    }

  // update map regardless of if there was any detected motion
  pthread_mutex_lock(&mMutex);
  mMap.update(motionBearings);

  if(DEBUG(MOTION_MAPPER))
    { mMap.printMap(20); }
  pthread_mutex_unlock(&mMutex);
}

/** This function checks to see if a significant number of motion
 * detectors have an active state due to detected motion.
 *
 * The member variable mBroadMotion is set accordingly, with
 * checkForBroadMotion() as its getter.
 */
void cMotionMapper::updateBroadMotion(void)
{
  int numDetectorsActivated = 0;
  const float kThresholdPercent = 0.5;

  for(std::vector<cMotionDetector *>::iterator
	iDetector = mpMotionDetectors.begin();
      iDetector != mpMotionDetectors.end(); ++iDetector )
    {
      // checks if the current detector has detected motion
      if((*iDetector)->getState() == true)
	{
	  numDetectorsActivated++;
	}
    }

  // set flag if the broad motion threshold has been met
  pthread_mutex_lock(&mMutex);
  if(numDetectorsActivated >= (int)(mpMotionDetectors.size() * kThresholdPercent))
    { mBroadMotion = true; }
  else
    { mBroadMotion = false; }
  pthread_mutex_unlock(&mMutex);
}

bool cMotionMapper::checkForBroadMotion(void)
{
  pthread_mutex_lock(&mMutex);
  bool val = mBroadMotion;
  pthread_mutex_unlock(&mMutex);
  return val;
}

/** Resets all of the motion map's stored values to zero. */
void cMotionMapper::resetMap(void)
{
  pthread_mutex_lock(&mMutex);
  mMap.resetMap();
  pthread_mutex_unlock(&mMutex);
}

void cMotionMapper::setupDetectors(int angularResolution)
{
  const int kOverlapFactor = 2;

  // figure out how many detectors are needed
  int pixelsPerDetector = (int) (ceil( ( ( (angularResolution / 360.f)
					   * CV_PANO_RES_X) / 
					 2.f ) ) );
  int numDetectors = (int) (ceil( (float) CV_PANO_RES_X / pixelsPerDetector) );

  // create detectors
  for(int i = 0; i < numDetectors; i++)
    {
      const int kRoiTop = CV_PANO_RES_Y / 2;
      int roiLeft = i * pixelsPerDetector;
      int roiWidth = pixelsPerDetector;
      const int kRoiHeight = CV_PANO_RES_Y / 2;

      // trim (what should be) last detector so that it doesn't extend past
      // edge of image
      if(roiLeft + roiWidth > CV_PANO_RES_X)
	{roiWidth -= ((roiLeft + roiWidth) - CV_PANO_RES_X);}
      
      cv::Rect roi(roiLeft, kRoiTop, roiWidth, kRoiHeight);
      
      mpMotionDetectors.push_back(new cMotionDetector(MOTION_TEST_PERIOD_S,
						      roi ) );
    }
}

/** This function gets the value for teh given angle from the underlying map.
 *
 * @return map value at give angle.
 */ 
int cMotionMapper::getMapValAt(int angle)
{
  pthread_mutex_lock(&mMutex);
  int val = mMap.getValAt(angle);
  pthread_mutex_unlock(&mMutex);
  return val;
}

/** This function gets the maximum value within a range of bearings around the
 * given nominal angle.
 *
 * @return maximum map value near the given angle.
 */
int cMotionMapper::getAngleWithMaxVal(int nominalAngle, int plusMinus)
{
  pthread_mutex_lock(&mMutex);
  int val = mMap.getAngleWithMaxVal(nominalAngle, plusMinus);
  pthread_mutex_unlock(&mMutex);
  return val;
}

/** This function returns the map which is keeping track of the positional data
 * of the things found by the cMotionMapper.
 *
 * @return the map containing a vector of integers.
 */
cMap cMotionMapper::getMap(void)
{
  pthread_mutex_lock(&mMutex);
  cMap map = mMap;
  pthread_mutex_unlock(&mMutex);
  return map;
}
