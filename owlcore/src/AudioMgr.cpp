///< AudioMgr - manages the audio path through the application

#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>

#include "owlHook.hpp"

#include "AudioMgr.hpp"

#include "AudioEqualizer.hpp"
#include "AudioLevelManager.hpp"
#include "beamform.hpp"
#include "beamMath.hpp"
#include "common.hpp"
#include "logging.hpp"
#include "params.h"
#include "soundLocalizer.hpp"
#include "AudioPlayer.hpp"

#ifdef __ANDROID__
#include "speex_echo.h"
#include "speex_preprocess.h"
#endif

#define FLOAT float // required since owlHook.hpp undefs FLOAT
extern cOwlHook *gpOwlHook;

const char MinSpeakerLevelKey[] = "persist.mowl.audio.msa";
const char MinNRAmountKey[] = "persist.mowl.audio.nramount";
const char AecPostKey[] = "persist.mowl.audio.aecpost";
int isSpeakerActive = 0;

#ifndef __ANDROID__
#define AUDIO_SYSTEM_DEBUG false
#define ALM_DEBUG          false

unsigned long long getNsecs()
{
  return 0;
}
unsigned long long getMsecs()
{
  return 0;
}

int GetAudioProperties(AUDIO_PROPERTIES *props)
{
  props->audioCursor = 90;
  props->micLeftChan = 4;
  props->micRightChan = 6;
  props->enableEq = 1;
  props->enableBeam = 1;
  props->enableAEC = 1;
  props->enableNR  = 1;
  props->enableLM  = 1;
  props->aecDebug = 0;
  props->micMute = 0;
  props->enableMicProc = 1;
  props->localDebug = 0;
  props->localNum = 0;
  props->localLog = 0;
  props->localRatio = 0;

  return 0;
}
/** Returns the configuration of the audio framework. This version used for Linux
*
* @param pConfig - pointer to the structure that will hold the config
*
* @return 0
*/
int GetAudioConfig(AUDIO_CONFIG *pConfig)
{
  pConfig->procFrameCount = PROC_FRAME_SIZE;
  pConfig->micCount = 8;
  return 0;
}

/** STUB - copies the audio interface status structure to the input
*
* @param pAis - pointer to the destination of the status structure
*
* @return 0 if successful, -1 if not
*/
int AudioIfStatusGet(AUDIO_IF_STATUS *pAis)
{

  pAis->refAvail  = 0;
  return 0;
}

/** STUB - Provides access to the speaker level
*
* @return db of RMS calculation of last audio buffer sent to speaker
*/
double GetSpeakerRMSLevel()
{
  return -96;
}

/** STUB - see audioframework.c in libOwlPG for description */
int PollTimerInit(POLL_TIMER *timer, unsigned long long interval)
{
  return 0;
}
/** STUB - see audioframework.c in libOwlPG for description */
int PollTimerPollTimer(POLL_TIMER *timer)
{
  return 0;
}
/** STUB - see audioframework.c in libOwlPG for description */
int PollTimerPollTimerOnce(POLL_TIMER *timer)
{
  return 0;
}
/** STUB - see audioframework.c in libOwlPG for description */
int PollTimerReset(POLL_TIMER *timer, unsigned long long interval)
{
  return 0;
}

/** STUB - see audioframework.c in libOwlPG for description */
extern int GetUsbSpkrActive();
int AudioPropertiesSet(const char *key, int value)
{
  return 0;
  
}


#endif

/// db calculations

/** pow2db - Calculates the db value from a power value
*
* @param in - power level 0-1
*
* @return level in db relative to a fs of 1;
*/
FLOAT pow2db(FLOAT in)
{
  return (FLOAT)(10.0 * log10(in));
}

/** db2pow - Calculates the power associated with a db value
*
* @param in - db value to calculate
*
* @return returns calculate power
*/
FLOAT db2pow(FLOAT in)
{
  return FLOAT(pow(10, in / 10.0));
}

/** mag2db - Calculate the db value for a particular magnitude
*
* @param in - magnitude value
*
* @return db value
*/
FLOAT mag2db(FLOAT in)
{
  if(in < 1.0e-6)
  {return(-120.0);}
  else
  {return FLOAT(20 * log10(in));}
}

/** db2mag - Calculate the magnitude from a db value
*
* @param in - db value
*
* @return calculated magnitude
*/
FLOAT db2mag(FLOAT in)
{
  return (FLOAT)(pow(10, in / 20.0));
}


/*** Allows user application to set the active speaker minimum level
*
* @param minLevel - Minimum audio level to declare remote audio active;
*        range -35 to -50 db fs
*
* @return 0 if successful; -1 if not
*/
int AMSetMinSpeakerLevel(int minLevel)
{
  char levelString[64];
  
  if(minLevel < MIN_MIN_SPEAKER_LEVEL)
  {
    return -1;
  }
  if(minLevel > MAX_MIN_SPEAKER_LEVEL)
  {
    return -1;
  }
  
  AudioPropertiesSet(MinSpeakerLevelKey,minLevel);
  
  return 0;
}

/** Allows user application to get the active speaker minimum level
*
* @param minLevel - Reference to minimum audio level to declare remote audio
*       active; range -35 to -50 db fs
*
* @return 0 if successful; -1 if not
*/
int AMGetMinSpeakerLevel(int &minLevel)
{
  AUDIO_PROPERTIES props;
  int returnVal = 0;
  
  if (GetAudioProperties(&props) == 0)
    { minLevel = props.minSpkrActive; }
  else
    { returnVal = -1; }

  return returnVal;
}

/*** Allows user application to set the active speaker minimum level
*
* @param minLevel - Minimum audio level to declare remote audio active;
*        range -35 to -50 db fs
*
* @return 0 if successful; -1 if not
*/
int AMSetNRAmount(int amount)
{
  if(amount < MIN_NR_AMOUNT)
  {
    return -1;
  }
  if(amount > MAX_NR_AMOUNT)
  {
    return -1;
  }
  
  AudioPropertiesSet(MinNRAmountKey,amount);
  
  return 0;
}

/** Allows user application to get the active speaker minimum level
*
* @param minLevel - Reference to minimum audio level to declare remote audio
*       active; range -35 to -50 db fs
*
* @return 0 if successful; -1 if not
*/
int AMGetNRAmount(int &amount)
{
  AUDIO_PROPERTIES props;
  int returnVal = 0;
  
  if (GetAudioProperties(&props) == 0)
    { amount = props.nrAmount; }
  else
    { returnVal = -1; }

  return returnVal;
}


/*** Allows user application to select the aec post algorithm (DTD or not)
*
* @param Algorithm select - 0 = no DTD 1 = DTD
*
* @return 0 if successful; -1 if not
*/
int AMSetAecPostAlgo(int algo)
{
  
  if(algo < 0)
  {
    return -1;
  }
  if(algo > 1)
  {
    return -1;
  }
  
  algo = algo + 2; // to align with internal values
  
  AudioPropertiesSet(AecPostKey,algo);
  
  return 0;
}

/** Allows user application to get the current AEC post algorithm selection
*
* @param algo - pointer to int to recieved algo value
*       valid values 0 & 1
*
* @return 0 if successful; -1 if not
*/
int AMGetAecPostAlgo(int &algo)
{
  AUDIO_PROPERTIES props;
  int returnVal = 0;
  
  if (GetAudioProperties(&props) == 0)
    { 
      algo = props.aecPost - 2; 
    }
  else
    { returnVal = -1; }

  return returnVal;
}

/** Checks the state of speaker
*
* @return 1 if the speaker is actve and 0 if not
*/
int AMIsSpeakerActive()
{
    return isSpeakerActive;
}



/** Computes the isSpeakerActive flag 
* uses the speaker level and a hysterisis period approx. equal to echo time
*
* @return 1 if the speaker is actve and 0 if not
*/
int AMCheckSpeakerActive()
{
#define SPKR_ACTIVE             1
#define SPKR_INACTIVE_WAIT_ECHO 2
#define SPKR_INACTIVE           3

  static int state = 0;
  static int lastState = -1;
  int retVal = 0;
  static int timerInit = 0;
  static POLL_TIMER spkrActiveTmr;  
  float spkrLvl;
  static int invokeCount = 0;
  static int lastSpkrActive = 0;
  cAudioMgr *pAM;
  AUDIO_PROPERTIES audioProps;
 
  GetAudioProperties(&audioProps);
   
  if(audioProps.minSpkrActive != lastSpkrActive)
  {
    printf("Min Speaker active changed %d->%d \n",lastSpkrActive,audioProps.minSpkrActive);
  }
  lastSpkrActive = audioProps.minSpkrActive;
  
  
  if(timerInit == 0)
  {
    PollTimerInit(&spkrActiveTmr, SPKR_HYS_TIME);
    timerInit = 1;
  }
  
  pAM = gpOwlHook->getAM();
  spkrLvl = pAM->getSpkrLevel();
  if((spkrLvl > MIN_SPKR_ACTIVE) && (GetUsbSpkrActive() == 1))
  {
    PollTimerReset(&spkrActiveTmr, SPKR_HYS_TIME);
    state = SPKR_ACTIVE;
    retVal =  1;
  }
  else
  {
    if(PollTimerPollTimerOnce(&spkrActiveTmr) == 0)
    {
      state = SPKR_INACTIVE_WAIT_ECHO;
       retVal =  1;
    }
    else
    {
      state = SPKR_INACTIVE;
      retVal =  0;
    }

  }
  if(AUDIO_SYSTEM_DEBUG)
  {
    if(lastState != state)
    {
      printf("%llu CheckSpeaker %d->%d, %d %d \n",getMsecs(),lastState, state, retVal,invokeCount);
    }
  }
  lastState = state;
  isSpeakerActive = retVal;
  return retVal;
}

/** This function implements a AutoRegressive Moving Averge filter.
*
* It allows for different filter coefficients for rising signals and falling signals
* @param lastVal - The last value of the signal to be filtered
* @param newVal - The new value of the signal to be filtered
* @param upCoeff - The filter coeff to use when the signal is rising
* @param downCoeff - The filter coeff to use when the signal is dropping
*
* @return The filtered value of the signal
*/


float ARMAFilter(float lastVal, float newVal, float upCoeff, float downCoeff)
{
	float coeff;

	if (newVal > lastVal)
		coeff = upCoeff;
	else
		coeff = downCoeff;

	lastVal = (newVal * coeff) + (1 - coeff)*lastVal;

	return lastVal;

}
/** Gets the detector values from the speex library
*
* @param detectsOut - The filtered value so the speex detectors
* @param pEcho - pointer to speex echo cancellation state
* @param pPost - pointer to speex post processor state
*
* @return 0 of sucessfull; -1 if not
*/
int GetSpeexDetectors(DETECTORS *pDetectsOut, SpeexEchoState *pEcho, SpeexPreprocessState *pPost)
{
#ifdef __ANDROID__
	float temp;
	SPEEX_DETECTORS spxDets;
	int speechProb;
	float upCoeff = 0.1;
	float downCoeff = 0.1;
  SPEEX_MDF_STATUS mdfStats;
	

	if(pEcho != NULL)
	{
    speex_echo_ctl(pEcho, SPEEX_ECHO_GET_STATUS, &mdfStats);    
    pDetectsOut->leakEstimate = mdfStats.leakEstimate;
    pDetectsOut->echoAdapted = mdfStats.echoAdapted;
    pDetectsOut->Pey = mdfStats.Pey;
    pDetectsOut->Pyy = mdfStats.Pyy;
	}
	else
	{pDetectsOut->echoAdapted = 0;}
	if(pPost != NULL)
  {
	speex_preprocess_ctl(pPost, SPEEX_PREPROCESS_GET_DETECTORS, &spxDets);	
	speex_preprocess_ctl(pPost, SPEEX_PREPROCESS_GET_PROB, &speechProb);
    pDetectsOut->speechProb = speechProb;
	
    pDetectsOut->meanResidualEcho = spxDets.meanResidualEcho;
    pDetectsOut->meanApriorSNR = spxDets.meanApriorSNR;
    pDetectsOut->meanApostSNR = spxDets.meanApostSNR;

  }
  else
  {
    pDetectsOut->echoAdapted = 1;
    pDetectsOut->meanApostSNR = 0;
    pDetectsOut->meanApriorSNR = 0;
    pDetectsOut->meanResidualEcho = 0;
    pDetectsOut->speechProb = 0;
  }
#else
  pDetectsOut->echoAdapted = 1;
  pDetectsOut->meanApostSNR = 0;
  pDetectsOut->meanApriorSNR = 0;
  pDetectsOut->meanResidualEcho = 0;
  pDetectsOut->speechProb = 0;
#endif
	return 0;
}

///< returns pointer to double talk detector owned by audio manager; 
///< primarily used for debug
cAudioDTD *cAudioMgr::getDTD()
{
  return mpDoubleTalkDetector;
}

///< returns the value of the speaker level
double cAudioMgr::getSpkrLevel()
{
  return mag2db(mSpkrActiveRMS);
}

///< Construct and initialize the components of the audio path
cAudioMgr::cAudioMgr()
{
  int status;
  ALM_CONFIG almConfig;
  int i;

#ifdef __ANDROID__  
  for(i = 0; i < MAX_PROC_SIZE; ++i)
  {
    mMicAecBuff[i] = 0;
    mRefAecBuff[i] = 0;
    mOutAecBuff[i] = 0;  
  
  }
#endif // __ANDROID__
  AUDIO_PROPERTIES props;
  GetAudioProperties(&props);
  
  if(APP_OUT_FILE_OUT)
  {
    mToUsbFd = fopen("/data/audio/app_to_usb.raw", "wb");
  }
  else
  {
    mToUsbFd = NULL;
  }
  mAudioMgrFs = DEFAULT_FS;
  mSpkrActiveRMS = 0;

  GetAudioConfig(&mAudioConfig);

  if(AUDIO_SYSTEM_DEBUG)
  {
    printf("Audio Config frame size %d mics %d \n", mAudioConfig.procFrameCount,
           mAudioConfig.micCount);
  }
  EqInit(&mLeftEq, mAudioMgrFs);
  EqConfigure(&mLeftEq, 0, HPF, 180, 0, 2.0);
  EqActivate(&mLeftEq, 0, 1);
  EqConfigure(&mLeftEq, 1, HPF, 180, 0, 2.0);  
  //EqActivate(&mLeftEq, 1, 1);
  EqConfigure(&mLeftEq, 2, LPF, 4500, 0, 2.0);
  //EqActivate(&mLeftEq, 2, 1);
  EqConfigure(&mLeftEq, 3, LSH, 1600, -8, 1.0);
  //EqActivate(&mLeftEq, 3, 1);
  EqConfigure(&mLeftEq, 4, PEQ, 800, 6, 0.3);
  //EqActivate(&mLeftEq, 4, 1);
  EqConfigure(&mLeftEq, 5, PEQ, 1600, -4, 0.3);
  //EqActivate(&mLeftEq, 5, 1);
  EqPostGain(&mLeftEq, 0);



  EqInit(&mRightEq, mAudioMgrFs);
  EqConfigure(&mRightEq, 0,  HPF, 180, 0, 2.0);
  EqActivate(&mRightEq, 0, 1);
//  EqConfigure(&mRightEq, 1,  HPF, 120, 0, 2.0);
  EqConfigure(&mRightEq, 1,  HPF, 180, 0, 2.0);  
  //EqActivate(&mRightEq, 1, 1);
  EqConfigure(&mRightEq, 2, LPF, 4500, 0, 2.0);
//  EqActivate(&mRightEq, 2, 1);
  EqConfigure(&mRightEq, 3, LSH, 1600, -8, 1.0);
//  EqActivate(&mRightEq, 3, 1);
  EqConfigure(&mRightEq, 4, PEQ, 800, 6, 0.3);
//  EqActivate(&mRightEq, 4, 1);
  EqConfigure(&mRightEq, 5, PEQ, 1600, -4, 0.3);
//  EqActivate(&mRightEq, 5, 1);
  EqPostGain(&mRightEq, 0);

  EqInit(&mRefEq, mAudioMgrFs); 
  EqConfigure(&mRefEq, 0, HPF, 180, 0, 2.0);
  EqActivate(&mRefEq, 0, 1);
  EqConfigure(&mRefEq, 1, HPF, 180, 0, 2.0);
  EqActivate(&mRefEq, 1, 1);
  EqPostGain(&mRefEq, 0);

  EqInit(&mSpkrActiveEq, mAudioMgrFs); 
  EqConfigure(&mSpkrActiveEq, 0, HPF, 180, 0, 2.0);
  EqActivate(&mSpkrActiveEq, 0, 1);
  EqConfigure(&mSpkrActiveEq, 1, HPF, 180, 0, 2.0);
  EqActivate(&mSpkrActiveEq, 1, 1);
  EqPostGain(&mSpkrActiveEq, 0);


  
  
  ALMInit(&mLevelMgr);
  ALMInit(&mOutputLevelMgr);

//  almConfig.enables = ALM_ENABLE_COMPRESSION | ALM_ALTERNATE_MEASURE_POINT;
  almConfig.enables = ALM_ENABLE_COMPRESSION | ALM_ALTERNATE_MEASURE_POINT;
  almConfig.compThreshold = -27;
  almConfig.compRatio = -0.05;
  almConfig.compAttack = 0.2;
  almConfig.compDecay = 0.4;
  almConfig.limitThreshold = -32;
  almConfig.limitRatio = 10;
  almConfig.limitAttack = 0.01;
  almConfig.limitDecay = 1.0;
  almConfig.makeup = 18;
  almConfig.duck = 15;
  almConfig.predelay = CURR_PREDELAY;
  almConfig.maxAtten = -30;
	almConfig.delayTime = 250;  

  ALMConfigure(&mLevelMgr, &almConfig);
  mBeamform.setOwner(this);

  ALMConfigure(&mOutputLevelMgr, &almConfig);

  mpDoubleTalkDetector = new cAudioDTD();
#ifdef __ANDROID__

  mpSpeexEcho = speex_echo_state_init(mAudioConfig.procFrameCount,
                                      ECHO_TAIL_SIZE);
  mpSpeexNoise = speex_preprocess_state_init(mAudioConfig.procFrameCount,
                 mAudioMgrFs);
                                  
  status = speex_echo_ctl(mpSpeexEcho, SPEEX_ECHO_SET_SAMPLING_RATE,
                          &mAudioMgrFs);
  status = speex_preprocess_ctl(mpSpeexNoise, SPEEX_PREPROCESS_SET_ECHO_STATE,
                                mpSpeexEcho);
  status = speex_preprocess_ctl(mpSpeexNoise, SPEEX_PREPROCESS_SET_NOISE_SUPPRESS,
				  &props.nrAmount);
  mLastNrAmount = props.nrAmount;				  

  mpDoubleTalkDetector->SetupDTDNR(mpSpeexEcho,mAudioMgrFs,mAudioConfig.procFrameCount); 
                                
#endif
  
}

///< Only contains debug code
cAudioMgr::~cAudioMgr()
{
  if((APP_OUT_FILE_OUT) && (mToUsbFd != NULL))
  {
    printf("Closing mic app raw \n");
    fclose(mToUsbFd);
  }

}

/** this function tells the audio manager about the sound localizer instances
*
* @param pLocalizer - pointer to localizer instance
*
* @return void
*/
void cAudioMgr::setLocalizer(cSoundLocalizer *pLocalizer)
{
  mpLocalizer = pLocalizer;
}


/** Configuration driven downmix of the 8 microphone channels to create 1 audio channel
*
* @param input - pointer to a buffer of 8 channbels of 16 bit audio samples 
* to be processed
* @param output - pointer to buffer to recieive downmixed audio
* @param len - Number of frames of audio in the input buffer
*
**/

void cAudioMgr::MixInput(short *input, short *output,int len)
{
  static int lastMicGainsDb[8] = { -100,-100,-100,-100,-100,-100,-100,-100} ;
  int i; 
  int j;
  float accum;
  short *tempPtr = input;
  int print = 0;

  for(i = 0; i < 8; ++i)
  {
    if(lastMicGainsDb[i] != mAudioProps.micGainsDb[i])
      { print = 1;}
  }

    
  for(i = 0; i < 8; ++i)
  {
    if(lastMicGainsDb[i] != mAudioProps.micGainsDb[i])
    {
      if(mAudioProps.micGainsDb[i] > -90)
       {mMicGains[i] = db2mag((float)mAudioProps.micGainsDb[i]); }
      else
       {mMicGains[i] = 0;}
      lastMicGainsDb[i] = mAudioProps.micGainsDb[i];
    }
  }
  if(print)
  {printf("\n");}
  for(j = 0; j < len*2; j += 2)
  {
    accum = 0;
    output[j+1] = *tempPtr;    
    for(i = 0; i < 8; ++i)
    {
      accum += (float) *tempPtr++ * mMicGains[i];
    }
    if(accum > SHRT_MAX)
    { accum = SHRT_MAX; }
    else if(accum < SHRT_MIN)
    { accum = SHRT_MIN; }
    
    output[j] = (short)accum;
  }
  


}

/** This function is called repeatedly for audio output processing; it
 * runs on a high-priority thread as part of the main A/V pipeline and
 * is intended for quickly assembling the UAC sound output.
 *
 * @param pPcmOut is a pointer to the PCM sound output data, expected
 * to be two interleaved channels.
 * @param pPcmIn is a pointer to the PCM sound input data, expected to
 * be N_MICS interleaved channels.
 * @param bufLen is the input data buffer length,
 */
void cAudioMgr::update(int16_t *pPcmOut, int16_t *pPcmIn, int16_t *pRefIn,
                       int bufLen)
{
  int i;
  int j;
  int16_t *pOut;
  int16_t *pMic;
  int16_t *pRef;
  AUDIO_IF_STATUS ifStatus;
  static int refAvail = 0;
  int status;
  int aecLeftOut;
  int aecRtOut;
  static int lastEnableLm = -1;
  int speechProb = 0;
  static int lastaecLeftOut = -1;
  static int lastaecRtOut = -1;
  static int lastSpeechDetected  = -1;
  int echoAdapted;
  static int lastEchoAdapted = -1;
  static int lastAecPost = -1;
  int  temp;
  double spkrActiveRMS;
  DETECTORS spxDetects;        ///< detector values from speex library  
  
  //#define SIMPLE_AUDIOPLAYER_TEST
  #ifdef SIMPLE_AUDIOPLAYER_TEST
  // to us this put the appropriate file in 
  // /data/audio
  static int playSound = 1;
  if(playSound)
  {
    cAudioPlayer *player;
    char filename[] = "/data/audio/female_test.wav";    
    player = new cAudioPlayer();
    player->playWavFile(filename, NULL);
    playSound = 0;
  }
  #endif

  if(mToUsbFd != NULL)
  {
//    fwrite(pPcmIn, bufLen, 2, mToUsbFd);

  }
  int frameCount = bufLen / 8;

  GetAudioProperties(&mAudioProps);

  aecLeftOut = mAudioProps.aecDebug % 10;
  aecRtOut = (mAudioProps.aecDebug / 10) % 10;


  AudioIfStatusGet(&ifStatus);
  AMCheckSpeakerActive();       // step the speaker active computation;
  j = 0;
  if(mAudioProps.enableBeam == 0)
  {
 
    MixInput(pPcmIn, pPcmOut,bufLen/8);
  }
  else
  {
    mBeamform.update(pPcmOut, pPcmIn, bufLen);
  }

  spkrActiveRMS = EqRMSProcess(&mSpkrActiveEq, pRefIn, frameCount, 0, 2);
  mSpkrActiveRMS = ARMAFilter(mSpkrActiveRMS,spkrActiveRMS,SPKR_ACTIVE_UP, SPKR_ACTIVE_DOWN);

#ifdef __ANDROID__  
  if(mAudioProps.enableMicProc == 1)
  {

    if(mAudioProps.enableEq == 1)
    {
      EqProcess(&mLeftEq, pPcmOut, frameCount, 0, 2);
      EqProcess(&mRightEq, pPcmOut, frameCount, 1, 2);
      EqProcess(&mRefEq, pRefIn, frameCount, 0, 2); // we only use one channel of the ref
    }

    ALMMeasureLevels(&mLevelMgr, pRefIn, frameCount, 0, 2);

    if((mAudioProps.enableAEC == 1) && (ifStatus.refAvail == 1))
    {
      if((refAvail == 0) && (ifStatus.refAvail == 1))
      
      {
        if(AUDIO_SYSTEM_DEBUG)
        {printf("%llu: Resetting AEC and nr  state \n",getMsecs());}
        speex_echo_state_destroy(mpSpeexEcho);
        speex_preprocess_state_destroy(mpSpeexNoise);

        mpSpeexEcho = speex_echo_state_init(mAudioConfig.procFrameCount,
                                            ECHO_TAIL_SIZE);
        mpSpeexNoise = speex_preprocess_state_init(mAudioConfig.procFrameCount,
                       mAudioMgrFs);
        status = speex_echo_ctl(mpSpeexEcho, SPEEX_ECHO_SET_SAMPLING_RATE,
                                &mAudioMgrFs);
        status = speex_preprocess_ctl(mpSpeexNoise, SPEEX_PREPROCESS_SET_ECHO_STATE,
                                      mpSpeexEcho);
        status = speex_preprocess_ctl(mpSpeexNoise, 
                 SPEEX_PREPROCESS_SET_NOISE_SUPPRESS,&mAudioProps.nrAmount);    
        int amount;
        if(mAudioProps.nrAmount == 0)
        {
          amount = 0;
        }
        else
        {
          amount = 1;
        }
        status = speex_preprocess_ctl(mpSpeexNoise,
              SPEEX_PREPROCESS_SET_DENOISE,&amount);                     

        mpDoubleTalkDetector->SetupDTDNR(mpSpeexEcho,mAudioMgrFs,mAudioConfig.procFrameCount);
        mpDoubleTalkDetector->resetDTD();
        mLastNrAmount = mAudioProps.nrAmount;                 
        printf("NR Amount ->%d \n", mAudioProps.nrAmount);        
                                    
      }

      ExtractChannel(pPcmOut, mMicAecBuff, frameCount, 0, 2);
      ExtractChannel(pRefIn, mRefAecBuff, frameCount, 0, 2);
      speex_echo_cancellation(mpSpeexEcho, mMicAecBuff, mRefAecBuff, mOutAecBuff);
      status = speex_echo_ctl(mpSpeexEcho, SPEEX_ECHO_GET_ADAPTED,
                                &echoAdapted);
      GetSpeexDetectors(&spxDetects,mpSpeexEcho,mpSpeexNoise);      
      // moved to pre noise reducer, since DTD uses it's own DTD.
      if(mpDoubleTalkDetector != NULL)
      {
        mpDoubleTalkDetector->ProcessDTD(mMicAecBuff,mOutAecBuff,mRefAecBuff,frameCount,&spxDetects);        
      }
      
      if(mAudioProps.enableNR == 1)
      {
        if(mLastNrAmount != mAudioProps.nrAmount)
        {
          status = speex_preprocess_ctl(mpSpeexNoise, 
                   SPEEX_PREPROCESS_SET_NOISE_SUPPRESS,&mAudioProps.nrAmount); 
          int amount = 0;
          if(mAudioProps.nrAmount == 0)
          {
            amount = 0;
          }
          else
          {
            amount = 1;
          }
          status = speex_preprocess_ctl(mpSpeexNoise, 
                 SPEEX_PREPROCESS_SET_DENOISE,&amount);    
        }
        mLastNrAmount = mAudioProps.nrAmount;                         
        speex_preprocess_run(mpSpeexNoise, mOutAecBuff);
        status = speex_preprocess_ctl(mpSpeexNoise, SPEEX_PREPROCESS_GET_PROB,
                                      &speechProb);
      }
      echoAdapted = 1;
      status = speex_echo_ctl(mpSpeexEcho, SPEEX_ECHO_GET_ADAPTED,
                                &echoAdapted);
      GetSpeexDetectors(&spxDetects,mpSpeexEcho,mpSpeexNoise);      
/* HMB moved to allow for DTD noise reducer
      if(mpDoubleTalkDetector != NULL)
      {
        mpDoubleTalkDetector->ProcessDTD(mMicAecBuff,mOutAecBuff,mRefAecBuff,frameCount,&spxDetects);        
      }
*/    
      pOut = pPcmOut;
      // routes input and intermediate audio to output buffers
      // debug
      for(i = 0; i < frameCount; ++i) 
      {
        switch(aecLeftOut)
        {
        case 1:
          pOut[i * 2] = mMicAecBuff[i];
          break;
        case 2:
          pOut[i * 2] = mRefAecBuff[i];
          break;
        case 3:
          pOut[i * 2] = (mMicAecBuff[i] >> 1) + (mRefAecBuff[i] >> 1);
          break;
        default:
          pOut[i * 2] = mOutAecBuff[i];
          break;
        }
        switch(aecRtOut)
        {
        case 1:
          pOut[i * 2 + 1] = mMicAecBuff[i];
          break;
        case 2:
          pOut[i * 2 + 1] = mRefAecBuff[i];
          break;
        case 3:
          pOut[i * 2 + 1] = (mMicAecBuff[i] >> 1) + (mRefAecBuff[i] >> 1);
          break;
        case 4:
          pOut[i * 2 + 1] = temp;
          break;
        default:
          pOut[i * 2 + 1] = mOutAecBuff[i];
          break;
        }
      }
    }
    refAvail = ifStatus.refAvail;
    if(AUDIO_SYSTEM_DEBUG)
    {
      if(lastEnableLm != mAudioProps.enableLM)
      {
        printf("Enable Level Manager set to %d \n", mAudioProps.enableLM);
      }
    }
    lastEnableLm = mAudioProps.enableLM;
    if(lastEchoAdapted != echoAdapted)
    {
      printf("%llu :Echo Adaptation changed %d->%d\n",getMsecs(),lastEchoAdapted,echoAdapted);
    }
    lastEchoAdapted = echoAdapted;
    if(mAudioProps.enableLM > 0)
    {
      if((echoAdapted == 1) || (AMIsSpeakerActive() == 0))
      {
        if(mAudioProps.aecPost == 2)  // original method
        {
          ALMProcess(&mLevelMgr, pPcmOut, pPcmOut, frameCount, 0, 2, 0);
        }
      }
      if(mAudioProps.aecPost == 3) // alt limiter method
      {
        FLOAT dtdAvail = 0;
        dtdAvail = mpDoubleTalkDetector->DTDAvailable();
        ALMProcess(&mLevelMgr, pPcmOut, pPcmOut, frameCount, 0, 2, dtdAvail);        
      }
      for(i = 0; i < frameCount * 2; i += 2)
      {
        pPcmOut[i + 1] = pPcmOut[i];      
      }
    }
  }
  else
  {
      // routes input and intermediate audio to output buffers
      // debug
 
      ExtractChannel(pPcmOut, mMicAecBuff, frameCount, 0, 2);    
      if(ifStatus.refAvail == 1)
      {ExtractChannel(pRefIn, mRefAecBuff, frameCount, 0, 2);}
      else
      {
        for(i= 0; i < frameCount; ++i)
        { mRefAecBuff[i] = 0;}
      }
      pOut = pPcmOut;
      for(i = 0; i < frameCount; ++i) 
      {
        switch(aecLeftOut)
        {
        case 1:
          pOut[i * 2] = mMicAecBuff[i];
          break;
        case 2:
          pOut[i * 2] = mRefAecBuff[i];
          break;
        case 3:
          pOut[i * 2] = (mMicAecBuff[i] >> 1) + (mRefAecBuff[i] >> 1);
          break;
        default:
          pOut[i * 2] = mMicAecBuff[i];
          break;

        }
        switch(aecRtOut)
        {
        case 1:
          pOut[i * 2 + 1] = mMicAecBuff[i];
          break;
        case 2:
          pOut[i * 2 + 1] = mRefAecBuff[i];
          break;
        case 3:
          pOut[i * 2 + 1] = (mMicAecBuff[i] >> 1) + (mRefAecBuff[i] >> 1);
          break;
        default:
          pOut[i * 2 + 1] = mMicAecBuff[i];
          break;

        }
      }
    }


    if(aecRtOut == 4)
    {
        for(i = 0; i < frameCount; i++)
        {
            pPcmOut[2*i + 1] = mpLocalizer->getDebugVal(i);          
        }
    }

  
  ALMMeasureLevels(&mOutputLevelMgr, pPcmOut, frameCount, 0, 2);
  // Because we don't call ALMProcess() on mOutputLevelMgr, we must copy
  // the final rms and peak level values into the 'lastLevels' struct.
  mOutputLevelMgr.lastLevels.rmsLevel =
    mOutputLevelMgr.levelState.rmsLevelBuffer[frameCount - 1];
  mOutputLevelMgr.lastLevels.peakLevel =
    mOutputLevelMgr.levelState.peakLevelBuffer[frameCount - 1];
#endif    
}

///< Passthrough to the undelying beamformer object
/** This function gets the desired angle for beamforming.
 *
 * @return the the desired angle in degrees [0, 359].
 */
int  cAudioMgr::getAngle(void)
{
  return mBeamform.getAngle();
}

///< Passthrough to the undelying beamformer object
/** This function sets the desired angle for beamforming.
 *
 * @param angle is the desired angle in degrees, enforced to [0, 359].
 */
void cAudioMgr::setAngle(int angle)
{
  mBeamform.setAngle(angle);
}

/** returns a pointer to the local properties structure
*
* @return pointer to AUDIO_PROPERTIES structure
*/
AUDIO_PROPERTIES *cAudioMgr::getAudioPropPtr()
{
  return &mAudioProps;
}

/** Passes back the RMS and Peak levels detected in the mic audio.
 *
 * @param pRMS Pointer to the location to write the RMS value.
 * @param pPeak Pointer to the location to write the Peak value.
 *
 */
void cAudioMgr::getOutputLevels(float *pRMS, float *pPeak)
{
  *pRMS = mOutputLevelMgr.lastLevels.rmsLevel;
  *pPeak = mOutputLevelMgr.lastLevels.peakLevel;
}
