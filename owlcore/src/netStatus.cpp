/*
 * netStatus.cpp
 *
 * Copyright (c) Owl Labs Inc. 2017.
 * All rights reserved.
 *
 *  Created on: Apr 28, 2017
 *
 *  Module implementing thread to check network configuration and internet
 *  connectivity.
 */

#include <unistd.h>
#include <vector>

#include "common.hpp"
#include "logging.hpp"
#include "netStatus.hpp"
#include "shellProcess.hpp"


// #define DEBUG_STARTUP  // Only for testing!
#ifdef DEBUG_STARTUP
#define LOGIS LOGI
#else
#define LOGIS(...) {}
#endif



/** Construct a cNetStatus object.
 *
 * Creates and starts the thread that updates this state machine.
 */
cNetStatus::cNetStatus()
  : mState(NO_NETWORK_CONCLUSION_YET),
    mAttemptCount(0),
    mNetStatusThread(std::bind(&cNetStatus::updateNetStatus, this),
                               "netStatus")
{
  mNetStatusThread.start();
}

/** Destroy a cNetStatus object.
 *
 */
cNetStatus::~cNetStatus()
{
  mNetStatusThread.join();
}

/** Run one cycle of the net status state machine.
 *
 */
void cNetStatus::updateNetStatus(void)
{
  switch (mState)
  {
    case NO_NETWORK_CONCLUSION_YET:
    case NETWORK_UNCONFIGURED:
      if (testNetworkConfigured() == true)
        {
          mState = NETWORK_CONFIGURED;
          // Reset the counter so Internet access has time to try
          mAttemptCount = 0;
        }
      else
        {
          // if we've tried long enough, conclude we're not configured
          if (mAttemptCount >= mkRetrySteadyStateCount)
            { mState = NETWORK_UNCONFIGURED; }
          else  // otherwise, we don't know the network state yet
            { mState = NO_NETWORK_CONCLUSION_YET; }
        }
      break;
    case NETWORK_CONFIGURED:
    case NO_INTERNET_CONCLUSION_YET:
    case INTERNET_INACCESSIBLE:
      if (testInternetAccessible() == true)
        {
          mState = INTERNET_ACCESSIBLE;
          // verify connectivity at slowest rate
          mAttemptCount = mkRetrySteadyStateCount;
        }
      else
        {
          // if we've tried long enough, conlcude we cannot reach the Internet
          if (mAttemptCount >= mkRetrySteadyStateCount)
            { mState = INTERNET_INACCESSIBLE; }
          else  // otherwise, we don't know if we have Internet connectivity
            { mState = NO_INTERNET_CONCLUSION_YET; }
        }
      break;
    case INTERNET_ACCESSIBLE:
      if (testNetworkConfigured() == false)
        {
          mState = NETWORK_UNCONFIGURED;  /* Lost contact with net? */
          LOGI("cNetStatus: Lost contact with network\n");
        }
      break;
    default:
      LOGW("cNetStatus::handleNetStatus(): Illegal mState.\n");
      break;
  }

  // Delay progressively longer times as attempts increase.
  mAttemptCount++;
  if (mAttemptCount >= mkRetrySteadyStateCount)
    { mAttemptCount = mkRetrySteadyStateCount; }
  usleep(mAttemptCount * mkRetryAttemptMinInterval);
}

/** Report status of network configuration check.
 *
 * @return 0: not configured,
 *         1: configured successfully,
 *        -1: don't know state yet.
 */
int cNetStatus::isNetworkConfigured(void)
{
  int retVal;

  switch (mState)
  {
    case NO_NETWORK_CONCLUSION_YET:
      retVal = -1;
      break;
    case NETWORK_UNCONFIGURED:
      retVal = 0;
      break;
    default: /* All other cases, we're good! */
      retVal = 1;
      break;
  }

  return retVal;
}

/** Report status of internet connection check.
 *
 * @return 0: not connected,
 *         1: connected successfully,
 *        -1: don't know state yet.
 */
int cNetStatus::isInternetAccessible(void)
{
  int retVal;

  switch (mState)
  {
    case NO_NETWORK_CONCLUSION_YET:
    case NETWORK_CONFIGURED:
    case NO_INTERNET_CONCLUSION_YET:
      retVal = -1;
      break;
    case NETWORK_UNCONFIGURED:
    case INTERNET_INACCESSIBLE:
      retVal = 0;
      break;
    default: /* All other cases, we're good! */
      retVal = 1;
      break;
  }

  return retVal;
}

/** Test the state of the network interface.
 *
 * @return true if the network interface has been configured.
 */
bool cNetStatus::testNetworkConfigured(void)
{
  bool networkConfigured = false;

#ifdef __ANDROID__
  // Check to see if the WiFi network interface is up (only valid for
  // Android systems)
  // Note: alternative could use 'ip link show wlan0' then look for either
  // NO-CARRIER, or 'state DOWN' (or 'state UP').
  std::string delims(" \t:.");
  std::vector<std::string> toks;
  cShellProcess netcfg(std::string("netcfg"),
    /* The following lambda function tokenizes each line returned by the
     * 'netcfg' system call, then finds the line containing the 'wlan0'
     * interface, then determines if the interface is up by checking to
     * see if the first element of the IP address is non-zero.
     */
    [&](const std::string *pLine)
    {
      if (pLine != nullptr)
        {
          toks.clear();
          tokenize(*pLine, delims, toks);
          if ((toks[0].find("wlan0") != std::string::npos)
              && (toks[2][0] != '0') && isdigit(toks[2][0]))
            { LOGIS(pLine->c_str()); networkConfigured = true; }
          return true; }
      else
        { return false; }
    },
    true );
#else  /* Linux */
  /* Check the state of the DHCP lease to know if the network is up.
   */
  cShellProcess checkLease(std::string(
      "ps aux | grep -o '[/]var/lib/NetworkManager/\\S*.lease'"),
      /* The following lambda function checks to see if any non-empty
       * strings are returned by the grep for leases. If so, then a
       * network interface must be configured.
       */
      [&](const std::string *pLine)
      {
        if (pLine != nullptr)
          {
            if (pLine->length() > 0)
              {
                LOGIS("checkLease: %s\n", pLine->c_str());
                networkConfigured = true;
              }
            return true;
          }
        else
          { return false; }
      },
      true );
#endif

  LOGIS("testNetworkConfigured: %s\n", (networkConfigured ? "true" : "false"));

  return networkConfigured;
}

/** Test the state of internet access.
 *
 * @return true if the internet is accessible.
 */
bool cNetStatus::testInternetAccessible(void)
{
  bool internetAccessible = false;
  std::string curlCmd = std::string(
      "curl -D - --connect-timeout 6 --max-time 6 -s -o /dev/null http://google.com/generate_204");

  // Check internet connectivity by opening the generate_204 page at google.com
  // and making sure that the response contains 204, which indicates that
  // the page returns no content.
  cShellProcess openGoogle(std::string(curlCmd),
                 [&](const std::string *pLine)
                 {
                   if (pLine != nullptr)
                    {
                      /* LOGIS("ping: %s\n", pLine->c_str()); */
                      if ((pLine->find("204") != std::string::npos) &&
                          (pLine->find("No Content") != std::string::npos))
                        { internetAccessible = true; }
                      return true;
                    }
                   else
                    { return false; }
                 },
                 true);

  LOGI("testInternetAccessible: %s\n",
        (internetAccessible ? "true" : "false"));

  return internetAccessible;
}

