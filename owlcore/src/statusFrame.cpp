/*
 * statusFrame.cpp
 *
 * Copyright (c) Owl Labs Inc. 2017.
 * All rights reserved.
 *
 * Description: Implements a status frame that contains status tiles. Each
 * tile contains one or more icons representing an aspect of system status.
 * A tile's icon is selected based on result of a periodic update where the
 * update is a callback passed to the tile at creation time.
 * The frame lets each tile render itself into the display buffer, then
 * passes the display buffer to the GPU to render. This behavior is overridden
 * if a tile requests animation. When a tile changes state to animate,
 * that tile alone is animated for the duration of the action, after which
 * the frame with all its tiles is rendered normally again. Finally, the frame
 * can fade away to invisibility if all the tiles have the allow-fade-away
 * feature set to true. When the frame becomes invisible, it is no-longer
 * rendered.
 */

#ifdef __ANDROID__
#include <pthread.h>           // Required for audioframework.h
#include "audioframework.h"    // Required for GetUsb*Active() libOwlPG funcs
#endif
#include "common.hpp"
#include "imageFileUtils.hpp"
#include "logging.hpp"
#include "params.h"
#include "statusFrame.hpp"
#include "systemtest/testImgCompositor.hpp"


/* The following flag enables testing of an additional tile with varying size
 * icons. This extra tile contains icons that test the following features:
 * 1) Different heights and widths of icons in the tile.
 * 2) Icons that allow fading out, and others that do not.
 * 3) Icons that animate, and others that do not.
 * Note, you must manually copy the test icon resources to the Owl with:
 * adb push ./assets/pig.png /system/media/OwlLabs
 * adb push ./assets/tree.png /system/media/OwlLabs
 * adb push ./assets/cheetah.png /system/media/OwlLabs
 */
// #define ENABLE_SIZE_TEST

#define DISABLE_STATUS_FRAME // Uncomment to suppress status frame.

/* *** cIconInitializer *** */

/** Constructs a copy of the provided object.
 *
 * @param object to copy.
 */

/** Constructs the icon initializer object used to create an icon. */
cIconInitializer::cIconInitializer(std::string filename,
				   bool zoomOnActivate,
				   bool zoomOnDeActivate,
				   bool allowFadeIn,
				   bool allowFadeAway )
  : mIconFileName(filename),
    mZoomOnActivate(zoomOnActivate),
    mZoomOnDeActivate(zoomOnDeActivate),
    mAllowFadeIn(allowFadeIn),
    mAllowFadeAway(allowFadeAway)
{
}

/** Destroys the object. */
cIconInitializer::~cIconInitializer()
{
}


/* *** cTileIcon *** */

/** Constructs an object given data from an initializer object.
 *
 * @param icon data in an initializer class.
 */
cTileIcon::cTileIcon(cIconInitializer icon)
  : mZoomOnActivate(icon.mZoomOnActivate),
    mZoomOnDeActivate(icon.mZoomOnDeActivate),
    mAllowFadeIn(icon.mAllowFadeIn),
    mAllowFadeAway(icon.mAllowFadeAway)
{
  int width;
  int height;

  // Load the icon bitmap
  pngFileGetImageSize((char *)icon.mIconFileName.c_str(),
                      &width,
                      &height);
  mWidth = width;
  mHeight = height;

  mpBuf = new uint32_t[width * height];

  pngFileRead((uint8_t *)mpBuf, (char *)icon.mIconFileName.c_str());

  LOGI("loaded %dx%d logo '%s'\n", mWidth, mHeight,
       icon.mIconFileName.c_str());
}

/** Constructs a copy of an object.
 *
 * @param object to copy.
 */
cTileIcon::cTileIcon(const cTileIcon &c)
{
  mWidth = c.mWidth;
  mHeight = c.mHeight;
  mZoomOnActivate = c.mZoomOnActivate;
  mZoomOnDeActivate = c.mZoomOnDeActivate;
  mAllowFadeIn = c.mAllowFadeIn;
  mAllowFadeAway = c.mAllowFadeAway;
  mpBuf = new uint32_t[mWidth * mHeight];
  memcpy(mpBuf, c.mpBuf, mWidth * mHeight);
}

/** Destroy a tile icon object.
 *
 */
cTileIcon::~cTileIcon(void)
{
  delete [] mpBuf;
}

/** Render a row of the icon into supplied memory buffer.
 *
 * This function should be called repeatedly for every row in the tile
 * in order to completely render the icon. It is called row by row to
 * accommodate the offset and stride of the destination buffer. The
 * selected icon is rendered such that it is centered in the area provided
 * for the tile.
 *
 * @param row of the tile that is to be rendered.
 * @param nTileRows number of rows in the containing tile.
 * @param nTileCols number of columns in the containing tile.
 * @param mpRowBuf pointer to the beginning of the row in the tile.
 */
void cTileIcon::renderRow(uint16_t row,
                          uint16_t nTileRows, uint16_t nTileCols,
                          uint32_t *mpRowBuf)
{
  int16_t rowStartOffset = (nTileRows - mHeight) / 2;  // Center in tile Y
  int16_t colStartOffset = (nTileCols - mWidth) / 2; // Center in tile X
  uint32_t *pIconRow;
  int16_t iconRow = (int16_t)row - rowStartOffset;

  if ((iconRow >= 0) && (iconRow < mHeight))
    {
      // Zero out any leading columns
      if (colStartOffset > 0)
        {
          memset(mpRowBuf, 0, colStartOffset * sizeof(uint32_t));
        }
      // Fill in the icon data for the row
      pIconRow = &mpBuf[iconRow * mWidth];
      memcpy(&mpRowBuf[colStartOffset], pIconRow, mWidth * sizeof(uint32_t));
      // Zero out any trailing columns
      if ((colStartOffset + mWidth) < nTileCols)
        {
          memset(&mpRowBuf[colStartOffset + mWidth], 0,
                 (nTileCols - colStartOffset - mWidth) * sizeof(uint32_t));
        }
    }
  else
    { // Icon does not cover the row, so zero it out
      memset(mpRowBuf, 0, nTileCols * sizeof(uint32_t));
    }
}


/* *** cTile *** */

/** Constructor to create a tile from a list of icon initializer objects.
 *
 * @param iconList list of objects describing all the icons in the tile.
 * @param selectedIconCallback function that returns the selected icons number.
 */
cTile::cTile(std::vector<cIconInitializer>iconList,
             tileIconSelectionCallback_t selectedIconCallback)
  : mSelectActiveIconCallback(selectedIconCallback),
    mSize(cv::Size(0,0)),
    mSelectedIcon(-1),
    mDeSelectedIcon(-1)
{
  for (std::vector<cIconInitializer>::iterator iconInit = iconList.begin();
      iconInit != iconList.end(); iconInit++)
    {
      cTileIcon *pIcon = new cTileIcon((*iconInit));
      mIcons.push_back(pIcon);
      uint16_t w = pIcon->getWidth();
      uint16_t h = pIcon->getHeight();
      if (w > mSize.width) { mSize.width = w; }
      if (h > mSize.height) { mSize.height = h; }
    }
}

/** Destructor to dispose of the icons that make up a tile.
 *
 */
cTile::~cTile(void)
{
  for (std::vector<cTileIcon *>::iterator pIcon = mIcons.begin();
      pIcon != mIcons.end(); pIcon++)
    {
      delete (*pIcon);
    }
}

/** Function that invokes the icon callbacks and updates the tile state.
 *
 * Calls mSelectActiveIconCallback() and sets pAnimate true if the newly
 * selected or de-selected icon requests zooming. Also returns it selection
 * changed or not.
 *
 * @param pAnimate returns true if tile demands animation, false otherwise.
 * @param pChangedSelection returns true if selected icon changes, false
 *        otherwise.
 */
void cTile::updateSelection(bool *pAnimate, bool *pChangedSelection)
{
  bool animationRequired = false;
  uint16_t startingSelection = mSelectedIcon;
  uint16_t newSelection;

  newSelection = (*mSelectActiveIconCallback)();
  if (validIcon(newSelection))
    {
      mSelectedIcon = newSelection;
    }

  if (mSelectedIcon != startingSelection)
    {
      *pChangedSelection = true;
      if (validIcon(startingSelection) &&
          (mIcons[startingSelection]->getZoomOnDeActivate()))
        {
          mDeSelectedIcon = startingSelection;
          animationRequired = true;
        }
      if (validIcon(mSelectedIcon) &&
          (mIcons[mSelectedIcon]->getZoomOnActivate()))
        {
          animationRequired = true;
          mDeSelectedIcon = -1;
        }
    }
  else
    {
      *pChangedSelection = false;
    }

  *pAnimate = animationRequired;
}

/** Function that renders the active icon into the tile in the status frame.
 *
 * An icon is rendered into the tile, and must be RGBA (4 bytes per pixel)
 * format.
 * Figures out offset needed to center the tile vertically in the frame,
 * then invokes cTileIcon::renderRow() for each row to render the selected
 * icon into the frame.
 *
 * @param col column where the tile begins in the frame.
 * @param frameRows number of rows in the frame.
 * @param frameCols number of columns in the frame.
 * @param mpFrameBuf pointer to memory that icons are rendered into.
 */
void cTile::render(uint16_t col,
              uint16_t frameRows, uint16_t frameCols,
              uint32_t *mpFrameBuf)
{
  uint16_t row;
  uint16_t rowStartOffset = (frameRows - mSize.height) / 2;
  cTileIcon *pI;
  uint32_t *pRow;

  if (validIcon(mSelectedIcon))
    {
      pI = mIcons[mSelectedIcon];
      // Zero out any rows before the tile starts
      for (row = 0; row < rowStartOffset; row++)
        {
          memset(&mpFrameBuf[row * frameCols + col], 0,
                 mSize.width * sizeof(uint32_t));
        }
      // Fill in the rows of the tile
      for (row = 0; row < mSize.height; row++)
        {
          pRow = &mpFrameBuf[(row + rowStartOffset) * frameCols + col];
          pI->renderRow(row, mSize.height, mSize.width, pRow);
        }
      // Zero out any rows after the tile ends
      for (row = rowStartOffset + mSize.height; row < frameRows; row++)
        {
          memset(&mpFrameBuf[row * frameCols + col], 0,
                 mSize.width * sizeof(uint32_t));
        }
    }
  else
    {  // No icon selected, so blank the frame
      for (row = 0; row < frameRows; row++)
        {
          memset(&mpFrameBuf[row * frameCols + col], 0,
                 mSize.width * sizeof(uint32_t));
        }
    }
}


/* *** cStatusFrame *** */

/** Constructor for the status frame object.
 *
 * Determines the size of the bounding rectangle containing all the tiles
 * and allocates space to hold the rendered frame.
 *
 * @param upperLeft Upper left coordinate in image space of the frame.
 * @param scale Scale factor to resize frame when displayed statically.
 * @param pTileList Pointer to list of cTile object pointers managed by the frame.
 */
cStatusFrame::cStatusFrame(cv::Point upperLeft, float scale,
                           std::vector<cTile *>*pTileList)
  : mDestOrigin(upperLeft),
    mAnimOrigin(upperLeft),
    mpTileList(pTileList),
    mScale(scale),
    mAnimScale(scale),
    mSize(cv::Size(0,0)),
    mAnimSize(cv::Size(0,0)),
    mAnimating(false),
    mpAnimatingTile(NULL),
    mT0({0,0}),
    mRenderAlpha(0.0f),
    mFadeTimeRef({0,0}),
    mFadedIn(false),
    mkAnimationDuration(1.5f),
    mkFadeInStart(0.0f),
    mkFadeInDuration(0.5f),
    mkFadeAwayStart(5.0f),
    mkFadeAwayDuration(3.0f),
    mDoUpdate(false),
    mRenderRequired(false),
    mVisible(false)
{
  uint16_t width;
  uint16_t height;

  for (std::vector<cTile *>::iterator pTile = pTileList->begin();
      pTile != pTileList->end(); pTile++)
    {
      width = (*pTile)->getSize().width;
      height = (*pTile)->getSize().height;
      if (height > mSize.height) { mSize.height = height; }
      mSize.width += width;
    }
  mpFrameBuf = new uint32_t [mSize.width * mSize.height];
  clock_gettime(CLOCK_MONOTONIC, &mFadeTimeRef);
}

/** Destroys the status frame.
 *
 * Deletes the constituent tiles, the tile list, and the frame buffer allocated
 * for the frame.
 */
cStatusFrame::~cStatusFrame(void)
{
  // Delete each tile in the frame
  for (std::vector<cTile *>::iterator pTile = mpTileList->begin();
      pTile != mpTileList->end(); pTile++)
    {
      delete (*pTile);
    }

  // Then delete the list of tiles itself
  delete mpTileList;

  // Delete the buffer used to compose the frame
  delete [] mpFrameBuf;
}

/** Renders all the tiles into the output frame buffer.
 *
 * Allows each tile to render itself into the status frame buffer. This
 * buffer then needs to be sent to the GPU to update the RGBA overlay.
 */
void cStatusFrame::renderAllTiles(void)
{
  uint16_t tileStartCol = 0;

  for (std::vector<cTile *>::iterator pTile = mpTileList->begin();
      pTile != mpTileList->end(); pTile++)
    {
      (*pTile)->render(tileStartCol,
                       mSize.height, mSize.width, mpFrameBuf);
      tileStartCol += (*pTile)->getSize().width;
    }
}

/** Updates the fade out timer, as appropriate.
 *
 * This function manages when fading starts and completes in the case where
 * the selected icon in every tile has the attribute to allow fade-out set
 * to true.
 */
void cStatusFrame::updateFade(void)
{
  bool allowFadeIn;
  bool allowFadeAway;
  double elapsedTime = 0.0;
  struct timespec tStart;

  // While rendering, check if the image should fade
  allowFadeIn = true;
  allowFadeAway = true;
  for (std::vector<cTile *>::iterator pTile = mpTileList->begin();
      pTile != mpTileList->end(); pTile++)
    {
      // Only if all the tiles have a selected icon that allows fading
      // should fading be enabled.
      allowFadeAway &= (*pTile)->getAllowFadeAway();
      allowFadeIn &= (*pTile)->getAllowFadeIn();
    }
  
  clock_gettime(CLOCK_MONOTONIC, &tStart);
  if (!allowFadeIn && !allowFadeAway)
    { // Reset the fading start reference if not fading
      mFadeTimeRef = tStart;
      mRenderAlpha = 1.0f;
    }
  else
    { // Fading is allowed, check how long that has been true
      elapsedTime = elapsedTimeSec(mFadeTimeRef, tStart);

      if(allowFadeIn && !mFadedIn && elapsedTime >= mkFadeInStart)
	{ // calculate alpha for fade-in
	  float k = ((float)elapsedTime - mkFadeInStart) / mkFadeInDuration;
	  k = std::max(std::min(k, 1.0f), 0.0f); // clamp to [0.0, 1.0]
	  mRenderAlpha = k;
	  
	  mRenderRequired = true;
	  if(mRenderAlpha >= 1.0)
	    { // done fading in
	      mFadedIn = true;
	      // reset time reference for fade out
	      mFadeTimeRef = tStart;
	    }
	}
      else if (allowFadeAway && elapsedTime >= mkFadeAwayStart)
        { // calculate alpha for fade-away
          float k = ((float)elapsedTime - mkFadeAwayStart) / mkFadeAwayDuration;
	  k = std::max(std::min(k, 1.0f), 0.0f); // clamp to [0.0, 1.0]
          mRenderAlpha = (1.0f - k);
	  
	  mRenderRequired = true;
	  if(mRenderAlpha <= 0.0)
	    { // done fading out -- no longer visible.
	      setVisible(false);
	    }
        }
    }
}


void cStatusFrame::setVisible(bool visibility)
{
  if(visibility != mVisible)
    {
      mVisible = visibility;
      if(mVisible)
	{
	  clock_gettime(CLOCK_MONOTONIC, &mFadeTimeRef);
	  mRenderRequired = true;
	  mFadedIn = false;
	}
      else
	{
	  mRenderAlpha = 0.0;
	}
    }

  if(!mVisible)
    { mRenderRequired = false; }
}

/** Reset animation state of all tiles in a frame.
 *
 */
void cStatusFrame::resetAnimation(void)
{
  // Update the selections for each tile to -1
  for (std::vector<cTile *>::iterator pTile = mpTileList->begin();
       pTile != mpTileList->end(); pTile++)
    {
      (*pTile)->resetSelection();
    }
  mAnimating = false;
}

/** Updates the contents of the status frame object for each new video frame.
 *
 * This function should be called in the update function that renders the
 * data for display through the GPU. This function updates the selected
 * icons for each tile first, then renders appropriately for the selections.
 * The result is new data in the frame buffer memory managed by the object,
 * and potentially updated values for the destination origin, retrieved by
 * getDest(), the bounding rectangle, retrieved by getRect(), the rendering
 * scale factor, retrieved by getScale(), and the alpha multiplier for level of
 * opaqueness, retrieved by getAlpha().
 * If an icon becomes active that is flagged to be animated, this function
 * manages rendering of the stages of the icon animation.
 *
 * @param true if an update should be forced because the GPU memory contains
 *        stale imagery that needs to be overwritten.
 *
 * @returns true if an update occurred requiring transfer of the image to
 *          the GPU.
 */
bool cStatusFrame::update(bool force)
{
  struct timespec t1;
  double elapsedTime = 0.0;
  struct timespec tStart;
  struct timespec tFinish;

  clock_gettime(CLOCK_MONOTONIC, &tStart);

  mRenderRequired = force;

  // Update the selections for each tile, and check for animation
  for (std::vector<cTile *>::iterator pTile = mpTileList->begin();
       pTile != mpTileList->end(); pTile++)
    {
      bool animate;
      bool changed;

      (*pTile)->updateSelection(&animate, &changed);
      if (!mAnimating && animate)
        {
	  setVisible(true);
          mAnimating = true;
          mpAnimatingTile = (*pTile);
          mT0 = tStart; // Remember this time for animation reference
          LOGI("Status Frame should be animating!\n");
        }
      if (changed)
        {
          mRenderRequired = true;
	  setVisible(true);
        }
    }
  
  if (!mAnimating)
    { // Not animating, and rendering required, render all the tiles
      if (mRenderRequired)
        {
	  setVisible(true);
          renderAllTiles();
        }
      updateFade();  // But, in any case, update the fade out parameters
    }
  else
    { // Animating one of the icons! (Big in middle of screen, then slide
      // to corner while reducing to standard size for display in the frame.
      mRenderAlpha = 1.0f;
      t1 = tStart;
      elapsedTime = elapsedTimeSec(mT0, t1);
      if (elapsedTime >= mkAnimationDuration)
        {  // Animating done
          LOGI("Animation complete\n");
          mAnimating = false;
          mpAnimatingTile = NULL;
          mRenderRequired = true; // Should do change detection to set this!
          renderAllTiles();       // Regenerate image of all tiles
          updateFade();           // Start up fade out tracking
        }
      else
        {  // Compute animation parameters
          double k;
          int animationDir = 1;
          const int kCenterX = OUTPUT_WIDTH / 2;
          const int kCenterY = (OUTPUT_HEIGHT + OUT_PANO_RES_Y) / 2;

          // convert elapsed time to fraction complete
          k = elapsedTime / mkAnimationDuration;

          // account for direction of animation (might be used later)
          if (animationDir < 0)
            { k = 1.0 - k; }

          // interpolate between endpoints
          mAnimOrigin.x = k * mDestOrigin.x +
              (1.0 - k) * (kCenterX - mpAnimatingTile->getSize().width / 2);
          mAnimOrigin.y = k * mDestOrigin.y +
              (1.0 - k) * (kCenterY - mpAnimatingTile->getSize().height / 2);
          mAnimScale = k * mScale + (1.0 - k) * 1.0;
          mAnimSize = cv::Size(mpAnimatingTile->getSize().width,
                               mpAnimatingTile->getSize().height);

          // render the one tile to the status frame buffer
          mRenderRequired = true;  // Should do change detection to set this!
          mpAnimatingTile->render(0, mAnimSize.height, mAnimSize.width,
                                  mpFrameBuf);
          updateFade();      // Start up fade in tracking
        }
    }

  return mRenderRequired; // return flag indicating rendering required
}



/* *** cBannerLine *** */

/** Construct the banner line object and allocate resources for it.
 *
 * Loads the banner line bitmap data from the resource file, allocates
 * memory to hold the data and sets up the bounding rectangle.
 *
 */
cBannerLine::cBannerLine(const char *pBannerFile, cGpu *pGpu, cv::Point origin)
  : mAlpha(1.0),
    mNewFile(std::string("")),
    mNewUL(cv::Point(0,0)),
    mpGpu(pGpu),
    mReadyToLoad(false)
{
  int width;
  int height;

  // Load the icon bitmap
  pngFileGetImageSize((char *)pBannerFile,
                      &width,
                      &height);

  //mBoundingRect = cv::Rect(origin.x, origin.y, width, height);
  mWidth = width;
  mHeight = height;

  mpBuf = new uint32_t[width * height];

  pngFileRead((uint8_t *)mpBuf, (char *)pBannerFile);

  LOGI("loaded %dx%d logo '%s'\n", width, height, pBannerFile);
}

/** Destroy the banner line object.
 *
 * Deletes the allocated buffer that holds the banner line bitmap data.
 */
cBannerLine::~cBannerLine()
{
  delete [] mpBuf;
}

/** Prepare to load a replacement banner line.
 *
 * This routine tells the banner line object info about a new banner line
 * (or larger screen object), but it is not instantiated until loadNewFile()
 * is called from the camera image handler pipeline callback in cOwl.
 *
 * @param pBannerFile is the file containing the asset to load.
 * @param origin is the upper left coordinate of the start of the new item.
 */
void cBannerLine::setNewFileToLoad(const char *pBannerFile, cv::Point origin)
{
  int width;
  int height;

  // Check if a file is already marked for loading and wait for completion
  //   (by loadNewFile() running in another thread).
  // Timeout after a second and proceed, if necessary.
  for (int retries = 0; retries < 100; retries++)
    {
      if (mReadyToLoad)
        { usleep(10000); }
      else
        {
          break;
        }
    }

  mReadyToLoad = false;
  mNewFile = std::string(pBannerFile);
  mNewUL = origin;

  if (mNewFile.length() > 0)
    {
      // Load the icon bitmap
      pngFileGetImageSize((char *)mNewFile.c_str(),
                          &width,
                          &height);

      mWidth = width;
      mHeight = height;
      mUpperLeft = mNewUL;

      if (mpBuf != nullptr)
        { delete [] mpBuf; }

      mpBuf = new uint32_t[width * height];

      pngFileRead((uint8_t *)mpBuf, (char *)mNewFile.c_str());
      /* The file, as loaded and when displayed, is flipped horizontally.
       * This is done because most VC clients show the local user video
       * that is horizontally flipped to emulate looking in a mirror.
       * After loading, though, we might want to add text, so we flip
       * back to non-mirrored coordinates. After this we'll add text
       * overlays, then flip again to mirrored coordinates before
       * finally writing the buffer to a GPU texture for display.
       */
      hFlip();

      LOGI("loaded %dx%d file '%s'\n", width, height, (char *)mNewFile.c_str());
    }
}

/** Flip buffer contents horizontally.
 *
 * Required to flip bitmaps prior to adding text overlays.
 */
void cBannerLine::hFlip(void)
{
  uint32_t *pLeft;
  uint32_t *pRight;
  uint32_t tmp;

  for (int iR = 0; iR < mHeight; iR++)
    {
      pLeft = &mpBuf[iR * mWidth];
      pRight = &mpBuf[(iR + 1) * mWidth - 1];
      for (int iC = 0; iC < mWidth / 2; iC++)
        {
          tmp = *pLeft;
          *pLeft = *pRight;
          *pRight = tmp;
          pLeft++;
          pRight--;
        }
    }
}

/** Draw a text message into the banner line buffer.
 *
 * @param msg is the message string to write.
 * @param left is the left side of the bounding box of the text on screen.
 * @param bottom is the bottom coordinate of the text bounding box on screen.
 * @param color is an OpenCV color value to use to draw the text.
 *
 * @return true if the message fits on the screen, otherwise false.
 */
bool cBannerLine::drawTextMessage(std::string msg, int left, int bottom,
                                  cv::Scalar color)
{
  bool visible = false;

  if ((left >= 0) && (left < mWidth) && (bottom >= 0) && (bottom < mHeight))
    {
      visible = true;
      cv::Mat imgMat(mHeight, mWidth, CV_8UC4, mpBuf);

      cv::putText(imgMat, msg, cv::Point(left, bottom),
                  cv::FONT_HERSHEY_SIMPLEX,
                  0.6, color, 1, cv::LINE_8);
    }

  return visible;
}

/** Mark the buffer as ready to load into the GPU.
 *
 */
void cBannerLine::markFileReadyToLoad(void)
{
  hFlip();  // Flip back to original state of mirroring.
  mReadyToLoad = true;
}

/** Load new banner line into buffer, if needed.
 *
 * If a new file has been selected by a previous call to setNewFileToLoad()
 * then it is actually opened and loaded by this routine. This must be done
 * in the image pipeline for threading and GPU sequencing reasons.
 */
bool cBannerLine::loadNewFile()
{
  bool fileLoaded = false;
  cGpu *pGpu = mpGpu;

  if (mReadyToLoad)
    {
      pGpu->setRgbaTexture(RENDER_RGBA_OVERLAY, (uint8_t *)mpBuf, mWidth, mHeight);

      // mNewFile = std::string("");
      mReadyToLoad = false;

      fileLoaded = true;

      int yMax = WINDOW_RES_Y - mHeight;
      setDest(cv::Point(0, yMax));
      int yOrigin = ::loadParameter<int>(OVERLAY_MANAGER_PARAMS,
                                         "RuntimeRow", getDest().y);
      if (yOrigin > yMax) { yOrigin = yMax; }
      if (yOrigin < 0)    { yOrigin = 0; }
      setDest(cv::Point(0, yOrigin));

      float alpha = ::loadParameter<float>(OVERLAY_MANAGER_PARAMS,
                                           "RuntimeAlpha", getAlpha());
      setAlpha(MAX(alpha, getAlpha()));
    }

  return fileLoaded;
}


/* *** State update callbacks *** */

#ifndef __ANDROID__
// Stubs for linux build
int GetUsbMicActive(void) { return 1; }
int GetUsbSpkrActive(void) { return 1; }
#endif

/** Callback function to select speaker status icon.
 *
 * @return True if speaker is muted, False if streaming data.
 */
static int16_t speakerStreamingCallback(void)
{
  extern bool SpeakerStreaming;
  int16_t speakerStreaming = SpeakerStreaming & GetUsbSpkrActive();

  if (DEBUG(STATUS_FRAME))
    { speakerStreaming = SpeakerStreaming & GetUsbSpkrActive(); }
  else
    { speakerStreaming = GetUsbSpkrActive(); }

  if (speakerStreaming) { return 0; }
  else                  { return 1; }
}

/** Callback function to select mic status icon.
 *
 * @return True if mic is muted, False if streaming data.
 */
static int16_t micStreamingCallback(void)
{
  extern bool MicStreaming;
  int16_t micStreaming = MicStreaming;

  if (DEBUG(STATUS_FRAME))
    { micStreaming = MicStreaming & GetUsbMicActive(); }
  else
    { micStreaming = GetUsbMicActive(); }

  if (micStreaming) { return 0; }
  else              { return 1; }
}

#ifdef ENABLE_SIZE_TEST
/** Callback cycles through the icons in the size test tile.
 *
 * The size test loads icons with differing heights and widths to verify
 * that the status frame handles varying sizes correctly.
 */
static int16_t sizeTestCallback(void)
{
  static int counter = 0;

  if (counter == 950)
    {
      counter = 0;
    }
  else
    {
      counter++;
    }
  return counter / 300;
}
#endif /* ENABLE_SIZE_TEST */


/* *** Overlay Manager *** */

/** Construct the overlay manager object.
 *
 */
cOverlayManager::cOverlayManager(cGpu *pGpu, cSystemTest *pSystemTest)
  : mpGpu(pGpu),
    mpSystemTest(pSystemTest),
    mAveCycleTimeUsec(-1.0f),
    mMinCycleTimeUsec(100000.0f),
    mMaxCycleTimeUsec(0.0f)
{
  init();

#ifdef USE_UPDATE_THREAD
  pthread_mutex_init(&mUpdateMutex, NULL);
  pthread_cond_init(&mUpdateCond, NULL);
  mpUpdateThread = new cThread(
      std::bind(&cStatusFrame::updateCallback, this), "statusFrameUpd");
  mpUpdateThread->start();
#endif
}

/** Destroy the overlay manager object.
 *
 * Deletes the contained overlay display objects.
 */
cOverlayManager::~cOverlayManager()
{
#ifdef USE_UPDATE_THREAD
  mpUpdateThread->signalStop();
  lockFrameBuffer();
  unlockFrameBuffer();
  mpUpdateThread->join();
  delete mpUpdateThread;
  pthread_cond_destroy(&mUpdateCond);
  pthread_mutex_destroy(&mUpdateMutex);
#endif

  delete mpStatusFrame;
  delete mpBannerLine;
  delete mpMuteFrame;
}

#ifdef USE_UPDATE_THREAD
/** Callback for status frame update thread to call to update buffer.
 *
 * Waits until the mDoUpdate variable is set and signaled via the mUpdateCond.
 * Then updates the status frame image after which the thread loop will
 * call this function again.
 */
void cOverlayManager::updateCallback(void)
{
  static int count = 0;

  pthread_mutex_lock(&mUpdateMutex);
  while (!mDoUpdate)
    {
      pthread_cond_wait(&mUpdateCond, &mUpdateMutex);
    }
  update();
  pthread_mutex_unlock(&mUpdateMutex);

  count++;
  if (DEBUG(STATUS_FRAME) && ((count % 100) == 0))
    {
      LOGI("status frame update time: %f (%f, %f)\n",
           getAveUpdateTime(),
           getMinUpdateTime(),
           getMaxUpdateTime());
      resetTimeStats();
    }
}
#endif

/** Locks the mutex associated with the status frame.
 *
 * The client that uses the status frame buffer to display the
 * generated overlay should call this function to lock the buffer before
 * using the buffer to update the overlay. After the buffer has been
 * copied into the GPU, the unlockFrameBuffer() function should be called
 * to release the buffer for the next update. This function also starts a
 * timer for timing how long the complete status frame rendering takes.
 */
void cOverlayManager::lockFrameBuffer(void)
{
  clock_gettime(CLOCK_MONOTONIC, &mCycleStart);

#ifdef USE_UPDATE_THREAD
  pthread_mutex_lock(&mUpdateMutex);
#endif
}

/** Unlocks the mutex associated with the status frame.
 *
 * The client that uses the status frame buffer to display the
 * generated overlay should call this function to unlock the buffer after
 * using the buffer to update the overlay. This function also stops the
 * rendering timer and updates rendering time statistics.
 */
void cOverlayManager::unlockFrameBuffer(void)
{
  struct timespec tEnd;
  double elapsedTime;

#ifdef USE_UPDATE_THREAD
  pthread_cond_signal(&mUpdateCond);
  mDoUpdate = true;
  pthread_mutex_unlock(&mUpdateMutex);
#endif

  clock_gettime(CLOCK_MONOTONIC, &tEnd);
  elapsedTime = elapsedTimeSec(mCycleStart, tEnd);

  if (elapsedTime > mMaxCycleTimeUsec) { mMaxCycleTimeUsec = elapsedTime; }
  if (elapsedTime < mMinCycleTimeUsec) { mMinCycleTimeUsec = elapsedTime; }
  if (mAveCycleTimeUsec < 0.0f) { mAveCycleTimeUsec = elapsedTime; }
  else
    {
      const float kLatency = 32.0f; // 32 sample latency IIR average
      mAveCycleTimeUsec = ((mAveCycleTimeUsec * (kLatency - 1.0)) +
          elapsedTime) / kLatency;
    }
}

/** Resets the debugging timer accumulators.
 *
 */
void cOverlayManager::resetTimeStats(void)
{
  mAveCycleTimeUsec = -1.0f;
  mMinCycleTimeUsec = 1000000.0f;
  mMaxCycleTimeUsec = 0.0f;
}

/** Creates the banner line object.
 *
 * Note that delete mpBannerLine must be called to free the resources
 * allocated in this function.
 *
 * Loads the banner line graphic and determines the position for rendering.
 *
 * @returns Pointer to the newly created banner line.
 */
cBannerLine *cOverlayManager::initBannerLine(void)
{
  mpBannerLine = new cBannerLine(STARTUP_BACKGROUND, mpGpu, cv::Point(0,0));

  int yMax = WINDOW_RES_Y - mpBannerLine->getHeight();
  mpBannerLine->setDest(cv::Point(0, yMax));
  int yOrigin = ::loadParameter<int>(OVERLAY_MANAGER_PARAMS,
                                     "Row", mpBannerLine->getDest().y);
  if (yOrigin > yMax) { yOrigin = yMax; }
  if (yOrigin < 0)    { yOrigin = 0; }
  mpBannerLine->setDest(cv::Point(0, yOrigin));

  float alpha = ::loadParameter<float>(OVERLAY_MANAGER_PARAMS,
                                       "Alpha", mpBannerLine->getAlpha());
  mpBannerLine->setAlpha(alpha);

  return mpBannerLine;
}

/** Sets up the icon lists for each tile and creates the status frame object.
 *
 * Note that delete mpStatusFrame must be called to free the resources
 * created in this function.
 *
 * To create a status frame, a calling program first creates a list of
 * icon initializer objects for each tile, and creates the tile by passing
 * in this list along with the callback function that returns the selected
 * icon number each time it is called.
 *
 * After creating all the tiles, then they are added to a list which is used
 * to create the status frame.
 *
 * @return Pointer to the newly created status frame.
 */
cStatusFrame *cOverlayManager::initStatusFrame(void)
{
  // Create a list of icons for the speaker status tile and create the tile
  std::vector<cIconInitializer> speakerTileIcons;
  speakerTileIcons.push_back(cIconInitializer(SPEAKER_ON_ICON,
                                              false, false, false, true));
  speakerTileIcons.push_back(cIconInitializer(SPEAKER_OFF_ICON,
                                              true, false, false, false));
  cTile *pSpeakerTile = new cTile(speakerTileIcons, speakerStreamingCallback);

  // Create a list of icons for the mic status tile and create the tile
  std::vector<cIconInitializer> micTileIcons;
  micTileIcons.push_back(cIconInitializer(MIC_ON_ICON,
                                          false, false, false, true));
  micTileIcons.push_back(cIconInitializer(MIC_OFF_ICON,
                                          true, false, false, false));
  cTile *pMicTile = new cTile(micTileIcons, micStreamingCallback);

  // Create a list of tiles and add the tiles to it
  std::vector<cTile *> *pTiles = new std::vector<cTile *>;
  pTiles->push_back(pSpeakerTile);
  pTiles->push_back(pMicTile);

#ifdef ENABLE_SIZE_TEST
#ifdef __ANDROID__
#define CHEETAH_ICON "/system/media/OwlLabs/cheetah.png"
#define PIG_ICON     "/system/media/OwlLabs/pig.png"
#define TREE_ICON    "/system/media/OwlLabs/tree.png"
#else
#define CHEETAH_ICON "assets/cheetah.png"
#define PIG_ICON     "assets/pig.png"
#define TREE_ICON    "assets/tree.png"
#endif

  // Create a list of icons for the size test tile
  std::vector<cIconInitializer> sizeTestTileIcons;
  sizeTestTileIcons.push_back(cIconInitializer(CHEETAH_ICON,
                                               true, false, false, true));
  sizeTestTileIcons.push_back(cIconInitializer(PIG_ICON,
                                               true, false, false, false));
  sizeTestTileIcons.push_back(cIconInitializer(TREE_ICON,
                                               false, false, false, false));
  cTile *pSizeTestTile = new cTile(sizeTestTileIcons, sizeTestCallback);

  pTiles->push_back(pSizeTestTile);
#endif

  // Set up the status frame geometry
  const int STATUS_FRAME_UPPER_LEFT_X = 2 * SUBFRAME_BORDER_MARGIN;
  const int STATUS_FRAME_UPPER_LEFT_Y = 600;

  cv::Point upperLeft = { 0, 0 };
  float kScale = 0.5;

  // Create the status frame at the upper left of the screen
  mpStatusFrame = new cStatusFrame(upperLeft, kScale, pTiles);

  // Now use the size of the resulting frame to reposition it near the
  // lower left corner of the output frame.
  upperLeft.y = WINDOW_RES_Y;
  upperLeft.y -= (2 * SUBFRAME_BORDER_MARGIN);
  upperLeft.y -= (mpStatusFrame->getRectSize().height * kScale);
  upperLeft.x += (2 * SUBFRAME_BORDER_MARGIN);

  mpStatusFrame->setDest(upperLeft);

  return mpStatusFrame;
}


/** Sets up the icon lists for each tile and creates the mute status frame object.
 *
 * Note that delete mpMuteFrame must be called to free the resources
 * created in this function.
 *
 * @return Pointer to the newly created mute status frame.
 */
cStatusFrame *cOverlayManager::initMuteFrame(void)
{
  // Create tile for mic mute icon
  std::vector<cIconInitializer> micTileIcons;
  micTileIcons.push_back(cIconInitializer(MIC_MUTE_ICON,
					  false, false, false, false));
  cTile *pMicTile = new cTile(micTileIcons, []() { return (int16_t)0; });
  
  // Create a list of tiles and add the tiles to it
  std::vector<cTile *> *pTiles = new std::vector<cTile *>;
  pTiles->push_back(pMicTile);

  // Set up the status frame geometry
  cv::Point upperLeft = { 0, 0 };
  float kScale = 1.0f;

  // Create the status frame at the upper left of the screen
  cStatusFrame *pMuteFrame = new cStatusFrame(upperLeft, kScale, pTiles);

  // Now use the size of the resulting frame to reposition it at the top-right
  // of the screen.
  int muteIconWidth = pMuteFrame->getRectSize().width;
  int muteIconHeight = pMuteFrame->getRectSize().height;
  upperLeft.y = OUT_PANO_RES_Y +
                (SUBFRAME_BORDER_MARGIN * 3 / 2) -
                muteIconHeight / 2;
  upperLeft.x = (WINDOW_RES_X - muteIconWidth) / 2;
  pMuteFrame->setDest(upperLeft);

  return pMuteFrame;

}

/** Create the status frame and banner line objects.
 *
 */
void cOverlayManager::init(void)
{
  mpStatusFrame = initStatusFrame();
  mpBannerLine = initBannerLine();
  mpMuteFrame = initMuteFrame();
}

/** Update the RGBA overlay.
 *
 * The calling application should call this function each time an output frame
 * is rendered via the GPU. This function implements the update policy choosing
 * whether to display the status frame or the banner line, and updating the
 * buffered data as necessary.
 */
void cOverlayManager::update(unsigned char *mpTestOverlayImage,
                             int mTestOverlayImageWidth,
                             int mTestOverlayImageHeight,
			     bool muted )
{
  lockFrameBuffer();
  bool needsRender = false;
  static void *pMostRecentOverlay = NULL; // For deciding when to force updates
  static int count = 0;

  // Test mode output gets rendered preferentially, if test is running
  if (mpSystemTest->getTesting())
    {
      mpSystemTest->renderImage(mpTestOverlayImage);
      
      // Always re-render because test image may have changed
      mpGpu->setRgbaOverlay(mpTestOverlayImage, 0, 0,
                            mTestOverlayImageWidth,
                            mTestOverlayImageHeight,
                            OUTPUT_WIDTH,
                            OUTPUT_HEIGHT,
                            0.5);
      pMostRecentOverlay = reinterpret_cast<void *>(mpSystemTest);
    }
  else   // Not test mode, so display status frame or banner line
    {
#ifndef DISABLE_STATUS_FRAME
#ifndef USE_UPDATE_THREAD
      needsRender = mpStatusFrame->update(needsRender);
#endif
      
      if (pMostRecentOverlay != reinterpret_cast<void *>(mpStatusFrame))
	{ // status frame needs to be rendered if not most recently drawn
	  needsRender = true;
	}

      if (mForceRender || (needsRender && mpStatusFrame->visible()))
        {  // Status frame is visible and changed, so send it to the GPU
          mpGpu->setRgbaOverlay((unsigned char *)mpStatusFrame->getFrameBuffer(),
                                mpStatusFrame->getDest().x,
                                mpStatusFrame->getDest().y,
                                mpStatusFrame->getRectSize().width,
                                mpStatusFrame->getRectSize().height,
                                mpStatusFrame->getRectSize().width *
                                mpStatusFrame->getScale(),
                                mpStatusFrame->getRectSize().height *
                                mpStatusFrame->getScale(),
                                mpStatusFrame->getAlpha());
          // Remember that the status frame was most recently shown
          pMostRecentOverlay = reinterpret_cast<void *>(mpStatusFrame);
        }
      count++;
      if (DEBUG(STATUS_FRAME) && ((count % 100) == 0))
        {
          LOGI("overlay cycle update time: %f (%f, %f)\n",
               getAveCycleTime(),
               getMinCycleTime(),
               getMaxCycleTime());
          resetTimeStats();
          count = 0;
        }
#endif /* DISABLE_STATUS_FRAME */
      if (!mpStatusFrame->visible())
        {  // No status frame to show, so display the feedback banner instead
	  
	  if(muted)
	    { // show mute frame instead of banner
	      if (mForceRender ||
	          pMostRecentOverlay != reinterpret_cast<void *>(mpMuteFrame))
                {
                  mpMuteFrame->resetAnimation();
                  mpMuteFrame->setVisible(true);
                }
	      
#ifndef USE_UPDATE_THREAD
              needsRender = mpMuteFrame->update(needsRender);
#endif
	      if (mForceRender || (needsRender && mpMuteFrame->visible()))
		{
		  mpGpu->setRgbaOverlay((unsigned char *)mpMuteFrame->getFrameBuffer(),
                                mpMuteFrame->getDest().x,
                                mpMuteFrame->getDest().y,
                                mpMuteFrame->getRectSize().width,
                                mpMuteFrame->getRectSize().height,
                                mpMuteFrame->getRectSize().width *
                                mpMuteFrame->getScale(),
                                mpMuteFrame->getRectSize().height *
                                mpMuteFrame->getScale(),
                                mpMuteFrame->getAlpha());
		  // Remember that the mute frame was most recently shown
		  pMostRecentOverlay = reinterpret_cast<void *>(mpMuteFrame);
		}
	    }
	  else
	    {
	      if (mForceRender ||
	          pMostRecentOverlay != reinterpret_cast<void *>(mpBannerLine))
		{
		  mpMuteFrame->setVisible(false);
		  
#ifndef USE_UPDATE_THREAD
                  needsRender = mpMuteFrame->update(needsRender);
#endif
		  mpGpu->setRgbaOverlay((unsigned char *)mpBannerLine->getFrameBuffer(),
					mpBannerLine->getDest().x,
					mpBannerLine->getDest().y,
					mpBannerLine->getWidth(),
					mpBannerLine->getHeight(),
					mpBannerLine->getWidth(),
					mpBannerLine->getHeight(),
					mpBannerLine->getAlpha());
		  // Remember that the banner line was most recently shown
		  pMostRecentOverlay = reinterpret_cast<void *>(mpBannerLine);
		}
	    }
        }
    }

  mForceRender = false;

  unlockFrameBuffer();
}

