#include "faceTracker.hpp"
#include "logging.hpp"
#include "params.h"
#include "debugSwitches.h"
#include "common.hpp"
#include "circleMath.hpp"

#include <cmath>
#include <algorithm>

cFaceTracker::cFaceTracker(void)
{
  reset();
}

/** @param face is the initial OpenCV rectangle that represents the location/size
 * of the assumed face.
 * @param spaceSize is the pixel bound beyond which the tracker can't track.
 */
cFaceTracker::cFaceTracker(cv::Rect face, cv::Size spaceSize)
{
  mSpaceSize = spaceSize;
  setFace(face);
}

cFaceTracker::~cFaceTracker()
{
}

/** Set the tracked faced representation to the provided rectangle.
 *
 * @param face is an OpenCV rectangle that represents the location/size of the
 * face that is being tracked.
 */
void cFaceTracker::setFace(cv::Rect face)
{
  reset();
  updateFace(face);
  mScore = 3;
}

/** Set the pixel boundaries where a face can be tracked 
 *
 * @param spaceSize is thepixel bound beyond which the tracker can't track.
 */
void cFaceTracker::setSpaceSize(cv::Size spaceSize)
{
  mSpaceSize = spaceSize;
}

/** Give the face tracker potential face candidates so that it can adjust the
 * center of attention as the tracked face potentially moves.
 *
 * Face rectangles may match this tracker and cause the center of focus to be
 * updated.  Rectangles that do no matche are returned to the caller so that 
 * other face trackers (if they exist) may see if these rectangles are a good
 * match for them.
 *
 * @param faces is a vector of OpenCV rectangles that represent the locations
 * and sizes of all of the faces that may have been found in the searched image.
 *
 * @return the set of face rectangles that did not match this tracker.
 */
std::vector<cv::Rect> cFaceTracker::update(std::vector<cv::Rect> faces)
{
  const int kMatchVal = 3;
  const int kLeakVal = 1;
  const int kMaxVal = 6;
  const int kMinVal = -1;

  // set of faces, less the ones that matched this tracker
  std::vector<cv::Rect> unmatchedFaces;

  for(std::vector<cv::Rect>::iterator iFace = faces.begin(); 
      iFace != faces.end(); ++iFace )
    {
      // TODO: Make face class that contains bounding Rect, center Point, etc. 
      cv::Point faceCenter((iFace->x + (int)(iFace->width / 2)),
			   (iFace->y + (int)(iFace->height / 2)) );

      int dx = std::min(mSpaceSize.width - faceCenter.x + mFocus.x,
			mSpaceSize.width + faceCenter.x - mFocus.x );
      dx = std::min(dx, std::abs(faceCenter.x - mFocus.x));

      // TODO: Put this into a common math file
      float distance = std::sqrt(std::pow(dx, 2) + 
				 std::pow((faceCenter.y - mFocus.y), 2) );
      
      if(distance < mTolerance || mTolerance == 0)
	{ // found matching face (tolerance == 0 indicates tracker is new and
	  // needs to be seeded with an initial face to track)
	  updateFace(*iFace);
	  mScore += kMatchVal;	  
	}
      else
	{ // store face in updated rev of data set
	  unmatchedFaces.push_back(*iFace);
	}
    }

  // update score
  mScore -= kLeakVal;
  mScore = CLAMP(kMinVal, mScore, kMaxVal);

  // update tracking state
  setState();

  if(DEBUG(FACE_TRACKER)) {
    LOGI("face score = %d\n", mScore); }

  return unmatchedFaces;
}

/** Check if the new focus has shifted by more than the given threshold.
 *
 * @param threshold is the amount (in the x-direction only) by which the focus
 * has to have moved in order to be considered shifted.
 * @param reset is a flag that causes the focus point of the tracker to be zeroed
 * in the event that focus has shifted.
 *
 * @returns whether or not the focus has changed.
 */
bool cFaceTracker::isFocusShifted(int threshold, bool reset)
{
  if(std::abs(mFocusShift.x) > threshold)
    {
      if(reset)
	{mFocusShift.x = 0; mFocusShift.y = 0;}

      return true;
    }
  return false;
}

/** Return the tracker state (either the face is tracked or lost). */
faceState_t cFaceTracker::getState(void)
{
  return mState;
}

/** Return the OpenCV rectangle that represents the size & location of the 
    currently tracked face. */
cv::Rect cFaceTracker::getFace(void)
{
  return mFace;
}

/** Return the OpenCV Point that represents the center of attention as 
    determined by the tracker. */
cv::Point cFaceTracker::getFocus(void)
{
  return mFocus;
}

/** This function uses a simple threshold to determine if the tracker is 
    currently tracking a face or not, and stores the resulting state. */
void cFaceTracker::setState(void)
{
  const int kThreshold = 0;

  if(mScore > kThreshold)
    {
      mState = faceState::TRACKING;
    }
  else
    {
      mState = faceState::LOST;
    }
}

/** This function resets all the paramater values that are used by the tracker.*/
void cFaceTracker::reset(void)
{
  mScore = 0;
  mTolerance = 0;
  mState = faceState::UNKNOWN;
  mFocusShift.x = 0; 
  mFocusShift.y = 0;
  mFace = cv::Rect(0,0,0,0);
}

/** This function used internally to make adjustments to tracker's parameter
 * values based on the provided face rectangle.
 *
 * @param face is an OpenCV rectangle that represents the rectangle that is
 * currently represets where the face is believed to be.
 */
void cFaceTracker::updateFace(cv::Rect face)
{
  const float kToleranceScalar = 1.0;

  // update how much the focus has shifted
  cv::Point faceMiddle(circleMath::intAdd(face.x, (int)(face.width / 2), 
					  CV_PANO_RES_X ),
		       face.y + (int)(face.height / 2) );
  
  mFocusShift.x = circleMath::intAdd(mFocusShift.x, 
				     circleMath::intSub(faceMiddle.x, mFocus.x, 
							CV_PANO_RES_X ),
				     CV_PANO_RES_X );
  mFocusShift.y += (faceMiddle.y - mFocus.y);
  
  // update the location of the current focus
  mFace = face;
  mFocus.x = circleMath::intAdd(mFace.x, (int)(mFace.width / 2), CV_PANO_RES_X);
  mFocus.y = mFace.y + (int)(mFace.height / 2);

  // update tolerance (used to identify consistent face) 
  mTolerance = kToleranceScalar * ((mFace.height + mFace.width) / 2);
}
