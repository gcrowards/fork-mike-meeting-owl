#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <fstream>
#include <ctime>

#include "opencv2/videoio/videoio_c.h"

#include "aspectMath.hpp"
#include "common.hpp"
#include "imageFileUtils.hpp"
#include "logging.hpp"
#include "owlHook.hpp"
#include "params.h"
#include "systemtest/systemTest.hpp"


cOwlHook::cOwlHook()
  : cOwlHook("")
{ }

cOwlHook::cOwlHook(const std::string &mockInputDirectory)
  : mMockInputDirectory(mockInputDirectory),
    mpSystemTest(new cSystemTest()),
    mpOverlayManager(NULL)
{
  init();
}

cOwlHook::~cOwlHook()
{
  // clean up the hardware UI handler
  delete mpUiHandler;
  
  // clean up instance of the system test
  delete mpSystemTest;
  
  // clean up instance of the core application
  delete mpOwl;
  
  // clean up mpSysfs
  delete mpSysfs;

  // clean up the overlay manager
  delete mpOverlayManager;

  // release panorama image buffer
  free(mpCvPano);
  // release mock image buffer
  free(mpMockCameraImage);
  // release factory test image buffer
  free(mpRgbaOverlayImage);
}

/** This function encapulates common setup for the class. */
void cOwlHook::init(void)
{
  // initialize loudspeaker status
  mSpeakerStatus.mActive = false;
  mSpeakerStatus.mLevelLeft = 0;
  mSpeakerStatus.mLevelRight = 0;
  // initialize microphone status
  mMicStatus.mActive = false;
  mMicStatus.mLevelLeft = 0;
  mMicStatus.mLevelRight = 0;

  mFps = -1;
  
  // setup the cSysfs instance, which automatically polls for changes
  // to any of the supported variables
  mpSysfs = new cSysfs();

  // initialize mic volume
  mpSysfs->setMicVolume(MIC_VOLUME_DEFAULT);
  LOGD("mic volume: %d\n", mpSysfs->getMicVolume());

  // check that CAMERA_HEIGHT is multiple of 4 so camera texture works properly
  if ( CAMERA_HEIGHT != 4 * (CAMERA_HEIGHT / 4) )
    { LOGE("CAMERA_HEIGHT must be a multiple of 4\n"); }

  // allocate CV pano buffer; buffer from GPU is RGBA, therefore 4 bytes/pixel
  mpCvPano = (unsigned char *) malloc(CV_PANO_RES_X * CV_PANO_RES_Y * 4);
  if (mpCvPano == NULL)
    LOGE("unable to allocate CV panorama buffer");

#if defined(__ANDROID__) && !defined(MOCK_INPUT)
  // real input -- no mock input setup
#else // linux or MOCK_INPUT
  // check the mock input directory for required files
  checkMockDirectory();

  // allocate mock image buffer
  mpMockCameraImage = (unsigned char *)malloc(CAMERA_WIDTH * CAMERA_HEIGHT * 3);
  if (mpMockCameraImage == NULL)
    {LOGE("unable to allocate mock image buffer\n");}
  mMockFrameNum = 0;
#endif
  
  mRgbaOverlayImageWidth = OUTPUT_WIDTH;
  mRgbaOverlayImageHeight = OUTPUT_HEIGHT;
  mpRgbaOverlayImage = (unsigned char*)malloc(mRgbaOverlayImageWidth *
					      mRgbaOverlayImageHeight * 4 );
  
  // reserve memory for overlay shape vectors, to eliminate re-allocation copies
  mOverlayLines.reserve(OVERLAY_SHAPE_VECTOR_SIZE);
  mOverlayRects.reserve(OVERLAY_SHAPE_VECTOR_SIZE);
  mOverlayEllipses.reserve(OVERLAY_SHAPE_VECTOR_SIZE);
  mBufferGraphs.reserve(OVERLAY_SHAPE_VECTOR_SIZE);

  // set up the panorama subframe for drawing the panorama strip on the system
  //  test.
  // TODO: Consider adding a base class for cOwl and cSystemTest for polymorphism
  //        in cOwlHook. The code for drawing the panorama should be unified in
  //        there (not enough time for Beta 1.1).
  mPanoSubframe = cSubframe(true);
  mPanoSubframe.setPanorama(&mPano);

  mPanoSubframe.setPos(cv::Point((int)(OUTPUT_WIDTH / 2.0f),
				 PANORAMIC_VERT_MARGIN ) );

  mPanoSubframe.setSize(cv::Size(OUT_PANO_RES_X, OUT_PANO_RES_Y));
  mPanoSubframe.setFocus(cv::Point((CV_PANO_RES_X / 2.0f), 0));

  mPanoSubframe.setLocked();

  // create an instance of the core application
  mpOwl = new cOwl(&mVirtualMic, mpSysfs);

  // set up the hardware UI handler
  mpUiHandler = new cUiHandler(&mButtonHandler, &mLedInterface);

  // retrieve a pointer to the analytics object
  cAnalytics *pAnalytics = mpOwl->getAnalyticsObject();
  // ... and tell the UiHandler about it
  mpUiHandler->setAnalytics(pAnalytics);
  // ... and tell the analytics to call back to cOwl::setAnalytics on meeting
  // boundaries
  pAnalytics->setAnalyticsCallback(std::bind(&cOwl::setAnalytics, mpOwl,
					    std::placeholders::_1,
					    std::placeholders::_2));

  // start with test mode disabled (connects button and led contexts from cOwl)
  setSystemTestMode(false);
  
  // start listening for buttons
  mpUiHandler->start();
}

/** This function sets a pointer to the cGpu instance so the cOwlHook
 *  instance can talk to it.
 *
 * @param pGpu is a pointer to the cGpu instance.
 */
void cOwlHook::setGpu(cGpu *pGpu)
{
  // -- store a pointer to the cGpu instance; then finish its initialization
  mpGpu = pGpu;

  // make sure an overlay manager does not already exist, then create fresh.
  if (mpOverlayManager != NULL) { delete mpOverlayManager; }
  mpOverlayManager = new cOverlayManager(mpGpu, mpSystemTest);
  mRgbaOverlays.mpOverlayManager = mpOverlayManager;

  // load PNG logo and transfer to GPU
  loadLogo();

  // load PNG pinned AOI image and transfer to GPU
  loadAoiPin();

  // set the camera center
  float cx, cy;
  aspectMath::getCameraCenter(&cx, &cy);
  mpGpu->setCameraCenter(cx, cy);
  LOGI("camera center set to (%6.4f, %6.4f)\n", cx, cy);
  LOGI("camera max radius is %6.4f\n", aspectMath::getMaxRadius());
}

void cOwlHook::setRgbaImage(unsigned char *image, int width, int height)
{
  mRgbaOverlayImageWidth = width;
  mRgbaOverlayImageHeight = height;
  mpRgbaOverlayImage = image;
}


/** This function is called repeatedly as part of the main A/V
 *  pipeline and, therefore, must not block or take a long time.
 *
 * @param pEglClientBuffer is the camera image buffer transferred via
 * the camera frame callback in main.cpp.
 */
void cOwlHook::updateGpu(void *pEglClientBuffer)
{
  double dt;
  static int count = 0;              // update counter
  const int kLogFpsPeriod = 100;     // period is in number of update cycles
  static struct timespec t0, t1;
  std::vector<owl::abcShape*> shapes;
  std::vector<owl::cBufferGraph*> graphs;
  owl::cRgbaOverlayLists *pOverlays;

  // ensure that the GPU and overlay manager are fully set up
  if ((mpGpu == NULL) || (mpOverlayManager == NULL)) { return; }

  // Forward the grab-fisheye flag to the GPU object to take action
  if ((mpOwl != NULL) && (mpOwl->grabFisheye()))
    {
      mpGpu->saveNextFisheye();
    }

  shapes = mOverlayShapes;
  graphs = mOverlayGraphs;
  pOverlays = &mRgbaOverlays;
  /* Note: If video is muted, then do the following instead of the
   * above two lines:
   * owl::cRgbaOverlayLists emptyOverlays;
   * pOverlays = &emptyOverlays;
   */
  
  // Note: for future use, this is a good place to manage the RGBA image
  // overlay using mpGpu->setRgbaOverlay() and mpGpu->clearRgbaOverlay().
  // Avoid loading images from file here, since that would likely introduce
  // a noticeable frame delay.
  // The overlay manager displays the system test overlay, if active, otherwise
  // the status frame, if visible, otherwise the banner line.
  mpOverlayManager->update(mpRgbaOverlayImage,
                           mRgbaOverlayImageWidth,
                           mRgbaOverlayImageHeight,
			   mpOwl->isMuted() );

  // update the GPU and render output
#if defined(__ANDROID__) && !defined(MOCK_INPUT)
  mpGpu->update(pEglClientBuffer, mCompositePanels, shapes, graphs, pOverlays);
#else // linux or MOCK_INPUT
  // need just one update with JPEG input, after it's loaded.
  // Otherwise, there's no need to keep making a costly texture
  // transfer, so pass a NULL image buffer instead of the mock image.
  // However, we do the texture transfer every frame if the input is
  // an AVI file.
  if (count == 1 || mMockInputAvi)
    { mpGpu->update(pEglClientBuffer, mCompositePanels, shapes, graphs, pOverlays); }
  else
    { mpGpu->update(NULL, mCompositePanels, shapes, graphs, pOverlays); }
#endif

  if(!mSystemTestMode)
    { // not in system test -- copy panoramic strip from GPU
      mpGpu->copyOutput(mpCvPano);
    }
  
  // clear the overlays
  clearOverlays();
  
  if(mSystemTestMode != mpSystemTest->getTesting())
    { // toggle the system test
      mpSystemTest->setTesting(mSystemTestMode);
    }

  if(mSystemTestMode)
    {
      mpSystemTest->update();

      // the only panel other than the test image overlay should be the panorama
      mCompositePanels.clear();
      mCompositePanels.push_back(mPanoSubframe.getPanel());
    }
  else
    {
       // update owl-core
       mCompositePanels = mpOwl->update(mpCvPano,
                                        &mRgbaOverlays,
                                        mSpeakerStatus, mMicStatus,
       				        mFps );
       // if a new banner line has been selected, here's where it gets loaded
       if (mRgbaOverlays.mpOverlayManager->getBannerLine()->loadNewFile())
         { mRgbaOverlays.mpOverlayManager->forceRender(); }
    }

  // slow down the update if necessary
#if defined(__ANDROID__) && !defined(MOCK_INPUT)
  // run at full speed allowed by libOwlPG camera handler
#else // linux or MOCK_INPUT
  capFrameRate(30); // cap at 30 FPS
#endif

  // log frame rate
  if (count % kLogFpsPeriod == 0)
    {
      currentTimeSpec(&t1);
      dt = elapsedTimeSec(t0, t1);
      currentTimeSpec(&t0);
      
      if (count > 0)
	{
	  // calc fps
	  mFps = (float)(kLogFpsPeriod / dt);
	  
	  if ( !DEBUG(SOUND_LOCALIZER) ) // disable when visualizing localizer
	    {
	      LOGV("Frame rate: %.1f FPS\n", mFps);
	    }
	}
    }

  count++;
  mMockFrameNum++;
}

/** This function is called repeatedly on a separate thread for audio
 * input processing; it runs at a slower rate than the main A/V
 * pipeline and is intended for sound localization.
 *
 * Note that access to mVirtualMic is guarded by a mutex.
 *
 * @param pPcmBuf is a pointer to the PCM sound data transferred via
 * the owlUpdate callback in main.cpp.
 * @param bufLen is the data buffer length.
 */
void cOwlHook::updateLocalize(int16_t *pPcmBuf, int bufLen)
{
  if(mSystemTestMode)
    {
      // skip localization and pass the data to the system test instead
      mpSystemTest->micDataCallback(pPcmBuf, bufLen);
    }
  else
    {
      int angle = NO_SOURCE_ANGLE;
      std::vector<int> soundBearings;
      bool doneAggregating;

#if defined(SIMULATE_PCM_DATA) || (defined(__ANDROID__) && !defined(MOCK_INPUT))
      doneAggregating = false; // controls virtual mic update below
      // pass the data to the sound localizer instance for processing; the
      // update returns true when it's done aggregating several chunks of
      // audio data
      if (mSndLoc.update(pPcmBuf, bufLen))
	{
      doneAggregating = true; // triggers virtual mic update below
      // check quality of localization results
      if ( !AMIsSpeakerActive() &&
           mSndLoc.getDispersion() < BEAM_DISPERSION_THRESHOLD )
	{ angle = mSndLoc.getAngle(); }
	}
#else // linux or MOCK_INPUT
      doneAggregating = true;  // forces virtual mic update below
      if(mMockSoundFilepath != "")
	{
	  // use data from the test file instead of the localizer.
	  angle = getMockSoundAngle();
	}
#endif

      // check for valid angle
      if(angle >= 0 && angle <= 360)
	{
	  // for now, make angle the voice bearing and the only sound bearing
	  soundBearings.push_back(angle);

	  // for now, jump the beamforming angle to the new sound
	  // bearing TODO: beamform whatever is on the stage, probably
	  // in cOwl::update() and with changes to cBeamform to
	  // support multiple beams
	  mAudioMgr.setAngle(angle);
	}

      // check if it's time to pass results to the virtual mic
      if (doneAggregating)
	{
	  // the virtual mic needs to receive both valid and invalid
	  // angles to update the sound map properly
	  mVirtualMic.update(angle, soundBearings);
	}
    }
}

/** This is a pass-through function from a libOwlPG callback to the
 * cBeamform instance for audio output processing; it runs on a
 * high-priority thread as part of the main A/V pipeline and is
 * intended for quickly assembling the UAC sound output.
 *
 * @param pPcmOut is a pointer to the PCM sound output data, expected
 * to be two interleaved channels.
 * @param pPcmIn is a pointer to the PCM sound input data, expected to
 * be N_MICS interleaved channels.
 * @param bufLen is the data buffer length.
 */
void cOwlHook::processAudio(int16_t *pPcmOut, int16_t *pPcmIn, int16_t *pRefIn, int bufLen)
{
  float rms, peak;
  int level;
  const float kMinRmsDb = -60.0; // Minimum RMS level to consider

  mAudioMgr.update(pPcmOut, pPcmIn, pRefIn, bufLen);

  // Update the mic-active detector
  mAudioMgr.getOutputLevels(&rms, &peak); // Retrieves RMS of most recent output

  // RMS values are negative, with quieter sounds increasingly negative.
  // Level used by updateMicActive is positive, maxing out around 1024.
  // This next line translates from RMS to level.
  level = (int)((1024.0 / fabs(kMinRmsDb)) * fmax((fabs(kMinRmsDb) + rms), 0.0));
  updateMicActive(level, level);
}

/** This function receives status from libOwlPG about whether the
 * loudspeaker is active, with some minor pre-processing in main.cpp.
 *
 * This function is called on a high-priority thread as part of the
 * main A/V pipeline and must return quickly.
 *
 * @param leftChanLevel is level of activity on loudspeaker left channel.
 * @param rightChanLevel is level of activity on loudspeaker right channel.
 */
void cOwlHook::updateSpeakerActive(int leftChanLevel, int rightChanLevel)
{
  const int kChanThreshold = 100;
  const int kActiveLatchLength = 10;
  static int  nQuiet = 0;

  // store the channel activity
  mSpeakerStatus.mLevelLeft  = leftChanLevel;
  mSpeakerStatus.mLevelRight = rightChanLevel;

  // check if estimate of loudspeaker energy is high
  if ( leftChanLevel > kChanThreshold
       || rightChanLevel > kChanThreshold )
    {
      mSpeakerStatus.mActive = true;
      nQuiet = 0;
    }
  else if (mSpeakerStatus.mActive)
    {
      // check if estimate of loudspeaker energy has been low long
      // enough to turn off the active flag
      nQuiet++;
      if (nQuiet >= kActiveLatchLength)
	{
	  mSpeakerStatus.mActive = false;
	  nQuiet = 0;
	}
    }
  // TODO: confirm that the two const values above work well across
  // various user tests and remove the commented out if-then-LOGD call
  // below (left for debugging convenience for now)
  //if (leftChanLevel > 0 || rightChanLevel > 0)
  //  { LOGD("LOUDSPEAKER   left: %4d   right: %4d   nQuiet: %d   active: %s\n",
  //	   leftChanLevel, rightChanLevel, nQuiet,
  //	   mSpeakerActive ? "true" : "false"); }

  // inform the UI handler of the current state of the speaker channel
  mpUiHandler->setSpeakerActive(mSpeakerStatus.mActive);
}

/** This function receives status from the audio manager and decides
 * if there is sufficient sound to indicate that the room is occupied.
 *
 * @param leftChanLevel is level of activity on mic left channel.
 * @param rightChanLevel is level of activity on mic right channel.
 */
void cOwlHook::updateMicActive(int leftChanLevel, int rightChanLevel)
{
  const int kChanThreshold = 350;
  const int kActiveLatchLength = 10;
  const int B = 4;   // IIR low pass filter latency = 1 << B
  static int lAccum = -1;
  static int rAccum = -1;
  static int  nQuiet = 0;
  static int prevL = 0, prevR = 0;
  static bool prevA = false;
  int changed;
  int lFilt;
  int rFilt;

  // store the channel activity
  mMicStatus.mLevelLeft  = leftChanLevel;
  mMicStatus.mLevelRight = rightChanLevel;

  // Determine if levels have changed since last call. (When they don't
  // that's a pretty clear indication that the audio has been stopped, even
  // though more packets are sent through.)
  changed = (leftChanLevel != prevL) || (rightChanLevel != prevR);

  // IIR low pass filter on each channel:
  //         f(n) = ((f(n-1) * (N-1)) + s(n)) / N
  //             where f(n) is filter at new time, f(n-1) previous time
  //                   s(n) is new signal value, N # of samples latency
  //         N * f(n) = ((N * f(n-1)) - ((N * f(n-1)) / N)) + s(n)
  //             define A(x) = N * f(x) gives
  //         A(n) = A(n-1) - (A(n-1) / N) + s(n)
  //             When N is the B'th power of 2
  //         A(n) = A(n-1) - (A(n-1) >> B) + s(n)
  //             And
  //         f(n) = A(n) >> B
  if (lAccum == -1)
    {
      lAccum = leftChanLevel << B;
      rAccum = rightChanLevel << B;
    }
  lAccum = lAccum - (lAccum >> B) + leftChanLevel;
  rAccum = rAccum - (rAccum >> B) + rightChanLevel;

  lFilt = lAccum >> B;
  rFilt = rAccum >> B;

  // check if estimate of microphone energy is high
  if ( (lFilt > kChanThreshold
        || rFilt > kChanThreshold )
      && changed)
    {
      mMicStatus.mActive = true;
      nQuiet = 0;
    }
  else if (mMicStatus.mActive)
    {
      // check if estimate of mic energy has been low long
      // enough to turn off the active flag
      nQuiet++;
      if (nQuiet >= kActiveLatchLength)
        {
          mMicStatus.mActive = false;
          nQuiet = 0;
        }
    }

  // TODO: confirm that the three const values above work well across
  // various user tests and remove the commented out if-then-LOGD call
  // below (left for debugging convenience for now)
//  if ( (leftChanLevel > 0 || rightChanLevel > 0) && (prevA == true))
//    { LOGD("MICROPHONE   left: %4d   right: %4d   nQuiet: %d   active: %s\n",
//         lFilt, rFilt, nQuiet,
//         mMicStatus.mActive ? "true" : "false"); }

  prevL = leftChanLevel;
  prevR = rightChanLevel;
  prevA = mMicStatus.mActive;

  // inform the UI handler of the current state of the mic channel
  mpUiHandler->setMicActive(mMicStatus.mActive);
}
/** resets the sound localizer in the event of major owl movements
*/
void cOwlHook::resetSoundLocalizer()
{
  mSndLoc.reset();
}

/** returns pointer to sound localizer object */
cSoundLocalizer *cOwlHook::getSoundLocalizer()
{
  return &mSndLoc;
}

/* returns pointer to double talk detector owned by audio manager */
cAudioDTD *cOwlHook::getDTD()
{
  return mAudioMgr.getDTD();
}

/* returns pointer to double talk detector owned by audio manager */
cAudioMgr *cOwlHook::getAM()
{
  return &mAudioMgr;
}



/**************************/
/* overlay shapes support */
/**************************/

/** Clear the overlay shape vectors. */
void cOwlHook::clearOverlays(void)
{
  mOverlayShapes.clear();
  mOverlayGraphs.clear();
  mOverlayLines.clear();
  mOverlayRects.clear();
  mOverlayEllipses.clear();
}

/** Add an ellipse to the queue of shapes that need drawing.
 *
 * @param shape is the ellipse object.
 */
void cOwlHook::queueOverlay(owl::cEllipse shape)
{
  owl::abcShape *pShape;
  if (mOverlayEllipses.size() < OVERLAY_SHAPE_VECTOR_SIZE)
    {
      // push the shape
      mOverlayEllipses.push_back(shape);
      // push a pointer to the copied shape
      pShape = &(mOverlayEllipses.back());
      mOverlayShapes.push_back(pShape);
    }
  else
    { 
      // it's a nice idea to print a warning message here, but that can result
      // in a massive number of console prints, which can bring the programe
      // to a halt.
    }
}

/** Add a line to the queue of shapes that need drawing.
 *
 * @param shape is the line object.
 */
void cOwlHook::queueOverlay(owl::cLine shape)
{
  owl::abcShape *pShape;
  if (mOverlayLines.size() < OVERLAY_SHAPE_VECTOR_SIZE)
    {
      // push the shape
      mOverlayLines.push_back(shape);
      // push a pointer to the copied shape
      pShape = &(mOverlayLines.back());
      mOverlayShapes.push_back(pShape);
    }
  else
    { 
      // it's a nice idea to print a warning message here, but that can result
      // in a massive number of console prints, which can bring the programe
      // to a halt.
    }
}

/** Add a rect to the queue of shapes that need drawing.
 *
 * @param shape is the rect object.
 */
void cOwlHook::queueOverlay(owl::cRect shape)
{
  owl::abcShape *pShape;
  if (mOverlayRects.size() < OVERLAY_SHAPE_VECTOR_SIZE)
    {
      // push the shape
      mOverlayRects.push_back(shape);
      // push a pointer to the copied shape
      pShape = &(mOverlayRects.back());
      mOverlayShapes.push_back(pShape);
    }
  else
    {
      // it's a nice idea to print a warning message here, but that can result
      // in a massive number of console prints, which can bring the programe
      // to a halt.
    }
}


/************************/
/* public drawing hooks */
/************************/

void cOwlHook::drawEllipse(int x, int y, int width, int height,
			   float r, float g, float b, float alpha,
			   float lineWidth, bool filled)
{
  owl::cEllipse ellipse = owl::cEllipse(x, y, width, height);
  ellipse.setColor(r, g, b, alpha);
  ellipse.setLineWidth(lineWidth);
  ellipse.setFilled(filled);
  queueOverlay(ellipse);
}

void cOwlHook::drawEllipseFilled(int x, int y, int width, int height,
				 float r, float g, float b, float alpha)
{
  drawEllipse(x, y, width, height, r, g, b, alpha, 1.0, true);
}

void cOwlHook::drawLine(int x0, int y0, int x1, int y1,
			float r, float g, float b, float alpha,
			float lineWidth)
{
  owl::cLine line = owl::cLine(x0, y0, x1, y1);
  line.setColor(r, g, b, alpha);
  line.setLineWidth(lineWidth);
  line.setLineWidth(lineWidth);
  queueOverlay(line);
}

void cOwlHook::drawRectangle(int left, int top, int width, int height,
			     float r, float g, float b, float alpha,
			     float lineWidth, bool filled)
{
  owl::cRect rect = owl::cRect(left, top, width, height);
  rect.setColor(r, g, b, alpha);
  rect.setLineWidth(lineWidth);
  rect.setFilled(filled);
  queueOverlay(rect);
}

void cOwlHook::drawRectangleFilled(int left, int top, int width, int height,
				   float r, float g, float b, float alpha)
{
  drawRectangle(left, top, width, height, r, g, b, alpha, 1.0, true);
}

/** Creates a graph buffer.
 *
 * @param pGraphData a pointer to the graph vertex data.
 */
int cOwlHook::createBufferGraph(const std::vector<sVertex> *pGraphData)
{
  mBufferGraphs.emplace_back(pGraphData, mpGpu->getGraphColorUniform());
  return mBufferGraphs.size() - 1;
}

/** Sets a graph to be drawn on screen next frame.
 *
 * @param graphId the index of the graph to draw
 * @param color the color to draw the graph.
 * @param lineWidth the width of the lines to draw.
 */
void cOwlHook::drawBufferGraph(int graphId, sColor color, float lineWidth)
{
  mBufferGraphs[graphId].setColor(color.r, color.g, color.b, color.a);
  mBufferGraphs[graphId].setLineWidth(lineWidth);
  mOverlayGraphs.push_back(&mBufferGraphs[graphId]);
}


/*****************/
/* private utils */
/*****************/

/** This function checks the time between calls, and if needed, sleeps
 * to put a cap on the frame rate.
 *
 * Note that due to inaccuracies with usleep() calls, we can't just
 * sleep the desired amount using a single call. Instead, we busy wait
 * the desired amount, sleeping a short time each iteration of the
 * wait loop.
 *
 * Also be aware that usleep() implementations vary with OS. It's
 * possible that very short sleep periods cause usleep() to busy wait
 * anyway, because the OS decides that's better than giving up the
 * CPU.
 *
 * @param maxFps is the desired maximum frame rate (usually 30).
 */
void   cOwlHook::capFrameRate(float maxFps)
{
  float  dt;
  float const kMargin = 0.001; // time margin so we err on side of too fast
  static int init = 1;
  static struct timespec t0, t1;

  if (init)
    {
      // set initial time
      currentTimeSpec(&t0);
      init = 0;
    }

  // busy wait
  while (true)
    {
      // check if time to break loop
      currentTimeSpec(&t1);
      dt = elapsedTimeSec(t0, t1);
      if (dt > 1.0 / maxFps - kMargin)
	{ break; }
      // otherwise sleep a short time
      usleep(1000);
    }
  currentTimeSpec(&t0); // set initial time for next call
}

/** Return a pointer to a mock image, either from a JPEG or AVI file. */
void   *cOwlHook::getMockCameraImage(void)
{
  static bool init = true;

  if (init)
    {
      // check file extension for AVI; otherwise assume JPEG
      // Note that mMockInputAvi == false by default
      const char *ext = strrchr(mMockVideoFilepath.c_str(), '.');
      if (ext && ext != mMockVideoFilepath.c_str())
	{
	  ext++;
	  if ( strcmp(ext, "avi") == 0
	       || strcmp(ext, "AVI") == 0)
	    { mMockInputAvi = true; }
	}
      init = false;
    }

  if (mMockInputAvi)
    { return getMockCameraImageAvi(); }
  else
    { return getMockCameraImageJpeg(); }
}

/** Return a pointer to a mock image from an AVI file. */
void   *cOwlHook::getMockCameraImageAvi(void)
{
  int  x, y;
  long i, j;
  cv::Mat cvFrame;
  static float fps = 30;
  static bool  init = true;
  static bool  mismatch = false;

  // TODO: if we need to playback more than one AVI file, add some
  // logic here that knows how to append a suffix to the filepath and
  // load that next file when the current AVI runs out.

  if (init)
    {
      mVideoReader = cv::VideoCapture(mMockVideoFilepath);
      if (mVideoReader.isOpened())
	{
	  fps = mVideoReader.get(CV_CAP_PROP_FPS);
	  LOGI("opened mock input '%s' (%.0f frames @ %.0f FPS)\n",
	       mMockVideoFilepath.c_str(),
	       mVideoReader.get(CV_CAP_PROP_FRAME_COUNT), fps);

	  // read the first frame to get its dimensions; Note that we do
	  // this because mVideoReader.get(CV_CAP_PROP_FRAME_HEIGHT)
	  // mistakenly returns the framw width
	  mVideoReader.read(cvFrame);
	  if (cvFrame.cols != CAMERA_WIDTH || cvFrame.rows != CAMERA_HEIGHT)
	    {
	      LOGW("mock input (%dx%d) does not match input size (%dx%d)\n",
		   cvFrame.cols, cvFrame.rows, CAMERA_WIDTH, CAMERA_HEIGHT);
	      mismatch = true;
	    }
	}
      else
	{
	  LOGW("unable to open mock input video '%s'\n", mMockVideoFilepath.c_str());
	  mismatch = true;
	}
      init = false;
    }

  // return early if there's a frame size mismatch
  if (mismatch)
    { return mpMockCameraImage; }

  // get the raw frame and copy to RGB buffer
  if ( mVideoReader.read(cvFrame) )
    {
      i = 0;
      for (y=0; y<CAMERA_HEIGHT; y++)
	{
	  j = cvFrame.step * y;
	  for (x=0; x<CAMERA_HEIGHT; x++)
	    {
	      mpMockCameraImage[i++] = cvFrame.data[j+2]; // R
	      mpMockCameraImage[i++] = cvFrame.data[j+1]; // G
	      mpMockCameraImage[i++] = cvFrame.data[j+0]; // B
	      j += 3;
	    }
	}
    }
  else
    {
      // assume end of file; re-open AVI file and clear the image buffer
      LOGI("rewinding AVI file\n");
      mVideoReader.open(mMockVideoFilepath);
      memset(mpMockCameraImage, 255, CAMERA_HEIGHT * CAMERA_HEIGHT * 3);

      // reset the frame number.
      mMockFrameNum = 0;
    }

  capFrameRate(2 * fps); // let playback run fast, but not too fast

  return mpMockCameraImage;
}

/** Return a pointer to a mock image from a JPEG file. */
void   *cOwlHook::getMockCameraImageJpeg(void)
{
  static bool init = true;

  if (init)
    {
      // load image from file
      int    width, height;
      jpegFileGetImageSize((char *) mMockVideoFilepath.c_str(), &width, &height);
      if (width != CAMERA_WIDTH || height != CAMERA_HEIGHT)
	{
	  LOGW("mock camera image (%dx%d) does not match input size (%dx%d)\n",
	       width, height, CAMERA_WIDTH, CAMERA_HEIGHT);
	}
      else
	{
	  jpegFileRead(mpMockCameraImage, (char *) mMockVideoFilepath.c_str());
	  LOGI("loaded mock camera image '%s'\n", mMockVideoFilepath.c_str());
	}
      init = false;
    }

  return mpMockCameraImage;
}

/** This function loads a logo image from a PNG file with alpha channel. */
void   cOwlHook::loadLogo(void)
{
  int           width, height;
  int           xStart;
  const float   scale = 1.0f;

  if (mpGpu == NULL) { return; } // ensure that the GPU is fully set up

  // Set up the logo control structure in the overlay lists
  owl::cIconOverlay *pLogo = new owl::cIconOverlay(
      RENDER_LOGO, std::string(LOGO_STARTUP_PNG));
  pLogo->setGpu(mpGpu);
  pLogo->setOwner(owl::cIconOverlay::eIconOwner_t::STARTUP);
  pLogo->getAnimationPtr()->setDuration(LOGO_ANIMATION_DURATION);
  pLogo->getAnimationPtr()->setDirection(1);
  pLogo->getSize(width, height);
  xStart = (int)((((float)OUTPUT_WIDTH - ((float)width * scale)) * 0.5f) + 0.5f);
  pLogo->getAnimationPtr()->setBegin(xStart, LOGO_Y,
                                   scale, scale, LOGO_INITIAL_ALPHA);
  pLogo->getAnimationPtr()->setEnd(xStart, LOGO_Y,
                                 scale, scale, 1.0);

  mRgbaOverlays.mIconOverlays.push_back(pLogo);

  // transfer to a GPU texture
  pLogo->loadActiveIcon();
  LOGI("loaded logo '%s'\n", LOGO_STARTUP_PNG);

  // Set up the logo control structure in the overlay lists
  pLogo = new owl::cIconOverlay(RENDER_LOGO,  std::string(LOGO_PNG));
  pLogo->setGpu(mpGpu);
  pLogo->setOwner(owl::cIconOverlay::eIconOwner_t::RUNTIME);
  pLogo->getAnimationPtr()->setDuration(LOGO_ANIMATION_DURATION);
  pLogo->getAnimationPtr()->setDirection(1);
  pLogo->getSize(width, height);
  pLogo->getAnimationPtr()->setBegin(LOGO_X, LOGO_Y, LOGO_SCALE, LOGO_SCALE,
                                      LOGO_ALPHA);
  pLogo->getAnimationPtr()->setEnd(LOGO_X, LOGO_Y, LOGO_SCALE, LOGO_SCALE,
                                    LOGO_ALPHA);

  mRgbaOverlays.mIconOverlays.push_back(pLogo);
  LOGI("loaded logo '%s'\n", LOGO_PNG);
}

/** This function loads a pin image from a PNG file with alpha channel. */
void   cOwlHook::loadAoiPin(void)
{
  int           width, height;
  unsigned char *pPinImage;

  if (mpGpu == NULL) { return; } // ensure that the GPU is fully set up

  // load the logo PNG file
  pngFileGetImageSize((char *) AOI_PIN_PNG, &width, &height);
  pPinImage = (unsigned char *) malloc(width * height * 4);
  pngFileRead(pPinImage, (char *) AOI_PIN_PNG);

  // transfer to a GPU texture and free the buffer
  owl::cPinnedAoiOverlay overlay(RENDER_AOI_PIN, width, height);
  overlay.setGpu(mpGpu);
  overlay.getAnimationPtr()->setDuration(PIN_ANIMATION_DURATION);
  overlay.getAnimationPtr()->setDirection(1);
  // Make 3 copies of the pin control structure, one for each possible AOI
  mRgbaOverlays.mPinnedAoiOverlays.push_back(overlay);
  mRgbaOverlays.mPinnedAoiOverlays.push_back(overlay);
  mRgbaOverlays.mPinnedAoiOverlays.push_back(overlay);

  mpGpu->setRgbaTexture(RENDER_AOI_PIN, pPinImage, width, height);
  LOGI("loaded %dx%d pin image '%s'\n", width, height, AOI_PIN_PNG);
  free(pPinImage);
}

/** This function provides a simplified interface for writing an image
 * buffer to a JPEG file.
 *
 * @param pImageBuf is a pointer to the RGBA image buffer transferred
 * out of the GPU.
 * @param pFilename is a pointer to the file name string; "/sdcard/"
 * will be prepended to it for Android.
 * @param width is the image width.
 * @param height is the image height.
 */
void   cOwlHook::writePreview(unsigned char *pImageBuf, char *pFilename,
			      int width, int height)
{
  char   pFilepath[256];

#ifdef __ANDROID__
  sprintf(pFilepath, "/sdcard/%s", pFilename);
#else
  strcpy(pFilepath, pFilename);
#endif
  // GPU output is RGBA, so convert to RGB
  inPlaceRGBAtoRGB(pImageBuf, width, height);
  jpegFileWrite(pImageBuf, pFilepath, width, height, 90);
}

/** Returns the next line in the owl.test file that is uncommented.
 *  Currently, the whole line must begin with a '#' to be ignored.
 *
 * @param testFileStream is a file stream opened to read the owl.test file.
 * @return the next uncommented line in the file.
 */
std::string cOwlHook::getActiveMockLine(std::ifstream &testFileStream)
{
  // skip lines beginning with '#'
  std::string line = "";
  while(line == "" || line[0] == '#')
    { std::getline(testFileStream, line); }

  return line;
}

/** Looks through the specified mock directory to find the paths of the test
 *   input components (video and sound source angles). */
void cOwlHook::checkMockDirectory()
{
  if(mMockInputDirectory != "")
    {
      // parse test directory contents
      std::string headerPath = mMockInputDirectory + "/owl.test";
      std::ifstream header(headerPath);
      LOGI("Opening test file '%s'\n", headerPath.c_str());

      // get the video format and path
      std::string formatStr = getActiveMockLine(header);

      if(formatStr[0] == 'I') // image sequence
	{
	  LOGE("The given test has an image sequence -- currently unsupported.\n");
	  return;
	}
      else if(formatStr[0] == 'V') // AVI video file
	{
	  mMockVideoFilepath = getActiveMockLine(header);
	  mMockVideoFilepath = mMockInputDirectory + "/" + mMockVideoFilepath;
	}
      else  // invalid file format
	{ LOGE("Invalid owl.test file in mock directory (video file format '%s').\n"); }

      // get the SSA file path
      // skip whitespace and comments
      mMockSoundFilepath = getActiveMockLine(header);
      mMockSoundFilepath = mMockInputDirectory + "/" + mMockSoundFilepath;

      // get the framerate
      // skip whitespace and comments
      std::string framerateStr = getActiveMockLine(header);
      int mockFrameRate =
	std::atoi(framerateStr.c_str()); //Framerate of simulation (currently unused)

      loadMockSoundFile();
    }
  else
    {
      // no test input directory specified, so use a still image as mock
      // video -- leave mMockInputDirectory and mMockSoundFilepath empty since
      // they weren't provided, but set mMockVideoFilepath to the default still
      // image.
      mMockVideoFilepath = FAUX_CAM_FILEPATH;

      LOGI("Using mock video still image '%s'.\n", mMockVideoFilepath.c_str());
    }
}

/** Loads the data from the mock SSA file specified by mMockSoundFilePath. */
void cOwlHook::loadMockSoundFile()
{
  LOGI("Loading mock sound file '%s'\n", mMockSoundFilepath.c_str());
  std::ifstream inFile(mMockSoundFilepath, std::ios::in);
  std::string line;

  mTestAngles.clear();

  // read each angle; one per line
  while(std::getline(inFile, line))
    {
      // tokenize the lines by comma
      size_t delimIndex = line.find(",");
      if(delimIndex != std::string::npos)
	{
	  // extract the data
	  int frame = std::atoi(line.substr(0, delimIndex).c_str());
	  int angle = std::atoi(line.substr(delimIndex + 1,
					    line.length() - 2 ).c_str());
	  mTestAngles.push_back( sSourceAngle{frame, angle} );
	}
    }
}

/** Gets the next mock sound source angle based off the active frame number
 *   (mMockFrameNum).
 *
 * @return the source angle.
 */
int cOwlHook::getMockSoundAngle()
{
  if(mTestAngles.size() > mMockFrameNum)
    { return mTestAngles[mMockFrameNum].mAngle; }
  else
    { return NO_SOURCE_ANGLE; }
}

/** Enables or disables system test mode.
 *
 * @param enabled whether the system test mode should be active.
 */
void cOwlHook::setSystemTestMode(bool enabled)
{  
  // start the system test
  mSystemTestMode = enabled;
  
  // switch GPU mode
  if(mpGpu)
    {
      mpGpu->setSystemTestMode(enabled);
    }

  // set the button context for the new mode
  mpUiHandler->stop();
  {
    if(enabled)
      {
	mpUiHandler->setLedContext(mpSystemTest->getLedContext());
	mpUiHandler->setButtonContext(mpSystemTest->getButtonContext());
      }
    else
      {
	mpUiHandler->setLedContext(mpOwl->getLedContext());
	mpUiHandler->setButtonContext(mpOwl->getButtonContext());
      }

    // TODO: Create persistent button bindings for combos that should
    //        work across all contexts (as opposed to binding them below).
  
    // bind system test button combination to this function (combo should be 
    // bound for either mode)
    // press MUTE twice while holding OWL + VOLUP to toggle test mode
    static const std::vector<sButtonEvent> kSysTestCombo =
      { sButtonEvent(BUTTON_OWL | BUTTON_VOLUP, eButtonHold),
	sButtonEvent(BUTTON_MUTE, eButtonClick),
	sButtonEvent(BUTTON_MUTE, eButtonClick) };
  
    mButtonHandler.bind(kSysTestCombo,
			std::bind(&cOwlHook::toggleSystemTest, this) );
    }
    mpUiHandler->start();
  
  LOGD("System test mode %s.\n", (enabled ? "enabled" : "disabled"));
}

/** Toggles system test mode. */
void cOwlHook::toggleSystemTest()
{
  setSystemTestMode(!mSystemTestMode);
}
