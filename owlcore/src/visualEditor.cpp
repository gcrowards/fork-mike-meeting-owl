#include "opencv2/imgcodecs/imgcodecs.hpp"

#include "circleMath.hpp"
#include "debugSwitches.h"
#include "logging.hpp"
#include "params.h"
#include "visualEditor.hpp"
#include "owlHook.hpp"
#include "audioDTD.hpp"
#include "elements/vertex.hpp"
// global hook to Owl application
extern cOwlHook *gpOwlHook;  // for global hook into GPU draw methods

// parameters to set the Layout's work area
const int kPanoramicVertMargin = PANORAMIC_VERT_MARGIN;  // in pixels
const int kLayoutTop = OUT_PANO_RES_Y + (2 * kPanoramicVertMargin);
const int kLayoutLeft = 0;
const int kLayoutWidth = OUTPUT_WIDTH;
const int kLayoutHeight = OUTPUT_HEIGHT - kLayoutTop;

// sets height of map debug visualization box
#if DEBUG(GUI_MINIMAL)
  // visualizations will cover bottom quarter of the pano
  const int kDebugBoxHeight = OUT_PANO_RES_Y / 4;
#else
  // visualizations will cover entire pano
  const int kDebugBoxHeight = OUT_PANO_RES_Y;
#endif

cVisualEditor::cVisualEditor(void)
{
  mLayout.setStageArea(cv::Rect(kLayoutLeft, kLayoutTop, 
				kLayoutWidth, kLayoutHeight ) );
  mpPano = NULL;
}

cVisualEditor::~cVisualEditor()
{
  for(auto graph : mInstantGraphs)
    { delete graph; }
  mInstantGraphs.clear();
  for(auto graph : mLongTermGraphs)
    { delete graph; }
  mLongTermGraphs.clear();
}

/** Assign the panorama source for the cVisualEditor.
 *
 * @param pPano is a pointer to a cPanorama that will be used as the
 * image source for the cVisualEditor. 
 */
void cVisualEditor::setPanorama(cPanorama *pPano)
{
  mpPano = pPano;
}

/** This is the externally facing function that should be called to trigger a
 * builds of the latest final output frame.
 *
 * This function updates the cLayout, animates the cSubframe objects that it's
 * comprised of, and then assembles the final output frame.
 *
 * @param pAois is a vector of pointers to the active cAreaOfInterest objects.
 * These objects in conjunction with the cLayout are everything that is needed
 * to construct the frame. 
 * @param pRgbaOverlays is a set of lists of RGBA overlays for each overlay
 * type where the list elements contain data needed to render the overlay.
 * @param pFaceMapper is a pointer to an object that keeps tracks of the bearings
 * at which faces have been detected.
 * @param pMotionPapper is a pointer to an object that keeps track of the 
 * bearings at which motion motion has been detected.
 * @param pSoundMapper is a pointer to an object that keeps track of the
 * bearings at which sound has been detected.
 * @param voices is a vector of voice objects that reprent what the mic array
 * system thinks is the actively speaking person.
 * @param speakerActive indicates if the loudspeaker output is above a
 * threshold of activity.
 * @param speakerLevelLeft is level of activity on loudspeaker left channel.
 * @param speakerLevelRight is level of activity on loudspeaker right channel.
 */
void cVisualEditor::update(std::vector<cAreaOfInterest *> pAois, 
                           owl::cRgbaOverlayLists *pRgbaOverlays,
			   cFaceMapper *pFaceMapper, 
			   cMotionMapper *pMotionMapper,
			   cSoundMapper *pSoundMapper,
			   cPersonMapper *pPersonMapper,
			   std::vector<cVoice> voices,
			   audioChannelStatus_t mSpeakerStatus,
			   audioChannelStatus_t mMicStatus,
			   bool frameRateSlow )
{ 
  if(DEBUG(VISUAL_EDITOR)) { 
    LOGI("\n// Visual Editor Update //\n"); }

  // update current pano shift angle (and broadcast)
  mpPano->setFocusX(mPanoramicSubframe.getFocus().x);

  // because Attention System may have changed AOI sizes:
  makeSpace(pAois);
  // ... then update layout to incorporate new Areas-of-Interest...
  updateLayout(pAois);
  // ... then prevent subframes from partially dupilcating one another
  // (not in their position on screen, but in the part of the image they show)
  mLayout.preventOverlap();
  // ... then animate the each cSubframe
  animateSubframes(pAois);

  // configure pin icon for any pinned Aois
  updatePinnedAois(pAois, pRgbaOverlays);

  // finally, render the output frame
  assembleFrameForGpu(pAois);

  // add overlay for startup progress bar
  overlayProgressBar(pRgbaOverlays->mStartupProgressBar,
                     pRgbaOverlays->mStartupProgressColor);

  // add overlay for interactive AOI positioning
  if (!pRgbaOverlays->getSuppressOverlays())
    {
      overlayAoiGui(pAois);

      if(DEBUG(GLOBAL_DEBUG)) {
        overlayDebugInfo(pAois, pFaceMapper, pMotionMapper,
                         pSoundMapper, pPersonMapper, voices,
                         mSpeakerStatus, mMicStatus,
                         frameRateSlow ); }
    }
  
  if(DEBUG(VISUAL_EDITOR)) { 
    LOGI("\n"); }
}

/** Clear the stage.
 *
 */
void cVisualEditor::clearLayout(void)
{
  mLayout.clearStage();
}

/** This function cycles to the next available nominal animation mode, as 
 * defined by the enum in subframe.hpp.
 */
void cVisualEditor::nextAnimationMode(void)
{
  cSubframe::nextAnimationMode();
}

/** This function returns a vector of cPanel objects that are to be used to 
 * enable the GPU to render the output composite image.
 *
 * @return a vector of panels of active Area-of-Interest Subframes.
 */
std::vector<owl::cPanel> cVisualEditor::getCompositePanels(void)
{
  return mCompositePanels;
}

/** This function creates a subframe containing a scaled-down panoramic image, 
 * which is to be included in the rendered output frame.
 */
void cVisualEditor::createPanoramicSubframe(float ang)
{
  if (mpPano == NULL)
    {
      LOGW("unable to create panoramic subframe\n");
      return;
    }
  
  mPanoramicSubframe = cSubframe(true);
  mPanoramicSubframe.setPanorama(mpPano);
  
  mPanoramicSubframe.setAnimationTime(0.3); // xtra-fast focus (low-pass filter)
  mPanoramicSubframe.setPos(cv::Point((int)(OUTPUT_WIDTH / 2.f), kPanoramicVertMargin));
  mPanoramicSubframe.setSize(cv::Size(OUT_PANO_RES_X, OUT_PANO_RES_Y));

  // calc focus X pos and normalize
  int focusX = fmod((CV_PANO_RES_X>>1) + (CV_PANO_RES_X * ang/360), CV_PANO_RES_X);
  if(focusX < 0)              { focusX += CV_PANO_RES_X; }
  if(focusX >= CV_PANO_RES_X) { focusX = 0;              }

  mPanoramicSubframe.setFocus(cv::Point(focusX, 0));
  mPanoramicSubframe.enableAnimation();
  
  mPanoramicSubframe.setLocked();
  mLayout.addToBase(&mPanoramicSubframe);
}

/* Shifts pano by angle [0, 360).
 *
 */
void cVisualEditor::shiftPanoramicSubframe(float ang)
{
  // calc focus X pos and normalize
  int focusX = fmod((CV_PANO_RES_X>>1) + (CV_PANO_RES_X * ang/360), CV_PANO_RES_X);
  if(focusX < 0)              { focusX += CV_PANO_RES_X; }
  if(focusX >= CV_PANO_RES_X) { focusX = 0;              }

  mPanoramicSubframe.adjustFocus(cv::Point(focusX, 0));
}

/** This function updates the cLayout by adding cSubframe objects that belong
 * to new areas-of-interest and removing cSubframe objects as necessary to 
 * create the require space in the cLayout.
 *
 * @param pAois is a vector of pointers to cAreaOfInterest objects that comprise
 * the set of AOIs that are part of the cLayout or that need to be added.
 */
void cVisualEditor::updateLayout(std::vector<cAreaOfInterest *> pAois)
{
  bool done = true;
  bool layoutChanged = false;
  int initialLayoutCount = mLayout.getLayoutCount();

  // look through AOIs looking for new ones that need to be added to the
  // layout
  for(std::vector<cAreaOfInterest *>::iterator iAoi = pAois.begin();
      iAoi != pAois.end(); ++iAoi )
    { 
      cSubframe *pSubframe = (*iAoi)->getSubframe();
      
      // if AOI is disabled remove subframe from layout
      if(!((*iAoi)->isEnabled()) && pSubframe->isInLayout())
	{ 
	  mLayout.removeFromStage(pSubframe);
	  layoutChanged = true;
	}

      // if AOI size was modified by the Attention System
      if(pSubframe->isMinReqSizeChanged())
	{layoutChanged = true;}
      
      // if AOI is new
      if((*iAoi)->isEnabled() && !(pSubframe->isInLayout()))
	{
	  // see if there's enough room in the Layout to add this subframe
	  bool enoughSpace = mLayout.checkSpaceFor(pSubframe);
	  
	  if(enoughSpace)
	    {
	      mLayout.addToStage(pSubframe);
	      layoutChanged = true;
	    }
	  else if((*iAoi)->getPriority() == cAreaOfInterest::MUST_SHOW)
	    {
	      // find out which AOIs/subframes need to be removed to make 
	      // space for the new subframe
	      int requiredSpace = (*iAoi)->getPatch()->dTheta;

	      if (makeSpace(requiredSpace, pAois))
	        {
                  // restart for-loop because AOI list changed
                  layoutChanged = true;
                  done = false;
	        }
	      else
	        {
	          // throw out the AOI because it won't fit
	          (*iAoi)->expire();
	        }
	      break;
	    }
	  else if((*iAoi)->getPriority() == cAreaOfInterest::TRY_TO_SHOW)
	    {
	      // throw out the AOI because it won't fit & it doesn't have
	      // priority to replace anyone else
	      (*iAoi)->expire();	     
	    }
	}
    }

  // refit the stage if any of the subframe sizes were changed
  if(layoutChanged)
    {
	  refitAndValidateLayout(pAois);
    }

  // if whole Stage is changing, mark for a special animation
  if(done)
    { // Note: have to make sure that this only happens on the edge of the
      //       transitions to 1 AOI, otherwise the existing single subframe
      //       will be marked to STAGE_SWAP on its next animation tick.
      if(mLayout.getLayoutCount() == 1 && initialLayoutCount != 1)
	{ // mark all subframes for stage-swap animation
	  for(std::vector<cAreaOfInterest *>::iterator iAoi = pAois.begin();
	      iAoi != pAois.end(); ++iAoi )
	    {
	      (*iAoi)->getSubframe()->setDisplayStateNow(cSubframe::STAGE_SWAP);
	    }
	}
    }
  else
    { // recurse
      updateLayout(pAois);
    }
}

/** Resizes in-layout subframes to fill stage and validates geometry and view integrity
 *  (no overlaps, etc).
 */
void cVisualEditor::refitAndValidateLayout(std::vector<cAreaOfInterest *> pAois)
{
  mLayout.refitStage();

  owl::cPatch patch;
  owl::cPatch testPatch;

  // check for overlapping views
  for(int i = 0; i < int(pAois.size())-1; i++)
  {
    if(pAois[i]->getSubframe()->isInLayout())
    {
      patch.theta  = pAois[i]->getPatch()->theta;
      patch.dTheta = stagePixToDeg(pAois[i]->getSubframe()->getEndSize().width);

      for(int j = i+1; j < pAois.size(); j++)
      {
        if(pAois[j]->getSubframe()->isInLayout())
        {
          testPatch.theta  = pAois[j]->getPatch()->theta;
          testPatch.dTheta = stagePixToDeg(pAois[j]->getSubframe()->getEndSize().width);

          if(patch.isOverlapped(testPatch))
          {
            mLayout.removeFromStage(pAois[i]->getSubframe());
            pAois[i]->disable();
            return refitAndValidateLayout(pAois); // recurse to refit and check again
          }
        }
      }
    }
  }
}

/** This version of makeSpace determines what areas-of-interest should be 
 * disabled in order to create the required amount of space, and disables
 * them (for a new cSubframe).
 *
 * @param requiredSpace is the amount of room that is needed.
 * @param pAois contains the cAreaOfInterest pointers that comprise the set of
 * of areas-of-interest that are in play.
 *
 * @return true if sufficient space was created.
 */
bool cVisualEditor::makeSpace(int requiredSpace,
			      std::vector<cAreaOfInterest *> pAois )
{
  int reclaimedSpace = 0;

  for(std::vector<cAreaOfInterest *>::iterator iAoi = pAois.begin();
      iAoi != pAois.end(); ++iAoi )
    {
      if((*iAoi)->getSubframe()->isInLayout() && !(*iAoi)->isPinned())
	{
          reclaimedSpace += (*iAoi)->getSubframe()->getEndSize().width;

	  // remove subframe from layout
	  mLayout.removeFromStage((*iAoi)->getSubframe());

	  // disable AOI that owns subframe
	  (*iAoi)->disable();
	}

      // all done when enough space has been freed
      if(reclaimedSpace >= requiredSpace)
	{break;}
    }

  return (reclaimedSpace >= requiredSpace);
}

/** This version of makeSpace checks to see if the minimum required space of 
 * all the provided AOIs is bigger than the stage size.  If it is, AOIs with
 * Subframes that are in the Layout are removed (oldest AOIs first) until the 
 * cumulative required size of all Subframes is less than the stage size.
 *
 * @param pAois contains the cAreaOfInterest pointers that comprise the set of
 * of areas-of-interest that are in play.
 */
void cVisualEditor::makeSpace(std::vector<cAreaOfInterest *> pAois)
{
  int usedWidth = 0;

  // figure out required minimum size of all AOIs in layout
  for(std::vector<cAreaOfInterest *>::iterator iAoi = pAois.begin();
      iAoi != pAois.end(); ++iAoi )
    {
      cSubframe *pSubframe = (*iAoi)->getSubframe();

      if(pSubframe->isInLayout())
	{usedWidth += pSubframe->getMinReqSize().width;}
    }

  // if stage isn't big enough for everyone, get rid of older AOIs
  if(usedWidth > OUTPUT_WIDTH)
    {
      for(std::vector<cAreaOfInterest *>::iterator iAoi = pAois.begin();
          iAoi != pAois.end(); ++ iAoi )
        {
          if (!(*iAoi)->isPinned())
            {
              cSubframe *pSubframe = (*iAoi)->getSubframe();
              usedWidth -= pSubframe->getMinReqSize().width;

              // remove subframe from layout
              mLayout.removeFromStage(pSubframe);

              // disable AOI that owns subframe
              (*iAoi)->disable();

              if(usedWidth <= OUTPUT_WIDTH)
                {break;}
            }
	}
    }
}

/** This function ticks each active cSubframe to animate.
 *
 * Additionally, any cSubframe that is marked for termination and is finished
 * animating is tagged for deletion.
 *
 * @param pAois is a vector of pointers to all the cAreaOfInterest objects that
 * are still alive.
 */
void cVisualEditor::animateSubframes(std::vector<cAreaOfInterest *> pAois)
{
  mPanoramicSubframe.animate();

  for(std::vector<cAreaOfInterest *>::iterator iAoi = pAois.begin();
      iAoi != pAois.end(); ++iAoi )
    {
      (*iAoi)->getSubframe()->animate();

      // remove subframes that are done animating off the screen
      if((*iAoi)->getSubframe()->isTerminated() && 
	 !((*iAoi)->getSubframe()->isAnimating()) )
	{
	  if(DEBUG(VISUAL_EDITOR)) { 
	    LOGI("* Terminated AOI set for removal!\n"); }

	  (*iAoi)->setReadyToRemove();
	}
    }
}

/** Update the display parameters for any pinned Aois on screen.
 *
 * At this point in the rendering process the list of Aois has been
 * updated with any new information applied from external pinned Aoi
 * commands. So, now we need to check through the list of Aois, looking
 * for those that are pinned. Then for each one found, find the
 * matching pinnedAoiOverlay for it. If no match is found, try to
 * create one. If the match is found, then update the pinnedAoiOverly
 * given any changes that may have been made to the Aoi.
 *
 * Finally, if a pinned Aoi has been removed, the corresponding
 * pinnedAoiOverlay must be released and made invisible. To accomplish
 * this, traverse the pinnedAoiOverlay list and make sure the
 * corresponding Aoi is still valid and pinned.
 *
 * @param *pAois Pointer to a list of pointers to area of interests.
 * @param *pOverlays Pointer to an object containing lists of overlays.
 */
void cVisualEditor::updatePinnedAois(std::vector<cAreaOfInterest *> pAois,
                                     owl::cRgbaOverlayLists *pOverlays)
{
  // For each pinned AOI, make sure pin is visible, and positioned right
  for (std::vector<cAreaOfInterest *>::iterator pA = pAois.begin();
      pA != pAois.end(); pA++)
    {
      if ((*pA)->isPinned())
        {
          bool found = false;

          // Find a matching element in the AOIs list and PinnedAoiOverlays list
          for (std::vector<owl::cPinnedAoiOverlay>::iterator
              oLay = pOverlays->mPinnedAoiOverlays.begin();
              oLay != pOverlays->mPinnedAoiOverlays.end();
              oLay++)
            {
              // Test each AOI for a match with each overlay
              if ((*oLay).getAoiId() == (*pA)->getId())
                {
                  // Got a match, so update this overlay for this Aoi
                  if ((*pA)->getPinnedState() == cAreaOfInterest::PINNED_FIXED)
                    {
                      // Show the pin graphic in upper right corner of AOI
                      cv::Point corner = (*pA)->getSubframe()->getPosInLayout();
                      cv::Size size = (*pA)->getSubframe()->getSize();
                      int renderedWidth;
                      int renderedHeight;
                      (*oLay).getSize(renderedWidth, renderedHeight);
                      int left = corner.x + size.width -
                          (renderedWidth * PIN_SCALE) - PIN_CLEARANCE;
                      int top  = corner.y + PIN_CLEARANCE;

                      // Update pin end position as the AOI might have moved
                      (*oLay).getAnimationPtr()->setEnd(left, top,
                                                  PIN_SCALE, PIN_SCALE,
                                                  PIN_ALPHA);
                    }
                  else
                    {
                      (*oLay).setVisible(false);
                      (*oLay).setAoiId(-1);
                    }
                  found = true;
                }
            }
          if (!found)
            {
              // Pinned AOI not yet assigned an overlay, so assign one
              for (std::vector<owl::cPinnedAoiOverlay>::iterator
                  oLay = pOverlays->mPinnedAoiOverlays.begin();
                  oLay != pOverlays->mPinnedAoiOverlays.end();
                  oLay++)
                {
                  if (((*oLay).getAoiId() < 0) &&
                      ((*pA)->getPinnedState() == cAreaOfInterest::PINNED_FIXED))
                    {
                      // Free overlay, use it
                      (*oLay).setAoiId((*pA)->getId());
                      // Show the pin graphic in upper right corner of AOI
                      cv::Point corner = (*pA)->getSubframe()->getPosInLayout();
                      cv::Size size = (*pA)->getSubframe()->getSize();
                      int renderedWidth;
                      int renderedHeight;
                      int left;
                      int top;

                      (*oLay).getSize(renderedWidth, renderedHeight);
                      top  = corner.y + PIN_CLEARANCE;

                      // Set pin start position and visibility
                      (*oLay).setVisible(true);
                      left = corner.x + size.width -
                          (renderedWidth * PIN_SCALE) - PIN_CLEARANCE;
                      (*oLay).getAnimationPtr()->setBegin(left, top,
                                                  PIN_SCALE * 2, PIN_SCALE * 2,
                                                  PIN_ALPHA);
                      (*oLay).getAnimationPtr()->setEnd(left, top,
                                                  PIN_SCALE, PIN_SCALE,
                                                  PIN_ALPHA);
                      (*oLay).getAnimationPtr()->start();
                      break;  // Don't need more than 1!
                    }
                }
            }
        }
    }

  // For each pinned AOI overlay, make sure it is still pinned;
  // disable the overlay if it is not.
  for (std::vector<owl::cPinnedAoiOverlay>::iterator
      oLay = pOverlays->mPinnedAoiOverlays.begin();
      oLay != pOverlays->mPinnedAoiOverlays.end();
      oLay++)
    {
      // Find a matching element in the pOverlays lists
      int aoiId = (*oLay).getAoiId();
      if (aoiId >= 0)
        {
          bool found = false;

          // Claims to be connected to a valid AOI, disable if not
          for (std::vector<cAreaOfInterest *>::iterator pA = pAois.begin();
              pA != pAois.end(); pA++)
            {
              if (((*pA)->getId() == aoiId) && (*pA)->isPinned())
                {
                  found = true;
                  break;
                }
            }
          if (!found)
            {
              // Disable this overlay
              (*oLay).setAoiId(-1);
              (*oLay).setVisible(false);
            }
        }
    }
}

/** Draw progress bar.
 *
 * @param rect is the extent of the progress bar.
 * @param color is the color of the progress bar.
 */
void cVisualEditor::overlayProgressBar(cv::Rect rect, cv::Scalar color)
{
  if (rect.height > 0)
    {
      int lineThickness = rect.height;
      cv::Point start = cv::Point(rect.x, rect.y + rect.height / 2);
      cv::Point end = cv::Point(rect.x + rect.width, rect.y + rect.height / 2);
      mOverlayHelper.drawLine(start, end, color, 1, lineThickness);
    }
}

/** Draw rectangles around any pinned, editable AOIs.
 *
 * Draw a yellow rectangle around each pinned, editable AOI. If an AOI
 * straddles the end of the panorama then draw open ended rectangles
 * on both ends.
 *
 * @param pAois Pointer to vector of AOIs to be drawn.
 */
void cVisualEditor::overlayAoiGui(std::vector<cAreaOfInterest *> pAois)
{
  // parameters and variables for marker rectangles
  float dTheta;
  const int kLeftMargin = (WINDOW_RES_X - OUT_PANO_RES_X) / 2;
  const int kRightMargin = kLeftMargin;
  const int kMarkerThickness = 3; // negative value means rect will be filled
  cv::Scalar markerColor = CYAN2;  // default color
  float aoiAlpha;

  cv::Rect outline;

  // the marker should fill the gap below the pano subframe
  outline.height = CV_PANO_RES_Y - kPanoramicVertMargin;

  // bottom edge of marker rect should be colinear with the bottom edge of pano
  outline.y = kPanoramicVertMargin;

  // iterate through all of the AOIs and draw markers
  for(std::vector<cAreaOfInterest *>::iterator iAoi = pAois.begin();
      iAoi != pAois.end(); ++iAoi )
    {
      // Draw a boundary around pinned, editable Aois
      if ((*iAoi)->isPinned())
        {
          aoiAlpha =
              (((*iAoi)->getPinnedState() == cAreaOfInterest::PINNED_EDITABLE) ?
              PIN_AOI_ALPHA_EDIT : PIN_AOI_ALPHA_PINNED);

          // get the width of the AOI in the CV pano
          dTheta = stagePixToDeg((*iAoi)->getSubframe()->getSize().width);
          if (dTheta < 10) { continue; } // Impossible AOI
          outline.width = dTheta * CV_PANO_RES_X / 360.;

          // get the center/focus of the AOI in the CV pano
          outline.x = (*iAoi)->getSubframe()->getFocusXWrtPano();

          // use the left edge of the marker as the cv::Rect x coordinate
          outline.x -= outline.width / 2;

          // if the outline rect wraps around the panorama, this will hold the
          // pieces
          std::vector<cv::Rect> markerRects =
              circleMath::cropRect( cv::Size(CV_PANO_RES_X,  OUT_PANO_RES_Y),
                                    outline );

          if (markerRects.size() == 1)
            {
              // One rectangle fits AOI (does not wrap around ends)
              markerRects[0].x *=     OUT_PANO_SCALE;
              markerRects[0].width *= OUT_PANO_SCALE;
              markerRects[0].x +=     kLeftMargin;
              mOverlayHelper.drawRect(markerRects[0], markerColor, aoiAlpha,
                                      kMarkerThickness);
            }
          else
            {
              // Rectangle straddles the ends of the panorama - draw 6 lines -
              // two sideways 'U's opening to the outsides.
              cv::Rect left, right;
              cv::Rect top, bottom;

              // Make sure the rects are sorted by X
              if (markerRects[0].x < markerRects[1].x)
                {
                  left = markerRects[0];
                  right = markerRects[1];
                }
              else
                {
                  left = markerRects[1];
                  right = markerRects[0];
                }

              // Draw left sideways U
              left.x += left.width;
              left.x *= OUT_PANO_SCALE;
              left.width = 1;
              left.x += kLeftMargin;
              mOverlayHelper.drawRect(left, markerColor, aoiAlpha,
                                      kMarkerThickness);
              top.x = kLeftMargin;
              top.width = left.x - kLeftMargin;
              top.y = left.y;
              top.height = 1;
              mOverlayHelper.drawRect(top, markerColor, aoiAlpha,
                                      kMarkerThickness);
              bottom = top;
              bottom.y = left.y + left.height;
              mOverlayHelper.drawRect(bottom, markerColor, aoiAlpha,
                                      kMarkerThickness);

              // Draw right sideways U
              top = right;  // Save for use below
              right.x *= OUT_PANO_SCALE;
              right.x += kLeftMargin;
              right.width = 1;
              mOverlayHelper.drawRect(right, markerColor, aoiAlpha,
                                      kMarkerThickness);
              top.width *= OUT_PANO_SCALE;
              top.x = right.x;
              top.height = 1;
              mOverlayHelper.drawRect(top, markerColor, aoiAlpha,
                                      kMarkerThickness);
              bottom = top;
              bottom.y = right.y + right.height;
              mOverlayHelper.drawRect(bottom, markerColor, aoiAlpha,
                                      kMarkerThickness);
            }
        }
    }
}

/** This function draws information on top of the output frame; generally used
 * for debug/testing.
 *
 * @param pAois is a pointer to a vector of currently active areas of interest.
 * @param pFaceMapper is pointer to a cFaceMapper object. The face mapper 
 * facilitates access to a room map that currently keeps bearing information 
 * about where people are located, based on faces.
 * @param pMotionMapper is a pointer to a cMotionMapper object.  The motion
 * mapper facilitates access to a room map that cuurently keeps bearing
 * informatio about where motion indicates that people may be located.
 * @param pSoundMapper is a pointer to an object that keeps track of the
 * bearings at which sound has been detected.
 * @param voices is a vector of currently active voices.
 * @param speakerActive indicates if the loudspeaker output is above a
 * threshold of activity.
 * @param speakerLevelLeft is level of activity on loudspeaker left channel.
 * @param speakerLevelRight is level of activity on loudspeaker right channel.
 */
void cVisualEditor::overlayDebugInfo(std::vector<cAreaOfInterest *> pAois,
				     cFaceMapper *pFaceMapper,
				     cMotionMapper *pMotionMapper,
				     cSoundMapper *pSoundMapper,
				     cPersonMapper *pPersonMapper,
				     std::vector<cVoice> voices,
				     audioChannelStatus_t mSpeakerStatus,
				     audioChannelStatus_t mMicStatus,
				     bool frameRateSlow )
{
  // draw general-purpose tools
  if(DEBUG(GENERAL))
    {
      // draw imager's offset degree markers below pano subframe 
      overlayPanoDegMarkers();
     
     // draw indicator if frame rate is too slow      
      if(frameRateSlow)
	{ 
	  int kFill= -1;   // means yes, fill
	  int kAlpha = 1.; // opaque
	  int kSize = 15;  // pixels
	  int kMargin = 5; // pixels
	  
	  mOverlayHelper.drawRect(cv::Rect(OUTPUT_WIDTH - kSize - kMargin,
					   OUTPUT_HEIGHT - kSize - kMargin,
					   kSize, kSize),
				  ORANGE, kAlpha, kFill );
	}
    }
  
  // draw lines to indicate that the loudspeaker is active
  if(DEBUG(mDebugManager.check(eLoudspeaker))) {
      overlayAudioLevels(mSpeakerStatus, 5); }

  // draw lines to indicate that the microphone is active
  if(DEBUG(mDebugManager.check(eMicrophone))) {
      overlayAudioLevels(mMicStatus, 10); }

  // draw markers where voices are heard
  if(DEBUG(mDebugManager.check(eVoiceTracker))) {
    overlayVoices(voices); }

  // draw markers where articles of interest are found
  if(DEBUG(mDebugManager.check(eAoi))) {
    overlayAois(pAois); }

  // highlight cFaceMapper search region
  if(DEBUG(mDebugManager.check(eFaceMapperSearch))) {
    cv::Rect searchRoi = pFaceMapper->getSearchRoi();

    // adjust for wraparound
    if (searchRoi.x >= CV_PANO_RES_X)
      { searchRoi.x -= CV_PANO_RES_X; }

    // put ROI in subframe's local coordinate system
    searchRoi.x -= mPanoramicSubframe.getRoi().x;

    // now put ROI in layout's coordinate system
    searchRoi.x += mPanoramicSubframe.getPosInLayout().x;
    searchRoi.y += mPanoramicSubframe.getPosInLayout().y;

    // scale from CV pano to OUT pano
    searchRoi.width *= OUT_PANO_SCALE;
    searchRoi.height *= OUT_PANO_SCALE;
    
    mOverlayHelper.drawRect(searchRoi, BLUE);
  }

  // set height of mapper debug box, overlay box if debug GUI should be minimal
  if ( DEBUG(GUI_MINIMAL) &&
       (DEBUG(mDebugManager.check(eFaceMapper)   || 
	      mDebugManager.check(eMotionMapper) ||
	      mDebugManager.check(eSoundMapper)  || 
	      mDebugManager.check(ePersonMapper) ) ) ) {
    
    // define box origin and size
    cv::Rect debugBox;
    debugBox.x = mPanoramicSubframe.getPosInLayout().x;
    debugBox.y = mPanoramicSubframe.getPosInLayout().y +
      mPanoramicSubframe.getSize().height - kDebugBoxHeight;
    debugBox.width = mPanoramicSubframe.getSize().width;
    debugBox.height = kDebugBoxHeight;
    
    // draw debug box overlay
    int boxIsFilled = -1; // means box should be filled
    mOverlayHelper.drawRect(debugBox, WHITE, 0.4, boxIsFilled);
  }

  // draw sound localizer visualization
  if(DEBUG(mDebugManager.check(eSoundLocalizer))) {
    overlaySoundLocalizer();
  }
  
  if(DEBUG(mDebugManager.check(eFaceMapper)))
    {
      const int kDeltaDeg = 1;
      const sColor kColor(0.0f, 0.0f, 1.0f, 1.0f); // blue
      const float kLineWidth = 3.0f;
      
      if(mDebugGraphs[mkFaceGraphId].gpuGraphId < 0)
	{ // initialize GPU buffer graph for face map
	  mDebugGraphs[mkFaceGraphId].gpuGraphId =
	    gpOwlHook->createBufferGraph(&mDebugGraphs[mkFaceGraphId].data);
	}
      overlayMap(pFaceMapper->getMap(), kDeltaDeg, kColor, kLineWidth,
		 mDebugGraphs[mkFaceGraphId] );
    }
  
  if(DEBUG(mDebugManager.check(eMotionMapper)))
    {
      const int kDeltaDeg = 1;
      const sColor kColor(1.0f, 0.0f, 0.0f, 1.0f); // red
      const float kLineWidth = 3.0f;
      
      if(mDebugGraphs[mkMotionGraphId].gpuGraphId < 0)
	{ // initialize GPU buffer graph for motion map
	  mDebugGraphs[mkMotionGraphId].gpuGraphId =
	    gpOwlHook->createBufferGraph(&mDebugGraphs[mkMotionGraphId].data);
	}
      overlayMap(pMotionMapper->getMap(), kDeltaDeg, kColor, kLineWidth,
		 mDebugGraphs[mkMotionGraphId] );
    }
  
  if(DEBUG(mDebugManager.check(eSoundMapper)))
    {
      const int kDeltaDeg = 1;
      const sColor kColor(0.0f, 1.0f, 0.0f, 1.0f); // green
      const float kLineWidth = 3.0f;
      
      if(mDebugGraphs[mkSoundGraphId].gpuGraphId < 0)
	{ // initialize GPU buffer graph for sound map
	  mDebugGraphs[mkSoundGraphId].gpuGraphId =
	    gpOwlHook->createBufferGraph(&mDebugGraphs[mkSoundGraphId].data);
	}
      overlayMap(pSoundMapper->getMap(), kDeltaDeg, kColor, kLineWidth,
		 mDebugGraphs[mkSoundGraphId] );
    }
  
  if(DEBUG(mDebugManager.check(ePersonMapper)))
    {
      const int kDeltaDeg = 1;
      const sColor kColor(1.0f, 0.0f, 1.0f, 1.0f); // magenta
      const float kLineWidth = 3.0f;
      
      if(mDebugGraphs[mkPersonGraphId].gpuGraphId < 0)
	{ // initialize GPU buffer graph for person map
	  mDebugGraphs[mkPersonGraphId].gpuGraphId =
	    gpOwlHook->createBufferGraph(&mDebugGraphs[mkPersonGraphId].data);
	}
      overlayMap(pPersonMapper->getMap(), kDeltaDeg, kColor, kLineWidth,
		 mDebugGraphs[mkPersonGraphId] );
    }
  
  // draw visualization of person mapper's instant item map
  if(DEBUG(mDebugManager.check(ePersonInstantItemMap)))
    {
      const int kDeltaDeg = 1;
      
      overlayItems(pPersonMapper->getInstantItemMap(), kDeltaDeg, false,
		   mInstantGraphs );
    }
  
  // draw visualization of person mapper's long-term item map
  if(DEBUG(mDebugManager.check(ePersonLongTermItemMap)))
    {
      const int kDeltaDeg = 1;
      
      overlayItems(pPersonMapper->getLongTermItemMap(), kDeltaDeg, true,
		   mLongTermGraphs );
    }
}

/** Draws audio level bars for left and right channels at given screen row.
 *
 * @param status contains channel levels and active state of audio channel.
 * @param line indicates the screen row at which to draw the line graph.
 */
void cVisualEditor::overlayAudioLevels(audioChannelStatus_t status, int line)
{
  int xLeft, xRight;
  const int kXCenter = OUTPUT_WIDTH / 2;
  const int kY = line;
  const int kLineThickness = 5;
  // set horizontal endpoint for left speaker channel
  xLeft = kXCenter - status.mLevelLeft;
  xLeft = xLeft < 0 ? 0 : xLeft;
  // set horizontal endpoint for right speaker channel
  xRight = kXCenter + status.mLevelRight;
  xRight = xRight > OUTPUT_WIDTH ? OUTPUT_WIDTH : xRight;
  // set color (GREEN: below threshold for speakerActive flag, RED above)
  cv::Scalar color = GREEN;
  if (status.mActive)
    { color = RED; }
  // draw the speaker activity lines
  mOverlayHelper.drawLine(cv::Point(kXCenter, kY), cv::Point(xLeft, kY),
                          color, 1, kLineThickness);
  mOverlayHelper.drawLine(cv::Point(kXCenter, kY), cv::Point(xRight, kY),
                          color, 1, kLineThickness);
  // draw a center line so we see still something when the speaker is silent
  mOverlayHelper.drawLine(cv::Point(kXCenter, kY-kLineThickness),
                          cv::Point(kXCenter, kY+kLineThickness),
                          color, 1, kLineThickness);
}

/** This function draws the imager's offset degree below the panoramic subframe;
 * generally used for debugging/testing.
 */
void cVisualEditor::overlayPanoDegMarkers(void)
{
  // the number of degree marker divisions to be labeled
  const int kNumDivisions = 8; // 360degrees / 8divisions = 45deg/div

  // convenience variables for panoramic subframe height and vertical position
  const int kPanoHeight = mPanoramicSubframe.getSize().height;
  const int kPanoPosY = mPanoramicSubframe.getPosInLayout().y;

  // used to define the degree marker line locations
  cv::Point lineStartPoint;
  cv::Point lineEndPoint;
  cv::Point extraPoint;

  // display degree marker line at each division
  for(int division = 0; division < kNumDivisions; division++)
    {
      // calculate the current degree number for this loop
      int currDegNum = division * (360 / kNumDivisions);

      // current degree marker's distance from the left edge of the output frame
      lineStartPoint.x = panoDegToFrameLoc(currDegNum);

      // set lineStartPoint.y equal to the bottom edge of the panoramic subframe
      lineStartPoint.y = kPanoPosY + kPanoHeight;

      // the line start/end x points should be the same
      lineEndPoint.x = lineStartPoint.x;

      // set line start/end y points to be kPanoramicVertMargin apart (fill gap)
      lineEndPoint.y = lineStartPoint.y + kPanoramicVertMargin;

      // draw marker line
      mOverlayHelper.drawLine(lineStartPoint, lineEndPoint, GRAY);

      // modify marker for 0-degree point
      if (division == 0)
	{
	  // make the marker point in the direction of increasing angle
	  if (panoDegToFrameLoc(currDegNum+1) > lineStartPoint.x)
	    { extraPoint.x = lineStartPoint.x + kPanoramicVertMargin; }
	  else
	    { extraPoint.x = lineStartPoint.x - kPanoramicVertMargin; }
	  extraPoint.y = (lineStartPoint.y + lineEndPoint.y) / 2;
	  mOverlayHelper.drawLine(lineStartPoint, extraPoint, GRAY);
	  mOverlayHelper.drawLine(lineEndPoint, extraPoint, GRAY);
	}
    }
}

/** This function draws indicators below the panoramic subframe showing where the
 * current detected voices are.
 *
 * @param voices is a vector of the current voices from the virtual mic.
 */
void cVisualEditor::overlayVoices(std::vector<cVoice> voices)
{
  const int kMarkerThickness = -1; // negative value means rect will be filled

  // size that will define voice marker rects
  cv::Size markerSize;
  markerSize.height = kPanoramicVertMargin;
  markerSize.width = markerSize.height;

  // location of the voice marker rects
  cv::Point locInFrame;
  locInFrame.y = mPanoramicSubframe.getSize().height + markerSize.height;

  for(std::vector<cVoice>::iterator iVoice = voices.begin();
      iVoice != voices.end(); ++iVoice )
    {
      // the degree that the voice is coming from
      int voiceDegree = (*iVoice).getDirection();

      // calc marker center's horizontal position from left edge of output frame
      locInFrame.x = panoDegToFrameLoc(voiceDegree) - (markerSize.width / 2);

      // draw marker on output frame
      mOverlayHelper.drawRect(cv::Rect(locInFrame, markerSize), GREEN, 1.0,
			      kMarkerThickness );
    }
}

/** This function draws colored rectangles above the panoramic subframe
 * that indicates the location of the AOI within the panorama; generally used
 * for debugging/testing purposes. An active AOI will be shown in red and an
 * AOI which is currently tracking a face will be shown in yellow.
 *
 * @param pAois is a pointer to all of the active areas of interest.
 */
void cVisualEditor::overlayAois(std::vector<cAreaOfInterest *> pAois)
{
  // parameters and variables for marker rectangles
  float dTheta;
  const int kLeftMargin = (WINDOW_RES_X - OUT_PANO_RES_X) / 2;
  const int kMarkerThickness = -1; // negative value means rect will be filled
  cv::Scalar markerColor = WHITE;  // default color

  cv::Rect marker;

  // the marker should fill the gap below the pano subframe
  marker.height = kPanoramicVertMargin;

  // bottom edge of marker rect should be colinear with the bottom edge of pano
  marker.y = OUT_PANO_RES_Y + kPanoramicVertMargin - marker.height;

  // iterate through all of the AOIs and draw markers
    for(std::vector<cAreaOfInterest *>::iterator iAoi = pAois.begin();
	iAoi != pAois.end(); ++iAoi )
      {
	// get the width of the AOI in the CV pano
	dTheta = stagePixToDeg((*iAoi)->getSubframe()->getSize().width);
	marker.width = dTheta * CV_PANO_RES_X / 360.;

        // get the center/focus of the AOI in the CV pano
	marker.x = (*iAoi)->getSubframe()->getFocus().x;

        // use the left edge of the marker as the cv::Rect x coordinate
	marker.x -= marker.width / 2;

        // if the marker rect wraps around the panorama, this will hold the
	// pieces
        std::vector<cv::Rect> markerRects =
	  circleMath::cropRect( cv::Size(CV_PANO_RES_X,  OUT_PANO_RES_Y),
			       marker );
	// iterate through the rectangle pieces and draw markers on output frame
	for(std::vector<cv::Rect>::iterator iRectPiece = markerRects.begin();
	    iRectPiece != markerRects.end(); ++iRectPiece)
	  {
	    cv::Rect markerPiece = (*iRectPiece);

	    // scale position and width to the OUT pano
	    markerPiece.x *= OUT_PANO_SCALE;
	    markerPiece.width *= OUT_PANO_SCALE;

	    // shift by the gap between the pano subframe & the left edge of
	    // frame
	    markerPiece.x += kLeftMargin;

	    // if the AOI is currently tracking a face, turn the marker yellow
	    if((*iAoi)->getFaceTrackerState() == faceState::TRACKING)
	      { markerColor = YELLOW; }

	    // if the AOI is not currently tracking a face, turn the marker red
	    if((*iAoi)->getFaceTrackerState() == faceState::LOST)
	      { markerColor = RED; }

	    // draw marker on output frame for current rectangle piece
	    mOverlayHelper.drawRect(markerPiece, markerColor, 1.0,
				    kMarkerThickness);
	  }
      }
}

/** This function draws a line graph visualization on top of the panorama for
 * the theta value of the items in the given cMap's Item Map (for debugging 
 * purposes).
 *
 * Note: In the future cItems may be described by an elevation (phi) in
 *       addition to the bearing (theta), in which case this function will need
 *       a major overhaul to draw an ellipse, or some sort of two-dimensional
 *       representation of each Item.
 *
 * @param kpItems is a vector of cItem objects that represents where things are 
 * within the room.
 * @param deltaDeg is angular increment (resolution) for the line graph.
 * @param uniqueIds whether the ids of the items in kpItems are unique.
 * @param itemGraphs is the a vector of graph buffers to store each item's data.
 */
void cVisualEditor::overlayItems(const std::vector<const cItem *> kpItems, 
				 int deltaDeg, bool uniqueIds,
				 std::vector<sDebugGraph *> &itemGraphs )
{
  const int kAlpha = 1;     // line's alpha value
  const float kLineWidth = 3.0f; // line's thickness
  const int kXSigma = 4;    // how far out to draw each item (e.g. 4-sigma)
  const int kScale = 1000;  // scales trace vertically for good visibility
                            // (assumes we won't do better than a standard dev.
                            // of 5 deg, corresponding to ~0.1, so 1000 scales
                            // to ~100 pixel peak)
  const int kPanoBottom = (kPanoramicVertMargin +
			   mPanoramicSubframe.getSize().height );

  for(auto pG : itemGraphs)
    { pG->used = false; }
  
  // draw each item with the buffer graph
  for(int iItem = 0; iItem < kpItems.size(); iItem++)
    {
      const cItem *pItem = kpItems[iItem];
      
      // find start/end points for this item's trace
      // NOTE: id will be the cItem's id only if their ids are unique
      //       (uniqueIds must be true). Otherwise it will be the item's index
      //       in kpItems.
      int id = (uniqueIds ? pItem->getId() : iItem);
      int meanAngle = pItem->getThetaMean();
      float stdDev = pItem->getThetaStdDev();
      int startAngle = meanAngle - (kXSigma * stdDev);
      int endAngle = meanAngle + (kXSigma * stdDev);

      // try to find graph previously used for this item
      sDebugGraph *pGraph = nullptr;
      for(auto pG : itemGraphs)
	{
	  if(pG->id == id)
	    { // found the graph for this item
	      pGraph = pG;
	      break;
	    }
	}

      if(!pGraph)
	{
	  // try to find an unused graph
	  for(auto pG : itemGraphs)
	    {
	      if(pG->id < 0)
		{
		  pGraph = pG;
		  pGraph->id = id;
		  break;
		}
	    }
	  if(!pGraph)
	    { // create a new graph for this item
	      pGraph = new sDebugGraph();
	      pGraph->id = id;
	      pGraph->gpuGraphId = gpOwlHook->createBufferGraph(&pGraph->data);
	      itemGraphs.push_back(pGraph);
	    }
	}

      sColor graphColor;
      if(id > 0)
	{
	  cv::Scalar idColor = mOverlayHelper.getColorFromId(id);
	  graphColor = sColor(idColor[0], idColor[1], idColor[2], 1.0);
	}
      
      cv::Point2f lastPoint;
      pGraph->data.clear();
      
      for(int angle = startAngle; angle < endAngle; angle += deltaDeg)
	{
	  cv::Point2f gpuPoint(panoDegToGpuLoc(angle),
			       (pItem->getThetaGaussianAt(angle) *
				(float)kScale / (float)CV_PANO_RES_Y ));

	  gpuPoint.x -= floor(gpuPoint.x);                         // wrap x
	  gpuPoint.y = std::min(1.0f, std::max(0.0f, gpuPoint.y)); // clamp y
	  gpuPoint.y -= 0.01; // hide zero points out of frame

	  if(gpuPoint.x < lastPoint.x)
	    { // wrapped panorama x
	      pGraph->data.emplace_back(lastPoint.x, lastPoint.y);

	      // interpolate to find point at the horizontal edge
	      float alpha = (1.0 - lastPoint.x) / (1.0f - lastPoint.x + gpuPoint.x);
	      lastPoint.x = 1.0f;
	      lastPoint.y = lastPoint.y * (1.0 - alpha) + gpuPoint.y * alpha;
	      pGraph->data.emplace_back(lastPoint.x, lastPoint.y);
	      lastPoint.x = 0.0f;
	    }

	  if(angle != startAngle)
	    { // wrap line split between panorama edges
	      pGraph->data.emplace_back(lastPoint.x, lastPoint.y);
	      pGraph->data.emplace_back(gpuPoint.x, gpuPoint.y);
	    }

	  lastPoint = gpuPoint;
	}
      gpOwlHook->drawBufferGraph(pGraph->gpuGraphId, graphColor, kLineWidth);
      pGraph->used = true;
    }

  // reset any graphs that are no longer used
  for(auto pG : itemGraphs)
    {
      if(!pG->used)
	{ pG->id = -1; }
    }
 }

 /** This function draws a line graph visualization on top of the panorama for a
 * given cMap for debugging purposes.
 *
 * @param map is the cMap that represents where things are within the room.
 * @param deltaDeg is angular increment (resolution) for the line graph.
 * @param color is the cv::Scalar color that will be used to draw the lines.
 * @param lineWidth is the width to draw the lines.
 * @param graph is the graph buffer to overlay the map with.
 */
void cVisualEditor::overlayMap(cMap map, int deltaDeg, sColor color,
			       float lineWidth, sDebugGraph &graph )
{
  std::vector<int> mapData = map.getMapData();
  float maxVal = (float)map.getMaxVal();
  cv::Point2f lastPoint;
  
  graph.data.clear();
  graph.data.reserve(360 / deltaDeg);
  for(int iAngle = 0; iAngle < 360; iAngle += deltaDeg)
    {
      cv::Point2f gpuPoint(panoDegToGpuLoc(iAngle),
			   (float)mapData[iAngle] / (float)maxVal );
      
      if(iAngle == 0)
      {
	lastPoint = cv::Point2f(panoDegToGpuLoc(360 - deltaDeg),
				(float)mapData[360 - deltaDeg] / (float)maxVal );
      }
      
      gpuPoint.x -= floor(gpuPoint.x);                         // wrap x
      gpuPoint.y = std::min(1.0f, std::max(0.0f, gpuPoint.y)); // clamp y
      gpuPoint.y -= 0.01f; // hide zero points out of frame

      if(gpuPoint.x < lastPoint.x)
	{ // wrapped panorama x
	  graph.data.emplace_back(lastPoint.x, lastPoint.y);

	  // interpolate to find point at the horizontal edge
	  float alpha = (1.0 - lastPoint.x) / (1.0f - lastPoint.x + gpuPoint.x);
	  lastPoint.x = 1.0f;
	  lastPoint.y = lastPoint.y * (1.0 - alpha) + gpuPoint.y * alpha;
	  graph.data.emplace_back(lastPoint.x, lastPoint.y);
	  lastPoint.x = 0.0f;
	}

      graph.data.emplace_back(lastPoint.x, lastPoint.y);
      graph.data.emplace_back(gpuPoint.x, gpuPoint.y);

      lastPoint = gpuPoint;
    }
  gpOwlHook->drawBufferGraph(graph.gpuGraphId, color, lineWidth);
}

/* Overlay tick marks representing static beams, along with some information
   about sound-source localization via those beams. */
void cVisualEditor::overlaySoundLocalizer(void)
{
  int kDeltaDeg = 1;
  int i;
  int mAngle;
  int speechStatus;
  cSoundLocalizer *pSndLoc;
  cAudioDTD *pDblTalkDetect;
  
  int dispersion; 
  int lastAngle;
  
  float scale;
#define DEBUG_DTD
#define DEBUG_LOCALIZER
  
  pSndLoc = gpOwlHook->getSoundLocalizer();
  pDblTalkDetect = gpOwlHook->getDTD();
#ifdef DEBUG_LOCALIZER  
  for(i = 0; i < N_BEAMS; ++i)
    {
      mAngle = (360. / N_BEAMS) * (double(i) + 0.5);
      speechStatus = pSndLoc->getSpeechStatus(i);
      if(speechStatus)
      {overlayAngle(mAngle, 0.25, YELLOW);}
      else
      {overlayAngle(mAngle, 0.25, PURPLE);}      
    }
  dispersion = pSndLoc->getDispersion();
  scale = (100.0 - ((float)dispersion)) / 100.0;
  
  lastAngle = pSndLoc->getLastAngle();
  if(dispersion > BEAM_DISPERSION_THRESHOLD)
  {
    overlayAngle(lastAngle, scale, BLUE);
  }
  else
  {
    overlayAngle(lastAngle, scale, GREEN);
  }
  if(AMIsSpeakerActive() == 0)
  {overlayAngle(0, 1.0, GREEN);}
  else
  {overlayAngle(0, 1.0, RED);}  
#endif
#ifdef DEBUG_DTD
  switch(pDblTalkDetect->DTDGetState())
  {
    case STATE_SILENCE:
        overlayAngle(0, 1.0, BLACK);
        break;    
    case STATE_LOCAL_SINGLE_TALK:
        overlayAngle(0, 1.0, GREEN);
        break;    
    case STATE_REMOTE_SINGLE_TALK:
        overlayAngle(0, 1.0, YELLOW);
        break;    
    case STATE_DOUBLE_TALK:
        overlayAngle(0, 1.0, RED);
        break;    
  }
#endif  
}

// *** TODO - DOCUMENT THIS FUNCTION CORRECTLY

/** This function draws a line on the panoramic view at a particular angle
 *
 * @param angle - the angle to draw a line at
 * @param scale - How much of the line to draw from bottom to top 
 * @param color is the cv::Scalar color that will be used to draw the lines.
 */
void cVisualEditor::overlayAngle(int angle,float scale, cv::Scalar color)
{
  const int kAlpha = 1;      // line's alpha value
  const int kThickness = 1;  // line's thickness
  float endY;
  cv::Point lineStart, lineEnd;

  // scale the map's value so that it fits in the debug box
  int scaledMapVal = 0.5 + (kDebugBoxHeight*scale);


  // draw a line between each point
  // get current map value

      // scale the map's value so that it fits in the debug box
    lineStart.x =  panoDegToFrameLoc(angle);
    lineStart.y =  kPanoramicVertMargin + mPanoramicSubframe.getSize().height;
    // define location of the line's end coordinate
    lineEnd.x = panoDegToFrameLoc(angle);
    lineEnd.y = (kPanoramicVertMargin +
                 mPanoramicSubframe.getSize().height -
                 scaledMapVal);
                     // the bottom of the pano
    mOverlayHelper.drawLine(lineStart, lineEnd, color,
    kAlpha, kThickness );

}

/** This function converts panorama degree (between 0 and 360) to the
 * horizontal distance from the left edge of the output frame that can
 * be used to locate objects relative to the panoramic subframe while
 * accounting for the scale of the subframe relative to the full
 * panorama.
 *
 * @param deg is an integer between 0 and 360 that should be converted
 * to a horizontal location from the left edge of the output frame.
 *
 * @return an integer that represents a horizontal distance from the left edge of
 * the output frame.
 */
int cVisualEditor::panoDegToFrameLoc(int deg)
{
  int ref = int(360 * (mPanoramicSubframe.getFocus().x - (CV_PANO_RES_X>>1)) / (float)CV_PANO_RES_X);

  // convert pixel location from original panorama to subframe panorama
  return mPanoramicSubframe.getPosInLayout().x +
                    (int)(OUT_PANO_SCALE * mpPano->pixelAt(circleMath::intSubAngs(deg, ref)));
}

/** This function converts panorama degree (between 0 and 360) to the
 * horizontal distance from the left edge of the output frame for use by the
 * GPU graph shader (between 0.0 and 1.0).
 *
 * @param deg is an integer between 0 and 360 that should be converted
 * to a horizontal location from the left edge of the output frame.
 *
 * @return an integer that represents a horizontal distance from the left edge of
 * the output frame. 
 */
float cVisualEditor::panoDegToGpuLoc(int deg)
{
  int ref = int(360 * (mPanoramicSubframe.getFocus().x - (CV_PANO_RES_X>>1)) / (float)CV_PANO_RES_X);

  // convert pixel location from original panorama to gpu panorama
  return mpPano->pixelAt(circleMath::intSubAngs(deg, ref)) / (float)CV_PANO_RES_X;
}

/** This function draws a box indicating the area of the subframe that is being
 * searched for faces.
 *
 * @param pAoi is a pointer to the cAreaOfInterest that is being searched.
 */
void cVisualEditor::highlightFaceSearchArea(cAreaOfInterest *pAoi)
{
  // TODO: consider restoring highlight in stage subframe

  // highlight search region in panoramic subframe
  cv::Rect searchRoi = pAoi->getFaceSearchRoi();

  // adjust scale to match panoramic's scale
  searchRoi.x *= OUT_PANO_SCALE;
  searchRoi.y *= OUT_PANO_SCALE;
  searchRoi.width *= OUT_PANO_SCALE;
  searchRoi.height *= OUT_PANO_SCALE;

  // put ROI in subframe's local coordinate system
  searchRoi.x -= mPanoramicSubframe.getRoi().x;

  // now put ROI in layout's coordinate system
  searchRoi.x += mPanoramicSubframe.getPosInLayout().x; 
  searchRoi.y += mPanoramicSubframe.getPosInLayout().y;

  mOverlayHelper.drawRect(searchRoi, BLUE, 1.0, 2);
}

/** This function draws a box around detected face.
 *
 * @param pAoi is a pointer to the cAreaOfInterest that contains the face that
 * is to be highlighted.
 */
void cVisualEditor::highlightFace(cAreaOfInterest *pAoi)
{
  // TODO: consider restoring highlight in stage subframe

  // highlight face in panoramic subframe
  cv::Rect face = pAoi->getFace();

  // adjust scale to match panoramic's scale
  face.x *= OUT_PANO_SCALE;
  face.y *= OUT_PANO_SCALE;
  face.width *= OUT_PANO_SCALE;
  face.height *= OUT_PANO_SCALE;

  // put ROI in subframe's local coordinate system
  face.x -= mPanoramicSubframe.getRoi().x;
  
  // now put ROI in layout's coordinate system
  face.x += mPanoramicSubframe.getPosInLayout().x; 
  face.y += mPanoramicSubframe.getPosInLayout().y;
  
  mOverlayHelper.drawRect(face, YELLOW, 1.0, 2);
}

/** This function builds a list of the panels that are used to build the output
 * composite frame by gathering together the panels from each cSubframe object
 * that is a part of an active cAreaOfInterest.
 *
 * @param pAois is a vector of pointers to all cAreaOfInterest objects that
 * are still aive.
 */
void cVisualEditor::assembleFrameForGpu(std::vector<cAreaOfInterest *> pAois)
{
  mCompositePanels.clear();

  // draw in permanent fixtures
  mCompositePanels.push_back(mPanoramicSubframe.getPanel());

  // draw in stage areas-of-interest
  for(std::vector<cAreaOfInterest *>::iterator iAoi = pAois.begin();
      iAoi != pAois.end(); ++iAoi )
    {
      mCompositePanels.push_back((*iAoi)->getSubframe()->getPanel());

      if(DEBUG(mDebugManager.check(eFaceDetector)) || 
	 mDebugManager.check(eFaceTracker) )
	{
	  highlightFaceSearchArea(*iAoi);
	  if((*iAoi)->getFaceTrackerState() == faceState::TRACKING)
	    { highlightFace(*iAoi); }
	}
    }
}
