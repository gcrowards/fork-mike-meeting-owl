#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>

#include "opencv2/core/core.hpp"

#include "owlHook.hpp"
#include "beamMath.hpp"
#include "common.hpp"
#include "logging.hpp"
#include "params.h"

#ifdef __ANDROID__
#include "audioframework.h"
#endif
#define FLOAT float
#include "AudioMgr.hpp"
#include "AudioLevelManager.hpp"
#include "soundLocalizer.hpp"



extern cOwlHook *gpOwlHook;
extern CIRC_BUFF *GetProcToLocalizerCB();
extern int CalcLevels(FLOAT input, LEVEL_STATE *pLevel, LEVELS *pLevelsOut);
extern FLOAT db2mag(FLOAT);

/** checks to see if 2 beam numbers are adjacent. Takes into account wrap
*
* @param beam1 - number of first beam;
* @param beam2 - number of 2nd beam
*
* @return - 1 if adjacent, 0 otherwise
*/
int CheckAdjacentBeamNumbers(int beam1, int beam2)
{
  int maxBeamIndex = (N_BEAMS - 1);
  if((beam1 + 1 <= maxBeamIndex) && (beam1 - 1 >= 0)) // beams not wrapped
  {
    if((beam1 + 1 == beam2) || (beam1 - 1 == beam2))
    { return 1;}
    else
    { return 0;}
  }
  else
  {
    if((beam1 == 0) && ((beam2 == 1) || (beam2 == maxBeamIndex)))
    {
      return 1;
    }
    else if ((beam1 == maxBeamIndex) && ((beam2 == beam1 - 1) || (beam2 == 0)))
    {
      return 1;
    }
    else
    {
      return 0;
    }
  }

}



cSoundLocalizer::cSoundLocalizer()
{
  int i;
  int temp;
  int status;

  //mBeamHistSize = mBeamHistDataIdx = 0;
  pthread_mutex_init(&mResetMutex, NULL);
  pthread_mutex_init(&mOutputMutex, NULL);  
  mFom = 0;
  mAngle = 0;
  mDispersion = 100;
  mConsecutiveCount = 0;
  mTotalEnergy = 0;
  beamMath::getBeamDelays(mBeamDelays, 1);
#ifdef __ANDROID__
  mpProcToLocalizerCB = GetProcToLocalizerCB();
#endif
  mValidMeasures = 0;
  mDebugFd = NULL;
  // log the mic delays for each beam
  if ( DEBUG(SOUND_LOCALIZER) )
  {
    // note that beam delay 0 means the mic is furthest from the
    // sound source; large delays mean the mic is close to the source
    char s[256];
    for (int b = 0; b < N_BEAMS; b++)
    {
      sprintf(&s[0], "BEAM %2d: ", b);
      for (int m = 0; m < N_MICS; m++)
      {
        sprintf(&s[strlen(s)], "%4d ", mBeamDelays[b][m]);
      }
      LOGD("%s\n", s);
    }
  }

  reset();

   // High pass the input data to remove isotropic noise
  for(i = 0; i < N_MICS; ++i)
  {
    EqInit(&mLocalEq[i], DEFAULT_FS);
    EqConfigure(&mLocalEq[i], 0, HPF, 400, 0, 2.0);
    EqActivate(&mLocalEq[i], 0, 1);
    EqConfigure(&mLocalEq[i], 1, HPF, 400, 0, 2.0);
//    EqActivate(&mLocalEq[i], 1, 1);
    EqPostGain(&mLocalEq[i], 0);


  }
  // remove low frequency noise
  EqInit(&mDetectEq, DEFAULT_FS);
  EqConfigure(&mDetectEq, 0, HPF, 400, 0, 2.0);
  EqActivate(&mDetectEq, 0, 1);
  EqConfigure(&mDetectEq, 1, HPF, 400, 0, 2.0);
  EqActivate(&mDetectEq, 1, 1);
  EqConfigure(&mDetectEq, 2, LPF, 6000, 0, 2.0);
  EqActivate(&mDetectEq, 2, 1);

  EqPostGain(&mDetectEq, 6);

#ifdef __ANDROID__
  mpSpeexDetectors = speex_preprocess_state_init(LOCALIZER_FRAME_COUNT,
                     DEFAULT_FS);


  temp = -40;
  status = speex_preprocess_ctl(mpSpeexDetectors,
                                SPEEX_PREPROCESS_SET_NOISE_SUPPRESS,
                                &temp);
#endif                                




}

cSoundLocalizer::~cSoundLocalizer()
{
}

/** reset the sound localizer data structures in the event the owl is moved
*/
void cSoundLocalizer::reset()
{
  int i; 
  
  pthread_mutex_lock(&mResetMutex);
  mHistInd = 0;
  mHistInit = 0;
  // Note the historgram entries are not inited here because they need the
  // timestamp to be as close to where they will be used as practical
  
  setAngleAndDispersion(0,100);
    
  for(i = 0; i < N_BEAMS; ++i)
  {
    int j;
    for(j = 0; j < ENERGY_HIST_COUNT; ++j)
    {
      mSigHistory[i].energyHistory[j].energy = 0;
      mSigHistory[i].energyHistory[j].diffEnergy = 0;      
      mSigHistory[i].energyHistory[j].time = 0;
    }
    mSigHistory[i].startTime = 0;
    mSigHistory[i].lastTime = 0;
    mSigHistory[i].eventCount = 0;
    mSigHistory[i].lastEnergy = 0;
    mSigHistory[i].totalEnergy = 0;
    mSigHistory[i].diffEnergy = 0;
    mSigHistory[i].energyIndex = 0;
    mSigHistory[i].type = SPEECH_TYPE;

  }

  
  pthread_mutex_unlock(&mResetMutex);

}

/** Manages the history of energies as seen by each beam. Calculate total energy
* of all events and the total differences between adjacent energies
*
* @param beamNum - The beam that this energy value is associated with
* @param energy  - energy of the signal during this event
* @param sigFlag - Set if this is a speech signal;
*
* @return - 1 if adjacent, 0 otherwise
*/

int cSoundLocalizer::ManageEnergyHistory(int beamNum, int energy, int sigFlag)
{
  int i;
  int j;
  unsigned long long currTime;
  unsigned long long currTotalEnergy;
  unsigned long long currDiffEnergy;
  int timedOutEvents = 0;

  currTime = getNsecs();

  // age out any old events
  for(i = 0; i < N_BEAMS; ++i)
  {
    for(j = 0; j <  ENERGY_HIST_COUNT; ++j)
    {
      if( (mSigHistory[i].energyHistory[j].energy !=  0) &&
          ((currTime - mSigHistory[i].energyHistory[j].time) > ENERGY_HIST_TIMEOUT))
      {
        mSigHistory[i].energyHistory[j].energy = 0;
        mSigHistory[i].energyHistory[j].diffEnergy = 0;
      }
    }
  }

  if((beamNum >= 0))
  {
    // include all signal events
    if(sigFlag == 1)
    {
      // add the new event
      mSigHistory[beamNum].energyHistory[mSigHistory[beamNum].energyIndex].energy =
        energy;
      mSigHistory[beamNum].energyHistory[mSigHistory[beamNum].energyIndex].diffEnergy
        =
          abs(energy - mSigHistory[beamNum].lastEnergy);
      mSigHistory[beamNum].energyHistory[mSigHistory[beamNum].energyIndex].time =
        currTime;
      mSigHistory[beamNum].lastEnergy = energy;


      mSigHistory[beamNum].energyIndex++;
      if(mSigHistory[beamNum].energyIndex >= ENERGY_HIST_COUNT)
      {
        mSigHistory[beamNum].energyIndex = 0;
      }
    }
  }
  // now calculate the totatl energy for this beam and the difference with the last energy

  for(i = 0; i < N_BEAMS; ++i)
  {
    currTotalEnergy = 0;
    currDiffEnergy = 0;
    mSigHistory[i].eventCount = 0 ;
    for(j = 0; j <  ENERGY_HIST_COUNT; ++j)
    {
      if(mSigHistory[i].energyHistory[j].energy != 0)
      {
        mSigHistory[i].eventCount++;
      }
      currTotalEnergy += mSigHistory[i].energyHistory[j].energy ;
      currDiffEnergy += mSigHistory[i].energyHistory[j].diffEnergy;
    }
    mSigHistory[i].totalEnergy = currTotalEnergy;
    mSigHistory[i].diffEnergy = currDiffEnergy;
  }
  return 0;

}


/** Maintains the signal/noise counters and decides when the beam
* sees more signal than noise
*
* @param beamNum - The selected beam number
* @param score - The noise/signal score
*
* @return >= 0 if this beam has been mostly signal; < 0
*/
int cSoundLocalizer::ManageSigHistory(int beamNum, int sigFlag, int energy)
{
  unsigned long long currTime;
  float energyRatio;
  int i;
  float ratioThreshold;
  static float lastRatio = 0;

  AUDIO_PROPERTIES props;
  GetAudioProperties(&props);

  ratioThreshold = (float)props.localRatio / 10.0;

  if(lastRatio != ratioThreshold)
  {
    printf("Ratio threshold changed %f->%f \n", lastRatio, ratioThreshold);
  }
  lastRatio = ratioThreshold;

  if((currTime - mSigHistory[beamNum].lastTime) >
      SPURIOUS_TIME) // if the events are too far apart; just ignore them
  {
    mSigHistory[beamNum].lastTime = currTime;
    return 0;
  }
  else
  {
    ManageEnergyHistory(beamNum, energy, sigFlag);
    mSigHistory[beamNum].lastTime = currTime;
  }

  if((mSigHistory[beamNum].eventCount >= MIN_EVENT_COUNT) && (mSigHistory[beamNum].totalEnergy != 0))
  {
    // calculate the ratio of total energy to the total differences
    energyRatio =
      (float)mSigHistory[beamNum].diffEnergy / (float)
      mSigHistory[beamNum].totalEnergy;
    if((energyRatio < ratioThreshold))
    {
      if(mSigHistory[beamNum].type != NOISE_TYPE)
      {
        if ( DEBUG(SOUND_LOCALIZER) )      
        {
          printf("%llu Beam %2d Noise  %d %f  %llu \n",getMsecs(),
                 beamNum, mSigHistory[beamNum].eventCount, energyRatio,
                 (currTime - mSigHistory[beamNum].startTime) / 1000000);
        }
      }
      mSigHistory[beamNum].type = NOISE_TYPE;
    }
    else
    {
      if(mSigHistory[beamNum].type != SPEECH_TYPE)
      {
        if ( DEBUG(SOUND_LOCALIZER) )      
        {
          printf("%llu Beam %2d Speech %d %f  %llu \n",getMsecs(),
                 beamNum, mSigHistory[beamNum].eventCount, energyRatio,
                 (currTime - mSigHistory[beamNum].startTime) / 1000000);
        }
      }
      mSigHistory[beamNum].type = SPEECH_TYPE;
    }
    mSigHistory[beamNum].startTime  = currTime;
    mSigHistory[beamNum].lastTime   = currTime;
  }

  if(sigFlag == 1)
  {
    // only pass events from a beam that has been determined speech
    if(mSigHistory[beamNum].type == SPEECH_TYPE)
    {
      return 1;
    }
    else
    {
      return 0;
    }
  }
  else
  {
    return 0;
  }
}

/** This function combines mic correlations into beam correlations.
 *
 * "Beam correlation" is an abuse of terminology in the sense that a
 * beam is not really correlated with another beam. Instead,
 * "correlation" here refers to a sum of all the pairwise mic
 * correlations computed for a particular beam.
 */
void cSoundLocalizer::computeBeamCorrs(void)
{
  int i, b, nC2;
  int m0, m1, tau0, tau1, tau;

  // TODO: consider changing from all-pairs to adjacent-pairs algorithm

  // set first mic
#ifdef PROTOTYPE_P0_5
  static int firstMic = 4; // skip first four mics for P05
#else
  static int firstMic = 0; // use all eight mics for P1
#endif

  // compute total number of pairs, to scale down results below
  nC2 = (N_MICS - firstMic) * (N_MICS - firstMic - 1) / 2; // n Choose 2

  // for every beam, combine its mic correlations
  for (b = 0; b < N_BEAMS; b++)
  {
    mBeamCorrs[b] = 0;
    mBeamCorrsNorm[b] = 0;
    // loop over all mic pairs
    for (m0 = firstMic; m0 < N_MICS; m0++)
    {
      for (m1 = m0 + 1; m1 < N_MICS; m1++)
      {
        double tempXcorr;
        double tempAcorr;
        // look up mic delays relative to a common reference
        tau0 = mBeamDelays[b][m0];
        tau1 = mBeamDelays[b][m1];
        // convert to a delay relative to one of the mics
        tau = tau0 - tau1;
        // update correlation for the beam and the pair of mics
        mBeamCorrs[b] += getMicCorrelation(m0, m1, tau) / nC2;
        tempXcorr = (double)getMicCorrelation(m0, m1, tau);
        // Calculate the auto correlation of one mic and use it normalize
        // crosscorrelation.
        tempAcorr = (double)getMicCorrelation(m0, m0, 0);
        if (tempXcorr != 0)
        { mBeamCorrsNorm[b] += (tempXcorr / tempAcorr) / (double)nC2; }
        else
        { mBeamCorrsNorm[b] = 0; }
      }
    }
  }
}
/** Function that computes the cross-correlation for one microphone
 * pair and one delay.
 *
 * @param m0 is the first microphone.
 * @param m1 is the second microphone.
 * @param tau is the delay in samples; negative delays are NOT
 * supported here, but are supported elsewhere.
 * @param pBuf is a pointer to the PCM data.
 * @param nSamplesPerMic is the number of data samples per microphone.
 * @return the correlation.
 */
int  cSoundLocalizer::computeCorrelation(int m0, int m1, int tau,
    int16_t *pPcmBuf, int nSamplesPerMic)
{
  int i, idx0, idx1;
  int cc, s0, s1;
  long long ccLong;
  const int kBitShift = 16;  // exponent to scale down values to avoid overflow

  // validate tau
  if (tau < 0)
  {
    LOGW("cannot compute correlation with negative delay\n");
    return 0;
  }

  // set indexes to first sample for m0 and delayed sample for m1
  idx0 = m0;
  idx1 = N_MICS * tau + m1;
  // compute cross-correlation, assuming long-term DC component already removed
  cc = 0;
  ccLong = 0;
  i = nSamplesPerMic - tau; // don't exceed buffer length
  while (i--)
  {
    // grab samples
    s0 = pPcmBuf[idx0];
    s1 = pPcmBuf[idx1];
    // cross-multiply and scale to avoid overflow
    cc += (s0 * s1) >> kBitShift;
    ccLong += (s0 * s1);
    // advance indexes to next samples
    idx0 += N_MICS;
    idx1 += N_MICS;
  }


  if (ccLong > INT_MAX)
  {
    ccLong = INT_MAX;
  }
  else if (ccLong < INT_MIN)
  {
    ccLong = INT_MIN;
  }
  cc = ccLong;

  return cc;
}

/** Function that computes the cross-correlation for all microphones
 * and all *needed* delays.
 *
 * @param pBuf is a pointer to the PCM data.
 * @param nSamplesPerMic is the number of data samples per microphone.
 * @return true if correlation table was updated, false if still
 * aggregating results for a future update.
 */
bool cSoundLocalizer::computeMicCorrs(int16_t *pPcmBuf, int nSamplesPerMic)
{
  int b, cc;
  int m0, m1, tau, tau0, tau1;
  bool ret;
  const int kNAggregated = BEAM_N_PCM_CHUNKS; // number of chunks combined
  static int nAggregated = 0;
  static int corr[N_MICS][N_MICS][(BEAM_MAX_TAU +
                                   1)]; // local in-progress calcs
  static int computed[N_MICS][N_MICS][(BEAM_MAX_TAU +
                                       1)]; // tracks needed values

  // set first mic
#ifdef PROTOTYPE_P0_5
  static int firstMic = 4; // skip first four mics for P05
#else
  static int firstMic = 0; // use all eight mics for P1
#endif

  // combine results from kNAggregated chunks before storing in mMicCorrs
  if (nAggregated == kNAggregated)
  {
    // done aggregating, so copy results to mMicCorrs and clear corr table
    memcpy(mMicCorrs, corr, N_MICS * N_MICS * (BEAM_MAX_TAU + 1 ) * sizeof(int));
    memset(corr, 0, N_MICS * N_MICS * (BEAM_MAX_TAU + 1 ) * sizeof(int));
    nAggregated = 0;
    ret = true; // done aggregating results for mMicCorrs table
  }
  else
  {
    // in progress aggregating results for mMicCorrs table
    ret = false;
  }

  // mark all results as not yet computed
  memset(computed, 0, N_MICS * N_MICS * (BEAM_MAX_TAU + 1 ) * sizeof(int));

  // compute autocorrelations
  for (m0 = firstMic; m0 < N_MICS; m0++)
  {
    m1 = m0; // both mics are the same
    tau = 0; // no delay
    cc = computeCorrelation(m0, m1, tau, pPcmBuf, nSamplesPerMic);
    corr[m0][m1][tau] += cc / kNAggregated;
    // mark the results as computed
    computed[m0][m1][tau] += 1;
  }

  // loop over all beams and all pairs of mics, looking up only the
  // needed delays rather than using all possible delays
  for (b = 0; b < N_BEAMS; b++)
  {
    // loop over all mic pairs
    for (m0 = firstMic; m0 < N_MICS; m0++)
    {
      for (m1 = m0 + 1; m1 < N_MICS; m1++)
      {
        // look up mic delays relative to a common reference
        tau0 = mBeamDelays[b][m0];
        tau1 = mBeamDelays[b][m1];
        // convert to a delay relative to one of the mics
        tau = tau0 - tau1;
        // check for negative delays
        if (tau < 0)
        {
          // handle negative delays by making tau positive and
          // swapping roles of mics
          tau = -tau;
          if ( !computed[m1][m0][tau] )
          {
            // compute correlation and update local corr results
            cc = computeCorrelation(m1, m0, tau, pPcmBuf, nSamplesPerMic);
            corr[m1][m0][tau] += cc / kNAggregated;
            // -- mark the results as computed
            // For debugging purposes, a counter is used
            // here rather than a boolean flag. During
            // development, it was useful to examine the
            // counters to see how many combinations of mics
            // pairs and delays were reused. Now, the
            // counters should all be either 0 or 1.
            computed[m1][m0][tau] += 1;
          }
        }
        else
        {
          // use m0, m1, tau as normal
          if ( !computed[m0][m1][tau] )
          {
            // compute correlation and update local corr results
            cc = computeCorrelation(m0, m1, tau, pPcmBuf, nSamplesPerMic);
            corr[m0][m1][tau] += cc / kNAggregated;
            // mark the results as computed
            computed[m0][m1][tau] += 1;
          }
        }
      }
    }
  }
  nAggregated++; // increment the in-progress aggregation counter
  return ret; // return whether still aggregating results or not (see above)
}


/** Get the cross-correlation for two microphones and a given delay.
 *
 * @param m0 is the first microphone.
 * @param m1 is the second microphone.
 * @param tau is the delay in samples; negative delays are supported.
 * @return the correlation, scaled down to avoid overflow.
 */
int  cSoundLocalizer::getMicCorrelation(int m0, int m1, int tau)
{
  // handle negative delays by swapping the roles of the two microphones
  if (tau < 0)
  {
    int temp = m0;
    m0 = m1;
    m1 = temp;
    tau = -tau;
  }

  // check bounds on tau; assume m0 and m1 are legal
  if (tau > BEAM_MAX_TAU)
  {
    LOGW("cross-correlation time delay too big (%d)\n", tau);
    return 0;
  }

  return mMicCorrs[m0][m1][tau];
}

/** @return the dispersion (quality) of the localized sound in [0,100]. */
int  cSoundLocalizer::getDispersion(void)
{
  int dispersion;
  pthread_mutex_lock(&mOutputMutex);  
  dispersion = mDispersion;
  pthread_mutex_unlock(&mOutputMutex);    
  return dispersion;
  
}

/** @return the angle of the localized sound. */
int  cSoundLocalizer::getAngle(void)
{
  int angle;
  pthread_mutex_lock(&mOutputMutex);  
  angle = mAngle;
  pthread_mutex_unlock(&mOutputMutex);    
  return angle;
}

/**  Set the angle of the sound and it's dispersion (quality) [0,100]. atomically
* @param angle - the angle of the found sound
* @param dispersion - dispersion of the sound 
*
 */
void cSoundLocalizer::setAngleAndDispersion(int angle, int dispersion)
{
  static int lastDisp = -1;
  static int lastAngle = -1;  

#if 0  
  if ( DEBUG(SOUND_LOCALIZER) )      
  { 
    if(lastDisp != dispersion)
    { printf(" Dispersion %d->%d \n",lastDisp, dispersion); lastDisp = dispersion;}
    if(lastAngle != angle)
    { printf(" Angle %d->%d \n",lastAngle, angle); lastAngle = angle;}
  }
#endif
  pthread_mutex_lock(&mOutputMutex);  
  mDispersion = dispersion;
  mAngle = angle;
  pthread_mutex_unlock(&mOutputMutex);    
  return;
  
}


/** @return the last angle calculated by the sound localizer - mostly debug. */
int  cSoundLocalizer::getLastAngle(void)
{
  return mLastAngle;
}

/** @return the total energy from the mics. */
int  cSoundLocalizer::getTotalEnergy(void)
{
  return mTotalEnergy;
}

/** Returns the noise/voice for particular beam number
*
* @param - beam number to return status for
*
* @return -  Returns the noise/voice for particular beam number
*/
int cSoundLocalizer::getSpeechStatus(int beamNum)
{
  return  mSigHistory[beamNum].type;
}
  

/** implements a historgram approach for determining the direction of
* signifcant noise. Entries are are entered into histogrqam in time order and
* are timed out.
*
* @param bMax - the beam number of the beam with the maximum score
* @param scoreMax - Score for this beam
* @param totalEnergy - totalEnergy in the sound at the time of this measurement
*
* @return none
*/
void cSoundLocalizer::UpdateVote(int bMax, int scoreMax, int totalEnergy)
{
  int i;
  int maxVote;
  unsigned long long timestamp;
  unsigned long long diff;
  int maxCount;
  int maxIndex;
  int j;
  int adjacent;
  int firstIndex;
  int secondIndex;
  int totalCount;
  static float aveNoiseEnergy = 0;
  int angle;
  int dispersion;
  int diffVotes;

  AUDIO_PROPERTIES props;
  GetAudioProperties(&props);

  dispersion = 100;
  timestamp = getNsecs();

  mInvalidCount = 0;
  // clkear the vote array
  for (i = 0; i < N_BEAMS; ++i)
  {
    mVoteArray[i] = 0;
    mVoteArraySorted[i].count = 0;
    mVoteArraySorted[i].index = 0;

  }
  // initialize the histogram history to no valid entries.
  if (mHistInit == 0)
  {
    for (i = 0; i < HIST_COUNT; ++i)
    {
      mBeamVoteHist[i].beamNum = -1;
      mBeamVoteHist[i].timestamp = timestamp;

    }
    mHistInit = 1;
  }

  // Timeout any entries that have over stayed there welcome.
  for (i = 0; i < HIST_COUNT; ++i)
  {
    diff = timestamp - mBeamVoteHist[i].timestamp;
    if(diff > VOTE_TIMEOUT)
    {
      mBeamVoteHist[i].beamNum = -1;
    }
  }

  // if it is worthy add this score to valid scores.
  // don't put any in during active far end
  // HMB - Add description of noise/speech determnination
  float temp;
  temp = db2mag(mPostNRLevels.rmsLevel) / db2mag(mPreNRLevels.rmsLevel);
  mPrePostRatio = ARMAFilter(mPrePostRatio, temp, 0.1, 0.1);
  temp = mPrePostRatio;
  if(temp > 1.0)
  { temp = 1.0; }

  float sigScore;
  if((temp > PRE_POST_RATIO_THRESH)
      && (mSpxDetects.meanApriorSNR > APRIORI_SNR_THRESH))
  {
    sigScore = 1;
  }
  else
  {
    sigScore = 0;
  }


  mDebugVal = 0;
  if(AMIsSpeakerActive() == 0)
  {
    int sigFlag;
    if(sigScore > 0.5)
    {sigFlag = 1;}
    else
    {
      sigFlag = 0;
    }

    if(scoreMax > ALT_BEAM_SCORE_THRESHOLD)
    {
      int score;

      score = ManageSigHistory(bMax, sigFlag, totalEnergy);
      // if the event is determined to be speech enter it in the history buffer
      if(score > 0)
      {
        mBeamVoteHist[mHistInd].beamNum = bMax;
        mBeamVoteHist[mHistInd].timestamp = timestamp;
        if(sigScore > 0.5)
        {
          mBeamVoteHist[mHistInd].speechNoise = 1;
        }
        else
        {
          mBeamVoteHist[mHistInd].speechNoise = 0;
        }
        mHistInd++;

        if (mHistInd >= HIST_COUNT)
        {
          mHistInd = 0;
        }
      }
    }
    else
    {
      mMinEnergy = ARMAFilter(mMinEnergy, mTotalEnergy, 0.05, 0.05);
    }
  }

  // count the "votes" for each beam
  totalCount = 0;
  for (i = 0; i < HIST_COUNT; ++i)
  {
    if(mBeamVoteHist[i].beamNum >= 0)
    {
      mVoteArray[mBeamVoteHist[i].beamNum] += mBeamVoteHist[i].speechNoise;
      totalCount += mBeamVoteHist[i].speechNoise;
    }
    else
    {
      mInvalidCount++;
    }
  }
  maxVote = 0;


  // Sort the votes array by number of hits for each beam
  j = 0;
  // This loop and it's inner loop sort the vote array into the sorted array.
  // It does this by interatively looking for largest vote-getter, saving that
  // as the next sorted value, then zeroing that beams votes and then repeating.
  // The loop ends when all non-zero vote-getters have been sorted (i.e. all
  // vote counts have been zeroed
  // Max iterations is N_BEAMS*N_BEAMS, but this will only happen if all beams
  // get votes,
  do
  {
    maxCount = 0;
    maxIndex = 0;
    for(i = 0; i < N_BEAMS; ++i)
    {
      if(mVoteArray[i] > maxCount)
      {
        maxCount = mVoteArray[i];
        maxIndex = i;
      }
    }
    if(maxCount > 1) // discard all ones and zeros, they are errors
	
    {
      mVoteArraySorted[j].count = maxCount;
      mVoteArraySorted[j].index = maxIndex;
      j++;
    }
    mVoteArray[maxIndex] = 0;

  }
  while(maxCount != 0);

  // If we don't have enough votes report maximum dispersion.
  // Otherwise the dispersion is the number of votes the winner got divided
  // by the total votes.

  // report the winner of the voting.
  mLastBeamNumber = mBeamNumber;
  mBeamNumber = mVoteArraySorted[0].index;
  // Allow the last one to persist if it has been going for awhile.
  
  // calculate new dispersion

  mTotalCount = totalCount;
  double fom;  
  if(mTotalCount != 0)
  {
    fom = (float)mVoteArraySorted[0].count/(float) mTotalCount;    
  }
  else
  {
    fom = 0;
  }
  mFom = ARMAFilter(mFom,fom,0.05,0.05);

  #ifdef V2_3_0_0
  diffVotes = mVoteArraySorted[0].count - mVoteArraySorted[1].count;  
  if(diffVotes < MIN_VOTE_DIFF)
  { dispersion = 100;}
  else
  {
    dispersion = 100 - ((diffVotes) * 100 / MAX_VOTE_DIFF);
    if(dispersion < 0)
    { dispersion = 0;}

  }  
  #else // V2_3_0_0
  
  int count0; 
  count0 = mVoteArraySorted[0].count;
  
  if(CheckAdjacentBeamNumbers(mVoteArraySorted[0].index, mVoteArraySorted[1].index ) == 1)
  {
    count0 += mVoteArraySorted[1].count;
  }
    
  dispersion = 100 - ((count0*100)/mTotalCount);
  
  
  
  #endif
  
  
  // allow and old direstiong to persist for awhile
  if(mLastBeamNumber != mBeamNumber)
  {
    mConsecutiveCount--;
    if(mConsecutiveCount <= 0)
    {
      // Accept the new one
      mConsecutiveCount = 1;
    }
    else
    {
      // else keep the last one and is't dispersion
      mBeamNumber = mLastBeamNumber;
      dispersion = getDispersion();
    }
  }
  else
  {
    // if the same as last time favor it more
    mConsecutiveCount++;
    if(mConsecutiveCount > MAX_CONSECUTIVE_COUNT)
    { mConsecutiveCount = MAX_CONSECUTIVE_COUNT;};
  }

  
  // report the angle as the middle of the beam (this empircally works better).
  angle = (360. / N_BEAMS) * ((float)mBeamNumber + 0.5);
  if(angle > 360)
  {angle = angle - 360;}
  if(angle < 0)
  {angle = angle + 360;}

  if(mLastAngle != angle)
  { mLastAngle = angle; }
  setAngleAndDispersion(angle, dispersion);

}
/** This function processes PCM data to localize sound sources;
 * various statistics are computed along the way.
 *
 * @param pPcmBuf is a pointer to the PCM sound data transferred via
 * the owlUpdate callback in main.cpp.
 * @param bufLen is the data buffer length.
 * @return true if correlation table was updated, false if still
 * aggregating results for a future update.
 */
bool cSoundLocalizer::processPcmData(int16_t *pPcmBuf, int nSamplesPerMic)
{
  int b, score, bMax, scoreMax;
  int b2, score2, bMax2, scoreMax2;
  bool doneAggregating;
  static int nLowScores = 0;
  double minNorm;
  double maxNorm;
  int diffNorm;
  int maxIndex;
  long long tempEnergy;


  // fill the mic cross-correlation table
  doneAggregating = computeMicCorrs(pPcmBuf, nSamplesPerMic);

  if (doneAggregating)
  {
    // use auto-correlations to compute mic energy
    mTotalEnergy = 0;
    tempEnergy = 0;
    for (int m = 0; m < N_MICS; m++)
    {
      mMicEnergy[m] = getMicCorrelation(m, m, 0);
      tempEnergy += mMicEnergy[m];
    }

    if(tempEnergy > INT_MAX)
    { tempEnergy = INT_MAX; }

    mTotalEnergy = tempEnergy;

    // combine to get beam correlations
    computeBeamCorrs();

    // determine the beam with the best cross correlation
    // determing the minimum xcorr at the same time.

    minNorm = mBeamCorrsNorm[0];
    maxNorm = mBeamCorrsNorm[0];
    maxIndex = 0;
    for (b = 1; b < N_BEAMS; b++)
    {

      if (mBeamCorrsNorm[b] < minNorm)
      {
        minNorm = mBeamCorrsNorm[b];
      }
      if (mBeamCorrsNorm[b] > maxNorm)
      {
        maxNorm = mBeamCorrsNorm[b];
        maxIndex = b;
      }
    }
#define MAX_DIFF_INT 1000
    diffNorm = (int) ((maxNorm - minNorm) * MAX_DIFF_INT);

    // This is the maximum of the normalized cross correlations
    bMax = maxIndex;
    // Use the difference between the minimum xcorr and the max xcorr
    // as a measure of goodness. If the diff is low, then low confidence
    scoreMax = diffNorm;


    UpdateVote(bMax, scoreMax, mTotalEnergy);
  }


  return doneAggregating;
}

/** prints debug information associated with the localizer to the serial port
*
*/
void cSoundLocalizer::printDebug()
{
  int i;
  double spkrLvl;
  cAudioMgr *pAM;

  AUDIO_PROPERTIES props;
  GetAudioProperties(&props);

  pAM = gpOwlHook->getAM();
  spkrLvl = pAM->getSpkrLevel();

  
  switch(props.localDebug)
  {
  case 0:
    break;
  case 1:
    i = 0;
    printf("Ang %3d %3d | %4.1f  %4.1f | %3d |", mAngle, mDispersion,mFom*100.0,spkrLvl,mTotalCount);

    while(mVoteArraySorted[i].count != 0)
    {
      printf("%2d-%3d|", mVoteArraySorted[i].index, mVoteArraySorted[i].count);
      i++;
    }
    printf("\n");
    break;
  case 2:
    printf("Sig/Noise history - |");
    for(i = 0; i < N_BEAMS; ++i)
    {
      if(mSigHistory[i].eventCount != 0)
      {
        printf("%2d-%3d-%1d|", i, mSigHistory[i].eventCount, mSigHistory[i].type);
      }
    }
    printf("\n");
    break;
  case 3:
    puts( "\033[2J" );    
    puts( "\033[1;1H");        
    for(i = 0; i < N_BEAMS; ++i)
    {
      printf("%2d|",i);
    }
    printf("\n");    
    for(i = 0; i < N_BEAMS; ++i)
    {
      printf(" %1d|",mSigHistory[i].type);
    }
    printf("\n");    
    break;
  case 4:
    puts( "\033[2J" );    
    puts( "\033[1;1H");        
    for(i = 0; i < N_BEAMS; ++i)
    {
      printf("%2d|",i);
    }
    printf("\n");    
    for(i = 0; i < N_BEAMS; ++i)
    {
      printf(" %1d|",mSigHistory[i].eventCount);
    }
    printf("\n");    
    break;
  case 5:
    printf("%2f,%2f\n",mFom,spkrLvl);
    break;
    
    
  default:
    break;
  }
}


/** This function receives PCM data from cOwlHook and passes it on for
 * processing.
 *
 * @param pPcmBuf is a pointer to the PCM sound data transferred via
 * the owlUpdate callback in main.cpp.
 * @param bufLen is the data buffer length.
 * @return true if correlation table was updated, false if still
 * aggregating results for a future update.
 */
bool cSoundLocalizer::update(int16_t *pPcmBuf, int bufLen)
{
  bool   doneAggregating = false;
  const  int kDebugInterval = 8;
  static int debugCount = 0;
  static int periodicCount; // used to control periodic house keeping
  int i;
  int maxCount;
  int maxIndex;
  static int logNum = 0;
  char filename[512];
  int localDone;

  AUDIO_PROPERTIES props;
  GetAudioProperties(&props);

  pthread_mutex_lock(&mResetMutex);

  if((props.localLog == 1) && (mDebugFd == NULL))
  {
    sprintf(filename,"/data/local_log_%d.csv",logNum);
    mDebugFd = fopen(filename, "wb");
    printf("Opened localizer log file %s \n", filename);
    logNum++;
  }
  if((props.localLog == 0) && (mDebugFd != NULL))
  {
    fclose(mDebugFd);
    printf("Closed  localizer log file \n");
    mDebugFd = NULL;
  }

  periodicCount += bufLen;

#define  BYTES_PER_SEC (48000 * 8 * 1)
  if(periodicCount > BYTES_PER_SEC)
  {
    ManageEnergyHistory(-1, 0, 0); // just age out events
    if ( DEBUG(SOUND_LOCALIZER) )
    {
      printDebug();
    }
    periodicCount = 0;    
  }
#ifdef __ANDROID__
  localDone = 0;
  while((CircBuffFilled(mpProcToLocalizerCB) >= LOCALIZER_FRAME_COUNT * N_MICS *
      sizeof(short)) )
  {
    static int localOverrunCount = -1;

    
    if((localOverrunCount != mpProcToLocalizerCB->overrunCount) && (DEBUG(SOUND_LOCALIZER) ))
    {
      printf("Localizer overrun %d \n",mpProcToLocalizerCB->overrunCount);
      localOverrunCount = mpProcToLocalizerCB->overrunCount;
    }
    CircBuffRead(mpProcToLocalizerCB, (ITEM_DEF *)&mProcBuffer[0],
                 LOCALIZER_FRAME_COUNT * N_MICS * sizeof(short));
    for(i = 0; i < N_MICS; ++i)
    {
      EqProcess(&mLocalEq[i], &mProcBuffer[0], LOCALIZER_FRAME_COUNT, i, N_MICS);
    }
    ExtractChannel(mProcBuffer, mDetectBuff, LOCALIZER_FRAME_COUNT, 0, 8);
    EqProcess(&mDetectEq, mDetectBuff, LOCALIZER_FRAME_COUNT, 0, 1);
    for(i = 0; i < LOCALIZER_FRAME_COUNT; ++i)
    {
      float temp;
      temp = mDetectBuff[i] / 32767.0;
      CalcLevels(temp, &mPreNRLevelState, &mPreNRLevels);
    }
    speex_preprocess_run(mpSpeexDetectors, mDetectBuff);
    for(i = 0; i < LOCALIZER_FRAME_COUNT; ++i)
    {
      float temp;
      temp = mDetectBuff[i] / 32767.0;
      CalcLevels(temp, &mPostNRLevelState, &mPostNRLevels);
    }

    DETECTORS spxDetects;
    GetSpeexDetectors(&spxDetects, NULL, mpSpeexDetectors);
	// Smoothing removed from get speex detectors; must now be done here.
    #define APRIOR_SMOOTH 0.1
    mSpxDetects.meanApriorSNR = ARMAFilter( mSpxDetects.meanApriorSNR, spxDetects.meanApriorSNR,APRIOR_SMOOTH, APRIOR_SMOOTH);    

    doneAggregating = processPcmData(&mProcBuffer[0], LOCALIZER_FRAME_COUNT);
    if(doneAggregating)
      break;
  }
#endif
  pthread_mutex_unlock(&mResetMutex);
  return doneAggregating;
}
