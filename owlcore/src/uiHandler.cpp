/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  uiHandler.cpp
 *  This is the implementation file for cUiHandler.
 *   --> Runs a thread which updates each aspect of the UI.
 *       - Polls for button events.
 *       - Notifies the Owl's analytics when USB is connected/disconnected, and
 *          when UVC streaming starts/stops.
 */

#include "blackBox.hpp"
#include "common.hpp"
#include "logging.hpp"
#include "shellProcess.hpp"
#include "uiHandler.hpp"

#ifdef __ANDROID__
#include "audioframework.h"
#include "UVCHandler.h"
#endif

#include <sys/stat.h>
#include <sys/time.h>
#include <unistd.h>

// #define DEBUG_REBOOT_CHECKER  // Only for testing!
#ifdef DEBUG_REBOOT_CHECKER
#define LOGID LOGI
#else
#define LOGID(...) {}
#endif

/** Define path to time backup files */
#ifdef __ANDROID__
const char *cUiHandler::mkLatestTime = TIME_FILE_NAME;
const char *cUiHandler::mkLatestTimeBak = TIME_FILE_NAME_BAK;
#else
const char *cUiHandler::mkLatestTime = TIME_FILE_NAME;
const char *cUiHandler::mkLatestTimeBak = TIME_FILE_NAME_BAK;
#endif



/** @param pButtonHandler a pointer to cOwlHook's button handler.
 *  @param pLedInterface a pointer to cOwlHook's LED interface.
 */
cUiHandler::cUiHandler(cButtonHandler *pButtonHandler, cLedInterface *pLedInterface)
  : mUiThread(std::bind(&cUiHandler::handleUi, this), "uiHandler"),
    mpButtonHandler(pButtonHandler), mpLedInterface(pLedInterface)
{
  mUiThread.start();
}

cUiHandler::~cUiHandler()
{
  stop();
}

/** Tell UI handler about the analytics object.
 *
 * @param pAnalytics is a pointer to the cAnalytics object.
 */
void cUiHandler::setAnalytics(cAnalytics *pAnalytics)
{
  mpAnalytics = pAnalytics;
}

/** Notifies the UI thread to start handling the UI. */
void cUiHandler::start()
{
  mActive = true;
}

/** Notifies the UI thread to stop handling the UI. */
void cUiHandler::stop()
{
  mActive = false;
  usleep(mkStopDelayUs);
}

/** Sets the current button context, which defines the bound buttons/combos and
 *   the callbacks they trigger.
 *
 * @param pButtonContext the context to set as active.
 */
void cUiHandler::setButtonContext(const cButtonContext *pButtonContext)
{
  mpButtonHandler->setContext(pButtonContext);
}

/** Sets the current LED context, which defines the current LED states (on/off).
 *
 * @param pLedContext the context to set as active.
 */
void cUiHandler::setLedContext(cLedContext *pLedContext)
{
  mpLedInterface->setContext(pLedContext);
}

/** Handles the UI while active. Run on a separate thread.
 *  Handles any button events, and updates analytics based on USB/UVC states.
 */
void cUiHandler::handleUi()
{
  int usbConnected = 0;
  int usbStateChanged = 0;
  int usbStateFixup = 0;
  int uvcStreaming = 0;
  int uvcStateChanged = 0;
  int usbSpeakerConnected = 0;
  int usbMicConnected = 0;
  int meetingInProgress = 0;
  int meetingInProgressChanged = 0;
  int micMuted = 0;
  int micMuteChanged = 0;
  static int iterationCount = 0;
  static int previouslyInMeeting = 0;
  static int micMutedPreviously = 0;

  updateSystemTime();

  while(mUiThread.isRunning())
    {
      if(mActive)
	{
	  // handle button events
	  mpButtonHandler->handleButtons();

	  // get USB/UVC states
#ifdef __ANDROID__

#ifdef REBOOT_NIGHTLY
	  // Check maintenance reboot, but don't reboot somebody's Linux system!
          updateNightlyReboot();
#endif /* REBOOT_NIGHTLY */

	  getUsbConnectedState(&usbConnected, &usbStateChanged);
	  getUsbStateFixup(&usbStateFixup);
	  getUvcStreamState(&uvcStreaming, &uvcStateChanged);
	  getOwlCoreMeetingInProgress(&meetingInProgress,
	                              &meetingInProgressChanged);
	  
	  usbSpeakerConnected = GetUsbSpkrActive();
	  usbMicConnected = GetUsbMicActive();

	  micMuted = GetMicMute();
	  if (micMuted != micMutedPreviously)
	    {
	      micMuteChanged = 1;
	      micMutedPreviously = micMuted;
	    }
#else
          // debug sequencer for testing meeting records on Linux
	  iterationCount++;
	  if (iterationCount == 3 * 30) // Start a meeting 3 seconds into run
	    {
	      uvcStreaming = true;
	      uvcStateChanged = true;
	      usbConnected = true;
	      usbStateChanged = true;
	      meetingInProgress = true;
	      meetingInProgressChanged = true;
	    }
	  else
	    {
	      uvcStateChanged = false;
	      usbStateChanged = false;
              meetingInProgressChanged = false;
	    }
	  if (iterationCount == 343 * 30) // End an 40 second test meeting
	    {
	      uvcStreaming = false;
	      uvcStateChanged = true;
	    }
#endif

 	  // tick the meeting state monitoring state machine
	  // NOTE: the mMicActive flag can get stuck in the on state if the
	  // mic stream stops when there's loud noise present as the stream
	  // closes. However, at that time the usbMicConnected flag reliably
	  // goes false. So, by ANDing the two flags together, we get a more
	  // reliable indicator for when the mic audio really terminates.
	  // The same applies to the speaker.
	  mpAnalytics->updateMeetingState(usbConnected, uvcStreaming,
	                                  usbSpeakerConnected && mSpeakerActive,
	                                  usbMicConnected && mMicActive,
	                                  usbStateFixup);
	  
	  // handle USB connected/disconnected
	  if(usbStateChanged)
	    {
	      cLedContext *pOldContext = mpLedInterface->getContext();
	      
	      mpLedInterface->setContext(&mSystemLedContext);
	      mSystemLedContext.enable(LEDS_MUTE | LEDS_BLUETOOTH);
	      usleep(mkUsbBlinkTimeUs);
	      mSystemLedContext.disable(LEDS_MUTE | LEDS_BLUETOOTH);
	      
	      usleep(mkUsbBlinkTimeUs);
	      mpLedInterface->setContext(pOldContext);
	    }

	  // handle UVC started/stopped streaming
	  // NOTE: Do not use 'meetingInProgressChanged' because it is not
	  // always reliably reporting a state change
	  if(uvcStateChanged)
	    {
	      if(uvcStreaming != 0)
	        { mpLedInterface->enable(LEDS_EYE); }
	      else
	        { mpLedInterface->disable(LEDS_EYE); }
	    }
	}
      
      saveSystemTime();

      usleep(mkThreadSleepUs);
    }
}

/** Informs the UiHandler whether sound is heard through the mics.
 *
 * @param active true if sound is currently audible.
 *
 */
void cUiHandler::setMicActive(bool active)
{
  mMicActive = active;
}

/** Informs the UiHandler whether sound is emitted by the speaker.
 *
 * @param active true if sound is currently audible.
 *
 */
void cUiHandler::setSpeakerActive(bool active)
{
  mSpeakerActive = active;
}

/** Checks to see if the system needs a maintenance reboot.
 *
 * If the system time is valid, and the network is up, and the current
 * time is inside the reboot time window, and the system has been
 * up for longer than the duration of the time window (so we don't
 * reboot repeatedly during the window), and there is no meeting happening,
 * and reboot has not been disabled with a parameter file, then reboot!
 *
 * This should only be called once each time through the uiHandler update
 * function so that it keeps roughly correct track of the passage of time.
 */
void cUiHandler::updateNightlyReboot()
{
  static int iterationCount = 0;

  // Only check if a reboot is required once every mkRebootCheckPeriodMinutes
  if (mkRebootCheckPeriodMinutes * 60 <=
      ((++iterationCount * mkThreadSleepUs) / (1000 * 1000)))
    {
      bool networkConnected = false;
      int upTimeSec = 0;
      bool timeValid = false;
      bool timeWithinRebootWindow = false;
      bool maintenanceRebootEnabled = true;
      std::string delims(" \t:.");
      std::vector<std::string> toks;
      time_t now;
      struct tm lTime;
      int thisYear;
      int thisHour;

      // Reset counter used to make the checks infrequent
      iterationCount = 0;

      // Check to see if the WiFi network interface is up (only valid for
      // Android systems)
      cShellProcess netcfg(std::string("netcfg"),
        /* The following lambda function tokenizes each line returned by the
         * 'netcfg' system call, then finds the line containing the 'wlan0'
         * interface, then determines if the interface is up by checking to
         * see if the first element of the IP address is non-zero.
         */
        [&](const std::string *pLine)
        {
          if (pLine != nullptr)
            {
              toks.clear();
              tokenize(*pLine, delims, toks);
              if ((toks[0].find("wlan0") != std::string::npos)
                  && (toks[2][0] != '0') && isdigit(toks[2][0]))
                { LOGID(pLine->c_str()); networkConnected = true; }
              return true; }
          else
            { return false; }
        },
        true );

      if (networkConnected)
        { LOGID("Network connected\n"); }
      else
        { LOGID("Network disconnected\n"); }

      // Find out how long the system has been running since it last booted
      cShellProcess uptime(std::string("cat /proc/uptime"),
        /* The following lambda function tokenizes the line returned by
         * printing the /proc/uptime file, then verifies the first token of
         * the response is a number, and uses that number for the up time.
         */
        [&](const std::string *pLine)
        {
          if (pLine != nullptr)
            {
              toks.clear();
              tokenize(*pLine, delims, toks);
              if (!toks.empty())
                {
                  upTimeSec = strtol(toks[0].c_str(), NULL, 0);
                  size_t newlinePos = pLine->find("\n");
                  std::string lineNoNewline(*pLine, 0, newlinePos);
                  LOGID("uptime lambda: %s => uptime = %d seconds\n",
                        lineNoNewline.c_str(), upTimeSec);
                  return true;
                }
            }
          return false;
        },
        true );

      // Determine the current real time (GMT - we don't know time zones yet)
      time(&now);
      gmtime_r(&now, &lTime);
      thisYear = (1900 + lTime.tm_year); // tm_year 0 is 1900
      thisHour = lTime.tm_hour - 6; // Offset of central time from UTC
      if (thisHour < 0) { thisHour += 24; }

      if (thisYear >= 2017)
        { timeValid = true; }
      if ((thisHour >= mkHourToRebootStart) &&
          (thisHour < mkHourToRebootEnd))
        { timeWithinRebootWindow = true; }
      LOGID("tm_year = %d, tm_hour = %d (%s), within reboot window: %s\n",
            thisYear, thisHour,
            ((timeValid) ? "valid" : "invalid"),
            ((timeWithinRebootWindow) ? "true" : "false"));

      // Make sure maintenance reboots are enabled (by default true, and
      // only disabled by writing 0 to reboots file)
      maintenanceRebootEnabled = loadParameter<bool>(UI_HANDLER_PARAMS,
                                                     "reboots", true);

      /* If the system time is valid, and the network is up, and the current
       * time is inside the reboot time window, and the system has been
       * up for longer than the duration of the time window (so we don't keep
       * rebooting during the window), and there is no meeting happening,
       * then reboot!
       */
      if (networkConnected &&
          timeValid &&
          timeWithinRebootWindow &&
          (upTimeSec > (mkHourToRebootEnd - mkHourToRebootStart) * 60 * 60) &&
          maintenanceRebootEnabled &&
          (mpAnalytics->getMeetingState() == cAnalytics::NOT_MEETING))
        {
          LOGI("Performing maintenance reboot!\n");
          int rc = system("reboot");
        }
    }
}

/** Save current time to file keeping backup in case of write failure.
 *
 * Updates should only be done once a second, but this is called
 * much more frequently, so it should only save the time occasionally.
 *
 * The stored file is used by updateSystemTime() when the system restarts.
 */
void cUiHandler::saveSystemTime(void)
{
  static struct timeval lastSaveTime = { 0, 0 };
  struct timeval now;
  struct timeval diff;
  struct timeval oneSec = { 1, 0 };

  gettimeofday(&now, NULL);
  timersub(&now, &lastSaveTime, &diff);
  if (timercmp(&diff, &oneSec, >))
    {
      lastSaveTime = now;
      mode_t origMask = umask(0);
      rename(mkLatestTime, mkLatestTimeBak);  // Make backup in case write
      FILE *pFile = fopen(mkLatestTime, "w"); //  to new file fails
      if (pFile != nullptr)
        {
          fprintf(pFile, "%ld, %ld\n", now.tv_sec, now.tv_usec);
          fclose(pFile);
          sync();
        }
      umask(origMask);
    }
}

/** Update system time at startup, if necessary.
 *
 * Purpose: Ensure that the system's time is always increasing, even if
 * the system is powered off for a while, and does not have internet access.
 *
 * Read time from mkLatestTime file; compare it to current time; if
 * current time is smaller than time stored in the file, set the
 * current time to the time from the file.
 */
void cUiHandler::updateSystemTime(void)
{
  struct timeval fileNow;
  struct timeval now;
  bool success = true;

  if (!getSystemTimeFromFile(mkLatestTime, &fileNow))
    {
      if (!getSystemTimeFromFile(mkLatestTimeBak, &fileNow))
        {
          success = false;
        }
    }
  if (success)
    {
      gettimeofday(&now, NULL);
      if (timercmp(&fileNow, &now, >))  // fileNow is later than now, update
        {
          if (settimeofday(&fileNow, NULL) != 0)
            {
              LOGW("updateSystemTime(): settimeofday FAILED!\n");
            }
          else
            {
              // Read back new system time and report it for confirmation
              LOGI("updateSystemTime(): time set to \"%s\"\n",
                   currentDateTimeHumanReadable().c_str());
            }
        }
      else
        {
          LOGI("updateSystemTime(): clock does not need update\n");
        }
    }
}

/** Get the time stored in a time backup file.
 *
 * @param pFileName points to a file name string for the backup file.
 * @param pTime points to the time structure to fill in with the file time.
 *
 * @return true if file was read correctly, false if not.
 */
bool cUiHandler::getSystemTimeFromFile(const char *pFileName,
                                       struct timeval *pTime)
{
  struct timeval fileNow;
  bool success = false;

  FILE *pFile = fopen(pFileName, "r");
  if (pFile == nullptr)
    {
      LOGW("getSystemTimeFromFile(): couldn't open \"%s\"\n", pFileName);
    }
  else
    {
      if (fscanf(pFile, "%ld, %ld", &fileNow.tv_sec, &fileNow.tv_usec) == 2)
        {
          *pTime = fileNow;
          success = true;
        }
      else
        {
          LOGW("getSystemTimeFromFile(): couldn't read 2 ints from \"%s\"\n",
               pFileName);
        }
      fclose(pFile);
    }

  return success;
}

