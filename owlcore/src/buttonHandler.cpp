/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  buttonHandler.cpp
 *  This is the implementation file for cButtonHandler.
 *   --> High-level button interface. Callbacks can be bound to button events.
 */

#include "buttonHandler.hpp"
#include "buttonContext.hpp"
#include "common.hpp"
#include "logging.hpp"
#include "params.h"

#include <sys/resource.h>
#include <unistd.h>

#ifdef __ANDROID__
#include "ButtonHandler.h"
#include "sx1508.h"
/* NOTE: Values duplicated from preprocessor defines in ButtonHandler.h (libOwlPG).
 *        Renamed by their main use to match their function and protect unsafe
 *         access of libOwlPG. */
const buttonType_t cButtonHandler::mkMuteButtonMask      = SX1508_BUTTON_MUTE;
const buttonType_t cButtonHandler::mkVolUpButtonMask     = SX1508_BUTTON_VOLUP;
const buttonType_t cButtonHandler::mkVolDnButtonMask     = SX1508_BUTTON_VOLDN;
const buttonType_t cButtonHandler::mkOwlButtonMask       = SX1508_BUTTON_BT;
const buttonType_t cButtonHandler::mkAllButtonMask       = SX1508_BUTTON_ALL;
#else // Linux
// placeholders -- no buttons on Linux
const buttonType_t cButtonHandler::mkMuteButtonMask      = 0x01;
const buttonType_t cButtonHandler::mkVolUpButtonMask     = 0x02;
const buttonType_t cButtonHandler::mkVolDnButtonMask     = 0x10;
const buttonType_t cButtonHandler::mkOwlButtonMask       = 0x20;
const buttonType_t cButtonHandler::mkAllButtonMask       = 0x33;
#endif

const int cButtonHandler::mkDelayThreadSleepUs = 100000;


cButtonHandler::cButtonHandler()
  : mDelayThread(std::bind(&cButtonHandler::delayUpdate, this),
		 "btnDelayThread", DEFAULT_THREAD_PRIORITY )
{
#ifdef __ANDROID__
  initButtons();
#endif
  
  mDelayThread.start();
}

cButtonHandler::~cButtonHandler()
{
  mDelayThread.join();
}

/** Binds a callback to a single button press.
 *
 * @param btnEvent the button event to bind to.
 * @param callback the callback to bind.
 *
 * @return whether the bind succeeded.
 */
bool cButtonHandler::bind(sButtonEvent btnEvent, buttonCallback_t callback)
{
  if(!callback)
    {
      LOGW("Callback passed to cButtonHandler::bind is null.\n");
      return false;
    }
  else if(btnEvent.type == BUTTON_NONE)
    {
      LOGW("Invalid button action passed to cButtonHandler::bind.\n");
      return false;
    }
  
  mBoundEvents.emplace_back(std::vector<sButtonEvent>{ btnEvent }, callback);
  return true;
}

/** Binds a callback to a button combination.
 *
 * @param comboEvents the combination of button events to bind to.
 * @param callback the callback to bind.
 *
 * @return whether the bind succeeded.
 */
bool cButtonHandler::bind(std::vector<sButtonEvent> comboEvents,
			  buttonCallback_t callback )
{
  if(!callback)
    {
      LOGW("Callback passed to cButtonHandler::bind is null.\n");
      return false;
    }
  else if(comboEvents.size() == 0)
    {
      LOGW("Button event list passed to cButtonHandler::bind is empty.\n");
      return false;
    }
  
  mBoundEvents.emplace_back(comboEvents, callback);
  return true;
}

/** Clears all bound combinations/callbacks. Automatically pauses the button
 *   handling thread if necessary. */
void cButtonHandler::unbindAll()
{
  mBoundEvents.clear();
}

/** Sets the active button context. Clears all button bindings and replaces them
 *   with those from the new context.
 *
 * @param pContext a pointer to the context to set.
 */
void cButtonHandler::setContext(const cButtonContext *pContext)
{
  // handle an event releasing all currently pressed buttons
  handleEvent(BUTTON_NONE, BUTTON_NONE, mCurrentState);
  mCurrentState = BUTTON_NONE;

  // swap out the context's events
  mBoundEvents.clear();
  for(auto binding : pContext->getBindings())
    { bind(binding.aEvents, binding.callback); }
}

/** Handles a button event.
 *
 * @param state the button states after the event.
 * @param pressed the mask of buttons that were pressed.
 * @param released the mask of buttons that were released.
 */
void cButtonHandler::handleEvent(buttonType_t state, buttonType_t pressed,
				 buttonType_t released )
{
  // update each combo with the sets of pressed and released buttons, which will
  //  call any triggered callbacks.
  for(auto &combo : mBoundEvents)
    { combo.updateState(state, pressed, released); }
}

/** Polls the button states and calls the appropriate callbacks when
 *   buttons are pressed. Runs in a separate thread. */
void cButtonHandler::handleButtons()
{
#ifdef __ANDROID__
  static buttonStatus status;
  testButtons(&status);

  if(status.events != BUTTON_NONE)
    {
      // apply the button events to the previous state
      buttonType_t newState = (status.previous ^ status.events);
      
      // handle all the buttons with hardware events
      buttonType_t pressed = (newState & status.events);
      buttonType_t released = (~newState & status.events);
      handleEvent(newState, pressed, released);

      if(newState != status.current)
	{
	  // warn about the missed event. Debounce timing may need to be
	  //  adjusted further if these show up.
	  LOGW("Correcting missed a button event (0x%X)\n",
	       status.events );
	      
	  // missed a state change -- correct the button states
	  buttonType_t missedEvents = (newState ^ status.current);
	      
	  // handle an extra event for the missed state change
	  pressed = (status.current & missedEvents);
	  released = (~status.current & missedEvents);
	  handleEvent(status.current, pressed, released);
	}
      
      // update states
      mCurrentState = status.current;
    }
#else
  usleep(30000);
  
#endif
}

/** Updates the delay timers of bound combinations. Run in a separate thread. */
void cButtonHandler::delayUpdate()
{
  static timespec lastTs;
  timespec currTs;
  currentTimeSpec(&currTs);
  
  double timeElapsed = 0.0;
  if(lastTs.tv_sec != 0 || lastTs.tv_nsec != 0)
    { // get the time difference
      timeElapsed = elapsedTimeSec(lastTs, currTs);
    }
  lastTs = currTs;

  // update 
  for(auto &combo : mBoundEvents)
    { combo.updateTime(timeElapsed); }

  usleep(mkDelayThreadSleepUs);
}


// cButtonCombo implementation

/** @param events the sequence of button events for this combination.
 *  @param callback the function to call when the combo is entered successfully.
 */
cButtonHandler::cButtonCombo::cButtonCombo(std::vector<sButtonEvent> events,
					   buttonCallback_t callback )
  : mCallback(callback)
{
  buttonType_t state = BUTTON_NONE; // start from an empty state
  
  mEventSequence.reserve(events.size());
  mStateSequence.reserve(events.size());
  for(auto &event : events)
    {
      switch(event.action)
	{
	case eButtonClick:
	  // change to one press and one release.
	  event.action = eButtonPress;
	  mEventSequence.push_back(event);
	  mStateSequence.push_back(state | event.type);
	  
	  event.action = eButtonRelease;
	  mEventSequence.push_back(event);
	  mStateSequence.push_back(state);
	  break;
	  
	case eButtonPress:
	  state |= event.type;
	  mEventSequence.push_back(event);
	  mStateSequence.push_back(state);
	  break;
	  
	case eButtonRelease:
	  state &= ~event.type;
	  mEventSequence.push_back(event);
	  mStateSequence.push_back(state);
	  break;
	  
	case eButtonHold:
	  state = event.type;
	  mEventSequence.push_back(event);
	  mStateSequence.push_back(state);
	  break;
	}
    }
}

/** Updates any delay timers in tis combination.
 *
 * @param timeElapsed the amount of time that's passed since the last call.
 */
void cButtonHandler::cButtonCombo::updateTime(double timeElapsed)
{
  if(mDelayTimer > 0.0)
    {
      mDelayTimer -= timeElapsed;
      
      if(mDelayTimer <= 0.0)
	{ // delay complete -- move on to the next step
	  mDelayTimer = 0.0;
	  nextStep();
	}
    }
}


/** Updates the progress of the combination.
 *
 * @param states the button states after the event.
 * @param pressed the buttons that were pressed during the last event.
 * @param released the buttons that were released during the last event.
 */
void cButtonHandler::cButtonCombo::updateState(buttonType_t state,
					       buttonType_t pressed,
					       buttonType_t released )
{
  bool valid = nextStepValid(state, pressed, released);
  if(mDelayTimer == 0.0 && valid)
    { // button entered successfully -- move on to next step
      mDelayTimer = mEventSequence[mSequenceStep].delay;
      if(mDelayTimer == 0.0)
	{ nextStep(); }
    }
  else if(!valid)
    { // button entered incorrectly -- reset progress.
      reset(state, pressed, released);
    }
}

/** Called when the next button event in the sequence has occurred (after delay,
 *   if applicable). Moves to the next step in the sequence, or calls the
 *   callback if the sequence is complete. */
void cButtonHandler::cButtonCombo::nextStep()
{
  if(mDelayTimer == 0.0)
    {
      int nextIndex = mSequenceStep + 1;
      if(nextIndex == mEventSequence.size())
	{ // button combination complete -- call callback
	  reset(BUTTON_NONE, BUTTON_NONE, BUTTON_NONE);
	  mCallback();
	}
      else
	{
	  sButtonEvent nextEvent = mEventSequence[nextIndex];
	  mSequenceStep = nextIndex;
	}
    }
}

/** Resets the combination progress back to the first button, and checks if the
 *   current state fulfills the first combination event.
 *
 * @param states the current button states.
 * @param pressed the buttons that were pressed during the last event.
 * @param released the buttons that were released during the last event.
 */
void cButtonHandler::cButtonCombo::reset(buttonType_t state,
					 buttonType_t pressed,
					 buttonType_t released )
{
  // go back to checking the first event
  mSequenceStep = 0;
  mDelayTimer = 0.0;

  if(nextStepValid(state, pressed, released))
    { // first event in the sequence is already fulfilled
      nextStep();
    }
}



/** Determines whether an event matches the next step in the sequence.
 *
 * @param states the button states after the event.
 * @param pressed the buttons that were pressed during the last event.
 * @param released the buttons that were released during the last event.
 *
 * @return whether the events matched.
 */
bool cButtonHandler::cButtonCombo::nextStepValid(buttonType_t state,
						 buttonType_t pressed,
						 buttonType_t released ) const
{
  sButtonEvent nextEvent = mEventSequence[mSequenceStep];
  buttonType_t nextState = mStateSequence[mSequenceStep];
  
  // unless it's a release event, the first event should match the states
  // exactly.
  if(nextEvent.action != eButtonRelease && mSequenceStep == 0 &&
     nextState != state )
    { return false; }
  
  switch(nextEvent.action)
    {
    case eButtonPress:
      return (nextEvent.type & pressed);
    case eButtonRelease:
      return (nextEvent.type & released);
    case eButtonHold:
      return (nextState == state);
    default:
      return false;
    }
}
