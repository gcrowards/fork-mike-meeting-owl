/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  thread.cpp
 *  This is the implementation file for cThread.
 *   --> High-level wrapper for creating a standard thread with a name and
 *        priority. Uses pthreads internally.
 */

#include "logging.hpp"
#include "thread.hpp"

#include <sys/resource.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <unistd.h>

int cThread::mNumThreads = 0; /**< total number of cThread instances created. */

/** This constructor creates a thread with a custom name and priority.
 *  The priority can be omitted to use the default priority level.
 *
 * @param threadCallback the function to run on a separate thread.
 * @param threadName the name to give the thread.
 * @param threadPriority (optional) the priority to give the thread.
 */
cThread::cThread(threadCallback_t threadCallback, const std::string &threadName,
		 int threadPriority )
  : mCallback(threadCallback), mName(threadName), mPriority(threadPriority)
{
  mNumThreads++;
}

cThread::~cThread()
{
  join();
  mNumThreads--;
}

/** Starts the thread; creates and initializes the internal pthread. */
void cThread::start()
{
  if(!mJoinNeeded)
    {
      // start the thread
      LOGV("THREADING ==> Starting thread '%s'...\n", mName.c_str());
      mRunning = true;
      mJoinNeeded = true;

      // create the pthread
      if(pthread_create(&mThread, nullptr, &cThread::threadWrapper, this) != 0)
	{
	  mRunning = false;
	  mJoinNeeded = false;
	  LOGE("THREADING ==> Unable to create thread (name: '%s')\n",
	       mName.c_str() );
	  return;
	}

      // set the pthread name
      if(pthread_setname_np(mThread, mName.c_str()) != 0)
	{
	  mRunning = false;
	  mJoinNeeded = false;
	  pthread_join(mThread, nullptr);
	  LOGE("THREADING ==> Unable to set thread name '%s'\n",
	       mName.c_str() );
	  return;
	}
    }
}

/** Signals that the thread should stop running, without joining yet.
 *  This can be used to stop looping before joining the thread if additional
 *   cleanup is necessary.
 */
void cThread::signalStop()
{
  mRunning = false;
}

/** Joins the running pthread. */
void cThread::join()
{
  if(mJoinNeeded)
    {
      LOGV("THREADING ==> Joining thread '%s'...\n", mName.c_str());
      mRunning = false;
      pthread_join(mThread, nullptr);
      mJoinNeeded = false;
    }
}

/** @return whether the thread is actively looping and calling the callback. */
bool cThread::isRunning() const
{
  return mRunning;
}

/** @return the name of the thread. */
std::string cThread::getName() const
{
  return mName;
}

/** @return the priority level of the thread. */
int cThread::getPriority() const
{
  return mPriority;
}

/** Static wrapper function, compatible with pthread_create().
 *
 * @param pContext a pointer to the relevant cThread instance.
 * @return a placeholder for function signature compatibility (returns null).
 */
void* cThread::threadWrapper(void *pContext)
{
  if(pContext)
    {
      cThread *pThread = reinterpret_cast<cThread*>(pContext);

      // set the thread priority
      pid_t tid = syscall(SYS_gettid);
      setpriority(PRIO_PROCESS, tid, pThread->mPriority);
      LOGV("THREADING ==> Thread '%s' has id %d and priority %d.\n",
	   pThread->mName.c_str(), tid, getpriority(PRIO_PROCESS, tid));

      if(pThread->mCallback)
	{
	  // call the callback repeatedly while the thread is running.
	  while(pThread->mRunning)
	    { pThread->mCallback(); }
	}
    }
  return nullptr;
}
