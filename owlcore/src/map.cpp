#include "circleMath.hpp"
#include "logging.hpp"
#include "map.hpp"
#include "ringBuffer.hpp"

#include <algorithm>
#include <cmath>

#define ADD_VAL_DEFAULT      1
#define LEAK_VAL_DEFAULT     0
#define MIN_VAL_DEFAULT      0
#define MAX_VAL_DEFAULT   1000
#define SPREAD_DEFAULT       5 // essentially the standard deviation in degrees

#define LONG_TERM_ITEM_SCORE_THRESHOLD 300 // empirically determined

// NOTE: MIN and MAX defaults chosen so map could act as a probability
// density function with three significant digits

cMap::cMap(void)
{
  init(ADD_VAL_DEFAULT, LEAK_VAL_DEFAULT,
       MIN_VAL_DEFAULT, MAX_VAL_DEFAULT,
       SPREAD_DEFAULT);
}

/**
 * @param addVal is the amount to add to map values when a particular bearing
 * is updated.
 * @param leakVal is the amount to add to map value every time an update happens.
 * @param minVal is the smallest value a map location can contain.
 * @param maxVal is the largest value a map location can contain.
 */
cMap::cMap(int addVal, int leakVal, int minVal, int maxVal)
{
  init(addVal, leakVal, minVal, maxVal, SPREAD_DEFAULT);
}

/**
 * @param addVal is the amount to add to map values when a particular bearing
 * is updated.
 * @param leakVal is the amount to add to map value every time an update happens.
 */
cMap::cMap(int addVal, int leakVal)
{
  init(addVal, leakVal, MIN_VAL_DEFAULT, MAX_VAL_DEFAULT, SPREAD_DEFAULT);
}

/** Copy constructor */
cMap::cMap(const cMap &src)
{
  deepCopySelf(src);
}

/** Assignment constructor */
cMap & cMap::operator=(const cMap &src)
{
  if(&src == this)   // watch out for self-assignment
    {
      return *this;
    }
  else
    {
      deepCopySelf(src);
      return *this;
    }
}

/** Helper funcion for making deep copy of cMap.
 *
 * This function is simplifies copy and assingment constructors.
 *
 * @param const cMap is the class instance to copy.
 */
void cMap::deepCopySelf(const cMap &src)
{
  // copy simple members
  this->mMap = src.mMap;
  this->mAddVal = src.mAddVal;
  this->mLeakVal = src.mLeakVal;
  this->mMinVal = src.mMinVal;
  this->mMaxVal = src.mMaxVal;
  this->mSpreadVal = src.mSpreadVal;  

  // deep copy Items Maps
  deepCopyItemMap(&(this->mpInstantItemsMap),
		  (const std::vector<cItem *>)src.mpInstantItemsMap );
  deepCopyItemMap(&(this->mpLongTermItemsMap), 
		  (const std::vector<cItem *>)src.mpLongTermItemsMap );
}

/**
 *  Copy Item Map.
 *
 * @param pDst is a pointer to the destination item map.
 * @param src is the source item map.
 */
void cMap::deepCopyItemMap(std::vector<cItem *> *pDst, 
			   const std::vector<cItem *> src )
{
  cItem *pItem;
  
  for(std::vector<cItem *>::const_iterator iItem = src.begin(); 
      iItem != src.end(); ++iItem )
    {
      pItem = new cItem;
      *pItem = (**iItem);
      
      pDst->push_back(pItem);
    }
}

cMap::~cMap()
{
  clearInstantItemMap();
  clearLongTermItemMap();
}

/** Initialization function called by constructors.
 *
 * @param addVal is the amount to add to map values when a particular bearing
 * is updated.
 * @param leakVal is the amount to add to map value every time an update happens.
 * @param minVal is the smallest value a map location can contain.
 * @param maxVal is the largest value a map location can contain.
 * @param spreadVal is the spread the determines how much nearby
 * bearings are incremented.
 */
void cMap::init(int addVal, int leakVal, int minVal, int maxVal, int spreadVal)
{
  setAddVal(addVal);
  setLeakVal(leakVal);  
  setMinVal(minVal);
  setMaxVal(maxVal);
  setSpreadVal(spreadVal);
  initMap();
}

/** Update the map with new bearings.
 *
 * @param bearings is a vector of integer angles at which the mapper 
 * found things of interest.
 */
void cMap::update(std::vector<int> bearings)
{
  int mapVal, deltaAngle, increment;
  double z;

  // update all map values
  for(int angle = 0; angle < 360; angle++)
    {
      mapVal = mMap[angle];

      // loop through all new bearings to increment map value at current angle
      for(std::vector<int>::iterator iBearing = bearings.begin();
	  iBearing != bearings.end(); ++iBearing )
	{
	  deltaAngle = circleMath::intDiffAngs(angle, (*iBearing));
	  z = (double) deltaAngle / mSpreadVal; // z-score
	  increment = std::exp(- z * z / 2) * mAddVal; // scaled Gaussian
	  mapVal += increment;
	}

      // leak
      mapVal -= mLeakVal;

      // clamp
      if (mapVal < mMinVal)
	{ mapVal = mMinVal; }
      else if (mMaxVal != 0 && mapVal > mMaxVal)
	{ mapVal = mMaxVal; }

      // assign the new value
      mMap[angle] = mapVal;
    }
}

/** Force-update the map with the given items (public facing function).
 *
 * This function is used to assign a specific state to the map, e.g. when 
 * inserting a simulated scenario or playing back a recorded scenario.
 * 
 * @param fauxItems is a vector of cItems that make up the map state to assign.
 */
void cMap::update(std::vector<cItem> fauxItems)
{
  updateItemMap(fauxItems);
}

/** Update estimates of positions/sizes of cItems that the raw map data implies.
 *
 * This algorithm makes some big assumptions in order to simplify the problem
 * of detecting unique peaks (and widths) in the underlying raw data. It assumes
 * the data is not noisy, and that there is zero values in the raw dataset 
 * between the unique peaks.  It is not meant to be a general-purpose peak
 * detector.
 */
void cMap::updateItemMap(void)
{
  const int kItemDegreeSeparation = 1;  // Number of degrees of data silence
                                        // that must be present in order to
                                        // consider two blocks of data to 
                                        // represent two unique items

  const int kMaxItemStdDev = 30;        // Don't count items with standard
                                        // deviations larger than this as real
                                        // items
  // TODO: Make these variables that can be adjusted by the user of the map,
  //       and add a min variable too.  This values is hardcoded for the case
  //       where the Item Map represents people.

  int startIdx = -1;  // deg theta angle to start peak search from
  const int kMapSize = mMap.size();
  const int kMapEnd = kMapSize - 1;
  
  // clear existing data (and free memory)
  clearInstantItemMap();
  
  // find a zero value to start off from (easier to start not-in-a-peak)
  for(std::vector<int>::iterator iVal = mMap.begin(); iVal != mMap.end();
      ++iVal )
    {
      startIdx++;
      if(*iVal == 0) {break;}
    }

  
  // copy map data into an array that starts at start index 
  // (makes things easier because we don't have to worry about wrap-around
  // while finding peaks)
  int aData[kMapSize];       // new data array to work with
  int *pData = mMap.data();  // start of source vector's data

  int *pSrc; int *pDst;
  int numBytes;

  // copy "back" to "front"
  pSrc = pData + startIdx;
  pDst = aData;
  numBytes = sizeof(int) * (kMapSize - startIdx);
  memcpy(pDst, pSrc, numBytes);

  // copy "front" to "back"
  pSrc = pData;
  pDst = aData + (numBytes / sizeof(int));
  numBytes = (sizeof(int) * kMapSize) - numBytes;
  memcpy(pDst, pSrc, numBytes);
  
  // find mean and standard deviation to represent each peak
  cRingBuffer<int> sumBuffer(kItemDegreeSeparation);
  bool inPeak = false;
  float mean, stdDev = 0;
  int count = 0;
  
  for(int theta = 0; theta < kMapSize; theta++)
    {    
      int val = aData[theta];

      sumBuffer.addElement(val); // Used to find sustained dead space between
                                 // unique groups of data
      
      if(inPeak) // look for peak end
	{
	  if(sumBuffer.calcSum() == 0 || (theta == kMapEnd)) // found end
	  {
	      // finalize mean and standard deviaiton & store in cItem
	      mean = (float)circleMath::intAddAngs((int)mean, startIdx);
	      stdDev = std::sqrt(stdDev / (count - 1));
	      
	      // if the item isn't too wide, store it in the Item Map
	      if(stdDev < kMaxItemStdDev)	     
		{
		  mpInstantItemsMap.push_back(new cItem((int)mean, stdDev));
		}

	      // reset to look for next peak
	      mean = stdDev = count = 0;
	      inPeak = false;	      
	    }
	  else // within peak (work on finding mean & standard deviation)
	    {
	      // update mean and stdDev
	      int freq = val;              // change name for clarity below
	      count += freq;
	      
	      int weightedTheta = freq * theta;
	      float tmpMean = mean;

	      if(count > 0) // shouldn't happen, but safety first
		{
		  mean += ((theta - tmpMean) * freq) / count;
		  stdDev += ((theta - tmpMean) * (theta - mean)) * freq;
		}
	      else
		{
		  LOGW("updateItemMap(): Avoided unexpected divide by zero!\n");
		}
	    }
	}
      else if(val > 0) // look for rising edge of potential new peak
	{
	  inPeak = true;

	  // reset theta so that this sample also gets included on next loop 
	  // cycle, where the peak's characteristics start getting calucated
	  theta--;
	}
    }

  // now update the filtered (for temporal stability) long-term item map
  // (Note: this must be done *after* the instantaneous map is updated)
  updateLongTermItemMap();
}

/** Initialize maps.
 *
 * The map contains an integer value for each integral angle (deg) in the 
 * circle.  It's initialized to zero, which means that no person is located
 * at each angle.
 */
void cMap::initMap(void)
{
  for(int angle = 0; angle < 360; angle++)
    {
      mMap.push_back(mMinVal);
    }

  mpInstantItemsMap.clear();
  mpLongTermItemsMap.clear();
}

/** Delete all the cItems in the instant-item map. */
void cMap::clearInstantItemMap(void)
{
  for(std::vector<cItem *>::iterator iItem = mpInstantItemsMap.begin(); 
      iItem != mpInstantItemsMap.end(); ++iItem )
    {
      delete *iItem;
    }
  
  mpInstantItemsMap.clear();
}

/** Delete all the cItems in the long-term item map. */
void cMap::clearLongTermItemMap(void)
{
  for(std::vector<cItem *>::iterator iItem = mpLongTermItemsMap.begin(); 
      iItem != mpLongTermItemsMap.end(); ++iItem )
    {
      delete *iItem;
    }
  
  mpLongTermItemsMap.clear();
}

/** Update long-term map using latest instantaneous instant-map data.
 *
 * This function looks through the instant-map items and those that look like
 * ones that are already part of the long-term map (positionally similar) have
 * their characteristics (mean & standard deviation) merged into the item on
 * the long-term map (through a low-pass filter).  Instant items that seem
 * unique are added straight into the long-term map.  Each time an instant
 * item matches a long-term item, that long-term item's strength score is 
 * increased.  Each cycle all long-term item's strengths leak.  When a user
 * asks for the long-term map, only items w/ sufficient scores are returned. 
 */
void cMap::updateLongTermItemMap(void)
{
  const int kMaxMergableDeltaTheta = 20; // +/- degress

  const int kScoreFill = 4;              // empirically determined
  const int kScoreLeak = 1;              // empirically determined
  const int kMaxScore = 1500;            // empirically determined
  const int kMinScore = -180;            // empirically determined

  // loop through all the instant items
  for(std::vector<cItem *>::iterator iItem = mpInstantItemsMap.begin();
      iItem != mpInstantItemsMap.end(); ++iItem )
    {
      int deltaTheta = kMaxMergableDeltaTheta;  // angular distance between items 
      cItem *pInstantItem = *iItem;             // convenience variable
      cItem *pMatchItem;                        // long-term item cloest to 
                                                // instant item

      // get long-term map item who's position is most similar to this item's
      pMatchItem = getItemAt(mpLongTermItemsMap, pInstantItem->getThetaMean());

      if(pMatchItem != nullptr)
	{ // find angular distance between items
	  deltaTheta = circleMath::intDiffAngs(pInstantItem->getThetaMean(), 
					       pMatchItem->getThetaMean() );
	  deltaTheta = std::abs(deltaTheta);
	}

      // see if closest matching item is angularly close enough to assume that 
      // it's the same item that's already in the long-term map
      if(deltaTheta < kMaxMergableDeltaTheta)
	{ // same item: merge item into existing long-term map
	  if(pMatchItem->getScore() < kMaxScore)
	    {
	      pMatchItem->addToScore(kScoreFill);
	    }

	  pMatchItem->filterInThetaMean(pInstantItem->getThetaMean());
	  pMatchItem->filterInThetaStdDev(pInstantItem->getThetaStdDev());
	}
      else
	{ // add new item to long-term map
	  // NOTE: Adding to the long-term map here means that newly added items
	  //       are tested against as the loop continues to look at the rest
	  //       of the instantaneous items.  Think this is okay, but it's 
	  //       worth being aware of.
	  cItem *pLongTermItem;
	  pLongTermItem = new cItem(*pInstantItem);
	  pLongTermItem->setId(); // give item a unique ID

	  mpLongTermItemsMap.push_back(pLongTermItem);
	}
    }

  // leak scores on all long-term items, and delete items that are very weak
  for(std::vector<cItem *>::iterator 
	iLongTermItem = mpLongTermItemsMap.begin();
      iLongTermItem != mpLongTermItemsMap.end(); )
    { 
      // leak score of all items in long-term map
      if((*iLongTermItem)->getScore() > kMinScore)
	{
	  (*iLongTermItem)->addToScore(-kScoreLeak);
	}
      
      // delete items that are maximally weak
      if((*iLongTermItem)->getScore() <= kMinScore)
	{
	  // free memory
	  delete *iLongTermItem;
	  // remove from vector (which increments the iterator)
	  mpLongTermItemsMap.erase(iLongTermItem);
	}
      else
	{ iLongTermItem++; }
    }
}

/** Force-update the instant and long-term map with the given items 
 * (private function).
 *
 * This function is used to assign a specific state to the map, e.g. when 
 * inserting a simulated scenario or playing back a recorded scenario.
 * 
 * @param fauxItems is a vector of cItems that make up the map state to assign.
 */
void cMap::updateItemMap(std::vector<cItem> fauxItems)
{
  // clear existing data (and free memory)
  clearInstantItemMap();

  // put all items on the instant map
  for(std::vector<cItem>::iterator iItem = fauxItems.begin(); 
      iItem != fauxItems.end(); ++iItem )
    {
      mpInstantItemsMap.push_back(new cItem((*iItem).getThetaMean(), 
					    (*iItem).getThetaStdDev()));
    }    

  // update long-term map
  updateLongTermItemMap();  
}

/** Find the cItem in the given map that has a mean angle that is closest to the
 * given angle.
 *
 * Note: If no match is found null pointer is returned.
 *
 * @param itemMap is a vector of cItems to consider.
 * @param angle is the direction to match to items in the map [0, 360) deg.
 * @param threshold is the minimum item strength threshold required to consider
 * an item on the map to be a real item.
 *
 * @return a pointer to the cItem that is closest to the given angle.
 */
cItem * cMap::getItemAt(std::vector<cItem *> itemMap, int angle, int threshold)
{
  cItem *pBestItem = nullptr;
  int bestDelta = 361; // max map value is 360 degrees
 
  for(std::vector<cItem *>::iterator iItem = itemMap.begin(); 
      iItem != itemMap.end(); ++iItem )
    {
      bool validItem = true;
      int delta = std::abs(circleMath::intDiffAngs((*iItem)->getThetaMean(), 
						   angle ) );      
      if(threshold != INT_MIN)
	{
	  if((*iItem)->getScore() < threshold)
	    { validItem = false; }
	}

      if(validItem && (delta < bestDelta))
	{
	  bestDelta = delta;
	  pBestItem = (*iItem);
	}
    }

  return pBestItem;
}

/** Return the value at the provided angle (deg).
 *
 * @param angle (deg) is the bearing of interest.
 */ 
int cMap::getValAt(int angle)
{
  return mMap.at(angle);
}

/** Determine the maximum map value near the given angle within the given range.
 *
 * @param angle (deg) is the nominal bearing around which to search for the 
 * maximum map value.
 * @param plusMinus (deg) is the range around the nominal bearing to search in.
 */
int cMap::getAngleWithMaxVal(int angle, int plusMinus)
{
  int angleMinus = angle - plusMinus;
  int anglePlus = angle + plusMinus;

  int maxVal = -1;
  int maxValAngle;

  for(int a = angleMinus; a <= anglePlus; a++)
    {
      angle = a;

      // correct 0/360 wrap-around
      if(angle < 0) {angle += 360;}
      if(angle >= 360) {angle -= 360;}
     
      if(mMap.at(angle) > maxVal)
	{
	  maxVal = mMap.at(angle);
	  maxValAngle = angle;
	}
    }

  return maxValAngle;
}

/** Get the current map data.
 *
 * @return the current map which is a vector of integers.
 */
std::vector<int> cMap::getMapData(void)
{
  return mMap;
}

/** @return the largest value a map location can contain. */
int cMap::getMaxVal(void)
{
  return mMaxVal;
}

/** @return the single-cycle map in the form of a list of distinct Items. */
const std::vector<const cItem *> cMap::getInstantItemMap(void)
{
  std::vector<const cItem *> constInstantItemMap;

  // repackage w/ pointers that can't be used to modify underlying data
  for(std::vector<cItem *>::iterator iItem = mpInstantItemsMap.begin();
      iItem != mpInstantItemsMap.end(); ++iItem )
    {
      constInstantItemMap.push_back((const cItem *)*iItem);
    }

  return (const std::vector<const cItem *>)constInstantItemMap;
}

/** @return the long-term map in the form of a list of distinct items. */
const std::vector<const cItem *> cMap::getLongTermItemMap(void)
{
  std::vector<const cItem *> filteredMap;

  // repackage w/ pointers that can't be used to modify underlying data
  for(std::vector<cItem *>::iterator iItem = mpLongTermItemsMap.begin();
      iItem != mpLongTermItemsMap.end(); ++iItem )
    {
      if((*iItem)->getScore() > LONG_TERM_ITEM_SCORE_THRESHOLD)
	{ // only include items w/ adequate strength scores
	  filteredMap.push_back((const cItem *)*iItem);
	}
    }

  return (const std::vector<const cItem *>)filteredMap;
}

/** Find the cItem in the instant item map (has no memory of past data) that has 
 * a mean angle that is closest to the given angle.
 *
 * Note: If no match is found null pointer is returned.
 *
 * @param int angle is the direction to match to items in the map [0, 360) deg.
 *
 * @return a pointer to the cItem that is closest to the given angle.
 */
cItem * cMap::getInstantItemAt(int angle)
{
  return getItemAt(mpInstantItemsMap, angle);
}

/** Find the cItem in the long-term item map (low-pass filetred) that has a mean 
 * angle that is closest to the given angle.
 *
 * Note: If no match is found null pointer is returned.
 *
 * @param int angle is the direction to match to items in the map [0, 360) deg.
 *
 * @return a pointer to the cItem that is closest to the given angle.
 */
cItem * cMap::getLongTermItemAt(int angle)
{
  return getItemAt(mpLongTermItemsMap, angle, LONG_TERM_ITEM_SCORE_THRESHOLD);
}

/** Reset all of the map values to zero. */
void cMap::resetMap(void)
{
  std::fill(mMap.begin(), mMap.end(), mMinVal);

  clearInstantItemMap();
  clearLongTermItemMap();
}

/** Prints out map values on console.
 *
 * @param stepSize is an integer that specifies the resolution of the output
 * (e.g. a value of 10 will cause the value to be printed at 10 degree
 * intervals).  Default step size is 10 degrees.
 */ 
void cMap::printMap(int stepSize)
{
  for(int angle = 0; angle < 360; angle += stepSize)
    {
      printf("Map | %d deg: %d\n", angle, getValAt(angle));
    }
}

/** Logs a map to a file, if LOG_TO_FILE is defined.
 *
 * @param pLabel is a string that labels the map data in the log.
 * @param stepSize is an integer that specifies the resolution of the output
 * (e.g. a value of 10 will cause the value to be printed at 10 degree
 * intervals).  Default step size is 10 degrees.
 */ 
void cMap::logMap(const char *pLabel, int stepSize)
{
  LOGF("\n");
  for(int angle = 0; angle < 360; angle += stepSize)
    {
      LOGF("%s | %3d deg: %8d\n", pLabel, angle, getValAt(angle));
    }
  LOGF("\n");
}

/** Set the amount that is added to map values when something is detected.
 *
 * @param addVal is the amount that will be added to a map location when 
 * something is detected at that location.
 */
void cMap::setAddVal(int addVal)
{
  mAddVal = addVal;
}

/** Set the amount that is subtracted from all map values on each up date cycle.
 *
 * @param leakVal is the amount that will be subtracted from each map location
 * on each update call to the map.
 */
void cMap::setLeakVal(int leakVal)
{
  mLeakVal = leakVal;
}

/** Set the minimum value a map location can hold.
 *
 * @param minVal is the smallest value a map location can contain.
 */
void cMap::setMinVal(int minVal)
{
  mMinVal = minVal;
}

/** Set the maximum value a map location can hold.
 *
 * @param maxVal is the largest value a map location can contain.
 */
void cMap::setMaxVal(int maxVal)
{
  mMaxVal = maxVal;
}

/** Set the spread value.
 *
 * @param spreadVal determines how much nearby bearings are incremented.
 */
void cMap::setSpreadVal(int spreadVal)
{
  mSpreadVal = spreadVal;
}

/** Set the value at a specific map location.
 *
 * @param angle is the location in the map vector.
 * @param val is the new value to write at given angle.
 */
void cMap::setValAt(int angle, int val)
{
  mMap[angle] = val;
}
