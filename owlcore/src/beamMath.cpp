#include <cfloat>
#include <cmath>
#include <stdint.h>
#include <string.h>

#include "beamMath.hpp"
#include "logging.hpp"
#include "params.h"

namespace beamMath
{
  // ------------------------------------------
  // start of a close port from code by CC @ PG
  // ------------------------------------------

#define AMBIENT_TEMP     (20)                                    // degrees C
#define SPEED_OF_SOUND   (331.3 + 0.606 * AMBIENT_TEMP)          // m/s
#define SAMPLE_RATE      (48000.0)                               // Hz
#define SAMPLE_SIZE      (1000.0 * SPEED_OF_SOUND / SAMPLE_RATE) // mm

typedef struct micPosition_s {
  double  x;              // X Coordinate of the mic
  double  y;              // Y Coordinate of the mic
  double  z;              // Z Coordinate of the mic
  double  l;              // Length of port extension
} micPosition_t;

// set position vectors for each mic (in mm)
#ifdef PROTOTYPE_P0_5
micPosition_t gMicPositions[] = {
  {  0.000,   0.000,  14.500, 0.0}, // Chan/Mic 0 not used
  {  0.000,   0.000,  14.500, 0.0}, // Chan/Mic 1 not used
  {  0.000,   0.000,  14.500, 0.0}, // Chan/Mic 2 not used
  {  0.000,   0.000,  14.500, 0.0}, // Chan/Mic 3 not used
  {  0.000, -40.500,  14.500, 0.0}, // Chan/Mic 4 at 270 degrees
  {-40.500,   0.000,  14.500, 0.0}, // Chan/Mic 5 at 180 degrees
  {  0.000,  40.500,  14.500, 0.0}, // Chan/Mic 6 at  90 degrees
  { 40.500,   0.000,  14.500, 0.0}, // Chan/Mic 7 at   0 degrees
};
#else // P1 PROTOTYPE
micPosition_t gMicPositions[] = {
  {-34.033, -34.033, -56.500, 0.0}, // Chan/Mic 0 at 225 degrees
  {-34.033,  34.033, -56.500, 0.0}, // Chan/Mic 1 at 135 degrees
  { 34.033,  34.033, -56.500, 0.0}, // Chan/Mic 2 at  45 degrees
  { 34.033, -34.033, -56.500, 0.0}, // Chan/Mic 3 at 315 degrees
  {-48.130,   0.000, -86.400, 0.0}, // Chan/Mic 4 at 180 degrees
  {  0.000,  48.130, -86.400, 0.0}, // Chan/Mic 5 at  90 degrees
  {  0.000, -48.130, -86.400, 0.0}, // Chan/Mic 6 at 270 degrees
  { 48.130,   0.000, -86.400, 0.0}, // Chan/Mic 7 at   0 degrees
};
#endif // PROTOTYPE P1 or P0.5

// Set direction vectors for each beam. Currently, the
// #if..#else..#endif is used to select the upward tilt of the vectors
// (presumably toward people's faces).
// TODO: Compute these at run time instead, with a variable to
// determine the amount that the vectors are tilted.
#if 1
  /** gBeamVectors is a list of unit vectors that point in the
   *  direction of the expected source from the mic origin.
   *  These values are based on a 10 degree up angle from
   *  horizontal and N_BEAMS equally spaced radial beams. */
  micPosition_t gBeamVectors[N_BEAMS] = {
    { 0.985,  0.000,  0.174, 0.0}, // beam  0 at   0.000 degrees
    { 0.966,  0.192,  0.174, 0.0}, // beam  1 at  11.250 degrees
    { 0.910,  0.377,  0.174, 0.0}, // beam  2 at  22.500 degrees
    { 0.819,  0.547,  0.174, 0.0}, // beam  3 at  33.750 degrees
    { 0.696,  0.696,  0.174, 0.0}, // beam  4 at  45.000 degrees
    { 0.547,  0.819,  0.174, 0.0}, // beam  5 at  56.250 degrees
    { 0.377,  0.910,  0.174, 0.0}, // beam  6 at  67.500 degrees
    { 0.192,  0.966,  0.174, 0.0}, // beam  7 at  78.750 degrees
    { 0.000,  0.985,  0.174, 0.0}, // beam  8 at  90.000 degrees
    {-0.192,  0.966,  0.174, 0.0}, // beam  9 at 101.250 degrees
    {-0.377,  0.910,  0.174, 0.0}, // beam 10 at 112.500 degrees
    {-0.547,  0.819,  0.174, 0.0}, // beam 11 at 123.750 degrees
    {-0.696,  0.696,  0.174, 0.0}, // beam 12 at 135.000 degrees
    {-0.819,  0.547,  0.174, 0.0}, // beam 13 at 146.250 degrees
    {-0.910,  0.377,  0.174, 0.0}, // beam 14 at 157.500 degrees
    {-0.966,  0.192,  0.174, 0.0}, // beam 15 at 168.750 degrees
    {-0.985,  0.000,  0.174, 0.0}, // beam 16 at 180.000 degrees
    {-0.966, -0.192,  0.174, 0.0}, // beam 17 at 191.250 degrees
    {-0.910, -0.377,  0.174, 0.0}, // beam 18 at 202.500 degrees
    {-0.819, -0.547,  0.174, 0.0}, // beam 19 at 213.750 degrees
    {-0.696, -0.696,  0.174, 0.0}, // beam 20 at 225.000 degrees
    {-0.547, -0.819,  0.174, 0.0}, // beam 21 at 236.250 degrees
    {-0.377, -0.910,  0.174, 0.0}, // beam 22 at 247.500 degrees
    {-0.192, -0.966,  0.174, 0.0}, // beam 23 at 258.750 degrees
    { 0.000, -0.985,  0.174, 0.0}, // beam 24 at 270.000 degrees
    { 0.192, -0.966,  0.174, 0.0}, // beam 25 at 281.250 degrees
    { 0.377, -0.910,  0.174, 0.0}, // beam 26 at 292.500 degrees
    { 0.547, -0.819,  0.174, 0.0}, // beam 27 at 303.750 degrees
    { 0.696, -0.696,  0.174, 0.0}, // beam 28 at 315.000 degrees
    { 0.819, -0.547,  0.174, 0.0}, // beam 29 at 326.250 degrees
    { 0.910, -0.377,  0.174, 0.0}, // beam 30 at 337.500 degrees
    { 0.966, -0.192,  0.174, 0.0}, // beam 31 at 348.750 degrees
  };
#else
  /** beamVector is a list of unit vectors that point in the
   *  direction of the expected source from the mic origin.
   *  These values are based on a 0 degree up angle from
   *  horizontal and N_BEAMS equally spaced radial beams. */
  micPosition_t gBeamVectors[N_BEAMS] = {
    { 1.000,  0.000,  0.000, 0.0}, // beam  0 at   0.000 degrees
    { 0.981,  0.195,  0.000, 0.0}, // beam  1 at  11.250 degrees
    { 0.924,  0.383,  0.000, 0.0}, // beam  2 at  22.500 degrees
    { 0.831,  0.556,  0.000, 0.0}, // beam  3 at  33.750 degrees
    { 0.707,  0.707,  0.000, 0.0}, // beam  4 at  45.000 degrees
    { 0.556,  0.831,  0.000, 0.0}, // beam  5 at  56.250 degrees
    { 0.383,  0.924,  0.000, 0.0}, // beam  6 at  67.500 degrees
    { 0.195,  0.981,  0.000, 0.0}, // beam  7 at  78.750 degrees
    { 0.000,  1.000,  0.000, 0.0}, // beam  8 at  90.000 degrees
    {-0.195,  0.981,  0.000, 0.0}, // beam  9 at 101.250 degrees
    {-0.383,  0.924,  0.000, 0.0}, // beam 10 at 112.500 degrees
    {-0.556,  0.831,  0.000, 0.0}, // beam 11 at 123.750 degrees
    {-0.707,  0.707,  0.000, 0.0}, // beam 12 at 135.000 degrees
    {-0.831,  0.556,  0.000, 0.0}, // beam 13 at 146.250 degrees
    {-0.924,  0.383,  0.000, 0.0}, // beam 14 at 157.500 degrees
    {-0.981,  0.195,  0.000, 0.0}, // beam 15 at 168.750 degrees
    {-1.000,  0.000,  0.000, 0.0}, // beam 16 at 180.000 degrees
    {-0.981, -0.195,  0.000, 0.0}, // beam 17 at 191.250 degrees
    {-0.924, -0.383,  0.000, 0.0}, // beam 18 at 202.500 degrees
    {-0.831, -0.556,  0.000, 0.0}, // beam 19 at 213.750 degrees
    {-0.707, -0.707,  0.000, 0.0}, // beam 20 at 225.000 degrees
    {-0.556, -0.831,  0.000, 0.0}, // beam 21 at 236.250 degrees
    {-0.383, -0.924,  0.000, 0.0}, // beam 22 at 247.500 degrees
    {-0.195, -0.981,  0.000, 0.0}, // beam 23 at 258.750 degrees
    { 0.000, -1.000,  0.000, 0.0}, // beam 24 at 270.000 degrees
    { 0.195, -0.981,  0.000, 0.0}, // beam 25 at 281.250 degrees
    { 0.383, -0.924,  0.000, 0.0}, // beam 26 at 292.500 degrees
    { 0.556, -0.831,  0.000, 0.0}, // beam 27 at 303.750 degrees
    { 0.707, -0.707,  0.000, 0.0}, // beam 28 at 315.000 degrees
    { 0.831, -0.556,  0.000, 0.0}, // beam 29 at 326.250 degrees
    { 0.924, -0.383,  0.000, 0.0}, // beam 30 at 337.500 degrees
    { 0.981, -0.195,  0.000, 0.0}, // beam 31 at 348.750 degrees
  };
#endif

  /** gBeamDelayDbls is a 2 dimensional array of the fractional
   *  delays per mic per beam */
  double gBeamDelayDbls[N_BEAMS][N_MICS];

  /** gBeamDelayInts is a 2 dimensional array of the fractional
   *  delays per mic per beam */
  int gBeamDelayInts[N_BEAMS][N_MICS];

  double dotProduct(micPosition_t* pt1, micPosition_t* pt2)
  {
    double result;
    result = pt1->x * pt2->x;
    result += pt1->y * pt2->y;
    result += pt1->z * pt2->z;
    return result;
  }

  void normalizeToOrigin(micPosition_t* origin, micPosition_t* positions, int posCnt)
  {
    int i;

    /* assume the origin is at 0,0,0 */
    origin->x = 0;
    origin->y = 0;
    origin->z = 0;

    /* calculate the origin by averaging all x,y,z coordinates */
    for (i = 0; i < posCnt; i++)
      {
	origin->x += positions[i].x;
	origin->y += positions[i].y;
	origin->z += positions[i].z;
      }
    origin->x /= posCnt;
    origin->y /= posCnt;
    origin->z /= posCnt;

    // normalize/offset so all are relative to the origin
    for (i = 0; i < posCnt; i++)
      {
	positions[i].x -= origin->x;
	positions[i].y -= origin->y;
	positions[i].z -= origin->z;
      }
  }

  void calcBeamDelays(void)
  {
    int mic,beam;
    double minDelay = DBL_MAX;
    micPosition_t micOrigin;

    // set the origin and make mic positions relative to it
    normalizeToOrigin(&micOrigin, gMicPositions, N_MICS);

    // beam delay per beam per mic in mm including port length correction
    for (beam = 0; beam < N_BEAMS; beam++)
      {
	for (mic = 0; mic < N_MICS; mic++)
	  {
	    gBeamDelayDbls[beam][mic] = dotProduct( &gBeamVectors[beam],
						    &gMicPositions[mic] )
	      + gMicPositions[mic].l;
	    if (gBeamDelayDbls[beam][mic] < minDelay)
	      { minDelay = gBeamDelayDbls[beam][mic]; }
	  }
      }

    // Normalize so all delays are positive and are in samples
    for (beam = 0; beam < N_BEAMS; beam++)
      {
	for (mic = 0; mic < N_MICS; mic++)
	  {
	    gBeamDelayDbls[beam][mic] -= minDelay;
	    gBeamDelayDbls[beam][mic] /= SAMPLE_SIZE;
	    gBeamDelayInts[beam][mic] = gBeamDelayDbls[beam][mic] + 0.5;
	  }
      }
  }


  // --------------------
  // start of non-PG code
  // --------------------

  /** This function computes the beam delays for the microphone array.
   *
   * @param pBeamDelays points to an N_BEAMS x N_MICS array of integer
   * delay values in units of scaled samples.
   * @param scale is a scale factor for the delay values; normally it
   * is either 1 or a number greater than 1 to give sub-sample
   * resolution.
   */
  void getBeamDelays(int pBeamDelays[][N_MICS], int scale)
  {
    // call the PG routine that calculates beam delays
    calcBeamDelays();
    // copy the results, scaling and rounding to the nearest integer
    for (int b = 0; b < N_BEAMS; b++)
      {
	for (int m = 0; m < N_MICS; m++)
	  {
	    pBeamDelays[b][m] = 0.5 + scale * gBeamDelayDbls[b][m];
	  }
      }
  }
  
  /** This function simulates a pure tone played on each microphone.
   *
   * The intention is that this function will be used for debugging
   * purposes, to overwrite the real data with what we expect from a
   * single, possibly moving, sound source.
   *
   * @param pPcmIn is a pointer to the PCM sound data transferred via
   * the owlUpdate callback in main.cpp.
   * @param bufLen is the data buffer length.
   * @param beam is the direction of the sound source in [0, N_BEAMS),
   * corresponding to [0, 360); pass a value outside this range
   * (usually a negative value) for silence.
   */
  void simulatePcmData(int16_t *pPcmIn, unsigned int bufLen, int beam)
  {
    int i, j, m;
    int nSamplesPerMic = bufLen / N_MICS;
    const double kSampleRateHz = 48000;     // UAC rate
    const double kGain         =  2000;     // amplitude of simulated tone
    const double kToneFreqHz   =   800;     // freq of simulated tone
    const double kSamplesPerCycle = kSampleRateHz / kToneFreqHz;
    const double kSamplesToRad    = 2.0 * M_PI / kSamplesPerCycle;
    const double kBeamToDeg = 360. / N_BEAMS;
    static unsigned long commonPhase = 0;   // phase of simulated tone
    static int micDelay[N_MICS];            // offsets from common phase
    static int beamDelays[N_BEAMS][N_MICS]; // expected offsets for each beam
    static int oldBeam = -1;
    static bool silence = true;
    static bool init = true;

    // initialization on first call
    if (init)
      {
	// get a copy of beam delays table
	getBeamDelays(beamDelays, 1);
	init = false;
      }

    // set mic delays for current direction (beam)
    if (beam != oldBeam)
      {
	if (beam >=0 && beam < N_BEAMS)
	  {
	    silence = false;
	    for (m = 0; m < N_MICS; m++)
	      { micDelay[m] = beamDelays[beam][m]; }
	    LOGI("SIMULATED SOUND SOURCE AT %4.1f deg\n", kBeamToDeg * beam);
	  }
	else
	  {
	    silence = true;
	    LOGI("SIMULATED SOUND SOURCE QUIET\n");
	  }
	oldBeam = beam;
      }

    if (silence)
      { memset( pPcmIn, 0, bufLen * sizeof(int16_t) ); }
    else
      {
	// generate a sine wave for each mic, with the appropriate phase delay
	// TODO: consider turning all the floating point calculations to an
	// integer lookup table
	j = 0; // index of samples in the interleaved PCM buffer
	for (i = 0; i < nSamplesPerMic; i++)
	  {
	    for (m = 0; m < N_MICS; m++)
	      {
		pPcmIn[j++] = 0.5 +
		  kGain * sin( kSamplesToRad * (commonPhase + micDelay[m]) );
	      }
	    commonPhase++;
	  }
      }
  }

} // namespace beamMath
