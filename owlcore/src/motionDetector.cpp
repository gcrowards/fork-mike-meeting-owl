#include "motionDetector.hpp"
#include "common.hpp"
#include "params.h"

#include "opencv2/imgproc/imgproc.hpp"

#define MOTION_PIXEL_CHANGE_THRESHOLD_PCT     0.02f /**< this sets bar for
						       motion/no motion. */
#define MOTION_DIFF_IMAGE_THRESHOLD_GRAY_VAL 25 /**< this zeros out differences
						   below the threshold. */
#define MAX_PIXEL_VAL               255

cMotionDetector::cMotionDetector(void)
{
  init();
}

/**
 * @param testPeriod is time (s) to wait between tests for motion.
 */
cMotionDetector::cMotionDetector(double testPeriod)
{
  init();
  setTestPeriod(testPeriod);
}

/**
 * @param testPeriod is time (s) to wait between tests for motion.
 * @param roi is a rectangle that specifies a region within the image to test
 * for motion.
 */
cMotionDetector::cMotionDetector(double testPeriod, cv::Rect roi)
{
  init();
  setTestPeriod(testPeriod);
  setRoi(roi);
}

cMotionDetector::~cMotionDetector()
{
}

/** Initialization code called by constructors. */
void cMotionDetector::init(void)
{
  mBufferPrimed = false;
  mPrevUpdateTime.tv_sec = -1.;
  setTestPeriod(-1.);
  setRoi(cv::Rect(0, 0, 0 ,0));
  mRoiIsSet = false;
  mFullImageRoi = false;
  mMaxPixelVal = MAX_PIXEL_VAL;  // TODO: Make this user settable
  mScore = 0.;
  mState = false;
}

/** This is the main function to be called externally to update the detector.
 *
 * If enough time has passed then a new image is stored in the ring buffer and
 * a test for motion is preformed.
 * 
 * @param pImage is a pointer to an new candiate image to test for motion 
 * against.
 */
bool cMotionDetector::update(cv::Mat *pImage)
{
  if(!mRoiIsSet)
    {
      // set ROI to be full image
      setRoi(cv::Rect(0, 0, pImage->cols, pImage->rows));
      mFullImageRoi = true;
    }

  if(!mBufferPrimed)
    {
      // store "base" image in ring buffer
      mImageBuffer.addElement(prepImage(pImage));       
      mBufferPrimed = true;
    }
      
  // don't compare frames too quickly:
  if(!isTimeToUpdate())
    {return false;}
  //
  // DANGER: I'm suspicious that the update rate limiter is not really working 
  //         right.

  // store new image & test for motion
  mImageBuffer.addElement(prepImage(pImage));

  calcMotionVal();
  calcDetectorScore();
  updateDetectorState();
  
  // return detector state
  return mState;
}

/** Sets the time to wait between frames before testing for motion.
 *
 * @param testPeriod time (s) to wait between frams before testing for motion.
 */
void cMotionDetector::setTestPeriod(double testPeriod)
{
  mTestPeriod = testPeriod;
}

/** Sets the region-of-interest to test for motion within.
 *
 * WARNING: If this is changed while the detector is running the following 
 *          motion test will be unreliable (most likely showing motion 
 *          regardless of what's going on).
 *
 * @param roi is the ROI to test for motion within.
 */
void cMotionDetector::setRoi(cv::Rect roi)
{
  mRoi = roi;
  mMaxImageVal = mRoi.width * mRoi.height * mMaxPixelVal;
  
  if(mMaxImageVal > 0) {mRoiIsSet = true;}

  // WARNING: If this is called externally and the call provides an ROI that
  //          is equal to the size of the image then motionDetector will *not*
  //          mark the ROI as mFullImageRoi = true.  Nothing will break, but
  //          there will be an unnecessary crop in prepImage().
}


/** @return the current region-of-interest value. */
cv::Rect cMotionDetector::getRoi(void)
{
  return mRoi;
}

/** @return a bool representing the current motion detector's state. */
bool cMotionDetector::getState(void)
{
  return mState;
}

/** Tests to see if enough time has passed since the last time the detector
 * tested for motion.
 *
 * @return true if enough time has passed since the last time the detector ran.
 */
bool cMotionDetector::isTimeToUpdate(void)
{
  struct timespec currentTime;
  clock_gettime(CLOCK_MONOTONIC, &currentTime);
  
  if(mPrevUpdateTime.tv_sec < 0)
    { // init
      clock_gettime(CLOCK_MONOTONIC, &mPrevUpdateTime);
    }
  else if(elapsedTimeSec(mPrevUpdateTime, currentTime) > mTestPeriod)
    { // test time reached
      mPrevUpdateTime = currentTime;
      return true;
    }
  
  return false;
}

/** This function prepares the input image for motion testing.
 *
 * Before testing for motion the input image needs to be changed to grayscale
 * and a little blur is applied (so which helps keep pixel noise from causing
 * trouble.
 *
 * @param pImage is the image that is to be prepared for the detector.
 *
 * @return the prepared imaged (which is a copy of the original image w/ filters
 * applied).
 */
cv::Mat cMotionDetector::prepImage(cv::Mat *pImage)
{
  const int kKernelSize = 5;
  cv::Mat preppedImage;

  if(!mFullImageRoi)
    {  // crop ROI if it's smaller than the image
      preppedImage = cv::Mat(pImage->clone(), mRoi);
    }
  else
    { // otherwise prep full image
      preppedImage = pImage->clone();
    }

  // make image grayscale, if not already
  if(preppedImage.channels() > 1)
    {cv::cvtColor(preppedImage, preppedImage, cv::COLOR_BGR2GRAY);}
  
  // add a bit of blur
  cv::blur(preppedImage, preppedImage, cv::Size(kKernelSize, kKernelSize));
  
  return preppedImage;
}

/** This function calculates the motion value, which is a measure of how 
 * different two images are.
 */
void cMotionDetector::calcMotionVal(void)
{
  cv::Mat *pCurrentImage = mImageBuffer.getElement();
  cv::Mat *pPastImage = mImageBuffer.getElement(1);  // 1 frame in the past

  // compute the frame difference and zero out values below a threshold
  cv::Mat diffImage;
  cv::absdiff(*pPastImage, *pCurrentImage, diffImage);
  cv::threshold(diffImage, diffImage, MOTION_DIFF_IMAGE_THRESHOLD_GRAY_VAL,
		0, cv::THRESH_TOZERO);

  mMotionVal = cv::sum(diffImage)[0];
}

/** This function calculates the motion score of the detector, which is a 
 * normalized metric of how different two images are.
 */
void cMotionDetector::calcDetectorScore(void)
{
  mScore = ((float)mMotionVal) / mMaxImageVal;
}

/** This function makes the final call on whether or not the detector is
 * tripped or not.
 */
void cMotionDetector::updateDetectorState(void)
{
  if(mScore > MOTION_PIXEL_CHANGE_THRESHOLD_PCT)
    {mState = true;}
  else
    {mState = false;}
}
