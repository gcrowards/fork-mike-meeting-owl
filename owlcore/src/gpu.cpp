#include <stdio.h>
#include <string.h>

#include <cmath>

#include "aspectMath.hpp"
#include "common.hpp"
#include "elements/bufferGraph.hpp"
#include "gpu.hpp"
#include "logging.hpp"
#include "params.h"



cGpu::cGpu(codecNativeWindowFnType nativeWindowFn)
  : mDisplay((EGLDisplay)NULL),
    mSurface((EGLSurface)NULL),
    mContext((EGLContext)NULL),
    mOwlProgram((GLuint)0),
    mGraphProgram((GLuint)0),
    mOutWidth(0),
    mOutHeight(0),
    mpVertexes((GLfloat *)NULL),
    mCvPanoFbo((GLuint)0),
    mCameraTexId((GLuint)0),
    mCvPanoFboTexId((GLuint)0),
    mLogoTexId((GLuint)0),
    mLogoWidth(0),  // Logo not yet loaded
    mLogoHeight(0),
    mRgbaOverlayTexId((GLuint)0),
    mRgbaOverlayX(0),
    mRgbaOverlayY(0),
    mRgbaOverlayWidth(0), // RGBA overlaly not yet loaded
    mRgbaOverlayHeight(0),
    mRgbaOverlayRenderWidth(0),
    mRgbaOverlayRenderHeight(0),
    mRgbaOverlayAlpha(0),
    mAoiPinTexId((GLuint)0)
{
  // store external function pointers for later use
  mGetNativeWindowFn = nativeWindowFn;

  // clear logo and overlay dimensions to indicate they're not yet loaded
  mLogoWidth = mLogoHeight = 0;
  mRgbaOverlayWidth = mRgbaOverlayHeight = 0;
  mSystemTestMode = false;
}

cGpu::cGpu(EGLNativeDisplayType nativeDisplay,
	   EGLNativeWindowType nativeWindow)
: mDisplay((EGLDisplay)NULL),
  mSurface((EGLSurface)NULL),
  mContext((EGLContext)NULL),
  mOwlProgram((GLuint)0),
  mGraphProgram((GLuint)0),
  mOutWidth(0),
  mOutHeight(0),
  mpVertexes((GLfloat *)NULL),
  mCvPanoFbo((GLuint)0),
  mCameraTexId((GLuint)0),
  mCvPanoFboTexId((GLuint)0),
  mLogoTexId((GLuint)0),
  mLogoWidth(0),  // Logo not yet loaded
  mLogoHeight(0),
  mRgbaOverlayTexId((GLuint)0),
  mRgbaOverlayX(0),
  mRgbaOverlayY(0),
  mRgbaOverlayWidth(0), // RGBA overlaly not yet loaded
  mRgbaOverlayHeight(0),
  mRgbaOverlayRenderWidth(0),
  mRgbaOverlayRenderHeight(0),
  mRgbaOverlayAlpha(0),
  mAoiPinTexId((GLuint)0)
{
  // store the provided native display and window for later setup
  mNativeDisplay = nativeDisplay;
  mNativeWindow  = nativeWindow;
  mSystemTestMode = false;
}

cGpu::~cGpu()
{
  finish();
}

// ----------
// shaders
const GLchar *gpOwlVertexShaderStr =
  "attribute vec2 a_position;\n"
  "uniform   bool  u_flipped;\n"
  "uniform   bool  u_mirrored;\n"
  "uniform   float u_x0;\n"
  "uniform   float u_x1;\n"
  "uniform   float u_y0;\n"
  "uniform   float u_y1;\n"
  "varying   vec2  v_texPos;\n"
  "void main() {\n"
  "   float x = u_x0 + a_position.x * (u_x1 - u_x0);\n"
  "   float y = u_y0 + a_position.y * (u_y1 - u_y0);\n"
  "   v_texPos = vec2(a_position.x, a_position.y);\n"
  "   vec2 clipPos = 2.0*vec2(x, y) - vec2(1.0, 1.0);\n"
  "   if (!u_flipped)\n"
  "   { // properly orient screen preview\n"
  "     clipPos.y = -clipPos.y;\n"
  "   }\n"
  "   if (u_mirrored)\n"
  "   { // Mirror in X\n"
  "     clipPos.x = -clipPos.x;\n"
  "   }\n"
  "   gl_Position = vec4(clipPos, 0.0, 1.0);\n"
  "}\n";

const GLchar *gpOwlFragmentShaderStr =
#if defined(__ANDROID__) && !defined(MOCK_INPUT)
  "#extension GL_OES_EGL_image_external : require\n"
  "uniform samplerExternalOES cameraSampler;\n"
#else // linux or MOCK_INPUT
  "uniform sampler2D cameraSampler;\n"
#endif
  "uniform sampler2D logoSampler;\n"
  "uniform sampler2D rgbaOverlaySampler;\n"
  "uniform sampler2D aoiPinSampler;\n"
  "precision highp float;\n"
  "uniform   int   u_render;\n"
  "uniform   vec3  u_color;\n"
  "uniform   float u_alpha;\n"
  "uniform   float u_r0;\n"
  "uniform   float u_r1;\n"
  "uniform   float u_cx;\n"
  "uniform   float u_cy;\n"
  "uniform   float u_theta0;\n"
  "uniform   float u_theta1;\n"
  "uniform   float u_stretch;\n"
  "uniform   float u_bend;\n"
  "varying   vec2 v_texPos;\n"
  "void main() {\n"
  "   float r, rnorm;\n"
  "   float theta;\n"
  "   vec2 pos;\n"
  "   vec4 color;\n"
  "   if (u_render == 0)\n"
  "   { // ===== RENDER_UNROLL =====\n"
  "     rnorm = v_texPos.y;\n"
  "     rnorm = rnorm - u_stretch * (1.0 - rnorm) * rnorm;\n"
  "     rnorm = rnorm + u_bend * (1.0 - rnorm) * pow(v_texPos.x - 0.5, 2.);\n"
  "     r =  clamp(u_r0 + rnorm * (u_r1 - u_r0), 0.0, 0.5);\n"
  "     theta = -u_theta0 - v_texPos.x * (u_theta1 - u_theta0);\n"
  "     pos = vec2(u_cx + r*cos(theta), u_cy + r*sin(theta));\n"
  "     color = texture2D(cameraSampler, pos);\n"
  "     color.a = u_alpha;\n"
  "   }\n"
  "   else if (u_render == 1)\n"
  "   { // ===== RENDER_LOGO =====\n"
  "     color = texture2D(logoSampler, v_texPos);\n"
  "     color.a = u_alpha * color.a;\n"
  "   }\n"
  "   else if (u_render == 2)\n"
  "   { // ===== RENDER_RGBA_OVERLAY =====\n"
  "     color = texture2D(rgbaOverlaySampler, v_texPos);\n"
  "     color.a = u_alpha * color.a;\n"
  "   }\n"
  "   else if (u_render == 3)\n"
  "   { // ===== RENDER_AOI_PIN =====\n"
  "     color = texture2D(aoiPinSampler, v_texPos);\n"
  "     color.a = u_alpha * color.a;\n"
  "   }\n"
  "   else if (u_render >= 4)\n"
  "   { // ===== RENDER_SHAPE =====\n"
  "     color = vec4(u_color, u_alpha);\n"
  "     if (u_render == 5)\n"
  "     { // ----- RENDER_SHAPE_ELLIPSE -----\n"
  "       pos = v_texPos - vec2(0.5, 0.5);\n"
  "       r = length(pos);\n"
  "       if (r > 0.5 || r < u_r0) color.a = 0.0;\n"
  "     }\n"
  "   }\n"
  "   gl_FragColor = color;\n"
  "}\n";

const GLchar *gpGraphVertexShaderStr =
  "attribute vec2 a_position;\n"
  "uniform mediump vec4 u_color;\n"
  "void main() {\n"
  "   gl_Position = vec4(a_position * 2.0 - 1.0, 0.0, 1.0);\n"
  "}\n";
const GLchar *gpGraphFragmentShaderStr =
  "uniform mediump vec4 u_color;\n"
  "void main() {\n"
  "   gl_FragColor = u_color;\n"
  "}\n";

// NOTE 1: Above, samplerExternalOES is needed for camera-based input
// textures on android; the usual sampler2D is used on linux

// NOTE 2: When unrolling, rnorm is used to control the vertical
// warping as the output image y-coordinate ranges from 0 to 1. The
// three equations above implement, respectively, a linear mapping
// with no extra warping, a vertical expansion that favors the top
// half of the output image, and a bending that increases towards the
// top of the output image and with greater horizontal deviation from
// the middle of the output image. Different equations could be used
// to add some nuance to how the warping is done, but the ones above
// are relatively simple and capture the main qualities of the warping
// with just two parameters to adjust.

// NOTE 3: The fragment shader above is a monolithic program with a
// series of if-else statements. Consider separating this into
// multiple programs if performance is a concern. Testing with the
// Adreno profiler showed no noticeable improvement when the if-else
// statements were removed and only the panoramic strip was rendered.

// NOTE 4: These shaders should be loaded dynamically from files rather than
// hard-coded. Hard-coding reduces flexibility and readability and requires
// recompiling to make any shader changes.

// ----------


/** This function animates the Owl logo based on timing and the number
 * of panels rendered.
 *
 * Animation is normally the resposibility of the owl-core subframes,
 * but for convenience the special case of the logo is handled
 * here. The intention is that the logo will be large and in the
 * middle of the stage when only the panoramic strip is
 * present. Otherwise it will be in a corner of the stage. And
 * transitions between the two will be animated.
 *
 * @param nPanels is the number of rendered panels.
 * @param pX is a pointer to the logo horizontal position.
 * @param pY is a pointer to the logo vertical position.
 * @param pScale is a pointer to the logo scale factor.
 */
void cGpu::animateLogo(int nPanels, int *pX, int *pY, float *pScale)
{
  int x, y;
  double elapsedTime = 0.0;
  float scale, k;
  const int kCenterX = OUTPUT_WIDTH / 2;
  const int kCenterY = (OUTPUT_HEIGHT + OUT_PANO_RES_Y) / 2;
  const float kAnimationDuration = 1.5; // seconds for logo to complete move
  static bool animating = false;
  static int animationDir = +1;
  static struct timespec t0, t1;
  static bool init = true;

  if (init)
    {
      clock_gettime(CLOCK_MONOTONIC, &t0);
      init = false;
    }

  if (animating)
    {
      // update elapsed seconds
      clock_gettime(CLOCK_MONOTONIC, &t1);
      elapsedTime = elapsedTimeSec(t0, t1);

      // check for end of animation
      if (elapsedTime > kAnimationDuration)
	{
	  animating = false; // turn off animation
	  animationDir = -animationDir; // change direction for next time
	  elapsedTime = 0; // reset elapsed time
	}
    }
  else
    {
      // check if animation should start
      if ( (animationDir > 0 && nPanels > 1)      // panels on stage
	   || (animationDir < 0 && nPanels < 2) ) // no panels on stage
	{
	  // reset timer and set flag to start animating
	  clock_gettime(CLOCK_MONOTONIC, &t0);
	  animating = true;
	}
    }

  // convert elapsed time to fraction complete
  k = elapsedTime / kAnimationDuration;

  // account for direction of animation
  if (animationDir < 0)
    { k = 1.0 - k; }

  // interpolate between endpoints
  x = k * LOGO_X + (1.0 - k) * (kCenterX - mLogoWidth / 2);
  y = k * LOGO_Y + (1.0 - k) * (kCenterY - mLogoHeight / 2);
  scale = k * LOGO_SCALE + (1.0 - k) * 1.0;

  // set the pointer values
  *pX = x;
  *pY = y;
  *pScale = scale;
}

/** This function checks for (and clears) any pending EGL error.
 *
 * @param pMsg is a pointer to a string to include as part of the log message.
 * @return the error number.
 */
int  cGpu::checkEglError(const char *pMsg)
{
  EGLint err = eglGetError();
  if (err != EGL_SUCCESS)
    { LOGE("EGL ERROR 0x%04x: %s\n", err, pMsg); }
  return err;
}

/** This function checks for (and clears) any pending GL error.
 *
 * @param pMsg is a pointer to a string to include as part of the log message.
 * @return the error number.
 */
int  cGpu::checkGlError(const char *pMsg)
{
  EGLint err = glGetError();
  if (err != GL_NO_ERROR)
    { LOGE("GL ERROR 0x%04x: %s\n", err, pMsg); }
  return err;
}

/** This function clears the general-purpose RGBA overlay, so it no
 * longer appears onscreen.
 */
void cGpu::clearRgbaOverlay(void)
{
  // to turn off overlay, set width and height to 0
  mRgbaOverlayWidth = mRgbaOverlayHeight = 0;
}

/** This function copies the CV panoramic strip to the CPU.
 *
 * @param pDst is a pointer to the destination buffer.
 */
void   cGpu::copyOutput(unsigned char *pDst)
{
#ifdef __ANDROID__
  // use a faster Graphic Buffer copy, as exposed by libOwlPG
  grBufCopyPixels(pDst);
#else
  // use a standard (slow) call to glReadPixels()
  glBindFramebuffer(GL_FRAMEBUFFER, mCvPanoFbo);
  glReadPixels(0, 0, CV_PANO_RES_X, CV_PANO_RES_Y,
	       GL_RGBA, GL_UNSIGNED_BYTE, pDst);
  checkGlError("glReadPixels");
#endif
}

/** This function implements boilerplate setup of the shader program. */
GLuint cGpu::createProgram(const GLchar *pvShaderSrc,
			   const GLchar *pfShaderSrc )
{
  // load shaders
  GLuint vShader = loadShader(GL_VERTEX_SHADER, pvShaderSrc);
  GLuint fShader = loadShader(GL_FRAGMENT_SHADER, pfShaderSrc);

  // create the program with shaders
  GLuint programId = glCreateProgram();
  checkGlError("glCreateProgram");
  glAttachShader(programId, fShader);
  checkGlError("glAttachShader (fragment)");
  glAttachShader(programId, vShader);
  checkGlError("glAttachShader (vertex)");

  // link and use the program
  glLinkProgram(programId);
  checkGlError("glLinkProgram");
  glUseProgram(programId);
  checkGlError("glUseProgram");

  // detach and delete shaders
  glDetachShader(programId, vShader);
  checkGlError("glDetachShader vShader");
  glDetachShader(programId, fShader);
  checkGlError("glDetachShader fShader");
  glDeleteShader(vShader);
  checkGlError("glDeleteShader vShader");
  glDeleteShader(fShader);
  checkGlError("glDeleteShader fShader");

  return programId;
}

/** This function cleans up resources. */
void cGpu::finish(void)
{
  if (mpVertexes != NULL)
    {
      free(mpVertexes);
    }

  if (mDisplay != (EGLDisplay)NULL)
    {
      // release EGL resources
      eglMakeCurrent(mDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
      checkEglError("eglMakeCurrent");
      eglDestroyContext(mDisplay, mContext);
      checkEglError("eglDestroyContext");
      eglDestroySurface(mDisplay, mSurface);
      checkEglError("eglDestroySurface");
      eglTerminate(mDisplay);
      checkEglError("eglTerminate");

      glDeleteProgram(mOwlProgram);
      checkGlError("glDeleteOwlProgram");
      glDeleteProgram(mGraphProgram);
      checkGlError("glDeleteGraphProgram");
    }
}

/** This function loads and compiles a shader from text input.
 *
 * @param shaderType is either GL_VERTEX_SHADER or GL_FRAGMENT_SHADER.
 * @param pShaderStr is a pointer to the text with the shader program.
 * @return the id of the loaded shader.
 */
int  cGpu::loadShader(int shaderType, const GLchar *pShaderStr)
{
  // create and compile the shader
  GLuint shader = glCreateShader(shaderType);
  glShaderSource(shader, 1, &pShaderStr, NULL);
  glCompileShader(shader);
  checkGlError("glCompileShader");

  GLint  status;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  if (!status)
    {
      // get length of the error log
      GLint logLen;
      glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLen);

      // get the error log
      GLchar *shaderLog = new GLchar[logLen];
      memset(shaderLog, 0, logLen);
      GLint actualLen = 0;
      glGetShaderInfoLog(shader, logLen, &actualLen, shaderLog);

      // print the log
      LOGE("Error: unable to compile %s shader.\n",
	   (shaderType == GL_VERTEX_SHADER ? "vertex" : "fragment") );
      LOGI("Shader Log:\n");
      LOGI("%s\n", shaderLog);
      
      delete shaderLog;
    }
  return shader;
}

/** This function sets up the attributes of the shader program. */
void cGpu::prepareAttributes(void)
{
  // set attributes
  glBindAttribLocation(mOwlProgram, 0, "a_position");
  checkGlError("glBindAttribLocation");
}

/** This function sets up the framebuffer object used for offscreen
    rendering of the computer vision panoramic strip. */
void cGpu::prepareCvPanoFbo(void)
{
  // create texture for panoramic strip
  glGenTextures(1, &mCvPanoFboTexId);
  glBindTexture(GL_TEXTURE_2D, mCvPanoFboTexId);
  checkGlError("glBindTexture CvPanoFbo");
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  checkGlError("glTexParameter CvPanoFbo");
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  checkGlError("glTexParameter CvPanoFbo");

#ifdef __ANDROID__
  // get a native buffer
  EGLClientBuffer buffer = \
    (EGLClientBuffer) grBufGetNativeBuffer(CV_PANO_RES_X, CV_PANO_RES_Y);
  // map a KHR image to the buffer
  EGLint pAttrs[] = {
    EGL_WIDTH,               CV_PANO_RES_X,
    EGL_HEIGHT,              CV_PANO_RES_Y,
    EGL_MATCH_FORMAT_KHR,    EGL_FORMAT_RGBA_8888_KHR,
    EGL_IMAGE_PRESERVED_KHR, EGL_TRUE,
    EGL_NONE
  };
  EGLImageKHR image = eglCreateImageKHR(eglGetCurrentDisplay(),
					EGL_NO_CONTEXT,
					EGL_NATIVE_BUFFER_ANDROID,
					buffer, pAttrs);
  checkEglError("eglCreateImageKHR CvPano");
  // bind image to texture
  glEGLImageTargetTexture2DOES(GL_TEXTURE_2D, image);
  checkGlError("glEGLImageTargetTexture2DOES CvPano");
#else
  // set up the texture images
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, CV_PANO_RES_X, CV_PANO_RES_Y, 0,
	       GL_RGBA, GL_UNSIGNED_BYTE, 0);
#endif

  // create framebuffer objects
  glGenFramebuffers(1, &mCvPanoFbo);
  glBindFramebuffer(GL_FRAMEBUFFER, mCvPanoFbo);

  // attach texture to FBO
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
			 mCvPanoFboTexId, 0);

  // check FBO status
  GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if(status != GL_FRAMEBUFFER_COMPLETE)
    { LOGE("GL ERROR 0x%04x: %s\n", status,
	   "glCheckFramebufferStatus CvPano"); }
  else
    { LOGI("created %dx%d framebuffer object\n",
	   CV_PANO_RES_X, CV_PANO_RES_Y); }

  // restore defaults
  glBindTexture(GL_TEXTURE_2D, 0);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

/** This function implements boilerplate setup of the EGL interface. */
void cGpu::prepareEgl(void)
{
  EGLint major, minor;
  EGLint numConfigs;
  EGLConfig config;
  EGLint width, height;
  EGLint format;

  // open connection to the display
  if (mNativeDisplay == NULL)
    { mDisplay = eglGetDisplay(EGL_DEFAULT_DISPLAY); }
  else
    { mDisplay = eglGetDisplay(mNativeDisplay); }
  checkEglError("eglGetDisplay");
    
  // initialize display connection
  eglInitialize(mDisplay, &major, &minor);
  checkEglError("eglInitialize");
  LOGI("Display initialized, version %d.%d\n", major, minor);

  // find a suitable configuration
  EGLint  pConfigAttrs [] =
    {
      EGL_RENDERABLE_TYPE,       EGL_OPENGL_ES2_BIT,
      EGL_SURFACE_TYPE,          EGL_WINDOW_BIT,
      EGL_RED_SIZE,              8,
      EGL_GREEN_SIZE,            8,
      EGL_BLUE_SIZE,             8,
      EGL_ALPHA_SIZE,            8,
#ifdef __ANDROID__
      EGL_RECORDABLE_ANDROID,    1,
#endif
      EGL_NONE
    };
  eglChooseConfig(mDisplay, pConfigAttrs, &config, 1, &numConfigs);
  checkEglError("eglChooseConfig");
      
  // get native window 
  EGLNativeWindowType nativeWindow;
#ifdef __ANDROID__
  nativeWindow = mGetNativeWindowFn();
#else
  nativeWindow = mNativeWindow;
#endif

  // create surface from native window
  mSurface = eglCreateWindowSurface(mDisplay, config, nativeWindow, NULL);
  checkEglError("eglCreateWindowSurface");

  // create rendering context
  EGLint  pContextAttrs [] =
    {
      EGL_CONTEXT_CLIENT_VERSION, 2,
      EGL_NONE,
    };
  mContext = eglCreateContext(mDisplay, config, EGL_NO_CONTEXT, pContextAttrs);
  checkEglError("eglCreateContext");
  eglMakeCurrent(mDisplay, mSurface, mSurface, mContext);
  checkEglError("eglMakeCurrent");

  // check surface size
  eglQuerySurface(mDisplay, mSurface, EGL_WIDTH, &width);
  eglQuerySurface(mDisplay, mSurface, EGL_HEIGHT, &height);
  checkEglError("eglQuerySurface width/height");
  LOGI("Created %dx%d surface\n", width, height);
}

/** This function sets up textures for the camera image, logo, and
    general-purpose overlay. */
void cGpu::prepareTextures(void)
{
  // create camera image texture and set properties
  glGenTextures(1, &mCameraTexId);
#if defined(__ANDROID__) && !defined(MOCK_INPUT)
  glBindTexture(GL_TEXTURE_EXTERNAL_OES, mCameraTexId);
  checkGlError("glBindTexture camera");
  glTexParameterf(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  checkGlError("glTexParameterf");
  glTexParameterf(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  checkGlError("glTexParameterf");
#else // linux or MOCK_INPUT
  glBindTexture(GL_TEXTURE_2D, mCameraTexId);
  checkGlError("glBindTexture camera");
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  checkGlError("glTexParameterf");
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  checkGlError("glTexParameterf");
#endif

  // create logo texture and set properties
  glGenTextures(1, &mLogoTexId);
  glBindTexture(GL_TEXTURE_2D, mLogoTexId);
  checkGlError("glBindTexture logo");
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  checkGlError("glTexParameterf");
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  checkGlError("glTexParameterf");

  // create image overlay texture and set properties
  glGenTextures(1, &mRgbaOverlayTexId);
  glBindTexture(GL_TEXTURE_2D, mRgbaOverlayTexId);
  checkGlError("glBindTexture image overlay");
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  checkGlError("glTexParameterf");
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  checkGlError("glTexParameterf");

  // create pinned AOI overlay texture and set properties
  glGenTextures(1, &mAoiPinTexId);
  glBindTexture(GL_TEXTURE_2D, mAoiPinTexId);
  checkGlError("glBindTexture pinned AOI overlay");
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  checkGlError("glTexParameterf");
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  checkGlError("glTexParameterf");

  // assign textures to texture units so we can use more than one sampler
  glActiveTexture(GL_TEXTURE0); // camera uses texture unit 0
#if defined(__ANDROID__) && !defined(MOCK_INPUT)
  glBindTexture(GL_TEXTURE_EXTERNAL_OES, mCameraTexId);
#else // linux or MOCK_INPUT
  glBindTexture(GL_TEXTURE_2D, mCameraTexId);
#endif
  glActiveTexture(GL_TEXTURE1); // logo uses texture unit 1
  glBindTexture(GL_TEXTURE_2D, mLogoTexId);
  glActiveTexture(GL_TEXTURE2); // general-purpose overlay uses texture unit 2
  glBindTexture(GL_TEXTURE_2D, mRgbaOverlayTexId);
  glActiveTexture(GL_TEXTURE3); // CvPano FBO uses texture unit 3
  // must call prepareCvPanoFbo before next line
  glBindTexture(GL_TEXTURE_2D, mCvPanoFboTexId);
  glActiveTexture(GL_TEXTURE4); // pinned AOI overlay -> texture unit 4
  glBindTexture(GL_TEXTURE_2D, mAoiPinTexId);

  // restore default
  glActiveTexture(GL_TEXTURE0);
}

/** This function sets up the uniforms of the shader program. */
void cGpu::prepareUniforms(void)
{
  // set up uniforms with reasonable initial values to be overridden later
  GLuint u;
  u = glGetUniformLocation(mOwlProgram, "u_render");
  glUniform1i(u, 0);
  u = glGetUniformLocation(mOwlProgram, "u_flipped");
  glUniform1i(u, 0);
  u = glGetUniformLocation(mOwlProgram, "u_mirrored");
  glUniform1i(u, 0);
  u = glGetUniformLocation(mOwlProgram, "u_r0");
  glUniform1f(u, 0.25);
  u = glGetUniformLocation(mOwlProgram, "u_r1");
  glUniform1f(u, 0.50);
  u = glGetUniformLocation(mOwlProgram, "u_cx");
  glUniform1f(u, 0.50);
  u = glGetUniformLocation(mOwlProgram, "u_cy");
  glUniform1f(u, 0.50);
  u = glGetUniformLocation(mOwlProgram, "u_theta0");
  glUniform1f(u, 0.0);
  u = glGetUniformLocation(mOwlProgram, "u_theta1");
  glUniform1f(u, M_PI * 2.0);
  u = glGetUniformLocation(mOwlProgram, "u_x0");
  glUniform1f(u, 0.0);
  u = glGetUniformLocation(mOwlProgram, "u_x1");
  glUniform1f(u, 1.0);
  u = glGetUniformLocation(mOwlProgram, "u_y0");
  glUniform1f(u, 0.0);
  u = glGetUniformLocation(mOwlProgram, "u_y1");
  glUniform1f(u, 1.0);
  u = glGetUniformLocation(mOwlProgram, "u_color");
  glUniform3f(u, 0.0, 0.0, 0.0);
  u = glGetUniformLocation(mOwlProgram, "u_alpha");
  glUniform1f(u, 1.0);
  u = glGetUniformLocation(mOwlProgram, "u_stretch");
  glUniform1f(u, 0.0);
  u = glGetUniformLocation(mOwlProgram, "u_bend");
  glUniform1f(u, 0.0);

  checkGlError("uniforms");

  // assign samplers to texture units
  u = glGetUniformLocation(mOwlProgram, "cameraSampler");
  glUniform1i(u, 0);
  u = glGetUniformLocation(mOwlProgram, "logoSampler");
  glUniform1i(u, 1);
  u = glGetUniformLocation(mOwlProgram, "rgbaOverlaySampler");
  glUniform1i(u, 2);
  u = glGetUniformLocation(mOwlProgram, "aoiPinSampler");
  glUniform1i(u, 4);
  checkGlError("uniforms samplers");
}

/** This function sets up the vertexes that define the quad used to
    render each image panel. */
void cGpu::prepareVertexes(void)
{
  GLuint i, nVertexes, nBytes;

  // set number of vertexes
  // we need 6 vertexes for a two-triangle quad, plus
  // 4 more vertexes for rendering a rectangle, plus
  // 2 more vertexes for rendering an arbitrary line
  nVertexes = 6 + 4 + 2;

  // allocate vertexes, accounting for 2 coordinates per vertex
  mpVertexes = (GLfloat *)  malloc(nVertexes * 2 * sizeof(GLfloat));
  if (mpVertexes == NULL)
    { LOGE("unable to allocate quads\n"); }

  // init the array index
  i = 0;

  // construct vertexes for the quad
  mpVertexes[i++] = 0; mpVertexes[i++] = 0;
  mpVertexes[i++] = 1; mpVertexes[i++] = 0;
  mpVertexes[i++] = 0; mpVertexes[i++] = 1;
  mpVertexes[i++] = 0; mpVertexes[i++] = 1;
  mpVertexes[i++] = 1; mpVertexes[i++] = 0;
  mpVertexes[i++] = 1; mpVertexes[i++] = 1;

  // construct vertexes for the rectangle
  mpVertexes[i++] = 0; mpVertexes[i++] = 0;
  mpVertexes[i++] = 1; mpVertexes[i++] = 0;
  mpVertexes[i++] = 1; mpVertexes[i++] = 1;
  mpVertexes[i++] = 0; mpVertexes[i++] = 1;

  // construct vertexes for the line
  mpVertexes[i++] = 0; mpVertexes[i++] = 0;
  mpVertexes[i++] = 1; mpVertexes[i++] = 1;

  // set up the vertex buffer
  glGenBuffers(1, &mQuadVboId);
  glBindBuffer(GL_ARRAY_BUFFER, mQuadVboId);
  nBytes = nVertexes * 2 * sizeof(GLfloat); // two coords per vertex
  glBufferData(GL_ARRAY_BUFFER, nBytes, mpVertexes, GL_STATIC_DRAW);
  checkGlError("VBO");

  // enable the vertex attribute array and set the pointer
  GLint a_position;
  a_position = glGetAttribLocation(mOwlProgram, "a_position");
  checkGlError("glGetAttribLocation");
  glEnableVertexAttribArray(a_position);
  checkGlError("glEnableVertexAttribArray");
  glVertexAttribPointer(a_position, 2, GL_FLOAT, GL_FALSE, 0, 0);
  checkGlError("glVertexAttribPointer");
}

/** This function implements boilerplate setup of the viewport. */
void cGpu::prepareViewport(void)
{
  // set viewport, check it, and store width and height
  int    pCoords[4];
  glViewport(0, 0, OUTPUT_WIDTH, OUTPUT_HEIGHT);
  checkGlError("glViewport");
  glGetIntegerv(GL_VIEWPORT, pCoords);
  LOGI("Viewport Coords: %d, %d, %d, %d\n",
       pCoords[0], pCoords[1], pCoords[2], pCoords[3]);
  mOutWidth = pCoords[2];
  mOutHeight = pCoords[3];
}

/** This functions is the primary routine for rendering all the
 * elements of each output frame.
 *
 * @param panels is a vector of panels that describe each sub-frame to render.
 * @param shapes is a vector of shapes (lines, rects, ellipses) to
 * render as overlays.
 */
void cGpu::renderFrame(std::vector<owl::cPanel> panels,
		       std::vector<owl::abcShape*> shapes,
		       std::vector<owl::cBufferGraph*> graphs,
		       owl::cRgbaOverlayLists *pOverlays)
{
  const float kAlpha = 1.0;
  int   panelNum = 0;
  int   x0, x1, y0, y1, w, h;
  float theta0, theta1;
  float rInner, rOuter;
  float hFovWeight, rOuterWeight;
  float stretchCoeff, bendCoeff;

  // clear frame
  glClear(GL_COLOR_BUFFER_BIT);

  // render unrolled panels
  for(std::vector<owl::cPanel>::iterator iPanel = panels.begin(); 
      iPanel != panels.end(); ++iPanel, panelNum++)
    {
      // set the warping coefficients
      // Note: These coefficient assignments are in the panel loop b/c
      // they will likely need to be generalized later to account for
      // the panel size
      stretchCoeff = CAMERA_WARP_STRETCH;
      bendCoeff    = CAMERA_WARP_BEND;

      // copy/calculate some convenient values
      x0 = (*iPanel).posX0;
      y0 = (*iPanel).posY0;
      x1 = (*iPanel).posX1;
      y1 = (*iPanel).posY1;
      w = x1 - x0;
      h = y1 - y0;
      theta0 = (*iPanel).theta0;
      theta1 = (*iPanel).theta1;
      rInner = (*iPanel).radiusInner;
      rOuter = (*iPanel).radiusOuter;

      // render the panel
      renderUnroll(x0, y0, w, h, theta0, theta1, rInner, rOuter,
		   kAlpha, stretchCoeff, bendCoeff);

#ifdef MOCK_STAGE_PANEL
      // render a panning stage panel
      const float  kDeltaPhase = 0.005;
      static float phase = 0;
      x0 = SUBFRAME_BORDER_MARGIN;
      y0 = OUT_PANO_RES_Y + 2 * SUBFRAME_BORDER_MARGIN;
      w = OUTPUT_WIDTH - 2 * SUBFRAME_BORDER_MARGIN;
      h = OUTPUT_HEIGHT - OUT_PANO_RES_Y - 3 * SUBFRAME_BORDER_MARGIN;
      theta0 = phase;
      theta1 = theta0 + stagePixToDeg(w * M_PI / 180);
      // guess at radii but then enforce a proper aspect ratio
      rOuter = 0.45;
      rInner = 0.25;
      aspectMath::adjustLegacyParams(&theta0, &theta1, &rInner, &rOuter,
				     aspectMath::getMaxRadius(),
				     w, h, 1.0, 0.0);
      renderUnroll(x0, y0, w, h, theta0, theta1, rInner, rOuter,
		   kAlpha, stretchCoeff, bendCoeff);
      phase += kDeltaPhase;
#endif
    }

  // render image overlay (system test, status frame, or banner line)
  if (mRgbaOverlayWidth > 0) // then an image is loaded and ready to render
    {
      renderRgbaOverlay(mRgbaOverlayX, mRgbaOverlayY,
			mRgbaOverlayRenderWidth, mRgbaOverlayRenderHeight,
			mRgbaOverlayAlpha );
    }

  if (!mSystemTestMode)
    {
      // render pinned Aoi pins, if necessary
      std::vector<owl::cPinnedAoiOverlay> *pPinnedAois =
          &pOverlays->mPinnedAoiOverlays;

      for (std::vector<owl::cPinnedAoiOverlay>::iterator iPin = pPinnedAois->begin();
          iPin != pPinnedAois->end();
          iPin++)
        {
          (*iPin).render();
        }

      // render logo icon, if necessary
      std::vector<owl::cIconOverlay *> *pIcon =
          &pOverlays->mIconOverlays;

      for (std::vector<owl::cIconOverlay *>::iterator iIcon = pIcon->begin();
          iIcon != pIcon->end();
          iIcon++)
        {
          if ((*iIcon)->isActive())
            {
              if (!(*iIcon)->isLoaded())
                {
                  (*iIcon)->loadActiveIcon();
                }
              (*iIcon)->animationControl(panels.size());
              (*iIcon)->render();
            }
        }
    }

  // render overlay shapes in the order they were queued
  // Note that we need a vector of **pointers** so that we can take
  // advantage of polymorphism. These pointers are backed by vectors
  // of objects (see owlHook.cpp)
  for(std::vector<owl::abcShape*>::iterator iShape = shapes.begin();
      iShape != shapes.end(); ++iShape)
    { (*iShape)->draw(this); }

  if(graphs.size() > 0)
    { // handle buffer graphs
      glUseProgram(mGraphProgram);
      checkGlError("renderSetGraphShader");
  
      GLint kPanoW = (GLint)((float)CV_PANO_RES_X * OUT_PANO_SCALE);
      GLint kPanoH = (GLint)((float)CV_PANO_RES_Y * OUT_PANO_SCALE);
      GLint kPanoX = (GLint)((float)(OUTPUT_WIDTH - kPanoW) / 2.0f);
      GLint kPanoY = OUTPUT_HEIGHT - PANORAMIC_VERT_MARGIN - kPanoH;
      glViewport(kPanoX, kPanoY, kPanoW, kPanoH);
      checkGlError("renderSetViewport1");

      for(auto pGraph : graphs)
	{ pGraph->draw(this); }
  
      // re-enable owl vertices and attributes
      glBindBuffer(GL_ARRAY_BUFFER, mQuadVboId);
      checkGlError("renderSetOOwlVBO");
  
      GLint a_position;
      a_position = glGetAttribLocation(mOwlProgram, "a_position");
      checkGlError("renderGetAttribLocation");
      glEnableVertexAttribArray(a_position);
      checkGlError("renderEnableVertexAttribArray");
      glVertexAttribPointer(a_position, 2, GL_FLOAT, GL_FALSE, 0, 0);
      checkGlError("renderVertexAttribPointer");
      glUseProgram(mOwlProgram);
      checkGlError("renderSetOwlShader");
  
      glViewport(0, 0, OUTPUT_WIDTH, OUTPUT_HEIGHT);
      checkGlError("renderSetViewport2");
    }
}

/** This function assigns the background color as a shade of gray. */
void cGpu::setBackgroundColor(void)
{
  const float gray = 0.0;
  glClearColor(gray, gray, gray, 1.0);
}

/** This function implements boilerpate setup of the blending mode. */
void cGpu::setBlending(void)
{
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

/** This function sets the uniforms that correspond to the camera
 *  center.
 *
 * @param xNormalized is the horizontal location of the center in
 * [0.0, 1.0], usually close to 0.5.
 * @param yNormalized is the vertical location of the center in
 * [0.0, 1.0], usually close to 0.5.
 */
void cGpu::setCameraCenter(float xNormalized, float yNormalized)
{
  GLint u;

  u = glGetUniformLocation(mOwlProgram, "u_cx");
  glUniform1f(u, xNormalized);
  u = glGetUniformLocation(mOwlProgram, "u_cy");
  glUniform1f(u, yNormalized);
}

/** This function sets the RGBA image for the logo overlay.
 *
 * Normally, this function would be called once at startup, to load
 * the desired image into the corresponding texture unit. This overlay
 * is intended to be permanent on the screen once setLog() is
 * called. For an RGBA overlay that might change, use setRgbaOverlay()
 * and clearRgbaOverlay() instead.
 *
 * @param pImageBuf is a pointer to an RGBA image.
 * @param width is the width of the RGBA image.
 * @param height is the height of the RGBA image.
 */
void cGpu::setLogo(unsigned char *pImageBuf, int width, int height)
{
  mLogoWidth = width;
  mLogoHeight = height;
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, mLogoTexId);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height,
	       0, GL_RGBA, GL_UNSIGNED_BYTE, pImageBuf);
  checkGlError("glTexImage2D setLogo");
  glActiveTexture(GL_TEXTURE0); // restore default
}

/** This function sets the general-purpose RGBA image overlay.
 *
 * This function could be called numerous times while the application
 * runs, with each call used to change either the source image or the
 * overlay properties (location, size, transparency). Use
 * clearRgbaOverlay() to efectively erase a loaded overlay from the
 * output.
 *
 * @param pImageBuf is a pointer to an RGBA image.
 * @param width is the width of the RGBA image.
 * @param height is the height of the RGBA image.
 * @param renderWidth is the onscreen width of the RGBA image.
 * @param renderHeight is the onscreen height of the RGBA image.
 * @param alpha is the transparency of the RGBA image in [0.,1.].
 */
void cGpu::setRgbaOverlay(unsigned char *pImageBuf, int x, int y,
			  int width, int height,
			  int renderWidth, int renderHeight,
			  float alpha)
{
  mRgbaOverlayX = x;
  mRgbaOverlayY = y;
  mRgbaOverlayWidth = width;
  mRgbaOverlayHeight = height;
  mRgbaOverlayRenderWidth = renderWidth;
  mRgbaOverlayRenderHeight = renderHeight;
  mRgbaOverlayAlpha = alpha;
  glActiveTexture(GL_TEXTURE2);
  glBindTexture(GL_TEXTURE_2D, mRgbaOverlayTexId);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height,
	       0, GL_RGBA, GL_UNSIGNED_BYTE, pImageBuf);
  checkGlError("glTexImage2D setRgbaOverlay");
  glActiveTexture(GL_TEXTURE0); // restore default
}

/** This function sets the RGBA image for the pinned AOI overlay.
 *
 * Normally, this function would be called once at startup, to load
 * the desired image into the corresponding texture unit. This overlay
 * is intended to be semi-permanent on the screen. When an AOI is pinned
 * in place on the stage, the pin icon will be shown until it is
 * released.
 *
 * @param texId is the texture unit number to use rendering this item.
 * @param pImageBuf is a pointer to an RGBA image.
 * @param width is the width of the RGBA image.
 * @param height is the height of the RGBA image.
 */
void cGpu::setRgbaTexture(int texId, unsigned char *pImageBuf,
                          int width, int height)
{
  int textureUnitMap[] = {0, 1, 2, 4, 3, 3};
  GLuint textureUnitId[] =
      {0, mLogoTexId, mRgbaOverlayTexId, mAoiPinTexId, 0, 0};
  glActiveTexture(GL_TEXTURE0 + textureUnitMap[texId]);
  glBindTexture(GL_TEXTURE_2D, textureUnitId[texId]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height,
               0, GL_RGBA, GL_UNSIGNED_BYTE, pImageBuf);
  checkGlError("glTexImage2D setRgbaTexture");
  glActiveTexture(GL_TEXTURE0); // restore default
}

/** This function provides a common point to initialize various
 * elements of the graphics system.
 *
 * Note that for the Android A/V pipeline, this function must not be
 * called with the cGpu instance is created. Instead, it must not be
 * called until the camera is initialized. See cGpu::update() below.
 */
void cGpu::setupGraphics(void)
{
  prepareEgl();
  setBackgroundColor();
  setBlending();
  prepareCvPanoFbo();   // must call before prepareTextures()
  prepareViewport();
  prepareTextures();

  // TODO: Abort if failed 
  mOwlProgram = createProgram(gpOwlVertexShaderStr, gpOwlFragmentShaderStr);
  mGraphProgram = createProgram(gpGraphVertexShaderStr, gpGraphFragmentShaderStr);
  mGraphColorUniform = glGetUniformLocation(mGraphProgram, "u_color");
  glUniform4f(mGraphColorUniform, 0.0, 0.0, 0.0, 1.0);
  glUseProgram(mOwlProgram);
  
  prepareAttributes();
  prepareUniforms();
  prepareVertexes();
}

/** This function updates the GPU with an empty set of panels and shapes. */
void cGpu::update(void *pEglClientBuffer)
{
  std::vector<owl::cPanel> panels;
  std::vector<owl::abcShape*> shapes;
  std::vector<owl::cBufferGraph*> graphs;
  owl::cRgbaOverlayLists overlays;
  update(pEglClientBuffer, panels, shapes, graphs, &overlays);
}

/** This function updates the GPU as part of the main A/V pipeline.
 *
 * @param pEglClientBuffer is a pointer to the camera image buffer.
 * @param panels is a vector of output panels to render.
 * @param shapes is a vector of overlay shapes to render.
 */
void cGpu::update(void *pEglClientBuffer, std::vector<owl::cPanel> panels,
		  std::vector<owl::abcShape*> shapes,
		  std::vector<owl::cBufferGraph*> graphs,
		  owl::cRgbaOverlayLists *pOverlays)
{
  int i, ret;
  static bool init=true;
    
  // wait for camera frame callback to trigger the first update
  // before setting up the graphics system
  if (init)
    {
      setupGraphics();
      init = false;
    }

  // For debug and development, save raw fisheye image if flag is set
#ifdef __ANDROID__
  if (mSaveFisheye)
    {
      void *pFrame;
      int width;
      int height;
      int stride;
      cv::Mat yuvImage;
      cv::Mat bgrImage;
      std::ostringstream nameStrm;
      std::string fileName;
      static int frameNum = 0;

      nameStrm << "/data/OwlLabs/fisheye_" << frameNum << ".png";
      fileName = nameStrm.str();
      frameNum++;

      LOGI("Writing image %s\n", fileName.c_str());

      lockGrBufForFrame(pEglClientBuffer, &pFrame, &width, &height, &stride);

      yuvImage = cv::Mat(height * 3 / 2, stride, CV_8UC1, pFrame);
      bgrImage = cv::Mat(height, stride, CV_8UC4);
      cv::cvtColor(yuvImage, bgrImage, cv::COLOR_YUV2BGR_NV21);
      cv::imwrite(fileName, bgrImage);

      unlockGrBufForFrame();

      LOGI("Frame saved\n");

      mSaveFisheye = false;
    }
#endif

  // ensure default framebuffer is active
  glBindFramebuffer(GL_FRAMEBUFFER, 0);

#if defined(__ANDROID__) && !defined(MOCK_INPUT)
  // transfer camera image to texture
  EGLImageKHR img = eglCreateImageKHR(mDisplay, EGL_NO_CONTEXT, EGL_NATIVE_BUFFER_ANDROID,
				      (EGLClientBuffer) pEglClientBuffer, 0);
  checkEglError("eglCreateImageKHR");
  glEGLImageTargetTexture2DOES(GL_TEXTURE_EXTERNAL_OES, (GLeglImageOES)img);
  checkGlError("glEGLImageTargetTexture2DOES");

  // render all the elements of the output frame
  renderFrame(panels, shapes, graphs, pOverlays);

  // clean up camera buffer
  eglDestroyImageKHR(mDisplay, img);
  checkEglError("eglDestroyImageKHR");
#else // linux or mock camera
  if (pEglClientBuffer != NULL)
    {
      // load the buffer into the texture; assume the image is square
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, CAMERA_HEIGHT, CAMERA_HEIGHT,
		   0, GL_RGB, GL_UNSIGNED_BYTE, pEglClientBuffer);
      checkGlError("glTexImage2D");
    }
  // render all the elements of the output frame
  renderFrame(panels, shapes, graphs, pOverlays);
#endif

  // finish the rendering and swap buffers
  glFinish();
  checkGlError("glFinish");
#ifdef __ANDROID__    
  setAndroidPresentationTime(mDisplay, mSurface);
  checkEglError("eglPresentationTimeANDROID");
#endif
  eglSwapBuffers(mDisplay, mSurface);
  checkEglError("eglSwapBuffers");

  // -- render offscreen panoramic strip for image processing by CPU
  // ensure the offscreen framebuffer is active
  glBindFramebuffer(GL_FRAMEBUFFER, mCvPanoFbo);
  // flip the image for GPU-to-CPU copy
  // Note that GPU and CPU use different coordinate systems so we must
  // flip any image intended for the CPU
  GLint u;
  u = glGetUniformLocation(mOwlProgram, "u_flipped");
  glUniform1i(u, 1);

  glClear(GL_COLOR_BUFFER_BIT);
  if (panels.size())
    {
      // render panoramic strip, taking radius values from first panel
      // NOTE: need to generalize this further
      float rInner = panels[0].radiusInner;
      float rOuter = panels[0].radiusOuter;
      float stretchCoeff = CAMERA_WARP_STRETCH;
      float bendCoeff    = CAMERA_WARP_BEND;
      renderUnroll(0, 0, CV_PANO_RES_X, CV_PANO_RES_Y,
		   0., M_PI * 2.0, rInner, rOuter,
		   1.0, stretchCoeff, bendCoeff); // alpha=1.0
    }

  // restore the u_flipped value
  glUniform1i(u, 0);
}

// ------------------------------
// public shape rendering methods
// ------------------------------

/** This function triggers the shader program to render an ellipse. */
void cGpu::renderEllipse(int x, int y, int width, int height,
			 float r, float g, float b, float alpha,
			 float lineWidth, bool filled)
{
  GLint u;

  // set the render value
  u = glGetUniformLocation(mOwlProgram, "u_render");
  glUniform1i(u, RENDER_SHAPE_ELLIPSE);

  // set the bounding box, converting from pixels to normalized values
  u = glGetUniformLocation(mOwlProgram, "u_x0");
  glUniform1f(u, float(x - width/2) / OUTPUT_WIDTH);
  u = glGetUniformLocation(mOwlProgram, "u_y0");
  glUniform1f(u, float(y - height/2) / OUTPUT_HEIGHT);
  u = glGetUniformLocation(mOwlProgram, "u_x1");
  glUniform1f(u, float(x + width/2) / OUTPUT_WIDTH);
  u = glGetUniformLocation(mOwlProgram, "u_y1");
  glUniform1f(u, float(y + height/2) / OUTPUT_HEIGHT);

  // set color and transparency
  u = glGetUniformLocation(mOwlProgram, "u_color");
  glUniform3f(u, r, g, b);
  u = glGetUniformLocation(mOwlProgram, "u_alpha");
  glUniform1f(u, alpha);

  // use u_r0 to control line width
  u = glGetUniformLocation(mOwlProgram, "u_r0");
  if (filled)
    {
      // u_r0 == 0.0 will fill the whole ellipse
      glUniform1f(u, 0.0);
    }
  else
    {
      // u_r0 sets the inner radius of an annulus; note that the rendered
      // line width will be non-uniform if the ellipse is not a circle
      glUniform1f(u, 0.5 - lineWidth/width);
    }
	
  // draw!
  glDrawArrays(GL_TRIANGLES, 0, 6);
  // 0 is start of vertex array; 6 is number of quad vertexes
}

/** This function triggers the shader program to render a line. */
void cGpu::renderLine(int x0, int y0, int x1, int y1,
		      float r, float g, float b, float alpha,
		      float lineWidth)
{
  GLint u;

  // set the render value
  u = glGetUniformLocation(mOwlProgram, "u_render");
  glUniform1i(u, RENDER_SHAPE);

  // set the endpoints, converting from pixels to normalized values
  u = glGetUniformLocation(mOwlProgram, "u_x0");
  glUniform1f(u, float(x0) / OUTPUT_WIDTH);
  u = glGetUniformLocation(mOwlProgram, "u_y0");
  glUniform1f(u, float(y0) / OUTPUT_HEIGHT);
  u = glGetUniformLocation(mOwlProgram, "u_x1");
  glUniform1f(u, float(x1) / OUTPUT_WIDTH);
  u = glGetUniformLocation(mOwlProgram, "u_y1");
  glUniform1f(u, float(y1) / OUTPUT_HEIGHT);

  // set color and transparency
  u = glGetUniformLocation(mOwlProgram, "u_color");
  glUniform3f(u, r, g, b);
  u = glGetUniformLocation(mOwlProgram, "u_alpha");
  glUniform1f(u, alpha);

  // set line width; note the GPU drivers impose a limit
  glLineWidth(lineWidth);

  // draw!
  glDrawArrays(GL_LINES, 10, 2);
  checkGlError("renderLine: ");
  // 10 is offset from quad+rect vertexes; 2 is number of line vertexes
}

/** This function triggers the shader program to render a rectangle. */
void cGpu::renderRectangle(int left, int top, int width, int height,
			   float r, float g, float b, float alpha,
			   float lineWidth, bool filled)
{
  GLint u;

  // set the render value
  u = glGetUniformLocation(mOwlProgram, "u_render");
  glUniform1i(u, RENDER_SHAPE);

  // set the bounding box, converting from pixels to normalized values
  u = glGetUniformLocation(mOwlProgram, "u_x0");
  glUniform1f(u, float(left) / OUTPUT_WIDTH);
  u = glGetUniformLocation(mOwlProgram, "u_y0");
  glUniform1f(u, float(top) / OUTPUT_HEIGHT);
  u = glGetUniformLocation(mOwlProgram, "u_x1");
  glUniform1f(u, float(left + width) / OUTPUT_WIDTH);
  u = glGetUniformLocation(mOwlProgram, "u_y1");
  glUniform1f(u, float(top + height) / OUTPUT_HEIGHT);

  // set color and transparency
  u = glGetUniformLocation(mOwlProgram, "u_color");
  glUniform3f(u, r, g, b);
  u = glGetUniformLocation(mOwlProgram, "u_alpha");
  glUniform1f(u, alpha);

  // draw!
  if (filled)
    {
      glDrawArrays(GL_TRIANGLES, 0, 6);
      // 0 is start of vertex array; 6 is number of quad vertexes
    }
  else
    {
      glLineWidth(lineWidth);
      glDrawArrays(GL_LINE_LOOP, 6, 4);
      // 6 is offset from quad vertexes; 4 is number of rectangle vertexes
    }
    
}


// ----------------------------------------
// panel and RGBA overlay rendering methods
// ----------------------------------------

/** This function renders an RGBA overlay image.
 *
 * @param renderType determines which texture unit is used, the one
 * for the logo or the one for the general-purpose overlay.
 * @param left gives the x-coordinate of the top-left corner of image.
 * @param top gives the y-coordinate of the top-left corner of image.
 * @param width gives the width of the onscreen, rendered image, which
 * can be different from the width of the loaded image.
 * @param height gives the height of the onscreen, rendered image, which
 * can be different from the height of the loaded image.
 * @param alpha is the rendered transparency of the image in [0.,1.].
 */
void cGpu::renderRgba(int renderType, int left, int top,
		      int width, int height, float alpha)
{
  GLint u;

  if (renderType != RENDER_LOGO
      && renderType != RENDER_RGBA_OVERLAY
      && renderType != RENDER_AOI_PIN)
    {
      LOGW("unknown image render type: %d\n", renderType);
      return;
    }

  u = glGetUniformLocation(mOwlProgram, "u_mirrored");
  glUniform1i(u, 0);

  // set the render value
  u = glGetUniformLocation(mOwlProgram, "u_render");
  glUniform1i(u, renderType);

  // set the bounding box, converting from pixels to normalized values
  u = glGetUniformLocation(mOwlProgram, "u_x0");
  glUniform1f(u, float(left) / OUTPUT_WIDTH);
  u = glGetUniformLocation(mOwlProgram, "u_y0");
  glUniform1f(u, float(top) / OUTPUT_HEIGHT);
  u = glGetUniformLocation(mOwlProgram, "u_x1");
  glUniform1f(u, float(left + width) / OUTPUT_WIDTH);
  u = glGetUniformLocation(mOwlProgram, "u_y1");
  glUniform1f(u, float(top + height) / OUTPUT_HEIGHT);

  // set transparency
  u = glGetUniformLocation(mOwlProgram, "u_alpha");
  glUniform1f(u, alpha);

  // draw!
  glDrawArrays(GL_TRIANGLES, 0, 6);
  // 0 is start of vertex array; 6 is number of quad vertexes
}

/** This function renders the general-purpose RGBA overlay.
 * 
 * @param left gives the x-coordinate of the top-left corner of image.
 * @param top gives the y-coordinate of the top-left corner of image.
 * @param width gives the width of the onscreen, rendered image, which
 * can be different from the width of the loaded image.
 * @param height gives the height of the onscreen, rendered image, which
 * can be different from the height of the loaded image.
 * @param alpha is the rendered transparency of the image in [0.,1.].
 */
void cGpu::renderRgbaOverlay(int left, int top, int width, int height, float alpha)
{
  renderRgba(RENDER_RGBA_OVERLAY, left, top, width, height, alpha);
}

/** This function renders the RGBA logo overlay.
 * 
 * @param left gives the x-coordinate of the top-left corner of image.
 * @param top gives the y-coordinate of the top-left corner of image.
 * @param width gives the width of the onscreen, rendered image, which
 * can be different from the width of the loaded image.
 * @param height gives the height of the onscreen, rendered image, which
 * can be different from the height of the loaded image.
 * @param alpha is the rendered transparency of the image in [0.,1.].
 */
void cGpu::renderRgbaLogo(int left, int top, int width, int height, float alpha)
{
  renderRgba(RENDER_LOGO, left, top, width, height, alpha);
}

/** @return the uniform id for the buffer graph shader. */
GLuint cGpu::getGraphColorUniform() const
{
  return mGraphColorUniform;
}

/** This function unrolls a portion of the raw camera image to a
 * rectangular pane.
 *
 * This function can be called multiple times per frame to render
 * multiple panes with different parameter values. The order of the
 * calls determines the layering of the panes, with the first pane
 * drawn on the bottom and the last pane drawn on top.
 *
 * @param left is the x-coordinate of the upper left corner of the pane.
 * @param top is the y-coordinate of the upper left corner of the pane.
 * @param width is the width of the pane.
 * @param height is the height of the pane.
 * @param r0 is the inner radius of the raw camera image.
 * @param r1 is the outer radius of the raw camera image.
 * @param theta0 is the start angle of the raw camera image.
 * @param theta1 is the stop angle of the raw camera image.
 * @param alpha is the overall transparency of the pane.
 * @param stretch is the vertical stretching coefficient (0 == no stretching).
 * @param bend is the vertical bending coefficient (0 == no bending).
 */
void cGpu::renderUnroll(int left, int top, int width, int height,
			float theta0, float theta1, float r0, float r1,
			float alpha, float stretch, float bend)
{
  GLint u;

  // set the render value
  u = glGetUniformLocation(mOwlProgram, "u_render");
  glUniform1i(u, RENDER_UNROLL);

  // set the arc angles
  u = glGetUniformLocation(mOwlProgram, "u_theta0");
  glUniform1f(u, theta0);
  u = glGetUniformLocation(mOwlProgram, "u_theta1");
  glUniform1f(u, theta1);

  // set the radii
  u = glGetUniformLocation(mOwlProgram, "u_r0");
  glUniform1f(u, r0);
  u = glGetUniformLocation(mOwlProgram, "u_r1");
  glUniform1f(u, r1);

  // set the warping coefficients
  u = glGetUniformLocation(mOwlProgram, "u_stretch");
  glUniform1f(u, stretch);
  u = glGetUniformLocation(mOwlProgram, "u_bend");
  glUniform1f(u, bend);

  // set the bounding box, converting from pixels to normalized values
  u = glGetUniformLocation(mOwlProgram, "u_x0");
  glUniform1f(u, float(left) / OUTPUT_WIDTH);
  u = glGetUniformLocation(mOwlProgram, "u_y0");
  glUniform1f(u, float(top) / OUTPUT_HEIGHT);
  u = glGetUniformLocation(mOwlProgram, "u_x1");
  glUniform1f(u, float(left + width) / OUTPUT_WIDTH);
  u = glGetUniformLocation(mOwlProgram, "u_y1");
  glUniform1f(u, float(top + height) / OUTPUT_HEIGHT);

  // set transparency
  u = glGetUniformLocation(mOwlProgram, "u_alpha");
  glUniform1f(u, alpha);

  // draw!
  glDrawArrays(GL_TRIANGLES, 0, 6);
  // 0 is start of vertex array; 6 is number of quad vertexes
}

/** This function enables/disables system test mode.
 *
 * @param mode is 1/0 to enable/disable system test output.
 */
void cGpu::setSystemTestMode(bool testModeOn)
{
  mSystemTestMode = testModeOn;
}

/** This function returns state of system test mode.
 *
 */
bool cGpu::getSystemTestMode(void)
{
  return mSystemTestMode;
}

/** Set flag to save the fisheye image on next received frame.
 *
 */
void cGpu::saveNextFisheye(void)
{
  mSaveFisheye = true;
}
