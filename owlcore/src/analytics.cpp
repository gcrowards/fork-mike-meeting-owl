/*
 * analytics.cpp
 *
 *  Created on: Nov 28, 2016
 *      Author: Tom Bushman
 *
 *  Description:
 *      Class to save run-time analytic data to be used to evaluate the
 *  meeting owl operation at customer sites.
 */


/* *** Includes *** */
#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#include "analytics.hpp"
#include "blackBox.hpp"
#include "common.hpp"
#include "logging.hpp"
#include "shellProcess.hpp"
#include "subframe.hpp"
#ifdef __ANDROID__
#include "UVCHandler.h"
#endif

/* *** Constants *** */

#define JSON_MAX_STRING_SIZE 1024 /**< Maximum allowed JSON data size. */

/* Note: All the below time constants are in seconds. */
const int kMeetingStartDelay = 10;/**< Wait time to confirm meeting start. */
const int kMeetingEndingDelay = 15;/**< Wait time to confirm meeting end. */
const int kMeetingRecordBackupInterval = 12; /**< Time between meeting record
                                                * backups being saved. */
const int kMeetingEndingNoPeopleDelay = (60 * 2);
    /**< Time with no UVC and no people before concluding a meeting. */

const int    kMinimumPersonWidthInDegrees = 30; /**< Angular minimum width of
                                                 * person. */
const double kMinimumPersonProbability    = 0.1;/**< Minimum probability of
                                                 * a person. */
const int kTimeBetweenConnectionStateDisplays = 3; /**< Time between
                                                    * debug messages. */

/* *** Implementation *** */

#ifndef __ANDROID__
/** Stub for libOwlPG function for the Linux build.
 *
 */
void setOwlCoreMeetingInProgress(int inMeeting)
{
  static int previousInMeeting = -1;

  if (previousInMeeting != inMeeting)
    {
      previousInMeeting = inMeeting;
      LOGI("In-meeting state changed to %s\n", (inMeeting ? "true" : "false"));
    }
}
#endif

/** Constructor for an instance of a cAnalytics object
 *
 */
cAnalytics::cAnalytics(cBlackBox *pBlackBox) :
  mpSaveDir(NULL),
  mpBlackBoxJson(NULL),
  mStartTime({0,0}),
  mEndTime({0,0}),
  mCachedMostRecentUpTime(0),
  mPersonCount(0),
  mPersonCountST(0),
  mHistUpdateCount(0),
  mFps(0.0f),
  mCameraTemp(0.0f),
  mCpuTemp(0.0f),
  mDtdTime(0.0f),
  mSamples(0),
  mMeetingState(NOT_MEETING),
  mSaveBackupTimer({0,0}),
  mShowStateTimer({0,0}),
  mMeetingTimer({0,0}),
  mInitialUpTimeMinutes(0),
  mInitialMeetingTimeMinutes(0),
  mLastBootReason(""),
  mCoreFileCount(0),
  mpBlackBox(pBlackBox),
  mSumPinnedAoiSize(0),
  mNumPinnedAoiSizes(0),
  mNPinnedAoiEdits(0),
  mPinnedAoiSeconds(0),
  mPinnedAoiStart({0, 0})
{
  for (int iD = 0; iD < MAX_PINNED_AOIS; iD++)
    {
      mPinnedAoiId[iD] = -1;
    }
  resetPeopleHistogram();
  resetShortTermPeopleHistogram();
  resetTalkTimeStats();
  clock_gettime(CLOCK_MONOTONIC, &mUpTimer);
}

/** Destructor for an instance of a cAnalytics object
 *
 */
cAnalytics::~cAnalytics()
{
  struct timespec tNow;

  clock_gettime(CLOCK_MONOTONIC, &tNow);
  updateBlackBoxTimers(tNow);

  if (mpSaveDir != NULL)
    {
      delete [] mpSaveDir;
    }
  if (mpBlackBoxJson != NULL)
    {
      delete [] mpBlackBoxJson;
    }
}

/** Initializes a cAnalytics object with directory in which to store data.
 *
 * The directory identified by pAnalyticsDir will contain the system-wide black
 * box JSON file as well as any MeetingRecord JSON files that have not yet
 * been pushed to the cloud.
 *
 * @param pAnalyticsDir is a string that identifies the directory where the
 * black box and meeting record files are stored.
 */
void cAnalytics::init(const char *pAnalyticsDir)
{
  mpSaveDir = new char [strlen(pAnalyticsDir)];
  strcpy(mpSaveDir, pAnalyticsDir);

  // Create analytics storage directory, if necessary
  if (access(mpSaveDir, R_OK) != 0)
    {
      mode_t origMask = umask(0);
      mkdirWithParents(mpSaveDir,
                       S_IRWXU | S_IRWXG | S_IRWXO);
      umask(origMask);
    }

  // Initialize some black box data
  initGlobalData();

  // If there's a backup meeting record, Owl must have crashed, so save
  // the backup.
  if (backupMeetingRecordExists())
    {
      LOGI("Promoting saved backup meeting record\n");
      mpBlackBox->incInt(BB_ROOT_STR, BB_N_MEETINGS_STR);
      moveBackupMeetingRecord();
      updateAndSaveBlackBox();
      sync();
    }

  // Update the reboot and application restart counters
  updateProgramCounts();
}

/** Stores the starting time for a meeting.
 *
 * @param startT is a time_t that holds the time at which a meeting started.
 */
void cAnalytics::setStartTime(void)
{
  currentTimeSpec(&mStartTime);
  mAnimModeStartTime = mStartTime;

  // Initialize meeting analytics variables
  mSumPinnedAoiSize = 0;
  mNumPinnedAoiSizes = 0;
  mPinnedAoiSeconds = 0;

  LOGI("setStartTime()\n");

  return;
}

/** Stores the ending time for a meeting.
 *
 * Also increments the meeting number in preparation for the next meeting.
 *
 * @param endT is a time_t that holds the time at which a meeting ended.
 *
 * @return true if valid meeting length, false if too short to count.
 */
bool cAnalytics::setEndTime(eMeetingRecordType type)
{
  uint32_t duration;
  bool validLengthMeeting;

  currentTimeSpec(&mEndTime);
  duration = elapsedTimeSec(mStartTime, mEndTime);
  validLengthMeeting = (duration >= MIN_VALID_MEETING_LENGTH_SECONDS);
  if (type == PUBLISHED_MEETING)
    {  // Only increment meeting number if this is end of meeting, not backup
      LOGI("Meeting duration: %d\n", duration);
      if (validLengthMeeting)
        {
          mpBlackBox->incInt(BB_ROOT_STR, BB_N_MEETINGS_STR);
        }

      setPinnedAoiEndTime(-1);    // Make sure to close off pinned Aois
                                  // but not for backup meeting records

      updateAnimationModeDuration();
    }

  return validLengthMeeting;
}

/** Update analytics measures of animation mode settings.
 *
 */
void cAnalytics::updateAnimationModeDuration(void)
{
  double animModeDurationSec = elapsedTimeSinceSec(mAnimModeStartTime);
  int minutes;
  eAnimationMode_t mode = cSubframe::getAnimationMode();

  currentTimeSpec(&mAnimModeStartTime);

  if (mode == eAnimationMode_t::SLIDING)
    {
      mpBlackBox->getInt(BB_ROOT_STR, BB_SLIDING_MODE_MINUTES, minutes);
      minutes += (int)(animModeDurationSec / 60.0);
      mpBlackBox->setInt(BB_ROOT_STR, BB_SLIDING_MODE_MINUTES, minutes);
    }
  else if (mode == eAnimationMode_t::STRAIGHT_CUT)
    {
      mpBlackBox->getInt(BB_ROOT_STR, BB_CUT_MODE_MINUTES, minutes);
      minutes += (int)(animModeDurationSec / 60.0);
      mpBlackBox->setInt(BB_ROOT_STR, BB_CUT_MODE_MINUTES, minutes);
    }
  std::string modeStr = mode.toString();
  LOGI("Minutes spent in %s animation mode: %d\n", modeStr.c_str(), minutes);
}

/** Clears the histogram of meeting participant positions.
 *
 */
void cAnalytics::resetPeopleHistogram(void)
{
  for (int iI=0; iI<360; iI++)
    {
      mThetaHist[iI] = 0;
      mStdDevSums[iI] = 0.0;
    }
  mHistUpdateCount = 0;
}

/** Clears the short term histogram of meeting participant positions.
 *
 */
void cAnalytics::resetShortTermPeopleHistogram(void)
{
  for (int iI=0; iI<360; iI++)
    {
      mThetaHistST[iI] = 0;
      mStdDevSumsST[iI] = 0.0;
    }
  mHistUpdateCountST = 0;
}

/** Clears talk-time stats data. 
 *
 */
void cAnalytics::resetTalkTimeStats(void)
{
  mLocalTalkTime = 0.f;
  mRemoteTalkTime = 0.f;
  mBothTalkTime = 0.f;
  mNeitherTalkTime = 0.f;

  mDtdTime = 0.0f;
}

/** Updates the histogram of meeting participant positions for every frame.
 *
 * @param pItems is the list of items denoting locations of
 * meeting participants.
 */
void cAnalytics::updatePeopleHistogram(const std::vector<const cItem *> kpItems)
{
  for(std::vector<const cItem *>::const_iterator iItem = kpItems.begin();
      iItem != kpItems.end(); ++iItem )
    {
      int angle = (*iItem)->getThetaMean();
      if ((angle >= 0) && (angle <= 360))
        {
          mThetaHist[angle]++;
          mStdDevSums[angle] += (double)((*iItem)->getThetaStdDev());
          mThetaHistST[angle]++;
          mStdDevSumsST[angle] += (double)((*iItem)->getThetaStdDev());
        }
    }
  mHistUpdateCount++;
  mHistUpdateCountST++;
}

/** Updates talk-time stats data.
 *
 * @param localTime is the Owl-side talk time in seconds since the last update 
 * call.
 * @param remoteTime is the non-Owl-side talk time in seconds since last update 
 * call.
 * @param bothTime is the double-talk time in seconds since last update call.
 * @param neitherTime is the quiet time in seconds since last update call.
 * @param dtdEnabledTime is amount of time that the double talk enabled
 *        feature was active during the report interval.
 */
void cAnalytics::updateTalkTimeStats(float localTime, float remoteTime,
				     float bothTime, float neitherTime,
				     float dtdEnabledTime)
{
  mLocalTalkTime += localTime;
  mRemoteTalkTime += remoteTime;
  mBothTalkTime += bothTime;
  mNeitherTalkTime += neitherTime;
  mDtdTime += dtdEnabledTime;
}

/** Receive the latest estimate of the video output's current frame rate.
 *
 * Note: Pass a value of -1 in the fps argument to initialize the
 * accumulators. This should be done at the beginning of each meeting.
 *
 * @param fps is the "instantaneous" self-measured frame rate of the output
 * video (frames per second).
 * @param cameraTemp is the measured camera temperature.
 * @param cpuTemp is the measured CPU temperature.
 */
void cAnalytics::updateSystemStatistics(float fps, float cameraTemp,
                                        float cpuTemp)
{
  if(fps >= 0)  // negative fps means fps is unknown
    {
       mSamples++;
       mFps = mFps + (fps - mFps) / mSamples;
       mCameraTemp = mCameraTemp + (cameraTemp - mCameraTemp) / mSamples;
       mCpuTemp = mCpuTemp + (cpuTemp - mCpuTemp) / mSamples;
    }
  else
    {
      mFps = 0.0f;
      mCameraTemp = 0.0f;
      mCpuTemp = 0.0f;
      mSamples = 0;
    }
}

/** Generate Gaussian distributions for each histogram bin and sum them.
 *
 * At each location in the output histogram, compute the contribution from
 * every one of the bins in the collected data. Each Gaussian is computed from
 * the collected data as follows: the standard deviation of the curve is
 * mStdDevSums[a] / mThetaHist[a], that is, the average value of the
 * standard deviations of all the Gaussians centered at that angle. The normal
 * distribution is then computed at the offset between the location and angle a,
 * and weighted by the number of times that angle was found as the center,
 * that is, mThetaHist[a].
 */
void cAnalytics::computeFinalMapHistogram(double paOutHist[],
                                          unsigned long paThetaHist[],
                                          double paStdDevSums[],
                                          double histUpdateCount)
{
  int iOutBin;
  int iInBin;
  cItem dI; // dummy cItem just to access getThetaGaussianAt()

  for (iOutBin = 0; iOutBin < 360; iOutBin++)
    {
      paOutHist[iOutBin] = 0.0;
    }

  for (iOutBin = 0; iOutBin < 360; iOutBin++)
    {
      for (iInBin = 0; iInBin < 360; iInBin++)
        {
          float binWeight = (float)paThetaHist[iInBin];
          float stdDevSum = (float)paStdDevSums[iInBin];
          if ((binWeight > 0.0f) && (stdDevSum > 0.0f))
            {
              float stdDev = stdDevSum / binWeight;

              dI.setThetaMean(iInBin);
              dI.setThetaStdDev(stdDev);
              paOutHist[iOutBin] += (double)(dI.getThetaGaussianAt(iOutBin) *
                  binWeight);
            }
        }
      paOutHist[iOutBin] /= (double)histUpdateCount;
    }
}

/** Filter meeting attendee histogram to count actual people.
 *
 * Given histogram, first sum over range of 1/2 minPersonWidth, then find
 * the maximum value of the sums over the range minPersonWidth and store.
 * Finally, at every point in the histogram, if the sum == the max value,
 * store the sum / (1/2 minPersonWidth), otherwise store 0. (This is
 * a non-maximal suppression filter.)
 *
 * @param minPersonWidth is the minimum angular width in the 360 degree
 * panorama scene that is dedicated to a single meeting participant.
 * @param minProb is the probability threshold above which a person is believed
 * to occupy a given angular location in the panorama.
 *
 * @return personCount is the number of people seen in the meeting.
 */
int cAnalytics::extractMeetingPersonCount(int minPersonWidth, double minProb,
                                          double paOutHist[])
{
  int personCount = 0;
  double sumV[360] = {}; // Create 0.0 filled array on the stack
  double maxV[360] = {}; // Same
  int halfWidth = minPersonWidth / 2;
  int quarterWidth = minPersonWidth / 4;

  for (int iA = 0; iA < 360; iA++)
    {
      int iS;
      for (int iB = iA - quarterWidth; iB < iA + quarterWidth; iB++)
        {
          if (iB < 0)        { iS = 360 + iB; }
          else if (iB > 360) { iS = iB - 360; }
          else               { iS = iB; }

          sumV[iA] += paOutHist[iS];
        }
    }

  for (int iA = 0; iA < 360; iA++)
    {
      int iS;
      for (int iB=iA-halfWidth; iB<iA+halfWidth; iB++)
        {
          if (iB < 0)        { iS = 360 + iB; }
          else if (iB > 360) { iS = iB - 360; }
          else               { iS = iB; }

          maxV[iA] = MAX(maxV[iA], sumV[iS]);
        }
    }

  for (int iA = 0; iA < 360; iA++)
    {
      // Keep only those values that are the maximum in their neighborhood
      if (maxV[iA] > sumV[iA])
        {
          maxV[iA] = 0.0;
        }
      if (maxV[iA] <= minProb)
        {
          maxV[iA] = 0.0;
        }
      else
        {
          personCount++;
        }
    }

  if (DEBUG(ANALYTICS))
    {
      saveDArrayToFile("FilteredMaxima.txt", maxV, 360);
    }

  return personCount;
}

/** Save double array to a file in the black box output directory.
 *
 * @param pFileName is the name of the file to save.
 * @param data is the array of double values to save.
 * @param len is the length of the data array.
 */
void cAnalytics::saveDArrayToFile(const char *pFileName, double data[], int len)
{
  FILE *pOutFile;
  int iA;
  std::string fullPath;

  pathify(fullPath, mpSaveDir, pFileName);

  mode_t origMask = umask(0);
  pOutFile = fopen(fullPath.c_str(), "w");
  umask(origMask);

  for (iA = 0; iA < len; iA++)
    {
      fprintf(pOutFile, "%04.4f\n", data[iA]);
    }

  fclose(pOutFile);
}

/** Save the people histogram for the most recent meeting.
 *
 */
void cAnalytics::savePeopleHistogram(void)
{
  if (DEBUG(ANALYTICS))
    {
      saveDArrayToFile("hist.txt", mOutHist, 360);
    }
}

/** Updates mBlackBoxDoc and saves to the black box file.
 *
 * If a black box file exists prior to this call, it is renamed as the
 * backup file. After updating the mBlackBoxDoc object with new data, this
 * routine stringifies the object and saves it to the new black box data file.
 *
 * @return 0 on success, -1 if black box could not be written.
 */
int cAnalytics::updateAndSaveBlackBox(void)
{
  return mpBlackBox->save();
}

/** Save a meeting record to a JSON file.
 *
 * Saves a JSON string containing data about the most recently concluded
 * meeting to a file. The file name includes a numeric ID. Up to 100 of
 * the most recent meetings are stored. If a file name already exists,
 * then the file is overwritten with new data. An external process will
 * transmit these files to a cloud server.
 *
 * @return 0 on success, -1 if the file could not be created.
 */
int cAnalytics::saveMeetingRecord(void)
{
  int returnVal = saveMeetingRecordToFile(PUBLISHED_MEETING);
  deleteBackupMeetingRecord();

  return returnVal;
}

/** Save a meeting record to a JSON file.
 *
 * Saves a JSON string containing data about the most recently concluded
 * meeting to a file. The file name includes a numeric ID. Up to 100 of
 * the most recent meetings are stored. If a file name already exists,
 * then the file is overwritten with new data. An external process will
 * transmit these files to a cloud server.
 *
 * @return 0 on success, -1 if the file could not be created.
 */
int cAnalytics::saveBackupMeetingRecord(void)
{
  return saveMeetingRecordToFile(BACKUP_MEETING);
}

/** Save a meeting record to a JSON file.
 *
 * Saves a JSON string containing latest data about the current
 * meeting to a file. The file name either includes a numeric ID, or is a
 * temporary name (if the meeting is in progress).
 * Up to 100 of  * the most recent meetings are stored. If a file name already
 * exists, then the file is overwritten with new data. An external process will
 * transmit these files to a cloud server.
 *
 * @param fileType is the type of file to save.
 *
 * @return 0 on success, -1 if the file could not be created.
 */
int cAnalytics::saveMeetingRecordToFile(eMeetingRecordType fileType)
{
  std::string fullPath;
  char fileName[PATH_MAX_STRING_SIZE];
  FILE *recordFile = NULL;
  uint32_t meetingDuration = (uint32_t)elapsedTimeSec(mStartTime, mEndTime);
  time_t currentTime = time(NULL);
  uint32_t startTime = 0;
  int nMeetingNumber = 0;

  if (currentTime > meetingDuration)
    { startTime = (uint32_t)currentTime - meetingDuration; }

  mpBlackBox->getInt(BB_ROOT_STR, BB_N_MEETINGS_STR, nMeetingNumber);

  switch (fileType)
  {
    case BACKUP_MEETING:
      sprintf(fileName, MEETING_RECORD_BACKUP_FILE_TEMPLATE);
      break;
    case PUBLISHED_MEETING:
      sprintf(fileName, MEETING_RECORD_FILE_TEMPLATE, nMeetingNumber % 100);
      break;
    default:
      break;
  }

  pathify(fullPath, mpSaveDir, fileName);

  mode_t origMask = umask(0);
  recordFile = fopen(fullPath.c_str(), "w");
  umask(origMask);

  if (recordFile == NULL)
    {
      return -1;
    }
  switch (fileType)
  {
    case BACKUP_MEETING:
      fprintf(recordFile,
              MEETING_RECORD_JSON_TEMPLATE,
              nMeetingNumber + 1,  // If backup used, it will be next meeting #
              startTime,
              meetingDuration,
              mPersonCount,
	      (int)mLocalTalkTime,
	      (int)mRemoteTalkTime,
	      (int)mBothTalkTime,
	      (int)mNeitherTalkTime,
              mFps,
              "true",
              mNPinnedAoiEdits,
              mSumPinnedAoiSize / (MAX(mNumPinnedAoiSizes,1)),
              getCurrentPinnedAoiDuration(),
              mCameraTemp,
              mCpuTemp,
              mDtdTime);
      break;
    case PUBLISHED_MEETING:
      fprintf(recordFile,
              MEETING_RECORD_JSON_TEMPLATE,
              nMeetingNumber,
              startTime,
              meetingDuration,
              mPersonCount,
	      (int)mLocalTalkTime,
	      (int)mRemoteTalkTime,
	      (int)mBothTalkTime,
	      (int)mNeitherTalkTime,
              mFps,
              "false",
              mNPinnedAoiEdits,
              mSumPinnedAoiSize / (MAX(mNumPinnedAoiSizes,1)),
              mPinnedAoiSeconds,
              mCameraTemp,
              mCpuTemp,
              mDtdTime);
      break;
    default:
      break;
  }
  fprintf(recordFile, "\n");
  fclose(recordFile);

  return 0;
}

/** Move a backup meeting record to publish-able JSON file.
 *
 * @return 0 on success, -1 if the file could not be created.
 */
int cAnalytics::moveBackupMeetingRecord()
{
  std::string fullPath;
  std::string backupFullPath;
  char fileName[PATH_MAX_STRING_SIZE];
  char backupFileName[PATH_MAX_STRING_SIZE];
  int returnVal;
  int nMeetingNumber;

  mpBlackBox->getInt(BB_ROOT_STR, BB_N_MEETINGS_STR, nMeetingNumber);

  sprintf(backupFileName, MEETING_RECORD_BACKUP_FILE_TEMPLATE);
  pathify(backupFullPath, mpSaveDir, backupFileName);

  sprintf(fileName, MEETING_RECORD_FILE_TEMPLATE, nMeetingNumber % 100);
  pathify(fullPath, mpSaveDir, fileName);

  mode_t origMask = umask(0);
  returnVal =  rename(backupFullPath.c_str(), fullPath.c_str());
  umask(origMask);

  return returnVal;
}

/** Delete a backup meeting record file.
 *
 * @return 0 on success, -1 if the file could not be removed.
 */
int cAnalytics::deleteBackupMeetingRecord()
{
  std::string backupFullPath;
  char backupFileName[PATH_MAX_STRING_SIZE];

  sprintf(backupFileName, MEETING_RECORD_BACKUP_FILE_TEMPLATE);
  pathify(backupFullPath, mpSaveDir, backupFileName);

  return unlink(backupFullPath.c_str());
}

/** Report existence of a backup meeting record file.
 *
 * @return true if file exists, false if not.
 */
bool cAnalytics::backupMeetingRecordExists()
{
  std::string backupFullPath;
  char backupFileName[PATH_MAX_STRING_SIZE];

  sprintf(backupFileName, MEETING_RECORD_BACKUP_FILE_TEMPLATE);
  pathify(backupFullPath, mpSaveDir, backupFileName);

  return (access(backupFullPath.c_str(), R_OK) == 0);
}

/** Saves black box and meeting record files at the end of a meeting.
 *
 * @return 0 if both operations succeed. Non-zero indicates that one failed or
 * the meeting was too short to record data.
 */
int cAnalytics::save(void)
{
  int rc;
  int duration = (int)elapsedTimeSec(mStartTime, mEndTime);

  if (duration > MIN_VALID_MEETING_LENGTH_SECONDS)
    {
      computeFinalMapHistogram(mOutHist, mThetaHist,
                               mStdDevSums, mHistUpdateCount);

      mPersonCount = extractMeetingPersonCount(
                                  kMinimumPersonWidthInDegrees,
                                  kMinimumPersonProbability,
                                  mOutHist);
      LOGI("Meeting had %d attendees\n", mPersonCount);

      rc = updateAndSaveBlackBox();

      rc |= saveMeetingRecord();

      savePeopleHistogram();

      sync();

      resetPeopleHistogram();
      resetTalkTimeStats();
    }
  else
    {
      rc = -1;
    }
  return rc;
}

/** Reads previous state of global data from black box to start running.
 *
 * @return 0 if all went well, -1 if failure.
 */
int cAnalytics::initGlobalData(void)
{
  int rc = 0;
  int nUpTimeMinutes = 0;
  int nMeetingMinutes = 0;
  int nUpTimeSinceBoot = 0;

  /* Store initial up-time and meeting minutes values for subsequent
   * duration based updates as time passes. This is done to avoid losing
   * time from rounding losses when converting from fractional seconds to
   * minutes.
   */
  // cache the starting cumulative up-time
  mpBlackBox->getInt(BB_ROOT_STR, BB_UP_TIME_MIN_STR, nUpTimeMinutes);
  mInitialUpTimeMinutes = nUpTimeMinutes;

  // cache the starting meeting minutes time
  mpBlackBox->getInt(BB_ROOT_STR, BB_MEETING_MIN_STR, nMeetingMinutes);
  mInitialMeetingTimeMinutes = nMeetingMinutes;

  // cache the previous up-time since rebooting value and re-initialize to 0
  mpBlackBox->getInt(BB_ROOT_STR, BB_UP_TIME_SINCE_BOOT_MIN_STR, nUpTimeSinceBoot);
  mCachedMostRecentUpTime = nUpTimeSinceBoot;
  nUpTimeSinceBoot = 0;
  mpBlackBox->setInt(BB_ROOT_STR, BB_UP_TIME_SINCE_BOOT_MIN_STR, nUpTimeSinceBoot);

  return rc;
}

/** Setter for the analytics callback function to call to change meeting
 * state.
 */
void cAnalytics::setAnalyticsCallback(analyticsCallback_t analyticsCallback)
{
  mAnalyticsCallback = analyticsCallback;
}


/** This function implements a state machine for
 * managing meeting state. The state machine is required to handle
 * delaying during state transitions to make sure the expected
 * transition is really sticking before committing to making the
 * transition into the new state.
 */
void cAnalytics::updateMeetingState(int usbConnected,
                                    int uvcStreaming,
                                    int usbSpeakerConnected,
                                    int usbMicConnected,
                                    int usbStateFixup)
{
  double elapsedTime = 0.0;
  struct timespec tNow;

  // Log when USB connection event is missed.
  mpBlackBox->incInt(BB_ROOT_STR, BB_N_USB_STATE_FIXUPS_STR, usbStateFixup);
  if (usbStateFixup != 0)
    {
      updateAndSaveBlackBox();
    }

  clock_gettime(CLOCK_MONOTONIC, &tNow);

  switch (mMeetingState)
  {
    case eMeetingState::NOT_MEETING:
      // State change:
      if (uvcStreaming && usbConnected)
        { // If USB connected, and UVC stream starts, consider starting meeting
          mTransitionTimer = tNow;
          LOGI("mMeetingState -> MEETING_STARTING: usb=%d, uvc=%d, mic=%d, spkr=%d\n",
               usbConnected, uvcStreaming,
               usbMicConnected, usbSpeakerConnected);
          mMeetingState = MEETING_STARTING;
          if(mAnalyticsCallback)
            { mAnalyticsCallback(false, true); }
        }
      break;

    case eMeetingState::MEETING_STARTING:
      // Abort state change:
      if (!uvcStreaming || !usbConnected)
        { // If UVC stream turn off, then forget it, not a meeting
          LOGI("mMeetingState -> NOT_MEETING: usb=%d, uvc=%d, mic=%d, spkr=%d\n",
               usbConnected, uvcStreaming,
               usbMicConnected, usbSpeakerConnected);
          mMeetingState = NOT_MEETING; // False start
        }

      // State change:
      elapsedTime = elapsedTimeSec(mTransitionTimer, tNow);
      if ((int)elapsedTime >= kMeetingStartDelay)
        { // Time has gone by and connection is sticking, so call it a meeting!
          LOGI("mMeetingState -> MEETING: usb=%d, uvc=%d, mic=%d, spkr=%d\n",
               usbConnected, uvcStreaming,
               usbMicConnected, usbSpeakerConnected);
          mMeetingState = MEETING;
          mSaveBackupTimer = tNow;
          mPersonCount = 0;
          mpBlackBox->getInt(BB_ROOT_STR, BB_MEETING_MIN_STR, mInitialMeetingTimeMinutes);
          mMeetingTimer = tNow;
          if(mAnalyticsCallback)
            { mAnalyticsCallback(true, true); }
        }
      break;

    case eMeetingState::MEETING:
      // State change 1:
      if ((!uvcStreaming && !usbSpeakerConnected && !usbMicConnected)
          || !usbConnected)
        { // All streams have stopped, or USB disconnected, consider ending
          mTransitionTimer = tNow;
          LOGI("mMeetingState -> MEETING_ENDING: usb=%d, uvc=%d, mic=%d, spkr=%d\n",
               usbConnected, uvcStreaming,
               usbMicConnected, usbSpeakerConnected);
          mMeetingState = MEETING_ENDING;
        }

      // State change 2:
      else if (!uvcStreaming && (mPersonCountST == 0))
        { // UVC video stream is stopped, and no people are seen
          mTransitionTimer = tNow;
           LOGI("mMeetingState -> MEETING_ENDING_NO_PEOPLE: usb=%d, uvc=%d, mic=%d, spkr=%d\n",
                usbConnected, uvcStreaming,
                usbMicConnected, usbSpeakerConnected);
           mMeetingState = MEETING_ENDING_NO_PEOPLE;
        }
      break;

    case eMeetingState::MEETING_ENDING:
      // Abort state change:
      if (uvcStreaming || usbSpeakerConnected || usbMicConnected)
        { // A stream restarted, so don't bail out of the meeting yet
          LOGI("mMeetingState -> MEETING: usb=%d, uvc=%d, mic=%d, spkr=%d\n",
               usbConnected, uvcStreaming,
               usbMicConnected, usbSpeakerConnected);
          mMeetingState = MEETING; // False ending
        }

      // State change:
      elapsedTime = elapsedTimeSec(mTransitionTimer, tNow);
      if ((int)elapsedTime >= kMeetingEndingDelay)
        { // Time has gone by and disconnect persists, so cancel the meeting!
          LOGI("mMeetingState -> NOT_MEETING: usb=%d, uvc=%d, mic=%d, spkr=%d\n",
               usbConnected, uvcStreaming,
               usbMicConnected, usbSpeakerConnected);
          updateBlackBoxTimers(tNow); /* Call *before* mMeetingState changes */
          mMeetingState = NOT_MEETING;
          if(mAnalyticsCallback)
            { mAnalyticsCallback(false, false); }
        }
      break;

    case eMeetingState::MEETING_ENDING_NO_PEOPLE:
      // Abort state change:
      if (uvcStreaming || (mPersonCountST > 0))
        { // UVC video stream restarted, or somebody showed up
          LOGI("mMeetingState -> MEETING: usb=%d, uvc=%d, mic=%d, spkr=%d\n",
               usbConnected, uvcStreaming,
               usbMicConnected, usbSpeakerConnected);
          mMeetingState = MEETING; // False ending
        }

      // State change 1:
      if ((!uvcStreaming && !usbSpeakerConnected && !usbMicConnected)
          || !usbConnected)
        { // All streams have stopped, or USB disconnected, consider ending
          mTransitionTimer = tNow;
          LOGI("mMeetingState -> MEETING_ENDING: usb=%d, uvc=%d, mic=%d, spkr=%d\n",
               usbConnected, uvcStreaming,
               usbMicConnected, usbSpeakerConnected);
          mMeetingState = MEETING_ENDING;
        }

      // State change 2:
      elapsedTime = elapsedTimeSec(mTransitionTimer, tNow);
      if ((int)elapsedTime >= kMeetingEndingNoPeopleDelay)
        { // Time has gone by and nobody showed up, so cancel the meeting!
          LOGI("mMeetingState -> NOT_MEETING: usb=%d, uvc=%d, mic=%d, spkr=%d\n",
               usbConnected, uvcStreaming,
               usbMicConnected, usbSpeakerConnected);
          updateBlackBoxTimers(tNow); /* Call *before* mMeetingState changes */
          mMeetingState = NOT_MEETING;
          if(mAnalyticsCallback)
            { mAnalyticsCallback(false, false); }
        }
      break;

    default:
      LOGE("Oops, bad mMeetingState (%d), mMeetingState -> NOT_MEETING!\n",
           mMeetingState);
      mMeetingState = NOT_MEETING;
      break;
  }

  // Update the meeting-in-progress flag in libOwlPG
  if (mMeetingState == NOT_MEETING)
    {
      setOwlCoreMeetingInProgress((int)false);
    }
  else
    {
      setOwlCoreMeetingInProgress((int)true);
    }

  // If in meeting, save temporary meeting record periodically
  if ((mMeetingState == MEETING) || (mMeetingState == MEETING_ENDING_NO_PEOPLE))
    {
      elapsedTime = elapsedTimeSec(mSaveBackupTimer, tNow);

      if ((int)elapsedTime >= kMeetingRecordBackupInterval)
        {
          mSaveBackupTimer = tNow;  // Reset backup interval timer
          setEndTime(BACKUP_MEETING);
          computeFinalMapHistogram(mOutHist, mThetaHist,
                                   mStdDevSums, mHistUpdateCount);
          mPersonCount = extractMeetingPersonCount(
                                      kMinimumPersonWidthInDegrees,
                                      kMinimumPersonProbability,
                                      mOutHist);
          saveBackupMeetingRecord();
          sync();
          computeFinalMapHistogram(mOutHistST, mThetaHistST,
                                   mStdDevSumsST, mHistUpdateCountST);
          mPersonCountST = extractMeetingPersonCount(
                                      kMinimumPersonWidthInDegrees,
                                      kMinimumPersonProbability,
                                      mOutHistST);
          if (DEBUG(ANALYTICS))
            {
              saveDArrayToFile("histST.txt", mOutHistST, 360);
            }
          resetShortTermPeopleHistogram();
          LOGI("Saved backup meeting record (%d short term persons)\n",
               mPersonCountST);
        }

    }

  if (timeToUpdateBlackBox(tNow))
    {
      updateBlackBoxTimers(tNow);
    }

  // Show state of connection periodically, if configured to do so
  //  (set kTimeBetweenConnectionStateDisplays = 0 to disable)
  elapsedTime = elapsedTimeSec(mShowStateTimer, tNow);
  uint16_t aggregateState = !!usbConnected |
                            (!!uvcStreaming << 1) |
                            (!!usbMicConnected << 2) |
                            (!!usbSpeakerConnected << 3);
  static uint16_t previousAggregateState = 0;
  if ((kTimeBetweenConnectionStateDisplays > 0) &&
      ((int)elapsedTime > kTimeBetweenConnectionStateDisplays) &&
      (aggregateState != previousAggregateState))
    {
      mShowStateTimer = tNow;
      previousAggregateState = aggregateState;
      LOGI("Connectivity state: usb=%d, uvc=%d, mic=%d, spkr=%d\n",
           usbConnected, uvcStreaming, usbMicConnected, usbSpeakerConnected);
    }
}

/** Return the current meeting state.
 *
 * @return eMeetingState - the current state of the system.
 */
cAnalytics::eMeetingState cAnalytics::getMeetingState(void)
{
  return mMeetingState;
}

/** Return true if it is time to update the black box.
 *
 * If enough time has passed sine the last time the black box was updated,
 * this returns true to indicate another update should be done. This is
 * specifically to make sure run time statistics are faithfully recorded.
 *
 * @param tNow Contains the current time.
 */
bool cAnalytics::timeToUpdateBlackBox(struct timespec tNow)
{
  bool doUpdate = false;
  static int updateCounter = 1;
  double elapsedTime = elapsedTimeSec(mUpTimer, tNow);

  if (elapsedTime > (double)(updateCounter * (BB_SAVE_INTERVAL_MINUTES * 60)))
    {
      doUpdate = true;
      updateCounter++;
    }

  return doUpdate;
}

/** Update up-time and meeting elapsed times and save in the black box.
 *
 * @param tNow Contains the current time.
 */
void cAnalytics::updateBlackBoxTimers(struct timespec tNow)
{
  double elapsedTime;
  int nUpTimeMinutes;
  int nMeetingMinutes;
  int nUpTimeSinceBoot;

  elapsedTime = elapsedTimeSec(mUpTimer, tNow);
  nUpTimeMinutes = mInitialUpTimeMinutes + (elapsedTime / 60);
  mpBlackBox->setInt(BB_ROOT_STR, BB_UP_TIME_MIN_STR, nUpTimeMinutes);

  nUpTimeSinceBoot = elapsedTime / 60;
  mpBlackBox->setInt(BB_ROOT_STR, BB_UP_TIME_SINCE_BOOT_MIN_STR, nUpTimeSinceBoot);

  if ((mMeetingState == MEETING)
      || (mMeetingState == MEETING_ENDING_NO_PEOPLE)
      || (mMeetingState == MEETING_ENDING))
    {
      elapsedTime = elapsedTimeSec(mMeetingTimer, tNow);
      nMeetingMinutes = mInitialMeetingTimeMinutes + (elapsedTime / 60);
      mpBlackBox->setInt(BB_ROOT_STR, BB_MEETING_MIN_STR, nMeetingMinutes);
    }

  updateAndSaveBlackBox();
}

/** Update reboot and program start counts and save in the back box.
 *
 * This should only be called once at the beginning of running the program.
 */
void cAnalytics::updateProgramCounts(void)
{
  int startCount = 0;
  FILE *pFile;
  std::string powerOffMessage;
  std::string startCountFileName = std::string(OWLLABS_DATA_DIRECTORY);
  startCountFileName.append("/");
  startCountFileName.append(PROGRAM_EVENTS_COUNT_FILE);

  if (access(startCountFileName.c_str(), R_OK | W_OK) == 0)
    {
      pFile = fopen(startCountFileName.c_str(), "r+");
      if (fscanf(pFile, "%d", &startCount) != 1)
        {
          LOGI("updateProgramCounts: Warning, malformed '%s' file\n",
               PROGRAM_EVENTS_COUNT_FILE);
        }
    }
  else
    {
      mode_t origMask = umask(0);   // Create file with correct permissions
      pFile = fopen(startCountFileName.c_str(), "w");
      umask(origMask);
    }
  if (startCount == 0)
    {  // Fresh start after a reboot, so increment appropriate boot counter
      if (isPowerOnBoot(powerOffMessage))
        {
          mpBlackBox->incInt(BB_ROOT_STR, BB_N_POWER_UP_STR);
        }
      else
        {
          mpBlackBox->incInt(BB_ROOT_STR, BB_N_CRASH_BOOT_STR);
          // Remember last non-power-off boot reason
          mpBlackBox->setString(BB_ROOT_STR, BB_BAD_BOOT_STR, powerOffMessage);
        }
      // Always remember last boot power-off reason
      mLastBootReason = powerOffMessage;
      // Store file containing startup message
      saveStartupMessage();
    }
  startCount++;
  mpBlackBox->incInt(BB_ROOT_STR, BB_N_APP_STARTS_STR);

  // Save the new start count back to the file
  if (pFile != NULL)
    {
      rewind(pFile);
      fprintf(pFile, "%d", startCount);
      fclose(pFile);
      sync();
    }

  updateAndSaveBlackBox();
}

/** Return true if most recent boot resulted from powering up (not crash).
 *
 * @param powerOffReason String describing the power off reason.
 */
bool cAnalytics::isPowerOnBoot(std::string &powerOffReason)
{
  std::string message = "";
  bool powerOnBoot = false;

  /* Create a new process to run the 'dmesg' command, and then filter the
   * output looking for lines containing "Power-off reason:". If the reason
   * contains the substring "Triggered from UVLO", then that indicates that
   * the last time the OS went down was due to loss of power. Any other
   * cause is considered to be a crash. The appropriate counter is
   * incremented depending on the type, and in the case of a crash, the
   * power-off reason string is stored.
   */
  cShellProcess findPowerOffMsg(std::string("dmesg|grep \"Power-off reason:\""),
  /* The following lambda function tokenizes each line returned by the
   * 'dmesg' system call, then finds the line containing the 'Power-off
   * reason', then puts the subsequent message into the string pMsg.
   */
  [&](const std::string *pLine)
  {
    if (pLine != nullptr)
      {
        // Note: this overwrites with each new match to return the LAST one!
        message = *pLine;
        return true; }
    else
      { return false; }
  },
  true );

  if (message.length() > 0)
    {
      if (message.find("Triggered from UVLO") != std::string::npos)
        {
          powerOnBoot = true;
        }
      powerOffReason = message.substr(message.find("Power-off reason:"),
                                  std::string::npos);
      powerOffReason.erase(powerOffReason.end() - 1); // Erase trailing \n
    }
  else
    {
      powerOffReason = "Unknown";
    }
  LOGI("isPowerOnBoot: %s, %s\n",
       ((powerOnBoot) ? "true" : "false"), powerOffReason.c_str());

  return powerOnBoot;
}

/** Save power-off reason message from most recent reboot.
 *
 */
void cAnalytics::saveStartupMessage(void)
{
  FILE *pFile;
  char startupRecordFileName[PATH_MAX_STRING_SIZE];
  std::string curDate = currentDateTime();
  int pathLen;

  pathLen = snprintf(startupRecordFileName, PATH_MAX_STRING_SIZE,
                     STARTUP_RECORD_FILE_TEMPLATE, ANALYTICS_DATA_DIRECTORY,
                     curDate.c_str());
  if (pathLen < PATH_MAX_STRING_SIZE)
    {
      int nDate;
      int nTime;
      char dateTime[256];

      sscanf(curDate.c_str(), "%d-%d", &nDate, &nTime);
      while (access(startupRecordFileName, R_OK) == 0)
        {
          // File already exists, increment time until unique name found.
          // This only happens when WiFi is not connected so time is unknown.
          LOGI("saveStartupMessage(): Fixing duplicate startup record name.\n");
          nTime++;
          snprintf(dateTime, sizeof(dateTime), "%d-%d", nDate, nTime);
          snprintf(startupRecordFileName, PATH_MAX_STRING_SIZE,
                   STARTUP_RECORD_FILE_TEMPLATE, ANALYTICS_DATA_DIRECTORY,
                   dateTime);
        }

      mode_t origMask = umask(0);
      pFile = fopen(startupRecordFileName, "w");
      umask(origMask);
      if (pFile != NULL)
        {
          fprintf(pFile, STARTUP_RECORD_JSON_TEMPLATE,
                  mLastBootReason.c_str(),
                  mCachedMostRecentUpTime,
                  countCoreFiles(),
                  (int)time(NULL));
          fclose(pFile);
        }
      else
        {
          LOGI("saveStartupMessage: Could not open file %s\n",
               startupRecordFileName);
        }
    }
  else
    {
      LOGI("saveStartupMessage: Path name too long for buffer\n");
    }
}

/** Return count of files in the OwlLabs data directory that start with core_
 *
 */
int cAnalytics::countCoreFiles(void)
{
  char lsCmd[512];
  int coreFileCount = 0;

  snprintf(lsCmd, sizeof(lsCmd), "ls %s/core_*", OWLLABS_DATA_DIRECTORY);
  /* Create a new process to run the 'ls' command looking for all files
   * that match the 'core_' pattern. For each non-empty line in the
   * resulting output, increment the file count.
   */
  cShellProcess findCoreFiles(std::string(lsCmd),
  /* The following lambda function just increments a counter for each line
   * of output with which it is called.
   */
  [&](const std::string *pLine)
  {
    if (pLine != nullptr)
      {
        coreFileCount++;
        return true;
      }
    else
      { return false; }
  },
  true );

  mCoreFileCount = coreFileCount;

  return coreFileCount;
}

/**
 *
 */
void cAnalytics::setPinnedAoiStartTime(int aoiId, int widthDegrees)
{
  bool found = false;
  int  firstEmpty = -1;
  int  emptyCount = 0;

  // First see if this ID is already in use
  for (int iD = 0; iD < MAX_PINNED_AOIS; iD++)
    {
      if (mPinnedAoiId[iD] == aoiId)
        {
          // Found! Timer already running, must be re-editing this Aoi
          // Inc MeetingRecord pinnedAoi edit count
          found = true;
          mNPinnedAoiEdits++;
          mSumPinnedAoiSize += widthDegrees;
          mNumPinnedAoiSizes++;
        }
      else if ((firstEmpty == -1) && (mPinnedAoiId[iD] == -1))
        {
          // Found empty slot, in case it is needed for this id
          firstEmpty = iD;
        }
      if (mPinnedAoiId[iD] == -1)
        {
          emptyCount++;
        }
    }

  if (!found)
    {
      if (emptyCount == MAX_PINNED_AOIS)
        {
          // First pinned Aoi defined, so start timer
          clock_gettime(CLOCK_MONOTONIC, &mPinnedAoiStart);
        }
      if (firstEmpty >= 0)
        {
          // Put this new Aoi in the first empty slot
          mPinnedAoiId[firstEmpty] = aoiId;
          mNPinnedAoiEdits++;
          mSumPinnedAoiSize += widthDegrees;
          mNumPinnedAoiSizes++;
          LOGI("setPinnedAoiStartTime(): aveSize=%d, numEdits=%d\n",
               mSumPinnedAoiSize / mNumPinnedAoiSizes, mNPinnedAoiEdits);
        }
      else if (emptyCount == 0)
        { LOGE("setPinnedAoiStartTime(): Aoi list overflow\n"); }
    }
}

/**
 *
 */
void cAnalytics::setPinnedAoiEndTime(int aoiId)
{
  struct timespec tNow;
  int pinnedAoiCount = 0;
  int pinnedAoiDeletedCount = 0;

  // End all defined pinned Aois that match aoiId
  for (int iD = 0; iD < MAX_PINNED_AOIS; iD++)
    {
      // Count number of pinned Aois on the way into the function
      if (mPinnedAoiId[iD] > -1)
        { pinnedAoiCount++; }

      if (((aoiId == -1) && (mPinnedAoiId[iD] > -1)) ||
          ((aoiId >= 0) && (mPinnedAoiId[iD] == aoiId)))
        {
          // Collect stats and free up the slot
          mpBlackBox->incInt(BB_ROOT_STR, BB_N_PINNED_AOIS);
          LOGI("setPinnedAoiEndTime(): Inc pinned Aoi Counts\n");

          mPinnedAoiId[iD] = -1;

          // Count the number of pinned Aois that are deleted
          pinnedAoiDeletedCount++;
        }
    }

  // If there were pinned Aois, but all deleted, update elapsed times
  if ((pinnedAoiCount > 0) &&
      (pinnedAoiCount == pinnedAoiDeletedCount))
    {
      clock_gettime(CLOCK_MONOTONIC, &tNow);

      double elapsedMins = elapsedTimeSec(mPinnedAoiStart, tNow) / 60.0;
      mpBlackBox->incInt(BB_ROOT_STR, BB_PINNED_AOI_MINUTES,
                         (int)(elapsedMins + 0.5));

      // Update meeting record fields
      mPinnedAoiSeconds += (int)(elapsedMins * 60.0 + 0.5);

      LOGI("setPinnedAoiEndTime(): Adding %d pinned Aoi seconds\n",
           mPinnedAoiSeconds);
    }
  mpBlackBox->save();
}

/** Returns true if any pinned Aoi is currently defined in the system.
 *
 */
bool cAnalytics::isAnyAoiPinned(void)
{
  bool pinnedAoiFound = false;

  for (int iD = 0; iD < MAX_PINNED_AOIS; iD++)
    {
      if (mPinnedAoiId[iD] > -1)
        {
          pinnedAoiFound = true;
        }
    }
  return pinnedAoiFound;
}

/** Return time since Aoi was pinned, 0 if no pinned Aois.
 *
 * Note: This function does not give correct results if the system
 * clock time changes between the pinned Aoi start time and the time
 * it is invoked. The assumption is that an Aoi cannot be pinned without
 * a) App connectivity to the Owl, and therefore b) WiFi connectivity.
 * If a WiFi connection is available, then the Owl will have a valid
 * time, and it will set its time long before a user could pin an Aoi.
 */
int cAnalytics::getCurrentPinnedAoiDuration(void)
{
  struct timespec tNow;
  int elapsedSecs = 0;

  if (isAnyAoiPinned())
    {
      clock_gettime(CLOCK_MONOTONIC, &tNow);

      elapsedSecs = (int)(elapsedTimeSec(mPinnedAoiStart, tNow) + 0.5);
    }

  return elapsedSecs + mPinnedAoiSeconds;
}

/** Accessor for getting a pointer to the black box object. */
cBlackBox *cAnalytics::getBlackBox(void)
{
  return mpBlackBox;
}


