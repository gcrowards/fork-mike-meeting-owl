///< AudioPlayer - manages playing audio through the speaker 

#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>

#include "common.hpp"
#include "logging.hpp"
#include "params.h"
#include "AudioPlayer.hpp"

// wav file structures; Borrowed from Tinyplay in tinyalsa.c

#define ID_RIFF 0x46464952
#define ID_WAVE 0x45564157
#define ID_FMT  0x20746d66
#define ID_DATA 0x61746164

struct riff_wave_header {
    uint32_t riff_id;
    uint32_t riff_sz;
    uint32_t wave_id;
};

struct chunk_header {
    uint32_t id;
    uint32_t sz;
};

struct chunk_fmt {
    uint16_t audio_format;
    uint16_t num_channels;
    uint32_t sample_rate;
    uint32_t byte_rate;
    uint16_t block_align;
    uint16_t bits_per_sample;
};


/** test callback 
*/
int testPlaybackCB(int result)
{
  printf("Test playback complete Callback %d \n", result);
  return result;
}
 
/** thread to playback the raw file - just calls into the object
* 
* @param - pointer the object that create the thread 
*
* @return - void 
*/
static void *rawPlaybackShim(void *pPlayer)
{
#ifdef __ANDROID__  
  cAudioPlayer *thisPlayer;
  thisPlayer = (cAudioPlayer *)pPlayer;
  
  thisPlayer->rawPlayback();
#endif  

  return NULL;

}

// object version of the playback code.

void cAudioPlayer::rawPlayback()
{
#ifdef __ANDROID__  
  char *buffer; 
  int playbackSize = FILE_PLAY_CB_SIZE/8;
  int sleepTime;
  int readAmount;
  int stopFlag;

  // sleetp time  is approximately the amount of time to play a buffer
  sleepTime = ((playbackSize/ (SAMPLE_SIZE)*1000000)/DEFAULT_SAMPLE_RATE);

  // get a circular buffer to send audio to playback engine
  mpFilePlaybackCB = GetPlayCB();
  if(mpFilePlaybackCB == NULL)
  {
    if(AUDIOPLAYER_DEBUG)
    {printf("ERROR - cAudioPlayer::rawPlayback can't get circular buffer\n"); } 
    return;
  }
      
  buffer = (char *)malloc(playbackSize*2);
  if(buffer == NULL)
  {
    FreePlayCB(mpFilePlaybackCB);
    return;
  }

  // feed the audio to circular buffer    
  while(((readAmount = fread(buffer,1,playbackSize,mPlaybackFile)) != 0) &&
      ((stopFlag = getStopFlag()) == 0))
  {
    while(CircBuffEmpty(mpFilePlaybackCB) < readAmount*2)
    {
      usleep(sleepTime);
    }
    // Spread the one channel over 2
    short *inPtr;
    short *outPtr;
    int outIndex; 
    int index;
    
    int i;
    inPtr = (short *)buffer;
    outPtr = (short *) buffer;
    outIndex = readAmount-1;
    index = readAmount/2-1;
    
    for(i = 0; i < readAmount/2; ++i)
    {
      outPtr[outIndex] = inPtr[index];
      outPtr[outIndex-1] = inPtr[index];      
      index--;
      outIndex -= 2;
    }
    CircBuffWrite(mpFilePlaybackCB,buffer,readAmount*2);
  }
  
  if(stopFlag == 0)
  {
    // read all of the data;

    playbackSize = CircBuffFilled(mpFilePlaybackCB);
    while(CircBuffFilled(mpFilePlaybackCB) > 0)
    {
      usleep(sleepTime);
    }
    if(AUDIOPLAYER_DEBUG)
    {printf("Playback completed \n");}
  }
  else
  {
    if(AUDIOPLAYER_DEBUG)  
    {printf("Playback stopped \n");  }
    CircBuffRestart(mpFilePlaybackCB);
  }
  
  fclose(mPlaybackFile);
  free(buffer);
  FreePlayCB(mpFilePlaybackCB);
  if(mPlaybackCallback != NULL)
  {
    mPlaybackCallback(stopFlag);
  }
  
  mPlaybackFile = NULL;
#endif  
  return;
}

// constructor for audioPlayer objects
cAudioPlayer::cAudioPlayer()
{
#ifdef __ANDROID__  
  pthread_mutex_init(&mPlayerMutex, NULL);
  mPlaybackFile = NULL;
  mPlaybackCallback = NULL;
#endif  

}

// Destructor for audioPlayer Object

cAudioPlayer::~cAudioPlayer()
{
#ifdef __ANDROID__  
  stopPlayback();
#endif  
}

/** stops playback of currently playing player object
 *
 * This method will stop the wav file playback immediately. Since the playback
 * occurs in a separate thread, this method will wait for completion of the 
 * stopping before returning. This can be in the 100ms range.
 */
void cAudioPlayer::stopPlayback()
{
#ifdef __ANDROID__
  pthread_mutex_lock(&mPlayerMutex);
  mStopFlag = 1;
  pthread_mutex_unlock(&mPlayerMutex);
  pthread_join(mPlaybackThread,NULL);
#endif  
}

/** waits for playback to complete 
 *
 * This method will cause the calling thread to be suspended until the wav file
 * playback is complete.
 */
void cAudioPlayer::waitPlayback()
{
#ifdef __ANDROID__
  pthread_join(mPlaybackThread,NULL);
#endif  
}

/** gets the current statue of the stopFlag
 *
 * @return state of the stopFlag;
 */
int cAudioPlayer::getStopFlag()
{
#ifdef __ANDROID__
  int tempFlag;
  pthread_mutex_lock(&mPlayerMutex);
  tempFlag = mStopFlag;
  pthread_mutex_unlock(&mPlayerMutex);
  return tempFlag;
#else
  return 1;
#endif
}



/** Plays the data from a raw file. It assumes that the data is in 1 channel,
* 16bit, 48khz sample rate. The playback is asynch.
*
* This function will initiate playback of a header less file. It as the same 
* implementation as playWavFile. It is aimed at primarily debugging.
*
* @param filename - Name of the file to play
* @param playbackCallback This function is called when the playback is 
* complete. It is called in the context of a worker thread so take appropriate
* care. The prototype for the call back is:
* typedef int (*playbackCompleteCB)(int result);
*
* @return 0 - if successfully starts playback; -1 if not 
*/
int cAudioPlayer::playRawFile(const char *filename, 
			      playbackCompleteCB playbackCallback )
{
#ifdef __ANDROID__
  int result; 

  if(mPlaybackFile != NULL)
  {
    if(AUDIOPLAYER_DEBUG)
    {printf("ERROR - Already Playing file %p \n",mPlaybackFile);}
    return -1;
  }
  mPlaybackCallback = playbackCallback;  
  
  mPlaybackFile = fopen(filename,"rb");
  if(mPlaybackFile == NULL)
  {
    if(AUDIOPLAYER_DEBUG)
    {printf("ERROR - cAudioPlayer::playRawFile could not open file %s \n",filename);}
    return -1;
  } 
  
  mStopFlag = 0;
  
  result = pthread_create(&mPlaybackThread,NULL, rawPlaybackShim, this);
  if (result == 0)
  {
    pthread_setname_np(mPlaybackThread, "AudioRawFilePlay");
  }
  else
  {
    if(AUDIOPLAYER_DEBUG)  
    {printf("ERROR - cAudioPlayer::playRawFile could not start playback thread\n");}
    fclose(mPlaybackFile);
    return -1;
  }
#endif    
  
  return 0;
}

/** Plays the data from a raw file. It assumes that the data is in 2 channels,
* 16bit, 48khz sample rate. The playback is asynch.
* 
* Once playback is complete, this method can be called again with another wav 
* file. If it is called while a file is playing, it will return an error.
*
* @param filename - Name of the file to play
* @param playbackCallback This function is called when the playback is 
* complete. It is called in the context of a worker thread so take appropriate
* care. The prototype for the call back is:
* typedef int (*playbackCompleteCB)(int result);
*
* @return 0 - if successfully starts playback; -1 if not 
*/
int cAudioPlayer::playWavFile(const char *filename, 
			      playbackCompleteCB playbackCallback )
{
#ifdef __ANDROID__
  int result; 
  struct riff_wave_header riff_wave_header;
  struct chunk_header chunk_header;
  struct chunk_fmt chunk_fmt;
  static int data_sz = 0;  
  int more_chunks = 1;  
  

  if(mPlaybackFile != NULL)
  {
    if(AUDIOPLAYER_DEBUG)
    {printf("ERROR - Already Playing file %p \n",mPlaybackFile);}
    return -1;
  }
  mPlaybackCallback = playbackCallback;  
  
  mPlaybackFile = fopen(filename,"rb");
  if(mPlaybackFile == NULL)
  {
    if(AUDIOPLAYER_DEBUG)
    {printf("ERROR - cAudioPlayer::playWavFile could not open file %s \n",filename);}
    return -1;
  } 
  
  fread(&riff_wave_header, sizeof(riff_wave_header), 1, mPlaybackFile);
  if ((riff_wave_header.riff_id != ID_RIFF) ||
      (riff_wave_header.wave_id != ID_WAVE)) {
      if(AUDIOPLAYER_DEBUG)      
      {fprintf(stderr, "ERROR - cAudioPlayer::playRawFile '%s' is not a riff/wave file\n", filename);}
      fclose(mPlaybackFile);
      mPlaybackFile = NULL;
      return -1;
      }

  do {
      fread(&chunk_header, sizeof(chunk_header), 1, mPlaybackFile);

      switch (chunk_header.id) {
      case ID_FMT:
          fread(&chunk_fmt, sizeof(chunk_fmt), 1, mPlaybackFile);
          /* If the format header is larger, skip the rest */
          if (chunk_header.sz > sizeof(chunk_fmt))
              fseek(mPlaybackFile, chunk_header.sz - sizeof(chunk_fmt), SEEK_CUR);
          break;
      case ID_DATA:
          /* Stop looking for chunks */
          data_sz = chunk_header.sz;
          more_chunks = 0;
          break;
      default:
          /* Unknown chunk, skip bytes */
          fseek(mPlaybackFile, chunk_header.sz, SEEK_CUR);
      }
  } while (more_chunks);

  if(AUDIOPLAYER_DEBUG)
  {
    printf("Wav File Format: %d channels, %d fs, %d bits \n",
    chunk_fmt.num_channels, chunk_fmt.sample_rate, chunk_fmt.bits_per_sample);
  }

  if(chunk_fmt.num_channels != 1)
  {
    if(AUDIOPLAYER_DEBUG)
    {  
      printf("Bad Channel Count %d \n",chunk_fmt.num_channels);
    }
    fclose(mPlaybackFile);
    mPlaybackFile = NULL;
    return -1;
  }
  
  if(chunk_fmt.sample_rate != 48000)
  {
    if(AUDIOPLAYER_DEBUG)
    {  
      printf("Bad Fs %d \n",chunk_fmt.sample_rate);
    }
    fclose(mPlaybackFile);
    mPlaybackFile = NULL;
    return -1;
  }
  
  if(chunk_fmt.bits_per_sample != 16)
  {
    if(AUDIOPLAYER_DEBUG)
    {
      printf("Bad word length %d \n",chunk_fmt.bits_per_sample);
    }
    fclose(mPlaybackFile);
    mPlaybackFile = NULL;
    return -1;
  }
   
  mStopFlag = 0;
  
  result = pthread_create(&mPlaybackThread,NULL, rawPlaybackShim, this);
  if (result == 0)
  {
    pthread_setname_np(mPlaybackThread, "AudioWavFilePlay");
  }
  else
  {
    if(AUDIOPLAYER_DEBUG)
    {  
      printf("ERROR - cAudioPlayer::playWavFile could not start playback thread\n");
    }
    fclose(mPlaybackFile);
    return -1;
  }
#endif    
  
  return 0;

  }

