#include "attentionSystem.hpp"
#include "circleMath.hpp"
#include "common.hpp"
#include "debugSwitches.h"
#include "elements/patch.hpp"
#include "item.hpp"
#include "logging.hpp"
#include "params.h"
#include "switchboard/jsonListReader.hpp"
#include "switchboard/pinnedAoi.hpp"

#include <algorithm>
#include <cmath>
#include <cstddef>

// Constants used to control attention system behavior
const int kSnapToSearchWindow = 45; // deg (+/-)
const int kXSigma = 3;              // number of standard deviations by which
                                    // to define a person's width
const int kMaxPersonWidth = 120;    // deg
const int kMinPersonWidth = 20;     // deg


cAttentionSystem::cAttentionSystem(void)
{
  mpPano = NULL;
  mpFaceMapper = NULL;
  mpPersonMapper = NULL;
  mpMotionMapper = NULL;
  mStageStartTimerSet = false;
}

/** @param pPano is a pointer to a cPanorama that will be used as the
    image source for the cAttentionSystem. */
cAttentionSystem::cAttentionSystem(cPanorama *pPano )
{
  setPanorama(pPano);
}

cAttentionSystem::~cAttentionSystem()
{
}

/** Assign the image source for the cAttentionSystem. 
 *
 * @param pPano is a pointer to a cPanorama that will be used as the
 * image source for the cAttentionSystem. 
 */
void cAttentionSystem::setPanorama(cPanorama *pPano)
{
  mpPano = pPano;
}

/** Store the cFaceMapper pointer.
 *
 * @param pFaceMapper is pointer to a cFaceMapper object.  The face mapper 
 * facilitates access to a room map, which currently keeps bearing 
 * information about where people are located, based on face detection.
 */
void cAttentionSystem::setFaceMapper(cFaceMapper *pFaceMapper)
{
  mpFaceMapper = pFaceMapper;
}

/** Store the cMotionMapper pointer.
 *
 * @param pMotionMapper is pointer to a cMotionMapper object.  The motion mapper 
 * facilitates access to a room map, which currently keeps bearing 
 * information about where motion indicates people may be located.
 */
void cAttentionSystem::setMotionMapper(cMotionMapper *pMotionMapper)
{
  mpMotionMapper = pMotionMapper;
}

/** Store the cPersonMapper pointer.
 *
 * @param pPersonMapper is pointer to a cPersonMapper object.  The person mapper 
 * facilitates access to a room map, which currently represents each person
 * as a gaussian. 
 */
void cAttentionSystem::setPersonMapper(cPersonMapper *pPersonMapper)
{
  mpPersonMapper = pPersonMapper;
}

/** This is the primary externally facing function, which is used to refesh the
 * cAttentionSystem data.
 *
 * @param voices is a vector of cVoice objects that are used to deterine which
 * directions the system should pay attention to (or keep paying attention to,
 * or stop paying attention to).
 * @pararm pPinnedAois is a pointer to a pinned AOI argument structure that
 * might contain a command to apply to a pinned AOI.
 *
 * @return vector of pointers to cAreaOfInterest objects.
 */
std::vector<cAreaOfInterest *> 
cAttentionSystem::update(std::vector<cVoice> voices,
                         cPinnedAoiArgs *pPinnedAois)
{
  if(DEBUG(ATTENTION_SYSTEM)) {
    LOGI("\n// Attention System Update //\n"); }

  updateAttentionSystem(voices, pPinnedAois);

  if(DEBUG(ATTENTION_SYSTEM)) {
    LOGI("%d AOIs\n", mpAois.size()); }

  return mpAois;
}

/** This function forces the cVoiceTracker of each cAreaOfInterest to be 
    reset. */
void cAttentionSystem::reset(void)
{
  for(std::vector<cAreaOfInterest *>::iterator iAoi = mpAois.begin();
      iAoi != mpAois.end(); ++iAoi )
    {
      (*iAoi)->forceVoiceTrackerReset();
    }
}

/** This function forces all AOIs to go away. */
void cAttentionSystem::clearAois(void)
{
  std::vector<cAreaOfInterest *> expiredAois;

  // delete all AOIs
  for(std::vector<cAreaOfInterest *>::iterator iAoi = mpAois.begin();
      iAoi != mpAois.end(); ++iAoi )
    {
      expiredAois.push_back(*iAoi);
    }

  for(std::vector<cAreaOfInterest *>::iterator iAoi = expiredAois.begin();
      iAoi != expiredAois.end(); ++iAoi )
    {
      deleteAoi(*iAoi);
    }
  
  // reset timer used in stage initialization (when there are no AOIs yet)
  mStageStartTimerSet = false;
}

/** This function is the primary internally facing function; it is responsible
 * for updating the cAreaOfInterest objects based on where sound is coming from.
 *
 * Currently, the goal is to use the latest mic data to focus the attention 
 * system on the parts of the view that contain active conversation participants.
 * In the future, the goal will be broadened.
 *
 * @param voices is a vector of Voices that are used to determine which 
 * cAreaOfInterest objects should stay, which should go, and which should be 
 * created.
 * @pararm pPinnedAois is a pointer to a pinned AOI argument structure that
 * might contain a command to apply to a pinned AOI.
 */
void cAttentionSystem::updateAttentionSystem(std::vector<cVoice> voices,
                                             cPinnedAoiArgs *pPinnedAois )
{
  double kStageStartupTimeout = 10.0;   // time in seconds for how long the
  // system gets to spend looking to display non-talking people in the room
  // (starts after first talking person is found)
  float kPersonVisualMatchTolerance = 15.0; // (+/- deg) used to match people on
                                            // map w/ existing AOIs

  std::vector<cAreaOfInterest *> expiredAois;

  // make copy of voices to be used later by Camera Lock feature
   std::vector<cVoice> allVoices = voices;

  // update each voice tracker based on latest microphone analysis,
  // and remove any AOIs that are disabled and off the display
  for(std::vector<cAreaOfInterest *>::iterator iAoi = mpAois.begin();
      iAoi != mpAois.end(); ++iAoi )
    {
      if((*iAoi)->isEnabled())
	{
	  // get back voices that didn't match this AOI
	  voices = (*iAoi)->updateVoiceTracker(voices);
	}
      else if((*iAoi)->isReadyToRemove())
	{
	  expiredAois.push_back(*iAoi);
	}
    }

  // start timer (only want to spend so long looking for non-voice
  // triggered people at start of meeting)
  if(!mStageStartTimerSet && (voices.size() != allVoices.size()))
    {                                 //  ^__ this indicates that there's a sound
                                      // source that matches with a mapped person 
      mStageStartTimerSet = true;
      currentTimeSpec(&mStageStartTimeSpec);      
    }

  // remove expired AOIs
  for(std::vector<cAreaOfInterest *>::iterator iAoi = expiredAois.begin();
      iAoi != expiredAois.end(); ++iAoi )
    {
      deleteAoi(*iAoi);
    }
  
  // create new areas-of-interest based on new voices
  for(std::vector<cVoice>::iterator iVoice = voices.begin();
      iVoice != voices.end(); ++iVoice)
    {
      // figure out if there's a person near where the voice is coming from
      int voiceBearing = (*iVoice).getDirection();      

      cItem *pPerson = mpPersonMapper->getPersonAt(voiceBearing);
      if(pPerson == nullptr) {continue;}
      // null pointer indicates that no person was found near voice beating

      // get size and location of person
      int personBearing = pPerson->getThetaMean();
      int personWidth = 2 * pPerson->getThetaStdDev() * kXSigma;

      // if person is too wide or too narrow then the data is probably not
      // good enough to use reliably
      if(personWidth > kMaxPersonWidth ||
	 personWidth < kMinPersonWidth )
	{
	  continue;
	}

      // if there is a person to associate with the voice, try to integrate a
      // new patch
      int bearingDelta = std::abs(circleMath::intDiffAngs(personBearing,
							  voiceBearing ) );      
      if(bearingDelta < (personWidth / 2))
	{
	  // update cVoice direction by snapping it to mapped person bearing
	  (*iVoice).setDirection(personBearing);

	  // try to integrate patch with existing AOIs (or create a new AOI)
	  owl::cPatch patch(personBearing, 0, personWidth, 0);
	  
	  bool isAdded = integrateInterest(&patch, *iVoice);

	  if(DEBUG(ATTENTION_SYSTEM)) {
	    if(isAdded) {LOGI("Added new person-based AOI\n");} }
	}
    }
  
  // Special case: If the system just started up, try to display as many people
  //               on the screen as can be found (and can fit (handled by Visual
  //               Editor) --no sound cues needed.
  if((mpAois.size() == 0) ||  // <-- this should never happen, but, safety first!
     (!mStageStartTimerSet) ||
     (mStageStartTimerSet  && 
      elapsedTimeSinceSec(mStageStartTimeSpec) < kStageStartupTimeout ) ||
     (mpAois.size() == 1 && isAoiStale(mpAois[0])) )
    // ^
    // |_ This is a hack so that when there is 1 AOI left that is stale, it can
    //    be removed if a new person can be found by which to replace that stale
    //    AOI (otherwise, we'll leave it so that the stage is never empty).
    {
      // get a copy of the people map, but with people that are too large/small
      // removed, and in order of decreasing confidence (i.e. first person in 
      // this new map has the smallest standard deviation)
      std::vector<const cItem *> culledPeopleMap = sortAndCullPeopleMap();

      // loop through people and try to create AOIs
      for(std::vector<const cItem *>::iterator 
	    iPerson = culledPeopleMap.begin();
	  iPerson != culledPeopleMap.end(); ++iPerson )
	{
	  // try to integrate patch with existing AOIs (or create a new AOI)

	  // if person is already part of an existing AOI skip over
	  bool skip = false;
	  for(auto iAoi = mpAois.begin(); iAoi != mpAois.end(); ++iAoi)
	    {
	      float distance = std::abs(circleMath::
					intDiffAngs((*iAoi)->getTheta(),
						    (*iPerson)->getThetaMean()));

	      if(distance < kPersonVisualMatchTolerance) {skip = true; break;}
	    }
	  
	  if(skip) {continue;}  // person is already captured by an AOI

	  // get size and location of person
	  int personBearing = (*iPerson)->getThetaMean();
	  int personWidth = 2 * (*iPerson)->getThetaStdDev() * kXSigma;

	  std::vector<int> fauxAudioData;
	  cVoice fauxVoice(personBearing, 0, fauxAudioData);

	  // create patch & attempt to intergrate
	  owl::cPatch patch(personBearing, 0, personWidth, 0);	  

	  bool isAdded = integrateInterest(&patch, fauxVoice,
					   bool {false}, // pinned = false
					   cAreaOfInterest::ePriority_t
					   {cAreaOfInterest::TRY_TO_SHOW} );

	  if(DEBUG(ATTENTION_SYSTEM)) {
	    if(isAdded) {LOGI("Added new person-based AOI\n");} }
	} 
    }

  // update AOIs based on state of current Person Map
  // (e.g. maybe a person moved in the AOI, or left it altogether)
  updateAois();

  // manage the status of the AOIs (this dictates who stays/goes)
  manageAois();

  // update the Camera Lock feature (use allVoices because voices has the
  // voices that matches an existing AOI removed, and the Camera Lock may
  // very well want to lock on to a person in an exisiting AOI)
  updateCameraLock(pPinnedAois, allVoices);
}

/* This function checks each AOI in the Attention System to see if the Person 
 * Map currently has a person on it who is within the AOI's bounds.
 *
 * If a person is present, the AOI's watchdog is updated to the current time.
 * This is used as an input to determine when an AOI has become stale and should
 * be removed.
 */
void cAttentionSystem::updateAois(void)
{
  const int kRecenterThreshold = 5; // degrees difference required before we'll
                                    // start recentering an AOI on the current
                                    // Person Map person location
  const float kCloseRecenterThreshold = 10; // if the recenter distance is less
                                            // than this, recenter very slowly.

  // get the high-level representaton of the Person Map
  std::vector<const cItem *> kpPeopleMap = mpPersonMapper->getLongTermItemMap();

  // check each AOI in the Attention System
  for(std::vector<cAreaOfInterest *>::iterator iAoi = mpAois.begin();
      iAoi != mpAois.end(); ++iAoi )
    {
      int personCount = 0;
      int bearing;           // of current person (deg)
      int recenterBearing;   // of person to recenter on (deg)
      float width;           // conservative estimate of person's delta theta
      float personWidth;     // last valid person's width (wasn't too big)
      int recenterDist = 0;  // distance from bearing to recenter bearing
      
      if ((*iAoi)->isPinned())
        { continue; }

      // check against each person on the map
      for(std::vector<const cItem *>::const_iterator 
	    iPerson = kpPeopleMap.begin();
	  iPerson != kpPeopleMap.end(); ++iPerson )
	{
	  bearing = (*iPerson)->getThetaMean();
	  width = 2 * (*iPerson)->getThetaStdDev() * kXSigma;
	  // TODO: consider phi direction too

	  // check if person is within this AOI & well located
	  if((*iAoi)->contains(bearing) && (width < kMaxPersonWidth))
	    {
	      int currentBearing = (*iAoi)->getPatch()->theta;
	      
	      // person's mean position is within AOI
	      personCount++;
	      personWidth = width;
	      recenterBearing = bearing;
	      recenterDist = std::abs(circleMath::intDiffAngs(currentBearing,
							      recenterBearing) );
	      (*iAoi)->petWatchdog();
	    }
	}

      // if there's more than one person don't make any changes because we may
      // not be working off the correct person
      if(personCount == 1)
	{
	  // re-center patch (and thereby subframe)
	  if(recenterDist > kRecenterThreshold)
	    {
	      if(recenterDist < kCloseRecenterThreshold)
		{ // adjust focus (veeery slowly so that the user doesn't really
		  //               notice that the view is moving)
		  (*iAoi)->recenterPatch(recenterBearing, 0);
		  // TODO: Replace zero with phi value
		}
	      else
		{ // adjust focus quickly
		  (*iAoi)->adjustPatchFocus(recenterBearing, 0);
		}

	      // adjust AOI's voice tracker (old direction may be invalid now)
	      (*iAoi)->setVoiceDirection(recenterBearing);
	    }

	  // adjust patch (and thereby the minimum required subframe size if
	  // the person's width is now better defined (this enables more Stage
	  // Sharing)
	  owl::cPatch *pPatch = (*iAoi)->getPatch();
	  int newPersonWidth = personWidth;

	  // assume that a person would never actually get substantially wider
	  if(newPersonWidth < pPatch->dTheta)
	    {
	      (*iAoi)->adjustPatchSize(newPersonWidth, pPatch->dPhi);

	      // adjust AOI's voice tracker (old direction may be invalid now)
	      (*iAoi)->setVoiceDirection(recenterBearing);
	    }
	}
    }
}

/** Given a patch of space that the system wants to pay attention to, figure
 * out the best way to integrate it into what's already being tracked.
 *
 * If the proposed patch is too similar to an existing AOI patch, then do
 * nothing.  If the proposed patch is in a completely new region of space, then
 * create a new AOI.  Finally, if the patch has some overlap with an exisiting
 * AOI's patch, then meld the new patch into that AOI.
 *
 * @param pNewPatch is a pointer to a patch that represents the area of space 
 * that the attention system wants to include in an area-of-interest.
 * @param voice is the voice that triggered the interest in the given patch,
 * and is used if the patch becomes an AOI (or part of an exisiting AOI).
 * @param pinned is true if this new AOI should be pinned to the stage.
 * @param priority is an ePriority_t that tells the Visual Editor how important
 * it is that this AOI's Subframe be placed in the Layout and displayed.
 *
 * @return true if the proposed patch was integrated into the attention system.
 */
bool cAttentionSystem::integrateInterest(owl::cPatch *pNewPatch,
                                         cVoice voice,
                                         bool pinned,
					 cAreaOfInterest::ePriority_t priority )
{
  // TODO: Right now we're only looking in theta direction, eventually we
  //       may want to look at phi direction too.
  
  typedef enum { 
    kNone,       // nothing to do
    kNew,        // create new AOI
    kNewTrimmed, // trim patch and create new AOI
    kCombine,    // combine patch with existing AOI
    kUpdate      // adjust existing AOI to new patch's dimensions
  } action_t;
 
  action_t action = kNone;
  cAreaOfInterest *pUpdateAoi = nullptr; 
  std::vector<cAreaOfInterest *>::iterator iUpdateAoi;

  const int kDeltaThetaMax = 100;        // (deg) largest "width" an AOI can be
  const int kSmallOverlapThreshold = 10; // (deg)

  // see how this patch can (or can't) integrate with existing AOIs
  //
  // Note: loop backwards because newest AIOs are at the back, and we'd
  //       like to build off newest AOI

  // figure out what action to take
  if((mpAois.size() == 0) || onlyPinnedAoisExist())
    {action = kNew;}

  if (pinned)
    { action = kNew; }
  else
    {
      for(std::vector<cAreaOfInterest *>::reverse_iterator 
	    iAoi = mpAois.rbegin();
          iAoi != mpAois.rend(); ++iAoi )
        {
          owl::cPatch *pAoiPatch = (*iAoi)->getPatch();

          if ((*iAoi)->isPinned())
            {
              // The following clause seems unnecessary
              if(pAoiPatch->isFullOverlap(*pNewPatch))
                {
                  // exisiting AOI already includes new patch; nothing to do
                  break;
                }
              continue;
            }

          // one patch fully includes the other:
          if(pAoiPatch->isFullOverlap(*pNewPatch))
            {
              if(pAoiPatch->dTheta >= pNewPatch->dTheta)
                {
                  // exisiting AOI already includes new patch; nothing to do
		  action = kNone;
                  break;
                }
              else
                {
                  // adjust AOI to size of new interest
                  pUpdateAoi = (*iAoi);
                  action = kUpdate;
                  break;
                }
            }

          // no overlap between patches:
          else if(!pAoiPatch->isOverlapped(*pNewPatch))
            {
              owl::cPatch combinedPatch = pAoiPatch->findSum(*pNewPatch);

              if(combinedPatch.dTheta < kDeltaThetaMax)
                {
                  // combine AOIs
                  pUpdateAoi = (*iAoi);
                  iUpdateAoi = std::next(iAoi).base();
                  // next() needed because base (forward) iterator points one off
                  // from reverse iterator
                  *pNewPatch = combinedPatch;
                  action = kCombine;
                  break;
                }
              else
                {
                  // make new AOI for new interest
                  action = kNew;
                  // don't break because there may still be another AOI to 
		  // integrate with
                }
            }

          // partial overlap between patches:
          else
            {
              owl::cPatch combinedPatch = pAoiPatch->findSum(*pNewPatch);
              owl::cPatch overlapPatch = pAoiPatch->findOverlap(*pNewPatch);

              if(combinedPatch.dTheta < kDeltaThetaMax)
                {
                  // combine AOIs
                  pUpdateAoi = (*iAoi);
                  iUpdateAoi = std::next(iAoi).base();
                  // next() needed because base (forward) iterator points one off
                  // from reverse iterator
                  *pNewPatch = combinedPatch;
                  action = kCombine;
                  break;
                }
              else if(overlapPatch.dTheta < kSmallOverlapThreshold)
                {
		  // trim and create new AOI
                  pUpdateAoi = (*iAoi);
                  action = kNewTrimmed;  // <-- NOT IMPLEMENTED (see below)
		  
                  // don't break because there may still be another AOI to 
		  // integrate with
                }
              else
                {
                  // adjust AOI to size of new interest
                  pUpdateAoi = (*iAoi);
                  action = kUpdate;
                  break;
                }
            }
        }
    }

  // if an AOI is being integrated w/ low priority we don't want to do AOI
  // updates or combines because this can result in low-priority AOIs becoming
  // high-priority AOIs; additionally, visually triggered AOIs will be found
  // frame after frame, which leads to very rapid (and continual) updating of
  // the Layout, is combines/updates are allow
  if(priority == cAreaOfInterest::TRY_TO_SHOW)
    {
      // don't do anything if a low-priority integration requires update or
      // combine
      if(action == kUpdate || action == kCombine)
	{ action = kNone; }
    }
  // TODO: Consider whether it'd be better to do a check before 
  // combining/updating so that they are allowe for the case when both AOIs
  // are of the same priority.  Though this would still have the problem of
  // visually-trigger AOIs updating the Layout frame after frame, so really
  // something else is needed so that visually-triggered AOIs are treated
  // differently (yet again, it probably makes sense to have different classes
  // of AOIs).

  // as a simplifcation, just do update instead of trimming and making 
  // a new AOI (TODO: Remove this and implement "trim-and-create-new-AOI").
  if(action == kNewTrimmed) 
    {action = kNew;}

  // take action
  switch(action)
    {
    case kNew:
      createAoi(voice, mpPano, *pNewPatch, priority, pinned);
      break;
    case kNewTrimmed:
      break; // not implemented yet (as simplification... and also may not
             //                      be useful)  
    case kUpdate:
      pUpdateAoi->adjustPatchSize(pNewPatch->dTheta, pNewPatch->dPhi);
      pUpdateAoi->adjustPatchFocus(pNewPatch->theta, pNewPatch->phi);
      pUpdateAoi->setVoice(voice);
      // TODO: Fix cAreaOfInterest::setPatch so that it can be used here
      break;
    case kCombine:
      pUpdateAoi->adjustPatchSize(pNewPatch->dTheta, pNewPatch->dPhi);
      pUpdateAoi->adjustPatchFocus(pNewPatch->theta, pNewPatch->phi);
      pUpdateAoi->setVoice(voice);
      // TODO: Fix cAreaOfInterest::setPatch so that it can be used here
      mpAois.erase(iUpdateAoi);        // erase and push_back so that updated
      mpAois.push_back(pUpdateAoi);    // AOI is the freshest in the list
      break;
    case kNone:
      break;
    default:
      break;
    }

  if(action != kNone) 
    {return true;}
  else
    {return false;}
}

/** This function examines the set of areas-of-interest and adjust their states
 * as necessary.
 *
 * Each AOI is considered for deactivation or to be disabled (or is left alone),
 * depending on what's going on.  This is also where each AOI's cFaceTracker is
 * updated, although that is probably not ideal.
 */
void cAttentionSystem::manageAois(void)
{
  for(std::vector<cAreaOfInterest *>::iterator iAoi = mpAois.begin();
      iAoi != mpAois.end(); ++iAoi )
    {
      if ((*iAoi)->isEnabled())
	{ // search for face, adjust focus
	  (*iAoi)->updateFaceTracking();
	  // TODO it would be nice for this not to happen in here, but for some 
	  // reason things don't seem to work right if this is update is done
	  // along with the cVoiceTracker up in updateAttentionSystem().

	  // deactivate area-of-interest if it doesn't currently have 
	  // particularly interesting things happening
	  if(((*iAoi)->getVoiceTrackerState() == voiceState::LOST)
	      && !(*iAoi)->isPinned())
	    {
	      (*iAoi)->deactivate();
	      // deactivation doesn't mean that the AOI will disappear, just
	      // that it's okay for it to be eliminated if there isn't enough
	      // space available in the Layout to support its subframe.
	    }
	}

      // dsiable stale AOIs that are not pinned (but don't disable the very 
      // last AOI because we don't want the stage to ever be empty)
      if((isAoiStale((*iAoi)) && mpAois.size() > 1)
	 && !(*iAoi)->isPinned() )
	// ^
	// |__ *** Note: Put all conditions here for removing an AOI _before_
	//               forced to by the need for more display space 
	{
	  (*iAoi)->disable();
	  return;
	}
    }
}

/** Create new cAreaOfInterest based on the given cVoice object.
 *
 * @param voice is a cVoice object that indicates what direction sound is coming
 * from.
 * @param pPano is a pointer to a cPanorama, which will be used as the
 * source for visual data for newly created cAreaOfInterest.
 * @param patch is the area of the panorama to include in this AOI.
 * @param priority is an ePriority_t that tells the Visual Editor how important
 * it is that this AOI's Subframe be placed in the Layout and displayed.
 * @param pinned is true if this new AOI is pinned to the stage.
 *
 * @return a pointer to a newly created cAreaOfInterest. 
 */
cAreaOfInterest * cAttentionSystem::createAoi(cVoice voice, 
					      cPanorama *pPano,
					      owl::cPatch patch,
					      cAreaOfInterest::ePriority_t 
					      priority,
					      bool pinned )
{
  int id = findFirstFreeAoiId();
  cAreaOfInterest *pAoi = new cAreaOfInterest(voice, pPano, patch, id,
					      priority, pinned);
  mpAois.push_back(pAoi);
  
  return pAoi;
}

/** Applies new command to pinned AOIs.
 *
 * Note, a reply is created in cOwl, if necessary.
 *
 * @param pPinnedAois is the command structure that may contain a command
 * to apply.
 */
void cAttentionSystem::updatePinnedAois(cPinnedAoiArgs *pPinnedAois)
{
  if (!pPinnedAois->getArgsConsumed())
    {
      // Fresh pinned AOI argument(s) available, so apply
      int bearing;
      int width;
      int id;
      ePinnedAoiCmd_t cmd = ePinnedAoiCmd_t::NONE;

      // Get the command contents (does not consume argument; leave for cOwl)
      pPinnedAois->getArgs(cmd, id, width, bearing);

      // Act on the command
      switch (cmd)
      {
        case ePinnedAoiCmd_t::CREATE:
        case ePinnedAoiCmd_t::CREATE_AUTO:
          // Validate the width
          if(width <= kMaxPersonWidth &&
              width >= kMinPersonWidth )
            {
              // create patch and integrate into the attention system
              owl::cPatch patch(bearing, 0, width, 0);

              // create a fake voice to direct attention
              std::vector<int> fauxAudioData;
              cVoice fauxVoice(bearing, 0, fauxAudioData);

              // add the new pinned AOI!
              bool isAdded = integrateInterest(&patch, fauxVoice, true);

              if(DEBUG(ATTENTION_SYSTEM)) {
                  if(isAdded) {LOGI("Added new pinned AOI\n");} }
            }
          break;
        case ePinnedAoiCmd_t::DELETE:
          for (std::vector<cAreaOfInterest *>::iterator iAoi = mpAois.begin();
              iAoi != mpAois.end();
              iAoi++)
            {
              // Find the Aoi to delete in the list of Aois
              if (((*iAoi)->getId() == id) && ((*iAoi)->isPinned()))
                {
                  // aoi with matching id has been found, so disable it
                  (*iAoi)->disable();
                  break;
                }
            }
          break;
        case ePinnedAoiCmd_t::DELETE_ALL:
          for (std::vector<cAreaOfInterest *>::iterator iAoi = mpAois.begin();
              iAoi != mpAois.end();
              iAoi++)
            {
              // Find all pinned Aois in the list of Aois and delete them
              if ((*iAoi)->isPinned())
                {
                  // Pinned Aoi has been found, so disable it
                  (*iAoi)->disable();
                }
            }
          break;
        case ePinnedAoiCmd_t::MOVE:
          for (std::vector<cAreaOfInterest *>::iterator iAoi = mpAois.begin();
              iAoi != mpAois.end();
              iAoi++)
            {
              // Find the Aoi to modify in the list of Aois
              if (((*iAoi)->getId() == id) && ((*iAoi)->isPinned()))
                {
                  // aoi with matching id has been found, so move and resize it
                  (*iAoi)->setPinnedEditable(true);
                  (*iAoi)->adjustPatchFocus(bearing, 0);
                  (*iAoi)->adjustPatchSize(width, 0, true);
                  break;
                }
            }
          break;
        case ePinnedAoiCmd_t::PIN:
          // Stop editing AOI position and fix in place
          for (std::vector<cAreaOfInterest *>::iterator iAoi = mpAois.begin();
              iAoi != mpAois.end();
              iAoi++)
            {
              // Find the Aoi to modify in the list of Aois
              if (((*iAoi)->getId() == id) && ((*iAoi)->isPinned()))
                {
                  // aoi with matching id has been found, so change state
                  (*iAoi)->setPinnedEditable(false);
                  break;
                }
            }
          break;
        case ePinnedAoiCmd_t::GET_ALL:
          // this is handled in cOwl when creating the reply message
          break;
        default:
          break;
      }
    }
}

/** Manage Camera Lock feature based on the given pinned AOI command.
 *
 * Camera Lock forces the Attention System to focus on a given area, regardless
 * of sensor data.  Camera Lock can be enabled via the mobile app, in which
 * case the user provides the focus point, or it can be enabled via a voice
 * command, in which case the Attention System tries to focus on the correct
 * region based on where in the room the voice command is believed to have come
 * from.
 *
 * @param pPinnedAois is a pointer to the pin command that came in via the
 * Switchboard.
 * @param voices is the current set of Voices (sound sources) that the Owl's
 * mic array has detected.
 */
void cAttentionSystem::updateCameraLock(cPinnedAoiArgs *pPinnedAois, 
					std::vector<cVoice> voices )
{
  static bool waitingForTarget = false; // indicates that user asked for camera
                                        // lock to turn on (e.g. via voice 
                                        // command)
  static bool movePinnedAoi = false;    // idicates that the current pinned AOI
                                        // has been requested to move to a new
                                        // location 
  static int cameraLockAoiId;           // the ID of the pinned AOI
  // !!! WARNING: This won't work right 
  // if we allow multiple pinned AOIs at
  // the same time. !!!

  // flip switch if Attention System needs to create a pinned AOI to
  // satisfy a Camera Lock request (will happen as soon as a person can be
  // found to lock on, which may not be on this call of this function)
  if(pPinnedAois->getCommand() == ePinnedAoiCmd_t::CREATE_AUTO)
    { // CREATE_AUTO indicates that a Camera Lock is requested, but that the
      // Attention System should decide the direction for the lock
      if(anyPinnedAoisExist())
	{ // if there's a pinned AOI, assume we should move it
	  movePinnedAoi = true; 
	}
      // !!! WARNING: This won't work right if we allow multiple pinned AOIs 
      //              at the same time. !!!
      
      waitingForTarget = true;
    }
  else if(pPinnedAois->getCommand() == ePinnedAoiCmd_t::DELETE)
    {
      waitingForTarget = false;  // reset state
      movePinnedAoi = false;     //
    }
  
  // try to find the right direction to lock the camera view on
  if(waitingForTarget)
    {
      for(std::vector<cVoice>::iterator iVoice = voices.begin();
	  iVoice != voices.end(); ++iVoice)
	{
	  // figure out if there's a person near where the voice is coming from
	  int voiceBearing = (*iVoice).getDirection();      

	  cItem *pPerson = mpPersonMapper->getPersonAt(voiceBearing);

	  if(pPerson != nullptr) // safety first
	    {
	      if(movePinnedAoi)
		{ // set up to pass along move existing pinned AOI command
		  pPinnedAois->setCommand(ePinnedAoiCmd_t::MOVE); 
		}
	      else
		{ // set up to pass along create new pinned AOI command
		  cameraLockAoiId = findFirstFreeAoiId();
		  pPinnedAois->setCommand(ePinnedAoiCmd_t::CREATE); 
		}

	      // update pinning parameters
	      pPinnedAois->setId(cameraLockAoiId);
	      pPinnedAois->setSize(kMaxPersonWidth);
	      pPinnedAois->setAngle(pPerson->getThetaMean());
	      
	      // reset state
	      waitingForTarget = false;
	      movePinnedAoi = false;
	    }
	}
    }
 
  // apply commands to pinned AOIs, if appropriate
  if(!waitingForTarget // <-- indicates target has been found
     || pPinnedAois->getCommand() == ePinnedAoiCmd_t::DELETE )
    { 
      updatePinnedAois(pPinnedAois); 
    }
  else
    { // reset, because no one else will and the stale command will keep
      // coming back in with each cycle of this update loop
      pPinnedAois->setCommand(ePinnedAoiCmd_t::NONE);
      pPinnedAois->setArgsConsumed();
    }
}

/** Find the lowest available ID number for a new AOI.
 *
 * @return the new ID.
 */
int cAttentionSystem::findFirstFreeAoiId(void)
{
  int freeId = 0;
  int maxId = 0;
  unsigned char idList[256];  // Assumes there cannot be more than 256 AOIs

  memset(idList, 0, sizeof(idList) * sizeof(idList[0]));

  for (std::vector<cAreaOfInterest *>::iterator iAoi = mpAois.begin();
      iAoi != mpAois.end();
      iAoi++)
    {
      int id = (*iAoi)->getId();
      idList[id] = 1;
      if (id > maxId)
        { maxId = id; }
    }

  for (int iSlot = 0; iSlot <= maxId + 1; iSlot++)
    {
      if (idList[iSlot] == 0)
        {
          freeId = iSlot;
          break;
        }
    }

  return freeId;
}

/** This function creates an edited copy of the person map.
 *
 * The sorted and culled map does not contain any items that are too wide or
 * too narrow (theta standard deviation).  Furthermore the map is ordered so
 * that the items with the smallest variance are listed first.
 *
 * @return a vector of pointers that point to cItem's that represent people.
*/
std::vector<const cItem *> cAttentionSystem::sortAndCullPeopleMap(void)
{
  std::vector<const cItem *> pFullPeopleMap = mpPersonMapper->getLongTermItemMap();
  std::vector<const cItem *> pCulledPeopleMap;

  // remove from the map people that are too wide or too narrow to be reliably
  // considered real people
  std::remove_if(pFullPeopleMap.begin(), pFullPeopleMap.end(), 
		 [](const cItem *pPerson) {
		   int width = 2 * pPerson->getThetaStdDev() * kXSigma;
		   return(width > kMaxPersonWidth || width < kMinPersonWidth); } );
    
  // sort remaining people using standard deviation as the metric (smallest 
  // variance first)
  while(pFullPeopleMap.size() > 0)
    {
      const cItem *pBestPerson = pFullPeopleMap[0];
      float bestStdDev = pBestPerson->getThetaStdDev();
      
      for(auto iPerson = pFullPeopleMap.begin(); 
	  iPerson != pFullPeopleMap.end(); ++iPerson )
	{
	  float stdDev = (*iPerson)->getThetaStdDev();
	  if(stdDev < bestStdDev)
	    {
	      bestStdDev = stdDev;
	      pBestPerson = (*iPerson);
	    }
	}
      
      // add best person to sorted list, and remove from full list
      pCulledPeopleMap.push_back(pBestPerson);
      pFullPeopleMap.erase(std::remove(pFullPeopleMap.begin(), 
				       pFullPeopleMap.end(), 
				       pBestPerson ), pFullPeopleMap.end() );
    }

  return pCulledPeopleMap;
}

/** Remove an existing cAreaOfInterest from the cAttentionSystem.
 *
 * When a given area is no longer interesting it needs to be removed from the
 * list of cAreaOfInterest objects.
 *
 * @param pAoi is a pointer to the cAreaOfInterest that is to be removed.
 */
void cAttentionSystem::deleteAoi(cAreaOfInterest *pAoi)
{
  for(std::vector<cAreaOfInterest *>::iterator iAoi = mpAois.begin(); 
      iAoi != mpAois.end(); ++iAoi )
    {
      if(*iAoi == pAoi)
	{
	  mpAois.erase(iAoi);
	  delete pAoi;

	  if(DEBUG(ATTENTION_SYSTEM)) {
	    LOGI("Deleted AOI!\n"); }

	  break;
	}
    }
}

/** This fuction checks to see if the given is "stale".
 *
 * Currently, a stale AOI is defined by it not having contained a detected
 * person in the past specified mount of time.
 *
 * @param pAoi is a pointer to the AOI that is to be checked.
 *
 * @return true if the AOI seems to have been uninteresting for a while.
 */
bool cAttentionSystem::isAoiStale(cAreaOfInterest *pAoi)
{
  const double kNoPersonTimeThreshold = 2 * 60; // sec

  if(elapsedTimeSinceSec(pAoi->getWatchdog()) > kNoPersonTimeThreshold)
    {
      return true;
    }

  return false;
}

/** Function to tell if any non-pinned AOIs exist.
 *
 * @return true if no non-pinned AOIs are found (only pinned AOIs exist).
 */
bool cAttentionSystem::onlyPinnedAoisExist(void)
{
  bool nonPinnedAoiFound = false;
  for(std::vector<cAreaOfInterest *>::reverse_iterator iAoi = mpAois.rbegin();
      iAoi != mpAois.rend(); ++iAoi )
    {
      if (!(*iAoi)->isPinned())
        {
          nonPinnedAoiFound = true;
          break;
        }
    }

  return !nonPinnedAoiFound;
}

/** Function to tell if any pinned AOIs exist.
 *
 * @return true if any pinned AOIs are found.
 */
bool cAttentionSystem::anyPinnedAoisExist(void)
{
  for(std::vector<cAreaOfInterest *>::reverse_iterator iAoi = mpAois.rbegin();
      iAoi != mpAois.rend(); ++iAoi )
    {
      if ((*iAoi)->isPinned())
        {
	  return true;
        }
    }
  return false;
}
