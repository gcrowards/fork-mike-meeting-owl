#include "logging.hpp"
#include "params.h"
#include "systemParams.hpp"
#include "sysfs.hpp"

#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>

cSystemParams::cSystemParams(void)
{
  loadParams();
}

/** This is the PRIVATE function that reads and returns a sysfs
 * variable value.
 *
 * @param pValueName is a pointer to the variable name; SYSFS_PATH
 * will be preprended to get the full path for the sysfs node.
 * 
 * @return the variable value, currently expected to be in [0,1728]
 * under normal conditions; the return value is SYSFS_BAD_VALUE
 * (currently -1) if something goes wrong.
 */
int cSystemParams::readOtpValue(const char *pValueName)
{
  int  value, ret;
  char pFilepath[256];
  FILE *fp;

  // open the filepath for the specific sysfs variable
  sprintf(pFilepath, "%s/%s", SYSFS_OTP_PATH, pValueName);
  fp = fopen(pFilepath, "r");

  // read the value, setting SYSFS_BAD_VALUE if anything goes wrong
  if (fp == NULL)
    { value = SYSFS_BAD_VALUE; }
  else
    {
      ret = fscanf(fp, "%d", &value);
      if (ret != 1)
	{ value = SYSFS_BAD_VALUE; }
      fclose(fp);
    }
  return value;
}

/** This function loads all the system parameters (currently just Optical 
 * Center, but one day maybe more). */
void cSystemParams::loadParams(void)
{
  loadOpticalCenter();
}

/** This function populates the optical center parameter values.
 * 
 * An attempt is made to real the camera's optical center from the camera's
 * OTP (One-Time Programmable) memory.  If that fails then the optical center
 * will be read from a hand-placed config file (currently in /data/OwlLabs).  If
 * that fails, the system defaults to using the hard-coded values in params.h.
 */
void cSystemParams::loadOpticalCenter(void)
{
  // Don't expect to find an optical center more than +/- 400 pixels from the
  // geometric center of the imager
  const int kOcThresholdLow = (3456 / 2) - 400;  
  const int kOcThresholdHigh = (3456 / 2) + 400;

  const int kMaxOtpTries = 3;  // times to attempt reading OC from camera OTP
  const int kOtpRetryPause = 0.010E6; // microseconds to sleep between attempts

  struct stat statBuf;
  int ret, readX, readY, magicNum;

  // attempt to read optical center data from camera OTP
  int readAttempts = 0;

  while(readAttempts < kMaxOtpTries)
    {
      readX = readOtpValue("msm_oc_x");
      readY = readOtpValue("msm_oc_y");
      magicNum = readX << 16 | readY;

      if (readX != SYSFS_BAD_VALUE && readY != SYSFS_BAD_VALUE &&
	  magicNum != SYSFS_OTP_ERR )
	{
	  // sucessful read!
	  break;
	}

      readAttempts++;

      usleep(kOtpRetryPause);
    }

  LOGI("Camera OTP read returned OC_X = 0x%04x, OC_Y = 0x%04x\n", readX, readY);

  // If there is a read error or optical center is not implemented in OTP, fall
  // back to the old way of setting these values
  if (readX == SYSFS_BAD_VALUE || readY == SYSFS_BAD_VALUE ||
      readX < kOcThresholdLow || readX > kOcThresholdHigh ||
      readY < kOcThresholdLow || readY > kOcThresholdHigh ||
      magicNum == SYSFS_OTP_ERR )
    {
      LOGE("Failed OTP read; falling back.\n");
      // make sure file exists
      ret = stat(CONFIG_PATH, &statBuf);
    if (ret == 0)
      {
	FILE *pFile = fopen(CONFIG_PATH, "r");
	
	ret = fscanf(pFile, "%d %d", &cameraCenterX, &cameraCenterY);
	fclose(pFile);
	
	if (ret <= 0)
	  // TODO: Would be better to make sure ret equals the exact number of
	  //       expected parameters
	  {
	    LOGE("Couldn't read from systemParams.cfg\n");
	  }
	else
	  {
	    LOGI("Loading optical center from systemParams.cfg\n");
	  }
      }
    else
      {      
	// populate member params from params.h
	cameraCenterX = CAMERA_CENTER_X;
	cameraCenterY = CAMERA_CENTER_Y;
	
	LOGI("Loading optical center from hard-coded params.h\n");
      }
    }
  else	// OTP read was successful
    {
      cameraCenterX = readX;
      cameraCenterY = readY;
      
#ifndef INPUT_3456
      // assume using the imager at 1728 resolution and cut the center values 
      // in half
      cameraCenterX /= 2;
      cameraCenterY /= 2;
#endif
    }
  
  LOGI("Optical center set (%d, %d)\n", cameraCenterX, cameraCenterY);
}

cSystemParams::~cSystemParams()
{
}
