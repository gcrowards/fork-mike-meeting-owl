/*
 * startupExp.cpp
 *
 * Copyright (c) Owl Labs Inc. 2017.
 * All rights reserved.
 *
 *  Created on: Apr 30, 2017
 *
 * Startup state machine for sequencing tutorial UI elements associated with
 * starting up the Meeting Owl.
 */

#include <unistd.h>

#include "buildId.hpp"
#include "elements/iconOverlay.hpp"
#include "logging.hpp"
#include "shellProcess.hpp"
#include "startupExp.hpp"
#include "statusFrame.hpp"
#include "switchboard/setupProg.hpp"
#include "systemtest/testImgCompositor.hpp"

#define DEBUG_STARTUP  // Only for testing!
#ifdef DEBUG_STARTUP
#define LOGIS LOGI
#else
#define LOGIS(...) {}
#endif

#define BANNER_LINE_MSG_X 1080 // x-cord for text message on banner line
#define BANNER_LINE_MSG_Y 690  // y-cord for text message on banner line

/** Constructs the startup experience class and starts its control thread.
 *
 * This class manages the startup experience state machine coordinating
 * spoken prompts and message displays to help familiarize the user with
 * the Owl.
 *
 * @param pAnalytics points to the analytics object for meeting state.
 * @param pBlackBox points to the black box object that stores system state.
 */
cStartupExperience::cStartupExperience(cAnalytics *pAnalytics,
                                       cBlackBox *pBlackBox,
                                       cLedContext *pLedContext)
  : mpAnalytics(pAnalytics),
    mpBlackBox(pBlackBox),
    mpLedContext(pLedContext),
    mStartTime({0,0}),
    mState(WAITING_TO_HOOT),
    mFirstTimeStartup(true),
    mAppConfigured(false),
    mUvcConnected(false),
    mSpeakFirstTimeMessage(false),
    mSpeakWiFiUnconfiguredMessage(false),
    mStageEmpty(true),
    mThread(std::bind(&cStartupExperience::update, this), "StartupExp"),
    mpOverlays(nullptr)
{
  mpNetStatus = new cNetStatus();

  initStartupExperience();

  mThread.start();
}

/** Destroys the startup experience object and thread of execution.
 *
 */
cStartupExperience::~cStartupExperience()
{
  delete mpNetStatus;

  mThread.join();
}

/** Passes the overlay control structure to the startup experience.
 *
 * This function must be called at least once after the overlays have
 * been created to allow the startup experience to draw overlays and
 * graphics.
 *
 * @param pOverlays points to the object containing overlay control data.
 */
void cStartupExperience::setOverlays(owl::cRgbaOverlayLists *pOverlays)
{
  mpOverlays = pOverlays;
}

/** Informs the startup experience of whether any people have been found.
 *
 * This function should be called in the frame-by-frame update loop to
 * let the startup experience know when it is appropriate to clear the
 * splash screen and show the panorama and stage.
 *
 * @param empty is false as soon as a person has been found.
 */
void cStartupExperience::setAoiStatus(bool empty)
{
  mStageEmpty = empty;
}

/** Executes one cycle of the startup experience state machine.
 *
 * This is called periodically by the startup experience thread. On each
 * call, the test function for the current state is evaluated. Generally
 * if the test returns true, then a state transition should occur, and
 * usually the state machine will do some action, too.
 *
 * States generally progress in the order of the cases. However, when a
 * meeting ends, the state machine jumps back to the state that waits for
 * a new meeting to begin.
 */
void cStartupExperience::update(void)
{
  bool done = false;
  static eStartupState_t prevState = MEETING_ENDED;

  // debug for tracking state transitions
  if (prevState != mState)
    { LOGI("startupExperience: state = %d\n", mState); }
  prevState = mState;

  switch (mState)
  {
    case WAITING_TO_HOOT:
      if (testWaitingBeforeHoot())
        {
          doStartHoot();
          doBlinkLeds(true);
          mState = HOOTING;
        }
      break;
    case HOOTING:
      if (testWaitingForHootDone())
        {
          doStartWaitingToSpeak();
          doBlinkLeds(false);
          mState = WAITING_TO_SPEAK;
        }
      break;
    case WAITING_TO_SPEAK:
      if (testWaitingToStartDone())
        {
          doStartSpeakingFirstMessage();
          mState = SPEAKING_FIRST_MESSAGE;
        }
      break;
    case SPEAKING_FIRST_MESSAGE:
      if (testWaitingForFirstMessageDone())
        {
          mState = WAITING_FOR_STREAMING_START;
        }
      break;
    case WAITING_FOR_STREAMING_START:
      if (testWaitingForStreamingStart())
        {
          doStartShowingLogo();
          mState = SHOWING_LOGO;
        }
      break;
    case SHOWING_LOGO:
      if (testTimeToShowMessage())
        {
          doStartShowingMessage();
          mState = SHOWING_MESSAGE;
        }
      break;
    case SHOWING_MESSAGE:
      if (testTimeToClearScreen())
        {
          LOGI("SHOWING_MESSAGE: timeToClearScreen\n");
          clearLogo();
          clearMessageDisplay();
          mState = STARTUP_COMPLETE;
        }
      else
        {
          updateMessageDisplay();
        }
      break;
    case STARTUP_COMPLETE:
      if (testWaitingForMeetingEnd())
        {
          mState = MEETING_ENDED;
        }
      break;
    case MEETING_ENDED:
      if (testWaitingForWiFiMessageDone())
        {
          doShowBackground();
          initStartupExperience();
          mState = WAITING_FOR_STREAMING_START;
        }
      break;
    default:
      LOGE("startupExperience::update(): Invalid state!\n");
      break;
  }

  // sleep for a while between iterations of the state machine
  usleep(mkPollPeriodNsec);
}

/** Initializes the startup experience state machine state variables.
 *
 */
void cStartupExperience::initStartupExperience()
{
  int uvcConnected;
  int appSetupProgress;

  currentTimeSpec(&mStartTime);

  // Get needed data from the black box
  // WLAN previously configured?
  mpBlackBox->getInt(SETTINGS_ROOT_STR,
                      SETTINGS_APP_SETUP_PROGRESS,
                      appSetupProgress);
  // We base all decisions off of whether the App has configured WiFi.
  // If the appSetupProgress value is odd, then WiFi has been configured.
  mAppConfigured = (appSetupProgress & 0x01);

  getUvcStreamStateWrapper(&uvcConnected);
  mUvcConnected = !(!uvcConnected);

  mMinimizeStartupExperience = loadParameter(STARTUP_EXP_PARAMS,
                                             "Minimize", false);
  if (mMinimizeStartupExperience)
    { mState = WAITING_FOR_STREAMING_START; }
}

/** Test to decide if it is time to hoot.
 *
 * @return true if it is now time to hoot at startup.
 */
bool cStartupExperience::testWaitingBeforeHoot(void)
{
  bool done = false;
  bool waitBeforeHootDone =
      (elapsedTimeSinceSec(mStartTime) >= mkDelayBeforeHoot);

  if (waitBeforeHootDone)
    {
      done = true;
    }

  return done;
}

/** Start playing the hoot startup sound.
 *
 */
void cStartupExperience::doStartHoot(void)
{
  char pHootFileName[128];

  // Select which hoot to play
  if (mpBlackBox->getVersionChanged())
    {
      strncpy(pHootFileName, VER_CHANGE_HOOT_WAV, sizeof(pHootFileName));
    }
  else
    {
      strncpy(pHootFileName, HOOT_WAV, sizeof(pHootFileName));
    }
  // Play the hoot!
  LOGIS("startupExperience: Start playing hoot.\n");
  gSpeechDoneFlag = false;
  if (mAudioPlayer.playWavFile(pHootFileName, speechDone) != 0)
    {
      gSpeechDoneFlag = true;
      LOGW("cStartupExperience: Audio file %s not found\n", pHootFileName);
    }
#ifndef __ANDROID__
  gSpeechDoneFlag = true;  // Linux: Flag done immediately, otherwise hangs
#endif
}

/** Test to decide if the hoot has finished.
 *
 * @return true if the message is finished.
 */
bool cStartupExperience::testWaitingForHootDone(void)
{
  bool done = true;

  // Check if message player has finished, set done false if not
  done = gSpeechDoneFlag;

  if (done)
    {
      mAudioPlayer.stopPlayback();
    }

  return done;
}

/** Start waiting for delay before speaking greeting.
 *
 */
void cStartupExperience::doStartWaitingToSpeak(void)
{
  currentTimeSpec(&mStartTime);
}

/** Test to decide if it is time to speak after the hoot.
 *
 * @return true if it is now time to speak the initial greeting.
 */
bool cStartupExperience::testWaitingToStartDone(void)
{
  bool done = false;
  bool waitBeforeFirstTimeMessageDone =
      (elapsedTimeSinceSec(mStartTime) >= mkDelayBeforeFirstSpeakingSec);

  if (waitBeforeFirstTimeMessageDone)
    {
      done = true;
      mSpeakFirstTimeMessage = !mAppConfigured;
    }

  return done;
}

/** Test to decide if the first spoken message has finished.
 *
 * Note: this returns true immediately of the this is not the first time
 * the system has started up (or first time after a factory reset message).
 *
 * @return true if the message is finished.
 */
bool cStartupExperience::testWaitingForFirstMessageDone(void)
{
  bool done = true;

  if (mSpeakFirstTimeMessage)
    {
      // Check if message player has finished, set done false if not
      done = gSpeechDoneFlag;

      if (done)
        {
          mAudioPlayer.stopPlayback();
          mSpeakFirstTimeMessage = false;
        }
    }

  return done;
}

/** Test to decide if the video stream is connected to start a meeting.
 *
 * @return true if the UVC connection has started streaming.
 */
bool cStartupExperience::testWaitingForStreamingStart(void)
{
  bool started = false;
  int uvcConnected;

  getUvcStreamStateWrapper(&uvcConnected);
  mUvcConnected = !(!uvcConnected);
  if ((mUvcConnected) && (mpOverlays != nullptr))
    {
      started = true;
      initStartupExperience();

      // Don't show graphics on top of startup screens
      mpOverlays->setSuppressOverlays(true);
    }

  return started;
}

/** Test to decide if it is time to display the startup message.
 *
 * Note: this returns true immediately if the App has already been configured.
 *
 * @return true if it is time to show the startup message.
 */
bool cStartupExperience::testTimeToShowMessage(void)
{
  bool timesUp = false;
  struct timespec tNow;
  double elapsed;

  if (!mAppConfigured)
    {
      currentTimeSpec(&tNow);
      elapsed = elapsedTimeSec(mStartTime, tNow);

      if ((int)elapsed >= mkDelayAfterShowingIcon)
        { timesUp = true; }
    }
  else
    { timesUp = true; }

  return timesUp;
}

/** Test to decide if it is time to clear the screen and start a meeting.
 *
 * Note: this returns true immediately if the App has already been configured.
 *
 * @return true if it is time to clear the overlays.
 */
bool cStartupExperience::testTimeToClearScreen(void)
{
  bool timesUp = false;

  if (!mAppConfigured)
    {
      int appSetupProgress;
      // Get needed data from the black box
      // WLAN previously configured?
      mpBlackBox->getInt(SETTINGS_ROOT_STR,
                          SETTINGS_APP_SETUP_PROGRESS,
                          appSetupProgress);

      // only show until Attention System is ready
      if (!mStageEmpty)
	{ timesUp = true; }      
    }
  else
    { timesUp = true; }

  return timesUp;
}

/** Test to decide if a meeting has ended.
 *
 * @return true if the meeting has ended.
 */
bool cStartupExperience::testWaitingForMeetingEnd(void)
{
  int uvcConnected;

  getUvcStreamStateWrapper(&uvcConnected);
  return ((!uvcConnected) &&
      (mpAnalytics->getMeetingState() == cAnalytics::NOT_MEETING));
}

/** Test to decide if speaking the WiFi disconnected message is done.
 *
 * Note: This returns true immediately if the WiFi is not disconnected.
 *
 * @return true if the audio message has finished playing.
 */
bool cStartupExperience::testWaitingForWiFiMessageDone(void)
{
  bool done = true;

  if (mSpeakWiFiUnconfiguredMessage)
    {
      done = gSpeechDoneFlag;
      if (done)
        {
          mAudioPlayer.stopPlayback();
          mSpeakWiFiUnconfiguredMessage = false;
        }
    }

  return done;
}

/** Show the splash screen background image.
 *
 * Note: Only shows the background if the App has not been configured.
 */
void cStartupExperience::doShowBackground(void)
{
  if (mpOverlays != nullptr)
    {
      if (!mAppConfigured)
        {
          // Show blank background
          mpOverlays->mpOverlayManager->getBannerLine()->setNewFileToLoad(
              STARTUP_BACKGROUND, cv::Point(0, 0));
          mpOverlays->mpOverlayManager->getBannerLine()->setAlpha(1.0);
          std::string buildId = cBuildId::getString();
          std::string buildIdMsg = "version " + buildId;
          mpOverlays->mpOverlayManager->getBannerLine()->
	    drawTextMessage(buildIdMsg, 
			    BANNER_LINE_MSG_X, BANNER_LINE_MSG_Y,
			    cTestImgCompositor::mkBgColor );
          mpOverlays->mpOverlayManager->getBannerLine()->markFileReadyToLoad();
          if (!mUvcConnected)
            {
              if (mpOverlays->mpOverlayManager->getBannerLine()->loadNewFile())
                { mpOverlays->mpOverlayManager->forceRender(); }

            }
        }
      if (mpOverlays->mIconOverlays.size() != 0)
        {
          mpOverlays->mIconOverlays[0]->setOwner(owl::cIconOverlay::STARTUP);
        }
  }
}

/** Start speaking the first-time startup message.
 *
 * Note: Only spoken the first time the system starts, or after a factory
 * reset message has been received.
 */
void cStartupExperience::doStartSpeakingFirstMessage(void)
{
  currentTimeSpec(&mStartTime);

  if (!mAppConfigured)
    {
      // Show blank background
      doShowBackground();

      if (mSpeakFirstTimeMessage)
        {
          // Start playing first time startup message
          LOGIS(
              "startupExperience: Start playing first-time startup message.\n");
          gSpeechDoneFlag = false;
          if (mAudioPlayer.playWavFile((char *)FIRST_STARTUP_WAV,
                                       speechDone) < 0)
            {
              gSpeechDoneFlag = true;
              mSpeakFirstTimeMessage = false;
            }
#ifndef __ANDROID__
          gSpeechDoneFlag = true;  // Linux: Flag done immediately, otherwise hangs
#endif
        }
    }
}

/** Show the Logo atop the splash screen background.
 *
 * Note: only show the logo if the App has not yet been configured.
 */
void cStartupExperience::doStartShowingLogo(void)
{
  currentTimeSpec(&mStartTime);

  // Change of plans: For all startups, always show the splash screen. This
  // is most easily accomplished by pretending the App is not configured
  // from this point in the state machine onwards.
  // NOTE: Once this policy is settled, this state machine should be modified
  // to disregard the mAppConfigured flag for the steps after this!
  mAppConfigured = false;

  if (!mAppConfigured)
    {
      owl::cIconOverlay *pLogo = mpOverlays->mIconOverlays[0];
      int xStart;
      int width;
      int height;
      float scale = 1.0f;

      mpOverlays->mIconOverlays[1]->setIconActive(false);

      pLogo->setOwner(owl::cIconOverlay::eIconOwner_t::STARTUP);
      pLogo->setIconActive(true);
      pLogo->getAnimationPtr()->setDuration(LOGO_ANIMATION_DURATION);
      pLogo->getAnimationPtr()->setDirection(1);
      pLogo->getSize(width, height);
      xStart = (int)((((float)OUTPUT_WIDTH - ((float)width * scale)) * 0.5f) + 0.5f);
      pLogo->getAnimationPtr()->setBegin(xStart, LOGO_Y,
                                       scale, scale, LOGO_INITIAL_ALPHA);
      pLogo->getAnimationPtr()->setEnd(xStart, LOGO_Y,
                                     scale, scale, 1.0);

      pLogo->getAnimationPtr()->start();
      pLogo->setVisible(true);
    }
}

/** Show the startup message.
 *
 * Note: only show a startup message if the App has not yet been configured.
 * The message to show is determined based on the first-time flag. If this is
 * the first time starting up, show the first-time message and set the black
 * box flag to know that future startups are not the first time. Also,
 * initialize the drawing of the progress bar.
 */
void cStartupExperience::doStartShowingMessage(void)
{
  currentTimeSpec(&mStartTime);

  if (!mAppConfigured)
    {
      // Show message
      if (mFirstTimeStartup)
        {
          mpOverlays->mpOverlayManager->getBannerLine()->setNewFileToLoad(
              STARTUP_FIRST_TIME, cv::Point(0, 0));
          mFirstTimeStartup = false;
        }
      else
        {
          mpOverlays->mpOverlayManager->getBannerLine()->setNewFileToLoad(
              STARTUP_NEXT_TIME, cv::Point(0, 0));
        }
      mpOverlays->mpOverlayManager->getBannerLine()->setAlpha(1.0);
      std::string buildId = cBuildId::getString();
      std::string buildIdMsg = "version " + buildId;
      mpOverlays->mpOverlayManager->getBannerLine()->
	drawTextMessage(buildIdMsg, 
			BANNER_LINE_MSG_X , BANNER_LINE_MSG_Y, 
			cTestImgCompositor::mkBgColor );
      mpOverlays->mpOverlayManager->getBannerLine()->markFileReadyToLoad();

      mpOverlays->setProgressBar(cv::Rect(mkProgressBarX,
                                          mkProgressBarY,
                                          mkProgressBarStartWidth,
                                          mkProgressBarStartHeight),
                                 cv::Scalar(mkProgressBarColor));
    }
}

/** If the WiFi network is disconnected, play the disconnected audio message.
 *
 * Note: if the App is not yet configured, also show the splash screen in
 * preparation for the next startup.
 */
void cStartupExperience::doStartSpeakingWiFiMessage(void)
{
  currentTimeSpec(&mStartTime);

  if (!mAppConfigured)
    {
      // Show blank splash screen background
      doShowBackground();
    }

  if (!mMinimizeStartupExperience &&
      (mpNetStatus->isNetworkConfigured() != 1))
    {
      // Start playing first time startup message
      LOGIS("startupExperience: Start playing WiFi unconfigured message.\n");
      mSpeakWiFiUnconfiguredMessage = true;
      gSpeechDoneFlag = false;
      if (mAudioPlayer.playWavFile((char *)WIFI_UNCONFIG_WAV, speechDone) != 0)
        { gSpeechDoneFlag = true; }
#ifndef __ANDROID__
      gSpeechDoneFlag = true;  // Flag done immediately, otherwise hangs
#endif
    }
  else
    {
      gSpeechDoneFlag = true;
    }
}

/** Update the progress bar while the splash screen and message are displayed.
 *
 * Note: only done if the App has not yet been configured.
 */
void cStartupExperience::updateMessageDisplay(void)
{
  if (!mAppConfigured)
    {
      struct timespec tNow;
      double elapsed;
      double elapsedRatio;
      int elapsedRepeatCount;
      double barFraction;
      bool growingForeground;
      int xStart;
      int delayTime = (mMinimizeStartupExperience) ?
          mkDelayAfterShowingMessageMin : mkDelayAfterShowingMessage;

      currentTimeSpec(&tNow);
      elapsed = elapsedTimeSec(mStartTime, tNow);

      elapsedRatio = elapsed / (double)delayTime;
      elapsedRepeatCount = (int)elapsedRatio;
      barFraction = elapsedRatio - (double)elapsedRepeatCount;
      // foreground grows on even repetitions, background on odds
      growingForeground = !(elapsedRepeatCount & 1);
      int newWidth = (int)(barFraction *
          (mkProgressBarEndWidth - mkProgressBarStartWidth));
      if (newWidth > mkProgressBarEndWidth)
        { newWidth = mkProgressBarEndWidth; }

      if (growingForeground)
        {
          xStart = mkProgressBarX + mkProgressBarEndWidth - newWidth;
        }
      else
        {
          xStart = mkProgressBarX;
          newWidth = mkProgressBarEndWidth - newWidth;
        }

      mpOverlays->setProgressBar(cv::Rect(xStart,
                                          mkProgressBarY,
                                          newWidth,
                                          mkProgressBarStartHeight),
                                 cv::Scalar(mkProgressBarColor));
    }
}

/** Erase the Owl logo from the startup screen.
 *
 */
void cStartupExperience::clearLogo(void)
{
  const int kCenterX = OUTPUT_WIDTH / 2;
  const int kCenterY = (OUTPUT_HEIGHT + OUT_PANO_RES_Y) / 2;
  owl::cIconOverlay *pLogo = mpOverlays->mIconOverlays[1];
  owl::cLinearAnimate *pAnim = pLogo->getAnimationPtr();

  mpOverlays->mIconOverlays[0]->setIconActive(false);

  // Prepare Logo for runtime use
  pLogo->setOwner(owl::cIconOverlay::RUNTIME);
  pLogo->setIconActive(true);

  // Logo fixed in the upper corner
  pAnim->setDuration(LOGO_ANIMATION_DURATION);
  pAnim->setDirection(-1);
  pLogo->setVisible(true);
}

/** Clear splash screen and replace startup assets with run-time banner line.
 *
 */
void cStartupExperience::clearMessageDisplay(void)
{
  mpOverlays->mpOverlayManager->getBannerLine()->setNewFileToLoad(
      BANNER_LINE, cv::Point(0, 0));
  // disable display of the banner line by setting alpha to 0
  mpOverlays->mpOverlayManager->getBannerLine()->setAlpha(0);
  mpOverlays->mpOverlayManager->getBannerLine()->markFileReadyToLoad();

  mpOverlays->setProgressBar(cv::Rect(0, 0, 0, 0),
                             cv::Scalar(mkProgressBarColor));

  mpOverlays->setSuppressOverlays(false);
}

/** Return the current state of the startup state machine.
 *
 * @return startup state.
 */
cStartupExperience::eStartupState_t cStartupExperience::getStartupState()
{
  return mState;
}

/* Flag set by callback when AudioPlayer clip is done. */
bool gSpeechDoneFlag = false;

/** Callback function called by AudioPlayer when clip has finished playing.
 *
 * @param result is the result status from the player.
 *
 * @return the value of result.
 */
int speechDone(int result)
{
  gSpeechDoneFlag = true;
  return result;
}

/** Turn on or off blinking LEDs at startup if the version has changed.
 *
 * @param on if true turns on blinking, if false, off.
 */
void cStartupExperience::doBlinkLeds(bool on)
{
  if (mpBlackBox->getVersionChanged())
    {
      if (on)
        {
          mpLedContext->startBlinking(LEDS_EYE | LEDS_MUTE | LEDS_BLUETOOTH,
                                      150, 100);
        }
      else
        {
          mpLedContext->stopBlinking();
        }
    }
}

