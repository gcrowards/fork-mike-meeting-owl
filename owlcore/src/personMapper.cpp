#include "debugSwitches.h"
#include "logging.hpp"
#include "personMapper.hpp"
#include "params.h"

#include <vector>


#define UPDATE_DIRECT   true /**< determines which style of map update is used,
				either direct setting of values or add/leak. */

// The weights below are used to combine the three component maps
// (face, motion, and sound) into a single person map using a
// quadratic model. THIS IS EXPERIMENTAL. Note that the quadratic
// terms are currently set to 0, effectively turning the model into a
// linear one.

// -- weights for linear terms
#define W_FAC          1.0
#define W_MOT          1.0
#define W_SND          1.0
// -- weights for squared terms
#define W_FAC_FAC      0.0
#define W_MOT_MOT      0.0
#define W_SND_SND      0.0
// -- weights for cross terms
#define W_FAC_MOT      0.0
#define W_FAC_SND      0.0
#define W_MOT_SND      0.0


cPersonMapper::cPersonMapper()
{
  // using default map parameters
}

cPersonMapper::~cPersonMapper()
{
}

/** This is the main externally facing function that is called to trigger the
 * cPersonMapper to perform an analysis cycle.
 *
 * Each time this function is called the latest sensor map data are
 * used to update the person map.
 *
 * @param pFaceMapper is a pointer to the face-detector-based mapper.
 * @param pMotionMapper is a pointer to the motion-based mapper.
 * @param pSoundMapper is a pointer to the sound-based mapper.
 */
void cPersonMapper::update(cFaceMapper *pFaceMapper,
			   cMotionMapper *pMotionMapper,
			   cSoundMapper *pSoundMapper)
{
  // choose one of two update approaches
  if (UPDATE_DIRECT)
    { updateDirect(pFaceMapper, pMotionMapper, pSoundMapper); }
  else
    { updateAddLeak(pFaceMapper, pMotionMapper, pSoundMapper); }

  if(DEBUG(PERSON_MAPPER))
    { mMap.printMap(20); }
}

/** Force map state to the provided data.
 *
 * This is an alternative update function for the special use of replaying
 * recorded data, simulating, testing, etc.  It's not expected to be used
 * as part of normal program operation.
 *
 * @param people is a vector of cItems that represent the state of the map.
 */
void cPersonMapper::update(std::vector<cItem> people)
{
  mMap.update(people);
}

/** This is an internallly facing function that updates the person map
 * as a leaky integrator, based on heuristics about how the component
 * maps should contribute to the update.
 *
 * @param pFaceMapper is a pointer to the face-detector-based mapper.
 * @param pMotionMapper is a pointer to the motion-based mapper.
 * @param pSoundMapper is a pointer to the sound-based mapper.
 */
void cPersonMapper::updateAddLeak(cFaceMapper *pFaceMapper,
				 cMotionMapper *pMotionMapper,
				 cSoundMapper *pSoundMapper)
{
  int   increment, leak;
  float f, m, s, p; // values for face, motion, sound, person
  const int   kMaxVal = 1000;             //
  const int   kDefaultIncrement = 100;    //
  const int   kFaceThreshold   = 100;     // emperically chosen
  const int   kMotionThreshold = 500;     //
  const int   kSoundThreshold  = 800;     //

  // data from individual maps
  std::vector<int> faceMap   = pFaceMapper->getMap().getMapData();
  std::vector<int> motionMap = pMotionMapper->getMap().getMapData();
  std::vector<int> soundMap  = pSoundMapper->getMap().getMapData();

  // update every map angle
  for (int angle=0; angle<360; angle++)
    {
      // grab component map values
      f = faceMap[angle];
      m = motionMap[angle];
      s = soundMap[angle];
      p = mMap.getValAt(angle);
      // clear the increment amount
      increment = 0;
      // increase the increment for each component map that has a
      // sufficiently large value. Component values are expected to be
      // in [0, 1000]
      if ( f > kFaceThreshold )
	{ increment += kDefaultIncrement; }
      if ( m > kMotionThreshold )
	{ increment += kDefaultIncrement; }
      if ( s > kSoundThreshold )
	{ increment += kDefaultIncrement; }
      // Set the leak equal to the default increment. This means that
      // one map can provide enough evidence to sustain a positive
      // value in the person map, but at least two maps must provide
      // evidence to grow that value to begin with.
      leak = kDefaultIncrement;
      // increment and leak the value
      p = p + increment - leak;
      // ensure value is non-negative and limit to a max value
      if (p < 0)
	{ p = 0; }
      else if (p > kMaxVal)
	{ p = kMaxVal; }
      // set the new value
      mMap.setValAt(angle, p);
    }

  // update item map representation of person map
  mMap.updateItemMap();
}

/** This is an internallly facing function that updates the person map
 * directly, according to a model of how the component maps should be
 * combined.
 *
 * @param pFaceMapper is a pointer to the face-detector-based mapper.
 * @param pMotionMapper is a pointer to the motion-based mapper.
 * @param pSoundMapper is a pointer to the sound-based mapper.
 */
void cPersonMapper::updateDirect(cFaceMapper *pFaceMapper,
				 cMotionMapper *pMotionMapper,
				 cSoundMapper *pSoundMapper)
{
  float f, m, s, p; // values for face, motion, sound, person
  const float kTotalWeight = W_FAC + W_MOT + W_SND
    + W_FAC_FAC + W_MOT_MOT + W_SND_SND
    + W_FAC_MOT + W_FAC_SND + W_MOT_SND;
  const float kScale = 1.5;
  const int   kMaxVal = 1000;

  // data from individual maps
  std::vector<int> faceMap   = pFaceMapper->getMap().getMapData();
  std::vector<int> motionMap = pMotionMapper->getMap().getMapData();
  std::vector<int> soundMap  = pSoundMapper->getMap().getMapData();

  // update every map angle
  for (int angle=0; angle<360; angle++)
    {
      // grab component map values
      f = faceMap[angle];
      m = motionMap[angle];
      s = soundMap[angle];
      // clear person value
      p = 0.0;
      // add in weighted linear terms
      p += W_FAC * f;         // face
      p += W_MOT * m;         // motion
      p += W_SND * s;         // sound
      // add in weighted squared terms
      p += W_FAC_FAC * f * f; // face
      p += W_MOT_MOT * m * m; // motion
      p += W_SND_SND * s * s; // sound
      // add in weighted cross terms
      p += W_FAC_MOT * f * m; // face * motion
      p += W_FAC_SND * f * s; // face * sound
      p += W_MOT_SND * m * s; // motion * sound
      // normalize by the total weight
      p /= kTotalWeight;
      // scale up and limit to a max value
      p *= kScale;
      if (p > kMaxVal)
	{ p = kMaxVal; }
      // set the new value
      mMap.setValAt(angle, p);
    }

  // update item map representation of person map
  mMap.updateItemMap();
}

/** Resets all of the map's stored values to zero. */
void cPersonMapper::resetMap(void)
{
  mMap.resetMap();
}

/** This function gets the value for teh given angle from the underlying map.
 *
 * @param angle is the nominal angle of interest.
 *
 * @return map value at give angle (summed over tolerance range).
 */
int cPersonMapper::getMapValAt(int angle)
{
  return mMap.getValAt(angle);
}

/** This function gets the maximum value within a range of bearings around the
 * given nominal angle.
 *
 * @param nominalAngle is the nominal angle of interest.
 * @param plusMinus is the +/- range to search.
 *
 * @return maximum map value near the given angle.
 */
int cPersonMapper::getAngleWithMaxVal(int nominalAngle, int plusMinus)
{
  return mMap.getAngleWithMaxVal(nominalAngle, plusMinus);
}

/** This function returns the map which is keeping track of the positional data
 * of the things found by the cPersonMapper.
 *
 * @return the map containing a vector of integers.
 */
cMap cPersonMapper::getMap(void)
{
  return mMap;
}

/** Pass-through function used to get list of all items on the instantaneous
 * item map (last frame).
 *
 * @return a vector of cItem pointers that represent all the items on the map
 * as detected in the current update cycle (with no memory of the past).
 */
const std::vector<const cItem *> cPersonMapper::getInstantItemMap(void)
{
  return mMap.getInstantItemMap();
}

/** Pass-through function used to get list of all items on the map.
 *
 * @return a vector of cItem pointers that represent all the items on the map,
 * which have been filtered to result in a map with some memory persistance
 * and some rejection of short-term events.
 */
const std::vector<const cItem *> cPersonMapper::getLongTermItemMap(void)
{
  return mMap.getLongTermItemMap();
}

/** Pass-through function used to find the person in the map who's nominal 
 * bearing is closest to the given bearing.
 *
 * Note: If no match is found null pointer is returned.
 *
 * @param int bering is the direction to match to people in the map [0, 360) deg.
 *
 * @return a pointer to the cItem (representing a person) that is closest to the
 * given bearing.
 */
cItem * cPersonMapper::getPersonAt(int bearing)
{
  return mMap.getLongTermItemAt(bearing);
}
