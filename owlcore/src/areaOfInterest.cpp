#include "areaOfInterest.hpp"
#include "circleMath.hpp"
#include "common.hpp"
#include "debugSwitches.h"
#include "logging.hpp"
#include "params.h"

#include <algorithm>


#define DEFAULT_AOI_DEG_WIDTH  (int)((2 / 3.) * STAGE_DEG_WIDTH)
#define DEFAULT_AOI_DEG_HEIGHT 0  // Not currently in use

/** Construct an AOI with highest display priority (MUST_SHOW).
 * 
 * @param voice is the cVoice object that is associated with this AOI.  If this
 * is an AOI for an active speaker then this voice is what caused this AOI to be
 * created.
 * @param pPano is a pointer to the image source where this AOI should take 
 * data from.
 * @param patch is the image area to assign to this new area of interest.
 * @param id is a unique numerical identifier for this new area of interest.
 * @param pinned indicates if this should be a pinned (true) or normal (false)
 * AOI.
 */
cAreaOfInterest::cAreaOfInterest(cVoice voice, cPanorama *pPano, 
				 owl::cPatch patch, int id,
				 bool pinned )
{
  init(id, voice, pPano, patch, ePriority_t {MUST_SHOW}, pinned);
}

/** Construct an AOI with the given display priority.
 * 
 * @param voice is the cVoice object that is associated with this AOI.  If this
 * is an AOI for an active speaker then this voice is what caused this AOI to be
 * created.
 * @param pPano is a pointer to the image source where this AOI should take 
 * data from.
 * @param patch is the image area to assign to this new area of interest.
 * @param id is a unique numerical identifier for this new area of interest.
 * @param priority is an ePriority_t that tells the Visual Editor how important
 * it is that this AOI's Subframe be placed in the Layout and displayed.
 * @param pinned indicates if this should be a pinned (true) or normal (false)
 * AOI.
 */
cAreaOfInterest::cAreaOfInterest(cVoice voice, cPanorama *pPano, 
				 owl::cPatch patch, int id,
				 ePriority_t priority, bool pinned )
{
  init(id, voice, pPano, patch, priority, pinned);
}

/** Contructor common code
 *
 * @param id is a unique numerical identifier for this new area of interest.
 * @param voice is the cVoice object that is associated with this AOI.  If this
 * is an AOI for an active speaker then this voice is what caused this AOI to be
 * created.
 * @param pPano is a pointer to the image source where this AOI should take
 * data from.
 * @param patch is the image area to assign to this new area of interest.
 * @param priority is an ePriority_t that tells the Visual Editor how important
 * it is that this AOI's Subframe be placed in the Layout and displayed.
 * @param pinned indicates if this should be a pinned (true) or normal (false)
 * AOI.
 */
void cAreaOfInterest::init(int id, cVoice voice, cPanorama *pPano, owl::cPatch patch,
			   ePriority_t priority, bool pinned )
{
  currentTimeSpec(&mStartTime);
  mWatchdog = mStartTime;
  
  cv::Size spaceSize = cv::Size(CV_PANO_RES_X, CV_PANO_RES_Y);
  mId = id;
  mPriority = priority;
  mPinned = (pinned ? PINNED_EDITABLE : NOT_PINNED);
  mpPano = pPano;
  mFaceTracker.setSpaceSize(spaceSize);
  mFaceDetector.setSpaceSize(spaceSize);
  mEnabled = true;
  mActive = true;
  mReadyToRemove = false;

  setPatch(patch);

  mVoiceTracker.setVoice(voice);
  createSubframe();

  if(DEBUG(AOI) || DEBUG(FACE_DETECTOR)) {
    LOGI("Focus on pixel %d (%d degree mark)\n",
	 mSubframe.getFocus().x, voice.getDirection() ); }
}

cAreaOfInterest::~cAreaOfInterest()
{
}

/** Allow voice tracker associated with this AOI to analyze the latest data
 *
 * @param voices is a vector of cVoice objects that represent all of the 
 * different sound sources the microphone module thinks it's hearing.
 *
 * @return the input vector with the voice(s) that matched this AOI removed.
 */ 
std::vector<cVoice> 
cAreaOfInterest::updateVoiceTracker(std::vector<cVoice> voices)
{
  return mVoiceTracker.update(voices);
}

/** Allow cFaceTracker associated with thsi AOI to try to find the expected face
 *
 * This function looks for a face in the expected region, and adjusts the size 
 * of the future search region, depending on the results.  It can also shift the
 * point in the AOI that should be focused on (in the sense of paying attention,
 * not in the sense of optical focus).
 */
void cAreaOfInterest::updateFaceTracking(void)
{
/// Face Tracking in the AOI disabled for the time being because it is too
/// computationally intensive for the Snapdragon. This code is being left here
/// so that it is easier to re-enable if we choose to do so in the future.
/// However, in order to do so faceTracker will need to be updated to work in
/// angles instead of pixels.
///
///--------------------------------------------------
///  const float kFocusTolerance = 0.10;
///  const int kFaceWidthScalar = 2;
///
///  bool faceFound = false;
///  int minReqHeight, minReqWidth;
///
///  // look for face & update tracker
///  std::vector<cv::Rect> faces = findFaces();
///
///  std::vector<cv::Rect> leftOverFaces = mFaceTracker.update(faces);
///
///  if(faces.size() != leftOverFaces.size())
///    {faceFound = true;}
///
///  // update point in panorama's image that subframe is focused on (if needed)
///  int threshold = (int)(mSubframe.getSize().width * kFocusTolerance);
///
///  if(mFaceTracker.isFocusShifted(threshold))
///    {
///      cv::Point focus = mFaceTracker.getFocus();
///      mSubframe.adjustFocus(focus);
///    }
///
///  // update future search ROI & subframe's minimum required size
///  cv::Rect face = mFaceTracker.getFace();
///
///  setFaceSearchRoi(face, faceFound);
///
///  if(!faceFound)
///    {minReqWidth = NO_FACE_AOI_WIDTH;}
///  else
///    {minReqWidth = std::max((kFaceWidthScalar * face.width), MIN_AOI_WIDTH);}
///
///  minReqWidth = std::min(minReqWidth,  OUTPUT_WIDTH);
///  minReqHeight = face.height;
///
///  mSubframe.setMinReqSize(cv::Size(minReqWidth, minReqHeight));
///--------------------------------------------------
}

/** This function does everything that is needed to mark the AOI to be killed 
 * by the cAttentionSystem. */
void cAreaOfInterest::expire(void)
{
  disable();
  setReadyToRemove();
}

/** Disable this AOI */
void cAreaOfInterest::disable(void)
{
  deactivate();
  mEnabled = false;
}

/** Deactive this AOI */
void cAreaOfInterest::deactivate(void)
{
  mActive = false;
  mPinned = NOT_PINNED;
}

/** Reset this AOI's Voice Tracker */
void cAreaOfInterest::forceVoiceTrackerReset(void)
{
  mVoiceTracker.forceReset();
}

/** Adjusts the Aoi's patch focus as well as the subframe's focus.
 *
 * This is a fast-pan of the camera.
 *
 * @param theta is an int that defines the bearing angle of the patch's focus.
 * @param phi is an int that defines the elevation angle of the patch's focus.
 */
void cAreaOfInterest::adjustPatchFocus(int theta, int phi)
{
  // adjust AOI's focus and get the subframe's focus (in stage pixels)
  cv::Point newFocus = updatePatchFocus(theta, phi);

  // adjust Subframe's focus
  mSubframe.adjustFocusFast(newFocus);
}

/** Recenter AOI/Subframe by adjusting the AOI's patch focus, but animate the
 * Subframe very, very slowly so that the adjustment isn't obvious (and
 * annoying) to the user.
 *
 * @param theta is an int that defines the bearing angle of the patch's focus.
 * @param phi is an int that defines the elevation angle of the patch's focus.
 */
void cAreaOfInterest::recenterPatch(int theta, int phi)
{
  // adjust AOI's focus and get the subframe's focus (in stage pixels)
  cv::Point newFocus = updatePatchFocus(theta, phi);
  
  // adjust Subframe's focus, but don't change animation speed if it's already
  // in-animation
  if(!mSubframe.isAnimating())
    {
      mSubframe.adjustFocusSlow(newFocus);
    }
  else
    {
      mSubframe.adjustFocus(newFocus);
    }
}

/** Update AOI's patch's focus & figure out subframe's corresponding focus.
 *
 * @param theta is an int that defines the bearing angle of the patch's focus.
 * @param phi is an int that defines the elevation angle of the patch's focus
 *
 * @return a cv::Point containing the  (x,y) pixel coordinates of the Subframe's
 * updated focus.
 */
cv::Point cAreaOfInterest::updatePatchFocus(int theta, int phi)
{
  // update the Aoi patch's focus (in degrees)
  mPatch.theta = theta;
  mPatch.phi = phi;
  
  // find subframe's new focus (in stage pixels)
  cv::Point newFocus = cv::Point(mpPano->pixelAt(mPatch.theta),
				 mpPano->pixelAt(mPatch.phi) );
  return newFocus;
}

/** Adjusts the Aoi's patch size as well as the subframe's minimum required size.
 *
 * @param dTheta is an int that defines the angular width of the patch.
 * @param dPhi is an int that defines the angular height of the patch.
 * @param override if true, then apply changes even if AOI is pinned.
 */
void cAreaOfInterest::adjustPatchSize(int dTheta, int dPhi, bool override)
{
  if (!isPinned() || override)
    {
      // update the Aoi patch's size (in degrees)
      mPatch.dTheta = dTheta;
      mPatch.dPhi = dPhi;

      // update subframe's minimum required size (in stage pixels)
      mSubframe.setMinReqSize(mPatch.getSizeInStagePix());
    }
}

/** Update watchdog to current time. */
void cAreaOfInterest::petWatchdog(void)
{
  currentTimeSpec(&mWatchdog);
}

/** Pass-through function to test if the given bearing is within the bounds of
 * this AOI.
 *
 * @param bearing is an integer angle [0, 360] to be checked.
 * @return true is given bearing is within bounds of this AOI.
 */
bool cAreaOfInterest::contains(int bearing)
{
  return mPatch.contains(bearing);
}

/** Set the display priority for this AOI's Subframe.
 *
 * The Visual Editor uses this to decide how/if/when this AOIs Subframe is
 * integrated into the display's Layout.
 *
 * @param priority is an ePriority_t that denotes the AOI's display priority.
 */
void cAreaOfInterest::setPriority(ePriority_t priority)
{
  mPriority = priority;
}

/** Set flag that indicates that this AOI can be dropped by the 
 *    cAttentionSystem. */
void cAreaOfInterest::setReadyToRemove(void)
{
  mReadyToRemove = true;
}

/** Assign new voice to AOI's cVoiceTracker.
 *
 * This function is used to change the voice that the AOI is tracking as its
 * primary interest point.
 *
 * @param voice is the cVoice to pass into the cVoiceTracker.
 */
void cAreaOfInterest::setVoice(cVoice voice)
{
  mVoiceTracker.setVoice(voice);
}

/** Force a new bearing for this AOI's voice tracker.
 *
 * @param direction is the new bearing (integral degrees)
 */
void cAreaOfInterest::setVoiceDirection(int direction)
{
  mVoiceTracker.setDirection(direction);
}

/** Assign the dimensions of the region that define the boundaries of the Aoi.
 *
 * @param a owl::cPatch that defines the focus (x and y) and size
 * (width and height) of the Aoi region in degrees.
 */
void cAreaOfInterest::setPatch(owl::cPatch patch)
{
  mPatch = patch;
}

/** @return whether or not this AOI is Enabled. */
bool cAreaOfInterest::isEnabled(void)
{
  return mEnabled;
}

/** @return the flag that indicates if this AOI can be dropped by the 
    cAttentionSystem. */
bool cAreaOfInterest::isReadyToRemove(void)
{
  return mReadyToRemove;
}

/** @return pinned state of type ePinState_t of the AOI. */
cAreaOfInterest::ePinState_t cAreaOfInterest::getPinnedState(void)
{
  return mPinned;
}

/** @return true if the AOI is pinned. */
bool cAreaOfInterest::isPinned(void)
{
  return (mPinned != NOT_PINNED);
}

/** Set editability state of a pinned AOI. */
void cAreaOfInterest::setPinnedEditable(bool editable)
{
  if (mPinned != NOT_PINNED)
    {
      if (editable)
        { mPinned = PINNED_EDITABLE; }
      else
        { mPinned = PINNED_FIXED; }
    }
}

/** @return the Id of this AOI. */
int cAreaOfInterest::getId(void)
{
  return mId;
}

/** @return the display priority of this AOI's Subframe. */
cAreaOfInterest::ePriority_t cAreaOfInterest::getPriority(void)
{
  return mPriority;
}

/** @return the state of the cVoiceTracker */
voiceState_t cAreaOfInterest::getVoiceTrackerState(void)
{
  return mVoiceTracker.getState();
}

/** @return the state of the cFaceTacker */
faceState_t cAreaOfInterest::getFaceTrackerState(void)
{
  return mFaceTracker.getState();
}

/** @return the rectangle representing the face search area */
cv::Rect cAreaOfInterest::getFaceSearchRoi(void)
{
  return mFaceSearchRoi;
}

/** @return the rectangle representing the detected face */
cv::Rect cAreaOfInterest::getFace(void)
{
  return mFaceTracker.getFace();
}

/** @return the cSubframe that is associated with this AOI */
cSubframe * cAreaOfInterest::getSubframe(void)
{
  return &mSubframe;
}

/** @return a pointer to the cVoice being tracked by the cVoiceTracker */
cVoice * cAreaOfInterest::getVoice(void)
{
  return mVoiceTracker.getVoice();
}

/** @return a pointer to the owl::cPatch that defines the Aoi in spherical polar
 * coordinates.
 */
owl::cPatch * cAreaOfInterest::getPatch(void)
{
  return &mPatch;
}

/** return theta value of the AOI's patch. */
int cAreaOfInterest::getTheta(void)
{
  return mPatch.theta;
}

/** return dTheta value of the AOI's patch. */
int cAreaOfInterest::getDTheta(void)
{
  return mPatch.dTheta;
}

/** return phi value of the AOI's patch. */
int cAreaOfInterest::getPhi(void)
{
  return mPatch.phi;
}

/** return dPhi value of the AOI's patch. */
int cAreaOfInterest::getDPhi(void)
{
  return mPatch.dPhi;
}

/** @return a timespec with the last time the watchdog was pet. */
struct timespec cAreaOfInterest::getWatchdog(void)
{
  return mWatchdog;
}

/** Create a new cSubframe for this AOI
 *
 * A cSubframe is a unit that is used by the Visual Editor to create the system's
 * output video.  Currently, a "Speaker Spotlight" (active conversation 
 * participant) is the only "real" type of AOI from which subframes are 
 * (there is a hacky function available to generate a panormaic subframe) 
 * generated. The focus and size of the subframe are defined by the Aoi's patch
 * converted into stage pixels.
 *
 * /// Face tracking currently disabled, so this does not apply:
 * /// A region to look in when the AOI's cFaceDetector searches for faces is
 * /// also generated by this function.
 */ 
void cAreaOfInterest::createSubframe(void)
{
/// Face Tracking in the AOI disabled for the time being because it is too
/// computationally intensive for the Snapdragon. This code is being left here
/// so that it is easier to re-enable if we choose to do so in the future.
/// However, in order to do so this will need to be updated to work in angles
/// instead of pixels.
///--------------------------------------------------
///  int width = MAX_ROI_WIDTH;
///  int height = MAX_ROI_HEIGHT;
///  int x = (int)(focusX - (width / 2.f));
///  int y = 0;
///
///  mFaceSearchRoi = cv::Rect(x, y, width, height);
///
/// Before we switched AOI to degrees & disabled faceTracking, focus was:
/// cv::Point focus = cv::Point(focusX, (int)(mFaceSearchRoi.y / 2.f));
///--------------------------------------------------

  // convert patch's focus from degrees to alignment-angle corrected stage pixels
  cv::Point focus = cv::Point(mpPano->pixelAt(mPatch.theta),
			      mpPano->pixelAt(mPatch.phi) );

  // convert patch's size from degrees to stage pixels
  cv::Size size = mPatch.getSizeInStagePix();

  mSubframe = cSubframe(focus, size);
  mSubframe.setPanorama(mpPano);
  mSubframe.setMinReqSize(size);
}

/** Search a sub-region of the AOI for faces 
 *
 * Note: Part of why this function is so complicated us because it needs to deal
 * with the possibility that the face is near the arbitrarily placed vertical 
 * boundaries of the unrolled imaged.  
 *
 * @return a vector of rectangles that contain found faces.
 */ 
std::vector<cv::Rect> cAreaOfInterest::findFaces(void)
{
  cv::Mat *pSearchImage = mpPano->getGrayImage();
  cv::Mat searchSubImage = cv::Mat(cv::Size(mFaceSearchRoi.width,
					    mFaceSearchRoi.height),
				   pSearchImage->type() );;
  std::vector<cv::Rect> searchRois;
  
  // massage ROI (in case it extends past the edge of the source image)
  searchRois = circleMath::cropRect(pSearchImage->size(), mFaceSearchRoi);
  
  if(searchRois.size() == 1)
    { // this avoids the extra Mat copy operation found in the "else" below
      searchSubImage = cv::Mat(*pSearchImage, mFaceSearchRoi);
    }
  else
    {
      // put together search sub-image
      int x = 0; int y = 0;
      
      for(std::vector<cv::Rect>::iterator iRoi = searchRois.begin();
	  iRoi != searchRois.end(); ++iRoi )
	{
	  cv::Rect dstRoi(x, y, (*iRoi).width, (*iRoi).height);
	  
	  if(DEBUG(AOI) || DEBUG(FACE_DETECTOR)) {
	    LOGI("src = (%d, %d) %d x %d\n", (*iRoi).x, 
		   (*iRoi).y, (*iRoi).width, (*iRoi).height );
	    LOGI("dst = (%d, %d) %d x %d\n", dstRoi.x, 
		   dstRoi.y, dstRoi.width, dstRoi.height );
	    
	    LOGI("searchImage width = %d, height = %d\n", 
		   pSearchImage->size().width, pSearchImage->size().height );
	    LOGI("searchSubImage width = %d, height = %d\n",
		   searchSubImage.size().width, searchSubImage.size().height ); }
	  
	  cv::Mat src(*pSearchImage, *iRoi);
	  cv::Mat dst(searchSubImage, dstRoi);
	  
	  src.copyTo(dst);
	  
	  x += (*iRoi).width;
	}
    }
  
  // Find faces!
  std::vector<cv::Rect> faces = mFaceDetector.detectFaces(&searchSubImage);

  if(DEBUG(FACE_DETECTOR)) {
    LOGI("%d faces found!\n", faces.size()); }

  // adjust found faces to panoramic camera image coordinate system
  // (these faces were returned in searchSubImage's coordinate system)
  for(std::vector<cv::Rect>::iterator iFace = faces.begin();
      iFace != faces.end(); ++iFace )
    {
      (*iFace).x = (*iFace).x + mFaceSearchRoi.x; 
      (*iFace).y = (*iFace).y + mFaceSearchRoi.y;

      if((*iFace).x > CV_PANO_RES_X)
	{(*iFace).x -= CV_PANO_RES_X;}
    }

  return faces;
}

/** Adjust the size of the region in the AOI where faces will be searched for
 *
 * If a face was found previously, then the future search region is made a bit
 * bigger than that face (and at the same location).  If a face was not found
 * then the search region is based on the previous one, but it is made larger
 * (in hopes of reacquiring the "missing" face). 
 *
 * @param face is a rectangle that may represent the bounds of a found face.
 * @param faceFound is a flag that indicates whether a face was actually found
 * previously.
 */
void cAreaOfInterest::setFaceSearchRoi(cv::Rect face, bool faceFound)
{
  const float kGrowRate = 0.20;
  const int   kMinWidth  = 0.5 + MIN_ROI_WIDTH  / (1.0 + kGrowRate);
  const int   kMinHeight = 0.5 + MIN_ROI_HEIGHT / (1.0 + kGrowRate);

  if(faceFound)
    {
      // set search area to the (relatively small) face rect
      mFaceSearchRoi = face;
      // ensure width is not too small
      if (face.width < kMinWidth)
	{
	  mFaceSearchRoi.width = kMinWidth;
	  mFaceSearchRoi.x -= (kMinWidth - face.width) / 2;
	}
      // ensure height is not too small
      if (face.height < kMinHeight)
	{
	  mFaceSearchRoi.height = kMinHeight;
	  mFaceSearchRoi.y -= (kMinHeight - face.height) / 2;
	}
    }

  int dx = (int)(0.5 + kGrowRate * mFaceSearchRoi.width);
  int dy = (int)(0.5 + kGrowRate * mFaceSearchRoi.height);

  // grow search region
  growFaceSearchRoi(dx, dy);
}

/** This function takes care of the nitty-gritty of increasing the size of the
 *  region where faces will be searched for.
 *
 * @param dx is the requested size by which to grow the horizontal bound of the 
 * search region.
 * @param dy is the requested size by which to grow the vertical bound of the
 * search region.
 */
void cAreaOfInterest::growFaceSearchRoi(int dx, int dy)
{
  if(mFaceSearchRoi.width != MAX_ROI_WIDTH)
    {
      mFaceSearchRoi.width += dx;
      mFaceSearchRoi.height += dy;
      
      // limit search region
      // TODO: This should be done by angle, or something, instead of pixel
      if(mFaceSearchRoi.width > MAX_ROI_WIDTH)
	{
	  dx -= (mFaceSearchRoi.width - MAX_ROI_WIDTH);
	  mFaceSearchRoi.width = MAX_ROI_WIDTH;
	}
      if(mFaceSearchRoi.height > MAX_ROI_HEIGHT)
	{
	  dy -= (mFaceSearchRoi.height - MAX_ROI_HEIGHT);
	  mFaceSearchRoi.height = MAX_ROI_HEIGHT;
	}
      
      mFaceSearchRoi.x -= (int)(dx / 2.f);
      mFaceSearchRoi.y -= (int)(dy / 2.f);       

      // keep witin bounds
      mFaceSearchRoi.y = std::max(0, mFaceSearchRoi.y);

      if((mFaceSearchRoi.y + mFaceSearchRoi.height) > CV_PANO_RES_Y)
	{
	  mFaceSearchRoi.height -= ((mFaceSearchRoi.y + mFaceSearchRoi.height) -
				    CV_PANO_RES_Y );
	}
      // Note-val is allowed to be outside the bounds of the panorama
      //       image, because the x-axis is actually cylindrical and wrap around
      //       is accomondated in subsequent use of this ROI.
    }
}

/** Debug helper function to print out the parameter values of the face search 
 * region-of-interest.
 *
 * @param tag is a string that will be printed out before the parameter values.
 */
void cAreaOfInterest::printFaceSearchRoi(std::string tag = "")
{
  LOGI("%s: \n", tag.c_str());
  LOGI("mFaceSearchRoi.x = %d\n", mFaceSearchRoi.x);
  LOGI("mFaceSearchRoi.width = %d\n", mFaceSearchRoi.width);
  LOGI("mFaceSearchRoi.y = %d\n", mFaceSearchRoi.y);
  LOGI("mFaceSearchRoi.height = %d\n", mFaceSearchRoi.height);  
}
