#include "common.hpp"
#include "logging.hpp"
#include "mapsRecorder.hpp"

#include <stdio.h>
#include <string.h>

/** Default constructor. */
cMapsRecorder::cMapsRecorder(void)
{
  init("", "");
}

/** Constructor.
 *
 * @param personDataFilename is the full path for recording person map data.
 * @param voiceDataFilename is the full path for recording voice map data.
 */
cMapsRecorder::cMapsRecorder(std::string personDataFilename,
			       std::string voiceDataFilename )
{
  init(personDataFilename, voiceDataFilename);
}

cMapsRecorder::~cMapsRecorder()
{
  stop();
}

/** Common initialization code used by constructors. 
 *
 * @param personDataFilename is the full path for recording person map data.
 * @param voiceDataFilename is the full path for recording voice map data.
 */
void cMapsRecorder::init(std::string personDataFilename,
			 std::string voiceDataFilename )
{
  setPersonDataFilename(personDataFilename);
  setVoiceDataFilename(voiceDataFilename);  
}

/** Start recording session.
 *
 * @return true if record files were able to be created.
 */
bool cMapsRecorder::start(void)
{
  if(!openPersonFile())
    {
      LOGE("Couldn't open person map record file\n");
      return false;
    }

  if(!openVoiceFile())
    { 
      LOGE("Couldn't open voice map record file\n");
      return false;
    }

  // note start time
  currentTimeSpec(&mStartTime);

  // prep csv files with header line
  writeHeaders();

  return true;
}

/** Stop recording session. */
void cMapsRecorder::stop(void)
{
  closeFiles();
}

/** Record the given person and voice map states.
 *
 * @param personMap is the person map (assumed current) to save.
 * @param voiceMap is the voice map (assumed current) to save.
 *
 * @return true on success.
 */
bool cMapsRecorder::recordState(std::vector<const cItem *> personMap, 
				std::vector<cVoice> voiceMap )
{
  // get current relative timestamp
  double time = elapsedTimeSinceSec(mStartTime);

  char personDataString[100];  // 100 is arbitrarily large
  char voiceDataString[100];   // 100 is arbitrarily large

  sprintf(personDataString, "%4.3f", time);
  sprintf(voiceDataString, "%4.3f", time);

  // save person map data
  for(auto pItem : personMap)
    {
      sprintf((personDataString + strlen(personDataString)), ", %d, %3.1f", 
	      pItem->getThetaMean(), 
	      pItem->getThetaStdDev() );
    }
  
  sprintf((personDataString + strlen(personDataString)), "\n");

  mPersonFileStream << personDataString;

  // save voice map data
  for(auto voice : voiceMap)
    {
      sprintf((voiceDataString + strlen(voiceDataString)), ", %d", 
	      voice.getDirection() );
    }
  
  sprintf((voiceDataString + strlen(voiceDataString)), "\n");
  
  mVoiceFileStream << voiceDataString;

  return true;
}

/** Store full path to person map record file.
 *
 * @param personDataFilename person map record data filename.
 */
void cMapsRecorder::setPersonDataFilename(std::string personDataFilename)
{
  mPersonDataFilename = personDataFilename;
}

/** Store full path to voice map record file.
 *
 * @param voiceDataFilename voice map record data filename.
 */
void cMapsRecorder::setVoiceDataFilename(std::string voiceDataFilename)
{
  mVoiceDataFilename = voiceDataFilename;
}

/** Open person map record file.
 *
 * @return true on success.
 */
bool cMapsRecorder::openPersonFile(void)
{
  mPersonFileStream.open(mPersonDataFilename, std::ofstream::out);

  if(mPersonFileStream.is_open())
    { return true; }
  else
    { return false; }
}

/** Open voice map record file.
 *
 * @return true on success.
 */
bool cMapsRecorder::openVoiceFile(void)
{
  mVoiceFileStream.open(mVoiceDataFilename, std::ofstream::out);

  if(mVoiceFileStream.is_open())
    { return true; }
  else
    { return false; }
}

/** Close person and voice map record files. */
void cMapsRecorder::closeFiles(void)
{
  mPersonFileStream.close();
  mVoiceFileStream.close();
}

/** Write CSV column headers to person and voice map files. */
void cMapsRecorder::writeHeaders(void)
{
  mPersonFileStream << "time (s), bearing 1 (deg), width 1 (std dev deg), ...\n";
  mVoiceFileStream << "time (s), bearing 1 (deg), bearing 2 (deg), ...\n";
  mPersonFileStream.flush();
  mVoiceFileStream.flush();
}
