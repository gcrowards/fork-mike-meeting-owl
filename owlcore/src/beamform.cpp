#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>

#include "AudioMgr.hpp"
#include "beamform.hpp"
#include "beamMath.hpp"
#include "common.hpp"
#include "logging.hpp"
#include "params.h"


cBeamform::cBeamform()
{
  mAngle = 0;
  beamMath::getBeamDelays(mBeamDelays, 1);

  // initialize mutex
  if (pthread_mutex_init(&mMutex, NULL) != 0)
    { LOGE("unable to init beamform mutex\n"); }
}

cBeamform::~cBeamform()
{
  // destroy mutex
  pthread_mutex_destroy(&mMutex);
}

void cBeamform::setOwner(cAudioMgr* owner)
{
  mOwner = owner;
}

/** This function performs the delay-and-sum calculation for
 * beamforming.
 *
 * @param pSum is a pointer to the result, a single output channel (no
 * interleaving).
 * @param pPcmIn is a pointer to the PCM sound input data, expected to
 * be N_MICS interleaved channels.
 * @param bufLen is the input data buffer length.
 * @param pDelays is a pointer to the delays used for each input channel.
 */
void cBeamform::delayAndSum(int16_t *pSum, int16_t *pPcmIn, int bufLen, int *pDelays)
{
  int  i, m, nSamplesPerMic = bufLen / N_MICS;
  int  idxIn[N_MICS];

  // set first mic
#ifdef PROTOTYPE_P0_5
  static int firstMic = 4; // skip first four mics for P05
  const  int kBitShift= 2; // exponent to scale down values to avoid overflow
#else
  static int firstMic = 0; // use all eight mics for P1
  const  int kBitShift= 3; // exponent to scale down values to avoid overflow
#endif

  // initialize indexes, assuming that the data are properly buffered
  // so we won't exceed array bounds with negative delays
  for (m = 0; m < N_MICS; m++)
    { idxIn[m] = - pDelays[m] * N_MICS + m; } // negative delay + mic offset

  for (i = 0; i < nSamplesPerMic; i++)
    {
      pSum[i] = 0; // clear the sum
      // loop over all supported mic channels
      // TODO: consider using only mics that approximately face the sound source
      for (m = firstMic; m < N_MICS; m++)
	{
	  // add the delayed input value, scaled to avoid overflow
	  pSum[i] += ( pPcmIn[ idxIn[m] ] >> kBitShift );
	  idxIn[m] += N_MICS; // advance input index
	}
    }
}

/** This function gets the desired angle for beamforming.
 *
 * @return the the desired angle in degrees [0, 359].
 */
int  cBeamform::getAngle(void)
{
  int angle;

  // protect mAngle with a mutex since access happens on different
  // threads (in cOwlHook)
  pthread_mutex_lock(&mMutex);
  angle = mAngle;
  pthread_mutex_unlock(&mMutex);
  return angle;
}

/** This function sets the desired angle for beamforming.
 *
 * @param angle is the desired angle in degrees, enforced to [0, 359].
 */
void cBeamform::setAngle(int angle)
{
  // enforce [-359, +359]
  angle = angle % 360;
  // enforce [0, 359]
  if (angle < 0)
    { angle += 360; }
  // protect mAngle with a mutex since access happens on different
  // threads (in cOwlHook)
  pthread_mutex_lock(&mMutex);
  mAngle = angle;
  pthread_mutex_unlock(&mMutex);
}

/** This function is called repeatedly for audio output processing; it
 * runs on a high-priority thread as part of the main A/V pipeline and
 * is intended for quickly assembling the UAC sound output.
 *
 * @param pPcmOut is a pointer to the PCM sound output data, expected
 * to be two interleaved channels.
 * @param pPcmIn is a pointer to the PCM sound input data, expected to
 * be N_MICS interleaved channels.
 * @param bufLen is the input data buffer length, expected to be no more
 * than BEAM_MAX_PCM_BUF_SIZE.
 */
void cBeamform::update(int16_t *pPcmOut, int16_t *pPcmIn, int bufLen)
{
  int    beam, angle;
  const  float kAngleToBeam = (float) N_BEAMS / 360;
  static int16_t pBeamSignal[BEAM_MAX_PCM_BUF_SIZE / N_MICS];
  static int selectedBeam = -1;

  // set up a buffer for storing the current PCM chunk plus any
  // portion of the last chunk needed for time-shifting the audio
  // signals. pPcmInCopy is a pointer to the start of the latest
  // chunk, with negative array indexes allowed, up to a value
  // corresponding to a delay of BEAM_MAX_TAU.
  static int16_t pPcmBuf[BEAM_MAX_PCM_BUF_SIZE];
  const  int     kPadLen = BEAM_MAX_TAU * N_MICS;
  int16_t *pPcmInCopy = &pPcmBuf[kPadLen];

  // validate the buffer size, in case libOwlPG changes its buffer size
  if (bufLen > BEAM_MAX_PCM_BUF_SIZE)
    {
      // we should never get here; if we do, the application will spew warnings
      LOGW("cBeamForm::update() PCM buffer size too big");
      return;
    }

  // copy the new chunk into the buffer; the end of the previous chunk
  // is already in place (see below)
  memcpy(pPcmInCopy, pPcmIn, bufLen * sizeof(int16_t));

  // get the desired angle for beamforming
  angle = getAngle(); // note that getAngle() provides mutex protection

  // convert angle to the beam number
  beam = 0.5 + kAngleToBeam * angle;
  if (beam >= N_BEAMS ) // check if rounding forces a wraparound
    { beam -= N_BEAMS; }

  // do the delay-and-sum beamforming
#ifdef NO_BEAMFORMING
  // override the beam delays to use zeros instead, i.e., no time-shifting
  // HMB: fix this; summing with zero delays is still a beamformer.
  static int noDelays[N_MICS] = {0};
  delayAndSum(pBeamSignal, pPcmInCopy, bufLen, noDelays);
#else
  AUDIO_PROPERTIES props;
  GetAudioProperties(&props);
  if(props.enableBeam > 1)
  {
    beam = props.enableBeam - 2;
    if(beam > 31)
      {beam = 31;}
  }
  if(beam != selectedBeam)
  {
   printf("Selected Beam Changed %d \n",beam);
   selectedBeam = beam;
  }
  delayAndSum(pBeamSignal, pPcmInCopy, bufLen, mBeamDelays[beam]);
#endif

  // for now, make both output channels a copy of the beam signal
  // TODO: consider making each output channel a weighted average of
  // up to three beam signals, depending on what is currently shown on
  // the stage
  int i, j, nSamplesPerMic = bufLen / N_MICS;
  for (i = 0, j = 0; i < nSamplesPerMic; i++)
    {
      pPcmOut[j+0] = pBeamSignal[i]; // left output
      pPcmOut[j+1] = pBeamSignal[i]; // right output
      j += 2; // advance to next output sample
    }

  // handle debugging as needed
  static int debugCount=0;
  const  int kDebugInterval=100;
  if ( DEBUG(BEAMFORMER) )
    {
      // visualize occasionally
      if (debugCount % kDebugInterval == 0)
	{
#ifdef NO_BEAMFORMING
	  int noDelays[] = { 0, 0, 0, 0, 0, 0, 0, 0};
	  visualize(pPcmOut, pPcmInCopy, bufLen, noDelays); // unshifted signals
#else
	  visualize(pPcmOut, pPcmInCopy, bufLen, mBeamDelays[beam]);
#endif
	  LOGD("ANGLE: %3d   BEAM: %2d\n", angle, beam);
	}
      debugCount++;
    }

  // prep for next call by copying the end of the chunk to start of the buffer
  memcpy(pPcmBuf, &pPcmIn[bufLen - kPadLen], kPadLen * sizeof(int16_t));
}

/** This function is for visualizing the beamforming calculations; it
 * should not be called too often and should only be used for
 * debugging purposes.
 *
 * @param pPcmOut is a pointer to the PCM sound output data, expected
 * to be two interleaved channels.
 * @param pPcmIn is a pointer to the PCM sound input data, expected to
 * be N_MICS interleaved channels.
 * @param bufLen is the input data buffer length.
 * @param pDelays is a pointer to the delays used for each input channel.
 */
void cBeamform::visualize(int16_t *pPcmOut, int16_t *pPcmIn, int bufLen, int *pDelays)
{
  int  i, iMax, m;
  int  idxIn[N_MICS], idxOut, pos;
  int  nSamplesPerMic, nNegative;
  char str[256];
  float val;
  const int kMaxVal = 7500;  // scale of input data
  const int kMaxLen =  140;   // width of console output, minus some margin
  const int kMaxLines = 40;  // height of console output, minus some margin
  const int kPos0 = kMaxLen / 2; // string position of data value 0

  // some blank lines
  for (int j = 0; j < 5; j++) { LOGD("\n"); }

  // initialize indexes, assuming that the data are properly buffered
  // so we won't exceed array bounds with negative delays
  i = 0;
  for (m = 0; m < N_MICS; m++)
    { idxIn[m] = (i - pDelays[m]) * N_MICS + m; } // delay + mic offset
  idxOut = i * 2;

  // look for a rising zero crossing to trigger on; count the number
  // of negative values to help decide if we have a good zero crossing
  nNegative = 0;
  nSamplesPerMic = bufLen / N_MICS;
  while (i < nSamplesPerMic)
    {
      val = pPcmIn[ idxIn[4] ]; // pick one channel; consider generalizing
      if (val < 0)
	{ nNegative++; }
      else
	{
	  if (nNegative > 10)  // count has exceeded threshold, so
			       // break to accept the crossing
	    { break; }
	  nNegative = 0;       // otherwise reset counter
	}

      // advance indexes to next sample
      i++;
      for (m = 0; m < N_MICS; m++)
	{ idxIn[m] += N_MICS; }
      idxOut += 2;
    }

  iMax = i + kMaxLines; // just display a portion of the signal
  iMax = iMax > nSamplesPerMic ? nSamplesPerMic : iMax;
  for (; i < iMax; i++)
    {
      memset(str, ' ' , kMaxLen+1); // fill string with spaces
      str[kPos0] = '|'; // plot the axis at 0

      // loop over all mic channels
      for (m = 0; m < N_MICS; m++)
	{
	  val = pPcmIn[ idxIn[m] ];
	  pos = 0.5 + val / kMaxVal * kPos0 + kPos0;
	  if (pos >= 0 && pos <= kMaxLen)
	    { str[pos] = '0' + m; } // plot the channel marker
	  idxIn[m] += N_MICS;
	}

      // left output channel
      val = pPcmOut[idxOut++];
      pos = 0.5 + val / kMaxVal * kPos0 + kPos0;
      if (pos >= 0 && pos <= kMaxLen)
	{ str[pos] = 'L'; }

      // right output channel
      val = pPcmOut[idxOut++];
      pos = 0.5 + val / kMaxVal * kPos0 + kPos0;
      if (pos >= 0 && pos <= kMaxLen)
	{ str[pos] = 'R'; }

      // null terminate the string
      str[kMaxLen+1] = '\0';

      // print it
      LOGD("%4d: %s\n", i, str);
    }
}
