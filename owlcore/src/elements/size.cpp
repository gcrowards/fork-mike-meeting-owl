#include "elements/size.hpp"
#include "logging.hpp"

owl::cSize::cSize(void)
{
}

/** @param widthVal represents an abstract width.
 * @param heightVal represents an abstract height.
 */
owl::cSize::cSize(int widthVal, int heightVal)
{
  width = widthVal;
  height = heightVal;
}

owl::cSize::~cSize()
{
}

/** Print comment and size specifications.
 *
 * @param comment is a text string that can optionally be printed before the
 * size specifications.
 */
void owl::cSize::print(std::string comment)
{
  LOGI("%s %d x %d\n", comment.c_str(), width, height);
}
