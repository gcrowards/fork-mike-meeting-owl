#include "circleMath.hpp"
#include "elements/patch.hpp"
#include "logging.hpp"

#include <cmath>

owl::cPatch::cPatch(void)
{
}

/** 
 * @param thetaVal is an int that defines the bearing angle (deg) of the center
 * of the patch.
 * @param phiVal is an int that defines the elevation angle (deg) of the center
 * of the patch.
 * @param dTheta is an int that defines the angular width (deg) of the patch.
 * @param dPhi is an int that defines the angular height (deg) of the patch.
 */
owl::cPatch::cPatch(int thetaVal, int phiVal, int dThetaVal, int dPhiVal)
{
  theta = thetaVal;
  phi = phiVal;
  dTheta = dThetaVal;
  dPhi = dPhiVal;
}

owl::cPatch::~cPatch()
{
}

bool owl::cPatch::operator==(const owl::cPatch &other) const
{
  return (other.theta == this->theta &&
	  other.phi == this->phi &&
	  other.dTheta == this->dTheta &&
	  other.dPhi == this->dPhi );
}

bool owl::cPatch::operator!=(const owl::cPatch &other) const
{
  return !(*this == other);
}

/** Converts width and height of the patch from degrees to a cv::Size containing
 * the stage pixel equivalents.
 *
 * @return a cv::Size containing the width & height of the patch in stage pixels.
 */
cv::Size owl::cPatch::getSizeInStagePix(void)
{
  return cv::Size(degToStagePix(dTheta), degToStagePix(dPhi));
}

/** This function calculates the parameters that are needed by other functions
 * in order to figure out when two arcs overlap, and by how much, and so forth.
 *
 * The theta angles of the edges of each patch (the one represented by this 
 * class and the one that is being provided to test against) are found and
 * rotated so that one ("a", which is this patches "left" edge) edge is at
 * zero degrees (to simplify things).
 *
 * @param testPatch is the patch that is being compared to the patch that is
 * represented by this class instance.
 * @param pB is this patch's right-most edge theta value (deg).
 * @param pC is test patch's left-most edge theta value (deg).
 * @param pD is test patch's right-most edge theta value (deg).
 */
void owl::cPatch::calcArcThetaParams(owl::cPatch testPatch,
				     int *pZeroOffset, 
				     int *pB, int *pC, int *pD )
{   
  *pZeroOffset = circleMath::intSubAngs(this->theta, (this->dTheta / 2));
  // zero offset is "missing" *pA
  *pB = circleMath::intAddAngs(*pZeroOffset, this->dTheta);
  
  *pC = circleMath::intSubAngs(testPatch.theta, (testPatch.dTheta / 2));
  *pD = circleMath::intAddAngs(*pC, testPatch.dTheta);

  *pB = circleMath::intSubAngs(*pB, *pZeroOffset);
  *pC = circleMath::intSubAngs(*pC, *pZeroOffset);
  *pD = circleMath::intSubAngs(*pD, *pZeroOffset);
}

/** Test if this patch is unlikely filled with meaningful data.
 *
 * If all patch data are zero then this patch has probably never been set,
 * or has been intentially set to this "empty" state.
 *
 * @return true if patch contains meaningless data.
 *
return true if this patch has zeros for all parameter values. */
bool owl::cPatch::isEmpty(void)
{
  return (theta == 0 && dTheta == 0 && phi == 0 && dPhi == 0);
}

/** This function tests if two patches overlap one another at all. 
 *
 * @param testPatch is an owl::cPatch that is to be tested for overlap 
 * against this patch.
 *
 * @return true if there is any overlap between patched.
 */
bool owl::cPatch::isOverlapped(owl::cPatch testPatch)
{
  int zeroOffset; // this patch's left-most edge theta value (deg);
  int b;          // this patch's right-most edge theta value (deg);
  int c;          // test patch's left-most edge theta value (deg);
  int d;          // test patch's right-most edge theta value (deg);

  calcArcThetaParams(testPatch, &zeroOffset, &b, &c, &d);
 
  // no overlap case
  if(b <= c && c <= d) 
    {return false;}

  return true;
}

/** This function tests if one patch completely overlaps another.
 *
 * @param testPatch is an owl::cPatch that is to be tested for full overlap 
 * with this patch.
 *
 * @return true if one patch fully overlaps the other (doesn't matter which
 * is the bigger patch).
 */
bool owl::cPatch::isFullOverlap(owl::cPatch testPatch)
{
  int zeroOffset;        // this patch's left-most edge theta value (deg);
  int b;                 // this patch's right-most edge theta value (deg);
  int c;                 // test patch's left-most edge theta value (deg);
  int d;                 // test patch's right-most edge theta value (deg);

  calcArcThetaParams(testPatch, &zeroOffset, &b, &c, &d);
  
  if((c < d) && d <= b)
    { // full overlap (no difference) Ex:
      // this patch: |a--------------->b        |
      // test patch: |      c====>d             |
      //             0                         360
      return true;
    }
  return false;
}

/** Find the arc (theta direction) that represents the portion that two patches
 * don't have in in common.
 *
 * This is essentially what is leftover when the test patch is subtracted from
 * this patch.  However, there are a couple of non-intuitive details: First, if
 * the test patch does not overlap this patch at all, the difference is reported
 * as the full test patch itself.  Second, if the test patch extends beyond both
 * the start and end of this patch, this function returns the difference patch 
 * as having the length of the arc difference, but with a bearing of -1 (really,
 * to properly represent this situation we'd have to return two difference 
 * patched.
 *
 * @param testPatch is the owl::cPatch that is to be subtracted from this patch.
 *
 * @return an owl::cPatch representing the difference that remains when the 
 * testPatch is subtracted from this patch (with exceptions explained above).
 */
owl::cPatch owl::cPatch::findDifference(owl::cPatch testPatch)
{
  owl::cPatch differencePatch(0, 0, 0, 0);

  int differenceArcLen;  // length of arc of test patch that doesn't overlap
                         // the arc of this patch
  int differenceBearing; // the bearing of the center of the arc described by
                         // differenceArcLen

  int zeroOffset;        // this patch's left-most edge theta value (deg);
  int b;                 // this patch's right-most edge theta value (deg);
  int c;                 // test patch's left-most edge theta value (deg);
  int d;                 // test patch's right-most edge theta value (deg);

  // this finds the start and end angles for each patch, with a rotation that
  // aligns this patch's start angle to zero degrees
  calcArcThetaParams(testPatch, &zeroOffset, &b, &c, &d);
  
  // find region of the patches (theta direction) that _aren't_ common
  if(c < d)
    {
      if(d <= b)
	{ // full overlap (no difference) Ex:
	  // this patch: |a--------------->b        |
	  // test patch: |      c====>d             |
	  //             0                         360
	  return differencePatch;
	}
      else if(b > c)
	{ // partial difference (w/ single overlap at patch theta end) Ex:
	  // this patch: |a--------------->b        |
	  // test patch: |            c=======>d    |
	  //             0                         360
	  differenceArcLen = d - b;
	  differenceBearing = b + (differenceArcLen / 2); 
	}
      else
	{ // no overlap (full difference) Ex:
	  // this patch: |a--------------->b        |
	  // test patch: |                   c===>d |
	  //             0                         360
	  return testPatch;
	}
    }
  else
    {
      if(d > b)
	{ // double difference, Ex:
	  // this patch: |a--------------->b        |
	  // test patch: |===================>d  c==|
	  //             0                         360
	  differenceArcLen = (360 - c) + (d - b);
	  differenceBearing = -1; // This is to denote that it's a combined
                                  // arc length and has no other meaning
	}
      else if(b < c)
	{ // partial difference (w/ single overlap at patch theta start) Ex:
	  // this patch: |a--------------->b        |
	  // test patch: |===>d                 c===|
	  //             0                         360
	  differenceArcLen = 360 - c;
	  differenceBearing = c + (differenceArcLen / 2); 
	}
      else
	{ // partial difference (w/ double overlap) Ex:
	  // this patch: |a--------------->b        |
	  // test patch: |===>d        c============|
	  //             0                         360
	  differenceArcLen = 360 - b;
	  differenceBearing = b + (differenceArcLen / 2);
	}
    }

  // reverse the zero offset adjustmet & account for wrap around
  differenceBearing = (differenceBearing + zeroOffset) % 360; 

  differencePatch.theta = differenceBearing;
  differencePatch.dTheta = differenceArcLen;

  return differencePatch;
}

/** This function combines two patches together (theta direction only) into a 
 * single patch.
 *
 * A non-intuitive behavior is that if the test patch does not overlap this 
 * patch at all, the sum is reported as the test patch's theta arc plus the 
 * amount of non-overlapping space that must be filled to connect to the edge
 * of this patch.
 *
 * @param testPatch is the owl::cPatch that is to be added to this patch.
 *
 * @return an owl::cPatch representing the patch that results when testPatch
 * is combined with this patch (theta direction only).
 */
owl::cPatch owl::cPatch::findSum(owl::cPatch testPatch)
{
  owl::cPatch sumPatch(0, 0, 0, 0);

  int sumArcLen;  // length of arc of test patch plus this patch (without
                  // double counting the portion that overlaps
  int sumBearing; // the bearing of the center of the arc described by sumArcLen

  int zeroOffset; // this patch's left-most edge theta value (deg);
  int b;          // this patch's right-most edge theta value (deg);
  int c;          // test patch's left-most edge theta value (deg);
  int d;          // test patch's right-most edge theta value (deg);
 
  // this finds the start and end angles for each patch, with a rotation that
  // aligns this patch's start angle to zero degrees
  calcArcThetaParams(testPatch, &zeroOffset, &b, &c, &d);

  // find the arc that results from combining the test patch with this patch  
  if(d > b)
    {
      if(c > d)
	{ // test patch extends past both ends of this patch, Ex:
	  // this patch: |a--------------->b        |
	  // test patch: |===================>d  c==|
	  //             0                         360
	  sumArcLen = 360 - (c - d);
	  sumBearing = d + ((c - d) / 2) - 180;
	}
      else if(c > b)
	{
	  // test if extending from start or end
	  if((c - b) < (360 - d))
	    { // test patch is closer to end, Ex:
	      // this patch: |a--------------->b        |
	      // test patch: |                  c==>d   |
	      //             0                         360
	      sumArcLen = d;
	      sumBearing = sumArcLen / 2;
	    }
	  else
	    { // test patch is closer to start, Ex:
	      // this patch: |a--------------->b        |
	      // test patch: |                     c==>d|
	      //             0                         360
	      sumArcLen = this->dTheta + (360 - c);
	      sumBearing =  b - (sumArcLen / 2);;
	    }
	}
      else
	{ // test patch overlaps and extends from end, Ex:
	  // this patch: |a--------------->b        |
	  // test patch: |              c======>d   |
	  //             0                         360
	  sumArcLen = d;
	  sumBearing = sumArcLen / 2;
	}
    }
  else if(c > b) 
    { // test patch overlaps and extends from start, Ex:
      // this patch: |a--------------->b        |
      // test patch: |===>d                 c===|
      //             0                         360
      sumArcLen = this->dTheta + (360 - c);
      sumBearing = b + ((c - b) / 2) - 180;
    }
  else if(c > d)  
    { // two patches result in fully closed circle, Ex:
      // this patch: |a--------------->b        |
      // test patch: |===>d        c============|
      //             0                         360
      sumArcLen = 360;
      sumBearing = 0;  // arbitrary, bearing is meaningless
    }
  else 
    { // test patch has overlaps, but doesn't extend past this patch, Ex:
      // this patch: |a--------------->b        |
      // test patch: |      c====>d             |
      //             0                         360
      sumPatch = *this;
      return sumPatch;
    }

  // reverse the zero offset adjustmet & account for wrap around
  sumBearing = (sumBearing + zeroOffset) % 360; 

  sumPatch.theta = sumBearing;
  sumPatch.dTheta = sumArcLen;

  return sumPatch;
}

/** This function finds the patch that represents the separation (theta only)
 * between two patches.
 * 
 * @param testPatch is the owl::cPatch that is to be compared to this patch.
 * If the two patches ovelap at all then an empty patch is returned.
 *
 * @return an owl::cPatch representing the amount of separation (theta only)
 * between the testPatch and this patch.
 */
owl::cPatch owl::cPatch::findSeparation(owl::cPatch testPatch)
{
  owl::cPatch separationPatch(0, 0, 0, 0);
  
  int sepArcLen;  // length of arc of test patch plus this patch (without
                  // double counting the portion that overlaps)
  int sepBearing; // the bearing of the center of the arc described by sepArcLen
  
  int zeroOffset; // this patch's left-most edge theta value (deg);
  int b;          // this patch's right-most edge theta value (deg);
  int c;          // test patch's left-most edge theta value (deg);
  int d;          // test patch's right-most edge theta value (deg);
 
  // make sure test patch doesn't overlap this patch
  if(isOverlapped(testPatch))
    {
      return separationPatch;
    }

  // this finds the start and end angles for each patch, with a rotation that
  // aligns this patch's start angle to zero degrees
  calcArcThetaParams(testPatch, &zeroOffset, &b, &c, &d);

  if((c -b) < (360 - d))
    { // this patch: |a------->b                |
      // test patch: |             c==>d        |
      //             0                         360
      sepArcLen = c - b;
      sepBearing = b + (sepArcLen / 2);
    }
  else
    { // this patch: |a------->b                |
      // test patch: |                   c==>d  |
      //             0                         360
      sepArcLen = 360 - d;
      sepBearing = d + (sepArcLen / 2);
    }

  // reverse the zero offset adjustmet & account for wrap around
  sepBearing = (sepBearing + zeroOffset) % 360; 

  separationPatch.theta = sepBearing;
  separationPatch.dTheta = sepArcLen;

  return separationPatch;
}

/** This function finds the patch that represents the overlap (theta only)
 * between two patches.
 * 
 * A non-intuitive behavior is that if the test patch overlaps both the start
 * and end of this patch, but not the middle the overlap should be represented
 * by two discrete patches.  Because this function can only return one patch
 * result (which is done to keep things simpler), in this scenario the overlap
 * is returned as if those two discrete overlaps were joined together as one.
 *
 * @param testPatch is the owl::cPatch that is to be compared to this patch.
 * If the two patches don't ovelap at all then an empty patch is returned.
 *
 * @return an owl::cPatch representing the amount of overlap (theta only)
 * between the testPatch and this patch.
 */
owl::cPatch owl::cPatch::findOverlap(owl::cPatch testPatch)
{
  owl::cPatch overlapPatch(0, 0, 0, 0);
  
  int overlapArcLen;  // length of arc of patch that represents the part that
                      // this patch & test patch have in common
  int overlapBearing; // the bearing of the center of the arc described by 
                      // overlapArcLen
  
  int zeroOffset; // this patch's left-most edge theta value (deg);
  int b;          // this patch's right-most edge theta value (deg);
  int c;          // test patch's left-most edge theta value (deg);
  int d;          // test patch's right-most edge theta value (deg);
 
  // make sure there's some overlap between the test patch and this patch
  if(!isOverlapped(testPatch))
    {
      return overlapPatch;
    }

  // this finds the start and end angles for each patch, with a rotation that
  // aligns this patch's start angle to zero degrees
  calcArcThetaParams(testPatch, &zeroOffset, &b, &c, &d);


  // find region of the patches (theta direction) that are common
  if(c < d)
    {
      if(d <= b)
	{ // full overlap (this patch bigger) Ex:
	  // this patch: |a--------------->b        |
	  // test patch: |      c====>d             |
	  //             0                         360
	  return testPatch;
	}
      else
	{ // partial overlap (at patch theta end) Ex:
	  // this patch: |a--------------->b        |
	  // test patch: |            c=======>d    |
	  //             0                         360
	  overlapArcLen = b - c;
	  overlapBearing = c + (overlapArcLen / 2); 
	}
    }
  else
    {
      if(d > b)
	{ // full overlap (test patch bigger) Ex:
	  // this patch: |a--------------->b        |
	  // test patch: |===================>d  c==|
	  //             0                         360
	  return *this;
	}
      else if(b < c)
	{ // partial overlap (at patch theta start) Ex:
	  // this patch: |a--------------->b        |
	  // test patch: |===>d                 c===|
	  //             0                         360
	  overlapArcLen = d;
	  overlapBearing = overlapArcLen / 2; 
	}
      else
	{ // double overlap (patch start & end, but not middle) Ex:
	  // this patch: |a--------------->b        |
	  // test patch: |===>d        c============|
	  //             0                         360
	  overlapArcLen = (b - c) + d;
	  overlapBearing = -1; // This is to denote that it's a combined
                               // arc length and has no other meaning
	}
    }

  // reverse the zero offset adjustmet & account for wrap around
  overlapBearing = (overlapBearing + zeroOffset) % 360; 

  overlapPatch.theta = overlapBearing;
  overlapPatch.dTheta = overlapArcLen;

  return overlapPatch;
}

/* This function tests if the given bearing is within the bounds of this cPatch.
 *
 * @param testTheta is an interger bearing [0 - 360] that is to be tested.
 *
 * @return true if the bearing is within this patch's bounds.
 */
bool owl::cPatch::contains(int testTheta)
{
  cPatch testPatch(testTheta, 0, 0, 0);

  int zeroOffset; // this patch's left-most edge theta value (deg)
  int b;          // this patch's right-most edge theta value (deg)
  int c;          // shifted testTheta (deg)
  int d;          // same as c, because test patch's dTheta = 0

  calcArcThetaParams(testPatch, &zeroOffset, &b, &c, &d);

  if(c < b) {return true;}
  else      {return false;}
}

/** This function is used to set the patches parameters based on start/end 
 * angles.
 *
 * Typically, a patch is set based on center/width angles, but sometimes it
 * is preferred to use start/end angles.  This function translates start/end
 * angles into center/width angles and assigns those values to the patch.
 *
 * WARNING: !!! Assumes patch is less than 180 degrees wide !!!
 *
 * @params theta0 is the start "horizontal" angle [0..360] degrees.
 * @params theta1 is the end "hotizontal" angle [0..360] degrees.
 * @params phi0 is the start "vertical" angle [0..90] degrees.
 * @params phi1 is the end "vertical" angle [0..90] degrees.
 */ 
void owl::cPatch::setPatch(int theta0, int theta1, int phi0, int phi1)
{
  // set phi values
  phi = (phi0 + phi1) / 2;
  dPhi = std::abs(phi0 - phi1);

  // set theta values (a bit trickier because of wrap at 0/360 degrees)
  theta = circleMath::intAvg(theta0, theta1, 360);
  dTheta = std::abs(circleMath::intDiffAngs(theta0, theta1));
}

/** Print comment and dimensions that define the patch (in degrees).
 *
 * @param comment is a text string that can optionally be printed before the
 *  dimensions.
 */
void owl::cPatch::print(std::string comment)
{
  LOGI("%s patch> theta:%d, phi:%d, dTheta:%d, dPhi:%d (degrees)\n",
       comment.c_str(), theta, phi, dTheta, dPhi );
}
