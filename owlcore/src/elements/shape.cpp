#include "elements/shape.hpp"
#include "gpu.hpp"
#include "logging.hpp"

/** Setter for the shape's color and alpha.
 *
/** @param r is the red value in [0., 1.].
 * @param g is the green value in [0., 1.].
 * @param b is the blue value in [0., 1.].
 * @param a is the alpha value in [0., 1.]; default is 1.0.
 */
void owl::abcShape::setColor(float r, float g, float b, float a)
{
  red = r;
  green = g;
  blue = b;
  alpha = a;
}

/** Setter to determine if shape is filled or an outline.
 *
 * @param filledVal is true if filled, false for outline.
 */
void owl::abcShape::setFilled(bool filledVal)
{
  filled = filledVal;
}

/** Setter for the shape's line width.
 *
 * Note that some shapes might not use the line width and that GPUs
 * have a maximum value that's supported.
 *
 * @param lineWidthVal is the width in pixels.
 */
void owl::abcShape::setLineWidth(float lineWidthVal)
{
  lineWidth = lineWidthVal;
}
