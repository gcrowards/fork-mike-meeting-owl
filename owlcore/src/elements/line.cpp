#include "elements/line.hpp"
#include "gpu.hpp"
#include "logging.hpp"

owl::cLine::cLine(void)
{
}

/** @param x0Val is the x coordinate of the first endpoint of the line.
 * @param y0Val is the y coordinate of the first endpoint of the line.
 * @param x1Val is the x coordinate of the second endpoint of the line.
 * @param y1Val is the y coordinate of the second endpoint of the line.
 */
owl::cLine::cLine(int x0Val, int y0Val, int x1Val, int y1Val)
{
  x0 = x0Val;
  y0 = y0Val;
  x1 = x1Val;
  y1 = y1Val;
}

/** @param point0 is the first endpoint of the line.
 * @param point1 is the second endpoint of the line.
 */
owl::cLine::cLine(owl::cPoint point0, owl::cPoint point1)
{
  x0 = point0.x;
  y0 = point0.y;
  x1 = point1.x;
  y1 = point1.y;
}

owl::cLine::~cLine()
{
}

/** Draw method that calls the corresponding GPU render method.
 *
 * @param gpu is a pointer to the cGpu instance for rendering.
*/
void owl::cLine::draw(cGpu *gpu)
{
  gpu->renderLine(x0, y0, x1, y1, red, green, blue, alpha, lineWidth);
}

/** Print comment and line specifications.
 *
 * @param comment is a text string that can optionally be printed before the
 * line specifications.
 */
void owl::cLine::print(std::string comment)
{
  LOGI("%s (%d, %d) (%d, %d)\n"
       "rgba: %.2f %.2f %.2f %.2f  lineWidth: %.1f\n",
       comment.c_str(), x0, y0, x1, y1,
       red, green, blue, alpha, lineWidth );
}
