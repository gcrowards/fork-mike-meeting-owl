/*
 * rgbaOverlayBase.cpp
 *
 *  Created on: Mar 28, 2017
 *
 * Generalized overlay object management system.
 */

#include "common.hpp"
#include "logging.hpp"
#include "elements/rgbaOverlayBase.hpp"
#include "gpu.hpp"

/* *** cRgbaOverlayBase *** */

/** Constructs a base class instance of an RGBA overlay control object.
 *
 * @param renderId is the ID used to render the overlay.
 * @param width is the width of the overlay bitmap.
 * @param height is the height of the overlay bitmap.
 */
owl::cRgbaOverlayBase::cRgbaOverlayBase(int renderId, int width, int height)
  : mRenderId(renderId),
    mWidth(width),
    mHeight(height),
    mVisible(false),
    mpGpu(NULL)
{
}

/** Constructs a base class instance of an RGBA overlay control object.
 *
 * @param renderId is the ID used to render the overlay.
 * @param width is the width of the overlay bitmap.
 * @param height is the height of the overlay bitmap.
 */
owl::cRgbaOverlayBase::cRgbaOverlayBase(int renderId)
  : mRenderId(renderId),
    mWidth(0),
    mHeight(0),
    mVisible(false),
    mpGpu(NULL)
{
}

/** Destroys an instance of an RGBA overlay control object. */
owl::cRgbaOverlayBase::~cRgbaOverlayBase()
{
}

/** Renders an RGBA overlay object after updating animation parameters. */
void owl::cRgbaOverlayBase::render(void)
{
  int left;
  int top;
  float hScale;
  float vScale;
  float alpha;

  if (mVisible)
    {
      mAnimation.update(left, top, hScale, vScale, alpha);
      mpGpu->renderRgba(mRenderId, left, top,
                        hScale * mWidth, vScale * mHeight, alpha);
    }
}

/** Gets the texture unit ID for rendering this object.
 *
 * @return the texture unit ID.
 */
int owl::cRgbaOverlayBase::getRenderId(void)
{
  return mRenderId;
}

/** Set the visible state of the RGBA overlay object.
 *
 * @param turnOn if true, shows the overlay, otherwise hides it.
 */
void owl::cRgbaOverlayBase::setVisible(bool turnOn)
{
  mVisible = turnOn;
}

/** Return visibility state of this RGBA overlay object.
 *
 * @return true if the overlay is visible, false if it is hidden.
 */
bool owl::cRgbaOverlayBase::getVisible(void)
{
  return mVisible;
}

/** Set a pointer to the GPU object required for rendering an RGBA overlay.
 *
 * @param pGpu is a pointer to the GPU object used to render overlay.
 */
void owl::cRgbaOverlayBase::setGpu(cGpu *pGpu)
{
  mpGpu = pGpu;
}

/** Get the size of the overlay object bitmap.
 *
 * @param width of the RGBA overlay bitmap.
 * @param height of the RGBA overlay bitmap.
 */
void owl::cRgbaOverlayBase::getSize(int &width, int &height)
{
  width = mWidth;
  height = mHeight;
}

/** Get a pointer to the object that controls animation of this overlay.
 *
 * @return pointer to a cLinearAnimate object that can be used to set up
 * the animation behavior for this overlay.
 */
owl::cLinearAnimate *owl::cRgbaOverlayBase::getAnimationPtr()
{
  return &mAnimation;
}

