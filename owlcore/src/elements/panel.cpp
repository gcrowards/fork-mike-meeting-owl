#include "elements/panel.hpp"
#include "logging.hpp"

owl::cPanel::cPanel(void)
{
}

/** @param posX0Val is x-coord of output rectangle's top-left corner.
 * @param posY0Val is y-coord of output rectangle's top-left corner.
 * @param posX1Val is x-coord of output rectangle's bottom-right corner.
 * @param posY1Val is y-coord of output rectangle's bottom-right corner.
 * @param theta0Val is start angle (radians).
 * @param theta1Val is end angle (radians).
 * @param radiusInnerVal is inner radius in [0.0, 0.5].
 * @param radiusOuterVal is outer radius in [0.0, 0.5].
 */
owl::cPanel::cPanel(int posX0Val, int posY0Val, int posX1Val, int posY1Val, 
		    float theta0Val, float theta1Val, float radiusInnerVal, 
		    float radiusOuterVal )
{
  posX0 = posX0Val;
  posY0 = posY0Val;
  posX1 = posX1Val;
  posY1  = posY1Val;
  theta0 = theta0Val;
  theta1 = theta1Val;
  radiusInner = radiusInnerVal;
  radiusOuter = radiusOuterVal ;
}

owl::cPanel::~cPanel()
{
}

/** Print comment and point coordinates.
 *
 * @param comment is a text string that can optionally be printed before the
 * point coordinates.
 */
void owl::cPanel::print(std::string comment)
{
  LOGI("%s input: %.2f to %.2f radians, %.2f - %.2f radii \n"
       "       %s output: (%d, %d) (%d, %d)\n", 
       comment.c_str(), theta0, theta1, radiusInner, radiusOuter,
       comment.c_str(), posX0, posY0, posX1, posY1 );
}
