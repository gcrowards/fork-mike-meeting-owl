#include "circleMath.hpp"
#include "elements/angle.hpp"

owl::cAngle::cAngle(float angle)
{
  val = angle;
}

owl::cAngle::~cAngle()
{
}

/** Assignment opertors. */
void owl::cAngle::operator=(const owl::cAngle &other)
{
  this->val = other.val;
}

void owl::cAngle::operator=(float val)
{
  this->val = val;
}

/** Addition operator.
 *
 * @return the sum of two cAngles, keeping the result on the [0..360) range.
 */
owl::cAngle owl::cAngle::operator+(const owl::cAngle &other) const
{
  owl::cAngle result(circleMath::floatAddAngs(this->val, other.val));
  return result;
}

/** Subtraction operator.
 *
 * @return the difference of two cAngles, keeping the result on the [0..350)
 * range.
 */
owl::cAngle owl::cAngle::operator-(const owl::cAngle &other) const
{
  owl::cAngle result(circleMath::floatSubAngs(this->val, other.val));
  return result; 
}

/** Comparison operators. */
bool owl::cAngle::operator==(const owl::cAngle &other) const
{
  return (other.val == this->val);
}


bool owl::cAngle::operator!=(const owl::cAngle &other) const
{
  return !(*this == other);
}

