#include "elements/point.hpp"
#include "logging.hpp"

owl::cPoint::cPoint(void)
{
}

/** @param xVal is the x coordinate that defines the point.
 * @param yVal is the y coordinate that defines the point.
 */
owl::cPoint::cPoint(int xVal, int yVal)
{
  x = xVal;
  y = yVal;
}

owl::cPoint::~cPoint()
{
}

/** Print comment and point coordinates.
 *
 * @param comment is a text string that can optionally be printed before the
 * point coordinates.
 */
void owl::cPoint::print(std::string comment)
{
  LOGI("%s (%d, %d)\n", comment.c_str(), x, y);
}
