#include "elements/rect.hpp"
#include "gpu.hpp"
#include "logging.hpp"

owl::cRect::cRect(void)
{
}

/** @param xVal is the x coordinate that defines the top-left corner of the
 * rectangle.
 * @param yVal is the y coordinate that defines the top-left corner of the
 * rectangle.
 * @param widthVal is the width of the rectangle.
 * @param heightVal is the height of the rectangle.
 */
owl::cRect::cRect(int xVal, int yVal, int widthVal, int heightVal)
{
  x = xVal;
  y = yVal;
  width = widthVal;
  height = heightVal;
}

/** @param xVal is the x coordinate that defines the top-left corner of the
 * rectangle.
 * @param yVal is the y coordinate that defines the top-left corner of the
 * rectangle.
 * @param size is the width and height of the a rectangle.
 */
owl::cRect::cRect(int xVal, int yVal, owl::cSize size)
{
  x = xVal;
  y = yVal;
  width = size.width;
  height = size.height;
}

/** @param point is the coordinate of the top-left corner of the rectangle.
 * @param widthVal is the width of the rectangle.
 * @param heightVal is the height of the rectangle.
 */
owl::cRect::cRect(owl::cPoint point, int widthVal, int heightVal)
{
  x = point.x;
  y = point.y;
  width = widthVal;
  height = heightVal;
}

/** @param point is the coordinate of the top-left corner of the rectangle.
 * @param size is the width and height of a rectangle.
 */
owl::cRect::cRect(owl::cPoint point, owl::cSize size)
{
  x = point.x;
  y = point.y;
  width = size.width;
  height = size.height;
}

owl::cRect::~cRect()
{
}

/** Draw method that calls the corresponding GPU render method.
 *
 * @param gpu is a pointer to the cGpu instance for rendering.
*/
void owl::cRect::draw(cGpu *gpu)
{
  gpu->renderRectangle(x, y, width, height, red, green, blue, alpha,
		       lineWidth, filled);
}

/** Print comment and rectangle specifications.
 *
 * @param comment is a text string that can optionally be printed before the
 * rectangle specifications.
 */
void owl::cRect::print(std::string comment)
{
  LOGI("%s (%d, %d) %d x %d\n"
       "rgba: %.2f %.2f %.2f %.2f  lineWidth: %.1f\n",
       comment.c_str(), x, y, width, height,
       red, green, blue, alpha, lineWidth );
}
