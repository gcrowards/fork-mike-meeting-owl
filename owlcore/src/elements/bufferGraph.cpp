/** Copyright 2017 Owl Labs Inc.
 *  All rights reserved.
 *
 *  elements/bufferGraph.cpp
 *  This is the header file for cBufferGraph.
 *   --> Overlays graph data to the screen using VBOs.
 */

#include "elements/bufferGraph.hpp"
#include "gpu.hpp"
#include "logging.hpp"

/**
 * @param pVertices a pointer to the vertex data vector.
 * @param colorUniform the id of u_color in the graph shader.
 */
owl::cBufferGraph::cBufferGraph(const std::vector<sVertex> *pVertices,
				GLuint colorUniform )
  : mpVertices(pVertices), mColorUniform(colorUniform)
{ }

owl::cBufferGraph::~cBufferGraph()
{
  cleanup();
}

/** Initializes GPU resources. */
void owl::cBufferGraph::init()
{
  // create VBO
  glGenBuffers(1, &mVBuffer);
  mInitialized = true;
}

/** Cleans up OpenGL resources. */
void owl::cBufferGraph::cleanup()
{
  glDeleteBuffers(1, &mVBuffer);
  mInitialized = false;
}

/** Renders the graph to the current viewport.
 * WARNING: This should only be called from the GPU thread.
 *
 * @param pGpu the parent cGpu instance (unused). 
 */
void owl::cBufferGraph::draw(cGpu *pGpu)
{
  if(!mInitialized)
    { init(); }

  if(mpVertices && lineWidth > 0.0f)
    {
      // update VBO data
      glBindBuffer(GL_ARRAY_BUFFER, mVBuffer);
      glBufferData(GL_ARRAY_BUFFER, mpVertices->size() * sizeof(sVertex),
		   mpVertices->data(), GL_DYNAMIC_DRAW );
      
      // set attribute pointers
      glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);
      glEnableVertexAttribArray(0);

      // draw graph
      glLineWidth(lineWidth);
      glUniform4f(mColorUniform, red, green, blue, alpha);
      glDrawArrays(GL_LINES, 0, mpVertices->size());

      // clean up
      glDisableVertexAttribArray(0);
      glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
}

/** Sets the vertex data vector.
 * @param pVertices a pointer to the vertex data vector to use.
 */
void owl::cBufferGraph::setVertexBuffer(const std::vector<sVertex> *pVertices)
{
  mpVertices = pVertices;
}

/** Prints vertex data. */
void owl::cBufferGraph::print(std::string comment)
{
  LOGI("Buffer Graph:\n (%s)\n\n Data:\n", comment.c_str());
  for(auto v : *mpVertices)
    { LOGI("     Vertex (%.3f, %.3f)\n", v.x, v.y); }
  LOGI("\n");
}
