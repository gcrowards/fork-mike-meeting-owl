/*
 * rgbaOverlay.cpp
 *
 *  Created on: Mar 28, 2017
 *
 * Generalized overlay object management system.
 */

#include "common.hpp"
#include "logging.hpp"
#include "elements/linearAnimate.hpp"


/* *** cLinearAnimate *** */
#ifdef CLAMP
#undef CLAMP
#endif
#define CLAMP(min, val, max) \
  (((val) < (min)) ? (min) : (((val) > (max)) ? (max) : (val)))

/** Constructs a linear animation control object. */
owl::cLinearAnimate::cLinearAnimate()
  : mDuration(0),
    mDir(0),
    mBeginLeft(0),
    mBeginTop(0),
    mBeginHScale(0),
    mBeginVScale(0),
    mBeginAlpha(0),
    mEndLeft(0),
    mEndTop(0),
    mEndHScale(0),
    mEndVScale(0),
    mEndAlpha(0),
    mStartTime({0,0})
{
}

/** Destroys a linear animation control object. */
owl::cLinearAnimate::~cLinearAnimate()
{
}

/** Sets parameters for the start point of the animation.
 *
 * @param left is the left side of the upper left corner of the bitmap.
 * @param top is the top side of the upper left corner of the bitmap.
 * @param hScale is the horizontal scale factor to apply.
 * @param hScale is the vertical scale factor to apply.
 * @param alpha is the transparency to apply.
 */
void owl::cLinearAnimate::setBegin(
    int left, int top, float hScale, float vScale, float alpha)
{
  mBeginLeft = CLAMP(0, left, OUTPUT_WIDTH);
  mBeginTop = CLAMP(0, top, OUTPUT_HEIGHT);
  mBeginHScale = CLAMP(0.0, hScale, OUTPUT_WIDTH);
  mBeginVScale = CLAMP(0.0, vScale, OUTPUT_HEIGHT);
  mBeginAlpha  = CLAMP(0.0, alpha, 1.0);
}

/** Sets parameters for the end point of the animation.
 *
 * @param left is the left side of the upper left corner of the bitmap.
 * @param top is the top side of the upper left corner of the bitmap.
 * @param hScale is the horizontal scale factor to apply.
 * @param hScale is the vertical scale factor to apply.
 * @param alpha is the transparency to apply.
 */
void owl::cLinearAnimate::setEnd(
    int left, int top, float hScale, float vScale, float alpha)
{
  mEndLeft = CLAMP(0, left, OUTPUT_WIDTH);
  mEndTop = CLAMP(0, top, OUTPUT_HEIGHT);
  mEndHScale = CLAMP(0.0, hScale, OUTPUT_WIDTH);
  mEndVScale = CLAMP(0.0, vScale, OUTPUT_HEIGHT);
  mEndAlpha  = CLAMP(0.0, alpha, 1.0);
}

/** Sets the duration of the animation motion.
 *
 * @param duration is the time in seconds to complete the animation.
 */
void owl::cLinearAnimate::setDuration(float duration)
{
  mDuration = CLAMP(0.0f, duration, 1000.0f);
}

/** Sets the direction of progress for the animation
 *
 * @param direction If >= 0, animate start -> end, else end -> start.
 */
void owl::cLinearAnimate::setDirection(int direction)
{
  mDir = ((direction >= 0) ? 1 : -1);
}

/** Sets the start time of the animation to the current time of the call.
 *
 */
void owl::cLinearAnimate::start(void)
{
  clock_gettime(CLOCK_MONOTONIC, &mStartTime);
}

/** Sets the start time to long ago.
 *
 * After a call to reset, the animation will render with the end
 * parameters until it is started again.
 */
void owl::cLinearAnimate::reset(void)
{
  mStartTime = {0, 0};
}

/** Returns true if the animation is actively changing the graphic.
 *
 * @return true if animating.
 */
bool owl::cLinearAnimate::animating(void)
{
  bool animating = false;
  struct timespec t1;
  double elapsedTime;

  // update elapsed seconds
  clock_gettime(CLOCK_MONOTONIC, &t1);
  elapsedTime = elapsedTimeSec(mStartTime, t1);

  if (elapsedTime < mDuration)
    { animating = true; }

  return animating;
}

/** Linearly interpolates between start and end parameter sets.
 *
 * The elapsed time since start() was called is used to determine the
 * proportion of start and end parameters to apply.
 *
 * @param left is a reference to the new left coordinate.
 * @param top is a reference to the new top coordinate.
 * @param hScale is a reference to the new horizontal scale value.
 * @param vScale is a reference to the new vertical scale value.
 * @param alpha is a reference to the new transparency value.
 */
void owl::cLinearAnimate::update(
    int &left, int &top, float &hScale, float &vScale, float &alpha)
{
  double elapsedTime = 0.0;
  float k;
  struct timespec t1;

  // update elapsed seconds
  clock_gettime(CLOCK_MONOTONIC, &t1);
  elapsedTime = elapsedTimeSec(mStartTime, t1);

  elapsedTime = CLAMP(0.0, elapsedTime, (double)mDuration);

  // convert elapsed time to fraction complete
  k = elapsedTime / mDuration;

  k = CLAMP(0.0f, k, 1.0f);

  // account for direction of animation
  if (mDir > 0)
    { k = 1.0 - k; }

  // interpolate between endpoints
  left =   k * mBeginLeft   + (1.0f - k) * mEndLeft   + 0.5f;
  top =    k * mBeginTop    + (1.0f - k) * mEndTop    + 0.5f;
  hScale = k * mBeginHScale + (1.0f - k) * mEndHScale;
  vScale = k * mBeginVScale + (1.0f - k) * mEndVScale;
  alpha  = k * mBeginAlpha  + (1.0f - k) * mEndAlpha;
}

