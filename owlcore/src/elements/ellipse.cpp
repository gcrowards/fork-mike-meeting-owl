#include "elements/ellipse.hpp"
#include "gpu.hpp"
#include "logging.hpp"

owl::cEllipse::cEllipse(void)
{
}

/** @param xVal is the x coordinate that defines the center of the ellipse.
 * @param yVal is the y coordinate that defines the center of the ellipse.
 * @param widthVal is the width of the ellipse.
 * @param heightVal is the height of the ellipse.
 */
owl::cEllipse::cEllipse(int xVal, int yVal, int widthVal, int heightVal)
{
  x = xVal;
  y = yVal;
  width = widthVal;
  height = heightVal;
}

/** @param xVal is the x coordinate that defines the center of the ellipse.
 * @param yVal is the y coordinate that defines the center of the ellipse.
 * @param size is the width and height of the a ellipse.
 */
owl::cEllipse::cEllipse(int xVal, int yVal, owl::cSize size)
{
  x = xVal;
  y = yVal;
  width = size.width;
  height = size.height;
}

/** @param point is the coordinate of the center of the ellipse.
 * @param widthVal is the width of the ellipse.
 * @param heightVal is the height of the ellipse.
 */
owl::cEllipse::cEllipse(owl::cPoint point, int widthVal, int heightVal)
{
  x = point.x;
  y = point.y;
  width = widthVal;
  height = heightVal;
}

/** @param point is the coordinate of the center of the ellipse.
 * @param size is the width and height of a ellipse.
 */
owl::cEllipse::cEllipse(owl::cPoint point, owl::cSize size)
{
  x = point.x;
  y = point.y;
  width = size.width;
  height = size.height;
}

owl::cEllipse::~cEllipse()
{
}

/** Draw method that calls the corresponding GPU render method.
 *
 * @param gpu is a pointer to the cGpu instance for rendering.
*/
void owl::cEllipse::draw(cGpu *gpu)
{
  gpu->renderEllipse(x, y, width, height, red, green, blue, alpha,
		     lineWidth, filled);
}

/** Print comment and ellipse specifications.
 *
 * @param comment is a text string that can optionally be printed before the
 * ellipse specifications.
 */
void owl::cEllipse::print(std::string comment)
{
  LOGI("%s (%d, %d) %d x %d\n"
       "rgba: %.2f %.2f %.2f %.2f  lineWidth: %.1f\n",
       comment.c_str(), x, y, width, height,
       red, green, blue, alpha, lineWidth );
}
