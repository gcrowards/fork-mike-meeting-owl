/*
 * rgbaOverlayLists.cpp
 *
 *  Created on: Mar 28, 2017
 *
 * Generalized overlay object management system.
 */

#include "common.hpp"
#include "logging.hpp"
#include "elements/rgbaOverlayLists.hpp"


/* *** cRgbaOverlayLists *** */

/** Constructs an object that contains lists of RGBA overlay control objects.
 *
 * This object contains lists for each different type of overlay.
 */
owl::cRgbaOverlayLists::cRgbaOverlayLists()
  : mStartupProgressBar(cv::Rect(0, 0, 0, 0)),
    mpOverlayManager(nullptr)
{
}

/** Destroys an object that contains lists of RGBA overlay control objects. */
owl::cRgbaOverlayLists::~cRgbaOverlayLists()
{
  // Delete the objects pointed to by elements of the icons list.
  std::vector<owl::cIconOverlay *> *pIcon = &mIconOverlays;

  for (std::vector<owl::cIconOverlay *>::iterator o = pIcon->begin();
      o != pIcon->end();
      o++)
    {
      delete (*o);
    }
}

/** Sets coordinates indicating extent of progress bar.
 *
 * When progress bar should be blank, or unused, pass width = 0.
 *
 * @param rect is the bounding rectangle of the progress bar.
 * @param color is the color of the progress bar.
 */
void owl::cRgbaOverlayLists::setProgressBar(cv::Rect rect, cv::Scalar color)
{
  mStartupProgressBar = rect;
  mStartupProgressColor = color;
}

/** Find a pinned Aoi overlay that uses the given texture unit.
 *
 * @param texId is the texture unit ID to find.
 *
 * @return a pointer to the cPinnedAoiOverlay object that matches, or nullptr
 * if a match is not found.
 */
owl::cPinnedAoiOverlay *
owl::cRgbaOverlayLists::findPinnedAoiOverlayForTexture(int texId)
{
  owl::cPinnedAoiOverlay *pOverlay = nullptr;

  for (std::vector<owl::cPinnedAoiOverlay>::iterator iter = mPinnedAoiOverlays.begin();
      iter != mPinnedAoiOverlays.end();
      iter++)
    {
      if ((*iter).getRenderId() == texId)
        {
          pOverlay = &(*iter);
        }
    }
  return pOverlay;
}

/** Tell the Visual Editor to draw or not draw overlay graphics.
 *
 * @param state is true to draw, false to not draw.
 */
void owl::cRgbaOverlayLists::setSuppressOverlays(bool state)
{
  mSuppressOverlays = state;
}

/** Get the current setting telling to draw or not draw graphics.
 *
 * @return true if graphics are drawn, false if not.
 */
bool owl::cRgbaOverlayLists::getSuppressOverlays(void)
{
  return mSuppressOverlays;
}
