/*
 * iconOverlay.cpp
 *
 *  Created on: Mar 28, 2017
 *
 * Generalized overlay object management system.
 */

#include "gpu.hpp"
#include "imageFileUtils.hpp"
#include "logging.hpp"
#include "elements/iconOverlay.hpp"


/* *** cIconOverlay *** */

/** Constructs an icon overlay object.
 *
 * @param renderId is the texture unit ID used to render this overlay.
 * @param width is the horizontal size of the bitmap to render.
 * @param height is the vertical size of the bitmap to render.
 */
owl::cIconOverlay::cIconOverlay(int renderId, std::string fileName)
  : owl::cRgbaOverlayBase(renderId),
    mIconId(renderId),
    mOwner(NONE),
    mAnimating(false),
    mAnimationDir(1),
    mActive(false),
    mLoaded(false)
{
  pngFileGetImageSize((char *)fileName.c_str(), &mWidth, &mHeight);
  mpBuf = new uint32_t [mWidth * mHeight];
  pngFileRead((uint8_t *)mpBuf, (char *)fileName.c_str());
}

/** Destroys an icon overlay object. */
owl::cIconOverlay::~cIconOverlay()
{
  delete [] mpBuf;
}

/** Set the activity that owns the icon for display.
 *
 * @param owner is the owning activitiy.
 */
void owl::cIconOverlay::setOwner(eIconOwner_t owner)
{
  if (owner != mOwner)
    {
      mOwner = owner;
      // do initialization stuff?
    }
}

/** Report the activity that owns the icon for display.
 *
 * @return the owner activity.
 */
owl::cIconOverlay::eIconOwner_t owl::cIconOverlay::getOwner(void)
{
  return mOwner;
}

/** Select the icon to be active.
 *
 * @param enable marks the icon as active if true, otherwise inactive.
 */
void owl::cIconOverlay::setIconActive(bool enable)
{
  mActive = enable;
  // if disabling, mark loaded as false
  if (!enable)
    {
      mLoaded = false;
    }
}

/** Report if icon is selected to be active.
 *
 * @return true if the icon is active, false otherwise.
 */
bool owl::cIconOverlay::isActive(void)
{
  return mActive;
}

/** Report if icon is loaded into GPU texture.
 *
 * @return true if the icon is loaded, false otherwise.
 */
bool owl::cIconOverlay::isLoaded(void)
{
  return mLoaded;
}

/** Load icon bitmap into the GPU.
 *
 */
void owl::cIconOverlay::loadActiveIcon(void)
{
  mpGpu->setRgbaTexture(mIconId, (uint8_t *)mpBuf, mWidth, mHeight);
  mLoaded = true;
}

/** Manage the animation of the logo icon, as necessary.
 *
 * @param nPanels is the number of panels displayed on stage.
 */
void owl::cIconOverlay::animationControl(int nPanels)
{
  // Only control the state of animation if in runtime
  if (mOwner == RUNTIME)
    {
      if (mAnimating)
        {
          // check for end of animation
          if (!mAnimation.animating())
            {
              mAnimating = false; // turn off animation
              mAnimationDir = -mAnimationDir; // change direction for next time
            }
        }
      else
        {
          // check if animation should start
          if ( (mAnimationDir > 0 && nPanels > 1)      // panels on stage
               || (mAnimationDir < 0 && nPanels < 2) ) // no panels on stage
            {
              // reset timer and set flag to start animating
              mAnimation.setDirection(mAnimationDir);
              mAnimation.start();
              setVisible(true);
              mAnimating = true;
            }
        }

    }
  else
    {
      // reset animation control parameters for next time
      mAnimating = false;
      mAnimationDir = 1;
    }
}

