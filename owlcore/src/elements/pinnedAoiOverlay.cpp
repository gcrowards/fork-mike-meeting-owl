/*
 * pinnedAoiOverlay.cpp
 *
 *  Created on: Mar 28, 2017
 *
 * Generalized overlay object management system.
 */

#include "common.hpp"
#include "logging.hpp"
#include "elements/pinnedAoiOverlay.hpp"


/* *** cPinnedAoiOverlay *** */

/* Constructs a 'pinned Aoi' overlay object.
 *
 * @param renderId is the texture unit ID used to render this overlay.
 * @param width is the horizontal size of the bitmap to render.
 * @param height is the vertical size of the bitmap to render.
 */
owl::cPinnedAoiOverlay::cPinnedAoiOverlay(int renderId, int width, int height)
  : owl::cRgbaOverlayBase(renderId, width, height),
    mPinnedAoiId(-1)
{
}

/* Destroys a 'pinned Aoi' overlay object. */
owl::cPinnedAoiOverlay::~cPinnedAoiOverlay()
{

}

/* Sets the ID of the Aoi object that this overlay object is associated with.
 *
 * @param pinnedAoiId is the ID of the Aoi that this pin icon should be drawn
 * on.
 */
void owl::cPinnedAoiOverlay::setAoiId(int pinnedAoiId)
{
  mPinnedAoiId = pinnedAoiId;
}

/* Gets the ID of the Aoi object that this overlay object is associated with.
 *
 * @return the ID of the Aoi that this pin icon should be drawn on.
 */
int owl::cPinnedAoiOverlay::getAoiId(void)
{
  return mPinnedAoiId;
}

