///< AudioEqualizer.cpp - Contains code for the parameteric equalizer

///< Start of imported code

/* Simple implementation of Biquad filters -- Tom St Denis
*
* Based on the work

Cookbook formulae for audio EQ biquad filter coefficients
---------------------------------------------------------
by Robert Bristow-Johnson, pbjrbj@viconet.com  a.k.a. robert@audioheads.com

* Available on the web at

http://www.smartelectronix.com/musicdsp/text/filters005.txt

* Enjoy.
*
* This work is hereby placed in the public domain for all purposes, whether
* commercial, free [as in speech] or educational, etc.  Use the code and please
* give me credit if you wish.
*
* Tom St Denis -- http://tomstdenis.home.dhs.org
*/

/* this would be biquad.h */
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "AudioEqualizer.hpp"

#ifndef M_LN2
#define M_LN2	   0.69314718055994530942
#endif

#ifndef M_PI
#define M_PI		3.14159265358979323846
#endif

/** Calculates the mag from a db value
*
* @param - db value
*
* @return - magnitude in 1.15
*/
double EqDb2mag(double dbval)
{
  double dVal;

  dVal = pow(10, dbval / 20.0);

  return dVal;

}

/** Processes a single sample through a Biquad
*
* @param sample - input sample in smp_type
* @param b - points to biquad structure
*
* @return processed sample
*/
inline smp_type BiQuad(smp_type sample, biquad *b)
{
	smp_type result;

	/* compute result */
	result = b->a0 * sample + b->a1 * b->x1 + b->a2 * b->x2 -
		b->a3 * b->y1 - b->a4 * b->y2;

	/* shift x1 to x2, sample to x1 */
	b->x2 = b->x1;
	b->x1 = sample;

	/* shift y1 to y2, result to y1 */
	b->y2 = b->y1;
	b->y1 = result;

	return result;
}

/** Calculates the coefficients for a biquad of a particular configuration
*
* @param b - pointer to a biquad structure
* @param type - type of filter to be configured
* @param freq - appropriate frequency of the filter (e.g. center, cutoff).
* @param gain - gain for the filter (if used).
* @param srate - sample rate for the filter
* @param bandwidth - bandwidth for the filter (if used).
*
* @return 0 if successful; -1 if not

/* sets up a BiQuad Filter */
int BiQuadConfigure(biquad *b, int type, smp_type dbGain, smp_type freq,
	smp_type srate, smp_type bandwidth)
{
	smp_type A, omega, sn, cs, alpha, beta,Q;
  smp_type wc; // used for 1 pole filters
  smp_type K;  // used for 1 pole filters

	smp_type a0, a1, a2, b0, b1, b2;

	/* setup variables */
	A = pow(10, dbGain / 40);
	omega = 2 * M_PI * freq / srate;
	sn = sin(omega);
	cs = cos(omega);
	alpha = sn * sinh(M_LN2 / 2 * bandwidth * omega / sn);
	beta = sqrt(A + A);
	Q = 1.0/(2 * sinh(M_LN2 / 2 * bandwidth));
	switch (type) {
	case LPF:
		b0 = (1 - cs) / 2;
		b1 = 1 - cs;
		b2 = (1 - cs) / 2;
		a0 = 1 + alpha;
		a1 = -2 * cs;
		a2 = 1 - alpha;
		break;
	case HPF:
		b0 = (1 + cs) / 2;
		b1 = -(1 + cs);
		b2 = (1 + cs) / 2;
		a0 = 1 + alpha;
		a1 = -2 * cs;
		a2 = 1 - alpha;
		break;
	case BPF:
		b0 = alpha;
		b1 = 0;
		b2 = -alpha;
		a0 = 1 + alpha;
		a1 = -2 * cs;
		a2 = 1 - alpha;
		break;
	case NOTCH:
		b0 = 1;
		b1 = -2 * cs;
		b2 = 1;
		a0 = 1 + alpha;
		a1 = -2 * cs;
		a2 = 1 - alpha;
		break;
	case PEQ:
		b0 = 1 + (alpha * A);
		b1 = -2 * cs;
		b2 = 1 - (alpha * A);
		a0 = 1 + (alpha / A);
		a1 = -2 * cs;
		a2 = 1 - (alpha / A);
		break;
	case LSH:
		b0 = A * ((A + 1) - (A - 1) * cs + beta * sn);
		b1 = 2 * A * ((A - 1) - (A + 1) * cs);
		b2 = A * ((A + 1) - (A - 1) * cs - beta * sn);
		a0 = (A + 1) + (A - 1) * cs + beta * sn;
		a1 = -2 * ((A - 1) + (A + 1) * cs);
		a2 = (A + 1) + (A - 1) * cs - beta * sn;
		break;
	case HSH:
		b0 = A * ((A + 1) + (A - 1) * cs + beta * sn);
		b1 = -2 * A * ((A - 1) + (A + 1) * cs);
		b2 = A * ((A + 1) + (A - 1) * cs - beta * sn);
		a0 = (A + 1) - (A - 1) * cs + beta * sn;
		a1 = 2 * ((A - 1) - (A + 1) * cs);
		a2 = (A + 1) - (A - 1) * cs - beta * sn;
		break;
  case LPF1P:
    // from Apogee Technology app note AN-06
    wc = omega;
    K = tan(wc/2.0);
    alpha = 1 + K;
    a0 = 1;
    a1 = -((1-K)/alpha);
    b0 = K/alpha;
    b1 = K/alpha;
    a2 = 0;
    b2 = 0;
    break;
  case HPF1P:
    // from Apogee Technology app note AN-06  
    wc = omega;
    K = tan(wc/2.0);
    alpha = 1 + K;
    a0 = 1;
    a1 = -((1-K)/alpha);
    b0 = 1/alpha;
    b1 = -1/alpha;
    a2 = 0;
    b2 = 0;
    break;
		
	default:
		return -1;
	}

	/* precompute the coefficients */
	b->a0 = b0 / a0;
	b->a1 = b1 / a0;
	b->a2 = b2 / a0;
	b->a3 = a1 / a0;
	b->a4 = a2 / a0;

	/* zero initial samples */
	b->x1 = b->x2 = 0;
	b->y1 = b->y2 = 0;

	return 0;
}
///< end of imported code

/** Initializes all of the eq sections
*
* @param pEq - pointer to the equalizer structure
* @param fs	 - sample rate for the entire eq
*
* @return void
*/
void EqInit(EQUALIZER *pEq, int fs)
{
	int i;
	pEq->fs = fs;
	pEq->trim = 0;

	for (i = 0; i < EQ_SECTION_COUNT; ++i)
	{
		pEq->sections[i].flags = 0;
		pEq->sections[i].freq = 1000;
		pEq->sections[i].gain = 0;
		pEq->sections[i].type = PEQ;
	}
	return;


}

/** Activates a particular channel of the equalizer
*
* @param pEq - pointer to the eq structure
* @param chanNum - number of the channel of the EQ to activate
* @param state - ~= 0 will activate the channel 0 will deactivate
*
* @return 0 if successful; -1 otherwise
*/
int EqActivate(EQUALIZER *pEq,int chanNum, int state)
{
	if((chanNum < 0) || (chanNum >= EQ_SECTION_COUNT))
	{
		return -1;
	}
	pEq->sections[chanNum].flags = state;

	return 0;
}

/** Configures a channel of the equalizer
*
* @param pEq - pointer to the equalizer structure
* @param chanNum - number of the channel to configure
* @param type - type of filter to be configured
* @param freq - appropriate frequency of the filter (e.g. center, cutoff).
* @param gain - gain for the filter (if used).
* @param bandwidth - bandwidth for the filter (if used).
*
* @return 0 if successful; -1 if not
*/
int EqConfigure(EQUALIZER *pEq, int chanNum, section_type type, double freq,
	double gain, double bandwidth)
{
	if ((chanNum < 0) || (chanNum >= EQ_SECTION_COUNT))
	{
		return -1;
	}
	pEq->sections[chanNum].type = type;
	pEq->sections[chanNum].freq = freq;
	pEq->sections[chanNum].gain = gain;
	pEq->sections[chanNum].bandwidth = bandwidth;
	BiQuadConfigure(&pEq->sections[chanNum].sectionBiquad,
		pEq->sections[chanNum].type,
		pEq->sections[chanNum].gain,
		pEq->sections[chanNum].freq,
		pEq->fs,
		pEq->sections[chanNum].bandwidth);
	return 0;
}

/** Configure the post gain for the EQ
*
* @param pEq - pointer to the equalizer to configure
* @param gain - Gain to be added after EQ, in db
*
* @return 0 if successful; -1 if not
*/
int EqPostGain(EQUALIZER *pEq,double gain)
{
  pEq->trim = EqDb2mag(gain);
  return 0;
}
/** Process a single sample of audio through all eq channels
*
* @param pEq - pointer to the eq to process
* @param sample - input sample
*
* @return processed sample
*/
inline short EqProcessSample(EQUALIZER *pEq, short sample)
{
	int i;
	smp_type inSample;
	short outSample;
	inSample = (smp_type)sample;
	for (i = 0; i < EQ_SECTION_COUNT; ++i)
	{
		if (pEq->sections[i].flags != 0)
		{
			inSample = BiQuad(inSample, &pEq->sections[i].sectionBiquad);
		}
	}
	inSample = inSample * pEq->trim;
	if (inSample < -32767.0)
	{
		inSample = -32767.0;
	}
	if (inSample > 32767.0)
	{
		inSample = 32767.0;
	}
	outSample = (short)(inSample);
	return outSample;
}

/** Processes a buffer of audio through the equalizer
*
* @param pEq - pointer to the eq structure
* @param pBuff - pointer to the buffer of audio
* @param len - number of audio frames to process
* @param eqChan - the channel to equalize
* @param totalChans - the total number of channels
*
* @return 0 if successful; -1 if not
*/
int EqProcess(EQUALIZER *pEq, short *pBuff, int len, int eqChan, int totalChans)
{
	int i;

	for (i = 0; i < len; ++i)
	{
		pBuff[i*totalChans + eqChan] = EqProcessSample(pEq, pBuff[i*totalChans + eqChan]);
	}

	return 0;
}


/** Processes a buffer of audio through the equalizer
*
* @param pEq - pointer to the eq structure
* @param pBuff - pointer to the buffer of audio
* @param len - number of audio frames to process
* @param eqChan - the channel to equalize
* @param totalChans - the total number of channels
*
* @return 0 if successful; -1 if not
*/
double EqRMSProcess(EQUALIZER *pEq, short *pBuff, int len, int eqChan, int totalChans)
{
	int i;
	short temp;
	double temp_f;
	double rms_sum = 0;

	for (i = 0; i < len; ++i)
	{
		temp  = EqProcessSample(pEq, pBuff[i*totalChans + eqChan]);
		temp_f = ((double) temp)/ 32768.0;
		rms_sum += (temp_f*temp_f);
	}
	
	return(sqrt(rms_sum/len));
}
