#include "voice.hpp"

#include <cstddef>

cVoice::cVoice(void)
{
  mDirection = -1;
  mScore = -1.f;
}

/** @param direction is the bearing to the audio source (0 - 360 degrees in 
 * 1 degree increments).
 * @param score is the confidence in the direction.
 * @param audioData is a vector containing the digitized audio signal associated
 * with this Voice.
 */
cVoice::cVoice(int direction, float score, std::vector<int> audioData)
{
  mDirection = direction;
  mScore = score;
  mAudioData = audioData;
}

cVoice::~cVoice()
{
}

/** Assign direction from which sound is believed to be coming from. 
 *
 * @param direction is the bearing to the audio source (0 - 360 degrees in 
 * 1 degree increments). 
 */
void cVoice::setDirection(int direction)
{
  mDirection = direction;
}

/** Assign Voice's score value.
 * 
 * @param score is the confidence in the direction. 
 */
void cVoice::setScore(float score)
{
  mScore = score;
}

/**  Assign audio data for Voice.
 *
 * @param audioData is a vector containing the digitized audio signal associated
 * with this Voice.
 */
void cVoice::setAudioData(std::vector<int> audioData)
{
  mAudioData = audioData;
}

/** Return the direction of the sound source (0 - 360 degrees in 1 degree
    increments). */
int cVoice::getDirection(void)
{
  return mDirection;
}

/** Return the confidence in the believed direction. */
float cVoice::getScore(void)
{
  return mScore;
}

/** Return a vector containing the digitized audio signal associated with the
    Voice. */
std::vector<int> * cVoice::getAudioData(void)
{
  return &mAudioData;
}
