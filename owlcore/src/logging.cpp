#include <string.h>

#include "common.hpp"
#include "logging.hpp"

/** Define LOG_TO_FILE to save LOGx() output to the Owl's internal disk
 * in the /sdcard directory.
 */
// #define LOG_TO_FILE


/** Callback used by logger to write to a switchboard client. Could also be
 * used to send logging strings to an arbitrary function for manipulation,
 * distribution, or storage.
 */
static logCallback_t gLogCallback = nullptr;
/** String to prepend to log messages sent through the callback. */
static char gLogHeader[MAX_LOG_MSG_LEN] = "";

/** This function is the interface for logging messages to either
 * stdout or a log file using one of the logging macros, LOGI(),
 * LOGE(), etc.
 *
 * The function expects a variable argument list that includes a
 * format string like printf(). Note that the LOGF() macro directs
 * logging information to the file only, and the LOGV() macro directs
 * logging information to stdout only. All other macros direct logging
 * information to both streams, assuming the global LOG_LEVEL is set
 * accordingly.
 *
 * @param level is the logging level.
 */
void   owlLog(int level, ...)
{
  va_list args;
  va_start(args, level);
  if (level != OWL_LOG_FILE_ONLY)
    {
      // log to stdout
      owlLogStream(stdout, level, args);
      if (gLogCallback != nullptr)
        {
          char msg[MAX_LOG_MSG_LEN];

          // Create the string in msg character array
          owlLogString(msg, gLogHeader, level, args);
          // Invoke the callback to send the string
          gLogCallback(msg, sizeof(msg));
        }
    }
#ifdef LOG_TO_FILE
  if (level >= OWL_LOG_DEBUG)
    {
      // log to file
      owlLogFile(level, args);
    }
#endif
  va_end(args);
}

/** Configure remote logging settings.
 *
 * @param logCallback is the interface to call to send each message.
 * @param pHeader is a string to pre-pend on each message.
 */
void   owlLogConfigureRemote(logCallback_t logCallback, char *pHeader)
{
  gLogCallback = logCallback;
  strncpy(gLogHeader, pHeader, sizeof(gLogHeader));
}

/** This function logs text to a file with a timestamped file name.
 *
 * @param level is the logging level.
 * @param ap is a variable argument list.
 */
void   owlLogFile(int level, va_list ap)
{
  static FILE *pFile = NULL;

  if (pFile == NULL)
    {
      char  pFilepath[256];
#ifdef __ANDROID__
      sprintf(pFilepath, "/sdcard/owl-%s.log", currentDateTime().c_str());
#else
      sprintf(pFilepath, "./owl-%s.log", currentDateTime().c_str());
#endif
      pFile = fopen(pFilepath, "w");
      if (pFile == NULL)
	{ printf("ERROR:   unable to open logging file for output\n"); }
    }

  owlLogStream(pFile, level, ap);
}

/** This function logs text to a stream.
 *
 * @param stream is a file stream, usually stdout or a text file.
 * @param level is the logging level.
 * @param ap is a variable argument list.
 */
void   owlLogStream(FILE *stream, int level, va_list ap)
{
  va_list args;
  char    *pFmt;
  
  switch(level)
    {
    case OWL_LOG_VERBOSE:   fprintf(stream, "VERBOSE: "); break;
    case OWL_LOG_DEBUG:     fprintf(stream, "DEBUG:   "); break;
    case OWL_LOG_INFO:      fprintf(stream, "INFO:    "); break;
    case OWL_LOG_WARN:      fprintf(stream, "WARN:    "); break;
    case OWL_LOG_ERROR:     fprintf(stream, "ERROR:   "); break;
    case OWL_LOG_FILE_ONLY: fprintf(stream, "LOGFILE: "); break;
    default:                fprintf(stream, "UNKN:    "); break;
    }
  va_copy(args, ap);
  pFmt = va_arg(args, char*);
  vfprintf(stream, pFmt, args);
  va_end(args);
  fflush(stream);
}

/** This function logs text to a string for distribution via the callback.
 *
 * @param stream is a file stream, usually stdout or a text file.
 * @param level is the logging level.
 * @param ap is a variable argument list.
 */
void   owlLogString(char msg[MAX_LOG_MSG_LEN], char *pHeader, int level,
                    va_list ap)
{
  va_list args;
  char    *pFmt;

  strncpy(msg, pHeader, MAX_LOG_MSG_LEN - 9);
  switch(level)
    {
    case OWL_LOG_VERBOSE:   strcat(msg, "VERBOSE: "); break;
    case OWL_LOG_DEBUG:     strcat(msg, "DEBUG:   "); break;
    case OWL_LOG_INFO:      strcat(msg, "INFO:    "); break;
    case OWL_LOG_WARN:      strcat(msg, "WARN:    "); break;
    case OWL_LOG_ERROR:     strcat(msg, "ERROR:   "); break;
    case OWL_LOG_FILE_ONLY: strcat(msg, "LOGFILE: "); break;
    default:                strcat(msg, "UNKN:    "); break;
    }
  va_copy(args, ap);
  pFmt = va_arg(args, char*);
  vsnprintf(&msg[strlen(msg)], MAX_LOG_MSG_LEN, pFmt, args);
  va_end(args);
}


