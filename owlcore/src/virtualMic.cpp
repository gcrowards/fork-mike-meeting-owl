#include <cmath>
#include <iostream>
#include <iomanip>
#include <unistd.h>

#include "debugSwitches.h"
#include "logging.hpp"
#include "params.h"
#include "virtualMic.hpp"

cVirtualMic::cVirtualMic(void)
{
  mIsReady = false;

  // initialize mutex
  if (pthread_mutex_init(&mMutex, NULL) != 0)
    { LOGE("unable to init virtualMic mutex\n"); }
}

cVirtualMic::~cVirtualMic()
{
  // destroy mutex
  pthread_mutex_destroy(&mMutex);
}

std::vector<cVoice> cVirtualMic::getVoices(void)
{
  // return copy of mVoices, guarded by mutex
  std::vector<cVoice> voices;
  pthread_mutex_lock(&mMutex);
  voices = mVoices;
  pthread_mutex_unlock(&mMutex);
  return voices;
}

std::vector<int> cVirtualMic::getSoundBearings(void)
{  
  // return copy of mSoundBearings, guarded by mutex
  std::vector<int> soundBearings;
  pthread_mutex_lock(&mMutex);
  soundBearings = mSoundBearings;
  pthread_mutex_unlock(&mMutex);
  return soundBearings;
}

/** Thuis function updates the sound data for the cVirtualMic instance.
 *
 * @param voiceBearing is an integer [0, 359] for the voice angle;
 * pass a negative value if the direction is unknown.
 * @param soundBearings is a vector of integers for sound angles, each
 * in [0, 359].
 */
void cVirtualMic::update(int voiceBearing, std::vector<int> soundBearings)
{
  // Note that there should be little computation done here so that we
  // don't block the main A/V pipeline when
  // cOwl::update(pEglClientBuffer) gets voice data and sound
  // bearings. Anything computationally intensive should be done in
  // cOwlHook::update(pBeamInfo) or in the thread that calls
  // cOwlHook::update(pBeamInfo), e.g., DirectorHandler.c for Android
  // libOwlPG.
  // Also note that a mutex is need to guard access to the member variables.
  
  const float kScore = 25.;  // This is a quick sub-in to match the scoring
  // system of the old 4-directional-mic system;
  // It has no real significance
   
  // lock resources
  pthread_mutex_lock(&mMutex);

  // clear previous voices
  mVoices.clear();
   
  // store new voice bearing
  mActiveMicDirection = voiceBearing;
  
  if(mActiveMicDirection >= 0 && mActiveMicDirection <= 360)
    {
      mVoices.push_back(cVoice(mActiveMicDirection, kScore,
			       std::vector<int>() ) ); 
    }
   
  // store new sound bearings
  mSoundBearings = soundBearings;

  // unlock resources
  pthread_mutex_unlock(&mMutex);
}

bool cVirtualMic::isReady(void)
{
  return true;
}
