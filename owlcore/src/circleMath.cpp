#include "circleMath.hpp"

#include <algorithm>
#include <cmath>

namespace circleMath
{
  /* add ang1 and ang2, account for 0/360 degree wrap */
  int intAddAngs(int ang1, int ang2)
  { // TODO: Have this function call intAdd() w/ iMax = 360
    int sum = ang1 + ang2;
    if(sum > 360) {sum -=360;}
    return sum;
  }
  
  /* subtract ang2 from ang1, account for 0/360 degree wrap */
  int intSubAngs(int ang1, int ang2)
  { // TODO: Have this function call intSub() w/ iMax = 360
    int diff = ang1 - ang2;
    if(diff < 0) {diff +=360;}
    return diff;
  }
  
  /** find the smallest angular distance between two angles; sign indicates
   * direction from ang1 to ang2 to traverse shortest angle. */
  int intDiffAngs(int ang1, int ang2)
  {
    int diff = circleMath::intSubAngs(ang1, ang2);
    if(diff > 180) {diff -= 360;}
    return diff;
  }
  
  /** Add two integers and account for wrap around at specified max value.
   * 
   * @param i1 is the first integer to operate on.
   * @param i2 is the second integer to operate on.
   * @param iMax is the maximum value before wrap-around to iMin.
   * @param iMin is the minimum value before wrap-around to iMax.
   *
   * @return i1 + i2 on the range [0, iMax].
   */
  int intAdd(int i1, int i2, int iMax, int iMin)
  {
    int sum = i1 + i2;
    if(sum > iMax) {sum -= (iMax + iMin);}
    if(sum < iMin) {sum += (iMax - iMin);}
    return sum;
  }

  /* Add two floats and account for wrap around at 360.0 degrees. 
   *
   * @param ang1 first float angle.
   * @param ang2 second float angle.
   *
   * @return sum of angles [0..360].
   */
  float floatAddAngs(float ang1, float ang2)
  { // TODO: Have this function call floatAdd() w/ iMax = 360
    float sum = ang1 + ang2;
    if(sum > 360.0) {sum -=360.0;}
    return sum;
  }

  /** Add two floats and account for wrap around at specified max value.
   * 
   * @param f1 is the first float to operate on.
   * @param f2 is the second float to operate on.
   * @param fMax is the maximum value before wrap-around to fMin.
   * @param fMin is the minimum value before wrap-around to fMax.
   *
   * @return f1 + f2 on the range [0, fMax].
   */
  float floatAdd(float f1, float f2, float fMax, float fMin)
  {
    float sum = f1 + f2;
    if(sum > fMax) {sum -= (fMax + fMin);}
    if(sum < fMin) {sum += (fMax - fMin);}
    return sum;
  }

  /** Subtract ang2 from ang1, account for 0/360 degree wrap.
   *
   * @param ang1 is the angle to subtract from.
   * @param ang2 is the angle to subtract.
   *
   * @return ang1 - ang2 on the range [0., 360.]
   */
  float floatSubAngs(float ang1, float ang2)
  { // TODO: Have this function call floatSub() w/ iMax = 360.0
    float diff = ang1 - ang2;
    if(diff < 0.) {diff +=360.;}
    return diff;
  }

  /** Subtract two integers and account for wrap around at iMin/iMax.
   * 
   * @param i1 is the first integer to operate on.
   * @param i2 is the second integer to operate on.
   * @param iMax is the maximum value before wrap-around to iMin.
   * @param iMin is the minimum value before wrap-around to iMax.
   *
   * @return i1 - i2 on the range [iMin, iMax].
   */
  int intSub(int i1, int i2, int iMax, int iMin)
  { // TODO: This function may not work right if subtracting negative number(s).
    //       See how intAdd() deals with the situation.
    int diff = i1 - i2;
    if(diff < iMin) {diff += iMax;}
    return diff;
  }
  
  /** Subtract two floats and account for wrap around at fMin/fMax.
   * 
   * @param f1 is the first float to operate on.
   * @param f2 is the second float to operate on.
   * @param fMax is the maximum value before wrap-around to fMin.
   * @param fMin is the minimum value before wrap-around to fMax.
   *
   * @return f1 - f2 on the range [fMin, fMax].
   */
  float floatSub(float f1, float f2, float fMax, float fMin)
  { // TODO: This function may not work right if subtracting negative number(s).
    //       See how floatAdd() deals with the situation.
    float diff = f1 - f2;
    if(diff < fMin) {diff += fMax;}
    return diff;
  }
 
  /** Find the smaller linear distance between to values (accounting for wrap
   * at iMin/iMax).
   *
   * The sign of the result indicates whether traveling right or left from
   * i1 will get you to i2 with the shortest number of steps.
   * 
   * @param i1 is the first integer to operate on.
   * @param i2 is the second integer to operate on.
   * @param iMax is the maximum value before wrap-around to iMin.
   * @param iMin is the minimum value before wrap-around to iMax.
   *
   * @return The smallest linear distance between i1 and i2 on 
   * range [iMin, iMax] (may be positive or negative).
   */
  int intDiff(int i1, int i2, int iMax, int iMin)
  {
    int diff = circleMath::intSub(i1, i2, iMax, iMin);
    if(diff > (iMax / 2)) {diff -= iMax;}
    return diff;
  }

  /** Find the smaller linear distance between to values (accounting for wrap
   * at fMin/fMax).
   *
   * The sign of the result indicates whether traveling right or left from
   * f1 will get you to f2 with the shortest number of steps.
   * 
   * @param f1 is the first float to operate on.
   * @param f2 is the second float to operate on.
   * @param fMax is the maximum value before wrap-around to fMin.
   * @param fMin is the minimum value before wrap-around to fMax.
   *
   * @return The smallest linear distance between f1 and f2 on 
   * range [fMin, fMax] (may be positive or negative).
   */
  float floatDiff(int f1, int f2, int fMax, int fMin)
  {
    float diff = circleMath::floatSub(f1, f2, fMax, fMin);
    if(diff > (fMax / 2)) {diff -= fMax;}
    return diff;
  }
  
  /** Average two intergers, accounting for wrap around at iMin/IMax.
   *
   * @params i1 is the first integer to operate on.
   * @params i2 is the second integer to operate on.
   * @param iMax is the maximum value before wrap-around to iMin.
   * @param iMin is the minimum value before wrap-around to iMax.
   *
   * @return The average value of i1 and i2.
   */
  int intAvg(int i1, int i2, int iMax, int iMin)
  {
    int avg;
    int diff = std::abs(circleMath::intDiff(i1, i2, iMax, iMin));

    if(diff == std::abs(i1 - i2))
      {avg = std::min(i1, i2) + (diff / 2);}
    else
      {avg = intAdd(std::max(i1, i2), (diff / 2), iMax);}

    return avg;
  }
 
  /** Test if the given point in contained within the given rectangle 
   * (accounting for the wrap-around at iMin/iMax).
   *
   * @param point is the cv::Point to test.
   * @param rect is the cv::Rect to test against.
   * @param iMax is the maximum value before wrap-around to iMin.
   * @param iMin is the minimum value before wrap-around to iMax.
   *
   * @return True if point is fully contained within rectangle.
   */
  bool isPointInRect(cv::Point point, cv::Rect rect, int iMax, int iMin)
  {
    // figure out if rectangle is hanging off the wrap-around boundary)
    int xRight = rect.x + rect.width;
    int adder = 0;

    // update adder if rectangle is beyond one of the boundaries
    // WARNING: Not checking for rectangle being beyond both boundaries at once!
    if(rect.x < iMin) 
      {adder = iMin - rect.x;}
    if(xRight > iMax) 
      {adder = iMax - xRight;}

    // shift point & rectangle to move away from wrap edge
    // Note: adder may be +, -, or 0
    point.x = circleMath::intAdd(point.x, adder, iMax, iMin);
    rect.x += adder;
     
    // apply test
    if((point.x >= rect.x) && 
       (point.x <= (rect.x + rect.width)) &&
       (point.y >= rect.y) &&
       (point.y <= (rect.y + rect.height)) )
      {
	return true;
      }

    return false;
  } 

  /** Generate list of rectangles needed to crop the given sub-rectangle out
   *  of the provided source rectangle.
   *   
   *  NOTE: Currently this function only operates on the x-axis!
   *
   * @param src is the cv::Size of the source rectangle that should be used to
   * crop the sub-rectangle
   * @param subRect is the cv::Rect that will be cropped based on its x position
   * and width relative to the source rectangle.
   * @param clip Optional boolean that specifies whether or not the excess rect
   * should be returned as well as the non-excess rect portion.
   *
   * @return The list of generated rectangles from the crop operation.
   */
  std::vector<cv::Rect> cropRect(cv::Size src, cv::Rect subRect, bool clip)
  {
    std::vector<cv::Rect> rects;
    int x; int y; int width; int height;
    
    if(clip)
      { // clip (if need be) the hanging sub-rectangle instead of wrapping
	x = std::max(subRect.x, 0);
	y = subRect.y;
	
	if((x + subRect.width) < src.width)
	  {width = subRect.width;}
	else
	  {width = src.width - x;}
	
	height = subRect.height;

	rects.push_back(cv::Rect(x, y, width, height));
      }
    else if(subRect.x < 0)
      { // sub-rectangle is falling off left side of source
	x = src.width + subRect.x;   // remember, subRect.x is negative
	y = subRect.y;
	width = std::min((src.width - x), subRect.width);
	height = subRect.height;
		
	rects.push_back(cv::Rect(x, y, width, height));
	
	x = 0;
	width = subRect.width - width;
	
	rects.push_back(cv::Rect(x, y, width, height));
      }
    else if((subRect.x + subRect.width) > src.width)
      { // sub-rectangle is falling off right side of source
	x = subRect.x;
	y = subRect.y;
	width = std::min((src.width - x), subRect.width);
	height = subRect.height;
	
	rects.push_back(cv::Rect(x, y, width, height));
	
	x = 0;
	width = subRect.width - width;
	
	rects.push_back(cv::Rect(x, y, width, height));
      }
    else
      { // sub-rectangle is fully within source
	rects.push_back(subRect);
      }
    
    return rects;    
  }
}
  
