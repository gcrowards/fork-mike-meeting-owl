#include "utils/colorGenerator.hpp"

cColorGenerator::cColorGenerator(void)
{
}

cColorGenerator::~cColorGenerator()
{
}

/** This function generates an RGB color based on the provided seed.  The same 
 * color is always returned for a given seed, and that color is tends to be 
 * relatively unique to colors returned for other nearby seeds.
 *
 * Note: This is a quick first-pass implementation that is completely made up.
 *       There are undoubtedly better ways to generate more distinct colors.
 *
 * @param seed is the seed value used to generate the color.
 *
 * @return an OpenCV scalar containing the determined RGB value.
 */
cv::Scalar cColorGenerator::getColorFromSeed(int seed)
{
  // color channels marked by 1 will get a non-zero value
  int rgbMask[3] = {0};

  // keep values to 8-bits
  if(seed > 0xFF) { seed %= 0xFF; }

  // offset seed so as to not do modulo math on 0 or 1
  seed += 2;

  // determine which color channels will be non-zero
  if(seed % 6 == 0)
    {
      rgbMask[0] = 1;
      rgbMask[1] = 1;
    }
  else if(seed % 5 == 0)
    {
      rgbMask[0] = 1;
      rgbMask[2] = 1;  
    }
  else if(seed % 4 == 0)
    {
      rgbMask[1] = 1; 
      rgbMask[2] = 1; 
    }
  else if(seed % 3 == 0)
    { 
      rgbMask[0] = 1; 
    }
  else if(seed % 2 == 0)
    { 
      rgbMask[1] = 1; 
    }
  else if(seed % 1 == 0)
    { 
      rgbMask[2] = 1; 
    }

  // set color channel value (scaled up to put some color space between 
  // sequential seed inputs)
  int val = (seed * 10) % 0xFF; 
  
  // values less than 100 yield colors so dark that it's hard to see the 
  // difference between small values changes
  if(val < 100) { val += 100; }

  return cv::Scalar(val * rgbMask[0], val * rgbMask[1], val * rgbMask[2]);
}
