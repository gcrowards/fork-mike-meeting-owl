#include "utils/csvParser.hpp"

/** Constructor
 *
 * @param discardCommentedLines denotes if the parser should treat a leading
 * "#" as a comment marker (true by default).
 */
cCsvParser::cCsvParser(bool discardCommentedLines)
{
  setDiscardCommentedLines(discardCommentedLines);
  setFilename("");
}

/** Constructor
 *
 * @param filename is the full path to the CSV file to parse.
 * @param discardCommentedLines denotes if the parser should treat a leading
 * "#" as a comment marker (true by default).
 */
cCsvParser::cCsvParser(std::string filename, bool discardCommentedLines)
{
  setDiscardCommentedLines(discardCommentedLines);
  setFilename(filename);
}

cCsvParser::~cCsvParser()
{
}

/** Parse the CSV file!
 *
 * This is the main function that makes all the action happe.
 *
 * @return true is file parsed without any detected errors.
 */
bool cCsvParser::parse(void)
{
  // step one, open the file
  if(!mFileStream.is_open())
    { 
      if(!openFile())
	{
	  return false;
	}
    }

  // step two, parse the file
  clearStringTable();
  std::string row;

  while(!mFileStream.eof())
    {
      // get each line of raw data
      std::getline(mFileStream, row);

      if(mDiscardCommentedLines)
	{
	  if(row.size() > 0) 
	    {
	      if(row.c_str()[0] == '#')
		{ // interpret # as a commented out line
		  continue; 
		}
	    }
	}

      // check for error
      if(mFileStream.bad())
	{ 
	  mFileStream.close();
	  return false; 
	}

      // parse this line of data
      std::vector<std::string> items = parseRow(row);

      // store parsed results in the String Table
      mStringTable.push_back(items);
    }

  // if we made it this far everything worked!
  mFileStream.close();

  return true;
}

/** Store filename of CSV file to parse. 
 *
 * @filename is the full path to the CSV file.
 */
void cCsvParser::setFilename(std::string filename)
{
  mFilename = filename;
}

/** Note whether or not to treat leading "#" as comment line (don't parse).
 *
 * @param discardCommentedLines true to discard comented lines.
 */
void cCsvParser::setDiscardCommentedLines(bool discardCommentedLines)
{
  mDiscardCommentedLines = discardCommentedLines;
}

/** @return the parsed data in string format. */
owl::stringTable_t cCsvParser::getStringTable(void)
{
  return mStringTable;
} 

/** Open CSV file.
 *
 * @return true on success.
 */
bool cCsvParser::openFile(void)
{
  if(mFilename == "") { return false; }

  mFileStream.open(mFilename);

  if(mFileStream.is_open())
    { return true; }
  else
    { return false; }
}

/** Close CSV file. */
void cCsvParser::closeFile(void)
{
  if(mFileStream.is_open()) { mFileStream.close(); }
}

/** Parse a the provided string (row) of raw data.
 *
 * @param row of data to parse.
 *
 * @return parsed data as vector of strings.
 */
std::vector<std::string> cCsvParser::parseRow(std::string row)
{
  std::vector<std::string> parsedItems {""};
  bool firstChar = true;  // first character of a field
  int itemNum = 0;

  // go through input string character by character
  for(char c : row)
    {
      switch(c)
	{
	case ',':
	  // end of comma separated field
	  parsedItems.push_back("");
	  itemNum++;
	  firstChar = true;
	  break;
	case ' ':
	  // ignore leading spaces
	  if(!firstChar) { break; }
	default:
	  // add character to item
	  parsedItems[itemNum].push_back(c);
	  firstChar = false;
	  break;
	}
    }
  
  return parsedItems;
}

/** Blank out the String Table.
 *
 * This makes it easy to re-use an instance to parse mulitple CSV files.
 */
void cCsvParser::clearStringTable(void)
{
  for(owl::stringTable_t::reverse_iterator iRow = mStringTable.rbegin();
      iRow != mStringTable.rend(); ++iRow )
    {
      mStringTable.erase(std::next(iRow).base());
    }
}
