/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  ledContext.cpp
 *  This is the implementation file for cLedContext.
 *   --> Creates an isolated set of LED states for different Owl modes. Can be
 *        passed to cLedInterface to apply a context to the physical LEDs.
 */

#include "ledContext.hpp"
#include "logging.hpp"

#include <unistd.h>

/** @param initialStates the starting LED states in this context. */
cLedContext::cLedContext(ledType_t initialStates)
  : mLedStates(initialStates)
{
  
}

/** Turns the specified LEDs on.
 *
 * @param leds a bitmask of the LEDs to enable.
 */
void cLedContext::enable(ledType_t leds)
{
  mLedStates |= leds;
  if(mpLedInterface)
    { mpLedInterface->enable(leds); }
}

/** Turns the specified LEDs off.
 *
 * @param leds a bitmask of the LEDs to disable.
 */
void cLedContext::disable(ledType_t leds)
{
  mLedStates &= (~leds);
  if(mpLedInterface)
    { mpLedInterface->disable(leds); }
}

/** Turns the specified LEDs on or off.
 *
 * @param leds a bitmask of the LEDs to set.
 * @param setEnabled true to enable, or false to disable.
 */
void cLedContext::setLeds(ledType_t leds, bool setEnabled)
{
  if(setEnabled)
    { enable(leds); }
  else
    { disable(leds); }
}

/** Turns the specified LEDs on.
 *
 * @return the current LED states in this context.
 */
ledType_t cLedContext::getStates() const
{
  return mLedStates;
}

void cLedContext::startBlinking(ledType_t leds, int onTimeMs, int offTimeMs)
{
  if(mpLedInterface)
    { mpLedInterface->startBlinking(leds, onTimeMs, offTimeMs); }
}

void cLedContext::stopBlinking()
{
  if(mpLedInterface)
    { mpLedInterface->stopBlinking(); }
}
