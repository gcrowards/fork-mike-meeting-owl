/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  buttonContext.cpp
 *  This is the implementation file for cButtonContext.
 *   --> Creates an isolated set of button event bindings to change button
 *        functionality for different Owl modes.
 */

#include "buttonContext.hpp"

void cButtonContext::addBinding(sButtonEvent event, buttonCallback_t callback)
{
  maBindings.emplace_back(sButtonBinding(std::vector<sButtonEvent>{ event },
				    callback ));
}

void cButtonContext::addBinding(std::vector<sButtonEvent> aEvents,
				buttonCallback_t callback )
{
  maBindings.emplace_back(sButtonBinding(aEvents, callback));
}

const std::vector<cButtonContext::sButtonBinding>& cButtonContext::getBindings() const
{
  return maBindings;
}

void cButtonContext::clearBindings()
{
  maBindings.clear();
}
