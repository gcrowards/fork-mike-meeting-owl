#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>

#ifdef __ANDROID__
#include "audioframework.h"
#endif

#include "analytics.hpp"
#include "blackBox.hpp"
#include "common.hpp"
#include "debugSwitches.h"
#include "ledInterface.hpp"
#include "logging.hpp"
#include "owlHook.hpp"    // *** TODO - ELIMINATE NEED FOR THIS INCLUDE
#include "owl.hpp"
#include "params.h"
#include "switchboard/jsonListReader.hpp"
#include "switchboard/jsonListWriter.hpp"
#include "switchboard/meetingInfo.hpp"
#include "switchboard/pinnedAoiArgs.hpp"
#include "switchboard/pinnedAoiReply.hpp"
#include "switchboard/setupProg.hpp"
#include "switchboard/uiAction.hpp"
#include "switchboard/videoParams.hpp"
#include "sysfs.hpp"


// *** TODO - ELIMINATE NEED FOR THIS REFERENCE
extern cOwlHook *gpOwlHook; /**< interface with core application. */

/** @param pMic is a pointer to the cVirtualMic instance from owlHook.
 *  @param pSysfs is a pointer to the cSysfs instance from owlHook.
 */
cOwl::cOwl(cVirtualMic *pMic, cSysfs *pSysfs)
  : mpVirtualMic(pMic),
    mpSysfs(pSysfs),
    mpSwitchboardServer(nullptr),
    mpSwitchboardClient(nullptr),
    mpDebugInfo(nullptr)
{
  // Start switchboard server and debug client
  mpSwitchboardServer = new cSwitchboardServer();
  mpSwitchboardClient = new cSwitchboardClient(
      std::string("127.0.0.1"),
      std::bind(&cOwl::switchboardClientErrorCallback, this,
                std::placeholders::_1,
                std::placeholders::_2,
                std::placeholders::_3));
  /* Set up logging to send messages to the switchboard server. The first
   * argument to owlLogConfigure() is a callback function to be invoked
   * by the logger with each messages to send to the server. The messages
   * will be sent with the switchboard client writeMessage() interface
   * of the specific client instance we just created, gpDebugSwitchboardClient.
   * The first argument to that callback for the logger is always going to be
   * the logging message type, MEETING_OWL_DEBUG_MSG, so that parameter is
   * defined as a constant in setting up the binding used to invoke the
   * callback. The remaining two arguments, the message and length, are passed
   * when the callback is invoked, so they are identified in the binding with
   * placeholders.
   * All messages will have the '(meetingOwl)' identifier pre-pended to identify
   * the source of the logs.
   */
  owlLogConfigureRemote(std::bind(&cSwitchboardClient::writeMessage,
                                  mpSwitchboardClient,
                                  eOipcMessageType::MEETING_OWL_DEBUG_MSG,
                                  std::placeholders::_1,
                                  std::placeholders::_2),
                        (char *)"(meetingOwl)");
  mpSwitchboardClient->subscribe(
      (eOipcMessageType)MEETING_OWL_DEBUG_CMD,
      std::bind(&cOwl::switchboardDebugCommandCallback, this,
                std::placeholders::_1,
                std::placeholders::_2,
                std::placeholders::_3));
  usleep(10000); // Make sure first subscription is processed.
  mpSwitchboardClient->subscribe(
      (eOipcMessageType)PINNED_AOI_CMD,
      std::bind(&cOwl::switchboardPinnedAoiMessageCallback, this,
                std::placeholders::_1,
                std::placeholders::_2,
                std::placeholders::_3));
  mpSwitchboardClient->subscribe(
      (eOipcMessageType)PANO_SHIFT_ANGLE_GET,
      std::bind(&cOwl::switchboardPanoShiftAngleGetCallback, this,
                std::placeholders::_1,
                std::placeholders::_2,
                std::placeholders::_3));
  mpSwitchboardClient->subscribe(
      (eOipcMessageType)PANO_SHIFT_ANGLE_SET,
      std::bind(&cOwl::switchboardPanoShiftAngleSetCallback, this,
                std::placeholders::_1,
                std::placeholders::_2,
                std::placeholders::_3));
  mpSwitchboardClient->subscribe(
      (eOipcMessageType)UI_ACTION_CMD,
      std::bind(&cOwl::switchboardUiActionCallback, this,
                std::placeholders::_1,
                std::placeholders::_2,
                std::placeholders::_3));
  mpSwitchboardClient->subscribe(
      (eOipcMessageType)MEETING_INFO_QUERY,
      std::bind(&cOwl::switchboardMeetingInfoQueryCallback, this,
                std::placeholders::_1,
                std::placeholders::_2,
                std::placeholders::_3));
  mpSwitchboardClient->subscribe(
      (eOipcMessageType)SETUP_PROGRESS_SET,
      std::bind(&cOwl::switchboardSetupProgressSetCallback, this,
                std::placeholders::_1,
                std::placeholders::_2,
                std::placeholders::_3));
  mpSwitchboardClient->subscribe(
      (eOipcMessageType)SETUP_PROGRESS_GET,
      std::bind(&cOwl::switchboardSetupProgressGetCallback, this,
                std::placeholders::_1,
                std::placeholders::_2,
                std::placeholders::_3));
  mpSwitchboardClient->subscribe(
      (eOipcMessageType)FACTORY_RESET_CMD,
      std::bind(&cOwl::switchboardFactoryResetCallback, this,
                std::placeholders::_1,
                std::placeholders::_2,
                std::placeholders::_3));
  mpSwitchboardClient->subscribe(
      (eOipcMessageType)AUDIO_PARAMETERS_CMD,
      std::bind(&cOwl::switchboardAudioParametersCallback, this,
                std::placeholders::_1,
                std::placeholders::_2,
                std::placeholders::_3));
  mpSwitchboardClient->subscribe(
      (eOipcMessageType)VIDEO_PARAMETERS_CMD,
      std::bind(&cOwl::switchboardVideoParameterCallback, this,
                std::placeholders::_1,
                std::placeholders::_2,
                std::placeholders::_3));
  mpSwitchboardClient->subscribe(
      (eOipcMessageType)DEBUG_INFO_CAPTURE_CMD,
      std::bind(&cOwl::switchboardDebugInfoCallback, this,
                std::placeholders::_1,
                std::placeholders::_2,
                std::placeholders::_3));

  // note start time because we want to wait a little before processing frames
  // (e.g. because camera needs time to stabalize its gain)
  currentTimeSpec(&mLaunchTime);
  mConnectTime = (time_t)0;
  mWarmedUp = false;
  mInCall = false;
  mProbablyInCall = false;
  mMuteMode = false;
  mBluetoothMode = false;
  
  // setup room mapping modules
  mpFaceMapper = new cFaceMapper(&mPanorama);
  mpMotionMapper = new cMotionMapper(&mPanorama);
  mpSoundMapper = new cSoundMapper(mpVirtualMic);
  mpPersonMapper = new cPersonMapper();

  // setup attention system
  mAttentionSystem.setPanorama(&mPanorama);
  mAttentionSystem.setFaceMapper(mpFaceMapper);
  mAttentionSystem.setMotionMapper(mpMotionMapper);
  mAttentionSystem.setPersonMapper(mpPersonMapper);

  // load black box
  mpBlackBox = new cBlackBox(ANALYTICS_DATA_DIRECTORY);

  // setup visual editor
  int panoShiftAng = 0;
  mpBlackBox->getInt(SETTINGS_ROOT_STR, SETTINGS_PANO_SHIFT_ANGLE, panoShiftAng);
  mVisualEditor.setPanorama(&mPanorama);
  mVisualEditor.createPanoramicSubframe(panoShiftAng);

  // setup analytics data capture
  mpAnalytics = new cAnalytics(mpBlackBox);
  mpAnalytics->init(ANALYTICS_DATA_DIRECTORY);

  // setup startup experience state machine
  mpStartupExperience = new cStartupExperience(mpAnalytics, mpBlackBox,
                                               &mLedContext);

  // setup volume control
  mpVolumeControl = new cVolumeControl(mpSysfs, mpBlackBox);

  // define cOwl's button context
  // mute and owl buttons call cOwl callbacks
  // volume buttons call cVolumeControl callbacks
  mButtonContext.addBinding(sButtonEvent(BUTTON_MUTE, eButtonClick),
			    std::bind(&cOwl::muteButtonCallback,
				      this ) );

  const double kOwlButtonHoldTime = 2.0; // seconds of hold time

  mButtonContext.addBinding(sButtonEvent(BUTTON_OWL, eButtonPress, 
                                         kOwlButtonHoldTime ),
                            std::bind(&cOwl::owlButtonCallback,
                                      this ) );

  // volume up button
  mButtonContext.addBinding(sButtonEvent(BUTTON_VOLUP, eButtonPress),
			    std::bind(&cVolumeControl::buttonPress,
				      mpVolumeControl, 1 ));
  mButtonContext.addBinding(sButtonEvent(BUTTON_VOLUP, eButtonPress, 
					 cVolumeControl::kHoldDelay ),
			    std::bind(&cVolumeControl::buttonHold,
				      mpVolumeControl ) );
  mButtonContext.addBinding(sButtonEvent(BUTTON_VOLUP, eButtonRelease),
			    std::bind(&cVolumeControl::buttonRelease,
				      mpVolumeControl, 1 ) );
  // volume down button
  mButtonContext.addBinding(sButtonEvent(BUTTON_VOLDN, eButtonPress),
			    std::bind(&cVolumeControl::buttonPress,
				      mpVolumeControl, -1 ) );
  mButtonContext.addBinding(sButtonEvent(BUTTON_VOLDN, eButtonPress, 
					 cVolumeControl::kHoldDelay ),
			    std::bind(&cVolumeControl::buttonHold,
				      mpVolumeControl ) );
  mButtonContext.addBinding(sButtonEvent(BUTTON_VOLDN, eButtonRelease),
			    std::bind(&cVolumeControl::buttonRelease,
				      mpVolumeControl, -1 ) );

  // debug visualization combo
  // press OWL-BUTTON three times while holding MUTE to toggle debug 
  // visualization
  static const std::vector<sButtonEvent> kDebugVisualizationCombo =
    { sButtonEvent(BUTTON_MUTE, eButtonHold),
      sButtonEvent(BUTTON_OWL, eButtonClick),
      sButtonEvent(BUTTON_OWL, eButtonClick),
      sButtonEvent(BUTTON_OWL, eButtonClick) };
  
  mButtonContext.addBinding(kDebugVisualizationCombo,
			    std::bind(&cOwl::debugVisualizationButtonCallback, 
				      this ) );
  
  if DEBUG(ENABLE_GRABBING_FISHEYE)
    {
      // debug grab fisheye combo
      // Press MUTE-BUTTON holding OWL-BUTTON to trigger capture of a fisheye
      static const std::vector<sButtonEvent> kDebugGrabFisheyeCombo =
        { sButtonEvent(BUTTON_OWL, eButtonHold),
          sButtonEvent(BUTTON_MUTE, eButtonClick) };

      mButtonContext.addBinding(kDebugGrabFisheyeCombo,
                                std::bind(&cOwl::debugGrabFisheyeButtonCallback,
                                          this ) );
    }

  // setup system health log capture
  if(DEBUG(SYSTEM_HEALTH_LOG)) {
    mpSystemHealth = new cSystemHealth();
  }

  // Set the static subframe animation mode from stored setting
  std::string modeStr;
  // get the setting from the black box we recently loaded
  mpBlackBox->getString(SETTINGS_ROOT_STR, SETTINGS_ANIMATION_MODE, modeStr);
  LOGI("Subframe animation mode: %s\n", modeStr.c_str());
  // set the mode from the loaded string
  eAnimationMode_t mode = eAnimationMode_t::fromString(modeStr.c_str());
  cSubframe::setAnimationMode(mode);

  // load mock map data, if requested
  if(DEBUG(MOCK_MAPS_PLAYBACK))
    {
      mMapsPlayer.setPersonDataFilename(PERSON_MAP_PLAYBACK_FILE);
      mMapsPlayer.setVoiceDataFilename(VOICE_MAP_PLAYBACK_FILE);
      mDebugManager.enable(LIVE_DEBUG_TOOLS);
    }
  
  // set up recorder, if requested
  if(DEBUG(MAPS_RECORD))
    {
      mMapsRecorder.setPersonDataFilename(PERSON_MAP_RECORD_FILE);
      mMapsRecorder.setVoiceDataFilename(VOICE_MAP_RECORD_FILE);
    }
}

cOwl::~cOwl()
{
  setAnalytics(false, false);  // Make sure meeting is closed, if open

  // delete dynamic objects
  delete mpFaceMapper;
  delete mpMotionMapper;
  delete mpSoundMapper;
  delete mpPersonMapper;
  delete mpStartupExperience;
  delete mpAnalytics;
  delete mpBlackBox;
  delete mpVolumeControl;

  if (mpDebugInfo != nullptr)
    { delete mpDebugInfo; }

  owlLogConfigureRemote(nullptr, (char *)"");
  delete mpSwitchboardClient;
  delete mpSwitchboardServer;
}

/** Refresh data, perform analysis, and generate results
 *
 * This function should be called at regular intervals.  In turn, it grabs
 * the latest data from the sensors and ticks the attention system and the
 * cVisualEditor (which together generate the output video).
 *
 * @param pSmPanoramic is a pointer to the panoramic image buffer.
 * @param pOverlays is a set of lists of overlay rendering control structs.
 * @param speakerActive indicates if the loudspeaker output is above a
 * threshold of activity.
 * @param speakerLevelLeft is level of activity on loudspeaker left channel.
 * @param speakerLevelRight is level of activity on loudspeaker right channel.
 */
std::vector<owl::cPanel> cOwl::update(unsigned char *pSmPanoramic,
                                      owl::cRgbaOverlayLists *pOverlays,
                                      audioChannelStatus_t mSpeakerStatus,
                                      audioChannelStatus_t mMicStatus,
                                      float fps )
{
  static int count = 0;               // loop counter, available for general use
  static timespec lastTalkTimeUpdate; // used for period update of talk stats
                                      // for analytics

  const double kWarmUpTime = 5.0;            // seconds
  const double kTalkTimeUpdatePeriod = 1.0; //seconds

  bool frameRateSlow = false; // used in updating Visual Editor
  
  std::vector<cAreaOfInterest *> pAois;
  std::vector<cVoice> voices;
  
  // update general-use loop counter
  count++;
  
  if(DEBUG(MOCK_MAPS_PLAYBACK))
    {
      if(count == 1)
	{ // load "simulation" data
	  mMapsPlayer.start();
	}
    }

  if(DEBUG(MAPS_RECORD))
    {
      if(count == 1)
	{ // start map data recording
	  mMapsRecorder.start();
	}
    }
  
  // don't process frames until system is fully up (e.g. camera needs time to
  // adjust gain)
  if(!mWarmedUp)
    {
      if(elapsedTimeSinceSec(mLaunchTime) < kWarmUpTime)
	{return std::vector<owl::cPanel>();}
      else
	{mWarmedUp = true;}
    }
  
  // update sensors; owlHook updates the cVirtualMic instance
  mPanorama.update(pSmPanoramic);

  // update sound map (motion and face maps are each updated on its own thread)
  mpSoundMapper->update();


  // update person mapper
  if(DEBUG(MOCK_MAPS_PLAYBACK))
    { // update person mapper with playback map data
      std::vector<cItem> mockPersonMap = mMapsPlayer.getPersonMap();
      mpPersonMapper->update(mockPersonMap);
    }
  else
    {
      // update person mapper with pointers to lower-level maps
      mpPersonMapper->update(mpFaceMapper, mpMotionMapper, mpSoundMapper);
    }

  // If there's motion detected everywhere reset the maps because this could
  // be the result of someone moving the owl (which could invalidate the map
  // data).
  // TODO: move this to a future class that manages the maps
  if(mpMotionMapper->checkForBroadMotion())
    {
      resetAllMaps();
    }
  
  // update attention system
  if(DEBUG(MOCK_MAPS_PLAYBACK))
    {
      voices = mMapsPlayer.getVoiceMap();
    }
  else
    {
      voices = mpVirtualMic->getVoices();
    }

  // save map data (for debug)
  if(DEBUG(MAPS_RECORD))
    {
      mMapsRecorder.recordState(mpPersonMapper->getInstantItemMap(), voices);
    }
  
  pAois = mAttentionSystem.update(voices, &mPinnedAoiArgs);
  
  // update the startup experience state machine with current cOwl status
  mpStartupExperience->setOverlays(pOverlays);
  mpStartupExperience->setAoiStatus(pAois.empty());

  // Create and send a pinned Aoi reply message, if needed, now that the
  // attention system has processed any pinned Aoi commands.
  if (!mPinnedAoiArgs.getArgsConsumed())
    {
      pinnedAoiReply(&pAois);
      mPinnedAoiArgs.setArgsConsumed();
    }

  // update analytics general stats
  if(fps > 0 && fps < REQUIRED_FPS)  // fps < 0 when not ready to report
    { frameRateSlow = true; }
  
  mpAnalytics->updatePeopleHistogram(mpPersonMapper->getInstantItemMap());
  mpAnalytics->updateSystemStatistics(
      fps,
      (float)mpSystemHealth->getTempAtLoc(cSystemHealth::CAMERA),
      (float)mpSystemHealth->getTempAtLoc(cSystemHealth::CPU_0) );

  // update analytics talk stats
  cAnalytics::eMeetingState meetingState= mpAnalytics->getMeetingState();

  if(meetingState == cAnalytics::MEETING_STARTING)
    { // reset DTD stats when new meeting starts (it's always accumulating)
      gpOwlHook->getDTD()->resetStats();
      currentTimeSpec(&lastTalkTimeUpdate);
    }
  else if(meetingState != cAnalytics::NOT_MEETING)
    {
      if(elapsedTimeSinceSec(lastTalkTimeUpdate) >= kTalkTimeUpdatePeriod)
	{
	  cAudioDTD *pDtd =gpOwlHook->getDTD();
	  int dtdEnabled = 0;

	  if (AMGetAecPostAlgo(dtdEnabled) != 0)
	    { dtdEnabled = 0; }
	  
	  mpAnalytics->updateTalkTimeStats(pDtd->getLocalTime(), 
					   pDtd->getRemoteTime(),
					   pDtd->getBothTime(),
					   pDtd->getSilenceTime(),
					   dtdEnabled * kTalkTimeUpdatePeriod);
	  /* Note that these values are only updated once every kTalk... seconds
	   * so the dtdEnabled report needs to be scaled up by the number
	   * of seconds between samples, thus the multiply above.
	   */
	  
	  pDtd->resetStats();
	  currentTimeSpec(&lastTalkTimeUpdate);
	}
    }
  
  // update output frame
  int panoShiftAng = 0;
  mpBlackBox->getInt(SETTINGS_ROOT_STR, SETTINGS_PANO_SHIFT_ANGLE, panoShiftAng);
  mVisualEditor.shiftPanoramicSubframe(panoShiftAng);
  mVisualEditor.update(pAois, pOverlays,
                       mpFaceMapper, mpMotionMapper, mpSoundMapper,
		               mpPersonMapper, voices,
		               mSpeakerStatus, mMicStatus,
		               frameRateSlow );

  // log system health (if enabled)
  if(DEBUG(SYSTEM_HEALTH_LOG)) {
    if(mInCall)
      {
	mpSystemHealth->update(fps);
      }
  }
  
  return mVisualEditor.getCompositePanels();
}

/** Create and send a pinned AOI reply message.
 *
 * The pinned Aoi reply message is a list of the state of each pinned Aoi
 * in the system.
 *
 * @param pAois is a pointer to the list of all the active Aois in the system
 * including any pinned Aois about which to reply.
 */
void cOwl::pinnedAoiReply(std::vector<cAreaOfInterest *> *pAois)
{
  // After receiving a command, always generate the reply containing a list
  // of affected Aois
  cJsonListWriter msgList;
  int bearing;
  int width;
  int id;
  ePinnedAoiCmd_t cmd = ePinnedAoiCmd_t::NONE;
  bool returnAllAois = false;
  cPinnedAoiArgs *pArgs = &mPinnedAoiArgs;

  // Get the command contents to see if we should return ALL Aois
  pArgs->getArgs(cmd, id, width, bearing);

  if (cmd == ePinnedAoiCmd_t::GET_ALL)
    {
          // set flag so when reply is created, it includes ALL aois
          returnAllAois = true;
    }

  if (cmd == ePinnedAoiCmd_t::DELETE)
    { mpAnalytics->setPinnedAoiEndTime(id); }

  if (cmd == ePinnedAoiCmd_t::DELETE_ALL)
    { mpAnalytics->setPinnedAoiEndTime(-1); }

  // Iterate through Aois adding to reply message as necessary
  for (std::vector<cAreaOfInterest *>::iterator iAoi = pAois->begin();
      iAoi != pAois->end();
      iAoi++)
    {
      int minWidth = (*iAoi)->getSubframe()->getMinReqSize().width;
      int width = (*iAoi)->getSubframe()->getSize().width;
      bool enabled = (*iAoi)->isEnabled();
      bool inLayout = (*iAoi)->getSubframe()->isInLayout();
      char extraMsg[256];

      if (((*iAoi)->isPinned()) || (returnAllAois))
        {
          owl::cPatch *patch = (*iAoi)->getPatch();
          ePinnedAoiCmd_t aoiCmd = ePinnedAoiCmd_t::NONE;

          if ((*iAoi)->getId() == id)
            {
              width = patch->dTheta;

              if (cmd == ePinnedAoiCmd_t::PIN)
                { mpAnalytics->setPinnedAoiStartTime(id, width); }

              aoiCmd = cmd;
            }

          snprintf(extraMsg, sizeof(extraMsg),
                   "%s, minReqSize:%d, size:%d, isEnabled:%s, isInLayout:%s",
                   (((*iAoi)->isPinned()) ? "Pinned" : "NOT pinned"),
                   minWidth, width, (enabled ? "t" : "f"),
                   (inLayout ? "t" : "f"));

          cPinnedAoiReply aoiReplyMsg(aoiCmd,
                                      (*iAoi)->getId(),
                                      patch->dTheta,
                                      patch->theta,
                                      0, extraMsg);
          char replyBytes[512];
          aoiReplyMsg.buildMsg(replyBytes, sizeof(replyBytes));
          msgList.addObject(std::string(replyBytes));
        }
    }

  // Always send a reply; it will be "[]" if all AOIs have been deleted
  std::string msg;

  msgList.getList(msg);
  mpSwitchboardClient->writeMessage(
      eOipcMessageType::PINNED_AOI_REPLY,
      (char *)msg.c_str(),
      msg.length() + 1 /*terminate string*/);
}

/** Clears all maps to rediscover meeting attendees from a blank slate.
 *
 */
void cOwl::resetAllMaps(void)
{
  mpFaceMapper->resetMap();
  mpMotionMapper->resetMap();
  mpSoundMapper->resetMap();
  mpPersonMapper->resetMap();
  gpOwlHook->resetSoundLocalizer();
}

/** Notifies of changes in the in-meeting state for analytics collection.
 *
 * When the analytics module transitions to a meeting, this routine is called
 * with 'true', and when the meeting ends, it is called with 'false'. These
 * transitions are used to trigger storage of meeting-specific analytics
 * data.
 *
 * @param inMeeting indicates if system has transitioned fully into a meeting.
 * @param probablyInMeeting indicates if system is starting or in a meeting.
 */
void cOwl::setAnalytics(bool inMeeting, bool probablyInMeeting)
{
  bool stateChanged = (probablyInMeeting != mProbablyInCall);

  mProbablyInCall = probablyInMeeting;
  
  if (inMeeting)
    {
      LOGI("cOwl::setAnalytics: Meeting start.\n");
      mInCall = true;
      
      mpAnalytics->setStartTime();

      if(DEBUG(SYSTEM_HEALTH_LOG)) {
	mpSystemHealth->start(SYSTEM_HEALTH_DATA_DIRECTORY);
      }

      mpFaceMapper->setStreaming(true);

      // Reset accumulators for meeting based system statistics
      mpAnalytics->updateSystemStatistics(-1.0f, -1.0f, -1.0f);
    }
  else if (mInCall)
    {
      bool saveMeetingRecord;

      mInCall = false;
      
      saveMeetingRecord = mpAnalytics->setEndTime();
      mpAnalytics->save();

      LOGI("cOwl::setAnalytics: Meeting stop (%s).\n",
           (saveMeetingRecord ? "Saved" : "Short, not saved"));

      if(DEBUG(SYSTEM_HEALTH_LOG)) {
        mpSystemHealth->close();
      }

      mpFaceMapper->setStreaming(false);

      // clear the stage and maps in prep for next meeting
      resetAllMaps();
      mVisualEditor.clearLayout();
      mAttentionSystem.clearAois();

      setMute(false);
    }

  if(stateChanged)
    { // notify app that the meeting state has changed.
      static char meetingStateMsg[1024];
      cMeetingInfo info(mProbablyInCall);
      bool success = info.buildMsg(meetingStateMsg, sizeof(meetingStateMsg));
      if(success)
	{
	  cJsonListWriter msgList;
	  success = msgList.addObject(std::string(meetingStateMsg));
	  std::string msg;
	  msgList.getList(msg);
	  strncpy(meetingStateMsg, msg.c_str(), sizeof(meetingStateMsg));
	  mpSwitchboardClient->writeMessage(MEETING_INFO_NOTIFY,
					    meetingStateMsg,
					    strlen(meetingStateMsg) + 1 );
	}
    }
  return;
}

/** @return whether the owl is currently muted. Note: this returns false
 * in the case where the Owl is actually muted, but the startup sequence
 * has not yet completed. This is important for mute display icon
 * management. */
bool cOwl::isMuted() const
{
  return mMuteMode &&
      (mpStartupExperience->getStartupState() >=
                      cStartupExperience::STARTUP_COMPLETE);
}

/** @return the owl's LED context. */
cLedContext* cOwl::getLedContext()
{
  return &mLedContext;
}

/** @return the owl's button calback context. */
const cButtonContext* cOwl::getButtonContext() const
{
  return &mButtonContext;
}

/** Mute / un-mute the Owl.
 *
 * @param mute is applied if true.
 */
void cOwl::setMute(bool mute)
{
  // set mic mute
  // video is also muted (see cOwl::update())
  mMuteMode = mute;

#ifdef __ANDROID__
  // mute the Owl's microphone
  SetMicMute(mMuteMode ? 1 : 0);
#endif
  
  // toggle the mute LEDs
  mLedContext.setLeds(LEDS_MUTE, mMuteMode);
  
  LOGI("Owl mute %s.\n", (mMuteMode ? "enabled" : "disabled"));
}

/** Callback bound to a mute button click. */
void cOwl::muteButtonCallback()
{
  // set mic mute
  // video is also muted (see cOwl::update())
  setMute(!mMuteMode);
}

/** Callback bound to owl button press, after 2-second delay. */
void cOwl::owlButtonCallback()
{
  // track time spent in animation mode we're about to leave
  mpAnalytics->updateAnimationModeDuration();

  // switch to next animation mode
  mVisualEditor.nextAnimationMode();

   // play sound to give some feedback 
  mAudioPlayer.playWavFile((char *)BLIP_WAV, NULL);

  // Transfer global subframe animation mode to black box and save
  eAnimationMode_t mode = cSubframe::getAnimationMode();
  std::string modeStr = mode.toString();
  mpBlackBox->setString(SETTINGS_ROOT_STR, SETTINGS_ANIMATION_MODE, modeStr);
  mpBlackBox->save();
  LOGI("Subframe animation mode: %s\n", modeStr.c_str());
}

/** Callback bound to mute button hold + 3 owl-button presses to toggle
 * debug visualization on/off. */
void cOwl::debugVisualizationButtonCallback(void)
{
  if(mDebugManager.check((liveDebugTools_e)LIVE_DEBUG_TOOLS))
    { // debug visualizations off
      mDebugManager.disable(LIVE_DEBUG_TOOLS);
    }
  else
    { // debug visualizations on
      mDebugManager.enable(LIVE_DEBUG_TOOLS);
    }
}

/** Callback bound to Owl button hold + mute-button press to grab fisheye.
 *
 * Called in response the the button combination, this function just sets
 * the 'grab fisheye' flag for the thread running the frame callback to check.
 */
void cOwl::debugGrabFisheyeButtonCallback(void)
{
  mGrabNextFisheye = true;
}

/** Accessor to check the state of the 'grab fisheye' flag.
 *
 * This function has the side effect of clearing the 'grab fisheye'
 * flag each time it is called.
 *
 * @return true if the 'grab fisheye' flag has been set.
 */
bool cOwl::grabFisheye(void)
{
  bool grab = mGrabNextFisheye;

  mGrabNextFisheye = false;

  return grab;
}

/** Getter for analytics object. */
cAnalytics *cOwl::getAnalyticsObject(void)
{
  return mpAnalytics;
}

/** Callback invoked only if client errors are reported from switchboard.
 *
 * @param type is the type of the message received.
 * @param pMsg is a pointer to char array containing the received message.
 * @param msgLen is the number of bytes in the message.
 */
void cOwl::switchboardClientErrorCallback(eOipcMessageType type,
                                    const char *pMsg,
                                    int msgLen)
{
  // If the type is CLIENT_VERSION_ERROR, then there's a major version
  // mismatch and no communications are allowed.
  LOGI("Error Message: Type: %d, Msg: %s\n", type, pMsg);
}

/** Callback invoked when a MEETING_OWL_DEBUG_CMD message is received.
 *
 * @param type is the type of the message received.
 * @param pMsg is a pointer to char array containing the received message.
 * @param msgLen is the number of bytes in the message.
 */
void cOwl::switchboardDebugCommandCallback(eOipcMessageType type,
                                           const char *pMsg,
                                           int msgLen)
{
  LOGI("switchboardDebugCommandCallback: Type: %d, Msg: %s\n", type, pMsg);
}

/** Callback invoked when a PINNED_AOI_CMD message is received.
 *
 * @param type is the type of the message received.
 * @param pMsg is a pointer to char array containing the received message.
 * @param msgLen is the number of bytes in the message.
 */
void cOwl::switchboardPinnedAoiMessageCallback(eOipcMessageType type,
                                               const char *pMsg,
                                               int msgLen)
{
  cJsonListReader listMsg;
  std::string key = PINNED_AOI_ARGS_STR;
  std::string cmd;
  // Next 2 constants are defined so if a message is not consumed in 1/10 of
  // a second, then it should be discarded because no frames are arriving.
  const int kRetryIterations = 10;
  const int kIterationDelay = 10000;  // 10 milliseconds

  listMsg.setList(pMsg);
  while (listMsg.getNextObject(key, cmd))
    {
      mPinnedAoiArgs.parseMsg(cmd.c_str());
      /* At this point the new frame callback will pass these arguments down
       * to cOwl through cOwlHook. That will consume the arguments. However, if
       * no frames are being sent from libOwlPG because we are not in a meeting
       * then the for loop below will reach termination, and abort the
       * message. (No use in trying to set up a pinned AOI if there's nothing
       * currently being shown via a meeting.)
       */
      for (int iter = 0; iter < kRetryIterations; iter++)
        {
          if (mPinnedAoiArgs.getArgsConsumed())
            {
              // Wait for attention system to consume message
              break;
            }
          else
            {  // Wait for message to be consumed before getting next
              usleep(kIterationDelay);
            }
        }
      mPinnedAoiArgs.abortArgs(); // In case not consumed and timed out.
    }
}

/** Callback invoked when a PANO_SHIFT_ANGLE_GET message is received.
 *
 * @param type is the type of the message received.
 * @param pMsg is a pointer to char array containing the received message.
 * @param msgLen is the number of bytes in the message.
 */
void cOwl::switchboardPanoShiftAngleGetCallback(eOipcMessageType type,
                                                const char *pMsg,
                                                int msgLen )
{
  static char replyMsg[512];

  if(mpSwitchboardClient == NULL) { return; }

  int panoShiftAng = 0;
  if(mpBlackBox != NULL)
  { mpBlackBox->getInt(SETTINGS_ROOT_STR, SETTINGS_PANO_SHIFT_ANGLE, panoShiftAng); }

  sprintf(replyMsg, "{\"PanoShiftAngleGetReply\": %d}", panoShiftAng);

  mpSwitchboardClient->writeMessage(PANO_SHIFT_ANGLE_GET_REPLY, replyMsg, strlen(replyMsg)+1);
}

/** Callback invoked when a PANO_SHIFT_ANGLE_SET message is received.
 *
 * @param type is the type of the message received.
 * @param pMsg is a pointer to char array containing the received message.
 * @param msgLen is the number of bytes in the message.
 */
void cOwl::switchboardPanoShiftAngleSetCallback(eOipcMessageType type,
                                                const char *pMsg,
                                                int msgLen )
{
  if(mpBlackBox == NULL) { return; }

  int panoShiftAng = 0;
  if(sscanf(pMsg, "{ \"PanoShiftAngleSet\" : %d }", &panoShiftAng) == 1)
  { mpBlackBox->setInt(SETTINGS_ROOT_STR, SETTINGS_PANO_SHIFT_ANGLE, panoShiftAng); }
}

/** Callback invoked when a MEETING_OWL_ACTION_CMD message is received.
 *
 * @param type is the type of the message received.
 * @param pMsg is a pointer to char array containing the received message.
 * @param msgLen is the number of bytes in the message.
 */
void cOwl::switchboardUiActionCallback(eOipcMessageType type,
				       const char *pMsg,
				       int msgLen )
{

  cJsonListReader listMsg;
  std::string key = UI_ACTION_ARGS_STR;
  std::string cmd;
  // Next 2 constants are defined so if a message is not consumed in 1/10 of
  // a second, then it should be discarded because no frames are arriving.
  const int kRetryIterations = 10;
  const int kIterationDelay = 10000;  // 10 milliseconds

  cUiAction uiAction;
  
  listMsg.setList(pMsg);
  while(listMsg.getNextObject(key, cmd))
    {
      uiAction.parseMsg(cmd.c_str());
      for(int iter = 0; iter < kRetryIterations; iter++)
        {
          if(uiAction.argsConsumed())
            { break; }
          else
	    {
              uiCommand_t cmdType = uiCommand_t::NONE;
	      bool consumed = uiAction.getArgs(cmdType);
	      LOGD("UI MSG: cmd='%s'\n", cmdType.toString());

	      switch(cmdType)
		{
		case uiCommand_t::PING_START_SLOW:
		  LOGD("Starting slow ping LED response...\n");
		  mLedContext.startBlinking(LEDS_EYE,
					    cUiAction::mkSlowPingBlinkOnTimeMs,
					    cUiAction::mkSlowPingBlinkOffTimeMs );
		  break;
		case uiCommand_t::PING_START_FAST:
		  LOGD("Starting fast ping LED response...\n");
		  mLedContext.startBlinking(LEDS_EYE,
					    cUiAction::mkFastPingBlinkOnTimeMs,
					    cUiAction::mkFastPingBlinkOffTimeMs );
		  break;
		case uiCommand_t::PING_STOP:
		  LOGD("Stopping ping LED response...\n");
		  mLedContext.stopBlinking();
		  break;
		default:
		  LOGD("UI message: Unknown command type (%d)\n", cmdType);
		  break;
		}
            }
        }
      uiAction.abortArgs(); // In case not consumed and timed out.
    }
}

/** Callback function invoked when a MEETING_INFO_QUERY message arrives.
 *
 * @param type is the type of the message received.
 * @param pMsg is a pointer to char array containing message.
 * @param msgLen is the number of bytes in the message.
 */
void cOwl::switchboardMeetingInfoQueryCallback(eOipcMessageType type,
					       const char *pMsg,
					       int msgLen )
{
  // no message -- a message of this type means the app wants a meeting info packet.
  LOGD("MeetingInfoQuery message: '%s', len: %d\n", pMsg, msgLen);

  // notify app of the meeting state
  static char meetingStateMsg[512];
  cMeetingInfo info(mProbablyInCall);
  bool success = info.buildMsg(meetingStateMsg, sizeof(meetingStateMsg));
  if(success)
    {
      cJsonListWriter msgList;
      success = msgList.addObject(std::string(meetingStateMsg));
      std::string msg;
      msgList.getList(msg);
      strncpy(meetingStateMsg, msg.c_str(), sizeof(meetingStateMsg));
      mpSwitchboardClient->writeMessage(MEETING_INFO_NOTIFY,
					meetingStateMsg,
					strlen(meetingStateMsg) + 1 );
    }
}

/** Callback function invoked when an APP_CONFIGURED message arrives.
 *
 * @param type is the type of the message received.
 * @param pMsg is a pointer to char array containing message.
 * @param msgLen is the number of bytes in the message.
 */
void cOwl::switchboardSetupProgressSetCallback(eOipcMessageType type,
                                            const char *pMsg,
                                            int msgLen)
{
  cJsonListReader listMsg;
  std::string key = SETUP_PROGRESS_SET_STR;
  std::string cmd;
  // Next 2 constants are defined so if a message is not consumed in 1/10 of
  // a second, then it should be discarded because no frames are arriving.
  const int kRetryIterations = 10;
  const int kIterationDelay = 10000;  // 10 milliseconds

  cSetupProgressMsgs setupProg(SETUP_PROGRESS_SET_STR);

  listMsg.setList(pMsg);
  while(listMsg.getNextObject(key, cmd))
    {
      setupProg.parseMsg(cmd.c_str());
      int state = (int)setupProg.getState();

      LOGD("SetupProgressSet message: '%s', len: %d\n", pMsg, msgLen);

      mpBlackBox->setInt(SETTINGS_ROOT_STR,
                            SETTINGS_APP_SETUP_PROGRESS,
                            state);
      sendSetupProgressMsg();  // reply by sending back new state
    }
  mpBlackBox->save();
}

/** Callback function invoked when an SETUP_PROGRESS_GET message arrives.
 *
 * @param type is the type of the message received.
 * @param pMsg is a pointer to char array containing an empty message.
 * @param msgLen is the number of bytes in the message (should be 0).
 */
void cOwl::switchboardSetupProgressGetCallback(eOipcMessageType type,
                                               const char *pMsg,
                                               int msgLen )
{
  // no message -- a message of this type means the app wants a meeting info packet.
  LOGD("SetupProgressGet message: '%s', len: %d\n", pMsg, msgLen);

  sendSetupProgressMsg();
}

void cOwl::sendSetupProgressMsg(void)
{
  // notify app of the meeting state
  static char setupProgressMsg[512];
  int state;

  mpBlackBox->getInt(SETTINGS_ROOT_STR,
                     SETTINGS_APP_SETUP_PROGRESS,
                     state);
  cSetupProgressMsgs prog(SETUP_PROGRESS_GET_REPLY_STR, eSetupProgress_t(state));
  bool success = prog.buildMsg(setupProgressMsg, sizeof(setupProgressMsg));
  if(success)
    {
      cJsonListWriter msgList;
      success = msgList.addObject(std::string(setupProgressMsg));
      std::string msg;
      msgList.getList(msg);
      strncpy(setupProgressMsg, msg.c_str(), sizeof(setupProgressMsg));
      mpSwitchboardClient->writeMessage(SETUP_PROGRESS_GET_REPLY,
                                        setupProgressMsg,
                                        strlen(setupProgressMsg) + 1 );
    }
}

/** Callback function invoked when a FACTORY_RESET message arrives.
 *
 * @param type is the type of the message received.
 * @param pMsg is a pointer to char array containing an empty message.
 * @param msgLen is the number of bytes in the message (should be 0).
 */
void cOwl::switchboardFactoryResetCallback(eOipcMessageType type,
                                           const char *pMsg,
                                           int msgLen)
{
  eSetupProgress_t none(eSetupProgress_t::NONE);
  int iNone = (int)none;

  LOGD("FactoryReset message: '%s', len: %d\n", pMsg, msgLen);

  // reset to default values
  mpBlackBox->setInt(SETTINGS_ROOT_STR, SETTINGS_APP_SETUP_PROGRESS, iNone);

  // noiseSuppressionLevel = 0
  AMSetNRAmount(0);

  // enableDTD = true
  AMSetAecPostAlgo(1);

  // panoShiftAngle = 0
  mpBlackBox->setInt(SETTINGS_ROOT_STR, SETTINGS_PANO_SHIFT_ANGLE, 0);

  mpBlackBox->save();

  sendSetupProgressMsg();
}

/** Callback invoked when a AUDIO_PARAMETERS_CMD message is received.
 *
 * @param type is the type of the message received.
 * @param pMsg is a pointer to char array containing the received message.
 * @param msgLen is the number of bytes in the message.
 */
void cOwl::switchboardAudioParametersCallback(eOipcMessageType type,
                                              const char *pMsg,
                                              int msgLen )
{
  cJsonListReader listMsg;
  std::string key = AUDIO_PARAMETERS_STR;
  std::string cmd;
  // Init audioParams to current state so any params not sent in message
  // stay at the current state.
  cAudioParameters audioParams;
  bool sendReply = true;

  listMsg.setList(pMsg);
  while(listMsg.getNextObject(key, cmd))
    {
      audioParams.parseMsg(cmd.c_str());

      eAudioParametersCmd_t cmdType = audioParams.getCommand();
      LOGD("audioParams MSG: \n'%s'\n", pMsg);

      switch(cmdType)
      {
        case eAudioParametersCmd_t::SET:
          {
            int minSpeakerLevel;
            int noiseSuppressionLevel;
            int volume;
            bool mute;
            bool enableDTD;

            if (audioParams.getMinSpeakerLevel(minSpeakerLevel))
              {
                AMSetMinSpeakerLevel(minSpeakerLevel);
                LOGI("MinSpeakerLevel = %d\n", minSpeakerLevel);
              }
            if (audioParams.getVolume(volume))
              { mpVolumeControl->setVolume(volume); }
            if (audioParams.getMute(mute))
              { setMute(mute); }
            if (audioParams.getNoiseSuppressionLevel(noiseSuppressionLevel))
              {
                // TODO: Set state of noise suppression here!
                AMSetNRAmount(noiseSuppressionLevel);
                LOGI("NoiseSuppressionLevel is %d\n", noiseSuppressionLevel);
              }
            if (audioParams.getEnableDTD(enableDTD))
              {
                AMSetAecPostAlgo((int)enableDTD);
              }
            LOGD("Set audio parameters\n");
          }
          break;
        case eAudioParametersCmd_t::GET:
          LOGD("Get audio parameters\n");
          break;
        default:
          sendReply = false;
          LOGD("audioParameters message: Unknown command type (%d)\n", cmdType);
          break;
      }
    }

  if (sendReply)
    {
      usleep(60000);  // Wait to allow parameters to be instantiated
      sendAudioParametersMsg();
    }
}

/** Send an audioParameters reply message to the switchboard.
 *
 * Note that if an error occurs in trying to retrieve the minimum
 * speaker level, then a very small level is returned (-10000 dB).
 */
void cOwl::sendAudioParametersMsg(void)
{
  // notify app of the meeting state
  static char audioParamsMsg[512];
  int state;
  int minSpeakerLevel;
  int volume;
  bool mute;
  int noiseSuppressionLevel = 0;
  int enableDTD;

  if (AMGetMinSpeakerLevel(minSpeakerLevel) != 0)
    { minSpeakerLevel = -10000; }
  mute = mMuteMode;
  volume = mpVolumeControl->getVolume();
  if (AMGetNRAmount(noiseSuppressionLevel) != 0)
    { noiseSuppressionLevel = -10000; }
  if (AMGetAecPostAlgo(enableDTD) != 0)
    { enableDTD = false; }

  cAudioParameters params(eAudioParametersCmd_t::GET, volume, mute,
                          minSpeakerLevel, noiseSuppressionLevel, enableDTD);
  bool success = params.buildMsg(audioParamsMsg, sizeof(audioParamsMsg));
  if(success)
    {
      cJsonListWriter msgList;
      success = msgList.addObject(std::string(audioParamsMsg));
      std::string msg;
      msgList.getList(msg);
      strncpy(audioParamsMsg, msg.c_str(), sizeof(audioParamsMsg));
      mpSwitchboardClient->writeMessage(AUDIO_PARAMETERS_REPLY,
                                        audioParamsMsg,
                                        strlen(audioParamsMsg) + 1 );
    }
}

/** Callback invoked when a AUDIO_PARAMETERS_CMD message is received.
 *
 * @param type is the type of the message received.
 * @param pMsg is a pointer to char array containing the received message.
 * @param msgLen is the number of bytes in the message.
 */
void cOwl::switchboardVideoParameterCallback(eOipcMessageType type,
                                              const char *pMsg,
                                              int msgLen )
{
  cJsonListReader listMsg;
  std::string msgType = VIDEO_PARAMETERS_STR;
  std::string cmd;
  cVideoParameters videoParams;
  bool sendReply = true;

  listMsg.setList(pMsg);
  while(listMsg.getNextObject(msgType, cmd))
    {
      videoParams.parseMsg(cmd.c_str());

      eVideoParametersCmd_t cmdType = videoParams.getCommand();
      LOGD("videoParams MSG: cmd='%s'\n", cmdType.toString());

#ifdef __ANDROID__
      switch(cmdType)
      {
        case eVideoParametersCmd_t::SET:
          {
            cameraParams_t param = {videoParams.getKey().c_str(),
                                    videoParams.getValue().c_str()};
            setCameraParameters(&param, 1);
            LOGD("Set video parameters\n");
          }
          break;
        case eVideoParametersCmd_t::GET:
          LOGD("Get video parameters\n");
          break;
        default:
          sendReply = false;
          LOGD("videoParameters message: Unknown command type (%d)\n", cmdType);
          break;
      }
#else
      LOGI("Camera parameters not supported under Linux.\n");
#endif
    }

  if (sendReply)
    {
      usleep(60000);  // Wait to allow parameters to be instantiated
      sendVideoParameterMsg(videoParams.getKey().c_str());
    }
}

/** Send a videoParameters reply message to the switchboard.
 *
 * Note that if an error occurs in trying to retrieve the requested key,
 * then an empty string is sent in the reply.
 *
 * @param pKey is a pointer to the key string for which the key - value
 *     pair should be sent to the switchboard.
 */
void cOwl::sendVideoParameterMsg(const char *pKey)
{
  // send video parameter reply message
  static char videoParamsMsg[512];
  char *pValue;
  char empty[] = "";
  int state;

#ifdef __ANDROID__
  if ((pValue = getCameraParameter(pKey)) == NULL)
    { pValue = empty; }
#else
  pValue = empty;
#endif

  cVideoParameters param(eVideoParametersCmd_t::GET, pKey, pValue);
  bool success = param.buildMsg(videoParamsMsg, sizeof(videoParamsMsg));
  if(success)
    {
      cJsonListWriter msgList;
      success = msgList.addObject(std::string(videoParamsMsg));
      std::string msg;
      msgList.getList(msg);
      strncpy(videoParamsMsg, msg.c_str(), sizeof(videoParamsMsg));
      mpSwitchboardClient->writeMessage(VIDEO_PARAMETERS_REPLY,
                                        videoParamsMsg,
                                        strlen(videoParamsMsg) + 1 );
    }
}

/** Callback invoked when a request to send logcat output is received
 *
 * When a logcat request is received, meetingOwl must:
 * 1) Extract parameters from the command message
 * 2) Execute the logcat command storing output in a file
 * 3) Start a thread running that will write chunks to the switchboard
 */
void cOwl::switchboardDebugInfoCallback(eOipcMessageType type,
                                           const char *pMsg,
                                           int msgLen)
{
  char pMsgCpy[msgLen + 1];

  memcpy(pMsgCpy, pMsg, msgLen + 1);
  if (mpDebugInfo != nullptr)
    {
      if (mpDebugInfo->done())
        {
          delete mpDebugInfo;
          mpDebugInfo = nullptr;
        }
    }
  if (mpDebugInfo == nullptr)
    {
      /* NOTE: The parameters here are deliberately cryptic: the fist char
       * selects the debug function to run, and then additional parameters
       * to supply to the command are provided starting at pMsg[2]. This is
       * done this way as a means of helping to provide security by making
       * the command less obvious (as it would be if it were encoded in
       * JSON). We'd rather not have people look through all our debug
       * output as they might learn something we don't want them to.
       */
      if (msgLen < 3)
        { pMsgCpy[2] = 0; }
      if (msgLen < 1)
        { pMsgCpy[0] = 0; }
      LOGI("switchboardDebugInfoCallback(): pMsg[%d]:\n %s\n  %s\n",
           msgLen, &pMsgCpy[0], &pMsgCpy[2]);
      mpDebugInfo = new cDebugInfo(mpSwitchboardClient,
                                   &pMsgCpy[0], &pMsgCpy[2]);
    }
  else
    {
      LOGW("switchboardDebugInfoCallback(): already busy!\n");
    }
}


