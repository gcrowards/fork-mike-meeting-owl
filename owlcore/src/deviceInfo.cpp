/** Copyright 2017 Owl Labs Inc.
 *  All rights reserved.
 *
 *  deviceInfo.cpp
 *  This is the implementation file for cDeviceInfo.
 *   --> Static class that provides global access to information about the
 *        physical owl running this software, including serial numbers and MAC
 *        addresses.
 */

#include "deviceInfo.hpp"
#include "logging.hpp"
#include "shellProcess.hpp"
#include <algorithm>
#include <sstream>

// retrieve device info during static member intialization.
const std::string cDeviceInfo::mkHwSerialStr = retrieveHwSerial();
const std::string cDeviceInfo::mkSwSerialStr = retrieveSwSerial();
const std::string cDeviceInfo::mkPcbaSerialStr = retrievePcbaSerial();
const std::string cDeviceInfo::mkRadioVersionStr = retrieveRadioVersion();
const std::string cDeviceInfo::mkWifiMacAddrStr = retrieveWifiMacAddr();
const std::string cDeviceInfo::mkBtMacAddrStr = retrieveBtMacAddr();

/** @return a string containing the device's hardware serial number. */
std::string cDeviceInfo::getHardwareSerial()
{
  return mkHwSerialStr;
}
/** @return a string containing the device's software serial number (adb devices). */
std::string cDeviceInfo::getSoftwareSerial()
{
  return mkSwSerialStr;
}

/** @return a string containing the device's PCBA serial number (main board). */
std::string cDeviceInfo::getPcbaSerial()
{
  return mkPcbaSerialStr;
}
/** @return a string containing the version of the WCNSS_qcom_wlan_nv.bin file,
 *   which configures the radio. */
std::string cDeviceInfo::getRadioVersion()
{
  return mkRadioVersionStr;
}
/** @return a string containing the device's wifi MAC address. */
std::string cDeviceInfo::getWifiMacAddress()
{
  return mkWifiMacAddrStr;
}
/** @return a string containing the device's bluetooth MAC address. */
std::string cDeviceInfo::getBluetoothMacAddress()
{
  return mkBtMacAddrStr;

}

/** Retrieves the device's hardware serial number.
 *  @return a string containing the device's hardware serial number.
 */
std::string cDeviceInfo::retrieveHwSerial()
{
  std::string message = "";

#ifdef __ANDROID__
  // create a new process to print the hardware serial, stored in /persist/dsn
  //  at the factory.
  cShellProcess findHwSerial("cat /persist/dsn",
  [&](const std::string *pLine)
  {
    if(pLine)
      {
        // Note: this overwrites with each new match to return the LAST one!
        message = *pLine;
        return true;
      }
    else
      { return false; }
  },
  true );
#endif
  
  if(!message.empty())
    {
      // remove any '\n' characters
      message.erase(std::remove(message.begin(), message.end(), '\n'),
		    message.end() );
      return message;
    }
  else
    { return "UNKOWN"; }
}

/** Retrieves the device's software serial number.
 *  @return a string containing the device's software serial number.
 */
std::string cDeviceInfo::retrieveSwSerial()
{
  std::string message = "";

#ifdef __ANDROID__
  // create a new process to query the android software serial.
  cShellProcess findSwSerial("getprop ro.serialno",
  [&](const std::string *pLine)
  {
    if(pLine)
      {
        // Note: this overwrites with each new match to return the LAST one!
        message = *pLine;
        return true;
      }
    else
      { return false; }
  },
  true );
#endif
  
  if(!message.empty())
    {
      // remove any '\n' characters
      message.erase(std::remove(message.begin(), message.end(), '\n'),
		    message.end() );
      return message;
    }
  else
    { return "UNKOWN"; }
}

/** Retrieves the device's PCBA serial number.
 *  @return a string containing the device's PCBA serial number.
 */
std::string cDeviceInfo::retrievePcbaSerial()
{
  std::string message = "";

#ifdef __ANDROID__
  // create a new process to print the PCBA serial number, stored in
  //  /persist/pcbsn at the factory.
  cShellProcess findPcbaSerial("cat /persist/pcbsn",
  [&](const std::string *pLine)
  {
    if(pLine)
      {
        // Note: this overwrites with each new match to return the LAST one!
        message = *pLine;
        return true;
      }
    else
      { return false; }
  },
  true );
#endif
  
  if(!message.empty())
    {
      // remove any '\n' characters
      message.erase(std::remove(message.begin(), message.end(), '\n'),
		    message.end() );
      return message;
    }
  else
    { return "UNKOWN"; }
}

/** Retrieves the version of the WCNSS_qcom_wlan_nv.bin file on the device.
 *  @return a string containing the version.
 */
std::string cDeviceInfo::retrieveRadioVersion()
{
  std::string message = "";

#ifdef __ANDROID__
  // create a new process to run the nv-version.sh script, which outputs the
  //  version number
  cShellProcess findRadioVersion("nv-version.sh",
  [&](const std::string *pLine)
  {
    if(pLine)
      {
        // Note: this overwrites with each new match to return the LAST one!
        message = *pLine;
        return true;
      }
    else
      { return false; }
  },
  true );
#endif
  
  if(!message.empty())
    {
      // remove any '\n' characters
      message.erase(std::remove(message.begin(), message.end(), '\n'),
		    message.end() );
      return message;
    }
  else
    { return "UNKOWN"; }
}

/** Retrieves the device's wifi MAC address.
 *  @return a string containing the device's wifi MAC address.
 */
std::string cDeviceInfo::retrieveWifiMacAddr()
{
  std::string message = "";

#ifdef __ANDROID__
  // create a new process to run 'netcfg', filtering the output for lines
  //  containing "wlan0".
  cShellProcess findWifiMacAddr("netcfg | grep \"wlan0\"",
  [&](const std::string *pLine)
  {
    if(pLine)
      {
        // Note: this overwrites with each new match to return the LAST one!
        message = *pLine;
        return true;
      }
    else
      { return false; }
  },
  true );
#endif
  
  if(!message.empty())
    {
      // find MAC address
      std::stringstream ss(message);
      std::string macAddr;
      ss >> macAddr; // "wlan0"
      ss >> macAddr; // "UP"/"DOWN"
      ss >> macAddr; // device ip address
      ss >> macAddr; // device flags
      ss >> macAddr; // device MAC address
      
      // capitalize letters
      std::transform(macAddr.begin(), macAddr.end(), macAddr.begin(), ::toupper);
      return macAddr;
    }
  else
    {
      return "UNKOWN";
    }
}

/** Retrieves the device's bluetooth MAC address.
 *  @return a string containing the device's bluetooth MAC address.
 */
std::string cDeviceInfo::retrieveBtMacAddr()
{
  std::string message = "";

#ifdef __ANDROID__
  // create a new process to query the bluetooth MAC address.
  cShellProcess findBtMacAddr("settings get secure bluetooth_address",
  [&](const std::string *pLine)
  {
    if(pLine)
      {
        // Note: this overwrites with each new match to return the LAST one!
        message = *pLine;
        return true;
      }
    else
      { return false; }
  },
  true );
#endif

  
  if(!message.empty())
    {
      // remove any '\n' characters
      message.erase(std::remove(message.begin(), message.end(), '\n'),
		    message.end() );
      return message;
    }
  else
    { return "UNKOWN"; }
}
