#include "voiceTracker.hpp"
#include "logging.hpp"
#include "circleMath.hpp"
#include "debugSwitches.h"
#include "common.hpp"

#include <cmath>

#define VOICE_MATCH_TOLERANCE 20  // (deg) voices within this +/- range will be
                                  // considered matches

cVoiceTracker::cVoiceTracker(void)
{
  reset();
}

/** @param voice is the cVoice object that contains information about the sound
    source that is being tracked. */
cVoiceTracker::cVoiceTracker(cVoice voice)
{
  setVoice(voice);
}

cVoiceTracker::~cVoiceTracker()
{
}

/** This function triggers the Voice Tracker to analyze the latest data and 
 * update that tracker's status.
 *
 * The list of Voices provided contains information about where the microphone
 * system thinks sound is coming from.  The function looks through these Voices
 * and removes the ones that match the Tracker and then returns the rest.  In
 * the process, the Tracker's score is updated and the state is adjusted as
 * necessary.
 *
 * @param voices is a vector of cVoice objects that represent potential sound
 * sources.
 *
 * @return a vector containing the Voices that didn't match the Tracker.
 */
std::vector<cVoice> cVoiceTracker::update(std::vector<cVoice> voices)
{
  const float kMatchVal = 10.0;
  const float kLeakVal = 0.1;
  const float kMaxVal = 500.0;

  bool matched = false;

  // set of voices, less the ones that matched this tracker
  std::vector<cVoice> unmatchedVoices;

  for(std::vector<cVoice>::iterator iVoice = voices.begin(); 
      iVoice != voices.end(); ++iVoice )
    {
      float distance = std::abs(circleMath::
				intDiffAngs(mDirection, 
					    (*iVoice).getDirection() ) );
      if(mDirection < 0)
	{ // negative direction == invalid voice tracker 
	  distance = VOICE_MATCH_TOLERANCE;
	}
      
      if(distance < VOICE_MATCH_TOLERANCE)
	{ // found matching voice
	  matched = true;
	  mVoice = *iVoice;

	  if(mScore >= 0)
	    {mScore += kMatchVal;}
	  else
	    {mScore = kMatchVal;}
	}
      else
	{ 
	  // store voice in updated rev of data set
	  unmatchedVoices.push_back(*iVoice);
	}
    }
  
  // update score
  if(!matched)
    {mScore -= kLeakVal;}

  // cap score
  if(mScore > kMaxVal) {mScore = kMaxVal;}

  // update tracking state
  setState();
  
  if(DEBUG(VOICE_TRACKER)) {
    LOGI("voice score = %f\n", mScore); }

  return unmatchedVoices;
}

/** Reset the Tracker's state and history. */
void cVoiceTracker::forceReset(void)
{
  reset();
}

/** Set the voice that the Tracker is based on.
 *
 * @param voice is the cVoice object that will be tracked.
 */
void cVoiceTracker::setVoice(cVoice voice)
{
  reset();
  mVoice = voice;
  mDirection = mVoice.getDirection(); 
  mScore = 14.0;//7.0; // 10.0 gives about 15 seconds of time in the bank whenever
                 // someone starts to speak.  15.0 gives about 30 seconds

  // TODO: Make this time based instead of score based! (maybe make score use
  //       time units?)
}

/** Force a new nominal bearing to track.
 *
 * @param direction is the new bearing (integral degrees).
 */
void cVoiceTracker::setDirection(int direction)
{
  mDirection = direction;
}

/** Return the state of the Tracker (tracking, lost, etc.). */
voiceState_t cVoiceTracker::getState(void)
{
  return mState;
}

/** Return the cVoice that the Tracker is based on.
 *
 * @return Pointer to cVoice
 */
cVoice * cVoiceTracker::getVoice(void)
{
  return &mVoice;
}

/** Return the direction (0 - 360 degrees in 1 degree increments) of the sound
    source. */
int cVoiceTracker::getDirection(void)
{
  return mDirection;
}

/** Reset the Tracker's state and history. */
void cVoiceTracker::reset(void)
{
  mScore = 0.0;
  mDirection = -1;
  mState = voiceState::UNKNOWN;
}

/** Update the Tracker's state by applying a simple threshold to the score. */
void cVoiceTracker::setState(void)
{
  const float kTrackingThreshold = 0.0;
  const float kPersistenceThreshold = 25.0;

  if(mScore > kPersistenceThreshold)
    {
      mState = voiceState::PERSISTENT;
    }
  else if(mScore > kTrackingThreshold)
    {
      mState = voiceState::TRACKING;
    }
  else
    {
      mState = voiceState::LOST;
    }
}
