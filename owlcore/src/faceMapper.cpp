#include <sys/resource.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <unistd.h>

#include "debugSwitches.h"
#include "circleMath.hpp"
#include "faceMapper.hpp"
#include "logging.hpp"
#include "params.h"

#include <vector>

#define NUM_SEARCH_BLOCKS 12
#define NUM_SEARCH_SHIFTS  5

#define MAP_ADD_VAL             500
#define MAP_LEAK_VAL             10
#define MAP_SPREAD_VAL            5 // degrees

cFaceMapper::cFaceMapper(cPanorama *pPano)
{
  mpPano = pPano;

  // set map scoring parameters
  mMap.setAddVal(MAP_ADD_VAL);
  mMap.setLeakVal(MAP_LEAK_VAL);
  mMap.setSpreadVal(MAP_SPREAD_VAL);
  // using default MIN and MAX values (0 and 1000)

  // create a place to copy the mpPano gray image
  mGrayImage = cv::Mat(CV_PANO_RES_Y, CV_PANO_RES_X, CV_8UC1);

  // initialize image size variables
  setImageSize();

  // assume we're streaming at the start to be safe
  mStreaming = true;

  // initialize mutex and start thread
  if (pthread_mutex_init(&mMutex, NULL) != 0)
    { LOGE("unable to init faceMapper mutex\n"); }
  start();
}

cFaceMapper::~cFaceMapper()
{
  // stop thread and destroy mutex
  join();
  pthread_mutex_destroy(&mMutex);
  // log the final map
  mMap.logMap("final face map:");
}


/** This function waits for the thread to exit. */
void cFaceMapper::join(void)
{
  mKeepThreading = false;
  pthread_join(mThread, NULL);
}

/** This function creates and starts the thread .*/
void cFaceMapper::start(void)
{
  mKeepThreading = true;
  if (pthread_create(&mThread, NULL, threadWrapper, this) != 0)
    { LOGE("unable to create faceMapper thread\n"); }
  else
    { pthread_setname_np(mThread, "cFaceMapper"); }
}

/** This function implements the thread update loop. */
void cFaceMapper::threadUpdate(void)
{
  pid_t tid = syscall(SYS_gettid);
  int ret = setpriority(PRIO_PROCESS, tid, DEFAULT_THREAD_PRIORITY);
  LOGV("THREADING ==> faceMapper thread has id %d and priority %d\n",
       tid, getpriority(PRIO_PROCESS, tid));
  while (mKeepThreading)
    {
      update();
      usleep(FACEMAPPER_THREAD_USLEEP);
    }
  LOGV("THREADING ==> faceMapper thread exiting\n");
}

/** This function provides a wrapper that's compatible with pthread_create(). */
void *cFaceMapper::threadWrapper(void *pContext)
{
  reinterpret_cast<cFaceMapper *>  (pContext)->threadUpdate();
  return((void *)NULL);
}

void cFaceMapper::setStreaming(bool streaming)
{
  mStreaming = streaming;
}

/** This is the main externally facing function that is called to trigger the
 * cFaceMapper to perform an analysis cycle.
 *
 * Each time this function is called it searches for faces in a small
 * sub-region of the provided panoramic image.  The results are fed
 * into the cMap and the search sub-region is then adjust in
 * anticipation of the next update call.  In this way, the whole
 * screen is searched bit-by-bit.
 */
void cFaceMapper::update(void)
{
  // Bail out immediately if not streaming. This allows the system to skip
  // CPU intensive operations when not in a meeting.
  if (!mStreaming) { return; }

  // get deep copy of gray image
  mpPano->copyGrayImage(&mGrayImage);

  // prepare search sub-image
  cv::Mat searchSubImage = cv::Mat(cv::Size(mSearchRoi.width, 
					    mSearchRoi.height ),
				   mGrayImage.type() );
  std::vector<cv::Rect> searchRois;

  searchRois = circleMath::cropRect(mImageSize, mSearchRoi);
  
  if(searchRois.size() == 1)
    { // this avoids the extra Mat copy operation found in the "else" below
      searchSubImage = cv::Mat(mGrayImage, mSearchRoi);
    }
  else
    {
      // paste together ROIs to recreate a sub-region that breach the edge of
      // the panoramic image
      int x = 0; int y = 0;
      
      for(std::vector<cv::Rect>::iterator iRoi = searchRois.begin();
	  iRoi != searchRois.end(); ++iRoi )
	{
	  cv::Rect dstRoi(x, y, (*iRoi).width, (*iRoi).height);
	  
	  cv::Mat src(mGrayImage, *iRoi);
	  cv::Mat dst(searchSubImage, dstRoi);
	  
	  src.copyTo(dst);
	  
	  x += (*iRoi).width;
	}
    }

  // find faces!
  std::vector<cv::Rect> faces = mFaceDetector.detectFaces(&searchSubImage);
  
  std::vector<int> faceBearings;

  // update map
  if(faces.size() > 0)
    { 
      for(std::vector<cv::Rect>::iterator iFace = faces.begin();
	  iFace != faces.end(); ++iFace )
	{
	  int pixel = mSearchRoi.x  + (*iFace).x + (*iFace).width / 2;
 
	  if(pixel > mImageSize.width) 
	    {pixel -= mImageSize.width;}

	  faceBearings.push_back((int)mpPano->angleAt(pixel));
	}
      
      pthread_mutex_lock(&mMutex);
      mMap.update(faceBearings);

      if(DEBUG(FACE_MAPPER))
	{ mMap.printMap(20); }
      pthread_mutex_unlock(&mMutex);
    }

  mMap.update(faceBearings);
  
  if(DEBUG(FACE_MAPPER)) {
    mMap.printMap(20); }

  // prep for next update
  incrementSearchRoi();
}

/** Resets all of the face map's stored values to zero. */
void cFaceMapper::resetMap(void)
{
  pthread_mutex_lock(&mMutex);
  mMap.resetMap();
  pthread_mutex_unlock(&mMutex);
}

/** This function is called once and serves to set up the sub-region search
 * system.
 *
 * In addition to storing the size of the scene image (which is used regularly
 * by other parts of this module), this function also determines the dimensional
 * parameters that will be used in sizing and position the search 
 * region-of-interest. 
 */
void cFaceMapper::setImageSize(void)
{
  mImageSize = mGrayImage.size();
  
  mSearchIncrement.width = (int)(mImageSize.width / (float)NUM_SEARCH_BLOCKS);
  mSearchIncrement.height = mImageSize.height;
  
  resetSearchRoi();
}

/** This function resets the position & size of the search sub-region. */
void cFaceMapper::resetSearchRoi(void)
{
  mSearchRoi.x = 0;
  mSearchRoi.y = 0;
  mSearchRoi.width = mSearchIncrement.width;
  mSearchRoi.height = mSearchIncrement.height;

  mShiftCount = 0;
}

/** This function moves the search sub-region to its next location. */
void cFaceMapper::incrementSearchRoi(void)
{
  // increment horizontally to next search region
  mSearchRoi.x += mSearchIncrement.width;

  // watch out for over-extension
  if (mSearchRoi.x > mImageSize.width) 
    {
      // apply a changing shift to the start location
      mSearchRoi.x = mShiftCount * mSearchIncrement.width / NUM_SEARCH_SHIFTS;
      // update the counter and check for wraparound
      mShiftCount++;
      if (mShiftCount >= NUM_SEARCH_SHIFTS)
	{ mShiftCount = 0; }
    }
}

/** This function is used to gain access to the rectangle that defines the 
 * sub-search region.
 *
 * @return OpenCV Rect that defines the search sub-region.
 */
cv::Rect cFaceMapper::getSearchRoi(void)
{
  return mSearchRoi;
}

/** This function gets the value for the given angle from the underlying map.
 *
 * @return map value at give angle.
 */ 
int cFaceMapper::getMapValAt(int angle)
{
  pthread_mutex_lock(&mMutex);
  int val = mMap.getValAt(angle);
  pthread_mutex_unlock(&mMutex);
  return val;
}

/** This function gets the maximum value within a range of bearings around the
 * given nominal angle.
 *
 * @return maximum map value near the given angle.
 */
int cFaceMapper::getAngleWithMaxVal(int nominalAngle, int plusMinus)
{
  pthread_mutex_lock(&mMutex);
  int val = mMap.getAngleWithMaxVal(nominalAngle, plusMinus);
  pthread_mutex_unlock(&mMutex);
  return val;
}

/** This function returns the map which is keeping track of the positional data
 * of the things found by the cFaceMapper.
 *
 * @return the map containing a vector of integers.
 */
cMap cFaceMapper::getMap(void)
{
  pthread_mutex_lock(&mMutex);
  cMap map = mMap;
  pthread_mutex_unlock(&mMutex);
  return map;
}
