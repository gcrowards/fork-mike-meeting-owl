#include "common.hpp"
#include "logging.hpp"
#include "systemHealth.hpp"

#include <cstring>
#include <fstream>
#include <iostream>
#include <string.h>
#include <sstream>
#include <sys/stat.h>
#include <unistd.h>


#define UPDATE_PERIOD        6.   /**< seconds between writes to log file. */
#define LOG_FILE_TEMPLATE "HealthStatus_%s.csv" /**< File name template to make 
						 *per meeting record files. */
#define PATH_MAX_STRING_SIZE 256  /**< Maximum allowed full path. */

#define HEADER_STRING "Time (m),Frame Rate (fps),Temp - zone 0 (C),"         \
  "Temp - zone 1 (C),Temp - zone 2 (C),Temp - zone 3 (C),Temp - zone 4 (C)," \
  "Temp zone 5 (C),Temp zone 6 (C),Core 0 (Hz),Core 1 (Hz),Core 2 (Hz),"     \
  "Core 3 (Hz),Total CPU % busy\n"

cSystemHealth::cSystemHealth(void)
{
  init();
}

/*
 * @param saveDirectory is a string that identifies the directory where the
 * system health log file will be stored.
 */
cSystemHealth::cSystemHealth(std::string saveDirectory)
{
  init();
  start(saveDirectory);
}

/** Perform common constructor initialization. */
void cSystemHealth::init(void)
{
  mPrimed = false;
  mpFileHandle = NULL;
  currentTimeSpec(&mLastUpdateTime);
}

cSystemHealth::~cSystemHealth()
{
  // close data file if still open
  close();
}

/** Externally facing function used to start a log session.
 *
 * @param saveDirectory is a string objects that contains the directory where
 * the log file should be stored (does not include file name).
 */
void cSystemHealth::start(std::string saveDirectory)
{
  int rc;  
  mSaveDir = saveDirectory;

  // make sure system is stopped (in case user called start() without first
  // calling close() on an already-running log)
  close();

  // open log file
  rc = openDataFile();
  if(rc < 0) { LOGE("Couldn't open System Health file\n"); }

  // write header
  rc = writeData(HEADER_STRING);
  if(rc < 0) { LOGE("Couldn't write to System Health header to file\n"); }
  
  // note start time (used for timestamp in log file)
  currentTimeSpec(&mStartTime);
}

/** Externally facing function used to trigger a sampling and logging.
 *
 * Note: This function will only take action if it's time to take a new sample, 
 *       as dictatd by UPDATE_PERIOD at the top of this file.
 *
 * @param fps is the video frame rate as measured elsewhere in the system.
 */
void cSystemHealth::update(float fps)
{
  std::string data;
  int rc;

  // don't sample unless it's time
  if(elapsedTimeSinceSec(mLastUpdateTime) < UPDATE_PERIOD)
    {
      return;
    }

  // append time since start
  double timeSinceStart = elapsedTimeSinceSec(mStartTime) / 60;
  std::ostringstream converter;
  converter << timeSinceStart << ",";

  // append fps
  converter.precision(3);
  converter << fps << ",";
  data += converter.str();

  // append cpu performance data
  data += getCpuStatusResults() + "\n";

  // write results to file
  rc = writeData(std::string(data));
  if(rc < 0) { LOGE("Couldn't write to System Health file\n"); }

  // reset update timer
  currentTimeSpec(&mLastUpdateTime);
}

/** Externally facing function used to close a logging session. */
void cSystemHealth::close(void)
{
  // close log file
  if(mpFileHandle != NULL)
    {
      int rc = closeDataFile();
      if(rc < 0) { LOGE("Couldn't close System Health file\n"); }
      else { mpFileHandle = NULL; }
    }  

  // need to re-prime /proc/stat next time a session is started
  mPrimed = false;
}

/** Open file to log to (create path, if necessary).
 *
 * @return 0 on success, -1 on failure.
 */
int cSystemHealth::openDataFile(void)
{
  // Create System Health storage directory, if necessary
  if (access(mSaveDir.c_str(), R_OK) != 0)
    {
      mode_t origMask = umask(0);
      mkdirWithParents(mSaveDir.c_str(),
                       S_IRWXU | S_IRWXG | S_IRWXO);
      umask(origMask);
    }
  
  // Generate filename
  char fullPath[PATH_MAX_STRING_SIZE];
  char fileName[PATH_MAX_STRING_SIZE];
  
  sprintf(fileName, LOG_FILE_TEMPLATE, currentDateTime().c_str());
  
  // Create full path and open file
  pathify(fullPath, fileName);

  mode_t origMask = umask(0);
  mpFileHandle = fopen(fullPath, "w");
  umask(origMask);

  if(mpFileHandle == NULL)
    { return -1; }
  else
    { return 0; }
}

/** Write the given string of data to log file.
 *
 * @param data is the string to log.
 *
 * @return 0 on success, -1 on failure.
 */
int cSystemHealth::writeData(std::string data)
{
  // write data and make sure it makes it right through to disk
  if(mpFileHandle != NULL)
    {
      fprintf(mpFileHandle, data.c_str(), "%s");  
      fflush(mpFileHandle);
      sync();

      return 0;
    }
  else
    {
      return -1;
    }
}

/** Close log file; session over.
 *
 * @return 0 on success, -1 on failure.
 */
int cSystemHealth::closeDataFile(void)
{
  int rc = 1;

  if(mpFileHandle != NULL)
    {      
      rc = fclose(mpFileHandle);

      if(rc > 0)
	{
	  sync();   // make sure any buffered data makes it to disk.
	  mpFileHandle = NULL;
	}
    }
      
  return rc;
}

/** Pre-pend the black box folder path to the filename and return file's length.
 *
 * @param pFullPath is the result of joining the file name to the directory name.
 * @param pFileName is the file name to append to the pre-set directory.
 *
 * @return the file's length, or -1 if the file was not found.
 */
int cSystemHealth::pathify(char *pFullPath, const char *pFileName)
{
  struct stat statBuf;
  const char* pSaveDir = mSaveDir.c_str();
  int rc;

  strcpy(pFullPath, pSaveDir);
  if (pSaveDir[strlen(pSaveDir) - 1] != '/')
    {
      // Only add trailing '/' if not already there
      strcat(pFullPath, "/");
    }
  strcat(pFullPath, pFileName);
  rc = stat(pFullPath, &statBuf);
  if (rc == 0)
    {
      rc = statBuf.st_size;
    }
  else
    {
      rc = -1;
    }
  return rc;
}

/** Recursively make directories as necessary to create entire path.
 *
 * @param pDir full or relative path of directory to create.
 * @param mode is the new directory's access rights value. (Usually combination
 * of OR'd 'S_Ixxxx' values from <stat.h>.)
 *
 * @return -1 if access rights prohibit completion of the task.
 */
int cSystemHealth::mkdirWithParents(const char *pDir, const mode_t mode)
{
  char tmp[PATH_MAX_STRING_SIZE];
  char *p = NULL;
  struct stat sb;
  size_t len;

  /* copy path */
  strncpy(tmp, pDir, sizeof(tmp));
  len = strlen(tmp);
  if (len >= sizeof(tmp))
    {
      return -1;
    }

  /* remove trailing slash */
  if(tmp[len - 1] == '/')
    {
      tmp[len - 1] = 0;
    }

  /* recursive mkdir */
  for(p = tmp + 1; *p; p++)
    {
      if(*p == '/')
	{
	  *p = 0;
	  /* test path */
	  if (stat(tmp, &sb) != 0)
	    {
	      /* path does not exist - create directory */
	      if (mkdir(tmp, mode) < 0)
		{
		  return -1;
		}
	    }
	  else if (!S_ISDIR(sb.st_mode))
	    {
	      /* not a directory */
	      return -1;
	    }
	  *p = '/';
	}
    }
  /* test path */
  if (stat(tmp, &sb) != 0)
    {
      /* path does not exist - create directory */
      if (mkdir(tmp, mode) < 0)
	{
	  return -1;
	}
    }
  else if (!S_ISDIR(sb.st_mode))
    {
      /* not a directory */
      return -1;
    }
  return 0;
}

/** Read single-line data from a system file (e.g. internal temperature).
 *
 * @param stateFileName is the name of the file (full path) that contains the
 * data of interest.
 *
 * @return a string object with the contents of the given file.
 */
std::string cSystemHealth::readCpuStateFile(std::string stateFileName)
{
  // open file input stream
  std::ifstream stream(stateFileName.c_str());
  std::string data = "-1";

  // read data, if possible
  if (stream.is_open())
    {
      getline(stream, data);
      stream.close();
    }
  else
    {
      static bool firstTime = true;
      // Only report the first error to avoid a barrage of errors
      if (firstTime)
        {
          LOGE("Unable to open CPU state file: %s\n", stateFileName.c_str());
          firstTime = false;
        }
    }
  
  return data;
}

/** Calculate CPU loading from data in /proc/stat. 
 *
 * @return the total percent load on the CPU since the last call to this 
 * function(across all cores).
 */
float cSystemHealth::getCpuLoad(void)
{
  const int kIdleIndex = 3;              // index in /proc/stat for idle process

  long double procStat[PROC_STAT_COUNT]; // raw data from /proc/stat 
  
  long double totalProcesses = 0.;       // processed data
  long double busyProcesses;             //
  long double idleProcesses;             //
  float percentBusy = -1;                //

  FILE *fp = fopen("/proc/stat", "r");
  
  if(fp != NULL)
    {
      // read in raw data
      int fieldsConverted = fscanf(fp,"%*s %Lf %Lf %Lf %Lf %Lf %Lf %Lf",
	     &procStat[0], &procStat[1], &procStat[2], &procStat[3],
	     &procStat[4], &procStat[5], &procStat[6] );
      if (fieldsConverted != 8)
        { LOGW("/proc/stat not parsed properly\n"); }
      fclose(fp);

      if(mPrimed)
	{
	  // process data to find percent busy
	  for(int i = 0; i < PROC_STAT_COUNT; i++)
	    {
	      totalProcesses += procStat[i] - mPrevProcStat[i];
	    }
	  
	  idleProcesses = procStat[kIdleIndex] - mPrevProcStat[kIdleIndex];
	  busyProcesses = totalProcesses - idleProcesses;
	  
	  percentBusy = (busyProcesses / totalProcesses) * 100;
	}
      else
	{
	  // prime for next call
	  std::memcpy(mPrevProcStat, procStat, 
		      sizeof(long double) * PROC_STAT_COUNT );
	  mPrimed = true;
	}
    }
  else
    {
      LOGE("Couldn't open /proc/stat\n");
    }
  
  return percentBusy;
}

/** @return a comma separated string containing the status of the CPU as 
 * described by various parameters.
 */
std::string cSystemHealth::getCpuStatusResults(void)
{
  // Qualcomm SOC Chip Temperatures
  // The document that contains the mapping is 'confidential', ask Peter or Will

  // right edge of SOC
  const std::string kThermalZone0Path =
    "/sys/devices/virtual/thermal/thermal_zone0/temp"; 
  // camera
  const std::string kThermalZone1Path =
    "/sys/devices/virtual/thermal/thermal_zone1/temp";
  // between GPU and Modem
  const std::string kThermalZone2Path =
    "/sys/devices/virtual/thermal/thermal_zone2/temp";
  // unknown location
  const std::string kThermalZone3Path =
    "/sys/devices/virtual/thermal/thermal_zone3/temp";
  // between CPU2 & CPU3
  const std::string kThermalZone4Path =
    "/sys/devices/virtual/thermal/thermal_zone4/temp";
  // CPU0
  const std::string kThermalZone5Path =
    "/sys/devices/virtual/thermal/thermal_zone5/temp";
  // unknown location
  const std::string kThermalZone6Path =
    "/sys/devices/virtual/thermal/thermal_zone6/temp";

  // Frequencies for each CPU
  const std::string kCpu0FreqPath =
    "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_cur_freq";
  const std::string kCpu1FreqPath =
    "/sys/devices/system/cpu/cpu1/cpufreq/cpuinfo_cur_freq";
  const std::string kCpu2FreqPath =
    "/sys/devices/system/cpu/cpu2/cpufreq/cpuinfo_cur_freq";
  const std::string kCpu3FreqPath =
    "/sys/devices/system/cpu/cpu3/cpufreq/cpuinfo_cur_freq";
  
  // build results string
  std::string cpuStatusString;

  cpuStatusString += readCpuStateFile(kThermalZone0Path) + ",";
  cpuStatusString += readCpuStateFile(kThermalZone1Path) + ",";
  cpuStatusString += readCpuStateFile(kThermalZone2Path) + ",";
  cpuStatusString += readCpuStateFile(kThermalZone3Path) + ",";
  cpuStatusString += readCpuStateFile(kThermalZone4Path) + ",";
  cpuStatusString += readCpuStateFile(kThermalZone5Path) + ",";
  cpuStatusString += readCpuStateFile(kThermalZone6Path) + ",";

  cpuStatusString += readCpuStateFile(kCpu0FreqPath) + ",";
  cpuStatusString += readCpuStateFile(kCpu1FreqPath) + ",";
  cpuStatusString += readCpuStateFile(kCpu2FreqPath) + ",";
  cpuStatusString += readCpuStateFile(kCpu3FreqPath) + ",";

  float cpuLoad = getCpuLoad();
  std::ostringstream converter;
  converter << cpuLoad;  
  cpuStatusString += converter.str();
  
  return cpuStatusString;
}

/** Get system temperature at location.
 *
 * @param eThermometerLoc_t Thermometer.
 *
 * @return Temperature in the selected zone.
 */
double cSystemHealth::getTempAtLoc(eTempSensorLoc_t loc)
{
  // Qualcomm SOC Chip Temperatures
  // The document that contains the mapping is 'confidential', ask Peter or Will

  // thermometer reading file path template
  const char thermalZonePathTemplate[] =
    "/sys/devices/virtual/thermal/thermal_zone%d/temp";
  char thermalZonePath[sizeof(thermalZonePathTemplate)];

  snprintf(thermalZonePath, sizeof(thermalZonePath),
           thermalZonePathTemplate, (int)loc);

  std::string cpuTempString = readCpuStateFile(thermalZonePath);

  int intVal;
  sscanf(cpuTempString.c_str(), "%d", &intVal);
  double dVal = (double)intVal;

  if (loc == CPU_0)
    { dVal /= 1000.0; }

  return dVal;
}
