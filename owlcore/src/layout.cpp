#include "circleMath.hpp"
#include "common.hpp"
#include "elements/patch.hpp"
#include "layout.hpp"
#include "logging.hpp"
#include "debugSwitches.h"
#include "params.h"

#include <stdlib.h>
#include <algorithm>
#include <cstddef>

cLayout::cLayout(void)
{
  for(int i = 0; i < NUM_STAGE_SLOTS; i++)
    {
      mpStageLayout.push_back(NULL);
    }
}

cLayout::~cLayout()
{
}

/** This is the externally facing function that should be used to add a cSubframe
 * to the Layout as a "base" frame, which means that it will be static and a
 * permanent part of the Layout.
 *
 * @param pSubframe is a pointer to the cSubframe to be added to the Layout.
 */
void cLayout::addToBase(cSubframe *pSubframe)
{
  addSubframeToBase(pSubframe);

  if(DEBUG(LAYOUT)) {printLayoutSize(); }
}

/** This is the externally facing function taht should be used to remove a 
 * cSubframe from the static base Layout (see addToBase() for more explanation).
 *
 * @param pSubframe is a pointer to the cSubframe to be removed from the Layout.
 */
bool cLayout::removeFromBase(cSubframe *pSubframe)
{
  bool removed = removeSubframeFromBase(pSubframe);

  if(DEBUG(LAYOUT)) {printLayoutSize(); }

  return removed;
}

/** This is the externally facing function that should be used to add a cSubframe
 * to the Layout stage.  The stage is the dynamic region of the Layout where
 * close-up views and the like come and go.
 *
 * When a cSubframe is added if comes in from the right.  Pre-existing subframes
 * are shifted left.  If the Layout is already full then the add request is
 * silently disregarded (which is probably not the best thing).
 *
 * // TODO: Consider making mpLayout a dynamic vector that allows as many 
 *          subframes to be added as we like, only switch to panoramic if there 
 *          are more than X slots filled.
 *
 * @param pSubframe is a pointer to the cSubframe to be added to the Layout.
 */
void cLayout::addToStage(cSubframe *pSubframe)
{
  for(std::vector<cSubframe *>::iterator iSlot = mpStageLayout.begin();
      iSlot != mpStageLayout.end(); ++iSlot )
    {
      if(*iSlot == NULL)
	{
	  *iSlot = pSubframe;
	  addSubframeToStage(pSubframe);
	  pSubframe->setInLayout(true);
	  break;
	}
    }
  
  if(DEBUG(LAYOUT)) {printLayoutSize(); }
}

/** This is the externally facing function that should be used to remove the 
 * given cSubframe from Layout. 
 *
 * When a cSubframe is removed the reamining subframes are shifted to the left.
 *
 * @param pSubframe is a pointer to the cSubframe to be removed from the Layout.
 *
 * @return true if the cSubframe was removed.
 */
bool cLayout::removeFromStage(cSubframe *pSubframe)
{
  bool subframeRemoved = false;
  int removeIndex = -1;

  for(std::vector<cSubframe *>::iterator iSubframe = mpStageLayout.begin();
      iSubframe != mpStageLayout.end(); ++iSubframe )
    {
      if(subframeRemoved)
	{
	  *(iSubframe - 1) = *iSubframe;
	  *iSubframe = NULL;
	}
      else if(*iSubframe == pSubframe)
	{
	  subframeRemoved = true;
	  removeIndex = (int)(iSubframe - mpStageLayout.begin());
	  *iSubframe = NULL;
	}
    }

  if(subframeRemoved)
    {
      removeSubframeFromStage(pSubframe, removeIndex);
      pSubframe->setInLayout(false);
      pSubframe->terminate();

      // Note: subframe is marked as out of layout, but it still exists until 
      //       it's done animating off the screen (but it's no longer part of 
      //       the layout for future planning purposes)

      if(DEBUG(LAYOUT)) {printLayoutSize();}

      return true;
    }
  else
    {
      if(DEBUG(LAYOUT)) {printLayoutSize(); }

      return false;
    }
}

/** This function nulls out the list of cSubframe objects that are part of the
    Layout. */
void cLayout::clearStage(void)
{
  // clears all subframes from layout
  for(std::vector<cSubframe *>::iterator iSlot = mpStageLayout.begin();
      iSlot != mpStageLayout.end(); ++iSlot )
    {
      *iSlot = NULL;
    }
}

/** This function checks to see if there's enough space left in the layout to
    include the given subframe. */
bool cLayout::checkSpaceFor(cSubframe *pSubframe)
{
  int usedSpace = pSubframe->getMinReqSize().width;

  for(std::vector<cSubframe *>::iterator iSubframe = mpStageLayout.begin();
      iSubframe != mpStageLayout.end(); ++iSubframe )
    {
      if(*iSubframe != NULL)
	{
	  usedSpace += (*iSubframe)->getMinReqSize().width;
	}
    }

  if(usedSpace < OUTPUT_WIDTH) {return true;}
  else {return false;}
}

/** This function returns the amont of space that would be available if all 
    subframes in the layout were reduced to the minimum required size. */
cv::Size cLayout::findReclaimableSpace(void)
{
  cv::Size reclaimableSpace(0, 0);
  
  for(std::vector<cSubframe *>::iterator iSubframe = mpStageLayout.begin();
      iSubframe != mpStageLayout.end(); ++iSubframe )
    {
      if(*iSubframe != NULL) 
	{
	  // the size it is now minus the smallest it can be yields how much
	  // it's allowed to be reduced by to fit new stuff on the stage
	  reclaimableSpace += 
	    ((*iSubframe)->getSize() - (*iSubframe)->getMinReqSize());
	}
    }

  return reclaimableSpace;
}

/** This function reassesses the space that each of the Subframes in the Layout
 * are consuming and adjust the size of them, if needed, in order to make them
 * fit.
 */
void cLayout::refitStage(void)
{
  std::vector<cSubframe *> nonNullSubframes;

  getOrderedValidFrames(nonNullSubframes);

  // do the refit
  fitSubframes(nonNullSubframes);

  // set each subframe to animate, in case any of their sizes were adjusted 
  // (it's not a problem if one or more doesn't actually need to animate, it'll
  // take care of itself)
  for(std::vector<cSubframe *>::iterator iSubframe = nonNullSubframes.begin();
      iSubframe != nonNullSubframes.end(); ++iSubframe )
    {
      (*iSubframe)->enableAnimation();
    }
}

/** This function sets the position and size of the space that is available for
    the Layout. */
void cLayout::setStageArea(cv::Rect stageArea)
{
  mStageArea = stageArea;
}

/** @return the number of subframes in the Stage Layout. */
int cLayout::getLayoutCount(void)
{
  int count = 0;
  
  for(std::vector<cSubframe *>::iterator iSubframe = mpStageLayout.begin();
      iSubframe != mpStageLayout.end(); ++iSubframe ) 
    {
      if((*iSubframe) != NULL)
	{ count++; }
    }

  return count;
}

/** This internally facing function adds the given cSubframe to the base Layout.
 *
 * A subframe that is part of the base Layout is static and a permanent part of 
 * the Layout.
 *
 * @param pAddFrame is a pointer to the cSubframe to add to the base Layout.
 */
void cLayout::addSubframeToBase(cSubframe *pAddFrame)
{
  if(DEBUG(LAYOUT)) {
    LOGI(" ++++ add frame to static Base Layout ++++\n"); }

  mpBaseLayout.push_back(pAddFrame);
}

/** This internally facing function removes the given cSubframe to the base 
 * Layout.
 *
 * A subframe that is part of the base Layout is static and a permanent part of 
 * the Layout (which means that it's kind of unlikely that it should be 
 * removed!).
 *
 * @param pRemoveFrame is a pointer to the cSubframe to remove from the base 
 * Layout.
 */
bool cLayout::removeSubframeFromBase(cSubframe *pRemoveFrame)
{
  if(DEBUG(LAYOUT)) {
    LOGI(" ++++ remove frame to static Base Layout ++++\n"); }

  for(std::vector<cSubframe *>::iterator iSubframe = mpBaseLayout.begin();
      iSubframe != mpBaseLayout.end(); ++iSubframe )
    {
      if(*iSubframe == pRemoveFrame)
	{
	  mpBaseLayout.erase(iSubframe);
	  return true;
	}
    }

  return false;
}

/** This internally facing function adds the given cSubframe to the stage Layout,
 is the dynamic portion of the Layout where close-up views come/go.
    
 // TODO: add and remove SubframeTo/FromLayout have a lot in common... they
 //       could probably be combined into less duplicated code

 * @param pAddFrame is a pointer to the cSubframe to add to the Layout.
 */
void cLayout::addSubframeToStage(cSubframe *pAddFrame)
{
  eInsertionLocation_t insertionHint = getInsertionLocation(pAddFrame);

  if(DEBUG(LAYOUT)) {
    LOGI(" ++++ add frame to dynamic Stage Layout ++++\n"); }

  // setup the new subframe to come in location 'insertionHint'
  if(pAddFrame->getLocked() == false)
    {
      if (insertionHint == LEFT)
        {
          pAddFrame->setPos(cv::Point(0, mStageArea.y));
          pAddFrame->setEndPos(cv::Point(0, mStageArea.y));
          pAddFrame->setSize(cv::Size(0, mStageArea.height) );
          if(DEBUG(LAYOUT))
            {
              LOGI("addSubframeToStage(): adding to left (focus: %d)\n",
                   pAddFrame->getFocus().x);
            }
        }
      else if (insertionHint == RIGHT)
        {
          pAddFrame->setPos(cv::Point(OUTPUT_WIDTH, mStageArea.y));
          pAddFrame->setSize(cv::Size(0, mStageArea.height) );
          if(DEBUG(LAYOUT))
            {
              LOGI("addSubframeToStage(): adding to right (focus: %d)\n",
                   pAddFrame->getFocus().x);
            }
        }
      else // insertionHint == MIDDLE
        {
          // Find divider between subframes currently on stage
          // This will be the largest of the left edges
          int largestLeft = 0;
          for(std::vector<cSubframe *>::iterator iSubframe = mpStageLayout.begin();
              iSubframe != mpStageLayout.end(); ++iSubframe )
            {
              if(*iSubframe != NULL)
                {
                  if((*iSubframe)->getLocked() == false)
                    {
                      int left = (*iSubframe)->getEndPos().x -
                                    (*iSubframe)->getEndSize().width / 2;
                      if (largestLeft < left)
                        {
                          largestLeft = left;
                        }
                    }
                }
            }
          pAddFrame->setPos(cv::Point(largestLeft, mStageArea.y));
          pAddFrame->setEndPos(cv::Point(largestLeft, mStageArea.y));
          pAddFrame->setSize(cv::Size(0, mStageArea.height) );
          if(DEBUG(LAYOUT))
            {
              LOGI("addSubframeToStage(): adding to between (focus: %d)\n",
                   pAddFrame->getFocus().x);
            }
        }
    }
  
  // collect set of frames that need to stay on the screen
  std::vector<cSubframe *> framesToFit;
  getOrderedValidFrames(framesToFit);
  
  // set display state for all subframes to adding (so that they all animate
  // with the adding-style)
  //
  // Note: This MUST be done before calling fitSubframes() (below)
  for(std::vector<cSubframe *>::iterator iSubframe = framesToFit.begin();
      iSubframe != framesToFit.end(); ++iSubframe )
    {
      cv::Point posPoint = (*iSubframe)->getEndPos();
      int subframePos = posPoint.x;
      if(DEBUG(LAYOUT))
        {
          LOGI("framesToFit (f:%d) X: %d\n",
               (*iSubframe)->getFocus().x,
               subframePos);
        }
      (*iSubframe)->setDisplayState(cSubframe::ADDING);
    }

  // this sets end conditions for the frames that are on screen
  fitSubframes(framesToFit); 

  // flip flags to animate
  for(std::vector<cSubframe *>::iterator iSubframe = mpStageLayout.begin();
      iSubframe != mpStageLayout.end(); ++iSubframe )
    {
    if(*iSubframe != NULL)
      {(*iSubframe)->enableAnimation();}
  }
}

/** Get position into which this subframe will be inserted.
 *
 * @param framesToFit is the ordered list of renderable frames to return.
 */
cLayout::eInsertionLocation_t cLayout::getInsertionLocation(cSubframe *pAddFrame)
{
  int newPos = pAddFrame->getFocusXWrtPano();
  bool aoiToRight = false;
  bool aoiToLeft = false;
  eInsertionLocation_t loc = RIGHT;

  // use insertion sort to order framesToFit by EndPos.x
  for(std::vector<cSubframe *>::iterator iSubframe = mpStageLayout.begin();
      iSubframe != mpStageLayout.end(); iSubframe++)
    {
      if ((*iSubframe) != nullptr)
        {
          int aoiPos = (*iSubframe)->getFocusXWrtPano();

          // if AOI list element is already in the layout; (if not, ignore it)
          if((*iSubframe)->isInLayout())
            {
              if (newPos < aoiPos)
                {
                  if(DEBUG(LAYOUT))
                    {
                      LOGI("getInsertionLocation(): new=%d, aio=%d, AOI to right\n",
                           newPos, aoiPos);
                    }
                  aoiToRight = true;
                }
              else if (newPos > aoiPos)
                {
                  if(DEBUG(LAYOUT))
                    {
                      LOGI("getInsertionLocation(): new=%d, aio=%d, AOI to left\n",
                           newPos, aoiPos);
                    }
                  aoiToLeft = true;
                }
            }
        }
    }

  if (aoiToRight)
    {
      if (aoiToLeft)
        { loc = MIDDLE; /* Insert between */ }
      else
        { loc = LEFT; /* Insert to left */ }
    }
  else
    { loc = RIGHT; /* Insert to right */ }

  if(DEBUG(LAYOUT)) {
      LOGI("getInsertionLocation(): returning %d\n", (int)loc); }

  return loc;
}

/** Get a list of subframes in rendering order.
 *
 * @param framesToFit is the ordered list of renderable frames to return.
 * @param unlockedOnly is true if only unlocked frames should be included in
 * the list. Set to true when removing frames. Defaults to false.
 */
void cLayout::getOrderedValidFrames(std::vector<cSubframe *> &framesToFit,
                                    bool unlockedOnly)
{
  // use insertion sort to order framesToFit by EndPos.x
  for(std::vector<cSubframe *>::iterator iSubframe = mpStageLayout.begin();
      iSubframe != mpStageLayout.end(); iSubframe++)
    {
      bool found = false;
      bool includeThisFrame = false;
      if (*iSubframe != NULL)
        {
          if (unlockedOnly)
            {
              if((*iSubframe)->getLocked() == false)
                { includeThisFrame = true; }
            }
          else
            { includeThisFrame = true; }
        }
      if (includeThisFrame)
        {
          int subframePos = (*iSubframe)->getFocusXWrtPano();
          for (std::vector<cSubframe *>::iterator iFrameToFit = framesToFit.begin();
              iFrameToFit != framesToFit.end(); iFrameToFit++)
            {
              int frameToFitPos = (*iFrameToFit)->getFocusXWrtPano();
              if (subframePos < frameToFitPos)
                {
                  if(DEBUG(LAYOUT))
                    {
                      LOGI("Inserting (f:%d) at %d before (f:%d) %d\n",
                           (*iSubframe)->getFocus().x,
                           subframePos,
                           (*iFrameToFit)->getFocus().x,
                           frameToFitPos);
                    }
                  framesToFit.insert(iFrameToFit, (*iSubframe));
                  found = true;
                  break;
                }
            }
          if (!found)
            { framesToFit.push_back(*iSubframe); }
        }
    }
}

/** This internally facing function removes the given cSubframe from the 
 * stage Layout, which is the dynamic portion of the Layout where close-up views 
 * come/go.
 *
 * @param pRemoveFrame is a pointer to the cSubframe to remove from the Layout.
 * @param removeIndex is the index into  mpLayout vector of the cSubframe that
 * is to be removed.
 */
void cLayout::removeSubframeFromStage(cSubframe *pRemoveFrame, int removeIndex)
{
  if(DEBUG(LAYOUT)) {
    LOGI("---- remove frame from dynamic Stage Layout ----\n"); }

  // figure out which direction the subframe being removed is going to go
  if(removeIndex == 0)
    { // remove subframe by moving to the left (because it's on the left now)
      pRemoveFrame->setEndPos(cv::Point(0, 0));
    }
  else
    { // remove subframe by moving to the right
      pRemoveFrame->setEndPos(cv::Point(OUTPUT_WIDTH, mStageArea.y));
    }
  pRemoveFrame->setEndSize(cv::Size(0, mStageArea.height));
      
  // figure out where the rest of the subframes will end up on the display
  std::vector<cSubframe *> framesToFit;
  getOrderedValidFrames(framesToFit, true); // Note: Only unlocked frames

  // set display state for all subframes to removing (so that they all animate
  // with the removing-style)
  //
  // Note: This MUST be done before calling fitSubframes() (below)
  for(std::vector<cSubframe *>::iterator iSubframe = framesToFit.begin();
      iSubframe != framesToFit.end(); ++iSubframe )
    {
      (*iSubframe)->setDisplayState(cSubframe::REMOVING);
    }
  
  // set end positions and sizes of layout subframes
  fitSubframes(framesToFit);

  // enable animation
  std::vector<cSubframe *> framesToAnimate = framesToFit;
  framesToAnimate.push_back(pRemoveFrame);

  for(std::vector<cSubframe *>::iterator iSubframe = framesToAnimate.begin();
      iSubframe != framesToAnimate.end(); ++iSubframe )
    {
      (*iSubframe)->enableAnimation();
    }
}

/** This function makes sure that the subframes on the Stage are not allowed
 * to display the same region of the source image.
 *
 * This is specifically an issue when one subframe is panning its focus, which
 * could result in it panning into a region that is being displayed by another
 * subframe.
 */
void cLayout::preventOverlap(void)
{
  // don't let subframes display region get closer than this
  const int kMinAllowableSeparation = 5; // degrees (this is ~ 6" of horizontal
                                         // separation 6'

  // look through stage layout
  for(cSubframe * pPanningSubframe : mpStageLayout)
    {
      if(pPanningSubframe == NULL) { continue; }
      
      if(pPanningSubframe->getDisplayState() == cSubframe::PANNING)
	{ // only consider panning subframes

	  // create Patch because Patches have built-in math for measuring 
	  // overlap
	  owl::cPanel panningPanel = pPanningSubframe->getPanel();
	  owl::cPatch panningPatch;
	  
	  panningPatch.setPatch((int)RAD2DEG(panningPanel.theta0), 
				(int)RAD2DEG(panningPanel.theta1), 0, 0 );
	  
	  // check panning subframe against all other subframes in layout
	  bool preventedOverlap = false;

	  for(cSubframe * pTestSubframe : mpStageLayout)
	    {
	      if(pTestSubframe == NULL) { continue; }
	      if(pTestSubframe == pPanningSubframe) { continue; }
	      
	      // again, create a Patch to take advangtage of "Patch math"
	      owl::cPanel testPanel = pTestSubframe->getPanel();
	      owl::cPatch testPatch;
	      
	      testPatch.setPatch((int)RAD2DEG(testPanel.theta0), 
				 (int)RAD2DEG(testPanel.theta1), 0, 0 );

	      // measure how close patches are to one another
	      owl::cPatch separationPatch = panningPatch.
		findSeparation(testPatch);
	      
	      // if patches are too close then reset focus to previous focus
	      // (which was marked good by a previous call to this function)
	      if(separationPatch.dTheta < kMinAllowableSeparation &&
		 !separationPatch.isEmpty() )
		{
		  // !!! This is where action is taken to stop the overlap
		  //     from happening.  It is commented out for now because
		  //     it's not clear that this is the best action to take
		  //     as it may result in the attention system seeming to
		  //     be unresponsive.
		  //
		  //     The commit is going into develop with everything but
		  //     the final action being taken because eveything that
		  //     was done to get to this point of detecting the overlap
		  //     is good.
		  //
		  //     This is the place to put the action to take to deal
		  //     with an overlap situation:
		  //
		  /// pPanningSubframe->revertFocus();
		  //
		  // !!!
		  preventedOverlap = true;
		}
	    }
	  
	  // note down the current focus as a usable focus
	  if(!preventedOverlap) 
	    { pPanningSubframe->updateLastGoodFocus(); }
	}
    }
}

/** This function looks at a group of cSubframe objects and figures out how much
 * screen real estate they can each take up and still all fit on the display.
 *
 * Each cSubframe contains an attribute that states the minimum size that it has
 * to be (if it can't be this size or bigger it needs to be eliminated).  This 
 * function figures out how much larger than its minimum size each subframe 
 * should be in order to fill the available screen space.
 *
 * NOTE: Currently, fitting is only done along the horizontal axis.
 *
 * @param framesToFit is a vector of pointers to cSubframe objects that are to
 * be fit to the available display space.
 */
void cLayout::fitSubframes(std::vector<cSubframe *> framesToFit)
{
  std::vector<int> widths;
  int totalWidth = 0;

  // figure out how much to add (or subtract) to each subframe's minimum required
  // width
  for(std::vector<cSubframe *>::iterator iSubframe = framesToFit.begin();
      iSubframe != framesToFit.end(); ++iSubframe )
    {
      int width = (*iSubframe)->getMinReqSize().width;
      totalWidth += width; 
      widths.push_back(width);
    }

  // add borders
  totalWidth += (framesToFit.size() - 1) * SUBFRAME_BORDER_MARGIN;
  int modifier = (int)((float)(OUTPUT_WIDTH - totalWidth) / framesToFit.size());
  
  // update each subframe's end size accordingly & set each's display state so
  // that it animates w/ the appropriate style
  for(std::vector<cSubframe *>::size_type i = 0; i < framesToFit.size(); i++)
    {
      framesToFit[i]->setEndSize(cv::Size(widths[i] + modifier, mStageArea.height));
      framesToFit[i]->setDisplayState(cSubframe::ADJUSTING);	  
    }
  
  // update each subframe's end position accordingly
  int xEndPos = 0;
  for(std::vector<cSubframe *>::size_type i = 0; i < framesToFit.size(); i++)
    {
      if(i == 0)
	{
	  xEndPos += (int)(framesToFit[i]->getEndSize().width / 2.f);
	}
      else
	{
	  xEndPos += (int)(((framesToFit[i-1]->getEndSize().width / 2.f) +
			    (framesToFit[i]->getEndSize().width / 2.f) ) + 
			   SUBFRAME_BORDER_MARGIN );
	}

      if(DEBUG(LAYOUT)) {
	LOGI("cLayout fitSubFrames(): (f:%d) xEndPos: %d\n",
	     framesToFit[i]->getFocus().x,
	     xEndPos ); }
      
      framesToFit[i]->setEndPos(cv::Point(xEndPos, 0));
    }
}
				    				    
/** Debugging helper: counts the number of dynamic cSubframe objects in the 
 * Layout. 
 */
int cLayout::countStageLayoutSubframes(void)
{
  int count = 0;
  
  for(std::vector<cSubframe *>::iterator iSubframe = mpStageLayout.begin();
      iSubframe != mpStageLayout.end(); ++iSubframe )
    {
      if(*iSubframe != NULL)
	{count++;}
    }
  return count;
}

/** Debugging helper: prints to console the number of Subframes in the base and
 * stage portions of the Layout.
 */
void cLayout::printLayoutSize(void)
{
  LOGI("%d frames in dynamic Stage Layout\n", countStageLayoutSubframes()); 
  LOGI("%d frames in static Base Layout\n", mpBaseLayout.size());
}
