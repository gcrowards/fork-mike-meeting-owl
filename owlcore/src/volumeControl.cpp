/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  volumeControl.cpp
 *  This is the implementation file for cVolumeControl.
 *   --> Controls volume adjustment for the volume buttons. When a
 *        VOLUME UP/VOLUME DOWN button is held, the volume will be adjusted
 *        continuously. When clicked quickly, the volume will be adjusted only
 *        one step.
 */

#include "blackBox.hpp"
#include "logging.hpp"
#include "sysfs.hpp"
#include "volumeControl.hpp"

#include <unistd.h>

const double cVolumeControl::kHoldDelay = 0.3;
const double cVolumeControl::kHoldSpacing = 0.1;
const int cVolumeControl::kClickStep = 6;
const int cVolumeControl::kHoldStep = 6;
const std::string cVolumeControl::mkBlipWavPath =
  "/system/media/OwlLabs/volumeBlip.wav";

cVolumeControl::cVolumeControl(cSysfs *pSysfs, cBlackBox *pBlackBox)
  : mpSysfs(pSysfs), mpBlackBox(pBlackBox),
    mVolumeThread(std::bind(&cVolumeControl::volumeAdjustCallback, this),
	    "volumeControl" )
{
  // get stored volume setting
  int storedVolume;
  mpBlackBox->getInt(SETTINGS_ROOT_STR, SETTINGS_SPKR_VOLUME_STR, storedVolume);
  mpSysfs->setVolume(storedVolume);

  LOGD("Speaker volume: %d\n", mpSysfs->getVolume());
  
  // start continuous adjustment thread
  mVolumeThread.start();
}

cVolumeControl::~cVolumeControl()
{
  mVolumeAdjusting = false;   // stop adjusting volume
  mVolumeDir = 0;
  mVolumeThread.signalStop(); // signal the thread to stop
  mVolumeCv.notify_all();     // unblock the condition_variable to stop the loop
  mVolumeThread.join();       // join the thread
}

/** Callback for starting continous volume adjustment when a volume button is
 *   pressed. */
void cVolumeControl::buttonPress(int direction)
{
  if(mVolumeDir == 0)
    {
      std::lock_guard<std::mutex> lock(mVolumeLock);
      mVolumeDir = direction;
    }
}
/** Callback for starting continous volume adjustment when a volume button is
 *   pressed. */
void cVolumeControl::buttonHold()
{
  std::lock_guard<std::mutex> lock(mVolumeLock);
  {
    mVolumeAdjusting = true;
  }
  mVolumeCv.notify_all();
}

/** Callback for stopping continous volume adjustment when a volume button is
 *   released. */
void cVolumeControl::buttonRelease(int direction)
{
  if(direction == mVolumeDir)
    {
      if(!mVolumeAdjusting)
	{ adjustVolume(mVolumeDir * kClickStep); }
      
      std::lock_guard<std::mutex> lock(mVolumeLock);
      {
	mVolumeDir = 0;
	mVolumeAdjusting = false;
      }
    }
}

/** Sets the speaker volume to a value.
 *
 * @param volume is the volume level to set between [0, 100].
 */
void cVolumeControl::setVolume(int volume)
{
  // clamp setting to [0, 100] range
  volume = std::max(std::min(volume, 100), 0);

  mpSysfs->setVolume(volume);
  LOGD("Set Volume: %d\n", mpSysfs->getVolume());

  // store the volume in the black box
  mpBlackBox->setInt(SETTINGS_ROOT_STR, SETTINGS_SPKR_VOLUME_STR, volume);
  mpBlackBox->save();
}

/** Gets the speaker volume.
 *
 * @return volume setting.
 */
int cVolumeControl::getVolume(void)
{
  return mpSysfs->getVolume();
}

/** Adds an amount to the speaker volume. New volume is clamped to [0, 100].
 *
 * @param delta the amount to change the volume by (positive or negative).
 */
void cVolumeControl::adjustVolume(int delta)
{
  // add the delta to the volume and clamp to [0, 100]
  int newVolume = mpSysfs->getVolume() + delta;
  newVolume = std::max(std::min(newVolume, 100), 0);
  // set the volume
  mpSysfs->setVolume(newVolume);

  // play audio blip on speaker
  mBlipPlayer.stopPlayback();

  // TODO: Consider loading wav file only once, instead of every time it's played.
  if(mBlipPlayer.playWavFile(mkBlipWavPath.c_str(), nullptr) < 0)
    { LOGE("Could not play volume blip wav file.\n"); }
  
  LOGD("Adjusted Volume: %d\n", mpSysfs->getVolume());
  
  // store the volume in persistent settings
  mpBlackBox->setInt(SETTINGS_ROOT_STR, SETTINGS_SPKR_VOLUME_STR, newVolume);
  mpBlackBox->save();
}

/** Continuously adjusts the volume when mVolumeDir is non-zero. Otherwise, waits
 *   for it to be with a condition variable. Runs in a separate thread. */
void cVolumeControl::volumeAdjustCallback()
{
  // wait for signal to start adjusting (from button hold)
  std::unique_lock<std::mutex> lock(mVolumeLock);
  mVolumeCv.wait(lock,
		 [this]()
		 { return (!mVolumeThread.isRunning() || mVolumeAdjusting); } );

  if(!mVolumeThread.isRunning()) // quit if thread stopped
    { return; }

  // adjust the volume continuously while holding the button.
  while(mVolumeThread.isRunning() && mVolumeAdjusting)
    {
      // record the step value and unlock the mutex for asynchronous button
      // pushes.
      const int stepSign = mVolumeDir;
      lock.unlock();
      {
	// adjust the volume
	adjustVolume(stepSign * kHoldStep);
	LOGV("Set Volume (hold):  %d\n", mpSysfs->getVolume());
	usleep((int)(kHoldSpacing * 1000000));
      }
      lock.lock();
    }
}
