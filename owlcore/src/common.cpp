#include <sys/stat.h>
#include <sstream>
#include <string.h>
#include <time.h>

#include "common.hpp"
#include "logging.hpp"


/** The latest date and time.
 *
 * @return the date and time as a string in format YYYYMMDD-HHMMSS.
 */
std::string currentDateTime(void)
{
  time_t now = time(0);
  struct tm timeStruct;
  char timestamp[80];

  timeStruct = *localtime(&now);
  strftime(timestamp, sizeof(timestamp), "%Y%m%d-%H%M%S", &timeStruct);

  return timestamp;
}

/** The latest date and time.
 *
 * @return the date and time as a string in format YYYY-MM-DD HH:MM:SS.
 */
std::string currentDateTimeHumanReadable(void)
{
  time_t now = time(0);
  struct tm timeStruct;
  char timestamp[80];

  timeStruct = *localtime(&now);
  strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", &timeStruct);

  return timestamp;
}

/** This function provides a common interface for getting the time.
 *
 * @param pTs is a pointer to a timespec stucture
 */
void currentTimeSpec(struct timespec *pTs)
{
  clock_gettime(CLOCK_MONOTONIC, pTs);
}

/** This function finds the difference, as a timespec, between two given times.
 *
 * Warning: If function is used with startTime being earlier than endTime,
 * a timespec will be returned with .tv_sec & .tv_nsec equal to zero.
 *
 * @param startTime is any timespec containing a time that should be earlier
 * than endTime.
 * @param endTime is any timespec containing a time that should be later than 
 * startTime.
 *
 * @return a timespec containing the difference between the two input timespecs.
 */
struct timespec elapsedTime(struct timespec startTime, struct timespec endTime)
{
  struct timespec deltaTime;

  // If startTime is after endTime, return zero for .tv_sec & .tv_nsec.
  if(((endTime.tv_sec - startTime.tv_sec) < 0) ||
     (((endTime.tv_sec - startTime.tv_sec) == 0) &&
      ((endTime.tv_nsec - startTime.tv_nsec) < 0)) )
    {
      deltaTime.tv_sec = 0;
      deltaTime.tv_nsec = 0;

      LOGW("Tried to find elapsed time where end time was *earlier* than start "
	   "time.\n" );
    }
  else
    {
      // If endTime's .tv_nsec is smaller than startTime's, perform a borrow.
      if((endTime.tv_nsec - startTime.tv_nsec) < 0)
	{
	  deltaTime.tv_sec = endTime.tv_sec - startTime.tv_sec - 1;
	  deltaTime.tv_nsec = 1000000000 + endTime.tv_nsec - startTime.tv_nsec;
	}
      else
	{
	  deltaTime.tv_sec = endTime.tv_sec - startTime.tv_sec;
	  deltaTime.tv_nsec = endTime.tv_nsec - startTime.tv_nsec;
	}
    }

  return deltaTime;
}

/** This function finds the difference, in decimal seconds, between two given
 * times.
 *
 * Warning: If function is used with startTime being earlier than endTime, if
 *          the endTime is earlier a value of 0.0 is returned.
 *
 * @param startTime is any timespec containing a time that should be earlier
 * than endTime.
 * @param endTime is any timespec containing a time that should be later than 
 * startTime.
 *
 * @return a double containing the difference between the two input timespecs.
 */
double elapsedTimeSec(struct timespec startTime, struct timespec endTime)
{
  struct timespec deltaTime = elapsedTime(startTime, endTime);

  return deltaTime.tv_sec + ((double)deltaTime.tv_nsec / 1000000000L);
}

/** This function finds the difference, in decimal seconds, since the given
 * start time.
 *
 * @param startTime is a timespec containing to be used as the initial time.
 *
 * @return a double containing number of seconds since start time.
 */
double elapsedTimeSinceSec(struct timespec startTime)
{
  struct timespec now;
  currentTimeSpec(&now);
  
  return elapsedTimeSec(startTime, now);
}

/** Return a list of tokens in str separated by delimiters in delim
 *
 * @param str is the string to be tokenized.
 * @param delim is a string each of whose characters is a separator.
 * @param tokens is a vector of resultant tokens (clear before calling!).
 */
void tokenize(const std::string str, const std::string delim,
              std::vector<std::string> &tokens)
{
  size_t start = str.find_first_not_of(delim);
  size_t end = start;

  while (start != std::string::npos)
    {
      // Find next occurrence of delimiter
      end = str.find_first_of(delim, start);
      // Push back the token found into vector
      tokens.push_back(str.substr(start, end - start));
      // Skip all occurrences of the delimiter to find new start
      start = str.find_first_not_of(delim, end);
    }
}

/** Converts input degrees into the stage pixel equivalent.
 *
 * @param degrees is an integer that will be converted to stage pixels.
 *
 * @return the integer number of stage pixels corresponding to the input 
 * number of degrees.
 */
int degToStagePix(int degrees)
{
  return (int)(degrees * ((float)OUTPUT_WIDTH / STAGE_DEG_WIDTH));
}

/** Converts input stage pixels into degrees.
 *
 * @param stagePix is an integer that will be converted to degrees.
 *
 * @return a float representing the number of degrees that equal the input
 * number of stage pixels.
 */
float stagePixToDeg(int stagePix)
{
  return (stagePix * (STAGE_DEG_WIDTH / (float)OUTPUT_WIDTH));
}

/** Print an ASCII bar graph to help visualize data.
 *
 * @param pLabel is a heading label for the block of data.
 * @param pData is a pointer to the data array.
 * @param nData is the number of data points to show.
 * @param scale is a scale factor to divide values by when drawing the
 * bar graph.
 */
void printBarGraph(const char *pLabel, int *pData, int nData, float scale)
{
  int   i, n, val;
  const  int  kMaxStrLen = 150;
  static char pStr[kMaxStrLen], pBarLabel[kMaxStrLen];

  // print blank line then the label
  LOGD("\n");
  LOGD("===== %s =====\n", pLabel);

  // plot the data
  for (i=0; i<nData; i++)
    {
      val = pData[i];
      n = (float) abs(val) / scale;
      snprintf(pBarLabel, kMaxStrLen, "%2d %10d: ", i, val);
      if (val > 0)
	sprintBar(pStr, kMaxStrLen, pBarLabel, '+', n);
      else
	sprintBar(pStr, kMaxStrLen, pBarLabel, '-', n);
      LOGD("%s\n", pStr);
    }
}

/** This function is a helper for creating ASCII bar graphs.
 *
 * @param pDst points to the destination string that holds the resulting bar.
 * @param maxLen is the maximum length allowed for the destination string.
 * @param pLabel points to a label that is copied to the beginning of pDst.
 * @param barChar is the character used to draw the bar.
 * @param barLen is the number of times to draw the bar character.
 */
void sprintBar(char *pDst, size_t maxLen, char *pLabel, char barChar, int barLen)
{
  int   n, len;

  // copy label to destination string
  snprintf(pDst, maxLen, "%s: ", pLabel);
  // calculate bar length, respecting maximum length of destination string
  len = strlen(pDst);
  if (barLen < maxLen - len)
    n = barLen;
  else
    n = maxLen - len - 1;
  // make the bar and terminate the string
  memset(&pDst[len], barChar, n);
  pDst[len + n] = '\0';
}

/** This function splits a string into separate lines.
 *
 * @param str the string to split.
 *
 * @return a vector where each element is a single line of the string.
 */
std::vector<std::string> splitStringLines(const std::string &str)
{
  std::stringstream ss(str);
  std::vector<std::string> lines;
  std::string token;
  
  while(std::getline(ss, token, '\n'))
    { lines.push_back(token); }

  return lines;
}

/** Iteratively make directories as necessary to create entire path.
 *
 * @param pDir full or relative path of directory to create.
 * @param mode is the new directory's access rights value. (Usually combination
 * of OR'd 'S_Ixxxx' values from <stat.h>.)
 *
 * @return  0 - success
 *         -1 - directory path too long
 *         -2 - attempt to create a directory failed
 *         -3 - element of the directory path is not a directory node
 *         -4 - could not change directory permissions
 */
int mkdirWithParents(const char *pDir, const mode_t mode)
{
  char tmp[PATH_MAX_STRING_SIZE];
  char *p = NULL;
  struct stat sb;
  size_t len;

  /* copy path */
  strncpy(tmp, pDir, sizeof(tmp));
  len = strlen(tmp);
  if (len >= sizeof(tmp))
    { return -1; /* path too long */ }

  /* remove trailing slash */
  if(tmp[len - 1] == '/')
    { tmp[len - 1] = 0; }

  /* iterative mkdir */
  for(p = tmp + 1; *p; p++)
    {
      if(*p == '/')
        {
          *p = 0;
          /* test path */
          if (stat(tmp, &sb) != 0)
            {
              /* path does not exist - create directory */
              if (mkdir(tmp, mode) < 0)
                { return -2; /* could not create the directory */ }
            }
          else if (!S_ISDIR(sb.st_mode))
            { return -3; /* not a directory */ }
          else if ( ((mode & S_IRUSR) && (!(sb.st_mode & S_IRUSR)))
                 || ((mode & S_IWUSR) && (!(sb.st_mode & S_IWUSR))) )
            {
              /* not readable but should be, or not writable but should be */
              if (chmod(tmp, mode) < 0)
                { return -4; /* could not change the permissions */ }
            }
          *p = '/';
        }
    }
  /* test path */
  if (stat(tmp, &sb) != 0)
    {
      /* path does not exist - create directory */
      if (mkdir(tmp, mode) < 0)
        { return -2; /* could not create the directory */ }
    }
  else if (!S_ISDIR(sb.st_mode))
    { return -3; /* not a directory */ }
  else if ( ((mode & S_IRUSR) && (!(sb.st_mode & S_IRUSR)))
         || ((mode & S_IWUSR) && (!(sb.st_mode & S_IWUSR))) )
    {
      /* not readable but should be, or not writable but should be */
      if (chmod(tmp, mode) < 0)
        { return -4; /* could not change the permissions */ }
    }

  return 0;
}

/** Pre-pend the supplied folder path to the filename and return file's length.
 *
 * @param fullPath is a reference to a string to fill with the full file name
 * and path.
 * @param pPath is the path of the directory containing the file.
 * @param pFileName is the name of the file.
 *
 * @return the file's length, or -1 if the file was not found.
 */
int pathify(std::string &fullPath, char *pPath, const char *pFileName)
{
  struct stat statBuf;
  int rc;

  fullPath = std::string(pPath);
  if (fullPath[fullPath.length() - 1] != '/')
    {
      // Only add trailing '/' if not already there
      fullPath.append("/");
    }
  fullPath.append(pFileName);
  rc = stat(fullPath.c_str(), &statBuf);
  if (rc == 0)
    {
      rc = statBuf.st_size;
    }
  else
    {
      rc = -1;
    }
  return rc;
}

/** Make camel case string out of enumerated value symbol string.
 *
 * Converts enumeration symbol (underscore separated all-caps words)
 * into camel-case string.
 *
 * @param pDest is a pointer to the buffer to hold the resultant string.
 * @param pSrc is a pointer to the input string.
 * @param len is the max length that buffer can hold.
 *
 * @return length of resultant string.
 */
int convertEnumToCamelCase(char *pDest, const char *pSrc, int len)
{
  int destLen = 0;
  char *pDestCh = pDest;

  if (len > 0)
    {
      *pDestCh = isupper(pSrc[0]) ? tolower(pSrc[0]) : pSrc[0];
      pDestCh++;
    }
  for (int idx = 1; (idx < len) && (pSrc[idx] != 0); idx++)
    {
      if (pSrc[idx - 1] == '_')
        {
          *pDestCh = (islower(pSrc[idx])) ? toupper(pSrc[idx]) : pSrc[idx];
          pDestCh++;
        }
      else if (pSrc[idx] == '_')
        {
          continue;
        }
      else
        {
          *pDestCh = (isupper(pSrc[idx])) ? tolower(pSrc[idx]) : pSrc[idx];
          pDestCh++;
        }
    }

  return (int)(pDestCh - pDest);
}

/** Test routine for Linux platform to generate a short faux meeting.
 *
 * @return true if in a (fake) meeting.
 */
#ifndef __ANDROID__
static bool fakeMeeting(void)
{
  bool inMeeting = false;
  static struct timespec t0 = {0, 0};
  struct timespec tNow;
  double elapsed;

  if (t0.tv_nsec == 0 && t0.tv_sec == 0)
    { currentTimeSpec(&t0); }
  currentTimeSpec(&tNow);
  elapsed = elapsedTimeSec(t0, tNow);

  if (elapsed >= 3.0 && elapsed <= 43.0)
    { inMeeting = true; }

  return inMeeting;
}
#endif

/** Wrapper for libOwlPG function to return state of UVC stream.
 *
 * This wrapper is provided to allow smooth integration of Linux testing.
 *
 * @param pStreaming points to value set to true if streaming, false otherwise.
 */
void getUvcStreamStateWrapper(int *pStreaming)
{
#ifdef __ANDROID__
  extern int gUvcHandlerStreaming;

  *pStreaming = gUvcHandlerStreaming;
#else
  if (fakeMeeting())
    { *pStreaming = 1; }
  else
    { *pStreaming = 0; }
#endif
}

/** Start a timer for measuring time spent in a function.
 *
 * This function should be called immediately before a function whose
 * CPU usage is to be measured.
 *
 * @param pTimer is a pointer to the timer control structure. This should
 * point to a static instance of a funcTimer_t structure!
 * @param double reportingPeriod indicates how often results should be shown.
 */
void startFuncTimer(funcTimer_t *pTimer, double reportingPeriod)
{
  if ((pTimer->mFirstTime.tv_nsec == 0) && (pTimer->mFirstTime.tv_sec == 0))
    {
      currentTimeSpec(&pTimer->mFirstTime);
      pTimer->mBeforeTime = pTimer->mFirstTime;
      pTimer->mLastReportTime = pTimer->mFirstTime;
      pTimer->mTotalElapsedTime = 0.0;
      pTimer->mFuncElapsedTime = 0.0;
    }
  else
    {
      currentTimeSpec(&pTimer->mBeforeTime);
    }
  pTimer->mReportingPeriod = reportingPeriod;
}

/** Stops a timer for measuring time spent in a function.
 *
 * This function should be called immediately after a function whose
 * CPU usage is to be measured.
 *
 * @param pTimer is a pointer to the timer control structure.
 */
void stopFuncTimer(funcTimer_t *pTimer)
{
  currentTimeSpec(&pTimer->mAfterTime);
  pTimer->mTotalElapsedTime = elapsedTimeSec(pTimer->mFirstTime,
                                             pTimer->mAfterTime);
  pTimer->mFuncElapsedTime += elapsedTimeSec(pTimer->mBeforeTime,
                                             pTimer->mAfterTime);
}

/** Indicates if the desired reporting period has passed.
 *
 * Call this function after stopping the timer to see if it is now time
 * to report results.
 *
 * @param pTimer is a pointer to the timer control structure.
 *
 * @return true if it is now time to report a result.
 */
bool reportFuncTimer(funcTimer_t *pTimer)
{
  bool timeToReport = false;

  double timeSinceLastReport = elapsedTimeSec(pTimer->mLastReportTime,
                                              pTimer->mAfterTime);

  if (timeSinceLastReport >= pTimer->mReportingPeriod)
    {
      timeToReport = true;
    }

  return timeToReport;
}

/** Return the percent of CPU time used by the timed function.
 *
 * @param pTimer is a pointer to the timer control structure.
 *
 * @return percent of CPU time spent between start and stop since the first
 * time that start was called.
 */
double percentCpuFuncTimer(funcTimer_t *pTimer)
{
  double percent;

  percent = 100.0 * pTimer->mFuncElapsedTime / pTimer->mTotalElapsedTime;

  pTimer->mLastReportTime = pTimer->mAfterTime;

  return percent;
}
