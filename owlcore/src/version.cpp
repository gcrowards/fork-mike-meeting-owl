#include <string>

#ifdef __ANDROID__
#include "libOwlPG.h"
#endif

#include "logging.hpp"
#include "version.hpp"


/** This function checks the version info for the main application and
 * any custom libraries (currently libOwlPg.so).
 *
 * @return true if there's a version mismatch, false otherwise.
 */
bool checkVersion(void)
{
  bool ret = false;

  // log the meetingOwl version
  LOGI("meetingOwl version: %s.%s.%s\n",
       MEETING_OWL_VERSION_MAJOR,
       MEETING_OWL_VERSION_MINOR,
       MEETING_OWL_VERSION_BUILD);

#ifdef __ANDROID__
  // construct the expected libOwlPG.so version from the header contents
  std::string expectedVersion = LIBOWLPG_VERSION_MAJOR;
  expectedVersion = expectedVersion			\
    + "." + LIBOWLPG_VERSION_MINOR			\
    + "." + LIBOWLPG_VERSION_BUILD			\
    + "." + LIBOWLPG_VERSION_TIMESTAMP;

  // query libOwlPG.so for its actual version
  std::string actualVersion = getLibOwlPgVersion();

  // now compare
  if (actualVersion != expectedVersion)
    {
      // version mismatch; log the error and change the return value
      LOGE("libOwlPG version mismatch; expected %s, found %s\n",
	   expectedVersion.c_str(), actualVersion.c_str());
      ret = true;
    }
  else
    {
      // versions match, so log actual version
      LOGI("libOwlPG version: %s\n", actualVersion.c_str());
    }
#endif // __ANDROID__

  return ret;
}
