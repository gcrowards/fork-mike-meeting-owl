#include <cstdlib>
#include <cmath>
#include <algorithm>

#include "faceDetector.hpp"
#include "logging.hpp"
#include "params.h"

#include "common.hpp"


cFaceDetector::cFaceDetector(void)
{
#if defined(__ANDROID__) && !defined(OPENCV_FACES)
  // set up the Qualcomm face detector
  if (qcff_create(&mQcffContext) != QCFF_RET_SUCCESS)
    {LOGE("unable to create qcff context\n");}
  else
    {
      if (qcff_set_mode(mQcffContext, QCFF_MODE_STILL) != QCFF_RET_SUCCESS)
	{
	  LOGE("unable to set qcff mode\n");
	  if(qcff_destroy(&mQcffContext) != QCFF_RET_SUCCESS)
	    {LOGE("unable to destroy qcff context\n");}
	}
    }
  // allocate max sized grayscale buffer for the QC face detector
  mpQcffGrayImage = (unsigned char *) malloc(CV_PANO_RES_X * CV_PANO_RES_Y);
  // initialize the width and height of search image
  mQcffWidth = mQcffHeight = 0; // values are updated in detectFacesQc()
#else // linux or OPENCV_FACES
  // set up OpenCV face detector
  if(!mFaceCascadeClassifier.load(FACE_DETECTOR_MODEL))
    {
      LOGE("--(!)Error load face cascade\n");
      std::exit(1);
    }
#endif
}

cFaceDetector::~cFaceDetector()
{
#if defined(__ANDROID__) && !defined(OPENCV_FACES)
  // clean up the Qualcomm face detector
  if(qcff_destroy(&mQcffContext) != QCFF_RET_SUCCESS)
    {LOGE("unable to destroy qcff context\n");}
  free(mpQcffGrayImage);
#endif
}

/** Assign the boundary in the image for the detector to operate within.
 *
 * NOTE: The whole image is searched for faces.  This parameter is used when
 *       doing things like combining faces that are close to each other into
 *       one "face."  I'm not enitrely sure why the resolution of the image 
 *       isn't used instead...
 *
 * @param spaceSize is an OpenCV Size that specifies the width and height over
 * which some Detector operators work.
 */
void cFaceDetector::setSpaceSize(cv::Size spaceSize)
{
  mSpaceSize = spaceSize;
}

/** Returns the simpified list of faces that were found the last time the 
    detector was run. */
std::vector<cv::Rect> cFaceDetector::getFaces(void)
{
  return mFaces;
}

/** This function runs the Face Detector on the provided image and returns the
 * results.
 *
 * @param pImage is a pointer to an OpenCV Mat that contains the image data to 
 * search.
 *
 * @return a vector of rectangles that represent the faces that were detected.
 * Some of these rectangles may represent faces that were very close to one
 * another and thus combined into one "face."  The unmodified set of faces can
 * get retieved by calling getRawFaces().
 */
std::vector<cv::Rect> cFaceDetector::getFaces(cv::Mat *pImage)
{
  detectFaces(pImage);
  return mFaces;
}

/** Returns the list of all faces that were found the last time the detector
    was run. */
std::vector<cv::Rect> cFaceDetector::getRawFaces(void)
{
  return mRawFaces;
}

/** This function is the heart of the Face Detector; call it to run the dector
 * on the provided image.
 *
 * A cascade classifier is used to find faces in the image.  Each face is 
 * defined by a rectangle that dictates the detected position and size.  If 
 * multiple faces are found in very close proximity to one another then a single
 * rectangle is used to represent the position and bounds of the cluster.  If 
 * the unclustered faces are needed, they can be retrived by calling 
 * getRawFaces().
 *
 * @param pImage is a pointer to an OpenCV Mat containing the image to be 
 * searched.
 *
 * @return a vector of OpenCV rectangles that each represent the size and 
 * location of a found faces (or cluster of faces that were detected very close
 * to one another).
 */
std::vector<cv::Rect> cFaceDetector::detectFaces(cv::Mat *pImage)
{
#if defined(__ANDROID__) && !defined(OPENCV_FACES)
  // detect faces with Qualcomm libfacialproc
  detectFacesQc(*pImage, mRawFaces);
#else // linux or OPENCV_FACES
  // detect faces with OpenCV
  mFaceCascadeClassifier.detectMultiScale(*pImage, mRawFaces, 1.1, 2, 
					  0|cv::CASCADE_SCALE_IMAGE, 
					  cv::Size(30,30) );
#endif
  
  // combine found faces that are very close to one another (into mFaces)
  simplifyFaces();
  
  return mFaces;
}

/** This function implements the Qualcomm face detector; see also
 * cFaceDetector::detectFaces().
 *
 * @param pImage is a pointer to an OpenCV Mat containing the image to be 
 * searched.
 * @param faces is a vector of OpenCV rectangles that store the detected faces.
 */
void cFaceDetector::detectFacesQc(cv::Mat& image, std::vector<cv::Rect>& faces)
{
#if defined(__ANDROID__) && !defined(OPENCV_FACES)
  int           w, h;
  qcff_config_t qcffConfig;
  uint32_t      numFaces, numFacesReturned;

  // clear the faces vector
  faces.clear();

  // get dimensions of sub-image
  w = image.cols;
  h = image.rows;

  // re-pack the data into the member buffer
  for (long y=0; y<h; y++)
    { memcpy(&mpQcffGrayImage[y * w], image.ptr(y), w); }
  
  // configure context image size, if changed
  // Note: if we try to configure every time rather than just when the
  // image size changes, the application stops spontaneously with
  // "Killed" printed to the adb shell
  if (mQcffWidth != w || mQcffHeight != h)
    {
      qcffConfig.width = mQcffWidth = w;
      qcffConfig.height = mQcffHeight = h;
      // qcff_config_t also has a member downscale_factor, although it's
      // only documentation says it's experimental. Leave it at its
      // default value Might want to set this to 0, although either way
      // doesn't seem to make a difference with the results.
      //qcffConfig.downscale_factor = 0;
      if (qcff_config(mQcffContext, &qcffConfig) != QCFF_RET_SUCCESS)
	{
	  LOGE("unable to set qcff width,height\n");
	  return;
	}
      else if (DEBUG(FACE_DETECTOR))
	{ LOGI("configured qcff context for %dx%d images\n", w, h); }
    }

  // set the image data
  if (qcff_set_frame(mQcffContext, mpQcffGrayImage) != QCFF_RET_SUCCESS)
    {
      LOGE("unable to set qcff frame\n");
      return;
    }
  
  // check for faces
  if (qcff_get_num_faces(mQcffContext, &numFaces) != QCFF_RET_SUCCESS)
    {
      LOGE("unable to get qcff num_faces\n");
      return;
    }

  // process faces
  if (numFaces > 0)
    {
      // clear the complete info object to 0 (every member is
      // a pointer, which should be NULL by default)
      qcff_complete_face_info_t completeInfo;
      memset((void*)&completeInfo, 0,
	     sizeof(qcff_complete_face_info_t));
      
      // only query the location of each face (as
      // rectangles). Leave the rest of the info that can be
      // queried as NULL so QCFF context won't calculate it.
      completeInfo.p_rects = new qcff_face_rect_t[numFaces];
      
      // set an array of indexes to query all detected faces
      uint32_t *pFaceIndices = new uint32_t[numFaces];
      for(int i=0; i<numFaces; i++)
	{pFaceIndices[i] = i;}
      
      // get the complete info for the specified faces
      qcff_get_complete_info(mQcffContext, numFaces, pFaceIndices,
			     &numFacesReturned, &completeInfo);
      
      // store the bounding boxes
      for(int i=0; i<numFaces; i++)
	{
	  qcff_rect_t *pBB;
	  pBB = &(completeInfo.p_rects[i].bounding_box);
	  faces.push_back( cv::Rect( pBB->x, pBB->y,
				     pBB->dx, pBB->dy ) );
	}
      
      // clean up
      delete[] pFaceIndices;
      delete[] completeInfo.p_rects;
    }
#endif // __ANDROID__ && !OPENCV_FACES
}

/** This function scaled up the provided faces to match the raw camera image.
 *
 * Faces are search for in a scaled-down version of the image.  This function
 * is a convenience if you want to get the faces back to the scale of the 
 * original image.
 *
 * @param faces is a vector of OpenCV rectangles that represent faces that are
 * to be scaled up.
 * @param scaleFactor is the amount by which to scale the images.  Typically
 * this will be the SCALE macro set in params.h.
 */
std::vector<cv::Rect> cFaceDetector::scaleFaces(std::vector<cv::Rect> faces, 
						int scaleFactor )
{
  // scale faces up to match raw camera image size
  for(std::vector<cv::Rect>::iterator iFace = faces.begin();
      iFace != faces.end(); ++iFace )
    {
      (*iFace).x *= scaleFactor;
      (*iFace).y *= scaleFactor;
      (*iFace).width *= scaleFactor;
      (*iFace).height *= scaleFactor;
    }
  return faces;
}

/** This function manages the process of combining found faces that are in very
 * close proximity into one representation.
 *
 * See compareFaces() for the details on how the decision to combine faces is
 * made.  See combineFaces() for the details on how faces are merged.
 * 
 * TODO: TEST THIS ALGORITHM THOROUGHLY!
 */
void cFaceDetector::simplifyFaces(void)
{
  bool lastFaceUsed = false;

  mFaces.clear();

  if(mRawFaces.size() > 1)
    {
      for(std::vector<cv::Rect>::size_type i = 0; i != mRawFaces.size() - 1; i++)
	{
	  cv::Rect f1 = mRawFaces[i];
	  
	  for(std::vector<cv::Rect>::size_type j = i + 1; 
	      j != mRawFaces.size(); j++ )
	    {
	      if(compareFaces(f1, mRawFaces[j]))
		{
		  f1 = combineFaces(f1, mRawFaces[j]);

		  if(j == (mRawFaces.size() - 1)) 
		    {lastFaceUsed = true;}
		}
	    }	  
	  mFaces.push_back(f1);
	}

      if(!lastFaceUsed)
	{mFaces.push_back(mRawFaces.back());}
    }
  else
    {mFaces = mRawFaces;}
}

/** This function compares two rectangles that represent a pair of detected faces
 * and determines if they are sufficiently close to one another to be 
 * represented by a single rectangle.
 *
 * @param f1 is an OpenCV rectangle representing one face.
 * @param f2 is an OpenCV rectangle representing another face.
 *
 * @return true if the faces should be merged.
 */
bool cFaceDetector::compareFaces(cv::Rect f1, cv::Rect f2)
{
  // TODO: make face class that has center pre-computed
  cv::Point c1;
  c1.x = f1.x + (f1.width/2);
  c1.y = f1.y + (f1.height/2);

  cv::Point c2;
  c2.x = f2.x + (f2.width/2);
  c2.y = f2.y + (f2.height/2);

  // TODO: put this stuff in a common file (it's used in cFaceTracker too!)
  int dx = std::min(mSpaceSize.width - c1.x + c2.x,
		    mSpaceSize.width + c1.x - c2.x );
  dx = std::min(dx, std::abs(c1.x - c2.x));
  
  float distance = std::sqrt(std::pow(dx, 2) + 
			     std::pow((c1.y - c2.y), 2) );
  
  // TODO: find optimal value for this threshold test!
  if(distance < std::max(f1.width, f2.width))
    {return true;}
  else
    {return false;}
}

/** This function takes two rectangles that represent a pair of faces that are
 * to be combined into a single rectangle that embodies both faces.
 *
 * The combined rectangle is sized and positioned such that it completely
 * encompasses both of the original rectangles.
 *
 *  NOTE: This function assumes faces are side-by-side, not one on top of the 
 *        other.
 *
 * @param f1 is an OpenCV rectangle representing one face.
 * @param f2 is an OpenCV rectangle representing another face.
 *
 * @return a rectangle representing the merger of the two faces. 
 */
cv::Rect cFaceDetector::combineFaces(cv::Rect f1, cv::Rect f2)
{
  // TODO: Consider better way to combine faces!

  cv::Rect combinedFace;
  cv::Rect leftFace = f1;
  cv::Rect rightFace = f2;

  if(f2.x < f1.x)
    {
      leftFace = f2;
      rightFace =f1;
    }

  if((mSpaceSize.width - (rightFace.x + rightFace.width)) < leftFace.x)
    {
      combinedFace.x = leftFace.x;
    }
  else
    { // wrap around 360 (right face is closer to 360 degree mark than left
      // face is to 0 degree mark)
      combinedFace.x = rightFace.x;
    }

  combinedFace.y = std::min(f1.y, f2.y);
  combinedFace.width = f1.width + f2.width;
  combinedFace.height = std::max(f1.height, f2.height);
  combinedFace.height = std::min(combinedFace.height, mSpaceSize.height);
  // NOTE: This all assumes faces are side-by-side (not one on top of the other)

  return combinedFace;
}
