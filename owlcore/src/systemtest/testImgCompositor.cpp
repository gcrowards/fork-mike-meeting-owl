/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  testImgCompositor.cpp
 *  This is the implementation file for cTestImgCompositor.
 *   --> Statically handles rendering system test results to an image for
 *        display.
 */

#include "logging.hpp"
#include "systemtest/testImgCompositor.hpp"

#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"

#include <unistd.h>

//
const int cTestImgCompositor::mkPadding      = 16;
const int cTestImgCompositor::mkInnerPadding = 6;
const int cTestImgCompositor::mkPanoHeight   = CV_PANO_RES_Y * OUT_PANO_SCALE;
const int cTestImgCompositor::mkOutlineThickness = 2;
const cv::Size cTestImgCompositor::mkTextOffset =
  cv::Size(mkInnerPadding + mkOutlineThickness,
	   mkInnerPadding + mkOutlineThickness );
const int cTestImgCompositor::mkFontFace = cv::FONT_HERSHEY_DUPLEX;
const int cTestImgCompositor::mkFontThickness = 1;
const double cTestImgCompositor::mkFontScale  = 0.8;

const cv::Scalar cTestImgCompositor::mkBadColor    (200, 50, 50, 255);   //RED
const cv::Scalar cTestImgCompositor::mkGoodColor   (50, 200, 50, 255);   //GREEN
const cv::Scalar cTestImgCompositor::mkNeutralColor(200, 200, 200, 255); //WHITE
const cv::Scalar cTestImgCompositor::mkAttnColor   (255, 255, 100, 255); //YELLOW
const cv::Scalar cTestImgCompositor::mkDarkColor   (150, 150, 150, 255); //GREY
const cv::Scalar cTestImgCompositor::mkBgColor     (50, 50, 50, 255); //DARK GREY
const cv::Scalar cTestImgCompositor::mkTestingColor(255, 255, 20, 255);  //YELLOW

/** Finds the size of the text if drawn.
 *
 * @param text the drawn text.
 * @param pBaselineOut if non-null, stores the baseline as returned by putText().
 * @return the size of the text (pixels).
 */
cv::Size cTestImgCompositor::textSize(const std::string &text, int *pBaselineOut)
{
  int baseline = 0;
  cv::Size size = cv::getTextSize(text, mkFontFace, mkFontScale,
				  mkFontThickness, &baseline );
  if(pBaselineOut)
    { *pBaselineOut = baseline; }
  
  return size;
}

/** Renders a string of text on the given image.
 *
 * @param img is the target image to render on.
 * @param pos is the position of the text origin.
 * @param text is the string to render.
 * @param color is the color to render in.
 * @return the size of the text drawn.
 */
cv::Size cTestImgCompositor::drawText(cv::Mat &img, cv::Point pos,
				      const std::string &text,
				      cv::Scalar color )
{
  // move origin to the upper-left corner
  cv::Size size = textSize(text);
  pos.y += size.height;
  cv::putText(img, text, pos, mkFontFace, mkFontScale, color, mkFontThickness,
	      CV_AA );
  return size;
}

/** Renders multiple lines of text on the given image.
 *
 * @param img is the target image to render on.
 * @param pos is the position of the first line's text origin.
 * @param lines is the list of strings to render one per line.
 * @param color is the color to render in.
 * @return the total size of the text drawn.
 */
cv::Size cTestImgCompositor::drawTextLines(cv::Mat &img, cv::Point pos,
					   const std::vector<std::string> &lines,
					   cv::Scalar color )
{
  //Display each line one below the next.
  cv::Size totalSize(0, 0);
  for(auto &line : lines)
    {
      cv::Size size = drawText(img, pos, line,
			       cTestImgCompositor::mkTestingColor );
      pos.y += size.height + mkInnerPadding;
      totalSize.width = std::max(totalSize.width, size.width);
      totalSize.height += size.height + mkInnerPadding;
    }
  return totalSize;
}

/** Renders a generic test status/result.
 *
 * @param img is the target image to render on.
 * @param pos is the position of the test result.
 * @param testName is a string to print as the test's name.
 * @param result is the current status of this test.
 * @return the size of the test box.
 */
cv::Size cTestImgCompositor::drawTest(cv::Mat &img, cv::Point pos,
				      const std::string &testName,
				      testResult_t result,
				      std::string extraText )
{
  // choose a color and label based on the result.
  cv::Scalar color = mkDarkColor;
  std::string resultStr;
  switch(result)
    {
    case eTestUnknown:
      color = mkDarkColor;
      resultStr = "   UNKNOWN";
      break;
    case eTestPassed:
      color = mkGoodColor;
      resultStr = "   PASS";
      break;
    case eTestFailed:
      color = mkBadColor;
      resultStr = "   FAIL";
      break;
    case eTestRunning:
      color = mkTestingColor;
      resultStr = "   TESTING...";
      break;
    case eTestInactive:
      color = mkNeutralColor;
    default:
      resultStr = "";
    }
  
   drawText(img, pos, testName + ":", color);
   pos.x += 250;
   cv::Size totalSize = drawText(img, pos, resultStr + (extraText.empty() ?
							" " :
							" (" + extraText + ")" ),
				 color );
   totalSize.width += 250;
   return totalSize;
}

/** Renders a measure bar for a displaying a mic level.
 *
 * @param img is the target image to render on.
 * @param pos is the position of the measure bar.
 * @param size is the size of the measure bar.
 * @param testActive denotes whether this is the test currently running.
 */
void cTestImgCompositor::drawMeasureBar(cv::Mat &img, cv::Point pos,
					cv::Size size, double fill )
{
  // draw bar background
  cv::rectangle(img, cv::Rect(pos, size), mkNeutralColor, CV_FILLED);
  // draw measurement fill
  if(fill > 0.0 && fill <= 1.0)
    {
      cv::Size fillSize(size.width * fill, size.height);
      cv::rectangle(img, cv::Rect(pos, fillSize), mkGoodColor, CV_FILLED );
    }
  // draw outline
  cv::rectangle(img, cv::Rect(pos, size), mkTestingColor,
		mkOutlineThickness, 8 );
}

/** Renders a border for a test.
 *
 * @param img is the target image to render on.
 * @param pos is the position of the outline box.
 * @param size is the size of the outline box.
 * @param testActive denotes whether this is the test currently running.
 */
void cTestImgCompositor::drawOutline(cv::Mat &img, cv::Point pos,
				     cv::Size size, bool testActive )
{
  size += cv::Size(mkOutlineThickness, mkOutlineThickness);
  cv::rectangle(img, cv::Rect(pos, size),
		(testActive ? mkTestingColor : mkNeutralColor),
		mkOutlineThickness, 4 );
}

/** Renders the prompt.
 *
 * @param img is the target image to render on.
 * @param pos is the position of the prompt.
 * @param textLines is lines of text to render as the prompt.
 * @param boxSize is an override for size in either dimension, if non-zero. 
 * @return the size of the prompt box.
 */
cv::Size cTestImgCompositor::drawPrompt(cv::Mat &img, cv::Point pos,
					const std::vector<std::string> &textLines,
					cv::Size boxSize )
{
  //Draw prompt lines
  cv::Point textPos = pos + cv::Point(mkTextOffset);
  cv::Size size = drawTextLines(img, textPos, textLines, mkAttnColor);
  size += mkTextOffset * 2;
  if(boxSize.width > 0)
    { size.width = boxSize.width; }
  if(boxSize.height > 0)
    { size.height = boxSize.height; }
  
  //Draw border
  drawOutline(img, pos, size, true);
  return size;
}

/** Renders mic test results.
 *
 * @param img is the target image to render on.
 * @param pos is the position of the test result.
 * @param micLevels is a list of the levels for each mic.
 * @param boxSize is an override for size in either dimension, if non-zero.
 * @return the size of the test box.
 */
cv::Size cTestImgCompositor::drawMicTest(cv::Mat &img, cv::Point pos, 
					 const std::array<double, N_MICS> &micLevels,
					 cv::Size boxSize )
{
  int kBarOffset = 100;
  
  // draw mic labels
  cv::Point linePos = pos + cv::Point(mkTextOffset);
  cv::Size totalSize(std::max(boxSize.width, 300), 0);
  std::stringstream testNameSs;
  cv::Size lineSize(0, 0);
  for(int iMic = 0; iMic < N_MICS; iMic++)
    {
      testNameSs << "MIC " << iMic;
      cv::Size size = drawText(img, linePos, testNameSs.str(), mkTestingColor);

      linePos.y += size.height + mkPadding;
      if(iMic == 0)
	{ lineSize.height = size.height; }
      totalSize.width = std::max(totalSize.width, size.width);
      totalSize.height += size.height + mkPadding;
      
      testNameSs.clear();
      testNameSs.str("");
    }
  // draw level measure bars
  lineSize.width = totalSize.width - kBarOffset - 2 * mkTextOffset.width;
  linePos = cv::Point(pos.x + kBarOffset, pos.y) + cv::Point(mkTextOffset);
  for(int iMic = 0; iMic < N_MICS; iMic++)
    {
      drawMeasureBar(img, linePos, lineSize, micLevels[iMic]);
      linePos.y += lineSize.height + mkPadding;
    }		     
  totalSize += mkTextOffset * 2;
  totalSize.height -= mkPadding;
  if(boxSize.width > 0)
    { totalSize.width = boxSize.width; }
  if(boxSize.height > 0)
    { totalSize.height = boxSize.height; }
  
  // draw border
  drawOutline(img, pos, totalSize, true);
  return totalSize;
}

/** Renders speaker test results.
 *
 * @param img is the target image to render on.
 * @param pos is the position of the test result.
 * @param result is the current status of the speaker test.
 * @param active denotes whether this is the test currently running.
 * @param boxSize is an override for size in either dimension, if non-zero.
 * @return the size of the test box.
 */
cv::Size cTestImgCompositor::drawSpeakerTest(cv::Mat &img, cv::Point pos,
					     testResult_t result, bool active,
					     cv::Size boxSize )
{
  //Draw results
  cv::Point textPos = pos + cv::Point(mkTextOffset);
  cv::Size size = drawTest(img, textPos, "SPEAKER", result);
  size += mkTextOffset * 2;
  if(boxSize.width > 0)
    { size.width = boxSize.width; }
  if(boxSize.height > 0)
    { size.height = boxSize.height; }
  
  //Draw border
  drawOutline(img, pos, size, active);
  return size;
}

/** Renders wifi test results.
 *
 * @param img is the target image to render on.
 * @param pos is the position of the test result.
 * @param result is the current status of the wifi test.
 * @param active denotes whether this is the test currently running.
 * @param stationStr the name of the strongest wifi signal station.
 * @param boxSize is an override for size in either dimension, if non-zero.
 * @return the size of the test box.
 */
cv::Size cTestImgCompositor::drawWifiTest(cv::Mat &img, cv::Point pos,
					  testResult_t result, bool active,
					  const std::string &stationStr,
					  cv::Size boxSize )
{
  //Draw test
  cv::Point textPos = pos + cv::Point(mkTextOffset);
  cv::Size size = drawTest(img, textPos, "WIFI", result, stationStr);
  size += mkTextOffset * 2;
  if(boxSize.width > 0)
    { size.width = boxSize.width; }
  if(boxSize.height > 0)
    { size.height = boxSize.height; }
  
  //Draw border
  drawOutline(img, pos, size, active);
  return size;
}

/** Renders bluetooth test results.
 *
 * @param img is the target image to render on.
 * @param pos is the position of the test result.
 * @param result is the current status of the bluetooth test.
 * @param active denotes whether this is the test currently running.
 * @param boxSize is an override for size in either dimension, if non-zero.
 * @return the size of the test box.
 */
cv::Size cTestImgCompositor::drawBtTest(cv::Mat &img, cv::Point pos,
					testResult_t result, bool active,
					cv::Size boxSize )
{
  //Draw test
  cv::Point textPos = pos + cv::Point(mkTextOffset);
  cv::Size size = drawTest(img, textPos, "BLUETOOTH", result);
  size += mkTextOffset * 2;
  if(boxSize.width > 0)
    { size.width = boxSize.width; }
  if(boxSize.height > 0)
    { size.height = boxSize.height; }
  
  //Draw border
  drawOutline(img, pos, size, active);
  return size;
}

/** Renders button test results.
 *
 * @param img is the target image to render on.
 * @param pos is the position of the test result.
 * @param buttonResults is a list of the current statuses of each button test.
 * @param active denotes whether this is the test currently running.
 * @param boxSize is an override for size in either dimension, if non-zero.
 * @return the size of the test box.
 */
cv::Size cTestImgCompositor::drawButtonTest(cv::Mat &img, cv::Point pos,
					    const std::array<testResult_t, N_BUTTONS>
					    &buttonResults, bool active,
					    cv::Size boxSize )
{
  static const std::array<std::string, N_BUTTONS> kButtonNames =
    { "FRONT MUTE", "VOLUME UP", "VOLUME DOWN",
      "REAR MUTE", "OWL BUTTON" };
  
  //Draw test
  cv::Point textPos = pos + cv::Point(mkTextOffset);
  cv::Size totalSize(0, 0);
  for(int iBtn = 0; iBtn < N_BUTTONS; iBtn++)
    {
      cv::Size size = drawTest(img, textPos, kButtonNames[iBtn],
			       buttonResults[iBtn] );
      textPos.y += size.height + mkInnerPadding;
      totalSize.width = std::max(totalSize.width, size.width);
      totalSize.height += size.height + mkInnerPadding;
    }
  totalSize += mkTextOffset * 2;
  totalSize.height -= mkInnerPadding;
  if(boxSize.width > 0)
    { totalSize.width = boxSize.width; }
  if(boxSize.height > 0)
    { totalSize.height = boxSize.height; }
  
  //Draw border
  drawOutline(img, pos, totalSize, active);
  return totalSize;
}

/** Renders led test results.
 *
 * @param img is the target image to render on.
 * @param pos is the position of the test result.
 * @param ledResult is the current status of the led test.
 * @param active denotes whether this is the test currently running.
 * @param boxSize is an override for size in either dimension, if non-zero.
 * @return the size of the test box.
 */
cv::Size cTestImgCompositor::drawLedTest(cv::Mat &img, cv::Point pos,
					   testResult_t ledResult, bool active,
					   cv::Size boxSize )
{
  //Draw test
  cv::Point textPos = pos + cv::Point(mkTextOffset);
  cv::Size size = drawTest(img, textPos, "LEDs", ledResult);
  size += mkTextOffset * 2;
  if(boxSize.width > 0)
    { size.width = boxSize.width; }
  if(boxSize.height > 0)
    { size.height = boxSize.height; }
  
  //Draw border
  drawOutline(img, pos, size, active);
  return size;
}
