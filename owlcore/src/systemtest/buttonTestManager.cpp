/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  systemtest/buttonTestManager.cpp
 *  This is the implementation file for cButtonTestManager.
 *   --> Handles the button testing for cSystemTest.
 *       Waits for each button to be pressed -- marks a pass if the correct
 *        button was pressed, or fail if a different button was pressed.
 */

#include "systemtest/buttonTestManager.hpp"


const std::array<buttonType_t, N_BUTTONS> cButtonTestManager::mkButtonIds =
  { BUTTON_MUTE, BUTTON_VOLUP, BUTTON_VOLDN,
    BUTTON_MUTE, BUTTON_OWL };

cButtonTestManager::cButtonTestManager()
{
  // initialize test results array
  mResults.fill(eTestInactive);
}

/** Starts the button tests. */
void cButtonTestManager::startTesting()
{
  if(mCurrentTest == eButtonTestIdle)
    {
      mCurrentTest = (buttonTest_t)(mCurrentTest + 1);
      mResults[mCurrentTest] = eTestRunning;
    }
}

/** Resets the button tests and results. */
void cButtonTestManager::reset()
{
  mResults.fill(eTestInactive);
  mCurrentTest = eButtonTestIdle;
}

/** Called to notify the button test when a button is clicked.
 *
 * @param button the button(s) that were clicked.
 * @return whether the button tests have completed.
 */
bool cButtonTestManager::update(buttonType_t button)
{
  if(mCurrentTest > eButtonTestIdle && !isComplete())
    {
      if(button & mkButtonIds[mCurrentTest])
	{ // correct button pressed -- mark test as passed
	  mResults[mCurrentTest] = eTestPassed;
	}
      else
	{ // incorrect button pressed -- mark test as failed
	  mResults[mCurrentTest] = eTestFailed;
	}
      
      // increment the test step
      mCurrentTest = (buttonTest_t)(mCurrentTest + 1);
      if(mCurrentTest < N_BUTTONS)
	{ mResults[mCurrentTest] = eTestRunning; }
    }

  // return whether button testing is complete.
  return isComplete();
}

/** @return whether the button tests are active. */
bool cButtonTestManager::isTesting() const
{
  return (mCurrentTest != eButtonTestIdle && !isComplete());
}

/** @return whether the button tests have finished, with complete results. */
bool cButtonTestManager::isComplete() const
{
  return (mCurrentTest == eButtonTestComplete);
}

/** @return the button test results. */
std::array<testResult_t, N_BUTTONS> cButtonTestManager::getResults() const
{
  return mResults;
}
