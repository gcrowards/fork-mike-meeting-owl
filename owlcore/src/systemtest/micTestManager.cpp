/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  systemtest/micTestManager.cpp
 *  This is the implementation file for cMicTestManager.
 *   --> Controls the mic testing for cSystemTest.Calibration values are defined
 *        in micTestManager.cpp.
 */

#include "logging.hpp"
#include "shellProcess.hpp"
#include "systemtest/micTestManager.hpp"

#include <iomanip>
#include <fstream>
#include <sstream>

const std::string cMicTestManager::mkConfigFilePath =
  "/system/media/OwlLabs/micTest-system.conf";
const std::string cMicTestManager::mkResultsOutputPath =
  "/data/OwlLabs/micTest-system-data.csv";

// mic testing parameters
const int cMicTestManager::mkMicsPerBoard = N_MICS / 2;
const int cMicTestManager::mkMicSampleRate = 48000;

const int cMicTestManager::mkRecordDelayMs = 1000;
const int cMicTestManager::mkTestLengthMs = 1000;

const int cMicTestManager::mkTestLengthSamples =
  ((mkTestLengthMs * mkMicSampleRate) / 1000);
const int cMicTestManager::mkRecordDelaySamples =
  ((mkRecordDelayMs * mkMicSampleRate) / 1000);
const int cMicTestManager::mkTestBufferSize =
  (mkTestLengthSamples * N_MICS);


cMicTestManager::cMicTestManager()
{
  
}

cMicTestManager::~cMicTestManager()
{
  reset();
}

/** Loads the tests defined in the micTest config file. */
void cMicTestManager::loadConfigFile()
{
  // reset tests
  reset();
  mTests.clear();

  // open config file
  std::ifstream configFile(mkConfigFilePath);
  if(!configFile.is_open())
    {
      LOGW("cMicTestManager ==> Could not open config file '%s'. "
	   "Skipping mic test.\n", mkConfigFilePath.c_str());
      return;
    }

  std::stringstream ss;      // string stream for tokenizing each line 
  std::string line;          // the line read from the file
  std::string token;         // for reading unknown tokens as strings
  
  bool buildingTest = false; // whether test information is being read
  int nextDelayTime = 0;     // amount of time to delay before the next test (ms)
  std::string testName;      // name of the test being read
  std::string wavPath;       // path of wav file to play for the test being read

  while(std::getline(configFile, line))
    { // process each line that was read from the config file
      ss.clear();
      ss.str(line);

      // check the first token in the line
      token = "";
      ss >> token;

      if(token == "TEST")
	{ // start defining a new test
	  buildingTest = true;
	  
	  // parse general test parameters
	  ss >> testName;
	  ss >> wavPath;
	  LOGI("cMicTestManager ==> New test '%s' (wav path: '%s')\n",
	       testName.c_str(), wavPath.c_str() );
	  
	  mTests.emplace_back(mTests.size(), testName, wavPath, nextDelayTime);
	  sMicTest &currentTest = mTests.back();

	  // read/parse the dBFS limits for each mic
	  for(int iMic = 0; iMic < N_MICS; iMic++)
	    {
	      if(std::getline(configFile, line))
		{
		  ss.clear();
		  ss.str(line);
		  
		  ss >> token;   // 'MX' -> label for microphone #X limits
		  ss >> currentTest.mLowerLimits[iMic]; // lower mic limit
		  ss >> currentTest.mUpperLimits[iMic]; // upper mic limit
		  
		  LOGI("   cMicTestManager ==> M%d limits: [%.2f, %.2f]\n",
		       iMic, currentTest.mLowerLimits[iMic],
		       currentTest.mUpperLimits[iMic] );
		}
	    }

	  // test definition complete -- reset parameters for next test.
	  testName = "";
	  wavPath = "";
	  nextDelayTime = 0;
	}
      else if(token == "DELAY")
	{
	  ss >> nextDelayTime;
	}
      else if(token == "")
	{
	  LOGD("Skipping blank line in micTest-system.conf.\n",
	       token.c_str(), line.c_str() );
	}
      else if(token[0] == '#')
	{
	  LOGD("Skipping commented line in micTest-system.conf.\n",
	       token.c_str(), line.c_str() );
	}
      else
	{
	  LOGW("Unrecognized token '%s' in micTest-system.conf -- '%s'",
	       token.c_str(), line.c_str() );
	}
    }
}


/** Starts the mic tests. */
void cMicTestManager::startTesting()
{
  if(!isTesting())
    {
      // reload the config file in case it's changed
      loadConfigFile();
      
      // start the first test
      mStarting = true;
    }
}

/** Resets the mic tests. */
void cMicTestManager::reset()
{
  for(auto &test : mTests)
    { test.reset(); }
  
  mCurrentTest = -1;
  mDiscardPackets = 0;
  mComplete = false;
  mStarting = false;
  
  if(mpAudioProc)
    {
      delete mpAudioProc;
      mpAudioProc = nullptr;
    }
}

/** Queries whether the mic tests are running.
 *
 * @return whether the mic tests are running.
 */
bool cMicTestManager::isTesting() const
{
  return (mCurrentTest >= 0 || mStarting);
}

/** Queries whether all mic test audio has been recorded.
 *
 * @return whether all mic test audio has been recorded.
 */
bool cMicTestManager::isComplete() const
{
  return mComplete;
}

/** Gets the pass/fail results of the specified test.
 *
 * @param test the ID of the mic test to query.
 *
 * @return the test's array of pass/fail for each mic.
 */
std::array<bool, N_MICS> cMicTestManager::getTestResults(int testId)
{
  return mTests[testId].mPassed;
}

/** Returns combined pass/fail results for all mic tests.
 *
 * @return an array with test results for each mic, combined over all tests.
 */
std::array<testResult_t, N_MICS> cMicTestManager::getAllTestResults()
{
  std::array<testResult_t, N_MICS> results;

  if(mCurrentTest >= 0 && !mStarting)
    { // tests are running
      results.fill(eTestRunning);
    }
  else
    { // testing hasn't started yet
      results.fill(eTestInactive);

      if(mStarting)
	{  // testing hasn't started yet
	  return results;
	}
    }

  for(auto &test : mTests)
    {
      if(test.isFull() && test.mAnalyzed)
	{
	  for(int iMic = 0; iMic < N_MICS; iMic++)
	    {
	      if(!test.mPassed[iMic])
		{ results[iMic] = eTestFailed; }
	    }
	}
    }

  if(isComplete())
    {
      for(int iMic = 0; iMic < N_MICS; iMic++)
	{
	  if(results[iMic] == eTestRunning || results[iMic] == eTestInactive)
	    { // any mics that didn't fail are passing
	      results[iMic] = eTestPassed;
	    }
	}
    }
  
  return results;
}

/** Performs each mic test that is finished recording audio. */
void cMicTestManager::update()
{
  for(auto &test : mTests)
    { // perform tests on any data sets that have finished recording
      analyzeData(test);
    }
}

/** Callback for receiving audio packets to record.
 *
 * @param pData a pointer to the audio data.
 * @param bufLen the number of audio samples in the data.
 */
void cMicTestManager::micDataCallback(int16_t *pData, int bufLen)
{
  const int beforePackets = (mkRecordDelaySamples /
			     (bufLen / N_MICS) ); /**< packets to discard before
						     recording. */
  if(isTesting())
    {
      if(mDiscardPackets > 0)
	{ // discard this packet by not recording it
	  mDiscardPackets--;
	  return;
	}
      else if(mTestRecorded && mpAudioProc && mpAudioProc->isActive())
	{ // keep waiting for mpAudioProc to become inactive
	  return;
	}
      else if(mTestRecorded || mStarting)
	{
	  if(mpAudioProc)
	    { // delete the last audio process
	      delete mpAudioProc;
	      mpAudioProc = nullptr;
	    }

	  int nextTest = mCurrentTest + 1;
	  if(!mTestDelayed && nextTest < mTests.size() &&
	     mTests[nextTest].mDelaySamples > 0 )
	    { // discard packets to apply the test's delay
	      LOGI("Delaying %d samples (%d ms)...\n",
		   mTests[nextTest].mDelaySamples,
		   ((mTests[nextTest].mDelaySamples * 1000) / mkMicSampleRate) );
      
	      discardPackets(mTests[nextTest].mDelaySamples / (bufLen / N_MICS));
	      mTestDelayed = true;
	      return;
	    }
	  
	  // move on to next mic test 
	  mCurrentTest++;
	  mStarting = false;
	  mTestDelayed = false;
	  if(mCurrentTest >= mTests.size())
	    { // all tests are finished
	      mCurrentTest = -1;
	      mComplete = true;
	      return;
	    }
	  mTestRecorded = false;
	  
	  if(mTests[mCurrentTest].mWavFilePath != "")
	    { // play next test's audio
	      LOGI(" Playing %s test audio (%s)...\n",
		   mTests[mCurrentTest].mName.c_str(),
		   mTests[mCurrentTest].mWavFilePath.c_str() );
		  
	      // start a new process to play the next test's audio
	      mpAudioProc = new cShellProcess(mTests[mCurrentTest].mAudioCmd);
	    }
	  
	  // delay before starting to record
	  discardPackets(beforePackets);
	}
      else
	{
	  // start/continue recording data
	  if(recordData(pData, bufLen))
	    {
	      // test buffer is filled
	      mTestRecorded = true;
	      LOGI(" Done recording %s.\n", mTests[mCurrentTest].mName.c_str());
	    }
	}
    }
}

/** Signals micDataCallback() to skip the specified number of packets.
 *
 * @param numPackets the number of packets to discard.
 */
void cMicTestManager::discardPackets(int numPackets)
{
  mDiscardPackets += numPackets;
}

/** Records the given audio to the current test's data.
 *
 * @param pData a pointer to the audio data.
 * @param bufLen the number of audio samples in the data.
 *
 * @return whether the test's audio buffer is filled.
 */
bool cMicTestManager::recordData(int16_t *pData, int bufLen)
{
  if(isTesting())
    {
      sMicTest &test = mTests[mCurrentTest];

      if(test.mAudioData.size() == 0)
	{ LOGI(" Recording %s...\n", test.mName.c_str()); }

      int testSamplesLeft = (mkTestBufferSize - test.mAudioData.size() -
			     bufLen );
      int recordSamples = (testSamplesLeft >= 0 ?
			   bufLen : std::max(0, bufLen + testSamplesLeft) );
  
      // append any new data to the back of the data vector.
      if(recordSamples > 0)
	{
	  std::copy(pData, pData + recordSamples,
		    std::back_inserter(test.mAudioData) );
	}

      // done testing if there's enough data recorded
      return (test.mAudioData.size() >= mkTestBufferSize);
    }
  else
    { // already done testing
      return true;
    }
}

/** Calculates the recorded audio's RMS to check its range for a pass/fail.
 *
 * @param test the test to analyze.
 *
 * @return whether the test data is done being analyzed.
 */
bool cMicTestManager::analyzeData(cMicTestManager::sMicTest &test)
{
  if(test.isFull() && !test.mAnalyzed)
    {
      test.mAnalyzed = true;
      findRms(test.mAudioData, test.mRms);
	
      for(int iMic = 0; iMic < N_MICS; iMic++)
	{
	  test.mDbfs[iMic] = rmsToDbfs(test.mRms[iMic]);
	  test.mPassed[iMic] =
	    (test.mDbfs[iMic] >= test.mLowerLimits[iMic] &&
	     test.mDbfs[iMic] <= test.mUpperLimits[iMic] );
	}
    }
  return test.mAnalyzed;
}


/** Prints the specified test's results to stdout.
 *
 * @param test the test to print.
 */
void cMicTestManager::printTestResult(cMicTestManager::sMicTest &test)
{
  // run test calculations if needed
  if(analyzeData(test))
    {
      // print results
      LOGI("%s results:\n", test.mName.c_str());
      for(int iMic = 0; iMic < N_MICS; iMic++)
	{
	  LOGI(" M%d (%.2f - %.2f):  %.2f | %s\n",
	       iMic, test.mLowerLimits[iMic], test.mUpperLimits[iMic],
	       test.mDbfs[iMic], (test.mPassed[iMic] ?
				      "PASS" : "FAIL" ) );
	}
    }
}

/** Prints each test's results sequentially. */
void cMicTestManager::printAllTestResults()
{
  for(auto &test : mTests)
    { printTestResult(test); }
}

/** Appends the mic test results to the specified CSV file. */
void cMicTestManager::writeResultsToFile()
{
  bool fileExists = std::ifstream(mkResultsOutputPath).good();
  std::ofstream outFile;
  outFile.open(mkResultsOutputPath, std::ios::app);

  if(!fileExists)
    { // write header -- titles for each column
      outFile << "DATE,";
      for(int iTest = 0; iTest < mTests.size(); iTest++)
	{
	  outFile << mTests[iTest].mName <<
	    ",MIC0,,MIC1,,MIC2,,MIC3,,MIC4,,MIC5,,MIC6,,MIC7,,,";
	}
      outFile << "\n,,,,,,,,,,,,,,,\n";
    }

  // write date/time
  outFile << currentDateTimeHumanReadable();

  // write each test's results (dBFS and pass/fail)
  outFile << ",,";
  for(int iTest = 0; iTest < mTests.size(); iTest++)
    {
      for(int iMic = 0; iMic < N_MICS; iMic++)
	{
	  outFile << std::fixed << std::setprecision(1)
		  << mTests[iTest].mDbfs[iMic] << ","
		  << (mTests[iTest].mPassed[iMic] ? "PASS" : "FAIL") << ",";
	}
      outFile << ",,";
    }
  outFile << "\n";
}

/** This finds the rms of the given mic audio data.
 *
 * @param audioData the given input mic audio data (channels interlaced).
 * @param rmsOut the rms for each mic's audio.
 */
void cMicTestManager::findRms(const std::vector<int16_t> &audioData,
			      std::array<double, N_MICS> &rmsOut )
{
  rmsOut.fill(0.0);
  
  const int numSamples = audioData.size() / N_MICS;
  int iData = 0;
  
  for(int iSample = 0; iSample < numSamples; iSample++)
    {
      for(int iMic = 0; iMic < N_MICS; iMic++, iData++)
	{ rmsOut[iMic] += (double)audioData[iData] * (double)audioData[iData]; }
    }
  
  for(int iMic = 0; iMic < N_MICS; iMic++)
    { rmsOut[iMic] = sqrt(rmsOut[iMic] / (double)numSamples); }
}


/** This function converts the given rms value to dBFS.
 *
 * @param rms the rms input value.
 *
 * @return the calculated dBFS value.
 */
double cMicTestManager::rmsToDbfs(double rms)
{ 
  const double kFullScaleSineRms =
    (double)(1 << 15) / sqrt(2); /**< rms value of a full-scale sine wave using
				    16-bit samples. */
  
  return 20.0 * log10(rms / kFullScaleSineRms);
}



// sMicTest implementation

/** @param testId the index/id of this test.
 *  @param testName the name of this test.
 *  @param wavFilePath the path to the wav file to play.
 *  @param delay the amount of time to delay before starting the test (ms).
 */
cMicTestManager::sMicTest::sMicTest(int testId, const std::string &testName,
				    const std::string &wavFilePath, int delay )
  : mTestId(testId), mName(testName), mWavFilePath(wavFilePath),
    mDelaySamples((delay * mkMicSampleRate) / 1000)
{
  // allocate/initialize data arrays
  mAudioData.reserve(cMicTestManager::mkTestBufferSize);
  reset();

  // create audio command
  if(mWavFilePath != "")
    {
      std::ostringstream audioCmdSs;
      audioCmdSs << "tinyplay " << mWavFilePath << " >/dev/null";
      mAudioCmd = audioCmdSs.str();
    }
}
    
/** Resets this test's data. */
void cMicTestManager::sMicTest::reset()
{
  mAnalyzed = false;
  
  mAudioData.clear();
  mRms.fill(0.0);
  mDbfs.fill(0.0);
  mPassed.fill(false);
}

/** Queries whether all the necessary data for this test has been recorded.
 *
 * @return whether enough data for this test has been recorded.
 */
bool cMicTestManager::sMicTest::isFull() const
{
  return (mAudioData.size() >= cMicTestManager::mkTestBufferSize);
}
