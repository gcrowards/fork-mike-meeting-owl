/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  systemtest/systemTest.cpp
 *  This is the implementation file for cSystemTest.
 *   --> The core manager class for the built-in system test.
 *       Tests the Owl's microphones, speaker, buttons and LEDs, with user input
 *        to confirm. The results are stored in an image and output over UVC so
 *        it can be fully used even after adb is locked.
 */


#include "buildId.hpp"
#include "deviceInfo.hpp"
#include "logging.hpp"
#include "shellProcess.hpp"
#include "systemtest/systemTest.hpp"
#include "systemtest/testImgCompositor.hpp"

#ifdef __ANDROID__
#include "SpeakerHandler.h"
#endif

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <unistd.h>

// the system test outputs the same size image as normal owl mode
const int cSystemTest::mkWidth = OUTPUT_WIDTH;
const int cSystemTest::mkHeight = OUTPUT_HEIGHT;

const std::vector<std::string> cSystemTest::mkTestPrompts =
  { // eSystemTestIdle
    "Please Wait...",
    // eSystemTestWifi
    "Please Wait -- Testing WiFi...",
    // eSystemTestBluetooth
    "",
    // eSystemTestSpeaker
    "Press VOLUME UP if there was noise coming from the speaker.\n"
    "Press VOLUME DOWN if there was no sound.",
    // eSystemTestLeds
    "Press VOLUME UP if all LEDs are on..\n"
    "Press VOLUME DOWN if any are off.",
    // eSystemTestButtons
    "Press each corresponding button when it turns yellow.\n"
    "If the button doesn't respond, press any other button to fail it.",
    // eSystemTestComplete
    "TESTING COMPLETE."
  };

// wav paths
const std::string cSystemTest::mkNoiseWavPath =
  "/system/media/OwlLabs/PinkFilt3min48k-6dB.wav";
const std::string cSystemTest::mkBlipWavPath =
  "/system/media/OwlLabs/volumeBlip.wav";

// thresholds
const int cSystemTest::mkMinPassingSignalStrength = -65;

cSystemTest::cSystemTest()
  : mImage(mkHeight, mkWidth, CV_8UC4)
{
  // define the system test button context
  // bind each button click with buttonClicked() with the appropriate argument.
  const std::vector<buttonType_t> kButtonTypes =
    { BUTTON_MUTE, BUTTON_OWL, BUTTON_VOLUP, BUTTON_VOLDN };
  
  for(auto buttonType : kButtonTypes)
    {
      mButtonContext.addBinding(sButtonEvent(buttonType, eButtonClick),
			  std::bind(&cSystemTest::buttonClicked,
				    this, buttonType ));
    }
}

cSystemTest::~cSystemTest()
{
  setTesting(false);
}

/** Updates the mic tests -- analyzes the mic data that has finished recorded
 *   and updates the test results. This can be called to update completed mic
 *   test results as audio is being recorded. */
void cSystemTest::update()
{
  if(mCurrentTest == eSystemTestWifi && mWifiTestManager.isComplete())
    { nextTest(); }
}

/** Renders the system test results image based on tests' current states.
 *
 * @param pDataOut points to a correctly-sized array in which to store the
 *  rendered image.
 */
void cSystemTest::renderImage(unsigned char *pDataOut)
{
  const int kPadding = cTestImgCompositor::mkPadding;
  
  // clear image
  mImage = cTestImgCompositor::mkBgColor;

  // draw prompt for interactive tests
  cv::Size promptSize(mkWidth - 2 * kPadding, 80);
  
  cv::Point promptPos(kPadding, cTestImgCompositor::mkPanoHeight + 2 * kPadding);
  cTestImgCompositor::drawPrompt(mImage, promptPos,
				 splitStringLines(mkTestPrompts[mCurrentTest]),
				 promptSize );
  
  const int boxWidth = mkWidth / 2 - (kPadding * 3) / 2;
  cv::Size halfBoxSize(boxWidth, 0);
  int topY = promptPos.y + promptSize.height + kPadding;
  int halfY = (mkHeight - topY) / 2;

  // draw serial numbers, build ID, and MAC addresses
  std::string serialStr = (cDeviceInfo::getHardwareSerial() + " | " +
			   cDeviceInfo::getSoftwareSerial() );
  std::string buildStr   = "SOFTWARE: " + cBuildId::getString();
  std::string radioStr   = "RADIO:     " + cDeviceInfo::getRadioVersion(); 
  std::string wifiMacStr = "WIFI:        " + cDeviceInfo::getWifiMacAddress();
  std::string btMacStr   = "BLUETOOTH: " + cDeviceInfo::getBluetoothMacAddress();

  cv::Size serialSize = cTestImgCompositor::textSize(serialStr, nullptr);
  cv::Size wifiSize = cTestImgCompositor::textSize(wifiMacStr);
  cv::Size btSize = cTestImgCompositor::textSize(btMacStr);
  
  int rightOffset = std::max(wifiSize.width, btSize.width); // offset from right of screen
  
  cv::Point hwSerialPos(kPadding, topY);
  cv::Size testSize =
    cTestImgCompositor::drawText(mImage, hwSerialPos, serialStr,
				 cTestImgCompositor::mkNeutralColor );
  cv::Point buildIdPos(kPadding, topY + testSize.height + kPadding);
  testSize =
    cTestImgCompositor::drawText(mImage, buildIdPos, buildStr,
				 cTestImgCompositor::mkNeutralColor );
  cv::Point wifiMacPos(mkWidth - rightOffset - kPadding, buildIdPos.y);
  testSize =
    cTestImgCompositor::drawText(mImage, wifiMacPos, wifiMacStr,
				 cTestImgCompositor::mkNeutralColor );
  cv::Point radioPos(kPadding, wifiMacPos.y + testSize.height + kPadding);
  testSize =
    cTestImgCompositor::drawText(mImage, radioPos, radioStr,
				 cTestImgCompositor::mkNeutralColor );
  cv::Point btMacPos(mkWidth - rightOffset - kPadding,
		     radioPos.y );
  testSize =
    cTestImgCompositor::drawText(mImage, btMacPos, btMacStr,
				 cTestImgCompositor::mkNeutralColor );

  // draw mic test
  cv::Point micPos(kPadding, btMacPos.y + testSize.height + kPadding);
  testSize =
    cTestImgCompositor::drawMicTest(mImage, micPos, mMicLevels, halfBoxSize);

  // draw wifi test
  cv::Point wifiPos(mkWidth / 2 + kPadding / 2, micPos.y);
  testSize = cTestImgCompositor::drawWifiTest(mImage, wifiPos,
  					      mWifiTestManager.getResult(),
  					      (mCurrentTest == eSystemTestWifi),
  					      mWifiTestManager.getStation(),
  					      halfBoxSize );
  // draw bluetooth test
  // TODO: implement bluetooth test and connect results.
  //cv::Point2f btPos(mkWidth / 2 + kPadding / 2,
  //		    wifiPos.y + testSize.height + kPadding );
  //testSize = cTestImgCompositor::drawBtTest(mImage, btPos, mBtResult,
  //	 			         (mCurrentTest == eSystemTestBluetooth),
  //                                     halfBoxSize );
  
  // draw speaker test
  cv::Point speakerPos(mkWidth / 2 + kPadding / 2,
		       wifiPos.y + testSize.height + kPadding );
  testSize =
    cTestImgCompositor::drawSpeakerTest(mImage, speakerPos, mSpeakerResult,
					(mCurrentTest == eSystemTestSpeaker),
					halfBoxSize );
  // draw led test
  cv::Point ledPos(mkWidth / 2 + kPadding / 2,
		   speakerPos.y + testSize.height + kPadding );
  testSize = cTestImgCompositor::drawLedTest(mImage, ledPos, mLedResult,
					     (mCurrentTest == eSystemTestLeds),
					     halfBoxSize );
  // draw button test
  cv::Point buttonPos(mkWidth / 2 + kPadding / 2,
		      ledPos.y + testSize.height + kPadding );
  testSize = cTestImgCompositor::drawButtonTest(mImage, buttonPos,
						mButtonTestManager.getResults(),
						mButtonTestManager.isTesting(),
						halfBoxSize );

  // wrap the output data in a cv::Mat to easily copy the image data
  cv::Mat image(mkHeight, mkWidth, CV_8UC4, pDataOut);
  mImage.copyTo(image);
}

/** Input point for mic data to test -- measures volume levels to update meaure
 *   bars.
 *
 * @param pData is a pointer to the audio data.
 * @param bufLen is the size of the data (in samples).
 */
void cSystemTest::micDataCallback(int16_t *pData, int bufLen)
{
  const double kMinValue = 10.0;
  const double kMaxValue = 55.0;
  std::vector<int16_t> dataVector(bufLen);
  std::array<double, N_MICS> micRms;
  std::copy(pData, pData + bufLen, std::back_inserter(dataVector));
  cMicTestManager::findRms(dataVector, micRms);
  // convert to a scale across range [0.0, 1.0]
  for(int iMic = 0; iMic < N_MICS; iMic++)
    {
      // convert to dBFS and then a ratio
      double micLevel = (sqrt(micRms[iMic]) - kMinValue) / (kMaxValue - kMinValue);
      // clamp to [0.0, 1.0]
      micLevel = std::max(0.0, std::min(micLevel, 1.0));
      // apply new mic level with smoothing
      mMicLevels[iMic] = (7.0 * mMicLevels[iMic] + micLevel) / 8.0;
    }
}

/** @return the system test's LED context. */
cLedContext* cSystemTest::getLedContext()
{
  return &mLedContext;
}

/** @return the system test's button context. */
const cButtonContext* cSystemTest::getButtonContext() const
{
  return &mButtonContext;
}

/** Starts or stops the system test.
 *
 * @param testing whether the system test should be active.
 */
void cSystemTest::setTesting(bool testing)
{
  mTesting = testing;
  
  if(mTesting)
    { // move on to the first test
      nextTest();
    }
  else
    { // stop testing
      reset();
    }
}

/** Testing status accessor
 *
 * @return whether system test is currently active.
 */
bool cSystemTest::getTesting() const
{
  return mTesting;
}

/** Resets the system test results. */
void cSystemTest::reset()
{
  // reset test step
  mCurrentTest = eSystemTestIdle;
  
  // reset leds
  mLedContext.disable(LEDS_ALL);
  
  // reset wifi test
  mWifiTestManager.reset();

  // reset button results
  mButtonTestManager.reset();
  // reset LED results
  mLedResult = eTestInactive;

  mSpeakerResult = eTestInactive; // reset speaker results
  mBtResult = eTestUnknown;       // reset bluetooth results
}

/** Called internally to move onto the next test step.
 *  Prepares resources and processes needed for the next test. */
void cSystemTest::nextTest()
{
  if(mTesting)
    {
      const systemTest_t lastTest = mCurrentTest;

      // increment the test step
      mCurrentTest = (systemTest_t)(mCurrentTest + 1);

      finishTest(lastTest);
      prepareTest(mCurrentTest);
    }
}

/** Prepares the specified test's resources.
 *
 * @param test the test to prepare.
 */
void cSystemTest::prepareTest(systemTest_t test)
{
  // initialize next test
  switch(test)
    {
    case eSystemTestWifi:
      // start the wifi test
      mWifiTestManager.startTest(mkMinPassingSignalStrength);
      LOGI("Starting wifi test...\n");
      break;
    case eSystemTestBluetooth:
      // no wifi/bluetooth test yet -- skip by moving on to the next test
      nextTest();
      break;
      
    case eSystemTestSpeaker:
      // mark the speaker test active
      mSpeakerResult = eTestRunning;
      // play pink noise
      // TODO: Consider loading wav file only once, instead of every time it's played.
      if(mWavPlayer.playWavFile(mkNoiseWavPath.c_str(), nullptr) < 0)
	{ LOGE("Could not play pink noise wav file.\n"); }
      break;
    case eSystemTestLeds:
      // enable all leds to check
      mLedContext.enable(LEDS_ALL);
      mLedResult = eTestRunning;
      break;
    case eSystemTestButtons:
      // start the button tests
      mButtonTestManager.startTesting();
      break;
    
    default:
      break;
    }
}

/** Cleans up the specified test's resources.
 *
 * @param test the test to clean up.
 */
void cSystemTest::finishTest(systemTest_t test)
{
  // finish up last test
  switch(test)
    {
    case eSystemTestLeds:
      // turn off LEDs
      mLedContext.disable(LEDS_ALL);
      break;
      
    default: // do nothing
      break;
    }
}

/** Called when one or more buttons are clicked and responds based on
 *   the current test. */
void cSystemTest::buttonClicked(buttonType_t button)
{
  if(mTesting)
    {
      int ledIndex;
      switch(mCurrentTest)
	{
	case eSystemTestSpeaker:
	  if(button == BUTTON_VOLUP)
	    { // speaker is working -- pass
	      mSpeakerResult = eTestPassed;
	      nextTest();
	    }
	  else if(button == BUTTON_VOLDN)
	    { // speaker isn't working -- fail
	      mSpeakerResult = eTestFailed;
	      nextTest();
	    }
	  else
	    {
	      LOGW("Invalid button pressed for speaker test --> 0x%X\n", button);
	      //mSpeakerResult = eTestFailed;
	    }
	  break;
      
	case eSystemTestWifi:
	case eSystemTestBluetooth:
	  // skip these tests
	  // TODO: Possibly implement some basic testing. E.g. connect to an access
	  //        point.
	  break;
	  
	case eSystemTestLeds:
	  if(button == BUTTON_VOLUP)
	    {
	      mLedResult = eTestPassed;
	      nextTest();
	    }
	  else if(button == BUTTON_VOLDN)
	    {
	      mLedResult = eTestFailed;
	      nextTest();
	    }
	  break;
    
	case eSystemTestButtons:
	  if(mButtonTestManager.update(button))
	    { // button test complete -- move on to next system test
	      nextTest();
	    }
	  break;

	default: // do nothing
	  break;
	}

      // play volume blip on every button press.
      // TODO: Consider loading wav file only once, instead of every time it's played.
      if(mWavPlayer.playWavFile(mkBlipWavPath.c_str(), nullptr) < 0)
	{ LOGE("Could not play volume blip wav file.\n"); }
    }
}
