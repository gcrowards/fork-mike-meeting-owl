/** Copyright 2017 Owl Labs Inc.
 *  All rights reserved.
 *
 *  systemtest/wifiTestManager.cpp
 *  This is the implementation file for cWifiTestManager.
 *   --> Runs WiFi testing for cSystemTest. 
 */

#include "common.hpp"
#include "logging.hpp"
#include "shellProcess.hpp"
#include "systemtest/wifiTestManager.hpp"

#include <unistd.h>
#include <vector>

const std::string cWifiTestManager::mkCommandLine =
  "wpa_cli -p /data/misc/wifi/sockets -i wlan0 scan &&"
  "sleep 3 &&"
  "wpa_cli -p /data/misc/wifi/sockets -i wlan0 scan_results";
const std::string cWifiTestManager::mkDelims = "\t";


cWifiTestManager::cWifiTestManager()
  : mTestThread(std::bind(&cWifiTestManager::testWifi, this), "wifi test")
{
  
}


cWifiTestManager::~cWifiTestManager()
{
  mTestThread.join();
}

/** @return whether wifi testing is complete. */
bool cWifiTestManager::isComplete() const
{
  return mTestComplete;
}

/** @return the name of the strongest wifi station. */
std::string cWifiTestManager::getStation() const
{
  return mWifiStation;
}

/** @return the result of the wifi test. */
testResult_t cWifiTestManager::getResult() const
{
  return mTestResult;
}

/** Start running the WiFi test.
 *
 * @param minSignal is the minimum signal strength to pass.
 */
void cWifiTestManager::startTest(int minSignal)
{
  mMinSignal = minSignal;
  mMaxSignal = minSignal;   // Signal must exceed minSignal to pass test
  mTestResult = eTestRunning;
  mTestThread.start();
}

/** Runs the wifi test. */
void cWifiTestManager::testWifi()
{
  if(mTestResult == eTestRunning && !mTestComplete)
    {
      cShellProcess testProcess(mkCommandLine,
				std::bind(&cWifiTestManager::processOutputHandler,
					  this, std::placeholders::_1 ), true );
    }
  else
    { usleep(100000); }
}

/** Resets the wifi test. */
void cWifiTestManager::reset()
{
  mTestThread.join();
  mTestComplete = false;
  mTestResult = eTestInactive;
  mWifiStation = "";
}


/* The following function tokenizes each line returned by the
 * commandLine, then finds the line containing the largest value
 * in the third token. This function is called by the wifi test cShellPocess.
 *
 * @param pLine a pointer to the next output line.
 * @return whether the testing is complete and the process should terminate.
 */
bool cWifiTestManager::processOutputHandler(const std::string *pLine)
{
  // Use wpa_cli to scan available WiFi networks, then find the
  // one with the highest signal strength. If the signal is stronger
  // than minSignal, return a string containing the network name
  // and signal strength, otherwise return an empty string.
  std::vector<std::string> toks;
  
  if (pLine != nullptr)
    {
      toks.clear();
      tokenize(*pLine, mkDelims, toks);
      // Each scan result line containing a valid network has 5 entries:
      // bssid / frequency / signal level / flags / ssid
      if (toks.size() >= 5)
	{
	  int sigLevel;
	  int numConverted = sscanf(toks[2].c_str(), "%d", &sigLevel);

	  if (numConverted == 1)
	    {
	      std::string networkInfo;  // SSID:signalLevel stored here
	      int newlinePos = toks[4].find("\n");
	      if (newlinePos != std::string::npos)
		{ networkInfo = toks[4].substr(0, newlinePos); }
	      else
		{ networkInfo = toks[4]; }
	      if (networkInfo.length() > 14)
		{
		  // SSID too long to fit on screen - truncate and add
		  // ... to the truncated end.
		  networkInfo.erase(14, std::string::npos);
		  networkInfo.append("...");
		}
	      // Append ':' followed by signal level (in dB)
	      networkInfo += (":" + toks[2]);
	      LOGI("Found: %s\n", networkInfo.c_str());
	      if (sigLevel > mMaxSignal)
		{
		  mMaxSignal = sigLevel;
		  mWifiStation = networkInfo;
		  LOGI("New strongest: %s\n", mWifiStation.c_str());
		}
	    }
	}
      LOGI("WiFi test result: %s\n", mWifiStation.c_str());
      return true;
    }
  else
    {
      mTestComplete = true;
      mTestResult = (mMaxSignal > mMinSignal ? eTestPassed : eTestFailed);
      return false;
    }
}
