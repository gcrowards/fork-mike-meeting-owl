/** blackBox.cpp
 *
 *  Created on: Mar 23, 2017
 *  Module containing interface for saving, loading, and manipulating
 *  the BlackBox.json system state storage file.
 */
#include <fcntl.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>

#include "blackBox.hpp"
#include "buildId.hpp"
#include "common.hpp"
#include "logging.hpp"
#include "rapidjson/pointer.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"


/** Create a BlackBox object.
 *
 * This function creates the BlackBox directory if it does not already
 * exist. It also loads existing black box data, if present, into the
 * JSON mDoc managed by this object. If a previous file is not present,
 * mDoc is initialized with default values.
 *
 * @param blackBoxDir is the directory in which to store BlackBox data.
 */
cBlackBox::cBlackBox(std::string blackBoxDir)
{
  mFileName = blackBoxDir;
  if (mFileName[mFileName.length()] != '/')
    {
      mFileName.append("/");
    }
  mBackName = mFileName;
  mFileName.append(BLACK_BOX_DATA);
  mBackName.append(BLACK_BOX_BACKUP);

  // Create analytics storage directory, if necessary
  if (access(blackBoxDir.c_str(), R_OK | W_OK) != 0)
    {
      mode_t origMask = umask(0);
      int retVal = mkdirWithParents(blackBoxDir.c_str(),
                                    S_IRWXU | S_IRWXG | S_IRWXO);
      umask(origMask);
      if (retVal < 0)
        {
          LOGE("cBlackBox: Attempt to create BlackBox dir failed (%d)\n",
               retVal);
        }
    }

  init();

  loadDeviceSerialNumber(std::string(DEVICE_SERIAL_NUMBER_PATH));

  updateVersionList();
}

/** Deletes the BlackBox object.
 *
 * This function saves any outstanding changes to the BlackBox before
 * destroying the object.
 */
cBlackBox::~cBlackBox()
{
  save();
}

/** Reads previous state of global data from black box to start running.
 *
 * This routine reads previously stored data from the black box file. If
 * the file is inaccessible or corrupted, then the backup file is loaded.
 * If the backup is also inaccessible or corrupted, then default values
 * are loaded. In the process of loading, the mDoc JSON document
 * object is initialized for subsequent use in manipulating black box data.
 *
 * @return 0 if all went well, -1 if the black box could not be created, read,
 * or written.
 */
int cBlackBox::init(void)
{
  int rc = -1;
  int blackBoxfd = 0;
  struct stat statBuf;
  int fileSize = 0;
  int rSize;

  if (access(mFileName.c_str(), R_OK | W_OK) == 0)
    {
      // File exists
      rc = stat(mFileName.c_str(), &statBuf);
      if (rc == 0)
        {
          fileSize = statBuf.st_size;
          blackBoxfd = open(mFileName.c_str(), O_RDONLY);
        }
    }
  if (blackBoxfd <= 0)
    {
      // File not right, so try the backup
      if (access(mBackName.c_str(), R_OK) == 0)
        {
          rc = stat(mBackName.c_str(), &statBuf);
          if (rc == 0)
            {
              fileSize = statBuf.st_size;
              blackBoxfd = open(mBackName.c_str(), O_RDONLY);
            }
        }
      else
        {
          // Problem accessing files
          rc = -1;
        }
    }

  // Parse the template into our internal document
  mDoc.Parse(BLACK_BOX_JSON);

  if (mDoc.GetParseError() != rapidjson::kParseErrorNone)
    {
      // This really shouldn't happen, but if it does, fix BLACK_BOX_JSON!
      rc = -1;
      LOGE("cBlackBox::init() Failure!\n");
    }

  if ((rc < 0) || (blackBoxfd <= 0))
    {
      // No black box found so create a new one
      rc = 0;
    }
  else
    {
      // Import values from pre-existing black box, if possible
      char buf[fileSize];
      rapidjson::Document diskDoc;

      rSize = read(blackBoxfd, buf, fileSize);
      if (rSize != fileSize)
        {
          // Read failed, so default to blank black box
          rc = 0;
        }
      else
        {
          // Parse the string from file into diskDoc
          diskDoc.Parse<rapidjson::kParseStopWhenDoneFlag>(buf);

          if (!mDoc.HasMember(BB_ROOT_STR) || !diskDoc.HasMember(BB_ROOT_STR))
            {
              // Black box is not valid, so default to blank black box
              rc = 0;
            }
          else
            {
              // Iterate through root fields in template filling values from disk
              for (rapidjson::Value::ConstMemberIterator
                  rootItr = mDoc.MemberBegin();
                  rootItr != mDoc.MemberEnd(); ++rootItr)
                {
                  const char * pRootStr = rootItr->name.GetString();

                  if (!diskDoc.HasMember(pRootStr))
                    {
                      // If this root key does not exist in file, skip it
                      continue;
                    }
#if DEBUG_BLACK_BOX
                  LOGI("Iterating on root: %s\n", pRootStr);
#endif
                  // Iterate through fields in template filling values from disk
                  for (rapidjson::Value::ConstMemberIterator
                      itr = rootItr->value.MemberBegin();
                      itr != rootItr->value.MemberEnd(); ++itr)
                    {
                      const char *pStr = itr->name.GetString();
                      if (diskDoc[pRootStr].HasMember(pStr))
                        {
                          std::string key = "/" + std::string(pRootStr) + "/" +
                              std::string(pStr);
                          rapidjson::Value *item =
                              rapidjson::Pointer(key.c_str()).Get(mDoc);
                          rapidjson::Value *fileItem =
                              rapidjson::Pointer(key.c_str()).Get(diskDoc);
                          if (item->GetType() != fileItem->GetType())
                            {
                              // Type has changed, discard old data by ignoring
                              continue;
                            }
                          if (item->IsString())
                            {
                              item->SetString(
                                  fileItem->GetString(), mDoc.GetAllocator());
                            }
                          else if (item->IsArray())
                            {
                              for (int elem = 0;
                                  elem < fileItem->Size(); elem++)
                                {
                                  if (!(*fileItem)[elem].IsString())
                                    {
                                      // Only support list of strings
                                      // Skip non-strings
                                      continue;
                                    }
                                  const char *pStr =
                                      (*fileItem)[elem].GetString();
                                  rapidjson::Value val;
                                  val.SetString(pStr, mDoc.GetAllocator());
                                  (*item).PushBack(val, mDoc.GetAllocator());
                                }
                            }
                          else
                            {
                              *item = *fileItem;
                            }
#if DEBUG_BLACK_BOX
                          rapidjson::StringBuffer buffer;
                          rapidjson::Writer<rapidjson::StringBuffer>writer(buffer);
                          itr->value.Accept(writer);
                          LOGI("mDoc[%s][%s] = %s\n", pRootStr, pStr,
                               buffer.GetString());
#endif
                        }
                    }
                }
            }
        }
      close(blackBoxfd);
    }

  return rc;
}

/** Confirms that a file is present and returns its size.
 *
 * @param file is the file to check.
 *
 * @return the size of the file, or -1 if the file is not present or 0 size.
 */
int cBlackBox::valid(std::string &file)
{
  int retVal = -1;
  int rc;
  struct stat statBuf;
  int fileSize = 0;

  if (access(file.c_str(), R_OK | W_OK) == 0)
    {
      // File exists
      rc = stat(file.c_str(), &statBuf);
      if (rc == 0)
        {
          fileSize = statBuf.st_size;
        }
      if (fileSize > 0)
        {
          retVal = fileSize;
        }
    }
  return retVal;
}

/** Loads the device serial number from the given file.
 *
 * This function assumes that the file contains nothing except the
 * device serial number in the first line.
 *
 * @param file is the name of the file containing the serial number.
 */
void cBlackBox::loadDeviceSerialNumber(std::string file)
{
  FILE *pFile = fopen(file.c_str(), "r");
  std::string serialNum;
  char pcFileData[128];
  const char pcUnknown[] = "unknown";

  if (pFile == nullptr)
    {
      serialNum = std::string(pcUnknown);
    }
  else
    {
      if (fgets(pcFileData, sizeof(pcFileData), pFile) != nullptr)
        {
          // found a serial number, remove newline on end, if needed
          if (pcFileData[strlen(pcFileData) - 1] == '\n')
            { pcFileData[strlen(pcFileData) - 1] = 0; }
          serialNum = std::string(pcFileData);
        }
      else
        {
          serialNum = std::string(pcUnknown);
        }
    }

  setString(BB_ROOT_STR, BB_DEVICE_SERIAL_NUMBER, serialNum);
  LOGI("Device serial number: %s\n", serialNum.c_str());
  save();
}

/** Saves the contents of the BlackBox to disk.
 *
 * Prior to saving the black box file, the previous version of the file is
 * copied to a backup file.
 *
 * @return 0 if all went well, -1 if the write failed.
 */
int cBlackBox::save(void)
{
  int bbSize;
  int backupSize;
  int blackBoxfd;
  int wSize;
  int nMeetings = 0;
  int rc = 0;

  // Copy existing black box to backup
  bbSize = valid(mFileName);
  backupSize = valid(mBackName);
  if (bbSize > 0)
    {
      rename(mFileName.c_str(), mBackName.c_str());
    }

  if (!mDoc.HasMember(BB_ROOT_STR))
    {
      LOGI("mBlackBoxDoc has no \"BlackBox\"\n");
      // Create root {"BlackBox":} here
      mDoc.Parse(BLACK_BOX_JSON);
    }

  // Extract the JSON string for the black box
  rapidjson::StringBuffer buffer;
  rapidjson::Writer<rapidjson::StringBuffer>writer(buffer);
  mDoc.Accept(writer);
  getInt(BB_ROOT_STR, BB_N_MEETINGS_STR, nMeetings);
  LOGI("mMeetingNumber: %d\n", nMeetings);

  // Write out the new JSON file
  mode_t origMode = umask(0);
  blackBoxfd = open(mFileName.c_str(), O_WRONLY | O_CREAT | O_TRUNC,
                    S_IRUSR | S_IWUSR |
                    S_IRGRP | S_IWGRP |
                    S_IROTH | S_IWOTH);
  umask(origMode);

  bbSize = buffer.GetLength();
  wSize = write(blackBoxfd, buffer.GetString(), bbSize);
  if (bbSize != wSize)
    {
      rc = -1;
    }

  close(blackBoxfd);

  sync();

  return rc;
}

/** Set the value of an integer field in the BlackBox.
 *
 * @param groupKey is a string identifying the group the field is in.
 * @param key is a string identifying the key to set.
 * @param val is the value to write for the key.
 *
 * @return true if the value is set, false if the key was not found.
 */
bool cBlackBox::setInt(const char *groupKey, const char *key, int val)
{
  bool rc = true;

  if (!mDoc[groupKey].HasMember(key))
    { rc = false; }
  else
    {
      /* Update black box data */
      rapidjson::Value &nVal = mDoc[groupKey][key];
      nVal.SetInt(val);
    }
  return rc;
}

/** Get the value of an integer field in the BlackBox.
 *
 * @param groupKey is a string identifying the group the field is in.
 * @param key is a string identifying the key to get.
 * @param val is the value to update with data read from the black box.
 *
 * @return true if the value is gotten, false if the key was not found.
 */
bool cBlackBox::getInt(const char *groupKey, const char *key, int &val)
{
  bool rc = true;

  if (!mDoc[groupKey].HasMember(key))
    { rc = false; }
  else
    {
      /* Update black box data */
      rapidjson::Value &nVal = mDoc[groupKey][key];
      if (nVal.IsInt())
        { val = nVal.GetInt(); }
      else
        { rc = false; }
    }
  return rc;
}

/** Add increment to an integer field in the BlackBox.
 *
 * @param groupKey is a string identifying the group the field is in.
 * @param key is a string identifying the key to modify.
 * @param increment is an optional value by which to increment the value
 *        currently stored. If not specified, value is incremented by 1.
 */
bool cBlackBox::incInt(const char *groupKey, const char *key, int increment)
{
  bool rc = true;
  int val;

  if (!mDoc[groupKey].HasMember(key))
    { rc = false; }
  else
    {
      /* Update black box data */
      rapidjson::Value &nVal = mDoc[groupKey][key];
      if (nVal.IsInt())
        {
          val = nVal.GetInt();
          val += increment;
          nVal.SetInt(val);
        }
      else
        { rc = false; }
    }
  return rc;
}

/** Set the value of a string field in the BlackBox.
 *
 * @param groupKey is a string identifying the group the field is in.
 * @param key is a string identifying the key to set.
 * @param str is the string to write for the key.
 *
 * @return true if the string is written, false if the key was not found.
 */
bool cBlackBox::setString(const char *groupKey, const char *key, std::string &str)
{
  bool rc = true;

  if (!mDoc[groupKey].HasMember(key))
    { rc = false; }
  else
    {
      /* Update black box data */
      rapidjson::Value &nVal = mDoc[groupKey][key];
      nVal.SetString(str.c_str(), mDoc.GetAllocator());
    }
  return rc;
}

/** Get the value of a string field in the BlackBox.
 *
 * @param groupKey is a string identifying the group the field is in.
 * @param key is a string identifying the key to get.
 * @param str is the string read from the key.
 *
 * @return true if the string is read, false if the key was not found.
 */
bool cBlackBox::getString(const char *groupKey, const char *key, std::string &str)
{
  bool rc = true;

  if (!mDoc[groupKey].HasMember(key))
    { rc = false; }
  else
    {
      /* Update black box data */
      rapidjson::Value &nVal = mDoc[groupKey][key];
      if (nVal.IsString())
        { str.assign(nVal.GetString()); }
      else
        { rc = false; }
    }
  return rc;
}

/** Set the value of an boolean field in the BlackBox.
 *
 * @param groupKey is a string identifying the group the field is in.
 * @param key is a string identifying the key to set.
 * @param val is the value to write for the key.
 *
 * @return true if the value is set, false if the key was not found.
 */
bool cBlackBox::setBool(const char *groupKey, const char *key, bool val)
{
  bool rc = true;

  if (!mDoc[groupKey].HasMember(key))
    { rc = false; }
  else
    {
      /* Update black box data */
      rapidjson::Value &nVal = mDoc[groupKey][key];
      nVal.SetBool(val);
    }
  return rc;
}

/** Get the value of an boolean field in the BlackBox.
 *
 * @param groupKey is a string identifying the group the field is in.
 * @param key is a string identifying the key to get.
 * @param val is the value to update with the contents of the black box.
 *
 * @return true if the value is gotten, false if the key was not found.
 */
bool cBlackBox::getBool(const char *groupKey, const char *key, bool &val)
{
  bool rc = true;

  if (!mDoc[groupKey].HasMember(key))
    { rc = false; }
  else
    {
      /* Update black box data */
      rapidjson::Value &nVal = mDoc[groupKey][key];
      if (nVal.IsBool())
        { val = nVal.GetBool(); }
      else
        { rc = false; }
    }
  return rc;
}

/** Get list of strings held in an array type field of the black box.
 *
 * @param groupKey is the top level key of a group in the black box.
 * @param key is the lower level key of an array list in the black box.
 *
 * @return vector of strings contained in the array list.
 */
std::vector<std::string> cBlackBox::getStringList(const char *groupKey,
                                                  const char *key)
{
  std::vector<std::string> stringList;

  if (mDoc.HasMember(groupKey) && mDoc[groupKey].HasMember(key))
    {
      if (mDoc[groupKey][key].IsArray())
        {
          for (int elem = 0; elem < mDoc[groupKey][key].Size(); elem++)
            {
              const char *pStr = mDoc[groupKey][key][elem].GetString();
              stringList.push_back(pStr);
            }
        }
      else
        {
          LOGW("getStringList(): mDoc[%s][%s] is not an array\n",
               groupKey, key);
        }
    }
  else
    {
      LOGW("getStringList(): mDoc[%s][%s] does not exist\n", groupKey, key);
    }

  return stringList;
}

/** Append a new item to an array list stored in the black box.
 *
 * @param groupKey is the top level key of a group in the black box.
 * @param key is the lower level key of an array list in the black box.
 * @param newVal is a string to add to the list.
 *
 * @return true if the item could be added, false if it could not.
 */
bool cBlackBox::appendStringList(const char *groupKey, const char *key,
                                 std::string newVal)
{
  bool success = false;
  rapidjson::Value val;

  if (mDoc.HasMember(groupKey) && mDoc[groupKey].HasMember(key))
    {
      if (mDoc[groupKey][key].IsArray())
        {
          val.SetString(newVal.c_str(), mDoc.GetAllocator());
          mDoc[groupKey][key].PushBack(val, mDoc.GetAllocator());
          success = true;
        }
      else
        {
          LOGW("appendStringList(): mDoc[%s][%s] is not an array\n",
               groupKey, key);
        }
    }
  else
    {
      LOGW("appendStringList(): mDoc[%s][%s] does not exist\n", groupKey, key);
    }

  return success;
}

/** Updates black box version list field with current software version.
 *
 * If the current software version is not the same as the last version stored
 * in list of all prior versions in the black box, then the new version is
 * appended to the list. In this way, a history of all the versions ever
 * run is maintained for the Owl.
 */
void cBlackBox::updateVersionList(void)
{
  std::string buildId = cBuildId::getString();  // Get current version
  std::vector<std::string> versionList = getStringList(VERSIONS_ROOT_STR,
                                                       VERSIONS_HIST_STR);

  // Check current version against history of prior versions
  if ((versionList.size() < 1) ||
      (buildId.compare(versionList[versionList.size() - 1]) != 0))
    {
      // New version, so add to the list
      appendStringList(VERSIONS_ROOT_STR, VERSIONS_HIST_STR, buildId);
      mVersionChanged = true;
    }
}

/** Return true if the version has updated since the last boot.
 *
 */
bool cBlackBox::getVersionChanged(void)
{
  return mVersionChanged;
}
