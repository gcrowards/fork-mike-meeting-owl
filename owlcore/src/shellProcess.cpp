/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  shellProcess.cpp
 *  This is the implementation file for cShellProcess.
 *   --> Opens a system shell process that executes the given command.
 *       A callback can be specified, which will be called with the output when
 *        the process terminates.
 */

#include "logging.hpp"
#include "shellProcess.hpp"

#include <cstdio>
#include <cstdlib>
#include <fcntl.h>
#include <limits.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

/** @param command the command to execute on the shell process.
 *  @param outCallback the callback to call with output when the process
 *          terminates. */
cShellProcess::cShellProcess(const std::string &command,
                             shellCallback_t outCallback)
  : mCommand(command), mOutputCallback(outCallback), mExitStatus(0)

{
    mPid = forkProcess(mCommand);
}

cShellProcess::cShellProcess(const std::string &command,
                             shellCallback_t outCallback,
                             bool oneTimeCommand)
  : mCommand(command), mOutputCallback(outCallback), mExitStatus(0)
  
{
  if (oneTimeCommand)
    {
      mPersist = false;
      mActive = false;
      mExitStatus = runOneTimeCommand(command);
    }
  else
    {
      mPid = forkProcess(mCommand);
    }
}

cShellProcess::~cShellProcess()
{
  if(!mPersist)
    { killProcess(); }
}

/** Kills this process if it's active. */
void cShellProcess::killProcess()
{
  if(mActive)
    {
      LOGD("KILLING PROC %d...\n", mPid);

      kill(mPid, SIGTERM);
      mActive = false;
    }
}

/** Waits for this process to terminate. */
void cShellProcess::waitForProcess()
{
  if(mActive)
    {
      LOGD("WAITING FOR PROC %d...\n", mPid);
      int status;
      pid_t pid = wait(&status);

      mActive = false;
    }
}

/** Marks this process to not be killed on destruction. */
void cShellProcess::persist()
{
  mPersist = true;
}

/** Return the exit status of a completed process.
 *
 * @return exit status of the process that executed.
 */
int cShellProcess::getExitStatus()
{
  return mExitStatus;
}

/** Queries whether this process is open and running.
 *
 * @return whether the process is open and running.
 */
bool cShellProcess::isActive()
{
  if(mActive)
    {
      int status;
      pid_t result = waitpid(mPid, &status, WNOHANG);
  
      if(result == -1)
	{ LOGE("cShellProcess::isRunning() ==> waitpid() failed.\n"); }

      mActive = (result == 0);

      if(!mActive)
	{
	  LOGD("\nShell process stopped!!\n\n");
	}
    }
  
  return mActive;
}

/** Forks the calling process and runs a command on the fork.
 *
 * @param command is the shell command to run.
 *
 * @return the process id of the forked process.
 */
pid_t cShellProcess::forkProcess(const std::string &command)
{
  pid_t pid = fork();

  if(pid == 0)
    {
      int r = system(command.c_str());
      exit(0);
    }
  
  return pid;
}

int cShellProcess::runOneTimeCommand(const std::string &command)
{
  FILE *fp;
  char path[1035];
  std::string outLine;

  /* Open the command for reading. */
  fp = popen(command.c_str(), "r");
  if (fp == NULL)
    {
      LOGI("Failed to run command \"%s\"\n", command.c_str());
      return INT_MIN;
    }

  if (mOutputCallback != nullptr)
    {
    /* Read the output a line at a time - output it. */
    while (fgets(path, sizeof(path)-1, fp) != NULL)
      {
        outLine = std::string(path);
        mOutputCallback(&outLine);
      }
    mOutputCallback(nullptr);   // NULL signals end of output
    }

  /* close pipe, return commands exit status */
  return pclose(fp);
}
