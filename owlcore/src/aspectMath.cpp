#include <cmath>
#include "aspectMath.hpp"
#include "logging.hpp"
#include "params.h"
#include "systemParams.hpp"

namespace aspectMath
{
  cSystemParams gSystemParams; /**< holds system parameter values as loaded
				* from disk (e.g. camera center pixel coords). */
  
  /** Compute the horizontal field of view angles and the inner and
   * outer radii for the panoramic image, to ensure a proper aspect
   * ratio given the panel size.
   *
   * @param pTheta0 points to a float that stores the computed
   * left-hand edge (in radians) of the horizontal field of view.
   * @param pTheta1 points to a float that stores the computed
   * right-hand edge (in radians) of the horizontal field of view.
   *
   * @param pRInner points to a float that stores the computed inner radius.
   * @param pROuter points to a float that stores the computed outer radius.
   * @param maxRadius constrains the maximum value for the outer radius.
   *
   * @param w is the width of the screen panel in pixels.
   * @param h is the height of the screen panel in pixels.
   * @param hFovWeight is a value in [0, 1] that determines how the
   * algorithm trades off adjustments to the horizontal and vertical
   * fields of view. A value of 1.0 maintains the horizontal FOV, and
   * a value of 0.0 maintains the vertical FOV. An intermediate value
   * compromises between the extremes.
   * @param rOuterWeight is a value in [0, 1] that determines how the
   * algorithm trades off adjustments to the outer radius. A value of
   * 1.0 maintains the original outer radius, and a value of 0.0
   * allows the outer radius to change completely to accomodate the
   * other constraints and heuristics.
   * @param lensCoeff is an adjustment factor; default is 1.0.
   * @param ruleOfThirds determines if the target point is supposed to
   * correspond to 1/3 from the top of the screen panel (true) or to
   * 1/2 from the top (false); the default is true.
   */
  void  adjustLegacyParams(float *pTheta0, float *pTheta1,
			   float *pRInner, float *pROuter,
			   float maxRadius, int w, int h,
			   float hFovWeight, float rOuterWeight,
			   float lensCoeff, bool ruleOfThirds)
  {
    float theta, phi;
    float rInner = *pRInner;
    float rOuter = *pROuter;
    float phiLower = radiusToPhi(rOuter);
    float phiUpper = radiusToPhi(rInner);
    float varVFov = phiUpper - phiLower;
    float varHFov = *pTheta1 - *pTheta0;

    // compute target horizontal angle as center of field of view
    theta = ( (*pTheta0) + (*pTheta1) ) /2;

    // compute target vertical angle
    if (ruleOfThirds)
      { phi = phiUpper - varVFov / 3; } // upper third
    else
      { phi = phiUpper - varVFov / 2; } // center

    // compromise between the current horizontal FOV and what the
    // horizontal FOV would be if the vertical FOV were constrained
    varHFov = hFovWeight * varHFov
      + (1.0 - hFovWeight) * hFov(varVFov, phi, w, h, lensCoeff);

    // now adjust the vertical FOV while constraining the horizontal FOV
    computeRadiiFromHFovPhi(&rInner, &rOuter, maxRadius,
			    varHFov, phi, w, h,
			    lensCoeff, ruleOfThirds);
    varVFov = radiusToPhi(rInner) - radiusToPhi(rOuter);

    // finally, tip the vertical FOV depending on the weight given to
    // the original outer radius
    rOuter = rOuterWeight * (*pROuter) + (1.0 - rOuterWeight) * rOuter;
    rInner = phiToRadius(radiusToPhi(rOuter) + varVFov);

    // store new values
    *pTheta0 = theta - varHFov / 2;
    *pTheta1 = theta + varHFov / 2;
    *pRInner = rInner;
    *pROuter = rOuter;
  }

  
  /** Compute the inner and outer radii for the panoramic image that
   * correspond to the given horizontal field of view, vertical angle,
   * and panel size.
   *
   * This function is useful for adjusting the vertical field of view
   * without changing the horiontal field of view or the target
   * vertical angle.
   *
   * @param pRInner points to a float that stores the computed inner radius.
   * @param pROuter points to a float that stores the computed outer radius.
   * @param maxRadius constrains the maximum value for the outer radius.
   *
   * @param hFov is the horizontal field of view in radians.
   * @param phi is the vertical elevation angle in radians.
   * @param w is the width of the screen panel in pixels.
   * @param h is the height of the screen panel in pixels.
   * @param lensCoeff is an adjustment factor; default is 1.0.
   * @param ruleOfThirds determines if the target point is supposed to
   * correspond to 1/3 from the top of the screen panel (true) or to
   * 1/2 from the top (false); the default is true.
   */
  void  computeRadiiFromHFovPhi(float *pRInner, float *pROuter, float maxRadius,
				float hFov, float phi, int w, int h,
				float lensCoeff, bool ruleOfThirds)
  {
    // compute new vertical FOV and convert to radii
    float varVFov = vFov(hFov, phi, w, h, lensCoeff);
    float rInner = phiToRadius(phiUpper(phi, varVFov, ruleOfThirds));
    float rOuter = phiToRadius(phiLower(phi, varVFov, ruleOfThirds));

    // ensure outer radius has a legal value
    if (rOuter > maxRadius)
      {
	// force outer radius to edge of panoramic image
	rOuter = maxRadius;
	// recompute inner radius
	// Note that we could move phi and recompute varVFov, but for
	// now we choose to keep them the same.
	rInner = phiToRadius(radiusToPhi(rOuter) + varVFov);
      }

    // store values
    *pRInner = rInner;
    *pROuter = rOuter;
  }
  
  
  /** Compute the horizontal field of view for the given panoramic
   * radii.
   *
   * This function is useful for adjusting the horizontal field of
   * view without changing the vertical field of view.
   *
   * @param w is the width of the screen panel in pixels.
   * @param h is the height of the screen panel in pixels.
   * @param rInner is the inner radius of the panoramic image in [0, 1/2].
   * @param rOuter is the outer radius of the panoramic image in [0, 1/2].
   * @param lensCoeff is an adjustment factor; default is 1.0.
   * @param ruleOfThirds determines if the target point is supposed to
   * correspond to 1/3 from the top of the screen panel (true) or to
   * 1/2 from the top (false); the default is true.
   *
   * @return the horizontal field of view in radians.
   */
  float computeHFovFromRadii(int w, int h, float rInner, float rOuter,
			     float lensCoeff, bool ruleOfThirds)
  {
    float phi;
    float phiLower = radiusToPhi(rOuter);
    float phiUpper = radiusToPhi(rInner);
    float varVFov = phiUpper - phiLower;
    if (ruleOfThirds)
      { phi = phiUpper - varVFov / 3; }
    else
      { phi = phiUpper - varVFov / 2; }
    return hFov(varVFov, phi, w, h, lensCoeff);
  }

  
  /** Compute the horizontal field of view for the given vertical
   * angle, panel size, and constraint on outer radius of the
   * panoramic image.
   *
   * This function is useful for determining how much horizontal zoom
   * is needed to make the lower edge of the screen panel correspond
   * to some radius (usually 1/2) in the panoramic image.
   *
   * @param phi is the vertical elevation angle in radians.
   * @param w is the width of the screen panel in pixels.
   * @param h is the height of the screen panel in pixels.
   * @param rOuter is the outer radius of the panoramic image in [0, 1/2].
   * @param lensCoeff is an adjustment factor; default is 1.0.
   * @param ruleOfThirds determines if the target point is supposed to
   * correspond to 1/3 from the top of the screen panel (true) or to
   * 1/2 from the top (false); the default is true.
   *
   * @return the horizontal field of view in radians.
   */
  float computeHFovFromPhiConstrainOuterRadius(float phi, int w, int h,
					       float rOuter, float lensCoeff,
					       bool ruleOfThirds)
  {
    float varVFov, phiUpper;
    float phiLower = radiusToPhi(rOuter);

    if (ruleOfThirds)
      {
	varVFov = (phi - phiLower) * 3 / 2;
	phiUpper = phi + varVFov / 3;
      }
    else
      {
	varVFov = (phi - phiLower) * 2;
	phiUpper = phi + varVFov / 2;
      }
    if (phiUpper > M_PI/2 || phiLower > phi || phiLower < 0)
      return 0; // bad input
    else
      return hFov(varVFov, phi, w, h, lensCoeff);
  }

  /** Get the camera center, normalized by the image size.
   *
   * @param pCx is a pointer to the float that stores the center x coordinate.
   * @param pCy is a pointer to the float that stores the center y coordinate.
   */
  void  getCameraCenter(float *pCx, float *pCy)
  {
    float cx, cy;
    if (CAMERA_HEIGHT != CAMERA_WIDTH)
      {
	// Assume the image will be cropped on left and right so result
	// is centered horizontally and cx is exactly 0.5.
	cx = 0.5;
      }
    else
      { cx = (float) gSystemParams.cameraCenterX / CAMERA_HEIGHT; }
    cy = (float) gSystemParams.cameraCenterY / CAMERA_HEIGHT;
    *pCx = cx;
    *pCy = cy;
  }

  /** @return the maximum radius, in [0., 0.5], that accounts for the
   *  camera center. */
  float getMaxRadius(void)
  {
    float cx, cy;
    getCameraCenter(&cx, &cy);
    return std::min( 0.5 - std::abs( 0.5 - cx ),
		     0.5 - std::abs( 0.5 - cy ) )
      - CAMERA_OUTER_RADIUS_MARGIN;
  }

  /** Compute the horizontal field of view given the vertical field of
   * view, vertical angle and, panel size.
   *
   * @param vFov is the vertical field of view in radians.
   * @param phi is the vertical elevation angle in radians.
   * @param w is the width of the screen panel in pixels.
   * @param h is the height of the screen panel in pixels.
   * @param lensCoeff is an adjustment factor; default is 1.0.
   *
   * @return the corresponding horizontal field of view in radians.
   */
  float hFov(float vFov, float phi, int w, int h, float lensCoeff)
  {
    float  a = lensCoeff * w / h; // panel aspect ratio with lens adjustment
    return a * vFov / cos(phi);
  }

  
  /** Compute the vertical field of view given the horizontal field of
   * view, vertical angle, and panel size.
   *
   * @param hFov is the horizontal field of view in radians.
   * @param phi is the vertical elevation angle in radians.
   * @param w is the width of the screen panel in pixels.
   * @param h is the height of the screen panel in pixels.
   * @param lensCoeff is an adjustment factor; default is 1.0.
   *
   * @return the corresponding vertical field of view in radians.
   */
  float vFov(float hFov, float phi, int w, int h, float lensCoeff)
  {
    float  a = lensCoeff * w / h; // panel aspect ratio with lens adjustment
    return hFov * cos(phi) / a;
  }

  
  /** Compute the vertical elevation angle that corresponds to the
   * given radius for the panoramic image.
   *
   * @param radius is the panoramic image radius in [0, 1/2].
   *
   * @return the vertical elevation angle in radians.
   */
  float radiusToPhi(float radius)
  {
    const float kPhiMin = CAMERA_LENS_MIN_PHI_DEG * M_PI / 180.;
    return (0.5 - radius) * (M_PI - 2.0 * kPhiMin) + kPhiMin;
  }

  /** Compute the radius for the panoramic image that corresponds to
   * the given vertical angle.
   *
   * @param phi is the vertical elevation angle in radians.
   *
   * @return the corresponding panoramic image radius in [0, 1/2].
   */
  float phiToRadius(float phi)
  {
    const float kPhiMin = CAMERA_LENS_MIN_PHI_DEG * M_PI / 180.;
    return 0.5 - (phi - kPhiMin) / (M_PI - 2.0 * kPhiMin);
  }

  
  /** Compute the lower vertical angle in radians given the target
   * angle and vertical field of view.
   *
   * "Lower" refers to closer to the bottom of the screen panel, which
   * corresponds to closer to a vertical angle of 0 and closer to the
   * outer edge of the panoramic image (radius 1/2).
   *
   * @param phi is the target elevation angle in radians.
   * @param vFov is the vertical field of view in radians.
   * @param ruleOfThirds determines if the target point is supposed to
   * correspond to 1/3 from the top of the screen panel (true) or to
   * 1/2 from the top (false); the default is true.
   *
   * @return the vertical angle in radians that corresponds to the
   * lower edge of the field of view.
   */
  float phiLower(float phi, float vFov, bool ruleOfThirds)
  {
    if (ruleOfThirds)
      { return phi - vFov / 3 * 2; }
    else
      { return phi - vFov / 2; }
  }

  
  /** Compute the upper vertical angle in radians given the target
   * angle and vertical field of view.
   *
   * "Upper" refers to closer to the top of the screen panel, which
   * corresponds to closer to a vertical angle of pi/2 and closer to
   * the center of the panoramic image (radius 0).
   *
   * @param phi is the target elevation angle in radians.
   * @param vFov is the vertical field of view in radians.
   * @param ruleOfThirds determines if the target point is supposed to
   * correspond to 1/3 from the top of the screen panel (true) or to
   * 1/2 from the top (false); the default is true.
   *
   * @return the vertical angle in radians that corresponds to the
   * upper edge of the field of view.
   */
  float phiUpper(float phi, float vFov, bool ruleOfThirds)
  {
    if (ruleOfThirds)
      { return phi + vFov / 3; }
    else
      { return phi + vFov / 2; }
  }


} // namespace aspectMath
