#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "imageFileUtils.hpp"
#include "logging.hpp"


/** This function converts BGRA format to RGB, overwriting the original data. */
void   inPlaceBGRAtoRGB(unsigned char *pImageBuf, int width, int height)
{
  int    x, y;
  long   i, j;

  i = j = 0;
  for (y=0; y<height; y++)
    {
      for (x=0; x<width; x++)
	{
	  pImageBuf[i+2] = pImageBuf[j++]; // copy B
	  pImageBuf[i+1] = pImageBuf[j++]; // copy G
	  pImageBuf[i+0] = pImageBuf[j++]; // copy R
	  j++;                             // skip A
	  i += 3;                          // next pixel
	}
    }
}

/** This function converts RGBA format to RGB, overwriting the original data. */
void   inPlaceRGBAtoRGB(unsigned char *pImageBuf, int width, int height)
{
  int    x, y;
  long   i, j;

  i = j = 0;
  for (y=0; y<height; y++)
    {
      for (x=0; x<width; x++)
	{
	  pImageBuf[i+0] = pImageBuf[j++]; // copy R
	  pImageBuf[i+1] = pImageBuf[j++]; // copy G
	  pImageBuf[i+2] = pImageBuf[j++]; // copy B
	  j++;                             // skip A
	  i += 3;                          // next pixel
	}
    }
}

/** This function passes back the width and height of a JPEG image file. 
 *
 * This routine is helpful for determining how large an image buffer
 * to allocate before reading a file.
*/
void   jpegFileGetImageSize(char *pFilename, int *pWidth, int *pHeight)
{
  struct jpeg_decompress_struct cinfo;
  struct jpeg_error_mgr         jerr;
  FILE   *pInfile;

  if ((pInfile = fopen(pFilename, "rb")) == NULL)
    {
      LOGE("JPEG: can't open %s, EXIT!\n", pFilename);
      usleep(500000);  // Allow short time for messages to traverse switchboard
      exit(1);
    }
  cinfo.err = jpeg_std_error(&jerr);
  jpeg_create_decompress(&cinfo);
  jpeg_stdio_src(&cinfo, pInfile);
  jpeg_read_header(&cinfo, TRUE);
  jpeg_start_decompress(&cinfo);
  *pWidth = cinfo.output_width;
  *pHeight = cinfo.output_height;
  jpeg_destroy_decompress(&cinfo);
  fclose(pInfile);
}

/** This function loads a JPEG image into a preallocated RGB buffer. */
void   jpegFileRead(JSAMPLE *pImageBuf, char *pFilename)
{
  /* This routine is from IJG's example.c file for libjpeg. */
  /* Minor changes were made and comments were removed.     */

  struct jpeg_decompress_struct cinfo;
  struct jpeg_error_mgr         jerr;
  JSAMPARRAY                    buffer;
  FILE       *pInfile;
  int        rowStride;
  long       nextScanline;

  if ((pInfile = fopen(pFilename, "rb")) == NULL)
    {
      LOGE("JPEG: can't open %s, EXIT!\n", pFilename);
      usleep(500000);  // Allow short time for messages to traverse switchboard
      exit(1);
    }

  /* Step 1: allocate and initialize JPEG decompression object */
  cinfo.err = jpeg_std_error(&jerr);
  jpeg_create_decompress(&cinfo);

  /* Step 2: specify data source (eg, a file) */
  jpeg_stdio_src(&cinfo, pInfile);

  /* Step 3: read file parameters with jpeg_read_header() */
  (void)jpeg_read_header(&cinfo, TRUE);
  
  /* Step 4: set parameters for decompression */
  /* In this example, we don't need to change any of the defaults set by
   * jpeg_read_header(), so we do nothing here.
   */

  /* Step 5: Start decompressor */
  (void) jpeg_start_decompress(&cinfo);
  rowStride = cinfo.output_width * cinfo.output_components;
  buffer = (*cinfo.mem->alloc_sarray)
    ((j_common_ptr) &cinfo, JPOOL_IMAGE, rowStride, 1);

  /* Step 6: while (scan lines remain to be read) */
  nextScanline = 0;
  while (cinfo.output_scanline < cinfo.output_height) {
    (void) jpeg_read_scanlines(&cinfo, buffer, 1);
    memcpy(&pImageBuf[nextScanline], buffer[0], rowStride);
    nextScanline += rowStride;
  }

  /* Step 7: Finish decompression */
  (void) jpeg_finish_decompress(&cinfo);

  /* Step 8: Release JPEG decompression object */
  jpeg_destroy_decompress(&cinfo);
  fclose(pInfile);
}

/** This function writes a JPEG file based on the provided RGB buffer.
 *
 * The image width and height must be provided, and the quality
 * parameter (usually between 70 and 95) controls the degree of
 * compression.
 */
void   jpegFileWrite(JSAMPLE *pImageBuf, char *pFilename,
		     int width, int height, int quality)
{
  /* This routine is from IJG's example.c file for libjpeg. */
  /* Minor changes were made and comments were removed.     */

  struct jpeg_compress_struct cinfo;
  struct jpeg_error_mgr       jerr;
  JSAMPROW                    rowPointer[1];
  FILE   *pOutfile;
  int    rowStride;

  /* Step 1: allocate and initialize JPEG compression object */
  cinfo.err = jpeg_std_error(&jerr);
  jpeg_create_compress(&cinfo);

  /* Step 2: specify data destination (eg, a file) */
  if((pOutfile = fopen(pFilename, "wb")) == NULL)
    {
      LOGE("JPEG: can't open %s, EXIT!\n", pFilename);
      usleep(500000);  // Allow short time for messages to traverse switchboard
      exit(1);
    }
  jpeg_stdio_dest(&cinfo, pOutfile);

  /* Step 3: set parameters for compression */
  cinfo.image_width = width;
  cinfo.image_height = height;
  cinfo.input_components = 3;
  cinfo.in_color_space = JCS_RGB;
  jpeg_set_defaults(&cinfo);
  jpeg_set_quality(&cinfo, quality, TRUE);

  /* Step 4: Start compressor */
  jpeg_start_compress(&cinfo, TRUE);

  /* Step 5: while (scan lines remain to be written) */
  /*           jpeg_write_scanlines(...); */
  rowStride = width * 3;
  while(cinfo.next_scanline < cinfo.image_height)
    {
      rowPointer[0] = & pImageBuf[cinfo.next_scanline * rowStride];
      (void) jpeg_write_scanlines(&cinfo, rowPointer, 1);
    }

  /* Step 6: Finish compression */
  jpeg_finish_compress(&cinfo);
  fclose(pOutfile);

  /* Step 7: release JPEG compression object */
  jpeg_destroy_compress(&cinfo);

  /* And we're done! */
}

/** This function passes back the width and height of a PNG image file.
 *
 * This routine is helpful for determining how large an image buffer
 * to allocate before reading a file.
 */
void   pngFileGetImageSize(char *pFilename, int *pWidth, int *pHeight)
{ 
  int    n;
  FILE   *pInfile;
  unsigned char   pHeader[8];

  if ((pInfile = fopen(pFilename, "rb")) == NULL)
    {
      LOGE("PNG: can't open %s, EXIT!\n", pFilename);
      usleep(500000);  // Allow short time for messages to traverse switchboard
      exit(1);
    }
  n = fread(pHeader, 1, 8, pInfile);
  if (png_sig_cmp(pHeader, 0, 8))
    {
      LOGE("PNG: %s is not a PNG file, EXIT!\n", pFilename);
      usleep(500000);  // Allow short time for messages to traverse switchboard
      exit(1);
    }
  png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  png_infop info = png_create_info_struct(png);
  png_init_io(png, pInfile);
  png_set_sig_bytes(png, 8);
  png_read_info(png, info);
  *pWidth      = png_get_image_width(png, info);
  *pHeight     = png_get_image_height(png, info);
  fclose(pInfile);
}

/** This function loads a PNG image into a preallocated RGBA buffer.
 *
 * Note that this is a basic PNG file reader that will not be robust
 * to all format variations. In particular, this version assumes that
 * the PNG file has an alpha channel in RGBA format.
*/
void   pngFileRead(unsigned char *pImageBuf, char *pFilename)
{
  int    n, y, width, height;
  long   i, stride;
  FILE   *pInfile;
  unsigned char   pHeader[8];
  png_bytep *pRowData;
  
  if ((pInfile = fopen(pFilename, "rb")) == NULL)
    {
      LOGE("PNG: can't open %s, EXIT!\n", pFilename);
      usleep(500000);  // Allow short time for messages to traverse switchboard
      exit(1);
    }
  n = fread(pHeader, 1, 8, pInfile);
  if (png_sig_cmp(pHeader, 0, 8))
    {
      LOGE("PNG: %s is not a PNG file, EXIT!\n", pFilename);
      usleep(500000);  // Allow short time for messages to traverse switchboard
      exit(1);
    }
  png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  png_infop info = png_create_info_struct(png);
  png_init_io(png, pInfile);
  png_set_sig_bytes(png, 8);
  png_read_info(png, info);
  width  = png_get_image_width(png, info);
  height = png_get_image_height(png, info);
  png_read_update_info(png, info);
  pRowData = (png_bytep*) malloc(sizeof(png_bytep) * height);
  i = 0;
  stride = width * 4; // assume RGBA format
  for (y=0; y<height; y++)
    {
      pRowData[y] = (png_byte *) &pImageBuf[i];
      i += stride;
  }
  png_read_image(png, pRowData);
  fclose(pInfile);
}
