/*
 * debugInfo.cpp
 *
 *  Created on: Apr 6, 2018
 *      Author: tom
 */

#include "debugInfo.hpp"
#include "logging.hpp"
#include "shellProcess.hpp"
#include "switchboard/switchboardMessages.hpp"

#include <string.h>
#include <unistd.h>


/** Constants defining commands and temporary output file. */
#ifdef __ANDROID__
static const std::string kDebugOutFile = "/data/OwlLabs/debugOut.tmp";
static const std::string kLogcatCmd = "logcat -b all -d ";
static const std::string kDmesgCmd = "dmesg ";
#else
static const std::string kDebugOutFile = "./debugOut.tmp";
static const std::string kLogcatCmd = "./logcat -b all -d ";
static const std::string kDmesgCmd = "dmesg ";
#endif


/** Private constructor for the object.
 *
 */
cDebugInfo::cDebugInfo ()
  : mThread(std::bind(&cDebugInfo::runIt, this), "debugInfo")
{
}

/** Public constructor that all clients must call.
 *
 * @param pSwitchboardClient Points to the switchboard client interface object.
 * @param command is the command to run in this thread.
 * @param params is a string containing 0 or more parameters for the command.
 */
cDebugInfo::cDebugInfo (cSwitchboardClient *pSwitchboardClient,
                                  std::string command,
                                  std::string params)
  : mpSwitchboardClient(pSwitchboardClient),
    mCommand(command),
    mParams(params),
    mDone(false),
    mThread(std::bind(&cDebugInfo::runIt, this), "debugInfo")
{
  mThread.start();
}

/** Destroyer for the object - this is self-called when done.
 *
 */
cDebugInfo::~cDebugInfo ()
{
  mThread.signalStop();
  mThread.join();
}

/** Return false while the operation is in process, true otherwise.
 *
 */
bool cDebugInfo::done(void)
{
  return mDone;
}

/** Thread to transfer debug information output to the switchboard.
 *
 * An independent thread runs this function to execute the debugging
 * command saving output to a file, then sends a header containing
 * the length of the resultant output file followed by the
 * contents of the file in message sized chunks.
 */
void cDebugInfo::runIt(void)
{
  uint8_t pMsg[OIPC_MAX_MSG_LEN];
  std::string cmd;

  mDone = false;
  switch (mCommand.c_str()[0])
  {
    case 'l':
      cmd += kLogcatCmd;
      cmd += mParams + " > " + kDebugOutFile;
      break;
    case 'd':
      cmd = kDmesgCmd + " > " + kDebugOutFile;
      break;
    default:
      cmd = "";
      break;
  }
  LOGI("runIt(): command: %s\n", cmd.c_str());

  if (cmd.length() > 0)
    {
      // Run the command; stdout is redirected to the temporary output file
      cShellProcess runCmd(cmd, nullptr, true);

      // Open the output file
      FILE *pOutFile = fopen(kDebugOutFile.c_str(), "rb");
      uint32_t fileSize = 0;
      if (pOutFile != nullptr)
        {
          // Determine the size of the output file to send in the header
          fseek(pOutFile, 0L, SEEK_END);
          fileSize = ftell(pOutFile);
          fseek(pOutFile, 0L, SEEK_SET);
        }
      // Prepare and send the header
      transferHeader_t header = { .mDebugInfoLength = fileSize };
      memcpy(pMsg, (void *)&header, sizeof(header));
      mpSwitchboardClient->writeMessage(DEBUG_INFO_HEADER_MSG,
                                        (char *)pMsg,
                                        sizeof(header));

      // Send the data in chunks the size of the largest message
      int numChunks = 1 + fileSize / OIPC_MAX_MSG_LEN;
      for (int chunk = 0; chunk < numChunks; chunk++)
        {
          usleep(1000);   // Try not to flood the socket
          int numRead = fread(pMsg, 1, OIPC_MAX_MSG_LEN, pOutFile);
          mpSwitchboardClient->writeMessage(DEBUG_INFO_CHUNK_MSG,
                                            (char *)pMsg,
                                            numRead);
        }

      // Delete the temporary output file
      unlink(kDebugOutFile.c_str());
    }

  mThread.signalStop();   // Run this thread function once only
  mDone = true;
}
