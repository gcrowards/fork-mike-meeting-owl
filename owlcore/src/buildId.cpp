/** Copyright 2017 Owl Labs Inc.
 *  All rights reserved.
 *
 *  buildId.hpp
 *  This is the implementation file for cBuildId.
 *   --> Static class that provides global access the the ID of the current
 *        software build. The build ID is retrieved during initialization of
 *        static members before main() is entered.
 */
#include "buildId.hpp"
#include "logging.hpp"
#include "shellProcess.hpp"

// retrieve the build id during static member initialization.
const std::string cBuildId::mkInvalidBuildIdStr = "UNKNOWN";
const std::string cBuildId::mkBuildIdStr = cBuildId::retrieveBuildId();

/** Retrieve the 'build ID' of the meetingOwl software. */
std::string cBuildId::retrieveBuildId()
{
  std::string message = "";

#ifdef __ANDROID__
  /* Create a new process to run the 'getprop' command, and then filter the
   * output looking for lines containing "ro.build.id". The complete
   * retrieved line will look like: "[ro.build.id]: [20008]".
   */
  cShellProcess findBuildID(std::string("getprop | grep \"ro.build.id\""),
  /* The following lambda function tokenizes each line returned by the
   * 'getprop' system call, then finds the line containing 'ro.build.id',
   * then puts the message into the string pMsg.
   */
  [&](const std::string *pLine)
  {
    if (pLine != nullptr)
      {
        // Note: this overwrites with each new match to return the LAST one!
        message = *pLine;
        return true;
      }
    else
      { return false; }
  },
  true );
#endif
  
  std::string buildId = formatBuildId(message);
  if (!buildId.empty())
    {
      LOGI("build ID: %s\n", buildId.c_str());
      return buildId;
    }
  else
    {
      LOGW("Unknown build ID.\n");
      return mkInvalidBuildIdStr;
    }
}

/** Extract the number substring, checking validity in the process.
 *
 * @param message the shell message returned in init().
 * @return the formatted build ID, or an empty string if failed.
 */
std::string cBuildId::formatBuildId(const std::string &message)
{
  std::string buildId = "";
  if (!message.empty())
    {
      // Find the ":" separator
      const int colon = message.find(':');
      if (colon != std::string::npos)
        {
          // Find the "[" and "]"
          const int openBracket = message.find('[', colon);
          const int closeBracket = message.find(']', colon);
	  
          // If both have been found, they bracket an ID!
          if ((openBracket != std::string::npos) &&
              (closeBracket != std::string::npos) )
            {
              // Extract the number contained by the brackets
	      buildId = message.substr(openBracket + 1,
				       closeBracket - openBracket - 1 );
	      // Insert periods to demarcate major, minor, patch revs (XX.XX.XX.XX)
	      const int decimalPos = buildId.length() - 2;
	      buildId.insert(decimalPos, ".");
	      buildId.insert(decimalPos - 2, ".");
	      if (decimalPos > 4)
	        { buildId.insert(decimalPos - 4, "."); }
            }
        }
    }
  return buildId;
}

/** @return a string with the current build ID. */
std::string cBuildId::getString()
{
  return mkBuildIdStr;
}

/** @return whether build ID retrieval was successful. */
bool cBuildId::isValid()
{
  return (!mkBuildIdStr.empty() && mkBuildIdStr != mkInvalidBuildIdStr);
}
