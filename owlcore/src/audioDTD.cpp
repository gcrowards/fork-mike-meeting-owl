/*
* Implements the double talk statemachine
*/
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <math.h>
#include <pthread.h>

#include "logging.hpp"
#ifdef __ANDROID__
#include "audioframework.h"
#include "AudioMgr.hpp"
#else
#include "AudioMgr.hpp"
#endif
#define FLOATING_POINT


#if __ANDROID__
extern "C" int CalcRMS(short *pBuff, int len, int channel, int stride);
#else
int CalcRMS(short *pBuff, int len, int channel, int stride)
{
  return 0;
}
#endif
extern float ARMAFilter(float lastVal, float newVal, float upCoeff,
                        float downCoeff);

#if __ANDROID__
#ifdef __cplusplus
extern "C" {
#endif
extern unsigned long long getMsecs();

#if defined(__cplusplus)
}  /* extern "C" */
#endif
#endif

#include "audioDTD.hpp"

typedef struct
{
  FLOAT refLevel;
  FLOAT atten;
  FLOAT threshold;
} THRESH_TABLE;



char dtdRefName[] = "DTDRef";
char dtdAecName[] = "DTDAec";
char dtdMicName[] = "DTDMic";


#ifdef __ANDROID__
extern FLOAT pow2db(FLOAT value);
extern FLOAT mag2db(FLOAT value);

#endif // __ANDROID__


/** Calculates an RMS value for a buffer.
*
* @param pBuff - pointer to a buffer of floats
* @param len - number of samples to process
*
* @return rms value of buffer
*/
FLOAT RMSBufferShort(short *pBuff, int len)
{
  FLOAT sum = 0;
  FLOAT temp;
  int i;

  for (i = 0; i < len; ++i)
  {
    temp = (FLOAT)(*pBuff++) / 32768.0;
    sum += (temp * temp);
  }

  return sqrt(sum / len);
}
/** Calculates an Total power value for a buffer
*
* @param pBuff - pointer to a buffer of floats
* @param len - number of samples to process
*
* @return total power value of buffer
*/
FLOAT PowerBufferShort(short *pBuff, int len)
{
  FLOAT sum = 0;
  FLOAT temp;
  int i;

  for (i = 0; i < len; ++i)
  {
    temp = (FLOAT)(*pBuff++) / 32768.0;
    sum += (temp * temp);
  }

  return(sum);

}

/** manages DTD debug files
*
* @param dirname - destination directory name for DTD debug files
* @param basename - basename for the DTD debug files.
*      params are only used for PC based debug
* @return 0
*/
int cAudioDTD::openDebug(char * pDirname, char *pBasename)
{
#ifdef DEBUG_DTD
#ifdef __ANDROID__

  AUDIO_PROPERTIES props;
  int returnVal = 0;

  GetAudioProperties(&props);

  static int file_num = 0;
  static char file_template[] = "/data/audio/dtd_debug_%d.csv";
  static char filename[1024];

  if(mpDebugDTDFd != NULL)
  {
    fclose(mpDebugDTDFd);
    mpDebugDTDFd = NULL;
  }
  sprintf(filename, file_template, file_num);

  mpDebugDTDFd = fopen(filename, "wb");
  printf("Opened DTD debug file %s \n", filename);

  if(mpMicDebugAudioFd != NULL)
  {
    fclose(mpMicDebugAudioFd);
    mpMicDebugAudioFd = NULL;
  }

  if(mpRefDebugAudioFd != NULL)
  {
    fclose(mpRefDebugAudioFd);
    mpRefDebugAudioFd = NULL;
  }



  if(props.aecDebugAudio != 0)
  {
    static char mic_file_template[] = "/data/audio/mic_aec_%d.raw";
    sprintf(filename, mic_file_template, file_num);
    mpMicDebugAudioFd = fopen(filename, "wb");
    printf("Opened Mic aec debug file %s \n", filename);

    static char ref_file_template[] = "/data/audio/ref_aec_%d.raw";
    sprintf(filename, ref_file_template, file_num);
    mpRefDebugAudioFd = fopen(filename, "wb");
    printf("Opened Ref aec debug file %s \n", filename);

  }

  file_num++;

#endif
#endif
  return 0;

}

void cAudioDTD::intResetStats()
{
  mStats.silenceTime = 0;
  mStats.remoteTime = 0;
  mStats.localTime = 0;
  mStats.bothTime = 0;
}
void cAudioDTD::accumStats(int samples)
{
  float timeInterval;
  DTDSTATS tempStats;
#ifdef DEBUG_DTD_STATS
  static int dbgSampleCount = 0;
  dbgSampleCount += samples;
  if(dbgSampleCount > (DEFAULT_FS * 5))
  {
    DTDGetStatistics(&tempStats, 0);
    printf("%d Silence = %f; Local = %f; Remote = %f; Both = %f \n", dbgSampleCount,
           tempStats.silenceTime,
           tempStats.localTime,
           tempStats.remoteTime,
           tempStats.bothTime);
    dbgSampleCount = 0;
  }
#endif
  timeInterval = (float)samples / (float)DEFAULT_FS;
  switch(mCurrState)
  {
  case STATE_SILENCE:
    mStats.silenceTime += timeInterval;
    break;
  case STATE_LOCAL_SINGLE_TALK:
    mStats.localTime += timeInterval;
    break;
  case STATE_REMOTE_SINGLE_TALK:
    mStats.remoteTime += timeInterval;
    break;
  case STATE_DOUBLE_TALK:
    mStats.bothTime += timeInterval;
    break;
  default:
    break;
  }
}
/** returns the current statistics values and optionally resets them.
*
* @param pStats - pointer to a structure to be filled in with statistics
* @param reset - If set to one then the statistics will be reset after being
* copied; otherwise the stats will continue to accumulate.
*
* @return - 1
*/
int cAudioDTD::DTDGetStatistics(DTDSTATS *pStats, int reset)
{
  *pStats = mStats;
  if(reset == 1)
  {
    resetStats();
  }

  return 1;
}
/** returns the silenceTime statistic
*
* @return - silenceTime
*/
float cAudioDTD::getSilenceTime(void)
{
  return mStats.silenceTime;
}
/** returns the silenceTime statistic
*
* @return - silenceTime
*/

float cAudioDTD::getRemoteTime(void)
{
  return mStats.remoteTime;
}
/** returns the silenceTime statistic
*
* @return - silenceTime
*/

float cAudioDTD::getLocalTime(void)
{
  return mStats.localTime;
}
/** returns the silenceTime statistic
*
* @return - silenceTime
*/

float cAudioDTD::getBothTime(void)
{
  return mStats.bothTime;
}

/** resets the statistics
*/

void cAudioDTD::resetStats(void)
{
  intResetStats();
};
/** Constructor for double talk detector object.
*/
cAudioDTD::cAudioDTD()
{
  int i;
  int status;
  int nrAmount;
#ifdef DEBUG_DTD
  mpMicDebugAudioFd = NULL;
  mpRefDebugAudioFd = NULL;
  mpOutDebugAudioFd = NULL;
#endif

  //mpNrBuff = (short *)malloc(frameCount*sizeof(short)*4);
  mpNrBuff = &mNrBuffer[0];
  mpSpeexDTD_NR  = NULL;

  resetDTD();
#ifdef __ANDROID__
  CircBuffInit(&mRefCB, DTD_PROC_SIZE * 4, DTD_CB_ITEM_SIZE, dtdRefName);
  CircBuffInit(&mAecCB, DTD_PROC_SIZE * 4, DTD_CB_ITEM_SIZE, dtdAecName);
  CircBuffInit(&mMicCB, DTD_PROC_SIZE * 4, DTD_CB_ITEM_SIZE, dtdMicName);
#endif



}

/** initializes the state for double talk detector noise reducer
*


*/

int cAudioDTD::SetupDTDNR(SpeexEchoState *pEchoState, int fs, int frameCount)
{

  int status;
  int nrAmount;
#ifdef __ANDROID__

  //mpNrBuff = (short *)malloc(frameCount*sizeof(short)*4);
  mpNrBuff = &mNrBuffer[0];

  if(mpSpeexDTD_NR != NULL)
  {
    speex_preprocess_state_destroy(mpSpeexDTD_NR);
    mpSpeexDTD_NR = NULL;
  }

  mpSpeexDTD_NR = speex_preprocess_state_init(frameCount, fs);
  status = speex_preprocess_ctl(mpSpeexDTD_NR, SPEEX_PREPROCESS_SET_ECHO_STATE,
                                pEchoState);

  nrAmount = 6;
  status = speex_preprocess_ctl(mpSpeexDTD_NR,
                                SPEEX_PREPROCESS_SET_NOISE_SUPPRESS,
                                &nrAmount);
#endif

  return 0;
}


/** Resets the internal state of the Double talk detector
*/
int cAudioDTD::resetDTD()
{
  resetStats();
  mMicRMSSilence = 0;
  mRefRMS = 0;

  mCurrState = STATE_SILENCE;
  mLastState = STATE_SILENCE;
  mLastNonSilentState = STATE_REMOTE_SINGLE_TALK;
  mLastNonSilentTime = 0;
  mStateChangeTime = 0;

  mLastDTDPossible = 0;

  FIRMeanFilterInit(mAecFIR);
  FIRMeanFilterInit(mAecFIR_p);
  FIRMeanFilterInit(mMicFIR_p);

  mAecRMSFiltered_p = 0;
  mMicRMSFiltered_p = 0;
  mAecRMSFiltered = 0;
  mMicRMSLt = 0;
  mAecRMSLt = 0;
  mMicRMSLt = 0;
  mRefRMSLt = 0;
  mRefActRMSLt = 0;
  mClipCount = 0;
  mClipState = CLIP_NO_CLIPPING;
  mClipStateInt = CLIP_NO_CLIPPING;
  mClipTime = 0;
  MinStatisticsFilterInit(mMinAecRMS, AEC_MIN_DEFAULT, RMS_MIN_VALUE,
                          AEC_STAT_DECAY);
  MinStatisticsFilterInit(mMinMicRMS, db2mag(-60), db2mag(-96),
                          MIC_SILENCE_LEVEL_DECAY);
  MinStatisticsFilterInit(mMinRefRMS, REF_MIN_DEFAULT,
                          db2mag(REF_DTD_MIN_SILENCE_THRESH), SILENCE_LEVEL_DECAY);
  mConvergedState = 0;
  mEchoAdapted = 0;

#ifdef DEBUG_DTD
  printf("DTD %llu - resetting to start state \n", getMsecs());
#endif


#ifdef __ANDROID__
  CircBuffRestart(&mRefCB);
  CircBuffRestart(&mAecCB);
  CircBuffRestart(&mMicCB);
#endif

  openDebug(NULL, NULL);
  return 0;


}

/** Destructor for double talk detector object
*/

cAudioDTD::~cAudioDTD()
{
#ifdef DEBUG_DTD
  if (mpDebugDTDFd != NULL)
  {
    fclose(mpDebugDTDFd);
  }
  if (mpMicDebugAudioFd != NULL)
  {
    fclose(mpMicDebugAudioFd);
  }
  if (mpRefDebugAudioFd != NULL)
  {
    fclose(mpRefDebugAudioFd);
  }

#endif
}

/** Returns the current state of the double talk detector
*
* @return state of the double talk detector
*/
DTDStates cAudioDTD::DTDGetState()
{
  return mCurrState;
}

/** Initializes the state for a minimum statistics filter.
*
* @param state - State structure to be initialized.
* @param defaultValue - The default value for memorrya and starts
* @param minAccept - The minimum value to accept as legal
*   (prevents propblems with 0s)
*
* @return 0
*/


int cAudioDTD::MinStatisticsFilterInit(MIN_STAT &state, FLOAT defaultValue,
                                       FLOAT minAccept, FLOAT decayRate)
{
  int i;
  for (i = 0; i < MIN_STAT_COUNT; ++i)
  {
    state.minMem[i] = defaultValue;
    state.minMaxMem[i] = 0;
  }
  state.minCurrMax = 0;
  state.minCount = 0;
  state.minCurrValue = 0;
  state.minLastValue = defaultValue;
  state.minMinValue = defaultValue;
  state.minMinMaxValue = 0;
  state.minSum = 0;
  state.minAcceptableValue = minAccept;
  state.minStartState = 1;
  state.minDecayRate = decayRate;

  return 0;
}

int cAudioDTD::MinStatisticsFilterReset(MIN_STAT &state)
{
  state.minSum = 0;
  state.minCount = 0;
  state.minCurrMax = 0;
  return 0;
}

FLOAT cAudioDTD::MinStatisticsFilterAccum(MIN_STAT &minState, FLOAT currValue)
{
  minState.minSum += currValue;
  minState.minCount += 1;
  if (minState.minCurrMax < currValue)
  {
    minState.minCurrMax = currValue;
  }
  return 0;
}

FLOAT cAudioDTD::MinStatisticsFilterCalc(MIN_STAT &minState)
{
  FLOAT tempMinVal = 0;
  int i;
  // leaving a remote talking state;
  // Calculate metric
  // and save it if it is small enough.
  if (minState.minCount >= MIN_STAT_EVENT_COUNT)
  {
    tempMinVal = minState.minSum / minState.minCount;
    minState.minCurrValue = tempMinVal;

    if (minState.minStartUp < MIN_STAT_COUNT)
    {
      minState.minMem[minState.minStartUp] = tempMinVal;
      minState.minSum = 0;
      minState.minCount = 0;
      minState.minStartUp++;
      return tempMinVal;
    }
    else
    {

      // if the value we have found is > than the min acceptable value
      if (tempMinVal > minState.minAcceptableValue)
      {
        FLOAT maxMinValue;
        int index;
        minState.eventCount++;
        if (minState.minStartState == 0)
        {
          maxMinValue = minState.minMem[0];
          index = 0;
          // search for the minimn of the min values;
          for (i = 1; i < MIN_STAT_COUNT; ++i)
          {
            if (minState.minMem[i] < maxMinValue)
            {
              maxMinValue = minState.minMem[i];
              index = i;
            }
          }
          minState.minMem[index] = minState.minMem[index] * minState.minDecayRate;
          maxMinValue = minState.minMem[0];
          index = 0;
          // search for the maximum of the min values;
          for (i = 1; i < MIN_STAT_COUNT; ++i)
          {
//            minState.minMem[i] = minState.minMem[i] * minState.minDecayRate; //hmbt
            if (minState.minMem[i] > maxMinValue)
            {
              maxMinValue = minState.minMem[i];
              index = i;
            }
          }
          // set the last value calced.
          minState.minLastValue = tempMinVal;
          // if the last vaule calced is less than the maximum value; replace the min in the array.
          if (minState.minLastValue < maxMinValue)
          {
            minState.minMem[index] = minState.minLastValue;
            minState.minMaxMem[index] = minState.minCurrMax;
          }
        }
        else
        {
          // if we are just starting, populate the min matrix with the first calced value.
          for (i = 1; i < MIN_STAT_COUNT; ++i)
          {
            minState.minMem[i] = tempMinVal;
            minState.minMaxMem[i] = minState.minCurrMax;
          }
          minState.minStartState = 0;
        }
      }
      FLOAT tempSum = 0;
      FLOAT tempMaxValue = 0;
      FLOAT tempMaxSum = 0;
      // sum all of the minimum values;
      // find the maximum of the mimum values.
      for (i = 0; i < MIN_STAT_COUNT; ++i)
      {
        tempSum += minState.minMem[i];
        tempMaxSum += minState.minMaxMem[i];
        if (minState.minMaxMem[i] > tempMaxValue)
        {
          tempMaxValue = minState.minMaxMem[i];
        }
      }
      // save the maximum value from the minimum intervals.
      minState.minMinMaxValue = tempMaxValue;
      // Average the minimum stats
      minState.minMinValue = tempSum / MIN_STAT_COUNT;
      // reset the state counters
      minState.minSum = 0;
      minState.minCount = 0;
    }
  }
  else
  {
    minState.rejectCount++;
  }
  return minState.minLastValue;
}



/** Maintains the lowest mic and speaker active segment
*  this is assumed to be a remote single talk segment and is used to caclulate
*  the threshold for the DTD.
*
*  @param currValue - The current value for filter
*  @param currState - the next state of the DTD
*  @param reset - currently not used
*
*  @return the last averge snr

*/
FLOAT cAudioDTD::AecMinStatisticsFilter(MIN_STAT &minState, FLOAT currValue,
                                        DTDStates currState, int reset)
{
  int DTDPossible = 0;
  static int frameCount = 0;
  frameCount++;

  // We are loooking for the mean of the N lowest aec power measurements
  // Since Double talk events are much higher power, it is assumed that
  // these are remote single talk and form an estimate of the power
  // during remte single talk.
  // THis is only calculated during remote active staes
  switch (currState)
  {
  case STATE_SILENCE:
  case STATE_LOCAL_SINGLE_TALK:
  case STATE_BAD_STATE:
    DTDPossible = 0;
    break;
  case STATE_REMOTE_SINGLE_TALK:
    MinStatisticsFilterAccum(minState, currValue);
    DTDPossible = 1;
    break;
  case STATE_DOUBLE_TALK:
    DTDPossible = 1;
//    MinStatisticsFilterAccum(minState, currValue);
    break;
  }

  if ((mLastDTDPossible == 0) && (DTDPossible == 1))
  {
    MinStatisticsFilterReset(minState);
    // Entering a remote talking state.
  }
  else if (((mLastDTDPossible == 1) && (DTDPossible == 0)) ||
//    ((mLastDTDPossible == 1) && (minState.minCount > MIN_STAT_MAX_EVENT_COUNT)))
           ((mLastDTDPossible == 1)))
  {
    MinStatisticsFilterCalc(minState);
  }
  mLastDTDPossible = DTDPossible;

  return minState.minLastValue;

}

/** Initialize the FIR mean filter
*
* @param state - FIlter state
*
* @return 0;
*/
int cAudioDTD::FIRMeanFilterInit(FIR_MEAN_STATE &state)
{
  int i;

  for (i = 0; i < FIR_MEAN_FILTER_SIZE; ++i)
  {
    state.firMem[i] = 0;
  }
  state.firInd = 0;
  state.firCurrVal = 0;
  state.firSum = 0;

  return 0;
}
/** Process a value through the FIR mean filter and get the filtered value back
*
* @param state - FIlter state
* @param input - new input value to the filter
*
* @return output - output of the filter
*/
FLOAT cAudioDTD::FIRMeanFilter(FIR_MEAN_STATE &state, FLOAT input)
{
  FLOAT temp;

  state.firSum = state.firSum - state.firMem[state.firInd] + input;
  state.firMem[state.firInd] = input;
  state.firInd++;
  if (state.firInd >= FIR_MEAN_FILTER_SIZE)
  {
    state.firInd = 0;
    int i;
    state.firSum = 0;
    for (i = 0; i < FIR_MEAN_FILTER_SIZE; ++i)
    {
      state.firSum += state.firMem[i];
    }
  }
  // round off errors have caused a not valid result; recalculate current value
  if (state.firSum < 0)
  {
    int i;
    state.firSum = 0;
    for (i = 0; i < FIR_MEAN_FILTER_SIZE; ++i)
    {
      state.firSum += state.firMem[i];
    }
  }
  state.firCurrVal = state.firSum / FIR_MEAN_FILTER_SIZE;

  return state.firCurrVal;
}


/** Indicates that the echo canceller has converged and that the DTD indications are valid
*
* @return 1 if the double talk indications are valid;
*
*/
int cAudioDTD::DTDAvailable()
{
  if ((mCurrState == STATE_REMOTE_SINGLE_TALK) && (mEchoAdapted == 0))
  {
    return -1;
  }
  if (mCurrState == STATE_DOUBLE_TALK)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/** Checks for clipping in the reference signal and sets mClipState
*
* @param refAudio - pointer to audio buffer to look for clipping
* @param len - len of the audio buffer
*
* @returns - 1 if clipping found 0 otherwise.
* sets mClipState appropriately
*
*/
int cAudioDTD::CheckClipping(short *refAudio, int len)
{
  int i;
  int currState = CLIP_NO_CLIPPING;
  // check this buffer for clipping.
  for (i = 0; i < len; ++i)
  {
    if (abs(refAudio[i]) > CLIP_LEVEL)
    {
      mClipCount++;
      if (mClipCount >= CLIP_DURATION)
      {
        currState = CLIP_CLIPPING;
      }
    }
    else
    {
      mClipCount = 0;
    }
  }


  if (currState == CLIP_CLIPPING)
  {
    mClipState = CLIP_CLIPPING;
    mClipStateInt = CLIP_CLIPPING;
    mClipTime = getMsecs() + CLIP_HYSTERSIS_TIME;
  }
  else
  {
    // if previously clipping wait hysterisis times.
    if (getMsecs() >= mClipTime)
    {
      mClipState = CLIP_NO_CLIPPING;
      mClipStateInt = CLIP_NO_CLIPPING;
    }
    else
    {
      mClipStateInt = CLIP_HYS;
    }
  }

  if (mClipState == CLIP_CLIPPING)
  {
    return 1; // if there is clipping detected, then prevent double talk
  }
  else
  {
    return 0;
  }
}

/** Process the audio data and determines the current state of speech
*
* @param pMicIn - pointer to a buffer of mic data
* @param pAecIn - pointer to a buffer of data that has been processed by AEC
* @param pRefIn - pointer the reference data from the speaker out
* @param len - Number of frames in each of the buffers
* @param pSpxDetectors - Pointer to a structure obtained from speex lib
*       that contains info about echo canceller and noise suppressor
*
* @return returns the state of the connection
*/
DTDStates cAudioDTD::ProcessDTD(short *pMicIn, short *pAecIn, short *pRefIn,
                                int len, DETECTORS *pSpxDetectors)
{
  static int lastAda = 0;
  FLOAT micRMS;
  FLOAT aecRMS;
  FLOAT aecRMS_p;
  FLOAT refRMS;
  FLOAT micRMSDbSilence;
  FLOAT micRMS_p;
  FLOAT refRMSDb;
  int refActive;
  int micActive;
  DTDStates nextState;
  FLOAT atten;
  // Determine DTD from the last X minimum values
  AUDIO_PROPERTIES props;
  static float lastBias = -1;
  GetAudioProperties(&props);

#ifdef __ANDROID__
#ifdef DEBUG_DTD
  if(props.aecDebugAudio != lastAda)
  {
    openDebug(NULL, NULL);
  }

  lastAda = props.aecDebugAudio;

  if(props.aecDebugAudio == 1)
  {
    if(mpMicDebugAudioFd != NULL)
    {
      fwrite(pMicIn, sizeof(short), len, mpMicDebugAudioFd);
    }
    if(mpRefDebugAudioFd != NULL)
    {
      fwrite(pRefIn, sizeof(short), len, mpRefDebugAudioFd);
    }
  }
#endif

  memcpy(mpNrBuff, pAecIn, len * sizeof(short));
  speex_preprocess_run(mpSpeexDTD_NR, mpNrBuff);

#endif

  mEchoAdapted = pSpxDetectors->echoAdapted;

  /// make sure the len is the length we expect or their could be buffer
  /// overruns
  if (len != DTD_PROC_SIZE)
  {
    LOGE("ProcessDTD - bad input length %d %d \n", len, DTD_PROC_SIZE);
    return STATE_SILENCE;
  }


  CheckClipping(pRefIn, len); // check for clipping

  // Calculate the RMS value for this buffer of mic data
  // need two different value for differnt purposes.
  micRMS = RMSBufferShort(pMicIn, len);
  micRMS_p = PowerBufferShort(pMicIn, len);
  mMicRMSFiltered_p = FIRMeanFilter(mMicFIR_p, micRMS_p);
  if(mMicRMSFiltered_p < 1e-6)
  {
    mMicRMSFiltered_p = 1e-6;
  }




  mMicRMSSilence = ARMAFilter(mMicRMSSilence, micRMS, MIC_SILENCE_RMS_UP_ARMA,
                              MIC_SILENCE_RMS_DOWN_ARMA);
  micRMSDbSilence = mag2db(mMicRMSSilence);
  mMicRMSLt = ARMAFilter(mMicRMSLt, micRMS, RMS_LT_ARMA, RMS_LT_ARMA);

  // Calculate the RMS value for this buffer of refernce data
  refRMS = RMSBufferShort(pRefIn, len);
  mRefRMS = ARMAFilter(mRefRMS, refRMS, REF_RMS_UP_ARMA, REF_RMS_DOWN_ARMA);
  refRMSDb = mag2db(mRefRMS);
  CalcRMS(pRefIn, len, 0, 1);

  // Calculate the total power in the echo cancelled signal
//  aecRMS_p = PowerBufferShort(pAecIn, len);
//  aecRMS   = RMSBufferShort(pAecIn, len);
  aecRMS_p = PowerBufferShort(mpNrBuff, len);
  aecRMS   = RMSBufferShort(mpNrBuff, len);


  if (mAecRMSLt != 0)
  {
    mAecRMSLt = ARMAFilter(mAecRMSLt, aecRMS, RMS_LT_ARMA, RMS_LT_ARMA);
  }
  else
  {
    mAecRMSLt = aecRMS;
  }


  // determine which streams are active
  // IF they are loud enough they are actove
  if (refRMSDb > REF_DTD_SILENCE_THRESH)
  {
    refActive = 1;
  }
  else
  {
    refActive = 0;
  }

  if (micRMSDbSilence > MIC_DTD_SILENCE_THRESH)
  {
    micActive = 1;
  }
  else
  {
    micActive = 0;
  }

  static int calcMicMin = 0;
  if ((micActive == 0) && (refActive == 0))
  {
    MinStatisticsFilterAccum(mMinMicRMS, mAecRMSFiltered_p);
    calcMicMin = 1;
  }
  else
  {
    if (calcMicMin)
    {
      MinStatisticsFilterCalc(mMinMicRMS);
      calcMicMin = 0;
    }
  }

  mAecRMSFiltered_p = FIRMeanFilter(mAecFIR_p, aecRMS_p);
  if(mAecRMSFiltered_p < 1e-6)
  {
    mAecRMSFiltered_p = 1e-6;
  }

  mAecRMSFiltered = FIRMeanFilter(mAecFIR, aecRMS);

  if((pSpxDetectors->leakEstimate < CONVERGE_LEAK_THRESHOLD))
  {
    mConvergedState = 1;
  }
  else
  {
    mConvergedState = 0;
  }
#ifdef DEBUG_DTD
  static int lastConverged = 0;
  if(lastConverged != mConvergedState)
  {

    printf("%llu - Converged %d->%d\n", getMsecs(), lastConverged, mConvergedState);
  }
  lastConverged = mConvergedState;
#endif


  if ((refActive == 0) && (micActive == 0))
  {
    // if niether level is significant then we are in silence
    nextState = STATE_SILENCE;
  }
  else if ((refActive == 0) && (micActive == 1))
  {
// If mic is active and speaker is not then in local single talk
// calculate the mean Local Single Talk level for
// the threshold computation.
    nextState = STATE_LOCAL_SINGLE_TALK;
  }
  else if ((refActive == 1) && (micActive == 0))
  {
    // This is a transitory state where the reference signal is ahead of the
    // Mic signal
    nextState = STATE_REMOTE_SINGLE_TALK;
  }
  else if ((refActive == 1) && (micActive == 1))
  {
    // This is either remote single talk or double talk.
    // For double talk to exist the follwing conditions must be met:
    // 1) Speex needs to think the echo canceller is adapted.
    // 2) The measured aec power has to be above the calculated threshold
    // 3) A ceration amouint of attenuation has been measured across the AEC
    // 4) the LST level has to be greater than 0. If less than 0 indicates it has
    //    note been measured yey.
    // 5) The estimate for local single talk level must be above estimate for
    //    signal during remote single talk. Otherwise no AEC has taken place.
    atten = pow2db(mMicRMSFiltered_p) - pow2db(mAecRMSFiltered_p);
    if (atten > (mAttenRMSFiltered_p / 2))
    {
      mAttenRMSFiltered_p = ARMAFilter(mAttenRMSFiltered_p, atten, 0.1, 0.0005);
    }

//    if (((pow2db(mMicRMSFiltered_p) -  pow2db(mAecRMSFiltered_p) < DTD_THRESH)) && (mConvergedState == 1) && (mClipState == CLIP_NO_CLIPPING))
    if ((atten < (mAttenRMSFiltered_p / 2)) && (mConvergedState == 1)
        && (mClipState == CLIP_NO_CLIPPING))
    {
      nextState = STATE_DOUBLE_TALK;
    }
    else
    {
      nextState = STATE_REMOTE_SINGLE_TALK;
      //mAttenRMSFiltered_p = ARMAFilter(mAttenRMSFiltered_p, atten, 0.1, 0.0004);
    }
  }
  else
  {
    nextState = STATE_BAD_STATE;
  }
  DTDStates minState = nextState;
  AecMinStatisticsFilter(mMinAecRMS, mAecRMSFiltered_p, minState, 0);


  // Add hysterisis to state changes to prevent bouncing signal during
  // trnasitions.

  unsigned long long tempTime;

  tempTime = getMsecs();

  if (nextState == mLastState)
  {
    // if the hys time has expired than change the state
    if (getMsecs() > mStateChangeTime)
    {
      mCurrState = mLastState;
    }
  }
  else
  {
    if ((mLastState == STATE_REMOTE_SINGLE_TALK)
        && (nextState == STATE_LOCAL_SINGLE_TALK))
    {
      mStateChangeTime = getMsecs() + STATE_CHANGE_HYS_TIME_RS_LS;
    }
    else if ((mLastState == STATE_LOCAL_SINGLE_TALK) &&
             (nextState == STATE_SILENCE))
    {
      mStateChangeTime = getMsecs() + STATE_CHANGE_HYS_TIME_LS_SIL;
    }
    else
    {
      mStateChangeTime = getMsecs() + STATE_CHANGE_HYS_TIME;
    }
    mLastState = nextState;
  }

  if (mCurrState != STATE_SILENCE)
  {
    mLastNonSilentState = mCurrState;
    mLastNonSilentTime = getMsecs();

  }
  else
  {
    if((mLastNonSilentTime != 0) &&
        (getMsecs() > (mLastNonSilentTime + NON_SILENT_TIMEOUT)) &&
        (mLastNonSilentState != STATE_SILENCE))
    {

      mLastNonSilentState = STATE_SILENCE;
    }
  }

#ifdef DEBUG_DTD
  // dbg2 - dump all of the DTD state to a file.
  static DTDStates lastDebugState = STATE_BAD_STATE;
  if (lastDebugState != mCurrState)
  {
    static unsigned long long lastChange = 0;
    printf("%llu - DTD State %d->%d; %llu \n", getMsecs(), lastDebugState,
           mCurrState, getMsecs() - lastChange);
    lastChange = getMsecs();

  }
  lastDebugState = mCurrState;
  int mDTAllowed = 1;

  fprintf(mpDebugDTDFd, "%d,%d,%d,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%d\n", mCurrState,
          mConvergedState, mClipState, //1,2,3
          mAecRMSFiltered_p, mag2db(mMinMicRMS.minMinValue), mMicRMSFiltered_p, //4,5,6
          mMinAecRMS.minMinValue, 0.0, pSpxDetectors->leakEstimate, //7,8,9
          refRMSDb, mAttenRMSFiltered_p, micRMSDbSilence, // 10,11,12
          atten, mEchoAdapted); //13,14


#endif
  accumStats(len);
  return STATE_SILENCE;

}



/** wraps the ProcessDTD function in circular buffers to decouple the buffer size
* required for DTD processing from audio processing.
*
* @param pMicIn - pointer to a buffer of mic data
* @param pAecIn - pointer to a buffer of data that has been processed by AEC
* @param pRefIn - pointer the reference data from the speaker out
*
* @return returns the state of the connection
*/
DTDStates cAudioDTD::ProcessDTDWrap(short *pMicIn, short *pAecIn, short *pRefIn,
                                    int len, DETECTORS *pSpxDets)
{
#ifdef __ANDROID__
  CircBuffWrite(&mRefCB, (ITEM_DEF*)pRefIn, len);
  CircBuffWrite(&mAecCB, (ITEM_DEF*)pAecIn, len);
  CircBuffWrite(&mMicCB, (ITEM_DEF*)pMicIn, len);

  if (CircBuffFilled(&mMicCB) >= DTD_BYTE_SIZE)
  {
    CircBuffRead(&mRefCB, (ITEM_DEF*) &mRefBuff, DTD_BYTE_SIZE);
    CircBuffRead(&mAecCB, (ITEM_DEF*) &mAecBuff, DTD_BYTE_SIZE);
    CircBuffRead(&mMicCB, (ITEM_DEF*) &mMicBuff, DTD_BYTE_SIZE);
    ProcessDTD(mMicBuff, mAecBuff, mRefBuff, DTD_PROC_SIZE, pSpxDets);
  }
#endif
  return mCurrState;

}

