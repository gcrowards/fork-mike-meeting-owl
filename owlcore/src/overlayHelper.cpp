#include "logging.hpp"
#include "overlayHelper.hpp"
#include "owlHook.hpp"

extern cOwlHook *gpOwlHook;  // for global hook into GPU draw methods

cOverlayHelper::cOverlayHelper()
{
}

cOverlayHelper::~cOverlayHelper()
{
}

/** This function draws an ellipse on the output image.
 *
 * @param bb is the bounding box of the ellipse to draw.
 * @param color is the color to draw with.
 * @param alpha is the alpha value to draw with.
 * @param thickness is the pixel size of pen to draw with.
 */
void cOverlayHelper::drawEllipse(cv::Rect bb, cv::Scalar color,
				 float alpha, int thickness )
{
  float k = 1.0/255; // need to convert BGR [0,255] to RGB [0.0,1.0]
  if (thickness < 0)
    {
      gpOwlHook->drawEllipseFilled(bb.x + bb.width / 2, bb.y + bb.height / 2,
				   bb.width, bb.height,
				   k * color.val[2],
				   k * color.val[1],
				   k * color.val[0],
				   alpha);
    }
  else
    {
      gpOwlHook->drawEllipse(bb.x + bb.width / 2, bb.y + bb.height / 2,
			     bb.width, bb.height,
			     k * color.val[2],
			     k * color.val[1],
			     k * color.val[0],
			     alpha, thickness);
    }
}

/** This function draws a rectangle on the output image.
 *
 * @param rect is the rectangle to draw.
 * @param color is the color to draw with.
 * @param alpha is the alpha value to draw with.
 * @param thickness is the pixel size of pen to draw with.
 */
void cOverlayHelper::drawRect(cv::Rect rect, cv::Scalar color,
			      float alpha, int thickness )
{
  float k = 1.0/255; // need to convert BGR [0,255] to RGB [0.0,1.0]
  if (thickness < 0)
    {
      gpOwlHook->drawRectangleFilled(rect.x, rect.y, rect.width, rect.height,
				     k * color.val[2],
				     k * color.val[1],
				     k * color.val[0],
				     alpha);
    }
  else
    {
      gpOwlHook->drawRectangle(rect.x, rect.y, rect.width, rect.height,
			       k * color.val[2],
			       k * color.val[1],
			       k * color.val[0],
			       alpha, thickness);
    }
}

/** This function draws a line between the two given points on the output image.
 *
 * @param startPoint is the point that the line will be drawn from.
 * @param endPoint is the point that the line will be drawn to.
 * @param color is the color to draw with.
 * @param alpha is the alpha value to draw with.
 * @param thickness is the pixel size of pen to draw with.
 */
void cOverlayHelper::drawLine(cv::Point startPoint, cv::Point
			      endPoint, cv::Scalar color, float alpha,
			      int thickness)
{
  float k = 1.0/255; // need to convert BGR [0,255] to RGB [0.0,1.0]
  gpOwlHook->drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y,
		      k * color.val[2], 
		      k * color.val[1], 
		      k * color.val[0],
		      alpha, thickness);
}

/** This function calls through to get an RGB color based on the provided
 * integer.  The same color is always returned for a given integer, and that
 * color tends to be relatively unique to colors returned for other integers.
 *
 * @param id is the seed value used to generate the color.
 *
 * @return an OpenCV scalar containing the determed RGB value.
 */
cv::Scalar cOverlayHelper::getColorFromId(int id)
{
  return mColorGenerator.getColorFromSeed(id);
}
