#include "logging.hpp"
#include "panorama.hpp"
#include "params.h"

#include <cmath>

#include "opencv2/imgproc/imgproc.hpp"

cPanorama::cPanorama(void)
{
  mFocusX = CV_PANO_RES_X>>1;

  mPixOffset = (float) CV_PANO_RES_X * CAMERA_ALIGNMENT_ANGLE / 360.;
  mDegPerPix = 360. / CV_PANO_RES_X;
  mPixPerDeg = 1. / mDegPerPix;

  // initialize gray image
  mGrayImage = cv::Mat(CV_PANO_RES_Y, CV_PANO_RES_X, CV_8UC1);

  // initialize mutex for mGrayImage access by other threads
  if (pthread_mutex_init(&mGrayImageMutex, NULL) != 0)
    { LOGE("unable to init gray image mutex\n"); }
}

cPanorama::~cPanorama()
{
  // destroy mutex
  pthread_mutex_destroy(&mGrayImageMutex);
}

/** This function finds the column number of the pixels that is associated with 
 * the given bearing from the Owl.
 *
 * @param angle is the integer degree value that is of interest.
 *
 * @return the x-coordinate of the pixels that are at the angle in question.
 */
int cPanorama::pixelAt(int angle)
{
  float pixel = mPixPerDeg * angle + mPixOffset;
  if (pixel > CV_PANO_RES_X) { pixel -= CV_PANO_RES_X; }
  return pixel;
}

/** This function finds the bearing angle associated with the given pixel 
 * column.
 *
 * @param pixel is the x-coordinate of the pixel of interest.
 *
 * @return the float angle associated with the given pixel.
 */
float cPanorama::angleAt(float pixel)
{
  float angle = mDegPerPix * (pixel - mPixOffset);
  if (angle < 0.) { angle += 360.; }
  return angle;
}

/** Do deep copy of gray image.
 *
 * @param pDst is a pointer to the destination image.
 */
void cPanorama::copyGrayImage(cv::Mat *pDst)
{
  pthread_mutex_lock(&mGrayImageMutex);
  mGrayImage.copyTo(*pDst);
  pthread_mutex_unlock(&mGrayImageMutex);
}

/** @return a reference to the gray-scale version of the panorama. */
cv::Mat * cPanorama::getGrayImage(void)
{
  return &mGrayImage;
}

/** This function is the recieves the image that was already unrolled and 
 * scaled down by the GPU and does any per frame updating
 *
 * @param pCameraCvImage is a pointer to the 8-bit, 4-channel panorama
 * image buffer.
 */
void cPanorama::update(unsigned char *pCameraCvImage)
{
  // build OpenCV Mat based on provided data (WARNING: the Mat is just a 
  // header, the image data still lives in the location of the give pointer)
  mFrame = cv::Mat(CV_PANO_RES_Y, CV_PANO_RES_X, CV_8UC4, pCameraCvImage);

  // build grayscale version of image
  pthread_mutex_lock(&mGrayImageMutex);
  cv::cvtColor(mFrame, mGrayImage, cv::COLOR_BGR2GRAY);
  cv::equalizeHist(mGrayImage, mGrayImage);
  pthread_mutex_unlock(&mGrayImageMutex);
}

/** Records pano center X for shift based calculations.
 *
 */
void cPanorama::setFocusX(int posX)
{
  mFocusX = posX;
}

/** Returns pano center X.
 *
 */
int cPanorama::getFocusX()
{
  return mFocusX;
}
