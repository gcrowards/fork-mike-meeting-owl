#include "debugSwitches.h"
#include "debugManager.hpp"

int cDebugManager::mToolState = -1;

cDebugManager::cDebugManager(void)
{
  if(mToolState < 0)
    { // initialize tool state

      allOff();
      
      // turn on tools as perscribed by debugSwitches.h
      int activeTools = 0;
      
      if(AOI_GUI) { activeTools += eAoi; }
      if(FACE_DETECTOR_GUI) { activeTools += eFaceDetector; }
      if(SOUND_LOCALIZER_GUI) { activeTools += eSoundLocalizer; }
      if(FACE_MAPPER_GUI) { activeTools += eFaceMapper; }
      if(FACE_MAPPER_SEARCH_GUI) { activeTools += eFaceMapperSearch; }
      if(FACE_TRACKER_GUI) { activeTools += eFaceTracker; }
      if(MOTION_MAPPER_GUI) { activeTools += eMotionMapper; }
      if(SOUND_MAPPER_GUI) { activeTools += eSoundMapper; }
      if(PERSON_MAPPER_GUI) { activeTools += ePersonMapper; }
      if(PERSON_INSTANT_ITEM_MAP_GUI) { activeTools += ePersonInstantItemMap; }
      if(PERSON_LONG_TERM_ITEM_MAP_GUI) { activeTools += ePersonLongTermItemMap; }
      if(VOICE_TRACKER_GUI) { activeTools += eVoiceTracker; }
      if(LOUDSPEAKER_GUI) { activeTools += eLoudspeaker; }
      if(MICROPHONE_GUI) { activeTools += eMicrophone; }
      
      enable(activeTools);
    }
}

cDebugManager::~cDebugManager()
{
}

/** Enable the specific debug tool (or tools).
 *
 * The tools correpsonding to the bits in bitMask that are set to 1 will be 
 * enabled.
 *
 * @param bitMask is an integer value whose individual bits represent which tools
 * should be enabled, according to the definition of liveDebugTools_e.
 */
void cDebugManager::enable(int bitMask)
{
  setTools(bitMask, true);
}

/** Disable the specifc debug tool (or tools).
 *
 * The tools correpsonding to the bits in bitMask that are set to 1 will be 
 * disabled.
 *
 * @param bitMask is an integer value whose individual bits represent which tools
 * should be disabled, according to the definition of liveDebugTools_e.
 */
void cDebugManager::disable(int bitMask)
{
  setTools(bitMask, false);
}

/** See if a particular debug tool (or set of tools) is enabled or not.
 *
 * @params tools is an integer whose bits represent which tools to check.
 *
 * @return true if the specific tool (or set of tools) is enabled.
 */
bool cDebugManager::check(liveDebugTools_e tools)
{
  if((mToolState & tools) == tools)
    { return true; }
  
  return false;
}

/** Turn off all run-time controllable debug tools. */
void cDebugManager::allOff(void)
{
  mToolState = 0;
}

/** Turn on all run-time controllable debug tools. */
void cDebugManager::allOn(void)
{
  const int kAll32BitsOn = 0xFFFFFFFF;

  mToolState = kAll32BitsOn;
}

/* Turn specified debg tool (or set of tools) on or off.
 *
 * This is the internally facing function that is used to enable or disable
 * debug tools.
 *
 * @param bitMaks is an integer value whose individual bits represent which 
 * tools should be disbaled or enabled, according to the definition of 
 * liveDebugTools_e.
 * @param state enables the given tools if true, otherwise it disables them.
 */
void cDebugManager::setTools(int bitMask, bool state)
{
  if(state == true)
    { mToolState |= bitMask; }
  else
    { mToolState &= (~bitMask); }
}
