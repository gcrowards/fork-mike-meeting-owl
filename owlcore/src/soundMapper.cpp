#include "debugSwitches.h"
#include "soundMapper.hpp"
#include "params.h"
#include "logging.hpp"

#include <vector>

#define MAP_ADD_VAL             100
#define MAP_LEAK_VAL             10
#define MAP_SPREAD_VAL            5 // degrees

cSoundMapper::cSoundMapper(cVirtualMic *pMic)
{
  mpMic = pMic;

  mMap.setAddVal(MAP_ADD_VAL);
  mMap.setLeakVal(MAP_LEAK_VAL);
  mMap.setSpreadVal(MAP_SPREAD_VAL);
  // using default MIN and MAX values (0 and 1000)
}

cSoundMapper::~cSoundMapper()
{
}

/** This is the main externally facing function that is called to trigger the
 * cSoundMapper to perform an analysis cycle.
 *
 * Each time this function is called the latest sound source localization data
 * is added to the map.
 */
void cSoundMapper::update(void)
{
  // get latest sound data
  std::vector<int> soundBearings = mpMic->getSoundBearings();

  // update map
  mMap.update(soundBearings);
  
  if(DEBUG(SOUND_MAPPER)) {
    mMap.printMap(30); }
}

/** Resets all of the sound  map's stored values to zero. */
void cSoundMapper::resetMap(void)
{
  mMap.resetMap();
}

/** This function gets the value for teh given angle from the underlying map.
 *
 * @return map value at give angle.
 */ 
int cSoundMapper::getMapValAt(int angle)
{
  return mMap.getValAt(angle);
}

/** This function gets the maximum value within a range of bearings around the
 * given nominal angle.
 *
 * @return maximum map value near the given angle.
 */
int cSoundMapper::getAngleWithMaxVal(int nominalAngle, int plusMinus)
{
  return mMap.getAngleWithMaxVal(nominalAngle, plusMinus);
}

/** This function returns the map which is keeping track of the positional data
 * of the things found by the cSoundMapper.
 *
 * @return the map containing a vector of integers.
 */
cMap cSoundMapper::getMap(void)
{
  return mMap;
}
