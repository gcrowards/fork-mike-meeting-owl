#include "circleMath.hpp"
#include "item.hpp"
#include "logging.hpp"

#include <cmath>
#include <limits.h>

#define DEFAULT_LPF_CONST 0.005;  // default value for low-pass filter constant,
                                  // empirically determined (used in item mean
                                  // and standard deviation filterting)

// initialize static member (provides unique ID across all cItem instances)
int cItem::mIdNewest = 0;

cItem::cItem(void)
{
  float thetaMean, phiMean = -1.0;
  float thetaStdDev, phiStdDev = -1.0;
  int assignId = false;

  init(thetaMean, thetaStdDev, phiMean, phiStdDev, assignId);
}

/** Constructor
 *
 * @param thetaMean is the mean of the guassian that represents this Item.
 * @param thetaStdDev is the standard deviation of the guassian that represents
 * the theta parameter of this Item.
 * @param assignId optional argument that when true gives this item instance a 
 * unique ID number (default: false).
 */
cItem::cItem(float thetaMean, float thetaStdDev, bool assignId)
{
  float phiMean = -1.0;
  float phiStdDev = -1.0;

  init(thetaMean, thetaStdDev, phiMean, phiStdDev, assignId);
}

/** Constructor
 *
 * @param thetaMean is the mean of the guassian that represents this Item.
 * @param thetaStdDev is the standard deviation of the guassian that represents
 * the theta parameter of this Item.
 * @param phiMean is the mean of the guassian that represents this Item.
 * @param phiStdDev is the standard deviation of the guassian that represents
 * the phi parameter of this Item.
 * @param assignId optional argument that when true gives this item instance a 
 * unique ID number (default: false).
 */
cItem::cItem(float thetaMean, float thetaStdDev, float phiMean, float phiStdDev, 
	     bool assignId )
{
  init(thetaMean, thetaStdDev, phiMean, phiStdDev, assignId);
}

cItem::~cItem()
{
}

/**  Constructor common code.
 * 
 * @param thetaMean is the mean of the guassian that represents this Item.
 * @param thetaStdDev is the standard deviation of the guassian that represents
 * the theta parameter of this Item.
 * @param phiMean is the mean of the guassian that represents this Item.
 * @param phiStdDev is the standard deviation of the guassian that represents
 * the phi parameter of this Item.
 * @param assignId optional argument that when true gives this item instance a 
 * unique ID number (default: false).
 */
void cItem::init(float thetaMean, float thetaStdDev, float phiMean, 
		 float phiStdDev, bool assignId )
{
  mScore = 0; // measure of strength of this Item's existance

  mAlphaMean = DEFAULT_LPF_CONST;   // low-pass filter constant values for theta
  mAlphaStdDev = DEFAULT_LPF_CONST; // mean and standard deviation

  // Set Item's id to a unique value, if requsted, otherwise set to zero
  if(assignId)
    {
      // deal with the unlikey event of overflowing the ID
      if(mIdNewest == INT_MAX)
	{
	  LOGW("WARNING: Out of cItem IDs! Restarting at 1. Items may not work "
	       "as expected.\n" ); 
	  mIdNewest = 0;  // setId() will add one before assigning
	}
      
      setId();
    }
  else
    { // zero indicates ID not set
      mId = 0;    
    }

  // set item's location info
  setThetaMean(thetaMean);
  setThetaStdDev(thetaStdDev);
  setPhiMean(phiMean);
  setPhiStdDev(phiStdDev);
}

/** Add given value to this Item's strength score.
 *
 * @param adder is the integer amount to add.
 */
void cItem::addToScore(int adder)
{
  mScore += adder;
}

/** Low-pass filter new theta mean into the existing mean value.
 *
 * @param newThetaMean is the new value.
 */
void cItem::filterInThetaMean(float newThetaMean)
{
  float meanDiff = circleMath::intDiffAngs((int)newThetaMean, (int)mThetaMean);
  // TODO: Add floatDiffAngs function to circleMath so these casts aren't
  //       necessary (using cast now because in a practical sense, doing this
  //       diff in integer has enough precision.

  mThetaMean = circleMath::floatAddAngs(mThetaMean, (mAlphaMean * meanDiff));
}

/** Low-pass filter new theta standard deviation into the existing standard
 * deviation value.
 *
 * @param newThetaStdDev is the new value.
 */
void cItem::filterInThetaStdDev(float newThetaStdDev)
{
  mThetaStdDev = mThetaStdDev + mAlphaStdDev * (newThetaStdDev - mThetaStdDev);
}

/** Low-pass filter new phi mean into the existing mean value.
 *
 * @param newPhiMean is the new value.
 */
void cItem::filterInPhiMean(float newPhiMean)
{
  mPhiMean = mPhiMean + mAlphaMean * (newPhiMean - mPhiMean);
}

/** Low-pass filter new phi standard deviation into the existing standard
 * deviation value.
 *
 * @param newPhiStdDev is the new value.
 */
void cItem::filterInPhiStdDev(float newPhiStdDev)
{
  mPhiStdDev = mPhiStdDev + mAlphaStdDev * (newPhiStdDev - mPhiStdDev);
}

/** Assign next unique cItem ID and assign to this instance. */
void cItem::setId(void)
{
  mId = (mIdNewest += 1);
}

/** @param score is a measure of strength in how well we believe that this
    item exists in the real world (and that it's characteristics are about 
    right. */
void cItem::setScore(int score)
{
  mScore = score;
}

/** @param thetaMean is horizontal angle of the item (0 - 360 deg).  */
void cItem::setThetaMean(float thetaMean)
{
  mThetaMean = thetaMean;
}

/** @param phiMean is elevation angle of the item (0 - 90 deg). */
void cItem::setPhiMean(float phiMean)
{
  mPhiMean = phiMean;
}

/** @param thetaStdDev is the certainty of theta. */
void cItem::setThetaStdDev(float thetaStdDev)
{
  mThetaStdDev = thetaStdDev;
}

/** @param phiStdDev is the certainty of phi. */
void cItem::setPhiStdDev(float phiStdDev)
{
  mPhiStdDev = phiStdDev;
}

/** @return the unique ID of this item. */
int cItem::getId(void) const
{
  return mId;
}

/** @return the strength score of this item. */
int cItem::getScore(void) const
{
  return mScore;
}

/** @return the horizontal angle of the item (0 - 360 deg). */
int cItem::getThetaMean(void) const
{
  // TODO: Change to return float if/when rest of owlcore is updated to work
  //       with floating point thetas.
  return (int)mThetaMean;
}

/** @return the elevation angle of the item (0 - 90 deg). */
int cItem::getPhiMean(void) const
{
  // TODO: Change to return float if/when rest of owlcore is updated to work
  //       with floating point thetas.
  return mPhiMean;
}

/** @return the tightness of the location of theta. */
float cItem::getThetaStdDev(void) const
{
  return mThetaStdDev;
}

/** @return the tightness of the location of phi. */
float cItem::getPhiStdDev(void) const
{
  return mPhiStdDev;
}

/** Find the gaussian's value at the give angle.
 *
 * @param int angle is the angle of interest.
 *
 * @return gaussian's value at given angle.
 */
float cItem::getThetaGaussianAt(int angle) const
{
  return getGaussianVal(angle, mThetaMean, mThetaStdDev);
}

/** Find the gaussian's value at the give angle.
 *
 * @param int angle is the angle of interest.
 *
 * @return gaussian's value at given angle.
 */
float cItem::getPhiGaussianAt(int angle) const
{
  return getGaussianVal(angle, mPhiMean, mPhiStdDev);
}

/** Calculate the gaussian function value given the following parameter values:
 *
 * @params int angle is x-value of the function.
 * @params float mean is the mean value of the gaussian.
 * @params float stdDev is the standard deviation of the gaussian.
 *
 * @return gaussian's value at angle.
 */
float cItem::getGaussianVal(int angle, float mean, float stdDev) const
{
  float meanDiff = angle - mean;

  // correct for case where caller is keeping angle[0, 360).
  if(meanDiff > 180) {meanDiff -= 360;}
  else if(meanDiff < -180) {meanDiff += 360;}
  // NOTE: We're assuming cItem represents an item in cylindrical coordindates
  
  float scalar = 1 / std::sqrt(2 * stdDev * stdDev * M_PI);
  float exponent = (meanDiff * meanDiff) / (-2 * std::pow(stdDev, 2));

  return scalar * std::exp(exponent);
}

/** Print out Item's parameter values. */
void cItem::print(std::string comment)
{
  LOGI("%s\n"
       "        mean | stddev\n"
       "theta:   %3.0f  |  %3.1f\n"
       "phi      %3.0f  |  %3.1f\n",
       comment.c_str(),
       mThetaMean, mThetaStdDev, mPhiMean, mPhiStdDev );
}
