# Maps Record, Sim, & Playback Usage
### October 9, 2017	

---

## Table of Contents

[Purpose](#purpose) <br>
[Record](#record) <br>
[Simulate](#simulate) <br>
[CSV File Format](#csv-file-format) <br>
[Playback](#playback) <br>

--

## Purpose <a name="purpose"></a>

The purpose of the Maps Playback module is to enable debugging and testing of the Attention System.  It's a very useful tool if you're able to capture an example of an Attention System failure [(see Record)](#record) because you can then playback that recording on the desktop version of the Meeting Owl application as many times as needed for debugging.  You can also test the fix on the desktop.  Beyond this, the Playback module could be used for unit testing of the Attention System by using pre-recorded or simulated [(see Simluate)](#simulate) data sets.

--

## Record <a name="record"></a>

Enable recording from owlcore/inc/debugSwitches.h.  Each time the meetingOwl application is run (on real hardware) the person map and voice map data will be recorded to CSV files as dictated by the paths in owlcore/inc/params.h.  

The files are overwritten each time a new meeting session begins so be sure to **adb pull** your wanted data before it's overwritten! 

--

## Simulate <a name="simulate"></a>

An alternative to recording real person/voice map data is to directly create your own CSV person and/or voice map files by hand.  This may be useful as a way to generate a scenario that is hard to create in real life.  It is also a good way to work with CSV files that are a little easier to read, since they only need to contain "key frames" (points in time when the state of the system changes in a significant way).

The Person Map CSV file should contain an entry for each time the person map has changes.  In between these times it will be assumed that the map maintains its previous state.

The Voice Map CSV file should contain an entry for each time the voice map has changes.  In between these times it will be assumed that the map maintains its previous state.  To indicate no sound an entry should be made with a timestamp, but no bearing information [(see CSV File Format)](#csv-file-format).

--

## CSV File Format <a name="csv-file-format"></a>

### Voice Map example:

~~~{.py}
time,theta
-----------
0
6, 270
8, 90
10, 315
12, 180
14

----------

#0, 45
#6, 180
#8, 270
#12, 240
#14, 300
#16, 45
~~~

* The header on line 1 is ignored because it can't be interpreted as numerical data
* The dashes on lines 2 and 10 are likewise ignored
* A header is **not** required
* Lines 12 through 17 are ignored because of the leading "#"
* Line 4 says that at time = 6 s, sound is coming from 270 degrees
* Lines 3 and 8 indicate that at times 0 s and 14 s there are no sound sources
* Only one voice bearing should be given per line because that's how the real sound-source localizer works

### Person Map example:
~~~{.py}
time (s), bearing 1 (deg), width 1 (std dev deg), ...
0.005
0.034
0.072
0.111
0.144, 218, 7.0
0.174, 218, 7.0
0.200, 228, 10.7
0.236, 243, 18.1
~~~

* Each row starts with the time (in seconds) followed by and arbitrary number of data pairs in the form of bearing-to-person (in degrees) and standard deviation of the the guassian whose mean is centered on that bearing (each pair represents the location of one person, and the horizontal field of view needed to capture that person)
* Each row is read in as *instantaneous* Person Map data
* Ignored lines, and the like, follow as described in the voice map example

--

## Playback <a name="playback"></a>

Enable playback from owlcore/inc/debugSwitches.h.  When the meetingOwl application is run it will use the CSV files to feed high-level virtual sensor data to the Attention System (raw camera and mic sensor data will be ignored).  This is intended to be used with the Linux desktop version of the Meeting Owl program, but should work on the real hardware as well.

See [(CSV File Format)](#csv-file-format) for detailed information on how the CSV data is interpreted by the Playback module. 

When playing back data the Plackback module can be set to playback every frame in the CSV files, or it can be set to playback in real-time.  If set to playback every frame, each frame will be interpretted, but the playback time will be slower or faster than the timestamps, depending on the speed of the playback machine.  If set to playback in real-time, the Playback module will use the computer's wall clock to grab the correct row of data from the CSV files.  This means that some rows of data may be interpretted multiple times, or may be skipped entirely (e.g. if the playback computer is too slow to keep up with the speed at which the data was recorded).  

**Please note that repeating or skipping data may change the Attention System's response** because of how the instantaneous person maps are filtered into the long-term map.