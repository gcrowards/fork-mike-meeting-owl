# Wav Player Usage Notes
### May 2, 2017	

---

## Table of Contents

[Introduction](#introduction) <br>
[Audio Player Class](#audio-player-class) <br>
[Audio Format](#audio-format) <br>
[cAudio Player Methods](#audio-player-methods) <br>

--

## Introduction <a name="introduction"></a>

There is a problem: we would like to be able to play back audio files from within the the meetingOwl application, even while conferencing is running. This document describes the mechanism introduced beta_1.7.x to provide for this.

--

## Audio Player Class <a name="audio-player-class"></a>

The cAudioPlayer class provides a simple interface to playing back audio files from within the meetingOwl application. A code snippet is below:

~~~{.c}
char filename[] = "/data/audio/ieee_male.wav";

pPlayer2 = new cAudioPlayer();

pPlayer2->playWavFile(filename, playbackCB);
~~~

It’s that simple.

1) Create an audio player object.
2) Tell it what audio file to play. Optionally, give it a call back function to call when playback is completed.

--

## Audio Format <a name="audio-format"></a>

The audio player can only play audio whose format is:

* 1 Channel
* 48000 samples per second
* 16 bit

--

## Audio Player Methods <a name="audio-player-methods"></a>

The following methods are the public methods that a user typically needs:

* playWavFile
* playRawFile
* waitPlayback
* stopPlayback

Please see cAudioPlayer for more details on these methods.
