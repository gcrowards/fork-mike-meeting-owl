# Switchboard (inter-system comms) Design Notes
### February, 2017	

--

## Table of Contents

[Purpose and Usage](#purpose-and-usage) <br>
[Modules](#modules) <br>
[Messages](#messages) <br>
[Design Overview](#design-overview) <br>
[Server Usage](#server-usage) <br>
[Server Design](#server-design) <br>
[Client Usage](#client-usage) <br>
[Client Design](#client-design) <br>

--

## Purpose and Usage <a name="purpose-and-usage"></a>

The Owl Switchboard classes have been designed and implemented to abstract away the complexities of using socket based interprocess communications and provide a simple uniform API for Owl applications to inter-communicate. Central to the design is the server object. Created either in a stand alone application, or as part of the meetingOwl application, the server accepts connections from clients, and forwards messages between clients based on subscriptions. Each client connects to the server, then tells the server about message types that it wishes to be sent. When it receives a message from a client, the server looks up which clients have subscribed to that type of message, and sends the message to all of them. When a client receives a message of a given type, the client code looks up the callback function to invoke when such messages are received, and calls that function supplying the message data.

So, let’s consider some example applications:

###Client to Client messaging
Both client A and client B connect to the server. Client A subscribes to messages of type B and Client B subscribes to messages of type A. When client A wants to send a message to client B, it creates and sends a message of type B. When client B wants to send a message to client A, it creates a message of type A and sends it.

###Test Client
In a working system, it is useful to inject commands and data to initiate test scenarios. In this architecture, a test client can create a message of any type it likes to simulate communications to any client in the system.

###Debug Client
A debug client can subscribe to any or all of the message types supported in the system in order to monitor system activities and capture logs of system operation.

--

## Modules <a name="modules"></a>

There are 3 headers and 2 modules that implement the switchboard. All the switchboard headers are contained in owlcore/inc/switchboard, and all the modules are contained in owlcore/src/switchboard. Each file is described below:

###switchboardMessages.hpp
Contains public information needed to configure the switchboard interconnect such as the TCP port to use, the enumeration of message types, the message header structure, and specific message type dependant structures that clients use to exchange information.

###switchboardClient.hpp
Defines the switchboard client object including the public interface that clients use when communicating through the system.

###switchboardServer.hpp
Defines the switchboard server object. The switchboard server public interface consists only of a constructor and destructor, so it is very simple to use.

###switchboardClient.cpp
Contains the implementation of the switchboard client class. Applications that want to communicate through the server should either incorporate this client, or an implementation that is similar (ideally a wrapper that uses this C++ module). 

###switchboardServer.cpp
Contains the implementation of the switchboard server class. This should either be incorporated into a stand alone server application, or be pulled into an existing application. Either way, the application will still need to create one or more client instances to communicate through the server with external clients.

--

## Messages <a name="messages"></a>

###Header
The message header, defined in switchboardMessage.hpp, is sent at the beginning of every message. The header starts with a magic number that is used to guarantee the data stream is properly aligned with message boundaries. After the header are a major and minor version numbers. Next is a message type that is used for message routing and decoding. Finally, the length field contains the size of the following payload data.

###Types
Message types are defined in the enumeration eOipcMessageTypes in switchboardMessage.hpp. There are a few private message types used for internal messaging at the beginning of the enum, and all client message types follow. The list must be terminated with the MAX_OIPC_TYPE enum. Each client application should define its messages in a contiguous group, and should start at an even multiple of 100. When at all possible, messages should not be moved around in the enumeration, and new messages should be added to the end of the application’s group. This allows plenty of room for each group to grow, if necessary, and minimizes the need for major version changes.

To support a new message channel between a pair of clients, simply add a new enum to the list to identify the message type. Then the client that receives that type of message must subscribe to the message type, providing a callback function to be invoked when such a message arrives. For two way communications, two message types should be defined, and the client on either end of a connection each subscribes to messages from the other side. However, the design clearly allows clients to subscribe to any set of message types that they want.

###Versioning

VERSIONING RULES:

* minorVer: When messages are added to the eOipcMessageType space, the minor    version should be incremented. If the majorVer is incremented, the minor version should be reset to 1.
* majorVer: When messages are moved in the eOipcMessageType space, or the  oipcMessageHeader structure definition is changed, or aspects of the internal implementation of OIPC change (such as maximum message length), then the major version should be incremented.
* If minor versions don't match, the server emits a warning message when it receives subscription messages.
* If major versions don't match, the server sends an error message to the client causing it to shut down the client connection, and the server refuses to forward messages. When the error is received at the client, it invokes a callback to inform the client application that the connection is non-functional.

Note, although the message header includes a version, it may, under some circumstances, still be important to include a version number in the payload, too. This will be true for data passed as contents of a class or structure. A safer way to pass data is to serialize the structure as JSON for transmission and deserialize at the receiver, providing defaults if new fields are added.

--

## Design Overview <a name="design-overview"></a>

The diagram below illustrates the C++ objects involved in the implementation (square blue boxes), the threads that control the interactions (wavy boxes), and the paths that data travels (arrows). It also indicates the ownership of the cServerInstance objects by the list maintained by the cSwitchboardServer object via the mpServerConnectionFactoryThread. The diagram shows three clients, but any reasonable number can be supported.

![](../../owlcore/designNotes/assets/owlSwitchboardDesign.png)

--

## Server Usage <a name="server-usage"></a>

The server is designed to manage the various connections to clients without any effort required from the application that owns the server object. The application simply creates an instance of the cSwitchboardServer, and the server handles creation of client connections and routing of messages internally. When the server is no longer needed, it should be destroyed to cleanly shut down the socket.

--

## Server Design <a name="server-design"></a>

The server is designed to automatically start up after it is created, and to restart if it ever dies due to network failures on the socket. The mRestartThread runs periodically and starts the connection up the first time, or manages restarting, if necessary. If the server is restarting, it first calls finish() to clean up the existing connection before calling init() to start anew. 

When the server starts up, it opens a socket and creates the mpServerConnectionFactoryThread to listen for connections. When a client connects to the server, the factory creates a new cServerInstance object and an associated mpInstanceThread.

The mpInstanceThreads each wait for the attached client to send a message. When the instance reads a message, it buffers the header and payload, then sets a flag to indicate that it has data ready.

Meanwhile, the server also runs one more thread, the mWriterThread, that polls the instances looking for new messages. When an available message is found, then the writer searches through each instance’s subscribed message type list, and if that instance has subscribed to the message type of the new message, then the new message is sent to the client. Once all the instances have been checked for matching subscriptions, then the writer releases the message so the instance can read another one.

Subscription messages are handled by the server, and never forwarded on to other clients. If a subscribe or unsubscribe message is received on a server instance, then the list of subscribed message types for that instance is updated accordingly.

--

## Client Usage <a name="client-usage"></a>

Clients similarly have a simple API, but message reception handling is based on a callback paradigm. A new connection to the server is created by creating an instance of a cSwitchboardClient object, passing the IP address of the server to which it should connect, as well as an error callback function. The error callback is invoked if a fatal error occurs that stops the client’s operation, such as a major version number mismatch.

After creating the client object, the client program calls subscribe() on the object once for each type of message the client wants to receive. With each subscription the client program provides a callback function to be called when the corresponding type of message arrives. The callback function runs in the context of the client object’s mReaderThread.

If, during operation, the client wants to stop getting messages of a given type, it can call the unSubscribe() method.

To send a message from the client program to another client, the client calls writeMessage() on the client object. This sends the message to the server where it is routed to subscribed clients based on the supplied message type.

Finally, if the client becomes disconnected from the server due to network errors, or server failure, the client will restart. The client will attempt to re-subscribe to all of its previously subscribed message types after restarting.

--

## Client Design <a name="client-design"></a>

When the client object is created, it starts up the mRestartThread. The thread establishes a connection to the server, and then creates the mpReaderThread that waits for messages to arrive from the server.

Upon receipt of a message, the reader thread searches the list of subscribed message types. When a match is found, the reader thread then invokes the corresponding callback function, passing the message type and payload data as arguments. It is the application’s responsibility to ensure that the callback function finishes in a reasonable amount of time, otherwise subsequent messages could be lost. (That is unlikely, though, as the OS buffers the TCP/IP messages for us.)

If a read from the socket fails (generally because the socket has become disconnected or the server shut down), then the restart thread cleans up the previous session, and attempts to start up a new connection with the server.
