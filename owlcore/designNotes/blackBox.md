# Black Box Design Notes
### March 27, 2017	

---

## Table of Contents

[Purpose](#purpose) <br>
[Layout](#layout) <br>
[Accessing from Code](#accessing-from-code) <br>
[Adding Fields](#adding-fields) <br>
[Areas for Improvement](#areas-for-improvement) <br>

--

## Purpose <a name="purpose"></a>

The Black Box is a place to persist information about the performance of the system, and to save some run-time parameters. Black Box data is stored as a JSON file. The Black Box is not a general repository for system data, but instead is meant to store that information which provides value to Owl Labs for analyzing usage of the meeting Owl device. A Java layer Android service copies the Black Box to Amazon Web Services periodically where the data is stored in a database from which queries can be made to better understand typical usage and system health.

--

## Layout <a name="layout"></a>

Black box data is stored in in the Owl’s file system in the following file:  
> /data/OwlLabs/BlackBox/BlackBox.json

The next most recent copy of the Black Box is backed up in:
> /data/OwlLabs/BlackBox/BlackBox.bak.json

The backup file is kept as a means of restoring the black box if the primary file is corrupted.

As of the writing of this document, the data is laid out in the black box in the following format:

~~~{.json}
{
	"BlackBox": {
		    "nMeetings": 31,
		    "nPowerUp": 5,
		    "nCrashBoot": 34,
		    "lastBadBoot": "Power-off reason: Triggered from PS_HOLD (PS_HOLD/MSM controlled shutdown)",
		    "nAppStarts": 46,
		    "nUpTimeMinutes": 20294,
		    "nMeetingMinutes": 1478,
		    "nUpTimeSinceBootMinutes": 760,
		    "nUsbStateFixups": 5
	},
	"Settings": {
		    "spkrVolume": 80
	}
}
~~~

Things to note:
1. Data is stored in a two level hierarchy: the root keys, and the leaf keys within each root.
2. The first root key is “BlackBox”, and stores system statistics.
3. The second root key is “Settings”, and stores persistent, but user changeable, settings.
4. In JSON terms, the data is a single object at the top level that contains two objects. Then each data item is a key / value pair.

It is easier to look at the JSON, and you can also verify that the JSON is valid if you copy the contents of the black box and paste it into the JSON validator window at this web site:
http://jsonlint.com/

--

##Accessing from Code <a name="accessing-from-code"></a>

The data stored in the Black Box is stored as a rapidjson Document object called ‘mDoc’ which is a member of the cBlackBox class (found in owlcore/src/blackBox.cpp and owlcore/inc/blackbox.hpp). 

The meetingOwl application should generally access the data fields directly whenever using or updating the data. The cBlackBox class provides accessors for the data that quickly extract or insert the data in mDoc. It is up to the programmer to decide if the Black Box should be flushed to disk by calling the ‘save()’ method after a change is made. In any case, the Black Box is flushed to disk every 10 minutes while the ‘meetingOwl’ application is running.

The cBlackBox accessors currently support 3 data types: integers, strings, and booleans. (Note, it would be very easy to add support for floating point numbers, but has not yet been done.) For each type, there are ‘get’ and ‘set’ functions. In addition, for integer types there is an ‘inc’ function that by default adds 1 to an integer field, but can also add an arbitrary value in its optional parameter.

All functions take similar arguments: root key, leaf key, and a reference to the value, and all return a boolean value indicating if the field exists in the Black Box template. If either key could not be found, no change is made to the black box on a ‘set’ call, or no change is made to the supplied parameter on a ‘get’ call.

The root and leaf keys are strings that are used for looking up the field of interest. These strings should be defined with a preprocessor symbol in a single place in cBlackBox.hpp (to avoid accidental mismatches of strings), then used in the black box template definition, as well as in all accessor function calls that reference a given key.

The accessor functions are: getInt(), setInt(), incInt(), getString(), setString(), getBool(), and setBool().

--

## Adding Fields <a name="adding-fields"></a>

The Black Box layout is determined by a template defined in the black box header, cBlackBox.hpp. At startup, the ‘meetingOwl’ application loads the template into the ‘mDoc’ document, then tries to find the Black Box file on disk. If the file is found, the initialization code iterates through the fields in the template looking for a matching key from the file. This allows the ‘meetingOwl’ application to be robust in the face of either additions or deletions of data fields as well as root keys.

To add a new data value to the template, do the following:
1. Define the preprocessor symbol for the key’s identifying string. This is the same string that will appear in the JSON Black Box textual representation.
2. Add the key symbol to the JSON template. Be careful to get the quotes right - all field names must be surrounded by quotation marks, as should strings, and because this is all wrapped in a C string, there end up being lots of escaped quotes in the definition.
3. Write code that accesses the new field. Try to write the code in such a way that the value is only modified local to a single function, and then copied back into the black box, if necessary.
4. If a value of significance has been modified, call the ‘save()’ method in cBlackBox to persist the update on disk. Be careful not to do this too often, though, as it requires writing the black box file to disk. For some values it could be Ok to allow the periodic 10 minute save to persist the data to disk.


--

##Areas for Improvement <a name="areas-for-improvement"></a>

1. Currently the BlackBox is not thread-safe. If 2 threads read a value, change it, then write it out again, the data might be corrupted. While it would be easy to add a lock mechanism, that might also introduce opportunities for deadlock. If multi-threaded access to variables becomes important, this should be designed with care…
2. The 10 minute flush interval length was chosen arbitrarily. There may be value in decreasing the interval length in the future to reduce the need for code to call the ‘save’ method.
3. The C code defining the Black Box template in the blackBox.hpp header is ugly and prone to errors. It would be nice to find a better way to define the template while still ensuring that the keys are all defined in a single place.
