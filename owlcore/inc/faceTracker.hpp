#ifndef __faceTracker_hpp
#define __faceTracker_hpp

#include "opencv2/core/core.hpp"

#include <vector>

/** Face tracker states. */
typedef enum class faceState : int { 
  UNKNOWN,     /**< Undefined state. */
    TRACKING,  /**< Successfully tracking face. */
    LOST       /**< Unable to track face. */
    } faceState_t;

/** This class figures out when a new face has appeared, when an existing face
 * has dissappeared, and when a face is stable.  The tracked face's reference 
 * frame is the full-scale camera's coorfinate system.
 */
class cFaceTracker
{
public:
  // constructors
  cFaceTracker(void);
  cFaceTracker(cv::Rect face, cv::Size spaceSize);
  
  // destructor
  ~cFaceTracker();

  // actions
  std::vector<cv::Rect> update(std::vector<cv::Rect> faces);
  bool isFocusShifted(int threshold, bool reset = true);

  // setters
  void setFace(cv::Rect face);
  void setSpaceSize(cv::Size spaceSize);

  // getters
  faceState_t getState(void);
  cv::Rect getFace(void);
  cv::Point getFocus(void);

private:
  // data
  cv::Rect mFace; /**< OpenCV rectangle that represents tracked face location. 
		   The reference frame is the full-scale camera's coordinate 
		   system. */
  cv::Point mFocus; /**< OpenCV point marking the center of our attention. */
  cv::Point mFocusShift; /**< OpenCV point showing how much the focus moved 
			    since the previous update call. */
  float mTolerance; /**< This tolerance sets how close a newly detected face 
		       needs to be to the previous detected face in order to be
		       assumed that it is the same face. */
  int mScore; /**< Leaky integrator value used to determine if face is being
		 successfully tracked, or if it's been lost. */
  cv::Size mSpaceSize; /**< This sets the pixel bounds beyond which the focus
			  point should not be set. */
  faceState_t mState; /**< This indicates if the face is lost or being tracked.*/

  // actions
  void setState(void);
  void reset(void);
  void updateFace(cv::Rect face);
};

#endif
