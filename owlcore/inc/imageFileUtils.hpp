#ifndef _IMAGE_FILE_UTILS_HPP_
#define _IMAGE_FILE_UTILS_HPP_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/** This collection of utility functions is for manipulating image files.

    Currently, JPEG and PNG files are supported in a limited way.
 */ 

// Note that the extern "C" wrapper is needed to keep C++
// from mangling object names, so they match the library
extern "C" {
#include <jpeglib.h>
#include <png.h>
}

// image buffer manipuation
void   inPlaceBGRAtoRGB(unsigned char *pImageBuf, int width, int height);
void   inPlaceRGBAtoRGB(unsigned char *pImageBuf, int width, int height);

// JPEG support
void   jpegFileGetImageSize(char *pFilename, int *pWidth, int *pHeight);
void   jpegFileRead(JSAMPLE *pImageBuf, char *pFilename);
void   jpegFileWrite(JSAMPLE *pImageBuf, char *pFilename,
		     int width, int height, int quality);

// PNG support
void   pngFileGetImageSize(char *pFilename, int *pWidth, int *pHeight);
void   pngFileRead(unsigned char *pImageBuf, char *pFilename);


#endif /* _IMAGE_FILE_UTILS_HPP_ */
