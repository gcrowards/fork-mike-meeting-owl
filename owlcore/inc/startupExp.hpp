/*
 * startupExp.hpp
 *
 * Copyright (c) Owl Labs Inc. 2017.
 * All rights reserved.
 *
 *  Created on: Apr 30, 2017
 *
 * Startup state machine for sequencing tutorial UI elements associated with
 * starting up the Meeting Owl.
 */

#ifndef OWLCORE_INC_STARTUPEXP_HPP_
#define OWLCORE_INC_STARTUPEXP_HPP_

#include "analytics.hpp"
#include "AudioMgr.hpp"
#include "AudioPlayer.hpp"
#include "blackBox.hpp"
#include "common.hpp"
#include "ledContext.hpp"
#include "elements/rgbaOverlayLists.hpp"
#include "netStatus.hpp"
#include "thread.hpp"



/** Class to manage the startup experience UI. */
class cStartupExperience
{
public:
  cStartupExperience(cAnalytics *pAnalytics, cBlackBox *pBlackBox,
                     cLedContext *pLedContext);
  ~cStartupExperience();

  void setOverlays(owl::cRgbaOverlayLists *pOverlays);
  void setAoiStatus(bool empty);

  /** The states which the system sequences through are enumerated in
   * eStartupState_t.
   */
  typedef enum
  {
    WAITING_TO_HOOT,             /**< Delay to allow startup to finish. */
    HOOTING,                     /**< Playing hoot audio clip. */
    WAITING_TO_SPEAK,            /**< Delay between hoot and greeting. */
    SPEAKING_FIRST_MESSAGE,      /**< Speaking first-time greeting. */
    WAITING_FOR_STREAMING_START, /**< Awaiting UVC connection. */
    SHOWING_LOGO,                /**< Showing logo only briefly. */
    SHOWING_MESSAGE,             /**< Showing logo and startup message. */
    STARTUP_COMPLETE,            /**< Messaging complete, normal meeting. */
    MEETING_ENDED                /**< Meeting completed, maybe WiFi warning. */
  } eStartupState_t;

  eStartupState_t getStartupState(void);


private:
  cThread mThread;            /**< Thread that runs this state machine. */
  eStartupState_t mState;     /**< Current state of the state machine. */
  struct timespec mStartTime; /**< Time when entered current state. */

  cAnalytics *mpAnalytics;    /**< Analytics object to get meeting state. */
  cBlackBox *mpBlackBox;      /**< Black box object for historical state. */
  cLedContext *mpLedContext;  /**< LED context to control eyes and skirt. */
  bool mFirstTimeStartup;     /**< Flag true if starting for first time ever. */
  bool mStageEmpty;           /**< Flag true if stage is empty (delay change).*/
  bool mAppConfigured;        /**< Flag true if App has configured Owl. */
  bool mUvcConnected;         /**< Flag true if UVC (video) is streaming. */

  bool mSpeakFirstTimeMessage;        /**< If true speak first-time message. */
  bool mSpeakWiFiUnconfiguredMessage; /**< If true speak WiFi unconfigured. */

  bool mMinimizeStartupExperience;    /**< Eliminate spoken msgs and delays. */

  cNetStatus *mpNetStatus;            /**< WiFi status monitor state machine. */
  owl::cRgbaOverlayLists *mpOverlays; /**< Control struct for overlays. */
  cAudioPlayer mAudioPlayer;          /**< Audio message player object. */

  /* State machine functions: */
  void update(void);

  void initStartupExperience(void);
  void doShowBackground(void);

  bool testWaitingBeforeHoot(void);
  void doStartHoot(void);
  bool testWaitingForHootDone(void);
  void doStartWaitingToSpeak(void);
  bool testWaitingToStartDone(void);
  void doStartSpeakingFirstMessage(void);
  bool testWaitingForFirstMessageDone(void);
  bool testWaitingForStreamingStart(void);
  void doStartShowingLogo(void);
  bool testTimeToShowMessage(void);
  void doStartShowingMessage(void);
  bool testTimeToClearScreen(void);
  void clearLogo(void);
  void clearMessageDisplay(void);
  void updateMessageDisplay(void);
  bool testWaitingForMeetingEnd(void);
  void doStartSpeakingWiFiMessage(void);
  bool testWaitingForWiFiMessageDone(void);
  void doBlinkLeds(bool on);


  /* Tuning parameters for control of state machine timing. */
  const double mkDelayBeforeHoot = 1.5; /**< Wait before hooting. */
  const double mkDelayBeforeFirstSpeakingSec = 1.0; /**< Wait till speaking. */
  const int mkDelayAfterShowingIcon = 2;       /**< Time icon alone. */
  const int mkDelayAfterShowingMessage = 10;   /**< Time showing message. */
  const int mkDelayAfterShowingMessageMin = 1; /**< Minimized time. */

  const int mkPollPeriodNsec = 30000;  /**< 30 mSec update rate of poll loop. */

  /* Progress bar position, size and color parameters. */
  const int mkProgressBarX = 490;
  const int mkProgressBarY = 533;
  const int mkProgressBarStartWidth = 0;
  const int mkProgressBarStartHeight = 5;
  const int mkProgressBarEndWidth = 301;
  const cv::Scalar mkProgressBarColor = cv::Scalar(0x57, 0xc7, 0xfe);
};

/* Callback function needed for AudioPlayer. */
int speechDone(int result);
extern bool gSpeechDoneFlag; /**< Flag set by callback when clip is done. */


#endif /* OWLCORE_INC_STARTUPEXP_HPP_ */
