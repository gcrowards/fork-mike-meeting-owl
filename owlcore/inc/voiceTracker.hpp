#ifndef __voiceTracker_hpp
#define __voiceTracker_hpp

#include "voice.hpp"

#include <vector>

/** Voice tracker states. */
typedef enum class voiceState : int {
  UNKNOWN,      /**< Undefined state. */
    TRACKING,   /**< Successfully tracking voice. */
    PERSISTENT, /**< Have been tracking voice for a long time. */
    LOST        /**< Unable to track voice. */
    } voiceState_t;

/** This class keeps track of a "voice" and where it is coming from. 
 *
 * A voice (or more accurately, a sound source that we are assuming is a person
 * speaking) is used to determine when new Areas of Interest need to be created, 
 * and old ones destroyed.
 *
 * The tracker keeps a score, which indicates how confident it is that it has
 * the location of the voice.  It also stores a direction, which indicates the
 * location of the voice.
 *
 * NOTE: This class is pretty sparse and is mostly a placeholder, so don't take
 *       things like "score" too seriously.
 */
class cVoiceTracker
{
public:
  // constructors
  cVoiceTracker(void);
  cVoiceTracker(cVoice voice);

  // destructor
  ~cVoiceTracker();

  // actions
  std::vector<cVoice> update(std::vector<cVoice> voices);
  void forceReset(void);

  // setters
  void setVoice(cVoice voice);
  void setDirection(int direction);

  // getters
  voiceState_t getState(void);
  cVoice * getVoice(void);
  int getDirection(void);

private:
  // data
  int mDirection; /**< The bearing at which the voice is believed to be. */
  float mScore;   /**< An indication of how confident the tracker is (negative
		     values indicate how long it's been since the tracker "saw"
		     anything). */
  cVoice mVoice;  /**< Source of audio data. */
  voiceState_t mState; /**< Voice tracker state (e.g. tracking, lost, etc.). */

  // actions
  void reset(void);
  void setState(void);
};

#endif
