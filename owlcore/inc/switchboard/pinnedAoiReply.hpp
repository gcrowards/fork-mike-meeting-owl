/*
 * pinnedAoiReply.hpp
 *
 *  Created on: Mar 2, 2017
 *
 *  Class for managing pinned AOI (Area of Interest) arguments. Defines
 *  a message to use for sending pinned AOI replies.
 */

#ifndef OWLCORE_INC_SWITCHBOARD_PINNEDAOIREPLY_HPP_
#define OWLCORE_INC_SWITCHBOARD_PINNEDAOIREPLY_HPP_

#include "switchboard/pinnedAoi.hpp"


/** Define strings that make up the JSON vocabulary for pinned Aoi reply.
 * A pinned AOI reply message has the syntax:
 * {"PinnedAoiReply":
 *   {"cmd":"cmdStr",
 *    "id":0,
 *    "size":0,
 *    "angle":0,
 *    "returnVal":0,
 *    "message":""}}
 */
#define PINNED_AOI_REPLY_STR            "PinnedAoiReply"
#define PINNED_AOI_REPLY_CMD_STR        "cmd"
#define PINNED_AOI_REPLY_ID_STR         "id"
#define PINNED_AOI_REPLY_SIZE_STR       "size"
#define PINNED_AOI_REPLY_ANGLE_STR      "angle"
#define PINNED_AOI_REPLY_RETURNVAL_STR  "returnVal"
#define PINNED_AOI_REPLY_MESSAGE_STR    "message"

/** Define RapidJson "pointers" for accessing specific reply fields. */
#define PINNED_AOI_REPLY_CMD_PTR        \
  "/" PINNED_AOI_REPLY_STR "/" PINNED_AOI_REPLY_CMD_STR
#define PINNED_AOI_REPLY_ID_PTR         \
  "/" PINNED_AOI_REPLY_STR "/" PINNED_AOI_REPLY_ID_STR
#define PINNED_AOI_REPLY_SIZE_PTR       \
  "/" PINNED_AOI_REPLY_STR "/" PINNED_AOI_REPLY_SIZE_STR
#define PINNED_AOI_REPLY_ANGLE_PTR      \
  "/" PINNED_AOI_REPLY_STR "/" PINNED_AOI_REPLY_ANGLE_STR
#define PINNED_AOI_REPLY_RETURNVAL_PTR  \
  "/" PINNED_AOI_REPLY_STR "/" PINNED_AOI_REPLY_RETURNVAL_STR
#define PINNED_AOI_REPLY_MESSAGE_PTR    \
  "/" PINNED_AOI_REPLY_STR "/" PINNED_AOI_REPLY_MESSAGE_STR


/** Pinned AOI (Area of Interest) reply message JSON interface */
class cPinnedAoiReply
{
public:
  cPinnedAoiReply();
  cPinnedAoiReply(ePinnedAoiCmd_t cmd, int id);
  cPinnedAoiReply(ePinnedAoiCmd_t cmd, int id, int retVal, std::string msg);
  cPinnedAoiReply(ePinnedAoiCmd_t cmd, int id, int size, int angle, int retVal, std::string msg);
  ~cPinnedAoiReply();

  bool parseMsg(const char *pMsg);
  bool buildMsg(char *pMsgBuf, int bufLen);

  ePinnedAoiCmd_t getCommand(void);
  int  getId(void);
  int  getSize(void);
  int  getAngle(void);
  int  getReturnVal(void);
  std::string getMessage(void);

private:
  ePinnedAoiCmd_t cmd;  /**< Command in this message. */
  int id;               /**< ID identifying this message's AOI. */
  int size;             /**< Angular extent in degrees of this AOI. */
  int angle;            /**< Central angle in degrees of this AOI. */
  int returnVal;        /**< Return value of command. */
  std::string message;  /**< Message to return in response to command. */
};

#endif /* OWLCORE_INC_SWITCHBOARD_PINNEDAOIREPLY_HPP_ */
