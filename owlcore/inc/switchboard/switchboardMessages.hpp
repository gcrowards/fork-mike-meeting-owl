/*
 * switchboardMessages.hpp
 *
 * Copyright (c) Owl Labs Inc. 2017.
 * All rights reserved.
 *
 * OIPC (Owl Inter-Process Communications) switchboard protocol shared messages
 * and structures.
 */
#ifndef SWITCHBOARDMESSAGES_HPP
#define SWITCHBOARDMESSAGES_HPP


#include <vector>

#include "thread.hpp"

/** The TCP port that is used for the connections. */
#define TCP_PORT   6300

/** Magic number that identifies the beginning of any message. */
#define OIPC_MAGIC   0xB18DF00D

/** Maximum payload length for any message. (Larger messages must use
 * multiple transfers.)
 */
#define OIPC_MAX_MSG_LEN  1024

/** Major and minor version numbers of the OIPC protocol. */
#define OIPC_MAJOR_VERSION     1
#define OIPC_MINOR_VERSION     3


/** Enum of all message types exchanged through the server */
typedef enum
{
  MIN_OIPC_TYPE = 0,    /**< First item, invalid as a message type. */
  /* Client / server internal messages - don't use these for apps! */
  CLIENT_PLEASE_CLOSE,  /**< Message from server to close connection. */
  CLIENT_VERSION_ERROR, /**< Message from server to close due to error. */
  SUBSCRIPTION_ADD,     /**< Message to server to subscribe to message type. */
  SUBSCRIPTION_REMOVE,  /**< Message to server to remove subscription to type.*/
  NOTIFICATION,         /**< Message from server signaling warning or error. */

  /* Application messages. Each client defines messages in its own range.
   * Each range should start at a multiple of 100 to ensure sufficient
   * gaps between ranges to add new messages. */
  UNIT_TEST = 100,       /**< Each client defines messages in its own range. */
  CLIENT_NAME,
  SERVER_NAME,
  TEST1,
  TEST2,

  MEETING_OWL_DEBUG_CLIENT = 200,    // Replace this for next client app!
  MEETING_OWL_DEBUG_CMD, /**< Debug command channel. */
  MEETING_OWL_DEBUG_MSG, /**< LOG messages from meetingOwl. */

  PINNED_AOI_CLIENT = 300,
  PINNED_AOI_CMD,
  PINNED_AOI_REPLY,

  PANO_SHIFT_ANGLE_GET = 310, /**< Query pano shift angle. */
  PANO_SHIFT_ANGLE_GET_REPLY, /**< Reply with integer angle [0, 360). */
  PANO_SHIFT_ANGLE_SET,       /**< Set pano shift angle and persist. */

  UI_ACTION_CLIENT = 400,
  UI_ACTION_CMD,

  MEETING_INFO_CLIENT = 500,
  MEETING_INFO_QUERY,
  MEETING_INFO_NOTIFY,

  APP_CONFIGURED_NOTIFICATION = 600, /**< App has completed configuration(old) */
  SETUP_PROGRESS_GET = 601,          /**< Ask for App config state value */
  SETUP_PROGRESS_GET_REPLY = 602,    /**< Return App config state */
  SETUP_PROGRESS_SET = 603,          /**< Set App config state value */

  FACTORY_RESET_CMD = 700, /**< Reset Owl to factory-fresh state */

  AUDIO_PARAMETERS_CMD = 800, /**< Audio parameters set / get interface */
  AUDIO_PARAMETERS_REPLY,     /**< Audio parameters set / get reply */

  VIDEO_PARAMETERS_CMD = 1300,/**< Video parameters set / get interface */
  VIDEO_PARAMETERS_REPLY,     /**< Video parameters set / get reply */

  DEBUG_INFO_CAPTURE_CMD = 1400, /**< Capture logcat/dmesg output and send back. */
  DEBUG_INFO_HEADER_MSG,          /**< Contains logcat/dmesg header. */
  DEBUG_INFO_CHUNK_MSG,           /**< Contains logcat/dmesg data. */
  DEBUG_INFO_TRIGGER_UPDATE,      /**< Trigger a Barn update in BLEService. */

  MAX_OIPC_TYPE         /**< Last item, invalid message, end of list marker. */
} eOipcMessageType;


/** OIPC (Owl Inter-Process Communications) message header. Note, it is
 * VERY important to use data types that have explicit sizes such as
 * uint32_t, uint8_t, etc to avoid data size mismatches between systems.
 *
 * VERSIONING RULES:
 * minorVer: When messages are added to the eOipcMessageType space, the minor
 * version should be incremented. If the majorVer is incremented, the minor
 * version should be reset to 1.
 * majorVer: When messages are moved in the eOipcMessageType space, or the
 * oipcMessageHeader structure definition is changed, or aspects of the
 * internal implementation of OIPC change (such as maximum message length),
 * then the major version should be incremented.
 * If minor versions don't match, the server emits a warning message when
 * it receives subscription messages.
 * If major versions don't match, the server sends an error message to the
 * client and refuses to forward messages.
 */
typedef struct
{
  uint32_t magic;    /**< OIPC_MAGIC identifying a message is valid. */
  uint16_t major;    /**< Major version number. */
  uint16_t minor;    /**< Minor version number. */
  uint32_t type;     /**< eOipcMessageType of a message. */
  uint32_t length;   /**< Length of the message payload. */
} oipcMessageHeader;

/** OIPC message from client subscribing to a type of messages */
typedef struct
{
  uint32_t type;  /**< Type of message to subscribe to */
} oipcSubscriptionMessage;

/** OIPC message from server containing warning / error message */
typedef struct
{
  char pMsg[OIPC_MAX_MSG_LEN];
} oipcNotificationMessage;


/** Error checking macros to apply to message fields when parsing.
 *  Use like so:
 *  IF_VERIFY_BOOL(stateP)
 *    {
 *      mState = stateP->GetBool();
 *    }
 *
 *  If the test fails, a warning message is printed to the console.
 */
#define IF_VERIFY_BOOL(p)                                                 \
  if (p != nullptr)                                                       \
    {                                                                     \
      if (!p->IsBool())                                                   \
        {                                                                 \
          LOGW("JSON Field should be a bool but is not at file %s, line %d\n", \
             __FILE__, __LINE__);                                         \
        }                                                                 \
    }                                                                     \
  else                                                                    \
    {                                                                     \
      LOGI("Bool field not found in JSON message at file %s, line%d\n",   \
           __FILE__, __LINE__);                                           \
    }                                                                     \
  if ((p != nullptr) && p->IsBool())

#define IF_VERIFY_INT(p)                                                  \
  if (p != nullptr)                                                       \
    {                                                                     \
      if (!p->IsInt())                                                    \
        {                                                                 \
          LOGW("JSON Field should be an int but is not at file %s, line %d\n", \
             __FILE__, __LINE__);                                         \
        }                                                                 \
    }                                                                     \
  else                                                                    \
    {                                                                     \
      LOGI("Int field not found in JSON message at file %s, line%d\n",    \
           __FILE__, __LINE__);                                           \
    }                                                                     \
  if ((p != nullptr) && p->IsInt())

#define IF_VERIFY_STRING(p)                                               \
  if (p != nullptr)                                                       \
    {                                                                     \
      if (!p->IsString())                                                 \
        {                                                                 \
          LOGW("JSON Field should be a string but is not at file %s, line %d\n", \
             __FILE__, __LINE__);                                         \
        }                                                                 \
    }                                                                     \
  else                                                                    \
    {                                                                     \
      LOGI("String field not found in JSON message at file %s, line%d\n", \
           __FILE__, __LINE__);                                           \
    }                                                                     \
  if ((p != nullptr) && p->IsString())


#endif  /* SWITCHBOARDMESSAGES_HPP */

