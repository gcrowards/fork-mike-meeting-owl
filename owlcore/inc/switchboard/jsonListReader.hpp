/*
 * jsonListReader.hpp
 *
 *  Created on: Mar 5, 2017
 *
 * Class for unpacking list of JSON objects.
 */

#ifndef OWLCORE_INC_SWITCHBOARD_JSONLISTREADER_HPP_
#define OWLCORE_INC_SWITCHBOARD_JSONLISTREADER_HPP_

#include <string>

#include "rapidjson/document.h"

/** Class that unpacks an array of JSON objects. */
class cJsonListReader
{
public:
  cJsonListReader();
  ~cJsonListReader();

  bool setList(std::string listMsg, bool pretty = false);
  bool getNextObject(std::string key, std::string &object);
  void resetList(void);
  int  getObjectCount(void);

private:
  rapidjson::Document mDoc;       /**< Document containing the array. */
  rapidjson::SizeType mNextItem;  /**< Index of the next item in the array. */
  int  mObjectCount;              /**< Number of objects in the array. */
  bool mValid;                    /**< Flag that's true if document is valid. */
};

#endif /* OWLCORE_INC_SWITCHBOARD_JSONLISTREADER_HPP_ */
