/*
 * pinnedAoi.hpp
 *
 *  Created on: Mar 2, 2017
 *
 *  Parent class for managing pinned AOI (Area of Interest) arguments.
 */

#ifndef OWLCORE_INC_SWITCHBOARD_PINNEDAOI_HPP_
#define OWLCORE_INC_SWITCHBOARD_PINNEDAOI_HPP_

#include "switchboard/enumString.hpp"


ENUM_WITH_STRINGS(ePinnedAoiCmd_t, int,
     NONE = 0,   /**< No command. */
     CREATE,     /**< Create a new pinned AOI. */
     CREATE_AUTO,/**< Create new pinned AOI but let Attention System choose the
		  * direction/size. */
     DELETE,     /**< Delete an existing AOI. */
     MOVE,       /**< Move an existing AOI. */
     PIN,        /**< Pin the AOI in place and clear AOI border graphic. */
     GET,        /**< Get parameters of existing AOI. */
     GET_ALL,    /**< Get info on ALL AOIs known to the system. */
     DELETE_ALL  /**< Delete all pinned AOIs. */
);


#endif /* OWLCORE_INC_SWITCHBOARD_PINNEDAOI_HPP_ */
