/*
 * switchboardServer.hpp
 *
 * Copyright (c) Owl Labs Inc. 2017.
 * All rights reserved.
 *
 *  Implements the OIPC (Owl Inter-Process Communications) server. The server
 *  allows clients to connect on the port 'TCP_PORT', subscribe to types
 *  of messages, and handle routing among clients. Each client writes messages
 *  to the server identified by type. Messages of a given type are then
 *  forwarded to every client that subscribed to that type.
 */
#ifndef SWITCHBOARDSERVER_HPP
#define SWITCHBOARDSERVER_HPP


#include <vector>

#include "switchboard/switchboardMessages.hpp"
#include "thread.hpp"


/* Generalized message reader and writer functions for both client and server */
int gReadMessage(int sockFd,
                 oipcMessageHeader *pHeader,
                 char *pcMsg,
                 size_t length);
int gWriteMessage(int sockFd,
                  eOipcMessageType type,
                  char *pcMsg,
                  size_t len);


/** [Design Explanation](pages.html) | 
 * Server object for the OIPC server infrastructure. */
class cSwitchboardServer
{
public:
  cSwitchboardServer();
  ~cSwitchboardServer();

private:
  class cServerInstance;  /* Forward declaration needed in following typedef
                           * that is used in the class declaration. */
  /** Enumeration of reasons the server tells client to shut down. */
  typedef enum
  {
    NONE,              /**< No reason yet specified. */
    NORMAL_SHUTDOWN,   /**< Normal shutdown. */
    VERSION_MISMATCH   /**< Shut down client because major version wrong. */
  } eDyingReason_t;
  typedef std::function<void(cSwitchboardServer::cServerInstance *, eDyingReason_t)>
                       clientDyingCallback_t; /**< Type of function called
                                               * to inform parent server that
                                               * a client connection ended. */

  /** Nested class to manage an instance of a OIPC server that interacts
   * with a single client. */
  class cServerInstance
  {
  public:
    cServerInstance(int sockFd, clientDyingCallback_t clientDyingCB);
    ~cServerInstance();

    bool hasMessage(void);
    bool getMessage(oipcMessageHeader **ppHeader, char **ppMsg);
    void releaseMessage(void);
    bool isTerminated();
    void setTerminationReason(eDyingReason_t reason);
    void writeMessage(eOipcMessageType type, char *pcMsg, uint32_t length);
    void terminate(eDyingReason_t reason);
    int  getSockFd(void);

  private:
    cThread *mpInstanceThread;  /**< Thread that reads for this instance */
    clientDyingCallback_t mClientDyingCB; /**< Informs parent when instance
                                           * dies */
    int mSockFd;        /**< File descriptor of socket for this instance */
    oipcMessageHeader mHeader; /**< Header of most recently received message */
    char mMsg[OIPC_MAX_MSG_LEN]; /**< Payload of most recent message */
    bool mMsgValid;            /**< true if message has been received and not
                                * yet sent */
    eDyingReason_t mTerminationReason;   /**< When not NONE, indicates instance
                                          * is ready to be terminated */

    void serverInstanceFunc();
  };

  /** Nested structure used to hold information about an instance of a server
   * including the instance structure and the list of message types to which
   * the instance has subscriptions.
   */
  typedef struct
  {
    cSwitchboardServer::cServerInstance *pInst;   /**< Instance of a connection
                                                   * to a client. */
    std::vector<eOipcMessageType> typeList; /**< List of subscribed types. */
  } instanceSubscriptionList_t;

  // Private parts of the cSwitchboardServer object.
  cThread mRestartThread;   /**< Thread to start and restart socket */
  cThread mWriterThread;    /**< Thread to write messages to clients */
  cThread *mpServerConnectionFactoryThread; /**< Thread to create instances */
  int mSockFd; /**< File descriptor of primary socket */
  std::vector<instanceSubscriptionList_t *> mSubscriptionListsList; /**< List
       * of structs that include threads, each of which manages a connection
       * with a client, as well as a list of message types the connection
       * has subscribed to. */
  int mSubscriptionListLooperCount; /**< Count of threads currently looping
                                     * through the list. Only allow pruning
                                     * when count is zero. Only access count
                                     * when mSubListMutex is locked. */
  pthread_mutex_t mSubListMutex; /**< Mutex protecting access to the
                                  * mSubscriptionListsList vector. */

  void writeManager(void);
  void serverFactory(void);
  void socketFinish(void);
  void restartServer(void);
  int  connectSocket(void);
  void lockSubscriptionList(void);
  void unlockSubscriptionList(void);
  bool lockSubscriptionListExclusive(bool wait = true);
  void unlockSubscriptionListExclusive(void);
  void sendToClients(oipcMessageHeader *pHeader, char *pMsg);
  void pruneDeadInstances(void);
  void clientDyingCallback(cSwitchboardServer::cServerInstance *dyingClient,
                           eDyingReason_t reason);
  void addSubscription(cSwitchboardServer::cServerInstance *pServerInst,
                       eOipcMessageType type);
  void removeSubscription(cSwitchboardServer::cServerInstance *pServerInst,
                          eOipcMessageType type);
  bool isSubscribed(std::vector<eOipcMessageType>list,
                    eOipcMessageType type);
};

#endif  /* SWITCHBOARDSERVER_HPP */

