/*
 * meetingInfo.hpp
 *
 *  Created on: Mar 30, 2017
 *
 *  Class defining packets for providing meeting information.
 */

#ifndef MEETING_INFO_HPP
#define MEETING_INFO_HPP

#include "analytics.hpp"
#include <cstdint>

/** Define strings that make up the JSON vocabulary for UiCommand messages.
 * A meeting status message has the syntax:
 * {"MeetingInfo":
 *   {"state":true}}
 */
#define MEETING_INFO_ARGS_STR    "MeetingInfo"
#define MEETING_INFO_STATE_STR   "state"

/** Define RapidJson "pointers" for accessing specific message fields. */
#define MEETING_INFO_STATE_PTR "/" MEETING_INFO_ARGS_STR "/" MEETING_INFO_STATE_STR

class cMeetingInfo
{
public:
  static const char pcMeetingInfoDOM[];
  
  cMeetingInfo();
  cMeetingInfo(bool state);
  ~cMeetingInfo();
  
  bool parseMsg(const char *pMsg);
  bool buildMsg(char *pMsgBuf, int bufLen);

  bool getArgs(bool &state);
  void abortArgs();
  bool argsConsumed();

  bool getState() const;

private:
  bool mState;     /**< current state of meeting. */
  bool mConsumed;  /**< false after message parsed until marked used. */
};

#endif // MEETING_INFO_HPP
