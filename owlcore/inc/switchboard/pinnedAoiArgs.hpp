/*
 * pinnedAoiArgs.hpp
 *
 *  Created on: Mar 2, 2017
 *
 *  Class for managing pinned AOI (Area of Interest) arguments. Includes
 *  the ability to serialize and deserialize data as JSON strings.
 *  Also defines a message to use for sending replies.
 */

#ifndef OWLCORE_INC_SWITCHBOARD_PINNEDAOIARGS_HPP_
#define OWLCORE_INC_SWITCHBOARD_PINNEDAOIARGS_HPP_

#include "switchboard/pinnedAoi.hpp"

/** Define strings that make up the JSON vocabulary for pinned Aoi messages.
 * A pinned AOI message has the syntax:
 * {"PinnedAoi":
 *   {"cmd":"cmdStr",
 *    "id":0,
 *    "size":0,
 *    "angle":0}}
 */
#define PINNED_AOI_ARGS_STR   "PinnedAoi"
#define PINNED_AOI_CMD_STR    "cmd"
#define PINNED_AOI_ID_STR     "id"
#define PINNED_AOI_SIZE_STR   "size"
#define PINNED_AOI_ANGLE_STR  "angle"

/** Define RapidJson "pointers" for accessing specific message fields. */
#define PINNED_AOI_CMD_PTR    "/" PINNED_AOI_ARGS_STR "/" PINNED_AOI_CMD_STR
#define PINNED_AOI_ID_PTR     "/" PINNED_AOI_ARGS_STR "/" PINNED_AOI_ID_STR
#define PINNED_AOI_SIZE_PTR   "/" PINNED_AOI_ARGS_STR "/" PINNED_AOI_SIZE_STR
#define PINNED_AOI_ANGLE_PTR  "/" PINNED_AOI_ARGS_STR "/" PINNED_AOI_ANGLE_STR

/** Pinned AOI (Area Of Interest) arguments JSON interface */
class cPinnedAoiArgs
{
public:
  cPinnedAoiArgs();
  cPinnedAoiArgs(ePinnedAoiCmd_t cmd, int id);
  cPinnedAoiArgs(ePinnedAoiCmd_t cmd, int id, int size, int angle);
  ~cPinnedAoiArgs();

  bool parseMsg(const char *pMsg);
  bool buildMsg(char *pMsgBuf, int bufLen);

  bool getArgs(ePinnedAoiCmd_t &cmd, int &id, int &size, int &angle);
  void abortArgs();
  bool getArgsConsumed();
  void setArgsConsumed();

  ePinnedAoiCmd_t getCommand(void);
  int  getId(void);
  int  getSize(void);
  int  getAngle(void);

  void setCommand(ePinnedAoiCmd_t cmd);
  void setId(int id);
  void setSize(int size);
  void setAngle(int angle);

private:
  ePinnedAoiCmd_t mCmd;  /**< Command in this message. */
  int mId;               /**< ID identifying this message's AOI. */
  int mSize;             /**< Angular extent in degrees of this AOI. */
  int mAngle;            /**< Central angle in degrees of this AOI. */

  bool mConsumed;        /**< false after message parsed until marked used. */
};


#endif /* OWLCORE_INC_SWITCHBOARD_PINNEDAOIARGS_HPP_ */
