/*
 * videoParams.hpp
 *
 * Header for class that defines JSON messages for exchanging video
 * setup parameters over a Switchboard connection.
 *
 * Copyright (c) Owl Labs Inc. 2017.
 * All rights reserved.
 *
 * Created on: Aug 14, 2017
 */

#ifndef OWLCORE_INC_SWITCHBOARD_VIDEOPARAMS_HPP_
#define OWLCORE_INC_SWITCHBOARD_VIDEOPARAMS_HPP_

#include <string>

#include "switchboard/enumString.hpp"

/** Values for the "cmd" field of an "audioParameters" message. */
ENUM_WITH_STRINGS(eVideoParametersCmd_t, int,
                  NONE,
                  SET,
                  GET);

/** Strings that define the JSON vocabulary for video parameters messages.
 * An video parameters set message has the syntax:
 * {"videoParameters":
 *   {"cmd":"set",
 *    "key":"keyString",
 *    "value":"valueString"}}
 *
 * An video parameters set/get reply message has the syntax:
 * {"videoParameters":
 *   {"cmd":"get",
 *    "key":"keyString",
 *    "value":"valueString"}}
 */
#define VIDEO_PARAMETERS_STR                       "videoParameters"
#define VIDEO_PARAMETERS_CMD_STR                   "cmd"
#define VIDEO_PARAMETERS_KEY_STR                   "key"
#define VIDEO_PARAMETERS_VALUE_STR                 "value"

/** Define RapidJson "pointers" for accessing specific message fields. */
#define VIDEO_PARAMETERS_CMD_PTR                        \
  "/" VIDEO_PARAMETERS_STR "/" VIDEO_PARAMETERS_CMD_STR

#define VIDEO_PARAMETERS_KEY_PTR      \
  "/" VIDEO_PARAMETERS_STR "/" VIDEO_PARAMETERS_KEY_STR

#define VIDEO_PARAMETERS_VALUE_PTR      \
  "/" VIDEO_PARAMETERS_STR "/" VIDEO_PARAMETERS_VALUE_STR

/** Video Parameters get/set arguments JSON interface */
class cVideoParameters
{
public:
  cVideoParameters();
  cVideoParameters(eVideoParametersCmd_t cmd);
  cVideoParameters(eVideoParametersCmd_t cmd, std::string key);
  cVideoParameters(eVideoParametersCmd_t cmd, std::string key, std::string value);
  ~cVideoParameters();

  bool parseMsg(const char *pMsg);
  bool buildMsg(char *pMsgBuf, int bufLen);

  bool getArgs(std::string &key, std::string &value);

  eVideoParametersCmd_t getCommand(void);
  std::string getKey(void);
  std::string getValue(void);

private:
  eVideoParametersCmd_t mCmd; /**< Cmd of this message (set or get). */
  std::string mKey;           /**< Key identifying the parameter of interest. */
  std::string mValue;         /**< Value associated with this key. */
};


#endif /* OWLCORE_INC_SWITCHBOARD_VIDEOPARAMS_HPP_ */
