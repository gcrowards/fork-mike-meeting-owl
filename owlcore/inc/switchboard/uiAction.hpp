/*
 * uiAction.hpp
 *
 *  Created on: Mar 30, 2017
 *
 *  Class defining action commands for Owl hardware UI (LEDs/buttons).
 */

#ifndef UI_ACTION_HPP
#define UI_ACTION_HPP

#include <cstdint>
#include "switchboard/enumString.hpp"

/** Define strings that make up the JSON vocabulary for UiAction messages.
 * A UI action message has the syntax:
 * {"UiAction":
 *   {"cmd":"cmdStr"}}
 */
#define UI_ACTION_ARGS_STR    "UiAction"
#define UI_ACTION_CMD_STR     "cmd"

/** Define RapidJson "pointers" for accessing specific message fields. */
#define UI_ACTION_CMD_PTR    "/" UI_ACTION_ARGS_STR "/" UI_ACTION_CMD_STR

ENUM_WITH_STRINGS(uiCommand_t, int,
                  NONE = 0,           // no command
                  PING_START_SLOW,    // ping the owl for a slow visual response
                  PING_START_FAST,    // ping the owl for a fast visual response
                  PING_STOP,          // stop ping visual response
                  OTA_UPDATE_START,   // start OTA update indication
                  OTA_UPDATE_STOP    // stop OTA update indication
);

class cUiAction
{
public:
  static const char pcUiActionDOM[];
  static const int mkSlowPingBlinkOnTimeMs;
  static const int mkSlowPingBlinkOffTimeMs;
  static const int mkFastPingBlinkOnTimeMs;
  static const int mkFastPingBlinkOffTimeMs;
  
  cUiAction();
  cUiAction(uiCommand_t cmd);
  ~cUiAction();
  
  bool parseMsg(const char *pMsg);
  bool buildMsg(char *pMsgBuf, int bufLen);

  bool getArgs(uiCommand_t &cmd);
  void abortArgs();
  bool argsConsumed();

  uiCommand_t getCommand(void);

private:
  
  uiCommand_t mCmd;  /**< Command in this message. */
  bool mConsumed;    /**< false after message parsed until marked used. */
};

#endif // UI_ACTION_HPP
