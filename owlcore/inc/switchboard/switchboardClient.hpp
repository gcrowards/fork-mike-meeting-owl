/*
 * switchboardClient.hpp
 *
 * Copyright (c) Owl Labs Inc. 2017.
 * All rights reserved.
 *
 *  Owl IPC (Owl Inter Process Communications) client implementation.
 */
#ifndef SWITCHBOARDCLIENT_HPP
#define SWITCHBOARDCLIENT_HPP

#include <string>
#include <vector>

#include "switchboard/switchboardServer.hpp"
#include "thread.hpp"


/** Callback function type for OIPC messages. */
typedef std::function<void(eOipcMessageType type, const char *pMsg, int msgLen)>
                      oipcMessageCallback_t;

#define SERVER_VER_ERR "Server version error!"

/** [Design Explanation](pages.html) | 
 * Client object for the OIPC client infrastructure. */
class cSwitchboardClient
{
public:
  cSwitchboardClient(std::string ipAddr, oipcMessageCallback_t errorCB);
  ~cSwitchboardClient();

  int  subscribe(eOipcMessageType type, oipcMessageCallback_t cb);
  int  unSubscribe(eOipcMessageType type);
  bool isConnectionOpen(void);
  bool isErrorState(void);

  int writeMessage(eOipcMessageType type, char *pMsg, size_t len);

private:
  /** Internal structure that maps message types to callback functions that
   * should be invoked when messages of that type arrive.
   */
  typedef struct
  {
    eOipcMessageType mType;      /**< Type of messages subscribed to. */
    oipcMessageCallback_t mCB;   /**< Callback to invoke with msg. */
  } oipcTypeCallback_t;

  cThread mRestartThread;  /**< Thread for (re)starting the connection. */
  cThread *mpReaderThread; /**< Thread for reading messages from the server. */
  oipcMessageCallback_t mErrorCB; /**< Callback called on link error. */
  std::vector<oipcTypeCallback_t> mTypeCallbackList; /**< List of message type
                                                      * to callback mappings.*/
  pthread_mutex_t mTypeListMutex; /**< Mutex to control access to list. */
  std::string mIpAddr;     /**< IP address of server. */
  int mSockFd;             /**< File descriptor of open socket to server. */
  bool mConnected;         /**< Flag set true when connection is valid. */
  bool mError;             /**< Flag set true when client is in error state. */
  oipcMessageHeader mHeader; /**< Header of most recently received message. */
  char mMsg[1024];         /**< Most recently received message buffer. */

  int init(void);
  int finish(void);
  void readerThreadFunc(void);
  void retryConnectionFunc(void);
};

#endif  /* SWITCHBOARDCLIENT_HPP */
