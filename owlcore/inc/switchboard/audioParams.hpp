/*
 * audioParams.hpp
 *
 * Header for class that defines JSON messages for exchanging audio
 * parameters over a Switchboard connection.
 *
 * Copyright (c) Owl Labs Inc. 2017.
 * All rights reserved.
 *
 * Created on: Jul 25, 2017
 */

#ifndef OWLCORE_INC_SWITCHBOARD_AUDIOPARAMS_HPP_
#define OWLCORE_INC_SWITCHBOARD_AUDIOPARAMS_HPP_

#include "switchboard/enumString.hpp"

/** Values for the "cmd" field of an "audioParameters" message. */
ENUM_WITH_STRINGS(eAudioParametersCmd_t, int,
                  NONE,
                  SET,
                  GET);

/** Strings that define the JSON vocabulary for audio parameters messages.
 * An audio parameters set message has the syntax:
 * {"audioParameters":
 *   {"cmd":"set",
 *    "volume":100,
 *    "mute":false,
 *    "minSpeakerLevel":-32,
 *    "noiseSuppressionLevel":10,
 *    "enableDTD":false}}
 *
 * An audio parameters set/get reply message has the syntax:
 * {"audioParameters":
 *   {"cmd":"get",
 *    "volume":100,
 *    "mute":false,
 *    "minSpeakerLevel":-42,
 *    "noiseSuppressionLevel":0,
 *    "enableDTD":false}}
 */
#define AUDIO_PARAMETERS_STR                          "audioParameters"
#define AUDIO_PARAMETERS_CMD_STR                      "cmd"
#define AUDIO_PARAMETERS_NOISE_LEVEL_THRESHOLD_STR    "minSpeakerLevel"
#define AUDIO_PARAMETERS_VOLUME_STR                   "volume"
#define AUDIO_PARAMETERS_MUTE_STR                     "mute"
#define AUDIO_PARAMETERS_NOISE_SUPPRESSION_LEVEL_STR  "noiseSuppressionLevel"
#define AUDIO_PARAMETERS_ENABLE_DTD_STR               "enableDTD"

/** Define RapidJson "pointers" for accessing specific message fields. */
#define AUDIO_PARAMETERS_OBJECT_PTR                     \
  "/" AUDIO_PARAMETERS_STR

#define AUDIO_PARAMETERS_CMD_PTR                        \
  "/" AUDIO_PARAMETERS_STR "/" AUDIO_PARAMETERS_CMD_STR

#define AUDIO_PARAMETERS_NOISE_LEVEL_THRESHOLD_PTR      \
  "/" AUDIO_PARAMETERS_STR "/" AUDIO_PARAMETERS_NOISE_LEVEL_THRESHOLD_STR

#define AUDIO_PARAMETERS_VOLUME_PTR \
  "/" AUDIO_PARAMETERS_STR "/" AUDIO_PARAMETERS_VOLUME_STR

#define AUDIO_PARAMETERS_MUTE_PTR \
  "/" AUDIO_PARAMETERS_STR "/" AUDIO_PARAMETERS_MUTE_STR

#define AUDIO_PARAMETERS_NOISE_SUPPRESSION_LEVEL_PTR      \
  "/" AUDIO_PARAMETERS_STR "/" AUDIO_PARAMETERS_NOISE_SUPPRESSION_LEVEL_STR

#define AUDIO_PARAMETERS_ENABLE_DTD_PTR      \
  "/" AUDIO_PARAMETERS_STR "/" AUDIO_PARAMETERS_ENABLE_DTD_STR

/** Audio Parameters get/set arguments JSON interface */
class cAudioParameters
{
public:
  cAudioParameters();
  cAudioParameters(eAudioParametersCmd_t cmd);
  cAudioParameters(eAudioParametersCmd_t cmd, int minSpeakerLevel,
                   int noiseSuppressionLevel);
  cAudioParameters(eAudioParametersCmd_t cmd, int volume, bool mute,
                   int minSpeakerLevel, int noiseSuppressionLevel);
  cAudioParameters(eAudioParametersCmd_t cmd, int volume, bool mute,
                   int minSpeakerLevel, int noiseSuppressionLevel,
                   bool enableDTD);
  ~cAudioParameters();

  bool parseMsg(const char *pMsg);
  bool buildMsg(char *pMsgBuf, int bufLen);

  bool getArgs(int &volume, bool &volumeValid,
               bool &mute, bool &muteValid,
               int &minSpeakerLevel, bool &minSpeakerLevelValid,
               int &noiseSuppressionLevel, bool &noiseSuppressionLevelValid,
               bool &enableDTD, bool &enableDTDValid);

  bool getVolume(int &volume);
  bool getMute(bool &mute);
  bool getMinSpeakerLevel(int &minSpeakerLevel);
  int  getNoiseSuppressionLevel(int &noiseSuppressionLevel);
  bool getEnableDTD(bool &enable);

  void setMinSpeakerLevel(int minSpeakerLevel);
  void setVolume(int volume);
  void setMute(bool mute);
  void setNoiseSuppressionLevel(int level);
  void setEnableDTD(bool enable);

  eAudioParametersCmd_t getCommand(void);

private:
  int  mMinSpeakerLevel;         /**< Noise level threshold setting. */
  bool mMinSpeakerLevelValid;   /**< true if value set in mMinSpeakerLevel. */
  int  mVolume;                 /**< Speaker volume setting (0 - 100). */
  bool mVolumeValid;            /**< true if volume is valid. */
  bool mMute;                   /**< Speaker and microphone mute. */
  bool mMuteValid;              /**< true if mute is valid. */
  int  mNoiseSuppressionLevel;  /**< noise suppression level value. */
  bool mNoiseSuppressionLevelValid; /**< true if value is valid. */
  bool mEnableDTD;              /**< Enable (true) or disable double talk. */
  bool mEnableDTDValid;         /**< true if value is valid. */
  eAudioParametersCmd_t mCmd;   /**< Cmd of this message (set or get). */
};


#endif /* OWLCORE_INC_SWITCHBOARD_AUDIOPARAMS_HPP_ */
