/*
 * setupProg.hpp
 *
 *  Created on: May 7, 2017
 */

#ifndef OWLCORE_INC_SWITCHBOARD_SETUPPROG_HPP_
#define OWLCORE_INC_SWITCHBOARD_SETUPPROG_HPP_

#include "switchboard/enumString.hpp"

ENUM_WITH_STRINGS(eSetupProgress_t, int,
                  NONE = 0,
                  WIFI_ONLY,
                  REGISTRATION_ONLY,
                  WIFI_AND_REGISTRATION,
                  TUTORIAL_ONLY,
                  TUTORIAL_AND_WIFI,
                  TUTORIAL_AND_REGISTRATION,
                  ALL);

/** String that defines the JSON vocabulary for setup progress messages.
 * A setup progress set SETUP_PROGRESS_SET message has the syntax:
 * {"SetupProgressSet":
 *   {"state":0}}
 *
 * A setup progress get reply SETUP_PROGRESS_GET_REPLY message syntax is:
 * {"SetupProgressGetReply":
 *   {"state":7}}
 *
 * In each case, "stateVal" is an integer element of eSetupProgress_t.
 */

#define SETUP_PROGRESS_SET_STR       "SetupProgressSet"
#define SETUP_PROGRESS_GET_REPLY_STR "SetupProgressGetReply"
#define SETUP_PROGRESS_STATE_STR     "state"

/** Define RapidJson "pointers" for accessing specific message fields. */
#define SETUP_PROGRESS_SET_STATE_PTR \
  "/" SETUP_PROGRESS_SET_STR "/" SETUP_PROGRESS_STATE_STR
#define SETUP_PROGRESS_GET_REPLY_STATE_PTR \
  "/" SETUP_PROGRESS_GET_REPLY_STR "/" SETUP_PROGRESS_STATE_STR

/** Pinned AOI (Area Of Interest) arguments JSON interface */
class cSetupProgressMsgs
{
public:
  cSetupProgressMsgs(std::string type);
  cSetupProgressMsgs(std::string type, eSetupProgress_t state);
  ~cSetupProgressMsgs();

  bool parseMsg(const char *pMsg);
  bool buildMsg(char *pMsgBuf, int bufLen);

  bool getArgs(eSetupProgress_t &state);
  void abortArgs();
  bool getArgsConsumed();
  void setArgsConsumed();

  eSetupProgress_t getState(void);

private:
  eSetupProgress_t mState;  /**< State in this message. */
  std::string mType;        /**< Type of this message (set or get_reply). */
  bool mConsumed;        /**< false after message parsed until marked used. */
};





#endif /* OWLCORE_INC_SWITCHBOARD_SETUPPROG_HPP_ */
