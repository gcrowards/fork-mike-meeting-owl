/*
 * EnumString.hpp
 *
 *  Created on: Apr 21, 2017
 *
 *  Typical usage:
 *
 *  Define Enumeration:
 *  ENUM_WITH_STRINGS(eColorChannel, RED = 1, GREEN, BLUE);
 *
 *  First argument is enum name. Subsequent entries define enumerated
 *  values.
 *
 *  Reference enumerated value:
 *  eColorChannel channel = eColorChannel::RED;
 *
 *  Get string given enumeration:
 *  std::string channelString = channel._to_string();
 *
 *  Get enumeration given string:
 *  eColorChannel channel = eColorChannel::fromString(eColorChannel::RED);
 *
 *  WARNING:
 *  In the current implementation this only supports a maximum of 12
 *  values in the enumeration!
 *
 *  NOTE:
 *  Most of this implementation was drawn from a StackOverflow
 *  answer at the following link:
 *  http://stackoverflow.com/questions/28828957/enum-to-string-in-modern-c-and-future-c17-c20
 *  However, it needed modification to eliminate the use of exceptions.
 */

#ifndef OWLCORE_INC_SWITCHBOARD_ENUMSTRING_HPP_
#define OWLCORE_INC_SWITCHBOARD_ENUMSTRING_HPP_


#include <limits.h>     // For INT_MIN
#include <cstddef>      // For size_t.
#include <cstring>      // For strcspn, strncpy.
#include <stdexcept>    // For runtime_error.

#include "common.hpp"   // For convertEnumToCamelCase.


// A "typical" mapping macro. MAP(macro, a, b, c, ...) expands to
// macro(a) macro(b) macro(c) ...
// The helper macro COUNT(a, b, c, ...) expands to the number of
// arguments, and IDENTITY(x) is needed to control the order of
// expansion of __VA_ARGS__ on Visual C++ compilers.
#define MAP(macro, ...)                                               \
    IDENTITY(                                                         \
        APPLY(CHOOSE_MAP_START, COUNT(__VA_ARGS__))                   \
            (macro, __VA_ARGS__))

#define CHOOSE_MAP_START(count) MAP ## count

#define APPLY(macro, ...) IDENTITY(macro(__VA_ARGS__))

#define IDENTITY(x) x

#define MAP1(m, x)      m(x)
#define MAP2(m, x, ...) m(x) IDENTITY(MAP1(m, __VA_ARGS__))
#define MAP3(m, x, ...) m(x) IDENTITY(MAP2(m, __VA_ARGS__))
#define MAP4(m, x, ...) m(x) IDENTITY(MAP3(m, __VA_ARGS__))
#define MAP5(m, x, ...) m(x) IDENTITY(MAP4(m, __VA_ARGS__))
#define MAP6(m, x, ...) m(x) IDENTITY(MAP5(m, __VA_ARGS__))
#define MAP7(m, x, ...) m(x) IDENTITY(MAP6(m, __VA_ARGS__))
#define MAP8(m, x, ...) m(x) IDENTITY(MAP7(m, __VA_ARGS__))
#define MAP9(m, x, ...) m(x) IDENTITY(MAP8(m, __VA_ARGS__))
#define MAP10(m, x, ...) m(x) IDENTITY(MAP9(m, __VA_ARGS__))
#define MAP11(m, x, ...) m(x) IDENTITY(MAP10(m, __VA_ARGS__))
#define MAP12(m, x, ...) m(x) IDENTITY(MAP11(m, __VA_ARGS__))

#define EVALUATE_COUNT(_1, _2, _3, _4, _5, _6, \
                       _7, _8, _9, _10, _11, _12, count, ...) \
                       count

#define COUNT(...) \
    IDENTITY(EVALUATE_COUNT(__VA_ARGS__, 12, 11, 10, 9, 8, 7, \
                            6, 5, 4, 3, 2, 1))



// The type "T" mentioned above that drops assignment operations.
template <typename U>
struct ignoreAssign_s {
    constexpr explicit ignoreAssign_s(U value) : _value(value) { }
    constexpr operator U() const { return _value; }

    constexpr const ignoreAssign_s& operator =(int dummy) const
        { return *this; }

    U   _value;
};



// Prepends "(ignoreAssign_s<_underlying>)" to each argument.
#define IGNORE_ASSIGN_SINGLE(e) (ignoreAssign_s<_underlying>)e,
#define IGNORE_ASSIGN(...) \
    IDENTITY(MAP(IGNORE_ASSIGN_SINGLE, __VA_ARGS__))

// Stringizes each argument.
#define STRINGIZE_SINGLE(e) #e,
#define STRINGIZE(...) IDENTITY(MAP(STRINGIZE_SINGLE, __VA_ARGS__))



// Some helpers needed for fromString.
constexpr const char    terminators[] = " =\t\r\n";

// The size of terminators includes the implicit '\0'.
constexpr bool isTerminator(char c, size_t index = 0)
{
    return
        index >= sizeof(terminators) ? false :
        c == terminators[index] ? true :
        isTerminator(c, index + 1);
}

// Recursive string compare with additional termination options.
constexpr bool matchesUntrimmed(const char *untrimmed, const char *s,
                                 size_t indexU = 0, size_t indexS = 0)
{
    return
        isTerminator(untrimmed[indexU]) ?
            s[indexS] == '\0' :
        (untrimmed[indexU] == '_') ?
            matchesUntrimmed(untrimmed, s, indexU + 1, indexS) :
        (toupper(s[indexS]) != toupper(untrimmed[indexU])) ?
            false :
            matchesUntrimmed(untrimmed, s, indexU + 1, indexS + 1);
}


// The macro proper.
//
// There are several "simplifications" in this implementation, for the
// sake of brevity. First, we have only one viable option for declaring
// constexpr arrays: at namespace scope. This probably should be done
// two namespaces deep: one namespace that is likely to be unique for
// our little enum "library", then inside it a namespace whose name is
// based on the name of the enum to avoid collisions with other enums.
// I am using only one level of nesting.
//
// Declaring constexpr arrays inside the struct is not viable because
// they will need out-of-line definitions, which will result in
// duplicate symbols when linking. This can be solved with weak
// symbols, but that is compiler- and system-specific. It is not
// possible to declare constexpr arrays as static variables in
// constexpr functions due to the restrictions on such functions.
//
// Note that this prevents the use of this macro anywhere except at
// namespace scope. Ironically, the C++98 version of this, which can
// declare static arrays inside static member functions, is actually
// more flexible in this regard. It is shown in the CodeProject
// article.
//
// Second, for compilation performance reasons, it is best to separate
// the macro into a "parametric" portion, and the portion that depends
// on knowing __VA_ARGS__, and factor the former out into a template.
//
// Third, this code uses a default parameter in fromString that may
// be better not exposed in the public interface.

/** Macro for creating an enumeration with string representations.
 *
 * Creates an enumeration with a given underlying type. The
 * enumeration values are used like normal enums, but it is possible
 * to access a string representation of a given enum, or to convert
 * a string into the corresponding enum value. Enum value symbols
 * are assumed to be composed of all capital letters with underscores
 * between words. Their corresponding string representations remove
 * the underscores, and convert from all capitals to camel-case.
 *
 * @param enumName is the name of the enumerated type.
 * @param underlying is the underlying data type of the enum.
 * @param ... represents zero or more enum values that may include
 *            assignments.
 */
#define ENUM_WITH_STRINGS(enumName, underlying, ...)                  \
namespace data_ ## enumName {                                         \
    using _underlying = underlying;                                   \
    enum { __VA_ARGS__ };                                             \
                                                                      \
    constexpr const size_t           _size =                          \
        IDENTITY(COUNT(__VA_ARGS__));                                 \
                                                                      \
    constexpr const _underlying      _values[] =                      \
        { IDENTITY(IGNORE_ASSIGN(__VA_ARGS__)) };                     \
                                                                      \
    constexpr const char * const     _rawNames[] =                    \
        { IDENTITY(STRINGIZE(__VA_ARGS__)) };                         \
}                                                                     \
                                                                      \
struct enumName {                                                     \
    using _underlying = underlying;                                   \
    enum _enum : _underlying { __VA_ARGS__ };                         \
                                                                      \
    const char * toString() const                                     \
    {                                                                 \
        for (size_t index = 0; index < data_ ## enumName::_size;      \
             ++index) {                                               \
                                                                      \
            if (data_ ## enumName::_values[index] == _value)          \
                return _trimmedNames()[index];                        \
        }                                                             \
                                                                      \
        return "";                                                    \
    }                                                                 \
                                                                      \
    constexpr static enumName fromString(const char *s,               \
                                           size_t index = 0)          \
    {                                                                 \
        return                                                        \
            index >= data_ ## enumName::_size ?                       \
                    (enumName)(_enum)INT_MIN :                        \
            matchesUntrimmed(                                         \
                data_ ## enumName::_rawNames[index], s) ?             \
                    (enumName)(_enum)data_ ## enumName::_values[      \
                                                            index] :  \
              fromString(s, index + 1);                               \
    }                                                                 \
                                                                      \
    enumName() = delete;                                              \
    constexpr enumName(_enum value) : _value(value) { }               \
    constexpr enumName(_underlying value) : _value(value) { }         \
    constexpr operator _enum() const { return (_enum)_value; }        \
                                                                      \
  private:                                                            \
    _underlying     _value;                                           \
                                                                      \
    static const char * const * _trimmedNames()                       \
    {                                                                 \
        static char     *theNames[data_ ## enumName::_size];          \
        static bool     initialized = false;                          \
                                                                      \
        if (!initialized) {                                           \
            for (size_t index = 0; index < data_ ## enumName::_size;  \
                 ++index) {                                           \
                                                                      \
                size_t  length =                                      \
                    std::strcspn(data_ ## enumName::_rawNames[index], \
                                 terminators);                        \
                                                                      \
                theNames[index] = new char[length + 1];               \
                                                                      \
                length = convertEnumToCamelCase(theNames[index],      \
                             data_ ## enumName::_rawNames[index],     \
                             length);                                 \
                theNames[index][length] = '\0';                       \
            }                                                         \
                                                                      \
            initialized = true;                                       \
        }                                                             \
                                                                      \
        return theNames;                                              \
    }                                                                 \
};




#endif /* OWLCORE_INC_SWITCHBOARD_ENUMSTRING_HPP_ */
