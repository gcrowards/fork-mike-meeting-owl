/*
 * jsonListWriter.hpp
 *
 *  Created on: Mar 5, 2017
 *
 * Class for composing a list of JSON objects.
 */

#ifndef OWLCORE_INC_SWITCHBOARD_JSONLISTWRITER_HPP_
#define OWLCORE_INC_SWITCHBOARD_JSONLISTWRITER_HPP_

#include <string>

#include "rapidjson/document.h"

/** Class that constructs a JSON array of JSON objects. */
class cJsonListWriter
{
public:
  cJsonListWriter();
  ~cJsonListWriter();

  bool addObject(std::string object);
  int  getList(std::string &list, bool pretty = false);
  int  getObjectCount(void);

// private:
  rapidjson::Document mDoc;   /**< RapidJson document containing array. */
  int mObjectCount;           /**< Number of objects in the array. */
};

#endif /* OWLCORE_INC_SWITCHBOARD_JSONLISTWRITER_HPP_ */
