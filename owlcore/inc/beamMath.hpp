#ifndef __beamMath_hpp
#define __beamMath_hpp

#include <vector>

#include "params.h"

/** This namespace contains math functions related to beam-forming. */
namespace beamMath
{
  void getBeamDelays(int pBeamDelays[][N_MICS], int scale);
  void simulatePcmData(int16_t *pPcmIn, unsigned int bufLen, int beam);
}

#endif  // __beamMath_hpp
