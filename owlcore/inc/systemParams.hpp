#ifndef __systemParams_hpp
#define __systemParams_hpp

/** This class is used for managing and updating input parameters.
 *
 * Parameters are either set to their defaults as dictated by params.h and
 * fledglingParams.h, or set based on a config file that can be populated with
 * a new set of parameters and placed in the filesystem at
 * /sdcard/inputParams.cfg.
 *
 * Users of the input parameters will need to reference the gpOwlParams class
 * instance, currently instantiated in main.cpp.  For convenient access, the
 * leading 'm' conventions have been purposely dropped from public parameter
 * variables.
 */
class cSystemParams
{
public:
  // constructor
  cSystemParams(void);
  
  // destructor
  ~cSystemParams();

  // data
  int cameraCenterX;        /**< camera center X parameter. */
  int cameraCenterY;        /**< camera center Y parameter. */
  
  // actions
  void loadParams(void);
  void loadOpticalCenter(void);

private:
  // actions
  int readOtpValue(const char *pValueName);
};

#endif
