/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  thread.hpp
 *  This is the header file for cThread.
 *   --> High-level wrapper for creating a standard thread with a name and
 *        priority. Uses pthreads internally.
 */

#ifndef THREAD_HPP
#define THREAD_HPP

#include "params.h"
#include <functional>
#include <string>

#define STRING_HELPER(x) #x
#define STRING_HELPER2(x) STRING_HELPER(x)
// macro to be passed as a default name for threads.
// name consists of the calling file name and line number.
//
// TODO: Consider whether this is useful/necessary.
//
#define DEFAULT_THREAD_NAME (__FILE__ ":" STRING_HELPER2(__LINE__))

typedef std::function<void()> threadCallback_t;

/** High-level wrapper for creating a standard thread with a name and priority.
 *  Uses pthreads internally. */
class cThread
{
public:
  cThread(threadCallback_t threadCallback, const std::string &threadName,
	  int threadPriority = DEFAULT_THREAD_PRIORITY );
  ~cThread();
  
  static int mNumThreads; /**< total number of cThread instances created. */
  //
  // TODO: Consider adding a variable similar to mNumThreads to keep track of
  //        the number of physical threads that are running at any point.
  //

  // control functions
  void start();
  void signalStop();
  void join();

  // accessors
  bool isRunning() const;
  std::string getName() const;
  int getPriority() const;
  
private:
  // members
  threadCallback_t mCallback; /**< callback to execute on each thread loop. */
  std::string mName; /**< name of the thread. */
  int mPriority; /**< thread priority level. */

  // internal resources
  pthread_t mThread; /**< pthread handle for this thread. */
  bool mRunning = false; /**< whether the thread should keep running. */
  bool mJoinNeeded = false; /**< whether the thread needs to be joined. */
  
  static void* threadWrapper(void *pContext);
};

#endif // THREAD_HPP
