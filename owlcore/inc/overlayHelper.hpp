#ifndef __overlayHelper_hpp
#define __overlayHelper_hpp

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <vector>
#include <string>

#include "common.hpp"
#include "utils/colorGenerator.hpp"

/** This class is used to draw helpful things on top of the output image. 
 *
 * For example, you might want to draw a circle around detected faces, or 
 * write a message on the screen.
 */
class cOverlayHelper
{
public:
  // constructors
  cOverlayHelper(void);

  // detructor
  ~cOverlayHelper();

  // actions
  void drawEllipse(cv::Rect bb, cv::Scalar color = RED,
		   float alpha = 1.0, int thickness = 1 );
  void drawRect(cv::Rect rect, cv::Scalar color = RED,
		float alpha = 1.0, int thickness = 1 );
  void drawLine(cv::Point startPoint, cv::Point endPoint, 
		cv::Scalar color = GREEN,
		float alpha = 1.0, int thickness = 1 );
  cv::Scalar getColorFromId(int id);

private:
  cColorGenerator mColorGenerator; /**< Generates unique-ish colors */
};

#endif
