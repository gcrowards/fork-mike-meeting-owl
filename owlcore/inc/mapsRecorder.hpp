#ifndef __mapsRecorder_hpp
#define __mapsRecorder_hpp

#include <fstream>
#include <string>
#include <time.h>
#include <vector>

#include "item.hpp"
#include "voice.hpp"


/** This class records person and voice map data.
 *
 * The intended use is to enable this to gather debug/test data while running
 * on real Meeting Owl hardware.  Data recorded with this module can be
 * played back with the mapsPlayer module (typically via the Linux desktop
 * application).
 */
class cMapsRecorder
{
public:
  // contructors
  cMapsRecorder();
  cMapsRecorder(std::string personDataFilename, std::string voiceDataFilename);

  // destructors
  ~cMapsRecorder();
  
  // actions
  bool start(void);
  void stop(void);
  bool recordState(std::vector<const cItem *> personMap, 
		   std::vector<cVoice> voiceMap );

  // accessors
  void setPersonDataFilename(std::string filename);
  void setVoiceDataFilename(std::string filename);

private:
  // data
  std::string mPersonDataFilename; /**< CSV file containing person map time 
				    * series data. */
  std::string mVoiceDataFilename;  /**< CSV file containing sound source location
				      (really voice bearing) time series data. */
  
  std::ofstream mPersonFileStream; /**< CSV output stream for person map. */
  std::ofstream mVoiceFileStream;  /**< CSV output stream for voice map. */

  struct timespec mStartTime;      /**< Wall time that corresponds to 0 second
				    * time in data files. */
      
  // internal actions
  void init(std::string personDataFilename, std::string voiceDataFilename);
  bool openPersonFile(void);
  bool openVoiceFile(void);
  void closeFiles(void);
  void writeHeaders(void);
};

#endif
