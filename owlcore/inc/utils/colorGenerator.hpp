#ifndef __colorGenerator_hpp
#define __colorGenerator_hpp

#include "opencv2/core/core.hpp"

/** This is a helper class that is used to generate RGB colors. */
class cColorGenerator
{
public:
  // constructors
  cColorGenerator(void);
  
  // destructor
  ~cColorGenerator();
  
  // actions
  
  // setters
  
  // getters
  cv::Scalar getColorFromSeed(int seed);
  
private:
  // data
  
  // actions
};

#endif
