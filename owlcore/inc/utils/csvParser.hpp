#ifndef __csvParser_hpp
#define __csvParser_hpp

#include <fstream>
#include <stdio.h>
#include <string>
#include <vector>

#include "logging.hpp"

// some custom types for convenience
namespace owl
{
  // list of lists of strings
  typedef std::vector<std::vector<std::string>> stringTable_t;

  // list of lists of arbitrary types
  template<typename T>
  using numericalTable_t = std::vector<std::vector<T>>;
}

/** This class is used to parse a comma separated file into a vector of string
 * vectors.  Optionally, it can also generate a vector of numerical vectors
 * by converting the string data to numerical data.
 *
 * By default, lines starting with "#" are discarded.  When generating the
 * numerical version of the parsed data, all lines with string data that can't 
 * be converted to numerical data are discarded.
 */
class cCsvParser
{
public:
  // contructors
  cCsvParser(bool discardCommentedLines = true);
  cCsvParser(std::string filename, bool discardedCommentedLines = true);
  
  // destructors
  ~cCsvParser();

  // accessors
  void setFilename(std::string filename);
  void setDiscardCommentedLines(bool discardCommentedLines);

  owl::stringTable_t getStringTable(void);

  /** This template function generates a numerical version of the CSV file
   * after it has been parsed into the mStringTable.  Strings that don't 
   * convert to numbers are discarded quietly.
   */
  template <typename T> 
  void fillNumericalTable(owl::numericalTable_t<T> *pNumericalTable)
  {
    for(auto row : mStringTable)
      {
	std::vector<T> numbers;

	for(auto item : row)
	  {
	    double number; 

	    if(sscanf(item.c_str(), "%lf", &number) == 1)
	      {
		numbers.push_back((T)number);
	      }
	    else
	      {
		// item in string table didn't convert properly (probably not a 
		// number, probably a header or a commented out line)
		LOGW("CSV parser couldn't convert %s for entry in numerical "
		     "table \n", item.c_str() );
	      }
	  }

	if(numbers.size() > 0)
	  { pNumericalTable->push_back(numbers); }
      }
  }
  
  // actions
  bool parse(void);
  
private:
  // data
  std::string mFilename;           /**< Full path to csv file of interest. */
  owl::stringTable_t mStringTable; /**< Parsed CSV as vector of strings. */
  std::ifstream mFileStream;       /**< CSV file stream. */
  bool mDiscardCommentedLines;     /**< denotes if lines in CSV starting with
				    * "#" should be ignored. */

  // internal actions
  bool openFile(void);
  void closeFile(void);
  std::vector<std::string> parseRow(std::string);
  void clearStringTable(void);
};

#endif
