#ifndef __motionDetector_hpp
#define __motionDetector_hpp

#include "ringBuffer.hpp"

#include <sys/time.h>

#include "opencv2/core/core.hpp"

/** This class is used for detecting motion within a given region of the image 
 * source.
 *
 * Motion is detected by diffencing two frames (that represent the same space in
 * the world) at different times and looking for a sufficient amount of change
 * in the average pixel intensity.
 */ 
class cMotionDetector
{
public:
  // constructors
  cMotionDetector(void);
  cMotionDetector(double testPeriod);
  cMotionDetector(double testPeriod, cv::Rect roi);

  void init(void);

  // destructor
  ~cMotionDetector();
  
  // action
  bool update(cv::Mat *pImage);
  bool isTimeToUpdate(void);
  
  // setters
  void setTestPeriod(double testPeriod);
  void setRoi(cv::Rect roi);

  // getters
  cv::Rect getRoi(void);
  bool getState(void);
  
private:
  // data
  cRingBuffer<cv::Mat> mImageBuffer; /**< circular buffer of images to compare.*/
  struct timespec mPrevUpdateTime; /**< time that prev image was stored in ring
				    * buffer. */
  cv::Rect mRoi;      /**< region-of-interest within image to test. */
  bool mRoiIsSet;     /**< denotes is ROI has been set. */
  bool mFullImageRoi; /**< denotes if ROI is set to the full size of the image.*/
  int mMotionVal;     /**< subtract two frames and add all pixel values. */
  float mScore;       /**< metrics of how much motion there is. */
  bool mState;        /**< denotes if motion is currently detected or not. */
  int mMaxPixelVal;   /**< biggest value a pixel can have. */
  int mMaxImageVal;   /**< max ROI value (sum of all pixels fully on). */
  double mTestPeriod; /**< how long to wait between frams that are tested. */
  bool mBufferPrimed; /**< true if detector has a base image to difference off.*/

  // actions
  cv::Mat prepImage(cv::Mat *pImage);
  void calcMotionVal(void);
  void calcDetectorScore(void);
  void updateDetectorState(void);
};

#endif
