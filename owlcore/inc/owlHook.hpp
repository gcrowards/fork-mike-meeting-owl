#ifndef _OWL_HOOK_HPP_
#define _OWL_HOOK_HPP_

#include <vector>

#include "opencv2/videoio/videoio.hpp"

#include "AudioMgr.hpp"
#include "buttonHandler.hpp"
#include "elements/bufferGraph.hpp"
#include "elements/ellipse.hpp"
#include "elements/line.hpp"
#include "elements/panel.hpp"
#include "elements/rect.hpp"
#include "elements/shape.hpp"
#include "gpu.hpp"
#include "ledInterface.hpp"
#include "owl.hpp"
#include "soundLocalizer.hpp"
#include "statusFrame.hpp"
#include "sysfs.hpp"
#include "uiHandler.hpp"
#include "virtualMic.hpp"

// forward declarations
class cSystemTest;

class cOwlHook
{
public:
  cOwlHook();
  cOwlHook(const std::string &testInputDirectory);
  ~cOwlHook();

  cSysfs *mpSysfs;         /**< Provides access to sysfs variables. */

  void *getMockCameraImage(void);

  void setGpu(cGpu *pGpu);
  void updateGpu(void *pEglClientBuffer);
  void updateLocalize(int16_t *pPcmBuf, int bufLen);
  void processAudio(int16_t *pPcmOut, int16_t *pPcmIn,int16_t *pRefIn, int bufLen);
  void updateSpeakerActive(int leftChanLevel, int rightChanLevel);
  void updateMicActive(int leftChanLevel, int rightChanLevel);

  void setRgbaImage(unsigned char *image, int width, int height);

  // public drawing methods with corresponding private render method in cGpu
  void   drawEllipse(int x, int y, int width, int height,
		     float r, float g, float b, float alpha=1.0,
		     float lineWidth=2.0, bool filled=false);
  void   drawEllipseFilled(int x, int y, int width, int height,
			   float r, float g, float b, float alpha=1.0);
  void   drawLine(int x0, int y0, int x1, int y1,
		  float r, float g, float b, float alpha=1.0,
		  float lineWidth=2.0);
  void   drawRectangle(int left, int top, int width, int height,
		       float r, float g, float b, float alpha=1.0,
		       float lineWidth=2.0, bool filled=false);
  void   drawRectangleFilled(int left, int top, int width, int height,
			     float r, float g, float b, float alpha=1.0);
  int    createBufferGraph(const std::vector<sVertex> *pGraphData);
  void   drawBufferGraph(int graphId, sColor color, float lineWidth);
  void resetSoundLocalizer();
  cSoundLocalizer *getSoundLocalizer();
  cAudioDTD *getDTD();
  cAudioMgr *getAM();
  
private:
  cOwl       *mpOwl;       /**< pointer to owl core. */
  cGpu       *mpGpu=NULL;  /**< pointer to cGpu instance. */
  cSystemTest *mpSystemTest; /**< pointer to system test instance. */
  
  cLedInterface mLedInterface;   /**< led interface instance. */
  cButtonHandler mButtonHandler; /**< button handler instance. */
  cUiHandler *mpUiHandler;         /**< hardware ui handler instance. */
  bool mSystemTestMode = false;  /**< whether the system test is active. */

  // TODO: This needs to go in its own class or cSystemTestManager. In the
  //        future, we may want to unify cOwl and cSystemTestManager -- possibly
  //        with a common base class that handles the panorama strip for both
  //        child classes.
  cPanorama mPano; /**< system test pano for rendering the pano strip
		      without cOwl. */
  cSubframe mPanoSubframe; /**< system test panorama subframe. */
  
  cSoundLocalizer mSndLoc; /**< sound localizer instance. */
  cAudioMgr   mAudioMgr;
  audioChannelStatus_t mSpeakerStatus; /**< Speaker channel status. */
  audioChannelStatus_t mMicStatus;     /**< Microphone channel status. */
  float       mFps;               /**< local average frame rate. */
  cVirtualMic mVirtualMic; /**< virtual mic instance. */
  unsigned char *mpCvPano; /**< cv pano RGBA buffer. */
  unsigned char *mpMockCameraImage; /**< mock input RGB buffer. */
  int mRgbaOverlayImageWidth;
  int mRgbaOverlayImageHeight;
  unsigned char *mpRgbaOverlayImage; /**< overlay imge data. */
  std::vector<owl::cPanel> mCompositePanels;
  std::vector<owl::abcShape*>  mOverlayShapes; /**< pointrs in drawing order. */
  std::vector<owl::cBufferGraph*> mOverlayGraphs;/**< buffer graph pointers. */
  std::vector<owl::cLine>    mOverlayLines;    /**< line shapes. */
  std::vector<owl::cRect>    mOverlayRects;    /**< rect shapes. */
  std::vector<owl::cEllipse> mOverlayEllipses; /**< ellipse shapes. */
  std::vector<owl::cBufferGraph> mBufferGraphs;/**< buffer graph shapes. */
  owl::cRgbaOverlayLists     mRgbaOverlays; /**< RGBA overlays. */
  std::string mMockInputDirectory; /**< directory containing test input. */
  std::string mMockVideoFilepath; /**< JPEG or AVI file input source. */
  std::string mMockSoundFilepath; /**< SSA file angle input source. */
  bool mMockInputAvi = false; /**< flag indicating if mock input is video. */
  int mMockFrameNum = 0; /**< the number of mock video frames displayed so far.
			      (index in mTestAngles of the active frame). */
  cOverlayManager *mpOverlayManager; /**< pointer to object used to update
                                          the RGBA overlay on the image. */

  /** holds a sound source angle direction and timestamp for storing
       test input. POD -- can be instantiated with:
      cSourceAngle{frame, angle} */
  struct sSourceAngle
  { int mFrame, mAngle; };

  std::vector<sSourceAngle> mTestAngles; /**< holds test source angles. */
  cv::VideoCapture mVideoReader; /**< used to read raw video from a file. */

  void init(void);
  void clearOverlays(void);
  void *getMockCameraImageAvi(void);
  void *getMockCameraImageJpeg(void);

  void queueOverlay(owl::cEllipse shape);
  void queueOverlay(owl::cLine shape);
  void queueOverlay(owl::cRect shape);

  void capFrameRate(float maxFps);
  void loadLogo(void);
  void loadAoiPin(void);
  void writePreview(unsigned char *pImageBuf, char *pFilename,
		    int width, int height);

  std::string getActiveMockLine(std::ifstream &testFileStream);
  void checkMockDirectory();
  void loadMockSoundFile();
  int getMockSoundAngle();

  void setSystemTestMode(bool enabled);
  void toggleSystemTest();
};


#endif // _OWL_HOOK_HPP_
