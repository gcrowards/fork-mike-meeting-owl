#ifndef __aspectMath_hpp
#define __aspectMath_hpp

#include <vector>

/** This namespace contains math functions related to aspect ratios,
 * which depend on the screen aspect ratio as well as the angular
 * field of view ratio. */
namespace aspectMath
{
  void  adjustLegacyParams(float *pTheta0, float *pTheta1,
			   float *pRInner, float *pROuter, float maxRadius,
			   int w, int h,
			   float hFovWeight, float rOuterWeight,
			   float lensCoeff=1.0, bool ruleOfThirds=true);
  void  computeRadiiFromHFovPhi(float *pRInner, float *pROuter, float maxRadius,
				float hFov, float phi, int w, int h,
				float lensCoeff=1.0, bool ruleOfThirds=true);
  float computeHFovFromRadii(int w, int h, float rInner, float rOuter,
			     float lensCoeff=1.0, bool ruleOfThirds=true);
  float computeHFovFromPhiConstrainOuterRadius(float phi, int w, int h,
					       float rOuter,
					       float lensCoeff=1.0,
					       bool ruleOfThirds=true);
  
  void  getCameraCenter(float *pCx, float *pCy);
  float getMaxRadius(void);

  float hFov(float vFov, float phi, int w, int h, float lensCoeff=1.0);
  float vFov(float hFov, float phi, int w, int h, float lensCoeff=1.0);

  float radiusToPhi(float radius);
  float phiToRadius(float phi);
  
  float phiLower(float phi, float vFov, bool ruleOfThirds=true);
  float phiUpper(float phi, float vFov, bool ruleOfThirds=true);

  
}

#endif  // __aspectMath_hpp
