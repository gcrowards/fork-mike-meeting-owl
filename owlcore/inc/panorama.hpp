#ifndef __panorama_hpp
#define __panorama_hpp

#include <pthread.h>

#include "opencv2/core/core.hpp"

/** This class manages the panoramic strip and access to it.
 *
 */
class cPanorama
{
public:
  // constructors
  cPanorama(void);

  // destructor
  ~cPanorama();

  // action
  float angleAt(float pixel);
  int pixelAt(int angle);
  void update(unsigned char *pCameraCvImage);

  // setters
  void setFocusX(int posX);

  // getters
  int getFocusX();
  void      copyGrayImage(cv::Mat *pDst);
  cv::Mat * getGrayImage(void);

private:
  // data 
  int mFocusX;

  cv::Mat mFrame;     /**< Most recent panorama image data. */
  cv::Mat mGrayImage; /**< Gray-scaled version of the panorama. */

  float mPixOffset; /**< This value describes the rotational alignment 
		       between the camera and the Owl's chassis. */
  float mPixPerDeg; /**< Ratio of panorama horizontal pixels to FOV in deg. */
  float mDegPerPix; /**< Ratio of panorama FOV in deg to horizontal pixels. */
  
  pthread_mutex_t mGrayImageMutex;  /**< Mutex for gray image. */

  // action
};

#endif
