/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  ledInterface.hpp
 *  This is the header file for cLedInterface.
 *   --> High-level interface for controlling the Owl's LEDs.
 */

#ifndef LED_INTERFACE_HPP
#define LED_INTERFACE_HPP

#include "thread.hpp"

/** Flags for different LEDs. */
#define LEDS_NONE      0x00
#define LEDS_MUTE      cLedInterface::mkMuteLedMask
#define LEDS_BLUETOOTH cLedInterface::mkBluetoothLedMask
#define LEDS_EYE       cLedInterface::mkEyeLedMask
#define LEDS_ALL       cLedInterface::mkAllLedMask
typedef int ledType_t; /**< type for LED id bitmasks. */

// forward declarations
class cLedContext;

/** High-level interface for controlling the Owl's LEDs. */
class cLedInterface
{
public:
  static const ledType_t mkMuteLedMask;      /**< owl mute LEDs mask. */
  static const ledType_t mkBluetoothLedMask; /**< owl bluetooth LEDs mask. */
  static const ledType_t mkEyeLedMask;       /**< owl eye LEDs mask. */
  static const ledType_t mkAllLedMask;       /**< mask for all owl LEDs. */
  
  cLedInterface();
  ~cLedInterface();

  void enable(ledType_t leds);
  void disable(ledType_t leds);
  void setLeds(ledType_t leds, bool setEnabled);

  void reapplyStates();
  
  void startBlinking(ledType_t leds, int onTimeMs, int offTimeMs);
  void stopBlinking();
  bool isBlinking() const;

  void setContext(cLedContext *pContext);
  cLedContext* getContext();
  
private:
  int mLedStates = LEDS_NONE; /**< the current state mask of the LEDs. */
  cLedContext *mpActiveContext = nullptr; /**< A pointer to the active LED
					     context if one is set. */
  cThread mBlinkThread; /**< thread for blinking LEDs. */
  ledType_t mBlinkingLeds = LEDS_NONE; /**< LEDs that are busy blinking. */
  int mBlinkOnTimeUs; /**< amount of time LEDs stay on during blink. */
  int mBlinkOffTimeUs; /**< amount of time LEDs stay off during blink. */
  
  void hwEnable(ledType_t leds);
  void hwDisable(ledType_t leds);
  void blinkLoop();
};



#endif // LED_INTERFACE_HPP
