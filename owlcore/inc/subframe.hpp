#ifndef __subframe_hpp
#define __subframe_hpp

#include "opencv2/core/core.hpp"

#include "elements/panel.hpp"
#include "common.hpp"
#include "panorama.hpp"
#include "switchboard/enumString.hpp"

#include <time.h>

// Enumeration of possible base animation modes, this dictates what general
// style should be used (higher level than eAnimationState_t below)
// Defined globally for use in BlackBox save/load.
ENUM_WITH_STRINGS(eAnimationMode_t, int,
       ANIMATION_ENUM_BEGIN,// notes first value in this enumeration
       SLIDING,             // slide in/out while expanding/contracting width
       STRAIGHT_CUT,        // instantly appear in final position
       ANIMATION_ENUM_END); // notes last value in this enumeration


/** A Subframe is a rectangular region that has been cut out from the provided 
 * image source, which in general, is larger than the subframe being created.
 *
 * This class holds an image and all the parameter and state information needed
 * to animate the subframe relative to the Display coordinate system.  At this
 * point, the subframe is able to animate sideways (sliding) and is able to 
 * shrink and grow (via cropping, not scaling), but it should be possible to 
 * extend it to allow for arbitrary motions and scaling without too much trouble.
 *
 * Subframes are intended to be used as the unit by which the output video frame
 * is built.  In other words, the output frame is a patchwork comprised of a 
 * number of subframes.  Because subframes can animate themselves, it's 
 * relatively easy to generate an output frame that is animated, when needed.
 */
class cSubframe
{
public:
  // constructors
  cSubframe(bool isPanorama = false);
  cSubframe(cv::Point focus, cv::Size size, bool isPanorama = false);

  // destructor
  ~cSubframe();

  // Enumeration of possible states for display on the Stage  (this allows
  // different animation types to be used during different parts of the life
  // cycle)
  typedef enum eDisplayState_t
  {
    ADDING,              // being added to the Stage
    REMOVING,            // being removed from the Stage
    ADJUSTING,           // re-fitting w/ other subframes
    PANNING,             // moving to new focus point
    STAGE_SWAP,          // replacing whole stage w/ one new subframe
    STATIC               // not doing anything
  } eDisplayState;

  // shared actions
  static void nextAnimationMode(void);

  // actions
  void init(void);
  cv::Rect animate(void);
  void enableAnimation(void);
  void disableAnimation(void);
  bool isAnimating(void);
  void terminate(void);
  bool isTerminated(void);
  void adjustFocus(cv::Point focus);
  void adjustFocusFast(cv::Point focus);
  void adjustFocusSlow(cv::Point focus);
  void updateLastGoodFocus(void);
  void revertFocus(void);

  bool isTooSmall(void);
  bool isMinReqSizeChanged(void);

  // setters
  void setDisplayState(eDisplayState_t displayState);
  void setDisplayStateNow(eDisplayState_t displayState);
  void setLocked(bool state = true);
  void setPanorama(cPanorama *pPano);
  void setPos(cv::Point pos);
  void setEndPos(cv::Point pos);
  void setSize(cv::Size size);
  void setEndSize(cv::Size size);
  void setFocus(cv::Point focus);
  void setMinReqSize(cv::Size minReqSize);
  void setAnimationTime(float time);
  void setInLayout(bool inLayout);
  static void setAnimationMode(eAnimationMode_t animationMode);

  // getters
  eDisplayState_t getDisplayState(void);
  bool getLocked(void);
  cv::Rect getRoi(void);
  cv::Point getPos(void);
  cv::Point getEndPos(void);
  cv::Size getSize(void);
  cv::Size getEndSize(void);
  cv::Point getFocus(void);
  int getFocusXWrtPano(void);

  owl::cPanel getPanel(void);
  cv::Size getMinReqSize(void);
  cv::Point getPosInLayout(void);
  static eAnimationMode_t getAnimationMode(void);
  bool isInLayout(void);

private:  
  // shared data
  static eAnimationMode_t sAnimationMode; /**< specifies base style for 
					      * animating (e.g. sliding, 
					      * straight cuts, fades). */

  // data
  bool mIsPanorama;       /**< to distinguish panoramic strip from others. */
  bool mAnimationEnabled; /**< Subframe is allowed to animate. */
  eDisplayState_t mDisplayState; /* What the subframe is doing on the display, 
				  * e.g. coming, going, adjusting, etc. */
  bool mTerminated;       /**< Subframe is marked for termination (but may not be
			   * ready to be destroyed yet, because it may still be
			   * animating off the screen, for example. */
  bool mLocked;           /**< Subframe may not be resized by cLayout. */

  cv::Point2f mPos; /**< position of the _center_ of subframe (in cLayout's
		     * reference frame). */
  cv::Size2f mSize; /**< actual height/width of subframe. */
  cv::Size mMinReqSize;  /**< The subframe needs at least this many pixels in
			  * order to display properly; if there are not enough
			  * pixels available in the layout, the value of this
			  * variable can be used to determine if this or another
			  * subframe should removed. */
  bool mNewMinReqSize;   /**< notes that the min-req-size has changed. */

  cv::Point2f mFocus; /**< focus marks where the center of the Subframe should 
		       * be (in cVirtualCamera's unrolled image ref. frame). */

  cv::Point mEndFocus; /**< target focus (for animation). */
  cv::Point mEndPos; /**< target final position (for animation). */
  cv::Size mEndSize; /**< target final size (for animation). */

  cv::Point2f mVelPos;   /**< velocity for positional animation. */
  cv::Point2f mVelFocus; /**< velocity for focus change. */
  cv::Point2f mVelSize;  /**< velocity for size animation. */

  owl::cPanel mPanel; /**< the panel contains all the information needed to draw
		       * the Subframe in the composite image (the dimensions to 
		       * use pull from the image source, and the dimension to 
		       * use  to paste into the output composite. */
  cv::Point mLastGoodFocus; /**< Last focus point that generated an acceptable 
			     * Stage Panel. */

  bool mMovingRight;  /**< flag indicating if slide animation is to the right. */
  bool mMovingLeft;   /**< flag indication if slide animation is to the left. */
  bool mGrowing;      /**< flag indicating if scale animation is growing. */
  bool mShrinking;    /**< flag indicating if scale animation is shrinking. */
  bool mFocusingRight;/**< flag indicating if focus is shifting right. */
  bool mFocusingLeft; /**< flag indicating if focus is shifting left. */
  
  cPanorama *mpPano; /**< pointer to the virtual camera that contains the
			       image from which to cut subframe. */

  cv::Rect mRoi; /**< This region-of-interest defines the subframe's 
		  * boundaries (in cVirtualCamera's unrolled image 
		  reference frame). */
  
  float mPosAnimationTime; /**< time period (s) over which subframe should go 
			    * from its start position to its end position. */
  float mFocusAnimationTime; /**< time period (s) over which subframe should go 
			      * from its start focus to its end focus. */
  float mSizeAnimationTime; /**< time period (s) over which subframe should go 
			     * from its start size to its end size. */

  struct timespec mPrevAnimationTime; /**< time at which previous subframe 
				       * animation update happened. */
  
  bool mNewAnimation; /**< true on first cycle of a new animation sequence. */
  bool mInLayout; /**< Flag indicating if this subframe is part of a current
		   * layout. */
  float mMaxRadius; /**< Maximum radius, in [0, 0.5], that accounts
		       for camera center. */

  // actions
  cv::Mat* getInputImage(void);
  void setPanel(void);
  void adjustFocusCore(cv::Point focus);
  void animateStraightCut(float focusDist);
  void animateSliding(float moveDist, float growDist, float focusDist);
};

#endif
