#ifndef __attentionSystem_hpp
#define __attentionSystem_hpp

#include "areaOfInterest.hpp"
#include "elements/patch.hpp"
#include "faceMapper.hpp"
#include "motionMapper.hpp"
#include "panorama.hpp"
#include "personMapper.hpp"
#include "switchboard/pinnedAoiArgs.hpp"
#include "voice.hpp"

#include <time.h>
#include <vector>

/** This class is used to track which areas in the world the Owl should be
 *  paying attention to.
 *
 * Currently people who are talking, or who have been talking recently, are what
 * the system wants to pay attention to.  In the future, we expect to also want
 * to pay attention to things like whiteboards (that are in use), Tv displays
 * (that are in use), objects (that are the focus of the discussion), etc. 
 *
 * The attention system generates Area-Of-Interest objects, which contain 
 * Subframs that capture regions of the raw camera input that are a focus of
 * attention.  These Area-of-Interest objects are what the video output Layout
 * are comprised of.
 */
class cAttentionSystem
{
public:
  // constructors
  cAttentionSystem(void);
  cAttentionSystem(cPanorama *pPano);

  // destructor
  ~cAttentionSystem();

  // actions
  std::vector<cAreaOfInterest *> update(std::vector<cVoice> voices,
                                        cPinnedAoiArgs *pPinnedAois);

  void reset(void);
  void clearAois(void);

  // setters
  void setPanorama(cPanorama *pPano);
  void setFaceMapper(cFaceMapper *pFaceMapper);
  void setMotionMapper(cMotionMapper *pMotionMapper);
  void setPersonMapper(cPersonMapper *pPersonMapper);

private:
  // data
  cPanorama *mpPano; /**< The input data source. */
  cFaceMapper *mpFaceMapper; /**< This provides information on where faces are
				 located, which feeds into some decisions. */
  cMotionMapper *mpMotionMapper; /**< This provides information on where motion
				  * inicates that there may be a person. */
  cPersonMapper *mpPersonMapper; /**< This provides information on where 
				    people are believed to be located. */
  std::vector<cAreaOfInterest *> mpAois; /**< A vector of all the Areas-Of-
					    Interest that are being paid 
					    attention to. */
  bool mStageStartTimerSet;  /**< Indicates that the timer has been started 
				that determines end of the stage-fill startup
				period. */
  struct timespec mStageStartTimeSpec; /**< Smart Stage fill timer. */

  // actions
  void init(void);
  void updateAttentionSystem(std::vector<cVoice> voices,
                             cPinnedAoiArgs *pPinnedAois);
  void updateAois(void);
  bool integrateInterest(owl::cPatch *pNewPatch, cVoice voice,
                         bool pinned = false, 
			 cAreaOfInterest::ePriority_t 
			 priority = cAreaOfInterest::MUST_SHOW );
  void manageAois(void);
  cAreaOfInterest * createAoi(cVoice voice, cPanorama *pPano, owl::cPatch patch,
                              cAreaOfInterest::ePriority_t 
			      priority = cAreaOfInterest::MUST_SHOW, 
			      bool pinned = false );
  void deleteAoi(cAreaOfInterest *pAoi);
  bool isAoiStale(cAreaOfInterest *pAoi);
  void updatePinnedAois(cPinnedAoiArgs *pPinnedAois);
  void updateCameraLock(cPinnedAoiArgs *pPinnedAois, std::vector<cVoice> voices );
  int  findFirstFreeAoiId(void);
  std::vector<const cItem *> sortAndCullPeopleMap(void);
  bool onlyPinnedAoisExist(void);
  bool anyPinnedAoisExist(void);
};

#endif
