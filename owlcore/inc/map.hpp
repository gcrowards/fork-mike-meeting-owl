#ifndef __map_hpp
#define __map_hpp

#include "item.hpp"

#include <vector>
#include <limits.h>

/** This class provides a representation of where people (and eventually 
 * objects) are located within the room.
 *
 * Currently, the map is polar in nature (units == integral degrees) but does
 * not have a radius component.  Each angular coordinate contains a score that 
 * indicates the strength of the belief that a person is present at that 
 * bearing.
 */ 
class cMap
{
public:
  // constructors
  cMap(void);
  cMap(int addVal, int leakVal);
  cMap(int addVal, int leakVal, int minVal, int maxVal);  

  cMap(const cMap &src);
  cMap & operator=(const cMap &src);

  void init(int addVal, int leakVal, int minVal, int maxVal, int spreadVal);

  // destructor
  ~cMap();

  // action
  void update(std::vector<int> bearings);
  void update(std::vector<cItem> fauxItems);
  void updateItemMap(void);
  void resetMap(void);
  void printMap(int stepSize = 20);
  void logMap(const char *pLabel, int stepSize = 20);

  // setters
  void setAddVal(int addVal);
  void setLeakVal(int leakVal);
  void setMinVal(int minVal);
  void setMaxVal(int maxVal);
  void setSpreadVal(int spreadVal);
  void setValAt(int angle, int val);

  // getters
  int getValAt(int angle);
  int getAngleWithMaxVal(int nominalAngle, int plusMinus);
  std::vector<int> getMapData(void);
  int getMaxVal(void);
  const std::vector<const cItem *> getInstantItemMap(void);
  const std::vector<const cItem *> getLongTermItemMap(void);
  cItem * getInstantItemAt(int angle);
  cItem * getLongTermItemAt(int angle);
  
private:
  // data
  std::vector<int> mMap;           /**< low-level map where each entry represents
				      a bearing (deg), and the value a score
				      strength. */
  std::vector<cItem *> mpInstantItemsMap; /**< interpretation of mMap data that 
					     groups distinct local maxima into 
					     cItems. */
  std::vector<cItem *> mpLongTermItemsMap;  /**< filtering of mpItemsMap to a
					       map that favors long-term 
					       stability over short-term
					       responsiveness. */
  int mAddVal;    /**< amount to add to detected map location when updated. */
  int mLeakVal;   /**< amount to reduce each map entry on each update call. */
  int mMinVal;    /**< smallest value a map location can contain. */
  int mMaxVal;    /**< largest value a map location can contain. */
  int mSpreadVal; /**< spread (essentially standard deviation in
		     degrees) that determines how much nearby
		     bearings are incremented. */

  // actions
  void deepCopySelf(const cMap &src);
  void deepCopyItemMap(std::vector<cItem *> *pDst, 
		       const std::vector<cItem *> src );
  void initMap(void);
  void clearInstantItemMap(void);
  void clearLongTermItemMap(void);
  void updateLongTermItemMap(void);
  void updateItemMap(std::vector<cItem> fauxItems);
  cItem * getItemAt(std::vector<cItem *> itemMap, int angle, 
		    int threshold = INT_MIN );
};

#endif
