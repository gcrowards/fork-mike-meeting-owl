#ifndef __item_hpp
#define __item_hpp

#include <string>

/** This class stores data about an item (e.g. location in space). */
class cItem
{
public:
  // constructors
  cItem(void);
  cItem(float thetaMean, float thetaStdDev, bool assignID = false);
  cItem(float thetaMean, float thetaStdDev, float phiMean, float phiStdDev,
	bool assignId = false );

  void init(float thetaMean, float thetaStdDev, float phiMean, float phiStdDev,
	    bool assignId );

  // destructor
  ~cItem();

  // actions
  void addToScore(int adder);
  void filterInThetaMean(float newThetaMean);
  void filterInThetaStdDev(float newThetaStdDev);
  void filterInPhiMean(float newPhiMean);
  void filterInPhiStdDev(float newPhiStdDev);
  void print(std::string comment = std::string(""));
  
  // setters
  void setId(void);
  void setScore(int score);
  void setThetaMean(float thetaMean);
  void setPhiMean(float phiMean);
  void setThetaStdDev(float thetaStdDev);
  void setPhiStdDev(float phiStdDev);
  
  // getters
  int getId(void) const;
  int getScore(void) const;
  int getThetaMean(void) const;
  int getPhiMean(void) const;
  float getThetaStdDev(void) const;
  float getPhiStdDev(void) const;
  float getThetaGaussianAt(int angle) const;
  float getPhiGaussianAt(int angle) const;
  
private:
  // data
  static int mIdNewest; /**< Last taken Id by any cItem. */
  int mId;              /**< Unique ID for this item. */

  float mThetaMean; /**< The horizontal bearing to the item (0 - 360 deg.). */
  float mPhiMean;   /**< The elevation bearing to the item (0 - 90 deg). */
  float mThetaStdDev;   /**< Theta direction certainty. */
  float mPhiStdDev;     /**< Phi direction certainty. */

  float mAlphaMean;     /**< Low-pass filter constant for item's mean. */
  float mAlphaStdDev;   /**< Low-pass filter constant for item's stand. dev. */

  int mScore;     /**< Rating of how confident we are in this items qualities
		     (including it's basic existance); 0 indicates no data. */

  // actions
  float getGaussianVal(int angle, float mean, float stdDev) const;
};

#endif
