/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  volumeControl.hpp
 *  This is the header file for cVolumeControl.
 *   --> Controls volume adjustment for the volume buttons. When a
 *        VOLUME UP/VOLUME DOWN button is held, the volume will be adjusted
 *        continuously. When clicked quickly, the volume will be adjusted only
 *        one step.
 */

#ifndef VOLUME_CONTROL_HPP
#define VOLUME_CONTROL_HPP

#include "AudioPlayer.hpp"
#include "thread.hpp"
#include <condition_variable>
#include <string>
#include <mutex>

// forward declarations
class cSysfs;
class cBlackBox;

/** Controls volume adjustment for the volume buttons. When a VOLUME UP/VOLUME
 *   DOWN button is held, the volume will be adjusted continuously. When clicked
 *   quickly, the volume will be adjusted only one step. */
class cVolumeControl
{
public:
  static const double kHoldDelay; /**< delay before starting to adjust volume
				     when holding button (seconds). */
  static const double kHoldSpacing; /**< delay between blips when adjusting
				       volume. */
  static const int kClickStep;  /**< percent volume change when
				   button is clicked. */
  static const int kHoldStep;  /**< percent volume change per
				  interval while holding button. */
  static const std::string mkBlipWavPath; /**< path of the 'blip' wav file to
					     play when the volume changes. */
  
  cVolumeControl(cSysfs *pSysfs, cBlackBox *pBlackBox);
  ~cVolumeControl();

  void buttonPress(int direction);
  void buttonHold();
  void buttonRelease(int direction);
  
  void setVolume(int volume);
  int  getVolume(void);

private:
  cSysfs *mpSysfs = nullptr; /**< pointer to cSysfs from cOwlHook. */
  cBlackBox *mpBlackBox = nullptr; /**< pointer to analytics black box. */

  // volume adjustment
  cThread mVolumeThread; /**< thread to adjust the volume continuously while the
			    button is pressed. */
  std::mutex mVolumeLock; /**< locks mVolumeDir for multithreaded access. */
  std::condition_variable mVolumeCv; /**< lets the volume thread wait for button
					input. */
  cAudioPlayer mBlipPlayer; /**< audio player for playing 'blip' on the speaker. */
  
  bool mVolumeAdjusting = false;
  int mVolumeDir = 0; /**< direction to adjust volume.
			 should be +1, -1, or 0 (no adjustment). */
  
  void adjustVolume(int delta);
  void volumeAdjustCallback();
};

#endif // VOLUME_CONTROL_HPP
