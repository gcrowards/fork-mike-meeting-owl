#ifndef __soundMapper_hpp
#define __soundMapper_hpp

#include "map.hpp"
#include "virtualMic.hpp"

#include "opencv2/core/core.hpp"


/** Structure for conveying audio channel (speaker or microphone) data. */
typedef struct
{
  bool   mActive;     /**< Channel carrying data recently? */
  int    mLevelLeft;  /**< Most recent average audio level on left channel. */
  int    mLevelRight; /**< Most recent average audio level on right channel. */
} audioChannelStatus_t;


/** This class is responsible for mapping out the locations of people in the
 * room based on recent sounds.
 *
 * There is no trigger mechanism, the system just takes in sound source data
 * from the audio system and puts it on a map.
 */ 
class cSoundMapper
{
public:
  // constructors
  cSoundMapper(cVirtualMic *pMic);
  
  // destructor
  ~cSoundMapper();

  // action
  void update(void);
  void resetMap(void);

  // setters

  // getters
  int getMapValAt(int angle);
  int getAngleWithMaxVal(int nominalAngle, int plusMinus);
  cMap getMap(void);
  
private:
  // data
  cMap mMap; /**< The map keeps track of the positional data of the things 
		found by this mapper. */

  cVirtualMic *mpMic; /**< Source of sound data. */
  
  // actions
};

#endif
