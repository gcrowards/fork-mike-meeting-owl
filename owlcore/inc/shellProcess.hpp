/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  shellProcess.hpp
 *  This is the header file for cShellProcess.
 *   --> Opens a system shell process that executes the given command.
 *       A callback can be specified, which will be called with the output when
 *        the process terminates.
 */

#ifndef SHELL_PROCESS_HPP
#define SHELL_PROCESS_HPP

#include <functional>
#include <string>

/** A callback type for receiving lines of text output. */
typedef std::function<bool(const std::string *pLine)> shellCallback_t;

/** Opens a system shell process that executes the given command.
 *  A callback can be specified, which will be called with the output when the
 *   process terminates. */
class cShellProcess
{
public:
  cShellProcess(const std::string &command,
                shellCallback_t outCallback = nullptr);
  cShellProcess(const std::string &command,
                shellCallback_t outCallback,
		bool oneTimeCommand );
  ~cShellProcess();

  // control
  void killProcess();
  void waitForProcess();
  void persist();

  // accessors
  bool isActive();
  int getExitStatus();
  
private:
  pid_t mPid = -1;   /**< the process's pid. */
  std::string mCommand = ""; /**< the command to be run. */
  shellCallback_t mOutputCallback = nullptr; /**< the callback to pass output. */
  bool mActive = true; /**< whether the process is open and running. */
  bool mPersist = false; /**< whether the process shell should persist after this
			    cShellProcess instance goes out of scope. */
  int  mExitStatus;    /**< Exit status of finished process. */

  pid_t forkProcess(const std::string &cmd);
  int   runOneTimeCommand(const std::string &command);
};


#endif // SHELL_PROCESS_HPP
