#ifndef AUDIOLEVELMANAGER_H_INCLUDED
#define AUDIOLEVELMANAGER_H_INCLUDED

#define ALM_ENABLE_COMPRESSION        1
#define ALM_ENABLE_LIMIT              2
#define ALM_ALTERNATE_MEASURE_POINT   4
#define ALM_ENABLE_ALT_LIMIT          8


//#define RMS_BUFF_SIZE 160
#define RMS_BUFF_SIZE 1024
//#define RMS_BUFF_SIZE 4096
#define MAX_PREDELAY  1600
#define CURR_PREDELAY 960
#define ALT_PREDELAY 32
#define PEAK_HOLD_COUNT 640



/// structure to set of levels
typedef struct
{
  FLOAT rmsLevel;
  FLOAT peakLevel;
} LEVELS;

//
// Structure that holds the state for the level calculations
//
typedef struct
{
  FLOAT rmsBuffer[RMS_BUFF_SIZE];
  int buffIndex;
  FLOAT rmsSum;
  FLOAT maxSum;
  FLOAT last;
  FLOAT lastPeak;
  FLOAT peakDecay;
  FLOAT lastgain;
  int peakHoldCount;
  FLOAT rmsLevelBuffer[MAX_PROC_SIZE];
  FLOAT peakLevelBuffer[MAX_PROC_SIZE];
} LEVEL_STATE;

typedef struct
{
  int Fs;
  int enables;
  int mLastState;
  FLOAT compThreshold;
  FLOAT compRatio;
  FLOAT compAttack;
  FLOAT compDecay;
  FLOAT limitThreshold;
  FLOAT limitAttack;
  FLOAT limitDecay;
  FLOAT limitRatio;

  FLOAT makeup;
  FLOAT duck;
  FLOAT maxAtten;
  int levelType;
  int predelay;
  FLOAT compAtCoeff;
  FLOAT compDecCoeff;
  FLOAT oneOverRatio;
  FLOAT limitAtCoeff;
  FLOAT limitDecCoeff;
  FLOAT limitOneOverRatio;
  FLOAT debugVal;
  FLOAT retToLinCoeff;
  FLOAT spkrActiveCoeff;
  FLOAT lastGain;
  int lastState;
  int readIndex;
  int writeIndex;
  FLOAT predelay_buffer[MAX_PREDELAY];
  FLOAT gainDebugBuff[MAX_PROC_SIZE];
  LEVELS lastLevels;
  LEVEL_STATE levelState;
  unsigned long long delayTime;
  unsigned long long delayTimeStamp;
  FLOAT lastCompressGain;
} ALM_STATE;

typedef struct
{
  int enables;
  FLOAT compThreshold;
  FLOAT compRatio;
  FLOAT compAttack;
  FLOAT compDecay;
  FLOAT limitThreshold;
  FLOAT limitAttack;
  FLOAT limitDecay;
  FLOAT limitRatio;

  FLOAT makeup;
  FLOAT duck;
  FLOAT maxAtten;
  int delayTime;
  int predelay;
} ALM_CONFIG;

extern int ALMInit(ALM_STATE *);
extern int ALMConfigure(ALM_STATE *, ALM_CONFIG *);
extern int ALMProcess(ALM_STATE *pLm, short *pDataIn, short *pDataOut,
                      int frames, int chan, int totalChans, float applyGain);
extern int ALMMeasureLevels(ALM_STATE *pAlmState, short *pAudioIn,
                     int frameCount, int procChan, int totalChans);
extern int ALMCheckSpeakerActive(ALM_STATE *pAlmState);


#endif
