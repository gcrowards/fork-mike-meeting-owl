/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  buttonHandler.hpp
 *  This is the header file for cButtonHandler.
 *   --> High-level button interface. Callbacks can be bound to button events.
 */

#ifndef BUTTON_HANDLER_HPP
#define BUTTON_HANDLER_HPP

#include "thread.hpp"

#include <functional>
#include <vector>

// button type id masks
#define BUTTON_NONE      0x00
#define BUTTON_MUTE      cButtonHandler::mkMuteButtonMask
#define BUTTON_VOLUP     cButtonHandler::mkVolUpButtonMask
#define BUTTON_VOLDN     cButtonHandler::mkVolDnButtonMask
#define BUTTON_OWL       cButtonHandler::mkOwlButtonMask
#define BUTTONS_ALL      cButtonHandler::mkAllButtonMask
typedef int buttonType_t; /**< type for button id bitmasks. */

// button actions
enum buttonAction_e
  {
    eButtonPress = 0, /**< button pressed down. If multiple buttons, they must be
			 pressed at the same time. */
    eButtonRelease,   /**< button released. If multiple buttons, they must be
			 released at the same time. */
    eButtonClick,     /**< button whole click -- pressed, then released.
			 If multiple buttons, they must be clicked at the same
			 time. */
    eButtonHold       /**< button(s) held down together. If multiple buttons,
			 they don't have to be pressed at the same time. Just
			 held down together with no other buttons. Useful for
			 hold/press button combinations (see system test
			 combination set in main). */
  };

/** Callback used to notify of a button press. */
typedef std::function<void()> buttonCallback_t;



// forward declarations
class cButtonContext;
struct buttonStatus;

/** A button event, defined by one or more button ids (bitmask) and an
 *   action. */
struct sButtonEvent
{
  sButtonEvent(buttonType_t btnType, buttonAction_e btnAction,
	       double eventDelay = 0.0 )
    : type(btnType), action(btnAction), delay(eventDelay)
  { }
  
  buttonType_t type; /**< the buttons the event pertains to. */
  buttonAction_e action; /**< the action of the buttons. */
  double delay; /**< the amount of time the action has to happen to fulfill the
		   event. */
};

/** High-level button interface. Callbacks can be bound to individual or
 *   combinations of button events. */
class cButtonHandler
{
public:
  static const buttonType_t mkMuteButtonMask; /**< mute button mask. */
  static const buttonType_t mkVolUpButtonMask; /**< volume up button mask. */
  static const buttonType_t mkVolDnButtonMask; /**< volume down button mask. */
  static const buttonType_t mkOwlButtonMask; /**< owl button mask. */
  static const buttonType_t mkAllButtonMask; /**< mask for all buttons. */
  
  static const int mkDelayThreadSleepUs; /**< amount of time to sleep in the delay
					   thread (us). */
  
  cButtonHandler();
  ~cButtonHandler();
  
  void start();
  void stop();
  
  bool bind(sButtonEvent btnEvent, buttonCallback_t callback);
  bool bind(std::vector<sButtonEvent> comboEvents, buttonCallback_t callback);
  void unbindAll();

  void setContext(const cButtonContext *pContext);
  void handleButtons();
  
private:
  /** Private subclass for handling button combos. */
  class cButtonCombo
  {
  public:
    cButtonCombo(std::vector<sButtonEvent> events, buttonCallback_t callback);
    
    void updateState(buttonType_t state, buttonType_t pressed, buttonType_t released);
    void updateTime(double timeElapsed);
    
  private:
    std::vector<sButtonEvent> mEventSequence; /** the sequence of button events
						  needed to trigger. */
    std::vector<buttonType_t> mStateSequence; /** the sequence of button states
						  needed to trigger. */
    int mSequenceStep = 0; /**< the next index of the sequence required. */
    double mDelayTimer = 0.0; /**< keeps track of time passed for delays. */
    buttonCallback_t mCallback; /**< the callback triggered by successfully
				 completing the combination. */

    void nextStep();
    void reset(buttonType_t state, buttonType_t pressed, buttonType_t released);
    bool nextStepValid(buttonType_t state, buttonType_t pressed,
		       buttonType_t released ) const;
  };
  
  buttonType_t mCurrentState = BUTTON_NONE; /**< the current state mask of the
					       buttons. */

  // TODO: Allow individual combination callback reassignment at run time.
  std::vector<cButtonCombo> mBoundEvents; /**< a list of bound button combos
					      with their respective callbacks. */
  cThread mDelayThread; /**< thread for keeping track of how long buttons are
			   pressed. */
  
  void handleEvent(buttonType_t state, buttonType_t pressed,
		   buttonType_t released );
  void waitForEvent(buttonStatus *pStatus);
  void delayUpdate();
};


#endif // BUTTON_HANDLER_HPP
