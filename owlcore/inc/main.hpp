#ifndef _MAIN_HPP_
#define _MAIN_HPP_

int  main(int argc, char **argv);
int  init(void);
int  finish(void);
bool quitKey(void);
void simulatePcmData(int16_t *pPcmIn, unsigned int bufLen);

#endif /* _MAIN_HPP_ */
