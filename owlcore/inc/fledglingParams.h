// Camera center parameters for various Meeting Owl prototypes (and other
// such development platforms)


// NOTE: Fledgling Owls no longer supported

//-- P0.5 Owls ------------------------------------------------------->

// P0.5 - Sooty
#define P05_SOOTY_CAMERA_WIDTH              1728
#define P05_SOOTY_CAMERA_HEIGHT             1728
#define P05_SOOTY_CAMERA_CENTER_X           923
#define P05_SOOTY_CAMERA_CENTER_Y           859
#define P05_SOOTY_CAMERA_ALIGNMENT_ANGLE    205

// P0.5 - Crested
#define P05_CRESTED_CAMERA_WIDTH            1728
#define P05_CRESTED_CAMERA_HEIGHT           1728
#define P05_CRESTED_CAMERA_CENTER_X         892
#define P05_CRESTED_CAMERA_CENTER_Y         855
#define P05_CRESTED_CAMERA_ALIGNMENT_ANGLE  205

// P0.5 - Chaco
#define P05_CHACO_CAMERA_WIDTH              1728
#define P05_CHACO_CAMERA_HEIGHT             1728
#define P05_CHACO_CAMERA_CENTER_X           889
#define P05_CHACO_CAMERA_CENTER_Y           853
#define P05_CHACO_CAMERA_ALIGNMENT_ANGLE    205

// P0.5 - Barn
#define P05_BARN_CAMERA_WIDTH               1728
#define P05_BARN_CAMERA_HEIGHT              1728
#define P05_BARN_CAMERA_CENTER_X            928
#define P05_BARN_CAMERA_CENTER_Y            931
#define P05_BARN_CAMERA_ALIGNMENT_ANGLE     205

//-- P1 Owls --------------------------------------------------------->

// P1   - MTR
#define P1_MTR_CAMERA_WIDTH                 1728
#define P1_MTR_CAMERA_HEIGHT                1728
#define P1_MTR_CAMERA_CENTER_X               864
#define P1_MTR_CAMERA_CENTER_Y               864
#define P1_MTR_CAMERA_ALIGNMENT_ANGLE        147

// P1   - Elf
#define P1_ELF_CAMERA_WIDTH                 1728
#define P1_ELF_CAMERA_HEIGHT                1728
#define P1_ELF_CAMERA_CENTER_X               838
#define P1_ELF_CAMERA_CENTER_Y               840
#define P1_ELF_CAMERA_ALIGNMENT_ANGLE        147

// P1   - Fulvous
#define P1_FULVOUS_CAMERA_WIDTH             1728
#define P1_FULVOUS_CAMERA_HEIGHT            1728
#define P1_FULVOUS_CAMERA_CENTER_X           850    
#define P1_FULVOUS_CAMERA_CENTER_Y           850    
#define P1_FULVOUS_CAMERA_ALIGNMENT_ANGLE    147

// P1   - Ashy
#define P1_ASHY_CAMERA_WIDTH                1728
#define P1_ASHY_CAMERA_HEIGHT               1728
#define P1_ASHY_CAMERA_CENTER_X              822
#define P1_ASHY_CAMERA_CENTER_Y              833
#define P1_ASHY_CAMERA_ALIGNMENT_ANGLE       147

//-- Mock Cameras --------------------------------------------------->

#define MOCK_3456_CAMERA_WIDTH             3456
#define MOCK_3456_CAMERA_HEIGHT            3456
#define MOCK_3456_CAMERA_CENTER_X          1728
#define MOCK_3456_CAMERA_CENTER_Y          1728
#define MOCK_3456_CAMERA_ALIGNMENT_ANGLE    147

#define MOCK_1728_CAMERA_WIDTH             1728
#define MOCK_1728_CAMERA_HEIGHT            1728
#define MOCK_1728_CAMERA_CENTER_X           864
#define MOCK_1728_CAMERA_CENTER_Y           864
#define MOCK_1728_CAMERA_ALIGNMENT_ANGLE      0

//-------------------------------------------------------------------|

// Setting defines which camera params will be used
#if defined(__ANDROID__) && !defined(MOCK_INPUT) && !defined(INPUT_3456)

//   Android with real camera input & UVC output
#ifdef PROTOTYPE_P0_5
#define FLEDGLING_CAMERA_WIDTH            P05_CRESTED_CAMERA_WIDTH
#define FLEDGLING_CAMERA_HEIGHT           P05_CRESTED_CAMERA_HEIGHT
#define FLEDGLING_CAMERA_CENTER_X         P05_CRESTED_CAMERA_CENTER_X
#define FLEDGLING_CAMERA_CENTER_Y         P05_CRESTED_CAMERA_CENTER_Y
#define FLEDGLING_CAMERA_ALIGNMENT_ANGLE  P05_CRESTED_CAMERA_ALIGNMENT_ANGLE
#else // P1 PROTOTYPE
#define FLEDGLING_CAMERA_WIDTH            P1_FULVOUS_CAMERA_WIDTH
#define FLEDGLING_CAMERA_HEIGHT           P1_FULVOUS_CAMERA_HEIGHT
#define FLEDGLING_CAMERA_CENTER_X         P1_FULVOUS_CAMERA_CENTER_X
#define FLEDGLING_CAMERA_CENTER_Y         P1_FULVOUS_CAMERA_CENTER_Y
#define FLEDGLING_CAMERA_ALIGNMENT_ANGLE  P1_FULVOUS_CAMERA_ALIGNMENT_ANGLE
#endif // PROTOTYPE P1 or P0.5

#elif defined(INPUT_3456)

// linux or MOCK_INPUT with full resolution image
#define FLEDGLING_CAMERA_WIDTH            MOCK_3456_CAMERA_WIDTH
#define FLEDGLING_CAMERA_HEIGHT           MOCK_3456_CAMERA_HEIGHT
#define FLEDGLING_CAMERA_CENTER_X         MOCK_3456_CAMERA_CENTER_X
#define FLEDGLING_CAMERA_CENTER_Y         MOCK_3456_CAMERA_CENTER_Y
#define FLEDGLING_CAMERA_ALIGNMENT_ANGLE  MOCK_3456_CAMERA_ALIGNMENT_ANGLE

#else

// linux or MOCK_INPUT with normal resolution image
#define FLEDGLING_CAMERA_WIDTH            MOCK_1728_CAMERA_WIDTH
#define FLEDGLING_CAMERA_HEIGHT           MOCK_1728_CAMERA_HEIGHT
#define FLEDGLING_CAMERA_CENTER_X         MOCK_1728_CAMERA_CENTER_X
#define FLEDGLING_CAMERA_CENTER_Y         MOCK_1728_CAMERA_CENTER_Y
#define FLEDGLING_CAMERA_ALIGNMENT_ANGLE  MOCK_1728_CAMERA_ALIGNMENT_ANGLE

#endif

// ----- params common to all hardware -----
// NOTE: at some point, each prototype may need its own value

// params to control warping of GPU-unrolled image; a value of 0.0
// turns off the particular type of warping
#define FLEDGLING_CAMERA_WARP_STRETCH  0.00
#define FLEDGLING_CAMERA_WARP_BEND     0.00

// param that corresponds to smallest possible elevation angle
// supported by the lens; set to a negative value if the lens can see
// below the plane of the camera
#define FLEDGLING_LENS_MIN_PHI_DEG     0.00

// param that reduces the maximum outer radius a small amount, to
// account for the unusable pixels near the edge of the fisheye image;
// set this value as a fraction of the image height
#define FLEDGLING_OUTER_RADIUS_MARGIN  0.020
