/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  buttonContext.hpp
 *  This is the header file for cButtonContext.
 *   --> Creates an isolated set of button event bindings to change button
 *        functionality for different Owl modes
 */

#ifndef BUTTON_CONTEXT_HPP
#define BUTTON_CONTEXT_HPP

#include "buttonHandler.hpp"

#include <vector>

/** Creates an isolated set of button event bindings to change button
 *   functionality for different Owl modes. */
class cButtonContext
{
public:
  struct sButtonBinding
  {
    sButtonBinding(const std::vector<sButtonEvent> &aButtonEvents,
		   buttonCallback_t buttonCallback )
      : aEvents(aButtonEvents), callback(buttonCallback)
    { }

    std::vector<sButtonEvent> aEvents;
    buttonCallback_t callback;
  };
  
  void addBinding(sButtonEvent button, buttonCallback_t callback);
  void addBinding(std::vector<sButtonEvent> aEvents, buttonCallback_t callback);
  void clearBindings();

  const std::vector<sButtonBinding>& getBindings() const;
  
private:  
  std::vector<sButtonBinding> maBindings; /**< the button bindings for this
					     context. */
  
};

#endif // BUTTON_CONTEXT_HPP
