#ifndef __areaOfInterest_hpp
#define __areaOfInterest_hpp

#include "opencv2/core/core.hpp"

#include "elements/patch.hpp"
#include "faceDetector.hpp"
#include "faceTracker.hpp"
#include "panorama.hpp"
#include "subframe.hpp"
#include "voice.hpp"
#include "voiceTracker.hpp"

#include <time.h>

#define MAX_ROI_WIDTH  (int((90 / 360.0) * CV_PANO_RES_X)) /**< Largest
							      horizontal 
							      region in which
							      the face detector
							      can look for a
							      face */
#define MAX_ROI_HEIGHT CV_PANO_RES_Y /**< Largest vertical region in which the
					face detector can look */
#define MIN_ROI_WIDTH   64 /**< Smallest horizontal region in which the face
			      detector can look. 64 appears to be an
			      undocumented minimum for the QC face detector */
#define MIN_ROI_HEIGHT  64 /**< Smallest vertical region in which the face
			      detector can look. 64 appears to be an
			      undocumented minimum for the QC face detector */

/** This class manages the data and algorithms that define a space in the world
 *  that may be of interest to the Attention System.
 *
 * Currently, a person talking is the only real type of aera-of-interest (AOI) 
 * that is being dealt with.  Ultimately, AOIs will include things like 
 * whiteboards, physical objects that are being actively referenced by meeting
 * participants, etc.
 */
class cAreaOfInterest
{
public:
  // enumeration of possible priorities for an AOI
  typedef enum ePriority
    {
      MUST_SHOW,       // Must display now
      TRY_TO_SHOW,     // Show if there's room on the Stage
      DONT_SHOW        // Don't show (for future use/completeness)
    } ePriority_t;
  // TODO: Consider if it would be better to have types of AOIs instead of priorities
  //       (e.g. voice-activated person, visually-activated person, whiteboard, etc.)

  // enumeration of possible states for pinning AOIs
  typedef enum ePinState
    {
      NOT_PINNED,      // Not a pinned AOI.
      PINNED_EDITABLE, // Pinned and changeable (shows rect in pano).
      PINNED_FIXED     // Pinned and not changeable (shows pin in subframe).
    } ePinState_t;

  // constructors
  cAreaOfInterest(cVoice voice, cPanorama *pPano, owl::cPatch patch, int id,
		  bool pinned = false );
  cAreaOfInterest(cVoice voice, cPanorama *pPano, owl::cPatch patch, int id,
		  ePriority_t priority, bool pinned = false );
  
  // destructor
  ~cAreaOfInterest();
  
  // actions
  std::vector<cVoice> updateVoiceTracker(std::vector<cVoice> voices);
  void updateFaceTracking(void);
  void expire(void);
  void disable(void);
  void deactivate(void);
  void forceVoiceTrackerReset(void);
  void adjustPatchFocus(int theta, int phi);
  void recenterPatch(int theta, int phi);
  void adjustPatchSize(int dTheta, int dPhi, bool override = false);
  void petWatchdog(void);
  bool contains(int bearing);
  
  // setters
  void setPriority(ePriority_t priority);
  void setReadyToRemove(void);
  void setVoice(cVoice voice);
  void setVoiceDirection(int direction);
  void setPatch(owl::cPatch patch);
  void setPinnedEditable(bool editable);

  // getters
  bool isEnabled(void);
  bool isReadyToRemove(void);
  ePinState_t getPinnedState(void);
  bool isPinned(void);
  int getId(void);
  ePriority_t getPriority(void);
  voiceState_t getVoiceTrackerState(void);
  faceState_t getFaceTrackerState(void);
  cv::Rect getFaceSearchRoi(void);
  cv::Rect getFace(void);
  cSubframe * getSubframe(void);
  cVoice * getVoice(void);
  owl::cPatch * getPatch(void);
  int getTheta(void);
  int getDTheta(void);
  int getPhi(void);
  int getDPhi(void);
  struct timespec getWatchdog(void);

private:
  //data
  ePriority_t mPriority; /**< Lets cVisualEditor know how important it is for
			     this AOI to be displayed in the cLayout. */
  bool mEnabled; /**< Flag indicating AOI should be given chance to update. */
  bool mActive;  /**< Flag indicating AOI contains an active trigger (meaning,
		    that it should be included in the Layout). */
  ePinState_t mPinned; /**< Pin state. */
  int  mId;     /**< Identifier for this AOI. */
  bool mReadyToRemove; /**< Flag indicating the Visual Editor is done with AOI.*/
  cVoiceTracker mVoiceTracker; /**< Object that tracks the voice associated with 
				  the AOI. */
  cPanorama *mpPano; /**< Panorama from which to get image data from
			for AOI. */
  cFaceTracker mFaceTracker; /**< Object tha tracks the face that belongs to 
				the AOI. */
  cFaceDetector mFaceDetector; /**< Detector that looks for a face in the AOI. */
  cSubframe mSubframe; /**< Subframe that is used to show this AOI in the output
			  video. */
  cv::Rect mFaceSearchRoi; /**< Region-of-interest in which faces may be looked
			      for (in camera's full-resolution reference 
			      frame. */
  owl::cPatch mPatch; /**< Patch that defines the geometry of the Aoi in 
			 spherical polar coordinates. */
  struct timespec mStartTime; /**< When the AOI was first created. */
  struct timespec mWatchdog;  /**< Used to help determine if an AOI has gone 
				 stale. */
  
  // actions
  void init(int id, cVoice voice, cPanorama *pPano, owl::cPatch patch, 
	    ePriority_t priority, bool pinned );
  void createSubframe(void);
  std::vector<cv::Rect> findFaces(void);
  void setFaceSearchRoi(cv::Rect face, bool faceFound);
  void growFaceSearchRoi(int dx, int dy);
  cv::Point updatePatchFocus(int theta, int phi);
  
  // debug helpers
  void printFaceSearchRoi(std::string tag);
};

#endif
