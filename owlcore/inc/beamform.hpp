#ifndef _BEAMFORM_HPP_
#define _BEAMFORM_HPP_

#include <pthread.h>

#include "params.h"

class cAudioMgr;

class cBeamform
{
public:
  cBeamform();
  ~cBeamform();

  int  getAngle(void);
  void setAngle(int angle);
  void update(int16_t *pPcmOut, int16_t *pPcmIn, int bufLen);
  void setOwner(cAudioMgr* owner);

private:
  int  mAngle;          /**< desired direction for beamforming. */
  int  mBeamDelays[N_BEAMS][N_MICS]; /**< array of delays for each beam and
					all mics. */

  ///< setup the owner audio manager so beamformer can call shared functions;
  cAudioMgr* mOwner;


  // thread variables
  pthread_mutex_t mMutex;  /**< Mutex for shared resources. */

  void delayAndSum(int16_t *pSum, int16_t *pPcmIn, int bufLen, int *pDelays);
  void visualize(int16_t *pPcmOut, int16_t *pPcmIn, int bufLen, int *pDelays);
};


#endif // _BEAMFORM_HPP_
