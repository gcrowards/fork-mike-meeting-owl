/*
 * netStatus.hpp
 *
 * Copyright (c) Owl Labs Inc. 2017.
 * All rights reserved.
 *
 *  Created on: Apr 28, 2017
 *
 *  Module implementing thread to check network configuration and internet
 *  connectivity.
 */

#ifndef OWLCORE_INC_NETSTATUS_HPP_
#define OWLCORE_INC_NETSTATUS_HPP_

#include <thread.hpp>


/** Class for state machine that tracks network connectivity. It reports
 * when the WiFi network is connected, and when Internet access is available.
 */
class cNetStatus
{
public:
  cNetStatus();
  ~cNetStatus();

  /** Enumeration that indicates current state of the state machine. */
  typedef enum
  {
    NO_NETWORK_CONCLUSION_YET,  /**< Don't know if WiFi is available yet. */
    NETWORK_UNCONFIGURED,       /**< WiFi is not yet configured. */
    NETWORK_CONFIGURED,         /**< WiFi is known to be configured. */
    NO_INTERNET_CONCLUSION_YET, /**< Don't know if Internet is available yet. */
    INTERNET_INACCESSIBLE,      /**< Internet is not accessible. */
    INTERNET_ACCESSIBLE,        /**< Internet is responding. */
  } eNetworkState_t;

  int  isNetworkConfigured(void);
  int  isInternetAccessible(void);
  void updateNetStatus(void);

private:
  cThread mNetStatusThread; /**< Thread that executes the state machine. */

  eNetworkState_t mState;   /**< Current state of the state machine. */
  int mAttemptCount;        /**< Controls rate of running the state machine. */
  long long mkRetryAttemptMinInterval = 100000; /**< Min time between attempts
      * to determine connectivity (in nano-seconds). */
  long long mkRetryAttemptMaxInterval = 50 * mkRetryAttemptMinInterval; /**< Max
      * time between attempts to determine connectivity (in nano-seconds). */
  int mkRetrySteadyStateCount = (int)(
      mkRetryAttemptMaxInterval / mkRetryAttemptMinInterval); /**< Number of
      * retries required to reach steady state (max) time between attempts. */

  bool testNetworkConfigured(void);
  bool testInternetAccessible(void);
};


#endif /* OWLCORE_INC_NETSTATUS_HPP_ */
