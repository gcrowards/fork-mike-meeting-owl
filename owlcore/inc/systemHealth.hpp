#ifndef __owlcore_system_health_hpp
#define __owlcore_system_health_hpp

#include <stdio.h>
#include <string>
#include <time.h>

#define PROC_STAT_COUNT 7 /**< number of items to read in from /proc/stat */

/**  cSystemHealth is used to monitor perfomance (e.g. frame rate, SOC 
 * temperature, etc.).
 *
 * This module should NOT be used in production, it is intended for development
 * and testing only.
 */
class cSystemHealth
{
public:
  // constructor(s)
  cSystemHealth(void);
  cSystemHealth(std::string saveDirectory);

  // destructor
  ~cSystemHealth();

  // Locations of temperature sensors in system
  typedef enum
  {
    RIGHT_EDGE_OF_SOC = 0,
    CAMERA = 1,
    BETWEEN_GPU_AND_MODEM = 2,
    UNKNOWN_1 = 3,
    BETWEEN_CPUS_2_AND_3 = 4,
    CPU_0 = 5,
    UNKNOWN_2 = 6
  } eTempSensorLoc_t;

  // public interface
  void start(std::string saveDirectory);
  void update(float fps);
  void close(void);
  
  double getTempAtLoc(eTempSensorLoc_t loc);

private:
  // data
  bool mPrimed;                     /**< Indicates ready to log data. */
  FILE* mpFileHandle;               /**< Handle to data log file. */
  std::string mSaveDir;             /**< Directory that contains the system 
				     * health log files. */
  float mFps;                       /**< Current video frame rate. */
  struct timespec mStartTime;       /**< System time when log started. */
  struct timespec mLastUpdateTime;  /**< Last time we wrote to log file. */
  long double mPrevProcStat[PROC_STAT_COUNT];  /**< Data from previous 
						* /proc/stat read (used to
						* calculate system loading. */
  
  // internal functions
  void init(void);
  int openDataFile(void);
  int writeData(std::string data);
  int closeDataFile();
  int pathify(char *pFullPath, const char *pFileName);
  int mkdirWithParents(const char *pDir, const mode_t mode);
  std::string readCpuStateFile(std::string stateFileName);
  float getCpuLoad(void);
  std::string getCpuStatusResults(void);
};

#endif
