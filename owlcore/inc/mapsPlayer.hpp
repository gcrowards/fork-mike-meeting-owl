#ifndef __mapsPlayer_hpp
#define __mapsPlayer_hpp

#include <map>
#include <string>
#include <time.h>
#include <vector>

#include "item.hpp"
#include "utils/csvParser.hpp"
#include "voice.hpp"


/** This class helps playback previous recorded, or otherwise generated person
 * and voice map data.
 *
 * Instant person Item maps and voice maps in CSV format can be used in place
 * of live sensor data to drive the attention system.  The data can be 
 * played back in real time (skipping data frames, or repeating data frames, if
 * need be), or each frame of data can be played back without loss or 
 * duplication (though the playback rate will not necessarily be true to the
 * rate at which the data was recorded in the CSV files).  For debugging 
 * applications you probably want to play back every frame of data to ensure
 * consistency from run-to-run on the same data set.  See usage notes for more
 * info: <a href="md_owlcore_designNotes_mapsRecordSimAndPlayblack.html"> 
 * Maps Record, Sim, & Playback Usage</a>.
 */
class cMapsPlayer
{
public:
  // contructors
  cMapsPlayer();
  cMapsPlayer(std::string personDataFilename, std::string voiceDataFilename);

  // destructors
  ~cMapsPlayer();
  
  // actions
  void start(float time = 0.0);
  void jumpTo(float time);
  void reset(void);
  void playEveryFrame(void);
  void playRealTime(void);
  double getTime(void);
 
  // accessors
  void setPersonDataFilename(std::string filename);
  void setVoiceDataFilename(std::string filename);
 
  std::vector<cItem> getPersonMap(void);
  std::vector<cVoice> getVoiceMap(void);

private:

  // custom types to make dealing with person map and voice map data easier
  typedef std::map<float, std::vector<cItem>> itemTable_t;
  typedef std::map<float, std::vector<cVoice>> voiceTable_t;

  // data
  std::string mPersonDataFilename; /**< CSV file containing person map time 
				    * series data. */
  std::string mVoiceDataFilename;  /**< CSV file containing sound source location
				      (really voice bearing) time series data. */
  
  struct timespec mStartTime;   /**< Reference time that by default corresponds 
				 * to 0 second time in data files. */
  
  itemTable_t mPersonMapTable; /**< Each row is a "keyframe" in time that
				* contains a vector of cItem objects that
				* describe the person map state. */
  voiceTable_t mVoiceMapTable; /**< Each row is a "keyframe" in time that 
				* contains a vector of sound source ("voice") 
				* bearings. */
  
  bool mPlayEveryFrame;        /**< Makes player play every line of data files
				* (instead of playing back based on relative 
				* wall time). */
  itemTable_t::iterator mPersonTableIterator; /**< Iterator to current time
					       * (only used when mPlayEveryFrame
					       * enabled). */
  voiceTable_t::iterator mVoiceTableIterator; /**< Iterator to current time
					       * (only used when mPlayEveryFrame
					       * enabled). */

  // internal actions
  void init(std::string personDataFilename, std::string voiceDataFilename);
  bool loadData(void);
  void clearData(void);

  bool createPersonMapTable(owl::numericalTable_t<float> rawTable);
  bool createVoiceMapTable(owl::numericalTable_t<float> rawTable);
};

#endif
