#ifndef __debugManager_hpp
#define __debugManager_hpp

// Bit mask for debug switches that can be software controlled
typedef enum liveDebugTools_e {
  eAoi =                   1 << 0,
  eFaceDetector =          1 << 1,
  eSoundLocalizer =        1 << 2,
  eFaceMapper =            1 << 3,
  eFaceMapperSearch =      1 << 4,
  eFaceTracker =           1 << 5,
  eMotionMapper =          1 << 6,
  eSoundMapper =           1 << 7,
  ePersonMapper =          1 << 8,
  ePersonInstantItemMap =  1 << 9,
  ePersonLongTermItemMap = 1 << 10,
  eVoiceTracker =          1 << 11,
  eLoudspeaker =           1 << 12,
  eMicrophone =            1 << 13
} liveDebugTools_e;


/** This class is used to control which debug tools are enabled. 
 *
 * The settings in debugSwitches.h is the starting point for which debug tools 
 * are enabled/disabled, but some tool may be turned on/off at runtime, and this
 * is the module through which that happens.
 */
class cDebugManager 
{
public:
  // constructors
  cDebugManager(void);

  // destructor
  ~cDebugManager();

  // actions
  void enable(int bitMask);
  void disable(int bitMask);
  bool check(liveDebugTools_e tool);

  void allOff(void);
  void allOn(void);

private:
  // data
  static int mToolState; /**< Stores the bit mask that dictates which debug
			  * tools are on and which are off. */

  //actions
  void setTools(int bitMask, bool state);
};

#endif
