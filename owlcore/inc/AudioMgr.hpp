#ifndef _AUDIOMGR_HPP_
#define _AUDIOMGR_HPP_

#include <pthread.h>
#include <stdio.h>
#ifdef __ANDROID__
#include "speex_echo.h"
#include "speex_preprocess.h"
#include "audioframework.h"
#else
typedef struct 
{
  int status;
} SpeexPreprocessState;
typedef struct 
{
  int status;
} SpeexEchoState;

#endif
#include "beamform.hpp"
#include "params.h"

#define FLOAT float

extern FLOAT db2mag(FLOAT in);

#define DEFAULT_FS 48000
#define SAMPLES_PER_MS  48
//#define ECHO_TAIL_MS    180
//#define ECHO_TAIL_MS    225
#define ECHO_TAIL_MS    480
#define ECHO_TAIL_SIZE  (ECHO_TAIL_MS*SAMPLES_PER_MS)
#define MAX_PROC_SIZE   (SAMPLES_PER_MS*100)

#define DEFAULT_NR      0
#define DEFAULT_NR_DEPTH  -20

//#define SPKR_HYS_TIME (1500000000LL)
#define SPKR_HYS_TIME (750000000LL)
#define MIN_SPKR_LEVEL -50

#define MIN_MIN_SPEAKER_LEVEL -50
#define MAX_MIN_SPEAKER_LEVEL -35

#define MIN_NR_AMOUNT 0
#define MAX_NR_AMOUNT 20

#define SPKR_ACTIVE_UP 0.01
#define SPKR_ACTIVE_DOWN 0.01
#define MIN_SPKR_ACTIVE -50

// set to match CalcSpkrRMS behaviour



#ifdef __ANDROID__
#include "audioframework.h"
#endif


///< audio manager setups up properties if in Linux environment
#ifndef __ANDROID__
#include "limits.h"

#define PROC_FRAME_SIZE 960
#define ONE_BILLION 1000000000L
#define SPKR_NOT_ACTIVE   0
#define SPKR_ACTIVE       1
#define AUDIO_SYSTEM_DEBUG false
#define ALM_DEBUG          false
#define APP_OUT_FILE_OUT   false
#define AUDIOPLAYER_DEBUG  false
/// Defintion of the structure contains values read from the andriod property store.
/// This is an abbreviated structure used by the Linux build. It does not contain
/// libOwlPG only parameters.
typedef struct
{
  int audioCursor;        ///< angle to draw cursor for measuring angles
  int micLeftChan;        ///< mic channel to send on the left channel to host
  int micRightChan;       ///< mic channel to send on the right channel to host
  int enableEq;           ///< if set, enables the microphone eq.
  int enableBeam;         ///< if set enable beamforming on the output
  int enableAEC;          ///< if set will enable acoustic echo cancellation
  int enableNR;           ///< if set will enable noise reduction
  int enableLM;           ///< if set will enable level manager
  int micMute;            ///< property tied to mic mute
  int aecDebug;           ///< property to control output from AEC
  int enableMicProc;      ///< if not set will bypass all mic processing
  int aecPost;            ///< configures which algorithm to use after AEC
  int altThresh;           ///< The threshold for the alternate threshold
  int altEnable;          ///< Set the enable level of the alternate limiter
  int localDebug;         ///< controls what debug info printed from localizer
  int localNum;           ///< controls what beam number is used debug localizer
  int localLog;           ///< controls logging of localizer info to file
  int localRatio;           ///< localizer pre/post ratio * 10.
  int testActive;         ///< test property for speaker active level
  int minSpkrActive;      ///< minimum level for speaker to be active
  int nrAmount;           ///< amount of noise reduction to apply
  int dtdGain;            ///< amount of gain to apply during double talk (and
  int dtdRstGain;         ///< amount of gain to applu during remote single talk
  int dtdThreshBias;      ///< bias for dtd threshold between LST and RST x100
  int dtdMaskThresh;      ///< threshold for echo to be considered masked pre converge
  int micGainsDb[8];      ///< gains for each of the microphones for the downmix  
} AUDIO_PROPERTIES;


typedef struct
{
  int procFrameCount;
  int micCount;
} AUDIO_CONFIG;

/// Structure contains information about the state of the audio interfaces
typedef struct
{
  int refAvail;

} AUDIO_IF_STATUS;

typedef struct
{
  int status;
}POLL_TIMER;

typedef struct 
{
  int status;
}CIRC_BUFF;


extern int GetAudioProperties(AUDIO_PROPERTIES *);
extern int GetAudioConfig(AUDIO_CONFIG *);
extern int AudioIfStatusGet(AUDIO_IF_STATUS *pAis);
extern unsigned long long getNsecs();
extern unsigned long long getMsecs();
extern double GetSpeakerRMSLevel();
extern int PollTimerInit(POLL_TIMER *timer, unsigned long long interval);
extern int PollTimerPollTimer(POLL_TIMER *timer);
extern int PollTimerPollTimerOnce(POLL_TIMER *timer);
extern int PollTimerReset(POLL_TIMER *timer, unsigned long long interval);


#endif

#include "AudioEqualizer.hpp"
#include "AudioLevelManager.hpp"

typedef struct
{
	int echoAdapted;
	float leakEstimate;
	float Pey;
	float Pyy;
	float meanResidualEcho;
	float meanApriorSNR;
	float meanApostSNR;
	int speechProb;

}DETECTORS;
#include "soundLocalizer.hpp"
#include "audioDTD.hpp"

///< class to manage audio processing

class cAudioMgr
{
public:
  cAudioMgr();
  ~cAudioMgr();

  void update(int16_t *pPcmOut, int16_t *pPcmIn, int16_t *pRefIn, int bufLen);
  int  getAngle(void);
  void setAngle(int angle);
  AUDIO_PROPERTIES *getAudioPropPtr();
  int mAudioMgrFs;
  void getOutputLevels(float *pRMS, float *pPeak);
  void setLocalizer(cSoundLocalizer *localizer);
  void controlNr(int);
  void MixInput(short *input, short *output,int len);  
  cAudioDTD *getDTD();
  double getSpkrLevel();


private:
  cSoundLocalizer *mpLocalizer; ///< Pointer to sound localizer instance
  EQUALIZER       mLeftEq;  ///< Equalizer for left microphone channel
  EQUALIZER       mRightEq; ///< Equalizer for right microphone channel
  EQUALIZER       mRefEq;   ///< used to remove signal from the ref 
                            ///< that Owl can't reproduce.
  EQUALIZER       mSpkrActiveEq; ///< used for getting speaker level
                                 ///< filters material the speaker filters 
  cAudioDTD       *mpDoubleTalkDetector; // double talk detector

  cBeamform       mBeamform; ///< beamforming instance. foo
  ALM_STATE       mLevelMgr;
  ALM_STATE       mOutputLevelMgr;
  ALM_STATE       mPostDTDLm;
  AUDIO_PROPERTIES mAudioProps;
  AUDIO_CONFIG    mAudioConfig;
  FILE            *mToUsbFd;
  int             mEnableNr;
  int             mLastNrAmount;  
  double          mSpkrActiveRMS;
  float           mMicGains[8];
  
  
#ifdef __ANDROID__

  SpeexEchoState *mpSpeexEcho;
  SpeexPreprocessState *mpSpeexNoise;
  
  int16_t mMicAecBuff[MAX_PROC_SIZE];
  int16_t mRefAecBuff[MAX_PROC_SIZE];
  int16_t mOutAecBuff[MAX_PROC_SIZE];

  
#endif


};

#ifdef __ANDROID__
extern int GetSpeexDetectors(DETECTORS *detectsOut, SpeexEchoState *pEcho, SpeexPreprocessState *pPost);
#endif

extern int AMIsSpeakerActive();
extern int AMSetMinSpeakerLevel(int minLevel);
extern int AMGetMinSpeakerLevel(int &minSpkrLevel);
extern FLOAT pow2db(FLOAT in);
extern FLOAT db2pow(FLOAT in);
extern FLOAT mag2db(FLOAT in);
extern FLOAT db2mag(FLOAT in);
extern float ARMAFilter(float lastVal, float newVal, float upCoeff, float downCoeff);
extern int AMSetNRAmount(int amount);
extern int AMGetNRAmount(int &amount);
extern int AMSetAecPostAlgo(int algo);
extern int AMGetAecPostAlgo(int &algo);

#endif // _AUDIOMGR_HPP_
