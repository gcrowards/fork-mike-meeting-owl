#ifndef __personMapper_hpp
#define __personMapper_hpp

#include "faceMapper.hpp"
#include "item.hpp"
#include "map.hpp"
#include "motionMapper.hpp"
#include "soundMapper.hpp"

/** This class is responsible for mapping out the locations of people in the
 * room based on evidence from other maps (face, motion, sound).
 */
class cPersonMapper
{
public:
  // constructors
  cPersonMapper();

  // destructor
  ~cPersonMapper();

  // action
  void update(cFaceMapper *pFaceMapper,
	      cMotionMapper *pMotionMapper,
	      cSoundMapper *pSoundMapper);
  void update(std::vector<cItem> people);
  void resetMap(void);

  // setters

  // getters
  int getMapValAt(int angle);
  int getAngleWithMaxVal(int nominalAngle, int plusMinus);
  cMap getMap(void);
  const std::vector<const cItem *> getInstantItemMap(void);
  const std::vector<const cItem *> getLongTermItemMap(void); 
  cItem * getPersonAt(int bearing);
  
private:
  // data
  cMap mMap; /**< The map keeps track of the positional data of the things 
		found by this mapper. */

  // actions
  void updateDirect(cFaceMapper *pFaceMapper,
		    cMotionMapper *pMotionMapper,
		    cSoundMapper *pSoundMapper);
  void updateAddLeak(cFaceMapper *pFaceMapper,
		     cMotionMapper *pMotionMapper,
		     cSoundMapper *pSoundMapper);
};

#endif
