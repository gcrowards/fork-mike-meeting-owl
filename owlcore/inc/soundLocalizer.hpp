#ifndef _SOUND_LOCALIZER_HPP_
#define _SOUND_LOCALIZER_HPP_

#include "params.h"
#ifdef __ANDROID__
#include "speex_echo.h"
#include "speex_preprocess.h"
#endif


// TODO: move the following parameters to params.h when they've stabilized
// #define BEAM_N_PCM_CHUNKS  8 /**< number of audio chunks aggregated for

//#define HIST_COUNT            128 ///< Number of past angles to vote
#define VOTE_TIMEOUT (3*ONE_BILLION)

#define HIST_COUNT            128 ///< Number of past angles to vote
//#define VOTE_TIMEOUT          (2*ONE_BILLION) //HMBT
#define LOCALIZER_FRAME_COUNT 240 ///< number of audio frames to process each time
#define BEAM_N_PCM_CHUNKS  16 /**< number of audio chunks aggregated for
        localization processing. */

///< Tunable parameters
#define ALT_BEAM_SCORE_THRESHOLD     50 /**< level of beam score below which the
             quality is suspicious. */
#define MINIMUM_VOTES                20
#define BEAM_DISPERSION_THRESHOLD    80 /**< level of localized angle dispersion
             above which quality is suspicious. */
#define MAX_RUNNING_COUNT               64
#define MIN_RUNNING_COUNT              -24
#define MAX_CONSECUTIVE_COUNT           5
#define MIN_VOTE_DIFF                   3
#define MAX_VOTE_DIFF                  10

#define INTER_SIG_TIME  (3*ONE_BILLION)
#define SEGMENT_TIME    (5*ONE_BILLION)
#define NOISE_WAIT_TIME (5*ONE_BILLION)
#define SPURIOUS_TIME   (3*ONE_BILLION)
#define ENERGY_HIST_TIMEOUT  (8*ONE_BILLION)
#define MIN_EVENT_COUNT      8
#define ENERGY_HIST_COUNT    32

#define PRE_POST_RATIO_THRESH 0.5
#define APRIORI_SNR_THRESH   1.0


#define NOISE_TYPE           0
#define SPEECH_TYPE          1
#define UNKNOWN_TYPE         2


typedef struct
{
  int beamNum;
  unsigned long long timestamp;
  int speechNoise;
} HIST_ENTRY;

typedef struct
{
  int count;
  int index;
} SORT_ENTRY;

typedef struct
{
  int energy;
  long long diffEnergy;
  unsigned long long time;
} ENERGY_ENTRY;

typedef struct
{
  unsigned long long startTime;
  unsigned long long lastTime;
  unsigned long long totalEnergy;
  int lastEnergy;
  unsigned long long diffEnergy;
  int eventCount;
  int type;
  int energyIndex;
  ENERGY_ENTRY energyHistory[ENERGY_HIST_COUNT];

} SIG_HISTORY;

class cSoundLocalizer
{
public:
  cSoundLocalizer();
  ~cSoundLocalizer();

  int  getAngle(void);
  int  getDispersion(void);
  void setAngleAndDispersion(int, int);
  int  getSpeechStatus(int beamNum);

  int  getLastAngle(void);
  int  getTotalEnergy(void);

  void resetBeamHist(void);
  bool update(int16_t *pPcmBuf, int bufLen);
  void reset();

  void setAudioMgr(cAudioMgr *pMgr)
  {
    mpAudioMgr = pMgr;
  };

  short getDebugVal(int index)
  {
    return mDebugVal;
  }

private:
  cAudioMgr *mpAudioMgr; ///< pointer the audio manger
  pthread_mutex_t mResetMutex;  
  pthread_mutex_t mOutputMutex;    
  
  short mDebugVal;
  int  mAngle;          /**< direction of localized sound. */
  int  mDispersion;     /**< dispersion [0,100] of localization angle. */
  int  mTotalEnergy;    /**< total energy from mics. */
  int  mMicEnergy[N_MICS];   /**< energy for each mic. */
  int  mBeamCorrs[N_BEAMS];  /**< beam correlations. */
  int  mMicCorrs[N_MICS][N_MICS][(BEAM_MAX_TAU + 1)]; /**< mic pair cross-
                   correlations. */
  int  mBeamDelays[N_BEAMS][N_MICS]; /**< array of delays for each beam and
            all mics. */
  double mBeamCorrsNorm[N_BEAMS]; ///< beam cross correlation normalized to
  ///< audio correlation of a mic
  double mFom;  // Figure of merit about how good the localization is

  int mVoteArray[N_BEAMS];       ///< Histogram of number of detections
  SORT_ENTRY mVoteArraySorted[N_BEAMS]; ///< sorted version by number of hits
  EQUALIZER mLocalEq[N_MICS];   ///< Equalizer for the localizer channels
  EQUALIZER mDetectEq;          ///< Equalizer for detector microphone channel
  DETECTORS mSpxDetects;        ///< detector values from speex library
#ifdef __ANDROID__  
  SpeexPreprocessState *mpSpeexDetect;  ///< speex state use for detectors
#endif  
  LEVEL_STATE mPreNRLevelState;   ///< levels before the noise reducer
  LEVEL_STATE mPostNRLevelState;  ///< levels after the noise reducer
  LEVELS mPreNRLevels;        ///< Calculated levels
  LEVELS mPostNRLevels;       ///< Calculated levels
  SIG_HISTORY mSigHistory[N_BEAMS]; ///< history of number of noise vs sig events

  float mPrePostRatio;        ///< ratio of pre and post noise reducer
  int mTotalCount;
  float mMinEnergy;           ///< energy of non-events
  ///< (used as proxy for min energy)


  int mHistInd;               ///< current write index into mBeamVoteHist
  int mHistInit;              ///< indication of mBeamVoteHist being initialized
  HIST_ENTRY mBeamVoteHist[HIST_COUNT]; ///< histogram memory of max beam corrs.
  int mValidMeasures;         ///< number of valid measurements made since start
  int mInvalidCount;          ///< number of invalid entries in mBeamVoteHist

  int mBeamNumber;          ///< number of the selected beam 
  int mLastBeamNumber;      ///< number of last beam selected
  int mConsecutiveCount;    ///< number of times the beam number has been the same
  FILE *mDebugFd;            ///< debug file descriptor
  int mLastAngle;

  ///< circular buffer and temp processing buffer
#ifdef __ANDROID__
  SpeexPreprocessState *mpSpeexDetectors;
  CIRC_BUFF *mpProcToLocalizerCB;
#endif
  DETECTORS mSpxDetectors;
  short  mProcBuffer[LOCALIZER_FRAME_COUNT * N_MICS];
  short  mDetectBuff[LOCALIZER_FRAME_COUNT];

  void computeBeamCorrs(void);
  int  computeCorrelation(int m0, int m1, int tau,
                          int16_t *pPcmBuf, int nSamplesPerMic);
  bool computeMicCorrs(int16_t *pPcmBuf, int nSamplesPerMic);
  int  getMicCorrelation(int m0, int m1, int tau);
  bool processPcmData(int16_t *pPcmBuf, int nSamplesPerMic);
  void UpdateVote(int beam, int score, int energy);
  void printDebug();
  int  ManageSigHistory(int beamNum, int score, int energy);
  int  ManageEnergyHistory(int beamNum, int energy, int sigFlag);


};


#endif // _SOUND_LOCALIZER_HPP_
