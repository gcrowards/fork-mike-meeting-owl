#ifndef __visualEditor_hpp
#define __visualEditor_hpp

#include <array>
#include <vector>

#include "opencv2/core/core.hpp"

#include "areaOfInterest.hpp"
#include "common.hpp"
#include "debugManager.hpp"
#include "elements/panel.hpp"
#include "elements/rgbaOverlayLists.hpp"
#include "elements/vertex.hpp"
#include "layout.hpp"
#include "faceMapper.hpp"
#include "motionMapper.hpp"
#include "overlayHelper.hpp"
#include "panorama.hpp"
#include "personMapper.hpp"
#include "soundMapper.hpp"
#include "subframe.hpp"
#include "voice.hpp"

/** This class is responsible for taking the active cAreaOfInterest objects and 
 * building the image frame that is the final output.
 *
 * It does this by updating the cLayout, animating the cSubframes that are part
 * of the cLayout, and from that, generating the output frame.
 */
class cVisualEditor
{
public:
  // constructors
  cVisualEditor(void);
  
  // destructor
  ~cVisualEditor();

  // actions
  void update(std::vector<cAreaOfInterest *> pAois, 
              owl::cRgbaOverlayLists *pRgbaOverlays,
	      cFaceMapper *pFaceMapper, 
	      cMotionMapper *pMotionMapper,
	      cSoundMapper *pSoundMapper,
	      cPersonMapper *pPersonMapper,
	      std::vector<cVoice> voices,
	      audioChannelStatus_t mSpeakerStatus,
	      audioChannelStatus_t mMicStatus,
	      bool frameRateSlow );
  void createPanoramicSubframe(float ang = 0);
  void shiftPanoramicSubframe( float ang);
  void clearLayout(void);
  void nextAnimationMode(void);
  
  // getters
  cv::Mat * getFrame(void);
  std::vector<owl::cPanel> getCompositePanels(void);

  // setters
  void setPanorama(cPanorama *pPano);

private:  
  /** Used to keep track of buffer graphs created by cGpu. */ 
  struct sDebugGraph
  {
    int id = -1;                /**< The ID of this debug graph. */
    int gpuGraphId = -1;        /**< The ID of the GPU buffer graph. */
    bool used = false;          /**< whether this graph is being used to display
				   data. */
    std::vector<sVertex> data;  /**< The graph vertex data. */
  };
  
  // data
  cPanorama *mpPano; /**< The input data source. */
  cOverlayHelper mOverlayHelper; /**< This is used to annotate the output 
				    frame. */
  cLayout mLayout; /**< This is where the cSubframe objects are arranged. */

  std::vector<owl::cPanel> mCompositePanels; /**< list of panels that are to be
					      * included in the output 
					      * composite. */
  
  cSubframe mPanoramicSubframe; /**< Subframe for scaled-to-fit panoramic image
				 * that is a static portion of the Layout. */

  cDebugManager mDebugManager; /**< Keeps track of which debug items are on and
				* which are off (for debug items that are 
				* software settable. */

  // buffer graph data
  static const int mkFaceGraphId = 0;  /**< Index of the face map buffer graph. */
  static const int mkMotionGraphId = 1;/**< Index of the motion map buffer graph. */
  static const int mkSoundGraphId = 2; /**< Index of the sound map buffer graph. */
  static const int mkPersonGraphId = 3;/**< Index of the person map buffer graph. */
  static const int mkGraphCount = 4;   /**< Number of debug graphs. */
  std::array<sDebugGraph, mkGraphCount> mDebugGraphs; /**< Debug graph vector. */
  std::vector<sDebugGraph *> mInstantGraphs; /**< Person instant map graphs. */
  std::vector<sDebugGraph *> mLongTermGraphs;/**< Person long-term map graphs. */

  // actions
  void updateLayout(std::vector<cAreaOfInterest *> pAois);
  void refitAndValidateLayout(std::vector<cAreaOfInterest *> pAois);
  bool makeSpace(int requiredSpace, std::vector<cAreaOfInterest *> pAois);
  void makeSpace(std::vector<cAreaOfInterest *> pAois);
  void animateSubframes(std::vector<cAreaOfInterest *> pAois);
  void overlayAoiGui(std::vector<cAreaOfInterest *> pAois);
  void overlayProgressBar(cv::Rect rect, cv::Scalar color);
  void overlayDebugInfo(std::vector<cAreaOfInterest *> pAois, 
			cFaceMapper *pFaceMapper, 
			cMotionMapper *pMotionMapper,
			cSoundMapper *pSoundMapper,
			cPersonMapper *pPersonMapper,
			std::vector<cVoice> voices,
			audioChannelStatus_t mSpeakerStatus,
                        audioChannelStatus_t mMicStatus,
			bool frameRateSlow );
  void overlayAudioLevels(audioChannelStatus_t, int line);
  void overlayPanoDegMarkers(void);
  void overlayVoices(std::vector<cVoice> voices);
  void overlayAois(std::vector<cAreaOfInterest *> pAois);
  void overlayMap(cMap map, int deltaDeg, sColor color, float lineWidth,
		  sDebugGraph &graph );
  void overlaySoundLocalizer(void);
  void overlayAngle(int angle, float scale, cv::Scalar color);  
  void overlayItems(const std::vector<const cItem *> kpItemMap, int deltaDeg,
		    bool uniqueIds, std::vector<sDebugGraph *> &itemGraphs );
  int  panoDegToFrameLoc(int deg);
  float panoDegToGpuLoc(int deg);
  void highlightFaceSearchArea(cAreaOfInterest *pAoi);
  void highlightFace(cAreaOfInterest *pAoi);
  void assembleFrame(std::vector<cAreaOfInterest *> pAois);
  void assembleFrameForGpu(std::vector<cAreaOfInterest *> pAois);
  void updatePinnedAois(std::vector<cAreaOfInterest *> pAois,
                        owl::cRgbaOverlayLists *pOverlays);
};

#endif
