/*
* Implements the audio double talk detector object.
*/

#ifndef _AUDIODTD_H_
#define _AUDIODTD_H_

#ifndef FLOAT
#define FLOAT float
#endif

#include "AudioMgr.hpp"


#define FRAME_SIZE PROC_FRAME_SIZE

//#define DEBUG_DTD
#define DTD_PROC_SIZE FRAME_SIZE
#define DTD_BYTE_SIZE (DTD_PROC_SIZE*sizeof(short))
#define DTD_CB_ITEM_SIZE 2

#define DTD_MARGIN 3.0

#define MIC_SILENCE_RMS_UP_ARMA 0.05
#define MIC_SILENCE_RMS_DOWN_ARMA 0.1
#define REF_RMS_UP_ARMA   0.05
#define REF_RMS_DOWN_ARMA 0.05
#define AEC_RMS_ARMA    0.1
#define RMS_LT_ARMA     0.005

//#define CONVERGE_LEAK_THRESHOLD 0.0075
//#define CONVERGE_LEAK_THRESHOLD 0.075
//#define CONVERGE_LEAK_THRESHOLD 0.01
#define CONVERGE_LEAK_THRESHOLD 0.025


#define AEC_MIN_DEFAULT 0.1
#define MIC_MIN_DEFAULT 0.005
#define REF_MIN_DEFAULT 0.005

#define MIC_DTD_SILENCE_THRESH  -60 // changed when inputs are attenuated; checking for min AEC
#define REF_DTD_SILENCE_THRESH  -60 // changed when inputs are attenuated; checking for min AEC
#define REF_DTD_MIN_SILENCE_THRESH  -60 // changed when inputs are attenuated; checking for min AEC
// Times in ms for hysterisis during particular state changed.
#define STATE_CHANGE_HYS_TIME  50
#define STATE_CHANGE_HYS_TIME_RS_LS 200 // longer hys time req. for rs to ls
#define STATE_CHANGE_HYS_TIME_LS_SIL 800

// Parameters for the FIR mean filter to make responsive but not chaotic.
//#define FIR_MEAN_FILTER_SIZE    60 //hmbt
#define FIR_MEAN_FILTER_SIZE    96
#define MIN_STAT_COUNT        16
#define MIN_STAT_EVENT_COUNT    5
#define MIN_STAT_MAX_EVENT_COUNT  25
#define AEC_STAT_DECAY        1.05
#define SILENCE_LEVEL_DECAY     1.002
#define MIC_SILENCE_LEVEL_DECAY   1.001
#define RMS_MIN_VALUE       (4.0000e-05)


// How long to wait to invalidate non silent state
#define NON_SILENT_TIMEOUT 1000

// Clipping detection parameters
#define CLIP_LEVEL          32300 // level where clipping can occur
#define CLIP_HYSTERSIS_TIME     3000  // time to wait (in ms) after last clip
#define CLIP_DURATION       3
#define CLIP_NO_CLIPPING      0   // indicates no clipping
#define CLIP_HYS          1   // Waiting for hysterisis timer
#define CLIP_CLIPPING       2   // clipping detected this frame

// Minimum ref level for calculating Total Coupling Loss (TCL)
#define TCL_MIN_LEVEL       -60
#define TCL_MAX_LEVEL       0

//#define __ANDROID__

// The various states the connection can be in.
enum DTDStates
{
  STATE_SILENCE,
  STATE_LOCAL_SINGLE_TALK,
  STATE_REMOTE_SINGLE_TALK,
  STATE_DOUBLE_TALK,
  STATE_BAD_STATE
};


/// State for minimum statistics filter for rst level calc.
typedef struct
{
  float silenceTime;
  float remoteTime;
  float localTime;
  float bothTime;
} DTDSTATS;
typedef struct
{
  FLOAT minSum;
  FLOAT minCount;
  FLOAT minMem[MIN_STAT_COUNT];
  FLOAT minMaxMem[MIN_STAT_COUNT];
  FLOAT minCurrMax;
  FLOAT minLastValue;
  FLOAT minMinValue;
  FLOAT minMinMaxValue;
  FLOAT minCurrValue;
  FLOAT minAcceptableValue;
  int   minStartState;
  FLOAT minDecayRate;
  int   minStartUp;
  int   eventCount;
  int   sumCount;
  int   rejectCount;

} MIN_STAT;

// State for FIR mean filter.
typedef struct
{
  FLOAT firMem[FIR_MEAN_FILTER_SIZE];
  int firInd;
  FLOAT firCurrVal;
  FLOAT firSum;

} FIR_MEAN_STATE;


class cAudioDTD
{
public:
  cAudioDTD();
  ~cAudioDTD();
  int SetupDTDNR(SpeexEchoState *, int fs, int frameCount);
  DTDStates ProcessDTD(short *pMicIn, short *pAecIn, short *pRefIn, int len,
                       DETECTORS *spxDetectors);
  DTDStates ProcessDTDWrap(short *pMicIn, short *pAecIn, short *pRefIn, int len,
                           DETECTORS *spxDetectors);
  DTDStates DTDGetState();
  int DTDAvailable();
  int DTDGetStatistics(DTDSTATS *pStats, int resetStats);
  int resetDTD();
  float getSilenceTime(void);
  float getRemoteTime(void);
  float getLocalTime(void);
  float getBothTime(void);
  void resetStats(void);
  int openDebug(char *, char *);

private:

  // Minimum statistics filters to estimate the local min value for various stats
  // Methods

  void  intResetStats();
  void  accumStats(int len);
  FLOAT AecMinStatisticsFilter(MIN_STAT &state, FLOAT currValue,
                               DTDStates currState, int reset);
  int   MinStatisticsFilterInit(MIN_STAT &state, FLOAT defaultValue,
                                FLOAT minAccept, FLOAT decayRate);
  int   MinStatisticsFilterReset(MIN_STAT &state);
  FLOAT MinStatisticsFilterCalc(MIN_STAT &state);
  FLOAT MinStatisticsFilterAccum(MIN_STAT &state, FLOAT currValue);
  int   CheckClipping(short *refAudio, int len);

  // FIR mean filter; similar to ARMA but quicker edge performance
  int   FIRMeanFilterInit(FIR_MEAN_STATE &state);
  FLOAT FIRMeanFilter(FIR_MEAN_STATE &state, FLOAT input);

  // pointer to NR used by DTD

  SpeexPreprocessState *mpSpeexDTD_NR;
  short *mpNrBuff;
  int16_t mNrBuffer[MAX_PROC_SIZE];
  // Variables
  DTDSTATS  mStats;

  DTDStates mCurrState;   ///< current state of the double talk detector
  DTDStates mLastState;   ///< last state the DTD
  DTDStates mLastNonSilentState; ///< last non silent state
  unsigned long long mLastNonSilentTime; // used to time out nont silent state
  unsigned long long
  mStateChangeTime; ///< how long to wait for the state to stablize


  int   mLastDTDPossible;
  MIN_STAT mMinAecRMS;
  MIN_STAT mMinMicRMS;
  MIN_STAT mMinRefRMS;
  FLOAT mAecRMSFiltered_p; // total power
  FLOAT mAecRMSFiltered;   // RMSvalue
  FLOAT mMicRMSFiltered_p;
  FLOAT mAttenRMSFiltered_p;  // difffernce in power filtered

  // members supporting FIR Mean
  FIR_MEAN_STATE mAecFIR_p; // filter for aec total poweer
  FIR_MEAN_STATE mAecFIR; // filter for aec RMS
  FIR_MEAN_STATE mMicFIR_p; // filter for aec total poweer


  FLOAT mMicRMSSilence; ///< ARMA filtered version of the mic RMS value
  ///< used to detect mic silence
  FLOAT mRefRMS;    ///< ARMA filtered version of the reference RMS value
  FLOAT mRefActRMS;    ///< ARMA filtered version of the reference RMS value, only calced when ref active
  FLOAT mRefActRMSLt;  ///< ARMA filtered version of the reference RMS value
  FLOAT mAecRMSLt;  ///< longer term RMS calculation for echo adapttion eval.
  FLOAT mMicRMSLt;
  FLOAT mRefRMSLt;

  short mClipCount;
  int   mClipState;
  int   mClipStateInt;
  unsigned long long mClipTime;

  int   mConvergedState;
  int   mEchoAdapted;
  int   mSpeakerActive;

#ifdef __ANDROID__
  // buffers for decoupling DTD processing from audio processing.
  // currently not used.
  short mRefBuff[DTD_PROC_SIZE];
  short mAecBuff[DTD_PROC_SIZE];
  short mMicBuff[DTD_PROC_SIZE];
  CIRC_BUFF mAecCB;
  CIRC_BUFF mRefCB;
  CIRC_BUFF mMicCB;
#endif

#ifdef DEBUG_DTD
  FILE *mpDebugDTDFd;
  FILE *mpMicDebugAudioFd;
  FILE *mpRefDebugAudioFd;
  FILE *mpOutDebugAudioFd;

#endif

};


#endif


