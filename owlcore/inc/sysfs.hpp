#ifndef __sysfs_hpp
#define __sysfs_hpp

#include <pthread.h>

// sysfs params
#define SYSFS_THREAD_USLEEP       200000 /**< microseconds for sysfs polling
					    thread to sleep, if at all. */
#define SYSFS_PATH  "/sys/devices/platform/snd_uac1.0" /**< path to the sysfs
							  node where kernel
							  and user space can
							  share variables. */
#define SYSFS_OTP_PATH	"/sys/devices/soc.0/1b0c000.qcom,cci" /**< path to sysfs
							     camera OTP vals. */
#define SYSFS_BAD_VALUE  -1 /**< unknown value of sysfs variable if, e.g.,
			       the sysfs node does not exist (as w/ linux). */
#define SYSFS_OTP_ERR	0xA5A55A5A

/** This class provides an interface to variables created on a sysfs
 * node by the kernel.
 */
class cSysfs
{
public:
  cSysfs();
  ~cSysfs();

  // setters
  int setMicMute(int value);
  int setMicVolume(int value);
  int setMute(int value);
  int setVolume(int value);

  // getters
  int getMicMute(void);
  int getMicVolume(void);
  int getMute(void);
  int getVolume(void);

private:
  int mMicMute;    /**< Most recently read mic mute. */
  int mMicVolume;  /**< Most recently read mic volume. */
  int mMute;       /**< Most recently read loudspeaker mute. */
  int mVolume;     /**< Most recently read loudspeaker volume. */

  // actions
  void update(void);

  // setters
  int  setValue(const char *pValueName, int value);

  // getters
  int  getValue(const char *pValueName);

  // thread variables
  pthread_t       mThread; /**< Thread for this object. */
  pthread_mutex_t mMutex;  /**< Mutex for shared resources. */
  bool    mKeepThreading;  /**< Flag to control iteration of thread loop. */

  // thread methods
  static void *threadWrapper(void *pContext); /**< Wrapper for pthread_create.*/
  void join(void);  /** Interface to exit the thread. */
  void start(void); /** Interface to start the thread. */
  void threadUpdate(void); /** Thread update loop. */
};

#endif // __sysfs_hpp
