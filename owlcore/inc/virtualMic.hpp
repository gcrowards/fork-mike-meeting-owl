#ifndef __virtualMic_hpp
#define __virtualMic_hpp

#include <pthread.h>
#include <vector>

#include "voice.hpp"

/** The virtual microphone class combines several physical microphones, 
 * identifies the strongest channel, and can return the audio data from any of 
 * the microphone channels.
 */
class cVirtualMic
{
public:
  // constructors
  cVirtualMic(void);

  // destructor
  ~cVirtualMic();

  // setters

  // getters
  std::vector<cVoice> getVoices(void);
  std::vector<int> getSoundBearings(void);

  // actions
  void update(int voiceBearing, std::vector<int> soundBearings);
  bool isReady(void);

private:
  // data
  std::vector<cVoice> mVoices;
  std::vector<int> mSoundBearings;

  bool mIsReady;

  int mActiveMicDirection;

  // thread variables
  pthread_mutex_t mMutex;  /**< Mutex for shared resources. */
};

#endif
