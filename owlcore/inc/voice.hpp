#ifndef __voice_hpp
#define __voice_hpp

#include <vector>

/** This class stores the data that represents an audio source that is assumed to
 * be a person speaking. 
 *
 * NOTE: This class is pretty sparse and is mostly a placeholder.
 */

class cVoice
{
public:
  // constructors
  cVoice(void);
  cVoice(int direction, float score, std::vector<int> audioData);

  // destructor
  ~cVoice();

  // setters
  void setDirection(int direction);
  void setScore(float score);
  void setAudioData(std::vector<int> audioData);

  // getters
  int getDirection(void);
  float getScore(void);
  std::vector<int> * getAudioData(void);

private:
  // data
  int mDirection; /**< The bearing of the Voice (0 - 360 degrees). */
  float mScore;   /**< The confidence that the direction is accurate. */
  std::vector<int> mAudioData; /**< The audio data that this Voice represents. */
};

#endif
