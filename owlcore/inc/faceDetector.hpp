#ifndef __faceDetector_hpp
#define __faceDetector_hpp

#include "opencv2/core/core.hpp"
#include "opencv2/objdetect/objdetect.hpp"

#if defined(__ANDROID__) && !defined(OPENCV_FACES)
extern "C"
{
#include "qcff_native.h"
}
#endif

#include <vector>
#include <string>

/** This class is used for locating faces within a provided image.
 *
 * Found faces are represented by a rectangle that defines the size and 
 * position.  The referece frame for the rectangle's location is the
 * search ROI at full-scale image (despite the faces being searched for
 * in a scaled-down image).
 *
 * By default, detected faces that are sufficiently close to one another are 
 * combined into a single representation that encompasses the pair.  If the full
 * set of detected faces are needed they can be retrieved by calling 
 * getRawFaces().
 */ 
class cFaceDetector
{
public:
  // constructors
  cFaceDetector(void);
  
  // destructor
  ~cFaceDetector();

  // action
  std::vector<cv::Rect> detectFaces(cv::Mat *pImage);
  std::vector<cv::Rect> scaleFaces(std::vector<cv::Rect> faces, int scaleFactor);
 
  // setters
  void setSpaceSize(cv::Size spaceSize);

  // getters
  std::vector<cv::Rect> getFaces(cv::Mat * pImage);
  std::vector<cv::Rect> getFaces(void);
  std::vector<cv::Rect> getRawFaces(void);

private:
  // data
  std::vector<cv::Rect> mRawFaces; /**< All the faces found (in search image's
				      frame-of-reference, but at full scale). */
  std::vector<cv::Rect> mFaces;    /**< The faces after close-to-each-other 
				      faces have been combined. */
  cv::CascadeClassifier mFaceCascadeClassifier; /**< OpenCV classifier. */
  cv::Size mSpaceSize; /**< The bounds within the image that can be used. */

#if defined(__ANDROID__) && !defined(OPENCV_FACES)
  // qualcomm face detector variables
  qcff_handle_t mQcffContext;           /**< A handle to the QCFF context. */
  int           mQcffWidth;             /**< width of searched image. */
  int           mQcffHeight;            /**< height of searched image. */
  unsigned char *mpQcffGrayImage;       /**< Copy of grayscale sub-image. */
#endif
  
  // actions
  void detectFacesQc(cv::Mat& image, std::vector<cv::Rect>& faces);
  void simplifyFaces(void);
  bool compareFaces(cv::Rect f1, cv::Rect f2);
  cv::Rect combineFaces(cv::Rect f1, cv::Rect f2);
};

#endif
