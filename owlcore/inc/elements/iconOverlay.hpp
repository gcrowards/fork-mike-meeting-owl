/*
 * iconOverlay.hpp
 *
 *  Created on: Mar 28, 2017
 *
 * Generalized overlay object to be rendered on the image.
 */

#ifndef OWLCORE_INC_ELEMENTS_ICONOVERLAY_HPP_
#define OWLCORE_INC_ELEMENTS_ICONOVERLAY_HPP_

#include <string>

#include "elements/rgbaOverlayBase.hpp"


namespace owl
{
  /** RGBA icon overlay display control object. */
  class cIconOverlay : public cRgbaOverlayBase
  {
  public:
    cIconOverlay(int renderId, std::string fileName);
    ~cIconOverlay();

    /** Enumeration of possible owners of the icon. */
    typedef enum
    {
      NONE,
      STARTUP,
      RUNTIME
    } eIconOwner_t;

    void setOwner(eIconOwner_t owner);
    eIconOwner_t getOwner(void);

    void setIconActive(bool enable);

    bool isActive(void);
    bool isLoaded(void);

    void loadActiveIcon(void);

    void animationControl(int nPanels);

  private:
    uint32_t *mpBuf;     /**< Buffer containing pixels. */
    int mIconId;         /**< ID used to identify GPU resources used. */
    eIconOwner_t mOwner; /**< eIconOwner_t indicating who owns icon. */

    bool mActive;        /**< true if icon is selected for use. */
    bool mLoaded;        /**< true if icon is loaded in GPU texture. */

    bool mAnimating;     /**< true if icon is in midst of animating. */
    int  mAnimationDir;  /**< 1 => beginning to end, -1 => reverse. */
  };

};

#endif /* OWLCORE_INC_ELEMENTS_ICONOVERLAY_HPP_ */
