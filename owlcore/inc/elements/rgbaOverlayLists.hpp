/*
 * rgbaOverlayLists.hpp
 *
 *  Created on: Mar 28, 2017
 *
 * Generalized overlay object to be rendered on the image.
 */

#ifndef OWLCORE_INC_ELEMENTS_RGBAOVERLAYLISTS_HPP_
#define OWLCORE_INC_ELEMENTS_RGBAOVERLAYLISTS_HPP_

#include <opencv/cv.hpp>
#include <vector>

#include "elements/iconOverlay.hpp"
#include "elements/pinnedAoiOverlay.hpp"

class cOverlayManager; // Can't include statusFrame.hpp here - causes trouble.

namespace owl
{
  /* *** cRgbaOverlayLists *** */

  /** Class to hold lists of all the RGBA overlay control objects by type. */
  class cRgbaOverlayLists
  {
  public:
    cRgbaOverlayLists();
    ~cRgbaOverlayLists();

    void setProgressBar(cv::Rect rect, cv::Scalar color);
    cPinnedAoiOverlay *findPinnedAoiOverlayForTexture(int texId);

    void setSuppressOverlays(bool state);
    bool getSuppressOverlays(void);

    std::vector<owl::cPinnedAoiOverlay> mPinnedAoiOverlays; /**< Pinned Aois. */
    std::vector<owl::cIconOverlay *> mIconOverlays;         /**< Icons. */
    cOverlayManager *mpOverlayManager; /**< Overlay manager: status frame,
        * banner, etc. */
    cv::Rect mStartupProgressBar;      /**< Extent of the progress bar. */
    cv::Scalar mStartupProgressColor;  /**< Color of the progress bar. */
    bool mSuppressOverlays;            /**< No graphics drawn when true. */
  };
};

#endif /* OWLCORE_INC_ELEMENTS_RGBAOVERLAYLISTS_HPP_ */
