#ifndef __size_hpp
#define __size_hpp

#include <string>

/** This class is used to represent the size of an object.
 *
 * Size is defined by both an abstract width and an abstract height.
 */
namespace owl
{
  class cSize
  {
  public:
    // constructors
    cSize(void);
    cSize(int widthVal, int heightVal);

    // destructors
    ~cSize();

    // actions
    void print(std::string comment = std::string(""));

    // data
    int width;       /**< Abstract width of an object. */
    int height;      /**< Abstract height of an object. */
  };
}
#endif
