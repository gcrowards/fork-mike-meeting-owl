/*
 * linearAnimate.hpp
 *
 *  Created on: Mar 28, 2017
 *
 * Generalized overlay object to be rendered on the image.
 */

#ifndef OWLCORE_INC_ELEMENTS_LINEARANIMATE_HPP_
#define OWLCORE_INC_ELEMENTS_LINEARANIMATE_HPP_

#include <time.h>


namespace owl
{
  /* *** cLinearAnimate *** */

  /** Class to manage parameters during a linear animation of a bitmap. */
  class cLinearAnimate
  {
  public:
    cLinearAnimate();
    ~cLinearAnimate();

    void setBegin(int left, int top, float hScale, float vScale, float alpha);
    void setEnd (int left, int top, float hScale, float vScale, float alpha);
    void setDuration(float duration);
    void setDirection(int direction);
    void update(int &left, int &top, float &hScale, float &vScale, float &alpha);
    void start(void);
    void reset(void);
    bool animating(void);

  private:
    float mDuration;     /**< Duration of the animation. */
    int   mDir;          /**< Animation direction, 1=start->end, -1 reverse. */
    int   mBeginLeft;    /**< Left of upper left of bitmap at start. */
    int   mBeginTop;     /**< Top of upper left of bitmap at start. */
    float mBeginHScale;  /**< Horizontal scale multiplier at start. */
    float mBeginVScale;  /**< Vertical scale multiplier at start. */
    float mBeginAlpha;   /**< Alpha blend value at start. */
    int   mEndLeft;      /**< Left of upper left of bitmap at end. */
    int   mEndTop;       /**< Top of upper left of bitmap at end. */
    float mEndHScale;    /**< Horizontal scale multiplier at end. */
    float mEndVScale;    /**< Vertical scale multiplier at end. */
    float mEndAlpha;     /**< Alpha blend value at end. */
    struct timespec mStartTime; /**< Start time of the animation. */
  };
};

#endif /* OWLCORE_INC_ELEMENTS_LINEARANIMATE_HPP_ */
