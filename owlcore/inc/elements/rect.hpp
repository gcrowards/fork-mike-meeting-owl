#ifndef __rect_hpp
#define __rect_hpp

#include <string>
#include "elements/shape.hpp"
#include "elements/size.hpp"
#include "elements/point.hpp"

/** This class is used to represent a rectangle.
 *
 * The rectangle is defined by the coordinates of its top-left corner, and
 * its dimensions (width & height).
 */
namespace owl
{
  class cRect : public abcShape
  {
  public:
    // constructors
    cRect(void);
    cRect(int xVal, int yVal, int widthVal, int heightVal);
    cRect(int xVal, int yVal, owl::cSize size);
    cRect(owl::cPoint point, int widthVal, int heightVal);
    cRect(owl::cPoint point, owl::cSize size);

    // destructors
    ~cRect();

    // actions
    void draw(cGpu *gpu);
    void print(std::string comment = std::string(""));

    // data
    int x;      /**< Coordinate of the left edge of the rectangle. */
    int y;      /**< Coordinate of top edge of the rectangle. */
    int width;  /**< Width of rectangle. */
    int height; /**< Height of rectangle. */
  };
}
#endif
