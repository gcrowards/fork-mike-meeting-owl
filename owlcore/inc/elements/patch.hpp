#ifndef __patch_hpp
#define __patch_hpp

#include "common.hpp"

#include <string>

/** This class is used to geometrically define a patch, which represents a region
 * in 3D space with spherical polar coordinates minus the radius component. The
 * focus/center of the patch is defined by the bearing and elevation angles
 * (theta and phi respectively) and the angular width and height of the patch
 * (dTheta and dPhi respectively).
 *
 * In order to illustrate the intended use of the patch, imagine an infinitely
 * small camera in space. Assume the camera imager is rectangular and is level
 * with the horizon. One could describe the direction it points by the bearing
 * and elevation angles (patch's theta and phi) and could describe view of the
 * camera by the horizontal and vertical viewing angles (patch's dTheta and dPhi)
 * dP. The radius is not necessary to describe the region of space that the
 * camera sees.
 */
namespace owl
{
  class cPatch
  {
  public:
    // constructors
    cPatch(void);
    cPatch(int thetaVal, int phiVal, int dThetaVal, int dPhiVal);

    // destructors
    ~cPatch();

    // operators
    bool operator==(const owl::cPatch &other) const;
    bool operator!=(const owl::cPatch &other) const;

    // actions
    bool isEmpty(void);
    bool isOverlapped(owl::cPatch testPatch);
    bool isFullOverlap(owl::cPatch testPatch);
    owl::cPatch findDifference(owl::cPatch testPatch);
    owl::cPatch findSum(owl::cPatch testPatch);
    owl::cPatch findSeparation(owl::cPatch testPatch);
    owl::cPatch findOverlap(owl::cPatch testPatch);
    bool contains(int testTheta);
    void print(std::string comment = std::string(""));

    // setters
    void setPatch(int theta0, int theta1, int phi0, int phi1);

    // getters
    cv::Size getSizeInStagePix(void);

    // data
    int theta;   /**< Bearing angle of the patch's center (deg). */
    int phi;     /**< Elevation angle of the patch's center (deg). */
    int dTheta;  /**< Angular width of the patch (deg). */
    int dPhi;    /**< Angular height of the patch (deg). */

  private:
    // actions
    void calcArcThetaParams(owl::cPatch testPatch, int *pZeroOffset, 
			    int *pB, int *pC, int *pD );
  };
}
#endif
