#ifndef __shape_hpp
#define __shape_hpp

#include <string>

class cGpu; // forward declaration since gpu.hpp includes shape.hpp

/** This class is a parent class for representing shapes, like a
 * rectangle or ellipse.
 */
namespace owl
{
  class abcShape
  {
  public:
    // actions
    virtual void draw(cGpu *gpu) = 0;
    void setColor(float r, float g, float b, float a = 1.0);
    void setFilled(bool filledVal);
    void setLineWidth(float lineWidthVal);
    virtual void print(std::string comment = std::string("")) = 0;

    // data
    bool  filled = false; /**< true if shape is filled; false for outline. */
    float red   = 1.; /**< red component of shape color in [0., 1.]. */
    float green = 1.; /**< green component of shape color in [0., 1.]. */
    float blue  = 1.; /**< blue component of shape color in [0., 1.]. */
    float alpha = 1.; /**< alpha component of shape color in [0., 1.]. */
    float lineWidth = 2.; /**< width of line or outline, if used. */
  };


}
#endif
