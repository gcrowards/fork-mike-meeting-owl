#ifndef __line_hpp
#define __line_hpp

#include <string>
#include "elements/shape.hpp"
#include "elements/point.hpp"

/** This class is used to represent a line.
 *
 * The line is defined by the coordinates of its two endpoints.
 */
namespace owl
{
  class cLine : public abcShape
  {
  public:
    // constructors
    cLine(void);
    cLine(int x0Val, int y0Val, int x1Val, int y1Val);
    cLine(owl::cPoint point0, owl::cPoint point1);

    // destructors
    ~cLine();

    // actions
    void draw(cGpu *gpu);
    void print(std::string comment = std::string(""));

    // data
    int x0;      /**< x coordinate of the first endpoint of the line. */
    int y0;      /**< y coordinate of the first endpoint of the line. */
    int x1;      /**< x coordinate of the second endpoint of the line. */
    int y1;      /**< y coordinate of the second endpoint of the line. */
  };
}
#endif
