/** Copyright 2017 Owl Labs Inc.
 *  All rights reserved.
 *
 *  elements/bufferGraph.hpp
 *  This is the header file for cBufferGraph.
 *   --> Overlays graph data to the screen using VBOs.
 */

#ifndef BUFFER_GRAPH_HPP
#define BUFFER_GRAPH_HPP

#include <vector>
#ifndef EGL_EGLEXT_PROTOTYPES
#define EGL_EGLEXT_PROTOTYPES
#endif

#ifndef GL_GLEXT_PROTOTYPES
#define GL_GLEXT_PROTOTYPES
#endif

#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES3/gl3.h>
#include <GLES3/gl3ext.h>

#include "elements/shape.hpp"
#include "elements/vertex.hpp"

namespace owl
{
  class cBufferGraph : public abcShape
  {
  public:
    cBufferGraph(const std::vector<sVertex> *pVertices, GLuint colorUniform);
    ~cBufferGraph();
    
    void setVertexBuffer(const std::vector<sVertex> *pVertices);
    virtual void draw(cGpu *pGpu) override;
    virtual void print(std::string comment = "") override;

  private:
    bool mInitialized = false;  /**< Initialization flag. */
    bool mRender = false;       /**< When true, cGpu will render this graph. */

    GLuint mColorUniform;       /**< Id of the color uniform in the graph shader. */
    GLuint mVBuffer = 0;        /**< Id of the VBO. */
    
    const std::vector<sVertex> *mpVertices = nullptr; /**< pointer to graph data. */
    
    void init();
    void cleanup();
  };
}

#endif //BUFFER_GRAPH_HPP
