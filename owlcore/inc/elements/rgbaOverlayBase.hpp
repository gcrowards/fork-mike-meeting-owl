/*
 * rgbaOverlayBase.hpp
 *
 *  Created on: Mar 28, 2017
 *
 * Generalized overlay object to be rendered on the image.
 */

#ifndef OWLCORE_INC_ELEMENTS_RGBAOVERLAYBASE_HPP_
#define OWLCORE_INC_ELEMENTS_RGBAOVERLAYBASE_HPP_

#include "elements/linearAnimate.hpp"

class cGpu;  // Forward declaration


namespace owl
{
  /* *** cRgbaOverlayBase *** */

  /** Base class for all RGBA overlay bitmap control object types. */
  class cRgbaOverlayBase
  {
  public:
    cRgbaOverlayBase(int renderId, int width, int height);
    cRgbaOverlayBase(int renderId);
    ~cRgbaOverlayBase();

    void render();
    void setGpu(cGpu *pGpu);
    void setVisible(bool turnOn);
    bool getVisible(void);
    int getRenderId(void);
    void getSize(int &width, int &height);
    cLinearAnimate *getAnimationPtr();

  protected:
    cLinearAnimate mAnimation; /**< Animation control structure. */
    int   mWidth;     /**< Native width of the bitmap. */
    int   mHeight;    /**< Native height of the bitmap. */
    cGpu *mpGpu;      /**< Pointer to the GPU object for rendering. */

  private:
    bool  mVisible;   /**< Render the overlay if true, otherwise, don't. */
    int   mRenderId;  /**< Maps to texture unit that contains bitmap. */
  };
};

#endif /* OWLCORE_INC_ELEMENTS_RGBAOVERLAYBASE_HPP_ */
