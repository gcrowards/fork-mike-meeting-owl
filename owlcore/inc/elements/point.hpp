#ifndef __point_hpp
#define __point_hpp

#include <string>

/** This class is used to represent a point.
 *
 * The point is defined by its coordinates (x & y).
 */
namespace owl
{
  class cPoint
  {
  public:
    // constructors
    cPoint(void);
    cPoint(int xVal, int yVal);

    // destructors
    ~cPoint();

    // actions
    void print(std::string comment = std::string(""));

    // data
    int x;      /**< The x coordinate of the point. */
    int y;      /**< The y coordinate of the point. */
  };
}
#endif
