#ifndef VERTEX_HPP
#define VERTEX_HPP

#include <GLES2/gl2.h>

/** This structure is used to store 2D vertices in a buffer for use on the GPU.
 *  It contains only the necessary data (the x and y coordinates) for memory
 *   efficiency.
 */
struct sVertex
{
  sVertex(GLfloat vx = 0.0f, GLfloat vy = 0.0f)
    : x(vx), y(vy)
  { }
  
  GLfloat x;
  GLfloat y;
};

/** This structure is used to store a color in a buffer for use on the GPU.
 *  It contains only the necessary data (the red, green, blue, and alpha
 *   color components) for memory efficiency.
 */
struct sColor
{
  sColor(GLfloat cr = 0.0f, GLfloat cg = 0.0f, GLfloat cb = 0.0f, GLfloat ca = 1.0f)
    : r(cr), g(cg), b(cb), a(ca)
  { }
  
  GLfloat r;
  GLfloat g;
  GLfloat b;
  GLfloat a;
};

#endif // VERTEX_HPP
