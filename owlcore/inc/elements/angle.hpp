#ifndef __angle_hpp
#define __angle_hpp

/** This class is intended to make working with angles that range from 0 
 * degrees to 360 degrees (where 0 == 360) easier to work with.
 *
 * The primary purpose of using cAngle is that the addition and subtraction
 * operators will keep the value within the [0..360) range.  
 *
 * TODO: Add additional functionality in from circleMath module.
 */
namespace owl
{
  class cAngle
  {
  public:
    // constructors
    cAngle(float angle);

    // destructors
    ~cAngle();

    // operators
    void operator=(const owl::cAngle &other);
    void operator=(float val);

    owl::cAngle operator+(const owl::cAngle &other) const;
    owl::cAngle operator-(const owl::cAngle &other) const;

    bool operator==(const owl::cAngle &other) const;
    bool operator!=(const owl::cAngle &other) const;
    
    // actions
    
    // getters
    
    // data
    float val;   /**< This cAngle's value. */
    
  private:

  };
}
#endif
