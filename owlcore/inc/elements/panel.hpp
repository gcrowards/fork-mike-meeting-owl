#ifndef __panel_hpp
#define __panel_hpp

#include <string>

/** This class is used to represent a panel, which defines the geometric 
 * mapping of a polar input system to a cartesian output system.
 *
 * The panel is defined by input source parameters (rInner, rOuter)
 * and (theta0 theta1) which define the polar coordinates of a two
 * dimensional arc, and output parameters (x0, y0) and (x1, y1) that
 * are the two opposing corners of a rectangle to map to.
 */
namespace owl
{
  class cPanel
  {
  public:
    // constructors
    cPanel(void);
    cPanel(int posX0Val, int posY0Val, int posX1Val, int posY1Val, 
	   float theta0Val, float theta1Val, float radiusInnerVal, 
	   float radiusOuterVal );

    // destructors
    ~cPanel();

    // actions
    void print(std::string comment = std::string(""));

    // data
    int posX0;       /**< x-coord of output rectangle's top-left corner. */
    int posY0;       /**< y-coord of output rectangle's top-left corner. */
    int posX1;       /**< x-coord of output rectangle's bottom-right corner. */
    int posY1;       /**< y-coord of output rectangle's bottom-right corner. */

    float theta0;      /**< start angle (radians). */
    float theta1;      /**< end angle (radians). */
    float radiusInner; /**< inner radius in [0.0, 0.5]. */
    float radiusOuter; /**< outer radius in [0.0, 0.5]. */
  };


}
#endif
