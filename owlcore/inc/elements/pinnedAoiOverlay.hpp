/*
 * pinnedAoiOverlay.hpp
 *
 *  Created on: Mar 28, 2017
 *
 * Generalized overlay object to be rendered on the image.
 */

#ifndef OWLCORE_INC_ELEMENTS_PINNEDAOIOVERLAY_HPP_
#define OWLCORE_INC_ELEMENTS_PINNEDAOIOVERLAY_HPP_

#include "elements/rgbaOverlayBase.hpp"


namespace owl
{
  /* *** cPinnedAoiOverlay *** */

  /** Pinned Aoi overlay display control object. */
  class cPinnedAoiOverlay : public owl::cRgbaOverlayBase
  {
  public:
    cPinnedAoiOverlay(int renderId, int width, int height);
    ~cPinnedAoiOverlay();

    void setAoiId(int pinnedAoiId);
    int  getAoiId(void);

  private:
    int mPinnedAoiId;
  };
};

#endif /* OWLCORE_INC_ELEMENTS_PINNEDAOIOVERLAY_HPP_ */
