#ifndef __ellipse_hpp
#define __ellipse_hpp

#include <string>
#include "elements/shape.hpp"
#include "elements/size.hpp"
#include "elements/point.hpp"

/** This class is used to represent an ellipse.
 *
 * The ellipse is defined by the coordinates of its center and
 * its dimensions (width & height).
 */
namespace owl
{
  class cEllipse : public abcShape
  {
  public:
    // constructors
    cEllipse(void);
    cEllipse(int xVal, int yVal, int widthVal, int heightVal);
    cEllipse(int xVal, int yVal, owl::cSize size);
    cEllipse(owl::cPoint point, int widthVal, int heightVal);
    cEllipse(owl::cPoint point, owl::cSize size);

    // destructors
    ~cEllipse();

    // actions
    void draw(cGpu *gpu);
    void print(std::string comment = std::string(""));

    // data
    int x;      /**< x coordinate of the center of the ellipse. */
    int y;      /**< y coordinate of the center of the ellipse. */
    int width;  /**< Width of ellipse. */
    int height; /**< Height of ellipse. */
  };
}
#endif
