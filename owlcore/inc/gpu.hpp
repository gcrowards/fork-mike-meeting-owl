#ifndef _GPU_HPP_
#define _GPU_HPP_

#include <vector>

#ifndef EGL_EGLEXT_PROTOTYPES
#define EGL_EGLEXT_PROTOTYPES
#endif

#ifndef GL_GLEXT_PROTOTYPES
#define GL_GLEXT_PROTOTYPES
#endif

#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

#ifdef __ANDROID__
#include "libOwlPG.h"
#endif

#include "elements/panel.hpp"
#include "elements/rgbaOverlayLists.hpp"
#include "elements/shape.hpp"
#include "elements/bufferGraph.hpp"

// render constants hard-coded in the fragment shader and used by
// symbolic name in the C++ code in gpu.cpp; be sure to update the fragment
// shader if any of these change
#define RENDER_UNROLL                0
#define RENDER_LOGO                  1
#define RENDER_RGBA_OVERLAY          2
#define RENDER_AOI_PIN               3
#define RENDER_SHAPE                 4
#define RENDER_SHAPE_ELLIPSE         5


/** Typedef for the codec function that returns an EGLNativeWindow. */
typedef EGLNativeWindowType (*codecNativeWindowFnType)();


class cGpu
{
public:
  cGpu(codecNativeWindowFnType nativeWindowFn);
  cGpu(EGLNativeDisplayType nativeDisplay,
       EGLNativeWindowType nativeWindow);
  ~cGpu();

  void   clearRgbaOverlay(void);
  void   copyOutput(unsigned char *pDst);
  void   setCameraCenter(float xNormalized, float yNormalized);
  void   setLogo(unsigned char *pImageBuf, int width, int height);
  void   setRgbaOverlay(unsigned char *pImageBuf, int x, int y,
			int width, int height,
			int renderWidth, int renderHeight, float alpha);
  void   setRgbaTexture(int texId, unsigned char *pImageBuf,
                        int width, int height);
  void   update(void *pEglClientBuffer);
  void   update(void *pEglClientBuffer, std::vector<owl::cPanel> panels,
		std::vector<owl::abcShape*> shapes,
		std::vector<owl::cBufferGraph*> graphs,
		owl::cRgbaOverlayLists *pRgbaOverlays);

  // public shape rendering methods
  void   renderEllipse(int x, int y, int width, int height,
		       float r, float g, float b, float alpha,
		       float lineWidth, bool filled);
  void   renderLine(int x0, int y0, int x1, int y1,
		    float r, float g, float b, float alpha,
		    float lineWidth);
  void   renderRectangle(int left, int top, int width, int height,
			 float r, float g, float b, float alpha,
			 float lineWidth, bool filled);

  void   setSystemTestMode(bool testModeOn);
  bool   getSystemTestMode(void);
  void   renderRgba(int renderType, int left, int top,
                    int width, int height, float alpha);

  GLuint getGraphColorUniform() const;
  
  void   saveNextFisheye(void);


private:
  EGLNativeDisplayType mNativeDisplay = NULL; /**< X11 display for Linux. */
  EGLNativeWindowType  mNativeWindow  = 0;    /**< X11 window  for Linux. */
  EGLDisplay mDisplay; /**< EGL boilerplate variable. */
  EGLSurface mSurface; /**< EGL boilerplate variable. */
  EGLContext mContext; /**< EGL boilerplate variable. */
  GLuint     mQuadVboId;  /**< ID of the quad vertex buffer. */
  GLuint     mOwlProgram; /**< EGL boilerplate variable. */
  GLuint     mGraphProgram; /**< EGL boilerplate variable. */
  int        mOutWidth;  /**< width of output frame. */
  int        mOutHeight; /**< height of output frame. */
  GLfloat    *mpVertexes; /**< buffer of vertex coordinates for shader quad. */
  codecNativeWindowFnType   mGetNativeWindowFn = NULL; /**< callback to get
							  native window. */
  GLuint     mCvPanoFbo; /**< offscreen FBO for panoramic strip. */
  GLuint     mCameraTexId; /**< texture id of camera image. */
  GLuint     mCvPanoFboTexId; /**< texture id of panoramic strip FBO. */
  GLuint     mLogoTexId;   /**< texture id of logo RGBA image. */
  int        mLogoWidth;  /**< logo width. */
  int        mLogoHeight; /**< logo height. */
  GLuint     mRgbaOverlayTexId; /**< texture id of RGBA overlay image. */
  int        mRgbaOverlayX; /**< RGBA overlay horizontal coordinate. */
  int        mRgbaOverlayY; /**< RGBA overlay vetical coordinate. */
  int        mRgbaOverlayWidth;  /**< RGBA overlay input width. */
  int        mRgbaOverlayHeight; /**< RGBA overlay input height. */
  int        mRgbaOverlayRenderWidth; /**< RGBA overlay output width. */
  int        mRgbaOverlayRenderHeight; /**< RGBA overlay output height. */
  float      mRgbaOverlayAlpha; /**< RGBA overlay transparency. */
  GLuint     mAoiPinTexId;

  bool       mSystemTestMode; /**< System test mode set by argv. */
  GLuint     mGraphColorUniform; /**< ID of the color uniform in graph shader. */
  
  bool       mSaveFisheye; /**< Set true to save next fisheye input frame */

  void   animateLogo(int nPanels, int *pX, int *pY, float *pScale);
  int    checkEglError(const char *pMsg);
  int    checkGlError(const char *pMsg);
  GLuint createProgram(const GLchar *pvShaderSrc,
		       const GLchar *pfShaderSrc );
  void   finish(void);
  int    loadShader(int shaderType, const GLchar *pShaderStr);
  void   prepareAttributes(void);
  void   prepareCvPanoFbo(void);
  void   prepareEgl(void);
  void   prepareTextures(void);
  void   prepareUniforms(void);
  void   prepareVertexes(void);
  void   prepareViewport(void);
  void   renderFrame(std::vector<owl::cPanel> panels,
		     std::vector<owl::abcShape*> shapes,
		     std::vector<owl::cBufferGraph*> graphs,
		     owl::cRgbaOverlayLists *pOverlays);
  void   setBackgroundColor(void);
  void   setBlending(void);
  void   setupGraphics(void);

  // panel and RGBA overlay rendering methods
  void   renderRgbaOverlay(int left, int top, int width, int height,
			   float alpha);
  void   renderRgbaLogo(int left, int top, int width, int height, float alpha);
  void   renderUnroll(int left, int top, int width, int height,
		      float theta0, float theta1, float r0, float r1,
		      float alpha, float stretch, float bend);
};
  
#endif // _GPU_HPP_
