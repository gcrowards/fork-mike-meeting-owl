#ifndef __layout_hpp
#define __layout_hpp

#include "subframe.hpp"

#include "opencv2/core/core.hpp"

#include <vector>

#define NUM_STAGE_SLOTS 3

/** This class determines how the various active cSubframe objects should be
 * laid out for display.
 *
 * When the layout is changed (e.g. a cSubframe is added or removed) the new
 * end positions (and/or sizes, etc.) within each cSubframe is updated so that
 * each knows how to animate itself properly.
 */
class cLayout
{
public:
  // constructors
  cLayout(void);

  // destructors
  ~cLayout();

  // actions
  void addToBase(cSubframe *pSubframe);
  bool removeFromBase(cSubframe *pSubframe);

  void addToStage(cSubframe *pSubframe);
  bool removeFromStage(cSubframe *pSubframe);
  void clearStage(void);

  bool checkSpaceFor(cSubframe *pSubframe);
  cv::Size findReclaimableSpace(void);
  void preventOverlap(void);
  void refitStage(void);
  
  // setters
  void setStageArea(cv::Rect workArea);
  
  // getters
  int getLayoutCount(void);


private:
  //data
  std::vector<cSubframe *> mpBaseLayout; /**< List of refs to the cSubframe
					    objects that are part of the static
					    Layout (e.g. panoramic strip). */
  std::vector<cSubframe *> mpStageLayout; /**< List of refs to the cSubframe
					     objects that are part of the dynamic
					     "stage" (where close-up views come
					     on and off the screen). */
  cv::Rect mStageArea; /**< Rectangle defining the part of the output frame where
			  close-up views are placed. */
  
  // actions
  void addSubframeToBase(cSubframe *pAddFrame);
  bool removeSubframeFromBase(cSubframe *pRemoveFrame);
  
  void addSubframeToStage(cSubframe *pAddFrame);
  void removeSubframeFromStage(cSubframe *pRemoveFrame, int removeIndex);
  void fitSubframes(std::vector<cSubframe *> framesToFit);
  void getOrderedValidFrames(std::vector<cSubframe *> &framesToFit,
                             bool unlockedOnly = false);

  typedef enum {
    LEFT = -1,
    MIDDLE = 0,
    RIGHT = 1
  } eInsertionLocation_t;
  eInsertionLocation_t getInsertionLocation(cSubframe *pAddFrame);

  // debug helpers
  int countStageLayoutSubframes(void);
  void printLayoutSize(void);
};

#endif
