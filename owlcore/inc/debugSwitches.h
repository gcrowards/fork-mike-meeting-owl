#ifndef _DEBUG_SWITCHES_H_
#define _DEBUG_SWITCHES_H_

#include "debugManager.hpp"

/** \file
 * This file is used to control which sections of the code will print out debug
 * information while the application is running.
 *
 * Macros that are set to true will cause the associated section of code to 
 * print debugging information during execution.
 *
 * The GLOBAL_DEBUG macro must be set to true or no debug messages will print.
 *
 * Debug print statements should wrapped inside the DEBUG() macro.  Usage 
 * example:
 *
 * if(DEBUG(MIC)) {
 *    printf("debugging mic system!\n"); } 
 */

// The following 2 global switches are updated automatically; do not re-name!
// Also, preserve formatting with spaces instead of tabs, with the operative
// column starting here ----->|
#define GLOBAL_DEBUG           true
#define GENERAL                false

// This is used to record things like CPU temperature to log file
#define SYSTEM_HEALTH_LOG      false

// Enable/disable mock map data
#define MOCK_MAPS_PLAYBACK     false
#define MAPS_RECORD            false

// This list of tools can be enabled (as a group) at runtime 
#define LIVE_DEBUG_TOOLS       (eSoundMapper + eMotionMapper + eFaceMapper + \
                                ePersonInstantItemMap +		             \
                                ePersonLongTermItemMap)

// Console only debug switches
#define AOI                             false
#define ATTENTION_SYSTEM                false
#define FACE_DETECTOR                   false
#define FACE_MAPPER                     false
#define FACE_TRACKER                    false
#define LAYOUT                          false
#define MIC                             false
#define MOTION_MAPPER                   false
#define SOUND_MAPPER                    false
#define PERSON_MAPPER                   false
#define SUBFRAME                        false
#define VISUAL_EDITOR                   false
#define VOICE_TRACKER                   false
#define SOUND_LOCALIZER                 false
#define BEAMFORMER                      false
#define ANALYTICS                       false
#define STATUS_FRAME                    false
#define KEYBOARD_INPUT                  false

// GUI only debug switches
#define AOI_GUI                         false
#define FACE_DETECTOR_GUI               false
#define SOUND_LOCALIZER_GUI             false
#define FACE_MAPPER_GUI                 false
#define FACE_MAPPER_SEARCH_GUI          false
#define FACE_TRACKER_GUI                false
#define MOTION_MAPPER_GUI               false
#define SOUND_MAPPER_GUI                false
#define PERSON_MAPPER_GUI               false
#define PERSON_INSTANT_ITEM_MAP_GUI     false
#define PERSON_LONG_TERM_ITEM_MAP_GUI   false
#define VOICE_TRACKER_GUI               false
#define LOUDSPEAKER_GUI                 false
#define MICROPHONE_GUI                  false
// Enable button sequence to grab a fisheye and store in /data/OwlLabs
#define ENABLE_GRABBING_FISHEYE         false

// Less distracting debugging GUI (shrinks map overlays)
#define GUI_MINIMAL                     false

#define DEBUG(a) ((a) && (GLOBAL_DEBUG) ? (true) : (false))



#endif /* _DEBUG_SWITCHES_H_ */
