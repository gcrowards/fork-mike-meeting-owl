/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  ledContext.hpp
 *  This is the header file for cLedContext.
 *   --> Creates an isolated set of LED states for different Owl modes. Can be
 *        passed to cLedInterface to apply a context to the physical LEDs.
 */

#ifndef LED_CONTEXT_HPP
#define LED_CONTEXT_HPP

#include "ledInterface.hpp"

/** Creates an isolated set of LED states for different Owl modes. Can be passed
 *   to cLedInterface to apply a context to the physical LEDs.  */
class cLedContext
{
public:
  cLedContext(ledType_t initialStates = LEDS_NONE);
  
  void enable(ledType_t leds);
  void disable(ledType_t leds);
  void setLeds(ledType_t leds, bool setEnabled);
  
  void startBlinking(ledType_t leds, int onTimeMs, int offTimeMs);
  void stopBlinking();

  ledType_t getStates() const;
  
private:
  ledType_t mLedStates = LEDS_NONE; /**< the led state in this context. */
  cLedInterface *mpLedInterface = nullptr; /**< a pointer to the cLedInterface
					     interface if this context is active,
					     or nullptr otherwise. this is set
					     directly by cLedInterface when the
					     context becomes active. */

  // make cLedInterface a friend so it can set itself as mpLedInterface when a
  //  context is set. cLedContext needs a pointer to the interface so the LEDs
  //  can be updated immediately when the context's states change.
  friend class cLedInterface;
};


#endif // LED_CONTEXT_HPP
