#ifndef __motionMapper_hpp
#define __motionMapper_hpp

#include <pthread.h>

#include "opencv2/core/core.hpp"

#include "map.hpp"
#include "motionDetector.hpp"
#include "panorama.hpp"

#include "opencv2/core/core.hpp"

#include <vector>

/** This class is responsible for mapping out the locations of people in the
 * room based on recent motion.
 *
 * There is no trigger mechanism, the system just looks through the room 
 * piece-by-piece and builds up a map for other systems to use.  
 */ 
class cMotionMapper
{
public:
  // constructors
  cMotionMapper(cPanorama *pPano);
  
  // destructor
  ~cMotionMapper();

  // actions
  void update(void);
  bool checkForBroadMotion(void);
  void resetMap(void);

  // setters

  // getters
  int getMapValAt(int angle);
  int getAngleWithMaxVal(int nominalAngle, int plusMinus);
  cMap getMap(void);
  
private:
  // data
  cMap mMap; /**< The map keeps track of the positional data of the things 
		found by this mapper. */
  std::vector<cMotionDetector *> mpMotionDetectors; /**< This set of vectors
						     * divides the panorama into
						     * separate sections that are
						     * analyzed for motion. */
  
  cPanorama *mpPano; /**< Source of image data. */
  cv::Mat mGrayImage; /**< Deep copy of panorama gray-scaled image. */

  bool mBroadMotion; /**< flag that indicates broad motion. */


  // actions
  void setupDetectors(int angularResolution); /** Builds vector of 
					       * cMotionDetectors that will be
					       * used to build motion map. */
  void updateBroadMotion(void); /** Update mBroadMotion. */
  
  // thread variables
  pthread_t       mThread; /**< Thread for this object. */
  pthread_mutex_t mMutex;  /**< Mutex for shared resources. */
  bool    mKeepThreading;  /**< Flag to control iteration of thread loop. */

  // thread methods
  static void *threadWrapper(void *pContext); /**< Wrapper for pthread_create.*/
  void join(void);  /** Interface to exit the thread. */
  void start(void); /** Interface to start the thread. */
  void threadUpdate(void); /** Thread update loop. */
};

#endif
