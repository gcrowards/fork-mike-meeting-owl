#ifndef __owl_hpp
#define __owl_hpp

#include <vector>

#include "analytics.hpp"
#include "AudioPlayer.hpp"
#include "blackBox.hpp"
#include "attentionSystem.hpp"
#include "buttonContext.hpp"
#include "debugInfo.hpp"
#include "elements/panel.hpp"
#include "elements/rgbaOverlayLists.hpp"
#include "debugManager.hpp"
#include "faceMapper.hpp"
#include "ledContext.hpp"
#include "mapsPlayer.hpp"
#include "mapsRecorder.hpp"
#include "motionMapper.hpp"
#include "panorama.hpp"
#include "personMapper.hpp"
#include "soundMapper.hpp"
#include "startupExp.hpp"
#include "switchboard/audioParams.hpp"
#include "switchboard/pinnedAoiArgs.hpp"
#include "switchboard/switchboardClient.hpp"
#include "systemHealth.hpp"
#include "virtualMic.hpp"
#include "visualEditor.hpp"
#include "volumeControl.hpp"

// forward declarations
class cSysfs;

/** This class represent the Meeting Owl embodiment.
 *
 *  It provides access to all the data collected by the hardware and performs 
 *  analysis required to build the output video stream. 
 */
class cOwl
{
public:
  // contructors
  cOwl(cVirtualMic *pMic, cSysfs *pSysfs);
  // destructors
  ~cOwl();

  // constants
  static const double kBluetoothHoldTime; /** time holding the bluetooth button
					      before debug visualizations will
					      show (in seconds). */
  
  // actions
  std::vector<owl::cPanel> update(unsigned char *pSmPanoramic,
                                  owl::cRgbaOverlayLists *pOverlays,
                                  audioChannelStatus_t mSpeakerStatus,
                                  audioChannelStatus_t mMicStatus,
                                  float fps );
  void setAnalytics(bool inMeeting, bool probablyInMeeting);
  cAnalytics *getAnalyticsObject(void);

  // accessors
  bool isMuted() const;
  cLedContext* getLedContext();
  const cButtonContext* getButtonContext() const;
  bool grabFisheye();

private:
  // data
  bool mWarmedUp;             /**< Flag to indicate that system is ready to start
			       * the normal processing loop. */
  bool mInCall;                /**< Indicates if call is active (streaming). */
  bool mProbablyInCall;        /**< Like mInCall, but includes starting state
                                * too. */
  bool mDebugMode;             /**< true when runtime debug enabled. */
  bool mMuteMode;              /**< true when the owl is muted. */
  bool mBluetoothMode;         /**< true when bluetooth mode is active. */

  bool mGrabNextFisheye;       /**< Flag to grab next received fisheye image */

  struct timespec mLaunchTime; /**< Time at which class was created */ 
  time_t mConnectTime;         /**< Time when a UVC video stream connection
                                * was established. */
  cPanorama mPanorama;         /**< Panoramic data source. */

  cButtonContext mButtonContext; /**< owl-specific context for button bindings. */
  cLedContext mLedContext; /**< owl-specific context for led states. */
  cVolumeControl *mpVolumeControl; /**< continous volume adjustment handler. */
  cSysfs *mpSysfs;             /**< interface to sysfs variables. */

  cVirtualMic *mpVirtualMic;   /**< Audio data source (may be be comprised of a
				  number of physical sensors (or just one)). */
  cAttentionSystem mAttentionSystem; /**< Figures out what the system needs to
  					pay attention to. */
  cFaceMapper *mpFaceMapper; /**< Keeps track of where all the people's faces
  			      * are in the room. */
  cMotionMapper *mpMotionMapper; /**< Keeps track of where motion indicates that
  				  * there may be people. */
  cSoundMapper *mpSoundMapper; /**< Keeps track of where sound has been coming.*/
  cPersonMapper *mpPersonMapper; /**< Keeps track of where there's
  				    combined evidence of people. */
  cVisualEditor mVisualEditor; /**< Where the output video frame is built. */
  cBlackBox  *mpBlackBox;      /**< Black box object. */
  cAnalytics *mpAnalytics;     /**< Analytics object used to collect and report
                                * system performance. */
  cStartupExperience *mpStartupExperience; /**< Controls startup UI sequence. */
  cSystemHealth *mpSystemHealth; /**< Logs to disk info like CPU temperature,
				  * throttle level, etc. */
  cDebugManager mDebugManager;  /**< Manages which runtime debug tools are on .*/
  cSwitchboardServer *mpSwitchboardServer; /**< Pointer to the server object. */
  cSwitchboardClient *mpSwitchboardClient; /**< Pointer to the client object. */
  cPinnedAoiArgs mPinnedAoiArgs;           /**< Most recently received set of
                                            * command and arguments for
                                            * controlling pinned AOIs. */
  cAudioPlayer mAudioPlayer;    /**< Audio file player object. */
  cMapsPlayer mMapsPlayer;      /**< Feeds in person map & voice bearing data 
				 * as inputs for general testing & debugging. */
  cMapsRecorder mMapsRecorder;  /**< Saves person and voice map data to csv
				 * file (to be used w/ cMapsPlayer for debug). */
  cDebugInfo *mpDebugInfo;      /**< Object manages debug info transfer out. */
  
  // internal actions
  void resetAllMaps(void);
  void setMute(bool mute);

  // button callbacks
  void muteButtonCallback();
  void owlButtonCallback();
  void debugVisualizationButtonCallback();
  void debugGrabFisheyeButtonCallback(void);

  void switchboardClientErrorCallback(eOipcMessageType type,
                                      const char *pMsg,
                                      int msgLen);
  void switchboardDebugCommandCallback(eOipcMessageType type,
                                       const char *pMsg,
                                       int msgLen);
  void switchboardPinnedAoiMessageCallback(eOipcMessageType type,
                                           const char *pMsg,
                                           int msgLen);
  void pinnedAoiReply(std::vector<cAreaOfInterest *> *pAois);
  void switchboardPanoShiftAngleGetCallback(eOipcMessageType type,
                                            const char *pMsg,
                                            int msgLen );
  void switchboardPanoShiftAngleSetCallback(eOipcMessageType type,
                                            const char *pMsg,
                                            int msgLen );
  void switchboardUiActionCallback(eOipcMessageType type,
				   const char *pMsg,
				   int msgLen );
  void switchboardMeetingInfoQueryCallback(eOipcMessageType type,
					   const char *pMsg,
					   int msgLen );
  void switchboardSetupProgressSetCallback(eOipcMessageType type,
                                        const char *pMsg,
                                        int msgLen);
  void switchboardSetupProgressGetCallback(eOipcMessageType type,
                                        const char *pMsg,
                                        int msgLen);
  void switchboardFactoryResetCallback(eOipcMessageType type,
                                       const char *pMsg,
                                       int msgLen);
  void switchboardAudioParametersCallback(eOipcMessageType type,
                                          const char *pMsg,
                                          int msgLen);
  void switchboardVideoParameterCallback(eOipcMessageType type,
                                         const char *pMsg,
                                         int msgLen);
  void switchboardDebugInfoCallback(eOipcMessageType type,
                                             const char *pMsg,
                                             int msgLen);

  void sendSetupProgressMsg(void);
  void sendAudioParametersMsg(void);
  void sendVideoParameterMsg(const char *pKey);
};

#endif
