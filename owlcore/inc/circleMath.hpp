#ifndef __circleMath_hpp
#define __circleMath_hpp

#include <vector>

#include "opencv2/core/core.hpp"

/** This namespace contains math functions related to angles that run on a 360
 * degree range where 360 and 0 represent the same angle. */
namespace circleMath
{
  int intAddAngs(int ang1, int ang2);
  float floatAddAngs(float ang1, float ang2);
  int intSubAngs(int ang1, int ang2);
  float floatSubAngs(float ang1, float ang2);
  int intDiffAngs(int ang1, int ang2);

  int intAdd(int i1, int i2, int iMax, int iMin = 0);
  float floatAdd(float i1, float i2, float iMax, float iMin = 0.);

  int intSub(int i1, int i2, int iMax, int iMin = 0);
  float floatSub(float f1, float f2, float fMax, float fMin);
  int intDiff(int i1, int i2, int iMax, int iMin = 0);
  float floatDiff(int f1, int f2, int fMax, int fMin = 0.);
  int intAvg(int i1, int i2, int iMax, int iMin = 0);

  bool isPointInRect(cv::Point point, cv::Rect rect, int iMax, int iMin = 0);

  std::vector<cv::Rect> cropRect(cv::Size src, cv::Rect subRect, 
				 bool clip = false );
}

#endif
