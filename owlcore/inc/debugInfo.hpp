/*
 * debugInfo.hpp
 *
 *  Created on: Apr 6, 2018
 *      Author: tom
 */

#ifndef OWLCORE_INC_DEBUGINFO_HPP_
#define OWLCORE_INC_DEBUGINFO_HPP_

#include "switchboard/switchboardClient.hpp"
#include "thread.hpp"

/** Class that manages running debug shell commands and then sending the
 * resultant text output to the switchboard. Supported commands are
 * currently 'dmesg', and 'logcat', and a facility is provided to send
 * filters to the 'logcat' command to decrease the volume of data.
 */
class cDebugInfo
{
private:
  cDebugInfo();

public:
  cDebugInfo(cSwitchboardClient *pSwitchboardClient,
                  std::string command,
                  std::string params);
  ~cDebugInfo();

  bool done();

  /** Structure containing information about the impending data transfer. */
  typedef struct {
    uint32_t mDebugInfoLength;
  } transferHeader_t;

private:
  cSwitchboardClient *mpSwitchboardClient;  /**< Switchboard client for writes*/
  std::string mCommand;          /**< Debugging command to run. */
  std::string mParams;           /**< Extra parameters to send to command. */

  cThread mThread;               /**< Thread that runs the command. */
  bool mDone;                    /**< false while operation is running. */

  void runIt(void);
};

#endif /* OWLCORE_INC_DEBUGINFO_HPP_ */
