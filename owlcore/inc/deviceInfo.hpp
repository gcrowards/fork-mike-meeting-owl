/** Copyright 2017 Owl Labs Inc.
 *  All rights reserved.
 *
 *  deviceInfo.hpp
 *  This is the header file for cDeviceInfo.
 *   --> Static class that provides global access to information about the
 *        physical owl running this software, including serial numbers and MAC
 *        addresses.
 */

#ifndef DEVICE_INFO_HPP
#define DEVICE_INFO_HPP

#include <string>

class cDeviceInfo
{
public:
  static std::string getHardwareSerial();
  static std::string getSoftwareSerial();
  static std::string getPcbaSerial();
  static std::string getRadioVersion();
  static std::string getWifiMacAddress();
  static std::string getBluetoothMacAddress();
  
private:
  // constructor is private to prevent class instantiation
  cDeviceInfo() { };

  static std::string retrieveHwSerial();
  static std::string retrieveSwSerial();
  static std::string retrievePcbaSerial();
  static std::string retrieveRadioVersion();
  static std::string retrieveWifiMacAddr();
  static std::string retrieveBtMacAddr();

  static const std::string mkHwSerialStr; /**< the hardware serial number. */
  static const std::string mkSwSerialStr; /**< the software serial number. */
  static const std::string mkPcbaSerialStr; /**< the main board PCBA serial
					       number. */
  static const std::string mkRadioVersionStr; /**< the radio version number. */
  static const std::string mkWifiMacAddrStr; /**< the wifi MAC address. */
  static const std::string mkBtMacAddrStr; /**< the bluetooth MAC address. */
};


#endif // DEVICE_INFO_HPP
