#ifndef __ringBuffer_hpp
#define __ringBuffer_hpp

#include <cstddef>
#include <vector>

#define NUM_ELEMENTS_DEFAULT    2  // default ring buffer size

template <typename T>
class cRingBuffer
{
public:
  // constructors
  cRingBuffer(void);
  cRingBuffer(int numElements);
 
  void init(void);
 
  // destuctor
  ~cRingBuffer();
  
  // setters
  void setNumElements(int numElements);
  
  // getters
  T* getElement(int stepsBack =  0);
  bool getBufferFilled(void);
  int getBufferFillCount(void);
  
  // actions
  void addElement(T element);
  T calcSum(void);
  
private:
  // data
  int mNumElements;          /**> size of ring buffer. */
  std::vector <T> *mpBuffer; /**> the buffer. */ 
  int mCurrentIndex;         /**> index of the most recently added item. */
  int mLastIndex;            /**> index of the last buffer spot. */
  int mBufferFillCount;      /**> how many items are currently in buffer. */
  bool mBufferFilled;        /**> true is all buffer spots have items. */
};

/* Template Functions Implementation */

template <typename T>
cRingBuffer <T>::cRingBuffer(void)
{
  init();
  setNumElements(NUM_ELEMENTS_DEFAULT);
}

/** @param numElements is the size of the ring buffer. */
template <typename T>
cRingBuffer <T>::cRingBuffer(int numElements)
{
  init();
  setNumElements(numElements);
}

/** Initialization code called by constructors. */
template <typename T>
void cRingBuffer <T>::init(void)
{
  mpBuffer = nullptr;
  mCurrentIndex = -1;
  mBufferFilled = false;
  mBufferFillCount = 0;
}

template <typename T>
cRingBuffer <T>::~cRingBuffer()
{
  if(mpBuffer != nullptr)
    {delete mpBuffer;}
}

/** Sets the number of elements the buffer can hold.
 *
 * NOTE: If this value is changed after the buffer is in use then the original 
 * buffer will be killed and a clean one started.
 *
 * @param numElements sets the size of the buffer.
 */
template <typename T>
void cRingBuffer <T>::setNumElements(int numElements)
{
  init();
  
  mNumElements = numElements;
  mLastIndex = numElements - 1;
  
  if(mpBuffer != nullptr) {delete mpBuffer;}
  mpBuffer = new std::vector <T> (mNumElements);
}

/** This function adds a new piece of data to the buffer.
 *
 * @param element is the new element to add to the buffer.
 */
template <typename T>
void cRingBuffer <T>::addElement(T element)
{
  // increment index w/ wrapping
  mCurrentIndex++;
  if(mCurrentIndex > mLastIndex) {mCurrentIndex = 0;}

  // add new data
  mpBuffer->at(mCurrentIndex) = element;

  // update data count (if necessary)
  if(mBufferFilled == false)
    {
      mBufferFillCount++;
      if((unsigned int) mBufferFillCount == mpBuffer->size())
        {
          mBufferFilled = true;
        }
    }
}

/** This function finds the sum value of the ring buffer.
 *
 * WARNING: This function assumes that typename T has a + operator!
 *
 * @return sum of the filled in buffer elements. 
 */
template <typename T>
T cRingBuffer <T>::calcSum(void)
{
  T sum = 0;
  
  for(int i = 0; i < mBufferFillCount; i++)
    {sum += mpBuffer->at(i);}
  
  return sum;
}

/** This function retrieves an element from the buffer.
 *
 * @param stepsBack denotes how far back into the buffer's history the desired
 * element is located.
 *
 * @returns a pointer to the requested data.
 */
template <typename T>
T* cRingBuffer <T>::getElement(int stepsBack)
{
  int index = mCurrentIndex - stepsBack;
  T *pElement;

  while(index < 0)
    {
      index += mpBuffer->size();
    }

  pElement = mpBuffer->data() + index;

  return pElement;
}

/** @return true if all the spots in the buffer have data in them. */
template <typename T>
bool cRingBuffer <T>::getBufferFilled(void)
{
  return mBufferFilled;
}

/** @return the number of spots in the buffer that have data in them. */
template <typename T>
int cRingBuffer <T>::getBufferFillCount(void)
{
  return mBufferFillCount;
}

#endif
