/** Copyright 2017 Owl Labs Inc.
 *  All rights reserved.
 *
 *  buildId.hpp
 *  This is the header file for cBuildId.
 *   --> Static class that provides global access the the ID of the current
 *        software build. The build ID is retrieved during initialization of
 *        static members before main() is entered.
 */

#ifndef BUILD_ID_HPP
#define BUILD_ID_HPP

#include <string>

class cBuildId
{
public:
  static std::string getString();
  static bool isValid();
  
private:
  // constructor is private to prevent class instantiation
  cBuildId() { }
  
  static std::string retrieveBuildId();
  static std::string formatBuildId(const std::string &message);

  static const std::string mkInvalidBuildIdStr; /**< the build ID to set if
						   retrieval failed. */
  static const std::string mkBuildIdStr; /**< the current build id. */
};

#endif // BUILD_ID_HPP
