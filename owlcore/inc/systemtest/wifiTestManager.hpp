/** Copyright 2017 Owl Labs Inc.
 *  All rights reserved.
 *
 *  systemtest/wifiTestManager.hpp
 *  This is the header file for cWifiTestManager.
 *   --> Runs WiFi testing for cSystemTest. 
 */

#ifndef WIFI_TEST_MANAGER_HPP
#define WIFI_TEST_MANAGER_HPP

#include <string>
#include "thread.hpp"
#include "systemtest/testCommon.hpp"

/** Runs WiFi testing for cSystemTest. */
class cWifiTestManager
{
public:
  
  static const std::string mkCommandLine;
  static const std::string mkDelims;
  
  cWifiTestManager();
  ~cWifiTestManager();

  bool isComplete() const;
  std::string getStation() const;
  testResult_t getResult() const;

  void startTest(int minSignal);
  void reset();
  
private:
  bool mTestComplete = false;     /**< whether test is complete. */
  testResult_t mTestResult = eTestInactive; /**< test result. */
  std::string mWifiStation = "";  /**< WiFi station data. */
  int mMinSignal = 0;        /**< minimum passing signal level. */
  int mMaxSignal = 0;             /**< maximum signal level for current test. */
  cThread mTestThread; /**< thread to run wifi test process on. */

  void testWifi();
  bool processOutputHandler(const std::string *pLine);
};


#endif // WIFI_TEST_MANAGER_HPP
