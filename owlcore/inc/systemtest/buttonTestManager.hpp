/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  systemtest/buttonTestManager.hpp
 *  This is the header file for cButtonTestManager.
 *   --> Handles the button testing for cSystemTest.
 *       Waits for each button to be pressed -- marks a pass if the correct
 *        button was pressed, or fail if a different button was pressed.
 */

#ifndef BUTTON_TEST_MANAGER_HPP
#define BUTTON_TEST_MANAGER_HPP

#include "buttonHandler.hpp"
#include "systemtest/testCommon.hpp"

enum buttonTest_t
  {
    eButtonTestIdle = -1,
    
    eButtonTestFMute = 0,
    eButtonTestVolUp,
    eButtonTestVolDn,
    eButtonTestRMute,
    eButtonTestBluetooth,
    eButtonTestComplete
  };

/** Handles the button testing for cSystemTest.
 *  Waits for each button to be pressed -- marks a pass if the correct button
 *   was pressed, or fail if a different button was pressed. */
class cButtonTestManager
{
public:
  static const std::array<buttonType_t, N_BUTTONS> mkButtonIds; /**< maps each
								   test to its
								   corresponding
								   button ID. */
  cButtonTestManager();
  
  // control
  void startTesting();
  void reset();
  bool update(buttonType_t button);

  // accessors
  bool isTesting() const;
  bool isComplete() const;
  std::array<testResult_t, N_BUTTONS> getResults() const;
  
private:
  buttonTest_t mCurrentTest = eButtonTestIdle; /**< the current active test. */
  std::array<testResult_t, N_BUTTONS> mResults; /**< the test results for each
						   button. */
};

#endif // BUTTON_TEST_MANAGER_HPP
