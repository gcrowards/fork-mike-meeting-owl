/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  systemtest/systemTest.hpp
 *  This is the header file for cSystemTest.
 *   --> The core manager class for the built-in system test.
 *       Tests the Owl's microphones, speaker, buttons and LEDs, with user input
 *        to confirm. The results are stored in an image and output over UVC so
 *        it can be fully used even after adb is locked.
 */

#ifndef SYSTEM_TEST_HPP
#define SYSTEM_TEST_HPP

#include "AudioPlayer.hpp"
// NOTE: This is necessary due to a conflict in OpenCV.
#ifdef FLOAT
#undef FLOAT
#endif
#include "buttonContext.hpp"
#include "ledContext.hpp"
#include "systemtest/buttonTestManager.hpp"
#include "systemtest/micTestManager.hpp"
#include "systemtest/testCommon.hpp"
#include "systemtest/wifiTestManager.hpp"
#include "opencv2/core.hpp"

#include <vector>
#include <functional>

/** The tests that will be conducted through the system test, in order. */
enum systemTest_t
  {
    eSystemTestIdle = 0,
    
    eSystemTestWifi,
    eSystemTestBluetooth,
    eSystemTestSpeaker,
    eSystemTestLeds,
    eSystemTestButtons,

    eSystemTestComplete,
    eSystemTestCount = eSystemTestComplete
  };

/** The core manager class for the built-in system test.
 *  Tests the Owl's microphones, speaker, buttons and LEDs, with user input to
 *   confirm. The results are stored in an image and output over UVC so it can
 *   be fully used even after adb is locked. */
class cSystemTest
{
public:
  static const int mkWidth; /**< output image width. */
  static const int mkHeight; /**< output image height. */
  static const std::vector<std::string> mkTestPrompts; /**< prompt to show for
							  each test. */
  static const std::string mkNoiseWavPath; /**< path to pink noise wav file. */
  static const std::string mkBlipWavPath; /**< path to volume blip wav file. */
  static const int mkMinPassingSignalStrength; /**< threshold for wifi test. */
  
  cSystemTest();
  ~cSystemTest();

  // control
  void setTesting(bool testing);
  void reset();
  void renderImage(unsigned char *pDataOut);

  // callbacks
  void update();
  void micDataCallback(int16_t *pData, int bufLen);

  // accessors
  bool getTesting() const;
  bool isComplete() const;
  cLedContext* getLedContext();
  const cButtonContext* getButtonContext() const;

private:
  // output image params
  cv::Mat mImage;     /**< image buffer to avoid frame tearing. */
  bool mTesting = false; /**< whether the system is actively testing. */

  // ui
  cButtonContext mButtonContext; /**< context for button event callbacks. */
  cLedContext mLedContext; /**< context for LED states. */

  // testing
  systemTest_t mCurrentTest = eSystemTestIdle; /**< the currently running test. */
  std::vector<std::string> mTestPrompt; /**< prompt text for the current test. */
  std::array<double, N_MICS> mMicLevels; /**< microphone levels (0.0 to 1.0). */
  cButtonTestManager mButtonTestManager; /**< manager to handle the button
					    tests. */
  cWifiTestManager mWifiTestManager; /**< manager for wifi test. */
  cAudioPlayer mWavPlayer; /**< for playing pink noise for the speaker test and
			      volume blips when buttons are pressed. */
  // test results
  testResult_t mSpeakerResult = eTestInactive;/**< speaker test results. */
  testResult_t mBtResult = eTestUnknown;      /**< bluetooth test results. */
  testResult_t mLedResult = eTestInactive;    /**< led test results. */

  void prepareTest(systemTest_t test);
  void finishTest(systemTest_t test);
  void nextTest();

  void buttonClicked(buttonType_t button);
};


#endif // SYSTEM_TEST_HPP
