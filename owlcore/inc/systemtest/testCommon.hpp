/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  systemtest/testCommon.hpp
 *  This header file contains common enumerations for the system test.
 */

#ifndef TEST_COMMON_HPP
#define TEST_COMMON_HPP

/** Result states for a system or mic test. */
enum testResult_t
  {
    eTestUnknown = -1,
    
    eTestPassed = 0,
    eTestFailed,
    eTestRunning,
    eTestInactive
  };

#endif // TEST_COMMON_HPP
