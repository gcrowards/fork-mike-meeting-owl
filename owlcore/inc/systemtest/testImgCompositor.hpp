/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  testImgCompositor.hpp
 *  This is the header file for cTestImgCompositor.
 *   --> Statically handles rendering system test results to an image for
 *        display.
 */

#ifndef TEST_IMG_COMPOSITOR_HPP
#define TEST_IMG_COMPOSITOR_HPP

#include "common.hpp"
#include "params.h"
#include "systemtest/testCommon.hpp"

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"

#include <array>

/** Statically handles rendering system test results to an image for display.
 *  Note that all points and sizes should be relative to the image (0.0 - 1.0).
 */
class cTestImgCompositor
{
public:
  // positioning parameters
  static const int mkPadding;           /**< padding for gui elements (px). */
  static const int mkInnerPadding;      /**< internal text padding (px). */
  static const int mkPanoHeight;        /**< height of the panorama strip (px).*/
  static const int mkOutlineThickness;  /**< thickness of border lines (px). */
  static const int mkFontFace;          /**< the font to render text with. */
  static const int mkFontThickness;     /**< the thickness to render text. */
  static const double mkFontScale;      /**< scaling factor for the font. */
  static const cv::Size mkTextOffset;   /**< offset of text from outline edge. */

  // colors
  static const cv::Scalar mkBadColor;        //RED
  static const cv::Scalar mkGoodColor;       //GREEN
  static const cv::Scalar mkNeutralColor;    //WHITE
  static const cv::Scalar mkAttnColor;       //LIGHT YELLOW
  static const cv::Scalar mkDarkColor;       //GREY
  static const cv::Scalar mkBgColor;         //DARK GREY
  static const cv::Scalar mkTestingColor;    //YELLOW

  static cv::Size textSize(const std::string &text, int *pBaselineOut = nullptr);

  // generic rendering functions
  static cv::Size drawText(cv::Mat &img, cv::Point pos,
			   const std::string &text,
			   cv::Scalar color = mkNeutralColor );
  static cv::Size drawTextLines(cv::Mat &img, cv::Point pos,
				const std::vector<std::string> &lines,
				cv::Scalar color = mkNeutralColor );
  static void drawOutline(cv::Mat &img, cv::Point pos, cv::Size size,
			  bool testActive );
  static void drawMeasureBar(cv::Mat &img, cv::Point pos, cv::Size size,
			     double fill );
  // test rendering functions
  static cv::Size drawTest(cv::Mat &img, cv::Point pos,
			   const std::string &testName, testResult_t result,
			   std::string extraTest = "" );
  static cv::Size drawPrompt(cv::Mat &img, cv::Point pos,
			     const std::vector<std::string> &textLines,
			     cv::Size boxSize = cv::Size(0, 0) );
  static cv::Size drawMicTest(cv::Mat &img, cv::Point pos, 
			      const std::array<double, N_MICS> &micLevels,
			      cv::Size boxSize = cv::Size(0, 0) );
  static cv::Size drawSpeakerTest(cv::Mat &img, cv::Point pos, 
				  testResult_t result, bool active,
				  cv::Size boxSize = cv::Size(0, 0) );
  static cv::Size drawWifiTest(cv::Mat &img, cv::Point pos, 
			       testResult_t result, bool active,
			       const std::string &stationStr,
			       cv::Size boxSize = cv::Size(0, 0) );
  static cv::Size drawBtTest(cv::Mat &img, cv::Point pos, 
			     testResult_t result, bool active,
			     cv::Size boxSize = cv::Size(0, 0) );
  static cv::Size drawButtonTest(cv::Mat &img, cv::Point pos, 
				 const std::array<testResult_t, N_BUTTONS>
				 &buttonResults, bool active,
				 cv::Size boxSize = cv::Size(0, 0) );
  static cv::Size drawLedTest(cv::Mat &img, cv::Point pos, 
			      testResult_t ledResult, bool active,
			      cv::Size boxSize = cv::Size(0, 0) );
private:
  cTestImgCompositor() { }
  
};

#endif // TEST_IMG_COMPOSITOR_HPP
