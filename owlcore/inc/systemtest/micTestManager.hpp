/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  systemtest/micTestManager.hpp
 *  This is the header file for cMicTestManager.
 *   --> Controls the mic testing for cSystemTest.Calibration values are defined
 *        in micTestManager.cpp.
 */

#ifndef MIC_TEST_MANAGER_HPP
#define MIC_TEST_MANAGER_HPP

#include "common.hpp"
#include "params.h"
#include "systemtest/testCommon.hpp"

#include <cstdint>
#include <cmath>
#include <vector>
#include <array>
#include <string>

// forward declarations
class cShellProcess;

/** Handles the mic testing for cSystemTest. Calibration values are defined in
 *   micTestManager.cpp. */
class cMicTestManager
{
private:
  struct sMicTest;
  
public:
  static const std::string mkConfigFilePath; /**< the path of the test
						configuration file to read
						from. */
  static const std::string mkResultsOutputPath; /**< the path of the data file
						   to output the results of the
						   test to. */
  static const int mkMicsPerBoard;      /**< number mics on each board. */
  static const int mkMicSampleRate;     /**< sample rate of the mics. */
  
  static const int mkRecordDelayMs;     /**< ms delay between playing audio and
					   recording. */
  static const int mkTestLengthMs;      /**< ms of mic data to record for each
					   test. */

  static const int mkTestLengthSamples; /**< test length (samples). */
  static const int mkRecordDelaySamples;/**< delay before recording
					   (samples). */
  static const int mkTestBufferSize;    /**< size of each buffer of test mic
					   data (bytes). */
  
  static const double mkNoiseTolerance; /**< radius of each noise test's
					   passing range. */
  
  cMicTestManager();
  ~cMicTestManager();

  // control
  void startTesting();
  void reset();
  void update();
  
  void micDataCallback(int16_t *pData, int bufLen);
  void printTestResult(struct sMicTest &test);
  void printAllTestResults();
  void writeResultsToFile();

  // accessors
  bool isTesting() const;
  bool isComplete() const;
  std::array<bool, N_MICS> getTestResults(int testId);
  std::array<testResult_t, N_MICS> getAllTestResults();
  
  // general tools
  // TODO: Move these to a better location
  static void findRms(const std::vector<int16_t> &audioData,
		      std::array<double, N_MICS> &rmsOut );
  static double rmsToDbfs(double rms);
  
private:

  /** Contains parameters and resources for a single mic test. */
  struct sMicTest
  {
    sMicTest(int testId, const std::string &name,
	     const std::string &wavFilePath, int delay );

    void reset();
    bool isFull() const;

    const int mTestId;        /**< the test's index. */
    std::string mName;        /**< the test's name. */
    std::string mWavFilePath; /**< the path of the wav file to play during
				 the test (empty string means no audio). */
    std::string mAudioCmd;    /**< the command to run to play the test's
				 audio. */
    bool mAnalyzed = false;   /**< whether all data has been recorded and
				 tested. */

    int mDelaySamples = 0;    /**< the number of mic samples to delay
				 before running the test. */
    std::vector<int16_t> mAudioData; /**< recorded test audio data. */
    std::array<double, N_MICS> mLowerLimits; /**< mic low bounds for the range
						test. */
    std::array<double, N_MICS> mUpperLimits; /**< mic high bounds for the range
						test. */
    std::array<double, N_MICS> mRms;  /**< rms values from each mic's recorded
					 data. */
    std::array<double, N_MICS> mDbfs; /**< dBFS values from each mic's recorded
					 data. */
    std::array<bool, N_MICS> mPassed; /**< whether each mic passed this test. */
  };
  
  int mCurrentTest = -1;        /**< index of the currently active test. */
  bool mStarting = false;       /**< set to true when testing has started, but
				   before the first test begins. */
  bool mTestDelayed = false;    /**< whether the current test's delay has been
				   applied. */
  bool mTestRecorded = false;   /**< whether the current test is done
				   recording data. */
  bool mComplete = false;       /**< set to true when all tests have finished
				   recording. */
  int mDiscardPackets = 0;      /**< number of mic packets to discard before
				   record any more data. */
  cShellProcess *mpAudioProc = nullptr; /**< pointer to a process playing
					   audio for the mics to record. */

  // data buffers
  std::vector<sMicTest> mTests; /**< sMicTestData instance for each mic test.
				   Index corresponds to mCurrentTest. */
  
  // actions
  void loadConfigFile();
  void discardPackets(int numPackets);
  bool recordData(int16_t *pData, int bufLen);
  bool analyzeData(sMicTest &test);
};

#endif // MIC_TEST_MANAGER_HPP
