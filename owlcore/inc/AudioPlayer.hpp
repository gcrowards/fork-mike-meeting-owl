#ifndef _AUDIOPLAYER_HPP_
#define _AUDIOPLAYER_HPP_

#include "AudioMgr.hpp"

#include <pthread.h>
#include <stdio.h>
#include "params.h"

typedef int (*playbackCompleteCB)(int result);

extern int testPlaybackCB(int result);


class cAudioPlayer
{
  public:
        cAudioPlayer();
        ~cAudioPlayer();        
//        static void *rawPlayback(void *);
        int playWavFile(const char *wavFileName,playbackCompleteCB);
        int playRawFile(const char *rawFileName,playbackCompleteCB );
        void waitPlayback();
        void stopPlayback();
        int getStopFlag();
        void rawPlayback();
        pthread_t mPlaybackThread;        

  private:
        // Variables
#ifdef __ANDROID__
        FILE *mPlaybackFile;
        CIRC_BUFF *mpFilePlaybackCB;
        pthread_mutex_t mPlayerMutex;
        int mStopFlag;
        playbackCompleteCB mPlaybackCallback;
#endif


};


#endif
