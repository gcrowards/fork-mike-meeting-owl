/** blackBox.hpp
 *
 *  Created on: Mar 23, 2017
 *
 *  Module containing interface for saving, loading, and manipulating
 *  the BlackBox.json system state storage file.
 *
 *  To add new fields to the black box, do the following:
 *  1) Define a new object key string (in BlackBox or Settings groups for now).
 *  2) Add the new object key to the black box template (including default).
 *  3) Use setXxx() and getXxx() functions in the code to access the data.
 *  4) Optional: if critcal changes are made, call save() to store to disk.
 */

#ifndef OWLCORE_INC_BLACKBOX_HPP_
#define OWLCORE_INC_BLACKBOX_HPP_

#ifdef Bool
#undef Bool  // Avoid name clash with X11 header
#endif

#include "rapidjson/document.h"
#include "rapidjson/reader.h"

#include <vector>

/* Black Box file names */
#define BLACK_BOX_DATA "BlackBox.json"       /**< Black Box file name. */
#define BLACK_BOX_BACKUP "BlackBox.bak.json" /**< Black Box backup file. */

/* Black Box root object keys */
#define BB_ROOT_STR "BlackBox"        /**< Root key of JSON in black box. */
#define SETTINGS_ROOT_STR "Settings"  /**< Settings section in black box. */
#define VERSIONS_ROOT_STR "Versions"  /**< Version history list in black box. */

/* Black Box data object keys */
#define BB_N_MEETINGS_STR   "nMeetings" /**< Number of meetings JSON key. */
#define BB_N_POWER_UP_STR   "nPowerUp"  /**< Number of times Owl boots from
                                         * power up. */
#define BB_N_CRASH_BOOT_STR "nCrashBoot"/**< Number of times Owl boots
                                         * recovering from system crash. */
#define BB_BAD_BOOT_STR     "lastBadBoot"/**< Reason for previous reboot. */
#define BB_N_APP_STARTS_STR "nAppStarts" /**< Number of runs of meetingOwl. */
#define BB_UP_TIME_MIN_STR  "nUpTimeMinutes"  /**< Time program runs. */
#define BB_MEETING_MIN_STR  "nMeetingMinutes" /**< Time in meetings. */
#define BB_UP_TIME_SINCE_BOOT_MIN_STR  "nUpTimeSinceBootMinutes"  /**< Time
    * since the system last booted. */
#define BB_N_USB_STATE_FIXUPS_STR "nUsbStateFixups" /**< Number of times USB
    * state had to be corrected in libOwlPG. */
#define BB_PINNED_AOI_MINUTES "nPinnedAoiMinutes" /**< Total minutes of meetings
    * where an AOI was pinned. */
#define BB_N_PINNED_AOIS     "nPinnedAois" /**< Total number of times AOIs have
    * been pinned during meetings. */
#define BB_DEVICE_SERIAL_NUMBER "deviceSerialNumber" /**< Hdwr serial number. */
#define BB_SLIDING_MODE_MINUTES "nSlidingModeMinutes" /**< Time spent in
    * 'sliding' animation mode. */
#define BB_CUT_MODE_MINUTES     "nCutModeMinutes"     /**< Time spent in
    * 'straight-cut' animation mode. */

/* Settings object keys */
#define SETTINGS_SPKR_VOLUME_STR "spkrVolume" /**< Persistent speaker volume. */
#define SETTINGS_APP_SETUP_PROGRESS "appSetupProgress" /**< Integer: furthest
    * progress in setting up the App (see eSetupProgress_t for range). */
#define SETTINGS_ANIMATION_MODE "animationMode" /**< Persist the mode for
    * transitions between speakers on the stage. */
#define SETTINGS_PANO_SHIFT_ANGLE "panoShiftAngle" /**< Persistent pano shift
                                                    *   angle [0, 360). */

/* Versions object key */
#define VERSIONS_HIST_STR "history"      /**< Array of version history. */


/** Template for black box JSON data. */
#define BLACK_BOX_JSON                                \
  "{"                                                 \
    "\"" BB_ROOT_STR "\": {"                          \
       "\"" BB_N_MEETINGS_STR "\":0,"                 \
       "\"" BB_N_POWER_UP_STR "\":0,"                 \
       "\"" BB_N_CRASH_BOOT_STR "\":0,"               \
       "\"" BB_BAD_BOOT_STR "\":\"None\","            \
       "\"" BB_N_APP_STARTS_STR "\":0,"               \
       "\"" BB_UP_TIME_MIN_STR "\":0,"                \
       "\"" BB_MEETING_MIN_STR "\":0,"                \
       "\"" BB_UP_TIME_SINCE_BOOT_MIN_STR "\":0,"     \
       "\"" BB_N_USB_STATE_FIXUPS_STR "\":0,"         \
       "\"" BB_PINNED_AOI_MINUTES "\":0,"             \
       "\"" BB_N_PINNED_AOIS "\":0,"                  \
       "\"" BB_DEVICE_SERIAL_NUMBER "\":\"unknown\"," \
       "\"" BB_SLIDING_MODE_MINUTES "\":0,"           \
       "\"" BB_CUT_MODE_MINUTES "\":0"                \
    "},"                                              \
    "\"" SETTINGS_ROOT_STR "\": {"                    \
       "\"" SETTINGS_SPKR_VOLUME_STR "\":80,"         \
       "\"" SETTINGS_APP_SETUP_PROGRESS "\":0,"       \
       "\"" SETTINGS_ANIMATION_MODE "\":\"Sliding\"," \
	   "\"" SETTINGS_PANO_SHIFT_ANGLE "\":0"          \
    "},"                                              \
    "\"" VERSIONS_ROOT_STR "\": {"                    \
       "\"" VERSIONS_HIST_STR "\": []"                \
    "}"                                               \
  "}"                                                 \
  /**< Format for the data stored as the system black box. */


/** [Design Explanation](pages.html) | 
 * Class for interacting with the black box.*/
class cBlackBox
{
public:
  cBlackBox(std::string blackBoxDir);
  ~cBlackBox();

  bool setInt(const char *groupKey, const char *key, int val);
  bool getInt(const char *groupKey, const char *key, int &val);
  bool incInt(const char *groupKey, const char *key, int increment = 1);
  bool setString(const char *groupKey, const char *key, std::string &str);
  bool getString(const char *groupKey, const char *key, std::string &str);
  bool setBool(const char *groupKey, const char *key, bool val);
  bool getBool(const char *groupKey, const char *key, bool &val);
  bool getVersionChanged(void);

  int  save(void);

private:
  rapidjson::Document mDoc;     /**< BlackBox document. */
  std::string mFileName;        /**< Complete file name and path. */
  std::string mBackName;        /**< Backup black box name and path. */
  bool mVersionChanged;         /**< True if version updated since last boot. */

  int  init(void);
  int  valid(std::string &file);
  void loadDeviceSerialNumber(std::string file);

  void updateVersionList(void);
  std::vector<std::string> getStringList(const char *groupKey,
                                         const char *key);
  bool appendStringList(const char *groupKey, const char *key,
                        std::string newVal);

};

#endif /* OWLCORE_INC_BLACKBOX_HPP_ */
