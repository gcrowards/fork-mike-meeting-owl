/*
 * analytics.hpp
 *
 *  Created on: Nov 28, 2016
 *      Author: Tom Bushman
 */

#ifndef OWLCORE_INC_ANALYTICS_HPP_
#define OWLCORE_INC_ANALYTICS_HPP_


/* *** Includes *** */

#ifdef Bool
#undef Bool  // Avoid name clash with X11 header
#endif
#include "blackBox.hpp"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "item.hpp"
#include "opencv2/core/core.hpp"
#include "thread.hpp"


/* *** Constants *** */

#define MEETING_RECORD_FILE_TEMPLATE "MeetingRecord%02d.json" \
  /**< File name template to make per meeting record files. */
#define MEETING_RECORD_BACKUP_FILE_TEMPLATE "BackupMR.json" \
  /**< File name for the backup meeting record file. */
#define PROGRAM_EVENTS_COUNT_FILE "StartCount.txt" /**< Contains number of times
   * application has started up since last reboot. */
#define STARTUP_RECORD_FILE_TEMPLATE "%s/StartupRecord_%s.json" /**< Startup
   * message file name. */

#define MR_MEETING_NUMBER_STR "meetingNumber" /**< Meeting number JSON key. */
#define MR_START_TIME_STR "startTimeSeconds"  \
  /**< Meeting start (seconds since 1970). */
#define MR_DURATION_STR "durationSeconds"     /**< Meeting duration JSON key. */
#define MR_ATTENDANCE_STR "personCount" /**< Number of meeting attendees. */
#define MR_LOCAL_TALK_TIME_STR "localTalkTimeSeconds"
#define MR_REMOTE_TALK_TIME_STR "remoteTalkTimeSeconds"
#define MR_BOTH_TALK_TIME_STR "bothTalkTimeSeconds"
#define MR_NEITHER_TALK_TIME_STR "neitherTalkTimeSeconds"
#define MR_FPS_STR "fps" /**< Frame rate too slow at some point. */
#define MR_CRASH_IN_MEETING_STR "crashInMeeting" /**< true if crashed. */
#define MR_N_PINNED_AOI_EDITS_STR  "nPinnedAoiEdits" /**< Number of times pinned
                                                      * Aois were edited. */
#define MR_AVE_PINNED_AOI_WIDTH_STR "avePinnedAoiWidth" /**< Average pinned Aoi
                                                         * size. */
#define MR_PINNED_AOI_TIME_STR "nPinnedAoiSeconds"  /**< Seconds of meeting with
                                                     * any pinned Aoi defined. */
#define MR_AVE_CAMERA_TEMP  "cameraTemp"  /**< Average camera temperature
                                           * over the duration of the meeting. */
#define MR_AVE_CPU_TEMP  "cpuTemp"  /**< Average CPU 0 temperature
                                     * over the duration of the meeting. */
#define MR_DTD_TIME_STR  "dtdEnabledSeconds"  /**< Meeting time with DTD on. */

#define MEETING_RECORD_JSON_TEMPLATE         \
  "{\"" MR_MEETING_NUMBER_STR "\":%d,"       \
  "\"" MR_START_TIME_STR "\":%d,"            \
  "\"" MR_DURATION_STR "\":%d,"              \
  "\"" MR_ATTENDANCE_STR "\":%d,"            \
  "\"" MR_LOCAL_TALK_TIME_STR "\":%d,"       \
  "\"" MR_REMOTE_TALK_TIME_STR "\":%d,"      \
  "\"" MR_BOTH_TALK_TIME_STR "\":%d,"        \
  "\"" MR_NEITHER_TALK_TIME_STR "\":%d,"     \
  "\"" MR_FPS_STR "\":%3.1f,"                \
  "\"" MR_CRASH_IN_MEETING_STR "\":%s,"      \
  "\"" MR_N_PINNED_AOI_EDITS_STR "\":%d,"    \
  "\"" MR_AVE_PINNED_AOI_WIDTH_STR "\":%d,"  \
  "\"" MR_PINNED_AOI_TIME_STR "\":%d,"       \
  "\"" MR_AVE_CAMERA_TEMP "\":%3.0f,"        \
  "\"" MR_AVE_CPU_TEMP "\":%3.1f,"           \
  "\"" MR_DTD_TIME_STR "\":%3.0f}"           \
  /**< Format for the data stored as per-meeting records. */

#define SR_STARTUP_RECORD "powerOffMessage" /**< Power-off message prior to last
    * reboot. */
#define SR_UP_TIME_BEFORE_REBOOT_STR "upTimeBeforeReboot" /**< System up time
    * prior to the last restart. */
#define SR_CORE_FILE_COUNT_STR "nCoreFiles" /**< Number of core dump files. */
#define SR_STARTUP_TIME_STR "startupTime"   /**< Time when system booted. */
#define STARTUP_RECORD_JSON_TEMPLATE           \
  "{ \"" SR_STARTUP_RECORD "\":\"%s\","        \
    "\"" SR_UP_TIME_BEFORE_REBOOT_STR "\":%d," \
    "\"" SR_CORE_FILE_COUNT_STR "\":%d,"       \
    "\"" SR_STARTUP_TIME_STR "\":%d}"          \
  /**< Format for StartupRecord files. */


#define MIN_VALID_MEETING_LENGTH_SECONDS  5 /**< Minimum meeting duration
                                             * worth counting. */
#define BB_SAVE_INTERVAL_MINUTES         10 /**< Time between saving updates
                                             * to the black box. */

#define MAX_PINNED_AOIS                   3 /**< Maximum number of simultaneous
                                             * pinned AOIs. */

typedef std::function<void(bool, bool)> analyticsCallback_t;


/**  cAnalytic is a class to encapsulate storage and retrieval of system-wide 
 * analytics.
 *
 * To use this class, first create an instance of the object with new, then
 * call init() passing in the path to the analytic data storage directory.
 * During run time, call setStartTime() to signal the beginning of a meeting,
 * and setEndTime() to mark its conclusion. Then call save() to store the
 * data.
 */
class cAnalytics
{
public:
  // constructor(s)
  cAnalytics(cBlackBox *pBlackBox);
  // destructor
  ~cAnalytics();

  /** Enumeration for managing state machine for tracking meetings. */
  typedef enum {
    NOT_MEETING,
    MEETING_STARTING,
    MEETING,
    MEETING_ENDING,
    MEETING_ENDING_NO_PEOPLE
  } eMeetingState;

  /** Enumeration used to identify type of meeting record. */
  typedef enum {
    PUBLISHED_MEETING,
    BACKUP_MEETING
  } eMeetingRecordType;

  // public interface
  void init(const char *pAnalyticsDir);
  void setStartTime(void);
  bool setEndTime(eMeetingRecordType type = PUBLISHED_MEETING);
  int save(void);
  void updatePeopleHistogram(const std::vector<const cItem *> pItems);
  void updateTalkTimeStats(float localTime, float remotesTime,
			   float bothTime, float neitherTime,
			   float dtdEnabledTime);
  void updateSystemStatistics(float fps, float cameraTemp, float cpuTemp);
  void setAnalyticsCallback(analyticsCallback_t analyticsCallback);
  void updateMeetingState(int usbConnected,
                          int uvcStreaming,
                          int usbSpeakerConnected,
                          int usbMicConnected,
                          int usbStateFixup);
  eMeetingState getMeetingState(void);
  void setPinnedAoiStartTime(int aoiId, int widthDegrees);
  void setPinnedAoiEndTime(int aoiId);
  cBlackBox *getBlackBox(void);
  void updateAnimationModeDuration(void);
  

private:
  // data
  struct timespec mStartTime;       /**< Storage for meeting start time */
  struct timespec mEndTime;         /**< Storage for meeting end time */
  struct timespec mAnimModeStartTime; /**< Save time when mode is set */
  char * mpSaveDir;                 /**< Directory that contains the black box
                                     * and meeting record files. */
  cBlackBox *mpBlackBox;            /**< Pointer to the black box object. */
  char * mpBlackBoxJson;            /**< Character buffer used to compose
                                     * JSON strings to save in files. */
  unsigned long mThetaHist[360];    /**< Histogram of angles where meeting
                                     * participants were found. */
  double mStdDevSums[360];          /**< Accumulator for std devs at each
                                     * angle. */
  double mOutHist[360];             /**< Computed composite histogram of all
                                     * personMap probability distributions from
                                     * the entire meeting. */
  int mHistUpdateCount;             /**< Number of times the histograms have
                                     * been updated (for normalization). */
  unsigned long mThetaHistST[360];  /**< Histogram of angles where meeting
                                     * participants were found (short term). */
  double mStdDevSumsST[360];        /**< Accumulator for std devs at each
                                     * angle (short term). */
  double mOutHistST[360];           /**< Computed composite histogram of all
                                     * personMap probability distributions from
                                     * the entire meeting (short term). */
  int mHistUpdateCountST;           /**< Number of times the histograms have
                                     * been updated (short term). */
  rapidjson::Document mBlackBoxDoc; /**< JSON document containing the
                                     * structure of the black box data. */
  /* Global Black Box Data */
  int mCoreFileCount;               /**< Number of core files present. */
  /* MeetingRecord Data */
  int mPersonCount;                 /**< Number of people attending meeting. */
  int mPersonCountST;               /**< Number of people attending meeting
                                     * (short term). */
  float mLocalTalkTime;             /**< Time Owl side talked (s). */
  float mRemoteTalkTime;            /**< Time non-Owl side talked (s). */
  float mBothTalkTime;              /**< Time both sides talked at same 
				     * time (s). */
  float mNeitherTalkTime;           /**< Time neither side was talking (s). */
  float mFps;                       /**< Average video frame rate of meeting. */
  float mCameraTemp;                /**< Average camera temp during meeting. */
  float mCpuTemp;                   /**< Average CPU0 temp during meeting. */
  int   mSamples;                   /**< Samples for above averages. */
  float mDtdTime;                   /**< Seconds spent with DTD enabled. */

  eMeetingState mMeetingState;       /**< State variable for meeting tracker. */
  struct timespec mTransitionTimer;  /**< Start time for state transitions. */
  struct timespec mSaveBackupTimer;  /**< Last time a backup record saved. */
  struct timespec mShowStateTimer;   /**< Time connection state was shown.*/
  struct timespec mUpTimer;          /**< Timing program run-time. */
  struct timespec mMeetingTimer;     /**< Timing running meeting. */
  int mInitialUpTimeMinutes;         /**< Time when program started running. */
  int mInitialMeetingTimeMinutes;    /**< Time when meeting started. */
  std::string mLastBootReason;       /**< Last boot reason (including p-up).*/
  int mCachedMostRecentUpTime;       /**< Temporary record of last up-time. */

  int mPinnedAoiId[MAX_PINNED_AOIS]; /**< Id of pinned Aoi in this slot. */
  struct timespec mPinnedAoiStart;   /**< Time this Aoi was
                                      * pinned. */
  int mSumPinnedAoiSize;             /**< Accumulator for pinned Aoi sizes. */
  int mNumPinnedAoiSizes;            /**< Count of values in accumulator. */
  int mNPinnedAoiEdits;              /**< Number of times pinned Aois were
                                      * modified. */
  int mPinnedAoiSeconds;             /**< Number of seconds during meeting
                                      * where an Aoi was pinned. */

  analyticsCallback_t mAnalyticsCallback = nullptr; /**< Function called when
                      * meetings start/end and set by setAnalyticsCallback(). */

  // internal functions
  int  initGlobalData();
  int  updateAndSaveBlackBox(void);
  int  saveMeetingRecord(void);
  int  saveBackupMeetingRecord(void);
  int  saveMeetingRecordToFile(eMeetingRecordType fileType);
  int  moveBackupMeetingRecord(void);
  int  deleteBackupMeetingRecord(void);
  bool backupMeetingRecordExists(void);
  void resetPeopleHistogram(void);
  void resetShortTermPeopleHistogram(void);
  void resetTalkTimeStats(void);
  void savePeopleHistogram(void);
  void computeFinalMapHistogram(double paOutHist[],
                                unsigned long paThetaHist[],
                                double paStdDevSums[],
                                double histUpdateCount);
  int  extractMeetingPersonCount(int minPersonWidth, double minProb,
                                 double paOutHist[]);
  void saveDArrayToFile(const char *pFileName, double data[], int len);
  bool timeToUpdateBlackBox(struct timespec tNow);
  void updateBlackBoxTimers(struct timespec tNow);
  void updateProgramCounts(void);
  bool isPowerOnBoot(std::string &powerOffReason);
  void saveStartupMessage(void);
  int  countCoreFiles(void);
  bool isAnyAoiPinned(void);
  int  getCurrentPinnedAoiDuration(void);
};


#endif /* OWLCORE_INC_ANALYTICS_HPP_ */
