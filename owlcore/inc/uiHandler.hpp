/** Copyright 2016 Owl Labs Inc.
 *  All rights reserved.
 *
 *  uiHandler.hpp
 *  This is the header file for cUiHandler.
 *   --> Runs a thread which updates each aspect of the UI.
 *       - Polls for button events.
 *       - Notifies the Owl's analytics when a meeting has started or stopped.
 */

#ifndef UI_HANDLER_HPP
#define UI_HANDLER_HPP

#include "analytics.hpp"
#include "buttonContext.hpp"
#include "ledContext.hpp"
#include "netStatus.hpp"
#include "thread.hpp"

/** Runs a thread which updates each aspect of the UI.
 *   - Polls for button events.
 *   - Notifies the Owl's analytics when a meeting has started or stopped.
 */
class cUiHandler
{
public:
  static const int mkThreadSleepUs = 30000; /**< microseconds for polling
					       thread to sleep while active. */
  static const int mkStopDelayUs = 300000; /**< microseconds to delay
					      after stopping to wait for the
					      thread to actually stop.
					   TODO: Replace this with a mutex, or a
					   more threading-oriented approach. */
  static const int mkUsbBlinkTimeUs = 500000; /**< the amount of time to turn on
						 the mute/bluetooth LEDs when USB
						 is connected or disconnected. */
  static const int mkRebootCheckPeriodMinutes = 10; /**< Check to see if we
                                                     * should do a maintenance
                                                     * reboot this often. */
  static const int mkHourToRebootStart = 4; /**< Reboot at 4 AM central time. */
  static const int mkHourToRebootEnd = 5;/**< Don't reboot past 5 AM central. */

  cUiHandler(cButtonHandler *pButtonHandler, cLedInterface *pLedInterface);
  ~cUiHandler();

  void start();
  void stop();

  void setButtonContext(const cButtonContext *pButtonContext);
  void setLedContext(cLedContext *pLedContext);
  void setAnalytics(cAnalytics *pAnalytics);
  
  void setSpeakerActive(bool active);
  void setMicActive(bool active);

private:
  cThread mUiThread;                /**< the thread that updates the UI. */
  cButtonHandler *mpButtonHandler;  /**< pointer to the button handler. */
  cLedInterface *mpLedInterface;    /**< pointer to the LED interface. */

  cLedContext mSystemLedContext;    /**< LED context for enacting system-related
				       LED behavior. */
  cAnalytics *mpAnalytics;           /**< Pointer to the analytics object. */
  bool mActive = false;             /**< whether the UI is currently being
				       handled. */

  /** Enumeration for managing state machine for tracking meetings. */
  typedef enum {
    NOT_MEETING,
    MEETING_STARTING,
    MEETING,
    MEETING_ENDING
  } eMeetingState;
  eMeetingState mMeetingState;       /**< State variable for meeting tracker. */
  struct timespec mTransitionTimer;  /**< Start time for state transitions. */

  bool mMicActive;                   /**< Indicates if mic hears sound. */
  bool mSpeakerActive;               /**< Indicates if speaker emitting sound.*/

  static const char *mkLatestTime;   /**< Name of most recent time file. */
  static const char *mkLatestTimeBak;/**< Name of recent time backup file. */
            /* Both the above initialized at top of implementation module. */

  void handleUi();
  void updateNightlyReboot();

  void updateSystemTime(void);
  void saveSystemTime(void);
  bool getSystemTimeFromFile(const char *pFileName, struct timeval *pTime);
};

#endif // UI_HANDLER_HPP
