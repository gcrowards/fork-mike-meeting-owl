#ifndef _LOGGING_HPP_
#define _LOGGING_HPP_

#include <functional>
#include <stdarg.h>
#include <stdio.h>

#include "debugSwitches.h"

// available log levels
#define OWL_LOG_EVERTHING  0
#define OWL_LOG_VERBOSE    2
#define OWL_LOG_DEBUG      3
#define OWL_LOG_INFO       4 
#define OWL_LOG_WARN       5
#define OWL_LOG_ERROR      6
#define OWL_LOG_FILE_ONLY  8
#define OWL_LOG_NOTHING    9

#if GLOBAL_DEBUG
  // set desired log level; can override with CFLAGS define, e.g., -DLOG_LEVEL=5
  // BE SURE TO DO 'make clean' IF LOG_LEVEL CHANGES
  #ifndef LOG_LEVEL
  #define LOG_LEVEL OWL_LOG_EVERYTHING
  #endif
#else
  // override if GLOBAL_DEBUG is false
  #ifdef LOG_LEVEL
  #undef LOG_LEVEL
  #endif
  #define LOG_LEVEL OWL_LOG_NOTHING
#endif

#define MAX_LOG_MSG_LEN  1024

/** Callback signature for interface to call to log remote messages. */
typedef std::function<void(char *pMsg, size_t msgLen)> logCallback_t;

// prototypes for logging functions
void   owlLog(int level, ...);
void   owlLogFile(int level, va_list ap);
void   owlLogStream(FILE *stream, int level, va_list ap);
void   owlLogString(char msg[MAX_LOG_MSG_LEN], char *pHeader, int level,
                    va_list ap);
void   owlLogConfigureRemote(logCallback_t logCallback, char *pHeader);

// -- use the preprocessor to determine if logging macros should expand to something

// LOGV() good for repetitive info that's expected to vary, like frame rate
#if LOG_LEVEL <= OWL_LOG_VERBOSE
#define LOGV(...) (owlLog(OWL_LOG_VERBOSE,  __VA_ARGS__))
#else
#define LOGV(...) {}
#endif

#if LOG_LEVEL <= OWL_LOG_DEBUG
#define LOGD(...) (owlLog(OWL_LOG_DEBUG,  __VA_ARGS__))
#else
#define LOGD(...) {}
#endif

#if LOG_LEVEL <= OWL_LOG_INFO
#define LOGI(...) (owlLog(OWL_LOG_INFO,  __VA_ARGS__))
#else
#define LOGI(...) {}
#endif

#if LOG_LEVEL <= OWL_LOG_WARN
#define LOGW(...) (owlLog(OWL_LOG_WARN,  __VA_ARGS__))
#else
#define LOGW(...) {}
#endif

#if LOG_LEVEL <= OWL_LOG_ERROR
#define LOGE(...) (owlLog(OWL_LOG_ERROR, __VA_ARGS__))
#else
#define LOGE(...) {}
#endif

// LOGF() good for logging data for regression testing
#if LOG_LEVEL <= OWL_LOG_FILE_ONLY
#define LOGF(...) (owlLog(OWL_LOG_FILE_ONLY, __VA_ARGS__))
#else
#define LOGF(...) {}
#endif

#endif /* _LOGGING_HPP_ */
