/*
 * statusFrame.hpp
 *
 * Copyright (c) Owl Labs Inc. 2017.
 * All rights reserved.
 *
 * Description: Implements a status frame that contains status tiles. Each
 * tile contains one or more icons representing an aspect of system status.
 * A tile's icon is selected based on result of a periodic update where the
 * update is a callback passed to the tile at creation time.
 * The frame lets each tile render itself into the display buffer, then
 * passes the display buffer to the GPU to render. This behavior is overridden
 * if a tile requests animation. When a tile changes state to animate,
 * that tile alone is animated for the duration of the action, after which
 * the frame with all its tiles is rendered normally again. Finally, the frame
 * can fade away to invisibility if all the tiles have the allow-fade-away
 * feature set to true. When the frame becomes invisible, it is no-longer
 * rendered.
 */

#ifndef OWLCORE_INC_STATUSFRAME_HPP_
#define OWLCORE_INC_STATUSFRAME_HPP_

#include <string>
#include <vector>

#include "opencv2/core/core.hpp"
#include "gpu.hpp"
#include "systemtest/systemTest.hpp"


//#define USE_UPDATE_THREAD /* Define to perform updates in separate thread */
#ifdef USE_UPDATE_THREAD
#include <pthread.h>
#include "thread.hpp"
#endif

/** Type for callback functions called by each tile to update the icon
 * selection.
 */
typedef int16_t (*tileIconSelectionCallback_t)();


/** Class to contain an icon initializer. A list of these is used to
 * initialize a tile.
 */
class cIconInitializer {
public:
  cIconInitializer(std::string filename,
                   bool zoomOnActivate,
                   bool zoomOnDeActivate,
		   bool allowFadeIn,
                   bool allowFadeAway );
   ~cIconInitializer();

  std::string mIconFileName;  /**< File containing an icon PNG. */
  bool mZoomOnActivate;       /**< Zooms from mid-screen down to frame when
                               * selected. */
  bool mZoomOnDeActivate;     /**< Zooms from frame to mid-screen when
                               * de-selected. */
  bool mAllowFadeIn;          /**< If this flag is set for all selected icons
			       * then the status frame will fade in after it
			       * becomes visible. */
  bool mAllowFadeAway;        /**< If this flag is set for all selected icons
                               * then the status frame fades after a while. */
};

/** Contains icon data for an icon. One or more icons are owned by a tile. */
class cTileIcon
{
public:
  cTileIcon(cIconInitializer icon);
  cTileIcon(const cTileIcon &c);
  ~cTileIcon();

  void renderRow(uint16_t row,
                 uint16_t nTileRows, uint16_t nTileCols,
                 uint32_t *mpRowBuf);

  /** Returns flag indicating icon should be animated when selected. */
  bool getZoomOnActivate(void) { return mZoomOnActivate; }

  /** Returns flag indicating icon should be animated when deselected. */
  bool getZoomOnDeActivate(void) { return mZoomOnDeActivate; }

  /** Returns flag indicating icon should fade in after an interval. */
  bool getAllowFadeIn(void) { return mAllowFadeIn; }
  /** Returns flag indicating icon should fade away after an interval. */
  bool getAllowFadeAway(void) { return mAllowFadeAway; }

  /** Returns the width of this icon. */
  uint16_t getWidth(void) { return mWidth; }

  /** Returns the height of this icon. */
  uint16_t getHeight(void) { return mHeight; }

private:
  uint32_t *mpBuf;   /**< Buffer containing RGBA icon pixels. */
  uint16_t mWidth;   /**< Width of the icon. */
  uint16_t mHeight;  /**< Height of the icon. */
  bool     mZoomOnActivate;   /**< Zoom when icon is made active. */
  bool     mZoomOnDeActivate; /**< Zoom when icon is made inactive. */
  bool     mAllowFadeIn;      /**< Fade in when parent frame becomes visible. */
  bool     mAllowFadeAway;    /**< Fade out after fade time passes. */
};

/** Contains one or more icons to display in the status frame. A callback
 * function determines which icon is active at any given time.
 */
class cTile
{
public:
  cTile(std::vector<cIconInitializer>iconList,
        tileIconSelectionCallback_t selectedIconCallback);
  ~cTile();

  void updateSelection(bool *pAnimate, bool *pChangedSelection);
  void render(uint16_t col,
              uint16_t frameRows, uint16_t frameCols,
              uint32_t *mpFrameBuf);

  /** Returns the size of the tile. */
  cv::Size &getSize(void) { return mSize; }

  /** Returns the index of the selected icon. */
  int16_t getSelectedIcon(void) { return mSelectedIcon; }

  /** Returns the index of the icon that has been de-selected. */
  int16_t getDeSelectedIcon(void) { return mDeSelectedIcon; }

  /** Returns whether the selected icon allows fade in. */
  bool getAllowFadeIn(void)
         { return mIcons[mSelectedIcon]->getAllowFadeIn(); }
  /** Returns whether the selected icon allows fade out. */
  bool getAllowFadeAway(void)
         { return mIcons[mSelectedIcon]->getAllowFadeAway(); }
  /** Reset selection to no icon to force immediate animation on visible. */
  void resetSelection(void)
         { mSelectedIcon = mDeSelectedIcon = -1; }

private:
  std::vector<cTileIcon *> mIcons;  /**< List of cTileIcon pointers. */
  tileIconSelectionCallback_t mSelectActiveIconCallback; /**< Callback function
                                     * invoked to find selected icon number. */
  int16_t mSelectedIcon;            /**< Selected icon number. */
  int16_t mDeSelectedIcon;          /**< Number of icon being de-selected. */
  cv::Size mSize;                   /**< Largest width and height of icons. */

  /** Returns true if the icon index is valid for this tile. */
  bool validIcon(uint16_t icon)
    { return ((icon >= 0) && (icon < mIcons.size())); }
};


/** Class that represents a status frame overlaid on the live video. The
 * status frame contains cTile objects. Each tile contains icons and invokes
 * a callback function at each update to find out which icon to display. Icons
 * are tagged with extra information used to determine behavior when switching
 * to or from that icon. When an icon becomes selected or loses the selection,
 * it can cause an animation on the screen. Also, if all icons are tagged with
 * the AllowFadeAway tag, then after a certain amount of time the status
 * frame will slowly fade from view.
 * The status frame is created by passing it a pointer to a list of cTile
 * pointers. Both the list and the cTile objects must be allocated externally,
 * but the destructor for cStatusFrame will properly dispose of the list
 * and objects.
 */
class cStatusFrame
{
public:
  /** Note, pTileList is allocated by the creator of an instance of a
   * cStatusFrame, but cStatusFrame deletes the list and all the cTile
   * objects it contains when done.
   */
  cStatusFrame(cv::Point upperLeft, float scale,
               std::vector<cTile *>*pTileList);
  ~cStatusFrame();

  /** Returns pointer to the buffer containing the frame to display.
   *
   * @returns pointer to the buffer to display.
   */
  uint32_t *getFrameBuffer(void) { return mpFrameBuf; }

  /** Returns the bounding rectangle for the buffer to display.
   *
   * @returns reference to the bounding rectangle of the buffer.
   */
  cv::Size &getRectSize(void)    { return (mAnimating ? mAnimSize : mSize); }

  /** Returns the upper left coordinates where the overlay should be rendered.
   *
   * @returns reference to a point in the output image coordinate frame.
   */
  cv::Point &getDest(void)       { return (mAnimating ? mAnimOrigin : mDestOrigin); }

  /** Returns the scale multiplier to apply when rendering the buffer.
   *
   * @returns scale factor for sizing the overlay.
   */
  float getScale(void)           { return (mAnimating ? mAnimScale : mScale); }

  /** Returns the transparency setting for the overlay rendering.
   *
   * @returns alpha value for overlay transparency.
   */
  float getAlpha(void)           { return (mRenderAlpha); }

  /** Set the scale multiplier to apply when rendering the buffer.
   *
   * @param scale value to apply when rendering in the GPU.
   */
  void setScale(float scale)     { mScale = scale; }

  /** Set the destination origin where the overlay should be rendered.
   *
   * @param Upper left corner of the overlay in the camera image.
   */
  void setDest(cv::Point p)      { mDestOrigin = p; }

  /** Return flag indicating if the status frame needs to be drawn.
   *
   * @return true if status frame contents have changed.
   */
  bool renderRequired(void)      { return mRenderRequired; }
  
  /** Return flag indicating if the status frame is visible.
   *
   * @return true if the status frame is visible on the screen.
   */
  bool visible(void)             { return mVisible; }
  
  void setVisible(bool visibility);
  void resetAnimation(void);
  bool update(bool force);

private:

  void animateTileUpdate(uint16_t *pX, uint16_t *pY, float *pScale);
  void renderAllTiles(void);
  void updateFade(void);

  std::vector<cTile *>*mpTileList;
  cv::Size mSize;
  cv::Size mAnimSize;
  cv::Point mDestOrigin; /**< Upper left point where frame is rendered */
  cv::Point mAnimOrigin; /**< Upper left when animating */
  float mScale;          /**< Scaling multiplier for resizing frame */
  float mAnimScale;      /**< Scaling multiplier for animating frame */
  uint32_t *mpFrameBuf;  /**< Render tiles into this space. */

  bool mRenderRequired;         /**< If true, GPU needs updating. */
  bool mVisible;                /**< If true, GPU should display frame. */
  bool mDoUpdate;               /**< Variable gating performing update; only
                                 * set when tile icon(s) have changed. */
  bool mAnimating;       /**< True when animating one icon (to draw
                          * attention to it). */
  cTile *mpAnimatingTile;/**< Tile currently being animated, or NULL. */
  struct timespec mT0;   /**< Time reference for start of animation. */
  const float mkAnimationDuration; /**< Duration of the animation. */

  float mRenderAlpha;           /**< Alpha scaler for rendering frame. */
  struct timespec mFadeTimeRef; /**< Start of countdown prior to fading. */
  bool mFadedIn;                /**< True once the frame has fully faded in. */
  const float mkFadeInStart;      /**< Seconds after which fade in begins. */
  const float mkFadeInDuration;   /**< Seconds over which fade in occurs. */
  const float mkFadeAwayStart;      /**< Seconds after which fade away begins. */
  const float mkFadeAwayDuration;   /**< Seconds over which fade away occurs. */

};


/** Class to manage a banner line overlay rendered on output frames.
 *
 * This class loads the banner line bitmap and provides the interface for
 * accessing it for display.
 */
class cBannerLine
{
public:
  cBannerLine(const char *pBannerFile, cGpu *pGpu, cv::Point upperLeft);
  ~cBannerLine();

  uint32_t  *getFrameBuffer(void)     { return mpBuf; }
  cv::Point &getDest(void)            { return mUpperLeft; }
  uint16_t   getWidth(void)           { return mWidth; }
  uint16_t   getHeight(void)          { return mHeight; }
  float      getAlpha(void)           { return mAlpha; }

  void       setDest(cv::Point p)     { mUpperLeft = p; }
  void       setAlpha(float a)        { mAlpha = a; }

  void       setNewFileToLoad(const char *pFile, cv::Point upperLeft);
  bool       drawTextMessage(std::string msg, int left, int bottom,
                             cv::Scalar color);
  void       markFileReadyToLoad(void);
  bool       loadNewFile();

private:
  uint32_t  *mpBuf;        /**< Buffer containing banner line bitmap. */
  cGpu      *mpGpu;        /**< Pointer to the GPU object. */
  cv::Point mUpperLeft;    /**< Upper left coordinate in camera image. */
  uint16_t  mWidth;        /**< Width of the banner line bitmap. */
  uint16_t  mHeight;       /**< Height of the banner line bitmap. */
  float     mAlpha;        /**< Alpha blending ratio for the banner line. */
  std::string mNewFile;    /**< File to load at next opportunity. */
  bool      mReadyToLoad;  /**< When true, load mpBuf into GPU. */
  cv::Point mNewUL;        /**< New upper left when file is loaded. */

  void hFlip(void);
};


/** Class that contains overlay updating objects and chooses which to display.
 *
 * The overlay manager creates the objects that generate overlay graphics to
 * be rendered by the RGBA overlay mechanism. At runtime, the update() member
 * function calls the update functions for the managed objects and mediates
 * which should be displayed since only one can use the RGBA overlay.
 */
class cOverlayManager
{
public:
  cOverlayManager(cGpu *pGpu, cSystemTest *pSystemTest);
  ~cOverlayManager();

  void init(void);
  void update(unsigned char *mpTestOverlayImage,
              int mTestOverlayImageWidth,
              int mTestOverlayImageHeight,
	      bool muted );
  void forceRender(void) { mForceRender = true; }

  void lockFrameBuffer(void);
  void unlockFrameBuffer(void);
  void resetTimeStats(void);

  cBannerLine *getBannerLine(void) { return mpBannerLine; }

  /** Return the average time needed to update the overlay.
   *
   * @returns Average render and update time over past 100 updates.
   */
  float getAveCycleTime(void)    { return mAveCycleTimeUsec; }
  /** Return the minimum time needed to update the overlay.
   *
   * @returns Minimum render and update time over the past 100 updates.
   */
  float getMinCycleTime(void)    { return mMinCycleTimeUsec; }
  /** Return the maximum time needed to update the overlay.
   *
   * @returns Maximum render and update time over the past 100 updates.
   */
  float getMaxCycleTime(void)    { return mMaxCycleTimeUsec; }

private:
  cBannerLine *initBannerLine(void);
  cStatusFrame *initStatusFrame(void);
  cStatusFrame *initMuteFrame(void);

  cGpu *mpGpu;                  /**< Pointer to GPU object used for rendering.*/
  cStatusFrame *mpStatusFrame;  /**< Pointer to status frame object. */
  cStatusFrame *mpMuteFrame;    /**< Pointer to mute frame object. */
  cBannerLine *mpBannerLine;    /**< Pointer to banner line object. */
  cSystemTest *mpSystemTest;    /**< Pointer to system test object. */
  bool        mForceRender;     /**< Force render on next iteration. */
#ifdef USE_UPDATE_THREAD
  cThread *mpUpdateThread;      /**< Thread that updates the frame buffer; */
  pthread_mutex_t mUpdateMutex; /**< Mutex between update and rendering. */
  pthread_cond_t mUpdateCond;   /**< Condition for waiting. */
  void updateCallback(void);    /**< Callback thread calls to update. */
#endif

  struct timespec mCycleStart; /**< Start time for an update. */
  float mAveCycleTimeUsec;     /**< Average time taken to run cycle. */
  float mMinCycleTimeUsec;     /**< Minimum time taken to run cycle. */
  float mMaxCycleTimeUsec;     /**< Maximum time taken to run cycle. */

};


#endif /* OWLCORE_INC_STATUSFRAME_HPP_ */
