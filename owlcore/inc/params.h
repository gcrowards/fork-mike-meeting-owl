#ifndef _PARAMS_H_
#define _PARAMS_H_

#include <cmath>

#include "fledglingParams.h"

// Hardware selections
// Note: MOCK_INPUT and other preprocessor defines are in Makefile

// Update Processing
#define PROCESSING_RATE         60 /**< maximum rate at which system interprets
				      audio and video data and updates the
				      output frame. */
// Hardware parameter values
#define FRAME_RATE              10 /**< target output video frame rate (Hz). */

#define CAMERA_WIDTH            FLEDGLING_CAMERA_WIDTH /**< raw (cirular) image
							  resolution. */
#define CAMERA_HEIGHT           FLEDGLING_CAMERA_HEIGHT /**< raw (circlar) 
							   image resolution. */
#define CAMERA_CENTER_X         FLEDGLING_CAMERA_CENTER_X /**< center of raw
							     (circular) image. */
#define CAMERA_CENTER_Y         FLEDGLING_CAMERA_CENTER_Y /**< center of raw
							     (circular) image. */
#define CAMERA_FOV              (2 * M_PI) /**< angular horiz. field-of-view. */
#define CAMERA_LENS_MIN_PHI_DEG FLEDGLING_LENS_MIN_PHI_DEG /**< min elevation
							      angle supported by
							      the lens. Negative
							      means below the
							      camera plane. */
#define CAMERA_OUTER_RADIUS_MARGIN FLEDGLING_OUTER_RADIUS_MARGIN
                                    /**< margin on the maximum outer radius that
				       accounts for unuasable pixels; set as a
				       fraction of the image height. */
#define CAMERA_ALIGNMENT_ANGLE  FLEDGLING_CAMERA_ALIGNMENT_ANGLE
                                    /**< degree difference between camera's
				     * frame of reference and Owl chassis's
				     * frame of reference. (this angle MUST be
				     * positive */
#define CAMERA_WARP_STRETCH     FLEDGLING_CAMERA_WARP_STRETCH
                                    /**< factor that controls amount of
				     * stretching of the GPU-unrolled image. */
#define CAMERA_WARP_BEND        FLEDGLING_CAMERA_WARP_BEND
                                    /**< factor that controls amount of
				     * bending of the GPU-unrolled image. */

// UI prarameter values
#define UI_NAME                 "Owl P1 Prototype" /**< display window name. */
#define WINDOW_RES_X            1280 /**< display window resolution. */
#define WINDOW_RES_Y            720 /**< display window resolution. */
#define SUBFRAME_BORDER_MARGIN   10 /**< pixel border around subframes. */
#define PANORAMIC_VERT_MARGIN    10 /**< top and bottom border of pano strip. */

// Display
#define OUTPUT_WIDTH            1280  /**< resolution of output display. */
#define OUTPUT_HEIGHT           720  /**< resolution of output display. */

#define VIDEO_DEVICE  "/dev/video0" /**< device that display loopback should use
				       (set when v4l2-loopback kernel module is
				       loaded. */
#define REQUIRED_FPS            29.0f /**< fps for video output to be consider 
					 good. */

// Processing resolutions
#define CV_PANO_RES_X          1280 /**< computer vision panorama size. */
#define CV_PANO_RES_Y           161 /**< computer vision panorama size. */

#define OUT_PANO_SCALE            0.925 /**< size of output panoramic strip
					   relative to CV panoramic strip;
					   must be <= 1.0. Setting a value
					   less than 1 is how we add a border
					   to the left and right of the strip
					   as seen in the output frame. */

#define OUT_PANO_RES_X ((int)(OUT_PANO_SCALE * CV_PANO_RES_X)) /**< output
								  panorama
								  size. */
#define OUT_PANO_RES_Y ((int)(OUT_PANO_SCALE * CV_PANO_RES_Y)) /**< output
								  panorama
								  size. */
#define STAGE_DEG_WIDTH 105. /**< The width of the full stage in degrees. */

// Paths
#ifdef __ANDROID__
#define SIMULATE_PCM_FILEPATH  "/data/simPcmAngle.txt" /**< for simulating PCM
							  data at a given angle
							  in degrees. */
#else // then Linux:
#define SIMULATE_PCM_FILEPATH  "./simPcmAngle.txt"
#endif

#ifdef __ANDROID__                                    /**< face dector model. */
#define FACE_DETECTOR_MODEL "/system/media/OwlLabs/haarcascade_frontalface_alt.xml"
#else // then Linux:
#define FACE_DETECTOR_MODEL "haarcascade_frontalface_alt.xml"
#endif

#define CAMERA_INFO_FILE    "./cameraIndex.txt" /**< Name of file created by
						   Python script with camera
						   dev number. */
#define MIC_INFO_FILE       "./micName.txt" /**< Name of file created by Python
					       script with mic name. */

// map data (for debugging/testing)
#ifdef __ANDROID__
#define PERSON_MAP_PLAYBACK_FILE     "/data/OwlLabs/personMap.csv"
#define VOICE_MAP_PLAYBACK_FILE      "/data/OwlLabs/voiceMap.csv"

#define PERSON_MAP_RECORD_FILE       "/data/OwlLabs/personMapRec.csv"
#define VOICE_MAP_RECORD_FILE        "/data/OwlLabs/voiceMapRec.csv"
#else // LINUX
#define PERSON_MAP_PLAYBACK_FILE     "./personMap.csv"
#define VOICE_MAP_PLAYBACK_FILE      "./voiceMap.csv"

#define PERSON_MAP_RECORD_FILE       "./personMapRec.csv"
#define VOICE_MAP_RECORD_FILE        "./voiceMapRec.csv"
#endif

// Comms
#ifdef __ANDROID__
#define SOCKET_LINK         "/data/angleComm" /**< This is the temporary file
					       to pass mic array data into
					       this program. */
#else // then Linux:
#define SOCKET_LINK         "ipc:///tmp/micData"
#endif

// Video Recording Settings
#define RECORD_VIDEO        false  /**< record fisheye video from camera. */

#define VIDEO_REC_FILENAME  "fisheyeVideo.avi" /**< Latter part of the output
						  video filename. The date and
						  time of the file creation will
						  be prepended to this. */
#define VIDEO_REC_CODEC     'M','J','P','G' /**< Desired video recording
					       codec. */
#define VIDEO_REC_FPS       12 /**< Framerate of the created video stream. */

// Faux Camera Settings
#ifdef __ANDROID__ /**< Filepath of the video or image file that should be read 
		      by faux camera. */

#ifdef INPUT_3456
#define FAUX_CAM_FILEPATH "/system/media/OwlLabs/panoramic3456-02.jpg"
#else
#define FAUX_CAM_FILEPATH "/system/media/OwlLabs/panoramic1728-02.jpg"
#endif

#else // then Linux:

#ifdef INPUT_3456
#define FAUX_CAM_FILEPATH "./assets/panoramic3456-02.jpg"
#else
#define FAUX_CAM_FILEPATH "./assets/panoramic1728-02.jpg"
#endif

#endif // __ANDROID__

// onscreen logo overlay
#ifdef __ANDROID__ /**< Filepath of the PNG file (with alpha channel) that will
		      loaded at startup. */
#define LOGO_PNG              "/system/media/OwlLabs/owlHaloIcon.png"
#define LOGO_STARTUP_PNG      "/system/media/OwlLabs/owlStartupIcon.png"
#define AOI_PIN_PNG           "/system/media/OwlLabs/owlPinnedAoi.png"
#define SPEAKER_ON_ICON       "/system/media/OwlLabs/speakerOnIcon.png"
#define SPEAKER_OFF_ICON      "/system/media/OwlLabs/speakerOffIcon.png"
#define MIC_ON_ICON           "/system/media/OwlLabs/micOnIcon.png"
#define MIC_OFF_ICON          "/system/media/OwlLabs/micOffIcon.png"
#define MIC_MUTE_ICON         "/system/media/OwlLabs/micMuteIcon.png"
#define VIDEO_MUTE_ICON       "/system/media/OwlLabs/videoMuteIcon.png"
#define BANNER_LINE           "/system/media/OwlLabs/bannerLine.png"
#define STARTUP_BACKGROUND    "/system/media/OwlLabs/startupBackground.png"
#define STARTUP_FIRST_TIME    "/system/media/OwlLabs/startupFirstTime.png"
#define STARTUP_NEXT_TIME     "/system/media/OwlLabs/startupNextTime.png"
#define FIRST_STARTUP_WAV     "/system/media/OwlLabs/owlFirstPowerUp.wav"
#define WIFI_UNCONFIG_WAV     "/system/media/OwlLabs/owlWiFiUnconfig.wav"
#define BLIP_WAV              "/system/media/OwlLabs/volumeBlip.wav"
#define HOOT_WAV              "/system/media/OwlLabs/owlHoot-48kHz.wav"
#define VER_CHANGE_HOOT_WAV   "/system/media/OwlLabs/owlHoot-NewVer-48kHz.wav"
#else
#define LOGO_PNG              "assets/owlHaloIcon.png"
#define LOGO_STARTUP_PNG      "assets/owlStartupIcon.png"
#define AOI_PIN_PNG           "assets/owlPinnedAoi.png"
#define SPEAKER_ON_ICON       "assets/speakerOnIcon.png"
#define SPEAKER_OFF_ICON      "assets/speakerOffIcon.png"
#define MIC_ON_ICON           "assets/micOnIcon.png"
#define MIC_OFF_ICON          "assets/micOffIcon.png"
#define MIC_MUTE_ICON         "assets/micMuteIcon.png"
#define VIDEO_MUTE_ICON       "assets/videoMuteIcon.png"
#define BANNER_LINE           "assets/bannerLine.png"
#define STARTUP_BACKGROUND    "assets/startupBackground.png"
#define STARTUP_FIRST_TIME    "assets/startupFirstTime.png"
#define STARTUP_NEXT_TIME     "assets/startupNextTime.png"
#define FIRST_STARTUP_WAV     "assets/owlFirstPowerUp.wav"
#define WIFI_UNCONFIG_WAV     "assets/owlWiFiUnconfig.wav"
#define BLIP_WAV              "assets/volumeBlip.wav"
#define HOOT_WAV              "assets/owlHoot-48kHz.wav"
#define VER_CHANGE_HOOT_WAV   "assets/owlHoot-NewVer-48kHz.wav"
#endif
#define LOGO_X      2 * SUBFRAME_BORDER_MARGIN   /**< x-coord of logo 
						    top left corner. */
#define LOGO_Y      OUT_PANO_RES_Y + 3 * SUBFRAME_BORDER_MARGIN /**< y-coord of
								   logo top left
								   corner. */
#define LOGO_SCALE   1.0  /**< Size of corner logo relative to full PNG size. */
#define LOGO_ALPHA   0.7  /**< Transparancy for entire logo. */
#define LOGO_ANIMATION_DURATION 1.0
#define LOGO_INITIAL_ALPHA 0.1

#define PIN_SCALE    0.3  /**< Geometric scale to apply on both axes. */
#define PIN_ALPHA    1.0  /**< Transparency for the pin graphic. */
#define PIN_CLEARANCE 10  /**< Pixels between pin graphic and AOI edges. */
#define PIN_ANIMATION_DURATION 0.75 /**< Time for animating pin on show. */
#define PIN_AOI_ALPHA_PINNED   0.5
#define PIN_AOI_ALPHA_EDIT     1.0

// Params config file
#ifdef __ANDROID__

#define CONFIG_PATH "/data/OwlLabs/systemParams.cfg"
#else
#define CONFIG_PATH "systemParams.cfg"
#endif

// Git version file
#ifdef __ANDROID__
#define GIT_VERSION_PATH "/system/media/OwlLabs/gitHash.txt"
#else
#define GIT_VERSION_PATH "./gitHash.txt"
#endif

// Device serial number file
#ifdef __ANDROID__
#define DEVICE_SERIAL_NUMBER_PATH "/persist/dsn"
#else
#define DEVICE_SERIAL_NUMBER_PATH "./testing/dsn"
#endif

// GPU overlay params
#define OVERLAY_SHAPE_VECTOR_SIZE   500 /**< max number of overlay
					   objects, per shape type. */

//
// DANGER: I'm skeptical that the percent pixel val change needs to be set so
//         low in order to get the motion detection to work well.
//


// sound localization and beamforming
#define N_MICS             8 /**< number of mics. */
#define N_BEAMS           32 /**< number of beams. */
#define BEAM_MAX_TAU      15 /**< max delay in samples between two mics. */
#define BEAM_MAX_PCM_BUF_SIZE 8192 /**< largest supported PCM buffer size. */
#define NO_SOURCE_ANGLE -1 /**< angle value meaning no sound source. */

// leds
#define N_BUTTONS               5 /**< number of physical buttons (mutes counted
				     separately). */
// buttons
#define N_LEDS                  3 /**< number of LED types. */

// Threading params
//
// TODO: Move default thread parameters to cThread once all owlcore threading has
//        been switched over to using the class.
//
#define DEFAULT_THREAD_PRIORITY     10 /**< default thread priority for
					  threads with lower priority
					  than main thread. */
#ifdef __ANDROID__
#define FACEMAPPER_THREAD_USLEEP 10000 /**< microseconds for facemapper thread
					  to sleep, if at all. */
#else // linux
#define FACEMAPPER_THREAD_USLEEP 25000 /**< microseconds for facemapper thread
					  to sleep, if at all. */
#endif // __ANDROID__ / linux

#define MOTIONMAPPER_THREAD_USLEEP 30000 /**< microseconds for motionmapper
					    thread to sleep, if at all. */

// volume params
#define MIC_VOLUME_DEFAULT       100 /**< starting mic volume (percent). */
#define VOLUME_DEFAULT           90  /**< starting speaker volume (percent). */

// Analytics & System Health data paths
#ifdef __ANDROID__
#define OWLLABS_DATA_DIRECTORY    "/data/OwlLabs"
#define ANALYTICS_DATA_DIRECTORY  "/data/OwlLabs/BlackBox"
#define SYSTEM_HEALTH_DATA_DIRECTORY  "/data/OwlLabs/SystemHealth"
#else // linux
#define OWLLABS_DATA_DIRECTORY    "./testing"
#define ANALYTICS_DATA_DIRECTORY  "./testing/BlackBox"
#define SYSTEM_HEALTH_DATA_DIRECTORY  "./testing/SystemHealth"
#endif

// Time most recently known to the system (guarantees always increasing time */
#ifdef __ANDROID__
#define TIME_FILE_NAME      "/data/OwlLabs/time.txt";
#define TIME_FILE_NAME_BAK  "/data/OwlLabs/time.txt.bak";
#else
#define TIME_FILE_NAME      "./time.txt";
#define TIME_FILE_NAME_BAK  "./time.txt.bak";
#endif

// Per class parameter storage location
#ifdef __ANDROID__
#define MEETING_OWL_PARAMS_ROOT "/data/OwlLabs/Params/"
#else
#define MEETING_OWL_PARAMS_ROOT "./tmp/Params/"
#endif
#define OVERLAY_MANAGER_PARAMS     "OverlayManager/"
#define UI_HANDLER_PARAMS          "UiHandler/"
#define STARTUP_EXP_PARAMS         "StartupExp/"

// local settings, if any, that override parameters defined above or
// in any file included above (like fledglingParams.h). The following
// line should remain the last one before the closing #endif for the
// _PARAMS_H_ guard.
#include "localParams.h"

#endif /* _PARAMS_H_ */
