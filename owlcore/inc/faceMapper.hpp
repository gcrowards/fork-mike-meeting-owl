#ifndef __faceMapper_hpp
#define __faceMapper_hpp

#include <pthread.h>

#include "opencv2/core/core.hpp"

#include "map.hpp"
#include "panorama.hpp"
#include "faceDetector.hpp"

/** This class is responsible for mapping out the locations of objects in the
 * room that may be of interest.  
 *
 * There is no trigger mechanism, the system just looks through the room 
 * piece-by-piece and builds up a map for other detection systems to use.  
 * Currently the only object of interest are people, and it tries to find them 
 * with only face detection.
 */ 
class cFaceMapper
{
public:
  // constructors
  cFaceMapper(cPanorama *pPano);
  
  // destructor
  ~cFaceMapper();

  // action
  void update(void);
  void resetMap(void);

  // setters
  void setStreaming(bool streaming);

  // getters
  cv::Rect getSearchRoi(void);
  int getMapValAt(int angle);
  int getAngleWithMaxVal(int nominalAngle, int plusMinus);
  bool isPersonAt(cv::Point pos);
  cMap getMap(void);
  
private:
  // data
  cMap mMap; /**< The map keeps track of the positional data of the things 
		found by the cFaceMapper. */
  cFaceDetector mFaceDetector; /**< Applies face classifier. */
  
  cPanorama *mpPano; /**< Source of image data. */
  cv::Mat mGrayImage; /**< Deep copy of panorama gray-scaled image. */

  cv::Size mImageSize; /**< Dimensions of image data source. */  
  cv::Rect mSearchRoi; /**< Region wihtin image data that is currently being
			  considered. */
  cv::Size mSearchIncrement; /**< Amount search ROI is moved on each subsequent
				search. */

  int mShiftCount; /**< Keeps track of how many times the search ROI has 
		      been shifted. */
  bool mStreaming; /**< When video streaming == true, otherwise false. */

  // actions
  void resetSearchRoi(void);
  void setImageSize(void);
  void incrementSearchRoi(void);

  // thread variables
  pthread_t       mThread; /**< Thread for this object. */
  pthread_mutex_t mMutex;  /**< Mutex for shared resources. */
  bool    mKeepThreading;  /**< Flag to control iteration of thread loop. */

  // thread methods
  static void *threadWrapper(void *pContext); /**< Wrapper for pthread_create.*/
  void join(void);  /** Interface to exit the thread. */
  void start(void); /** Interface to start the thread. */
  void threadUpdate(void); /** Thread update loop. */
};

#endif
