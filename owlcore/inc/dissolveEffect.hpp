#ifndef __dissolveEffect_hpp
#define __dissolveEffect_hpp

#include "opencv2/core/core.hpp"

#include <ctime>

/* This class is used to dissolve one image into another */
class cDissolveEffect
{
public:
  // constructors
  cDissolveEffect(double duration);
  cDissolveEffect(cv::Mat *pTopFrame, cv::Mat *pBottomFrame, double duration);

  // destructor
  ~cDissolveEffect();

  // actions
  bool update(cv::Mat *pResultImage, 
	      cv::Mat *pImageA = NULL, 
	      cv::Mat *pImageB = NULL );

  // getters
  bool getDone(void);

private:
  //data 
  cv::Mat* mpImageA;
  cv::Mat* mpImageB;

  clock_t mPrevTime;
  double mDuration;
  double mSpeed;

  double mAlpha;
  double mBeta;

  bool mDone;

  // actions
  void init(void);
  void setSpeed(void);
};

#endif
