#ifndef __common_hpp
#define __common_hpp

#include <algorithm>
#include <array>
#include <cassert>
#include <cstddef>
#include <fstream>
#include <iostream>
#include <string>
#include <time.h>
#include <vector>

#include "params.h"

// Colors
#define WHITE      (cv::Scalar(255, 255, 255))  // B,G,R
#define BLACK      (cv::Scalar(0, 0, 0))        //
#define GRAY       (cv::Scalar(100,100,100))    //
#define YELLOW     (cv::Scalar(0, 255, 247))    //
#define RED        (cv::Scalar(0, 0, 255))      //
#define ORANGE     (cv::Scalar(24, 138, 255))   //
#define GREEN      (cv::Scalar(0, 255, 0))      //
#define BLUE       (cv::Scalar(255, 0, 0))      //
#define PURPLE     (cv::Scalar(255, 0, 230))    //
#define CYAN       (cv::Scalar(255, 230, 0))    //
#define CYAN2      (cv::Scalar(202, 189, 0))    //

#define ROW        0    // these match the indices used in OpenCV
#define COL        1    //

#define CLAMP(minVal, val, maxVal) (std::max(minVal, std::min(val, maxVal)))

#define RAD2DEG(x) ((x) * 180.0f / M_PI)
#define DEG2RAD(x) ((x) * M_PI / 180.0f)

#define PATH_MAX_STRING_SIZE 256  /**< Maximum allowed full path length. */

// Returns current date and time with format YYYYMMDD-HHMMSS.
std::string currentDateTime(void);

// Returns current data and time with format YYYY-MM-DD HH:MM:SS.
std::string currentDateTimeHumanReadable(void);
  
// Fills a timespec structure with the current time
void currentTimeSpec(struct timespec *pTs);

// Returns the difference between two timespecs if end is later than start.
struct timespec elapsedTime(struct timespec startTime, struct timespec endTime);

// Returns the difference between two timespecs if end is later than start.
double elapsedTimeSec(struct timespec startTime, struct timespec endTime);

// Returns the time since given start time
double elapsedTimeSinceSec(struct timespec startTime);

// Builds a vector of tokens in the given string
void tokenize(const std::string str, const std::string delim,
              std::vector<std::string> &tokens);

// Converts input degrees into the stage pixel equivalent.
int degToStagePix(int degrees);

// Converts input stage pixels into degrees.
float stagePixToDeg(int stagePix);

// prints data as an ASCII bar graph
void printBarGraph(const char *pLabel, int *pData, int nData, float scale);

// helper for making ASCII bar graphs
void sprintBar(char *pDst, size_t maxLen, char *pLabel, char barChar, int barLen);

// splits a string into individual lines
std::vector<std::string> splitStringLines(const std::string &str);

// make directory path, including any parents required
int mkdirWithParents(const char *pDir, const mode_t mode);

// append file name to path and return size of the file
int pathify(std::string &fullPath, char *pPath, const char *pFileName);

// convert enum name to camel-case
int convertEnumToCamelCase(char *pDest, const char *pSrc, int len);

// wrapper for libOwlPG function to return state of UVC stream
void getUvcStreamStateWrapper(int *streaming);

/** Reads a parameter file to extract a single parameter value.
 *
 * This function is templated so that it can read a single parameter of
 * most any type from a parameter file. If the parameter file does not
 * exist or if the contents of the file cannot be read into a variable of
 * the type T, then defaultVal is returned.
 *
 * @param dir is the class directory name under MEETING_OWL_PARAMS_ROOT
 * @param name is the name of the parameter file.
 * @param defaultVal is the default value if reading fails.
 *
 * @returns the value read from the file or the defaultVal.
 */
template <class T>
T loadParameter(std::string dir, std::string name, T defaultVal)
{
  T value = defaultVal;
  std::ifstream inFile;
  std::string fileName = std::string(MEETING_OWL_PARAMS_ROOT) + dir + name;

  inFile.open(fileName);
  if (inFile.is_open())
    {
      inFile >> value;
      if (inFile.rdstate() & (std::ios_base::failbit | std::ios_base::badbit))
        {
          value = defaultVal;
        }
      inFile.close();
    }

  return value;
}

// Template Class to make a vector look like a 2D array
template<typename T> class array2d 
{
  std::size_t data;
  std::size_t col_max;
  std::size_t row_max;
  std::vector<T> a;
  
 public:
  array2d()
  {}

 array2d(std::size_t col, std::size_t row) 
   : data(col*row), col_max(col), row_max(row), a(data) 
  {}
  
  T& operator()(std::size_t col, std::size_t row) 
    {
      assert(col_max > col && row_max > row);
      return a[(col_max * row) + col];
    }
};

/** Function timer debugging tool:
 * Structure to hold state for function timer routines below */
typedef struct funcTimer
{
  struct timespec mFirstTime = {0,0}; /**< Very first time struct is used */
  struct timespec mBeforeTime;        /**< Time before running function */
  struct timespec mAfterTime;         /**< Time after running function */
  double mReportingPeriod;            /**< Time between reporting results */
  struct timespec mLastReportTime = {0,0}; /**< Time of most recent report */
  double mTotalElapsedTime;           /**< Total time since mFirstTime */
  double mFuncElapsedTime;            /**< Total time between before and after
                                        * time pairs */
} funcTimer_t;

// Start a timer used to measure function time usage before a function call.
void startFuncTimer(funcTimer_t *pTimer, double reportingPeriod);

// Stop a timer after the function call.
void stopFuncTimer(funcTimer_t *pTimer);

// Check if the time has come to report results.
bool reportFuncTimer(funcTimer_t *pTimer);

// Compute the percent of total elapsed time spent in the function.
double percentCpuFuncTimer(funcTimer_t *pTimer);

#endif
