#ifndef AUDIOEQUALIZER_H_INCLUDED
#define AUDIOEQUALIZER_H_INCLUDED
/* whatever sample type you want */
#define EQ_SECTION_COUNT 8

typedef double smp_type;

/* this holds the data required to update samples thru a filter */
typedef struct {
	smp_type a0, a1, a2, a3, a4;
	smp_type x1, x2, y1, y2;
}
biquad;
typedef enum {
	LPF, /* low pass filter */
	HPF, /* High pass filter */
	BPF, /* band pass filter */
	NOTCH, /* Notch Filter */
	PEQ, /* Peaking band EQ filter */
	LSH, /* Low shelf filter */
	HSH, /* High shelf filter */
	LPF1P, // One pole low pass
	HPF1P // one pole high pas
}section_type;

typedef struct
{
	int flags;
	section_type type;
	smp_type gain;
	smp_type freq;
	smp_type bandwidth;
	biquad sectionBiquad;
}EQ_SECTION;

typedef struct
{
	smp_type trim;
	int		 fs;
	EQ_SECTION sections[EQ_SECTION_COUNT];
}EQUALIZER;

extern smp_type BiQuad(smp_type sample, biquad * b);
extern biquad *BiQuad_new(int type, smp_type dbGain, /* gain of filter */
	smp_type freq,             /* center frequency */
	smp_type srate,            /* sampling rate */
	smp_type bandwidth);       /* bandwidth in octaves */

							   /* filter types */

extern void EqInit(EQUALIZER *pEq, int fs);
extern int  EqActivate(EQUALIZER *pEq, int chanNum, int state);
extern int  EqConfigure(EQUALIZER *pEq, int chanNum, section_type type, double freq,
	double gain, double bandwidth);
extern int EqPostGain(EQUALIZER *pEq,double gain);
extern int  EqProcess(EQUALIZER *pEq, short *pBuff, int len, int eqChan, int totalChans);
extern double EqRMSProcess(EQUALIZER *pEq, short *pBuff, int len, int eqChan, int totalChans);





#endif