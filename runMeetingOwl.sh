#!/bin/bash

# set source as Jabra microphone
SOURCE=`pactl list sources short |grep Jabra |grep input |awk '{print substr($0,1,2)}'`
`pacmd set-default-source $SOURCE &`
`pacmd set-source-volume $SOURCE 0x10000 &`

# try to set sink as Jabra analog speaker
SINKANALOG=`pactl list sinks short |grep Jabra |grep analog |awk '{print substr($0,1,2)}'`
`pacmd set-default-sink $SINKANALOG &`
`pacmd set-sink-mute $SINKANALOG 0 &`
`pacmd set-sink-volume $SINKANALOG 0x10000 &`

`sleep 1s &` #gives audio a little time to set up

# try to set sink as Jabra digital speaker (preferred so this happens second)
SINKDIGITAL=`pactl list sinks short |grep Jabra |grep iec |awk '{print substr($0,1,2)}'`
`pacmd set-default-sink $SINKDIGITAL &`
`pacmd set-sink-mute $SINKDIGITAL 0 &`
`pacmd set-sink-volume $SINKDIGITAL 0x10000 &`

`sleep 1s &` #gives audio a little time to set up

cd ~/meetingOwlP0 #make sure you're in the meetingOwlP0 directory

#kills all meetingOwlP0 related processes except for this script
`kill -9 $(ps aux | \
           grep meetingOwlP0 | \
           grep -v 'runMeetingOwl.sh' | \
           awk '{ print $2 }') &`

pkill firefox & #kills all instances of firefox

`sleep 1s &` #gives firefox a little time to close before it is reopened

firefox --new-tab --url http://calendar.google.com & #this assumes home page is already set to main google hangouts page

./meetingOwlP0 & #executes meeting owl program

#runs python localize with no visualizations and a threshold of 15
echo `~/owl-mic-array/python/localize.py -nv -th 15`
