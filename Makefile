##########

# project name
PRJ_NAME = meetingOwl

##########

# NOTE: run 'make clean' after changing any of the compile defs below

# Linux compile defs with -D<name> option
LINUX_COMPILE_DEFS := # uncomment needed DEFS below
#LINUX_COMPILE_DEFS += -DLOG_TO_FILE
#LINUX_COMPILE_DEFS += -DINPUT_3456
#LINUX_COMPILE_DEFS += -DMOCK_STAGE_PANEL
#LINUX_COMPILE_DEFS += -DSIMULATE_PCM_DATA

# Android compile defs with -D<name> option
JNI_COMPILE_DEFS := # uncomment needed DEFS below
#JNI_COMPILE_DEFS += -DLOG_TO_FILE
#JNI_COMPILE_DEFS += -DINPUT_3456 # experimental (can freeze UVC output)
#JNI_COMPILE_DEFS += -DMOCK_INPUT
#JNI_COMPILE_DEFS += -DMOCK_STAGE_PANEL
#JNI_COMPILE_DEFS += -DOPENCV_FACES
#JNI_COMPILE_DEFS += -DPROTOTYPE_P0_5
#JNI_COMPILE_DEFS += -DNO_BEAMFORMING
#JNI_COMPILE_DEFS += -DSIMULATE_PCM_DATA

# Suppress malloc related warnings. Without this, included files from Android
# NDK generate a ton of warnings!
JNI_COMPILE_DEFS += -Wno-unknown-attributes 
# ***WARNING*** the above should be removed when issue is resolved!

##########

# device directory to push Android application to
DEVICED = /system/bin

# target architecture; possibilities: armeabi, armeabi-v7a, arm64-v8a
APP_ABI := armeabi-v7a

# target platform
APP_PLATFORM := android-21

##########

# owl core source files
SRC_FILES =					\
	analytics.cpp				\
	areaOfInterest.cpp			\
	aspectMath.cpp				\
	attentionSystem.cpp			\
	AudioEqualizer.cpp			\
	AudioLevelManager.cpp 			\
	AudioMgr.cpp				\
	AudioPlayer.cpp 			\
	audioDTD.cpp				\
	beamform.cpp				\
	beamMath.cpp				\
	blackBox.cpp                    	\
	buildId.cpp				\
	buttonHandler.cpp			\
	buttonContext.cpp			\
	circleMath.cpp				\
	common.cpp				\
	debugInfo.cpp                           \
	debugManager.cpp			\
	deviceInfo.cpp				\
	elements/angle.cpp			\
	elements/bufferGraph.cpp		\
	elements/ellipse.cpp    		\
	elements/iconOverlay.cpp		\
	elements/line.cpp			\
	elements/linearAnimate.cpp		\
	elements/panel.cpp			\
	elements/patch.cpp			\
	elements/pinnedAoiOverlay.cpp		\
	elements/rect.cpp			\
	elements/rgbaOverlayBase.cpp		\
	elements/rgbaOverlayLists.cpp		\
	elements/shape.cpp			\
	faceDetector.cpp			\
	faceMapper.cpp				\
	faceTracker.cpp				\
	gpu.cpp					\
	imageFileUtils.cpp			\
	item.cpp				\
	layout.cpp				\
	ledContext.cpp				\
	ledInterface.cpp			\
	logging.cpp				\
	main.cpp				\
	map.cpp					\
	mapsPlayer.cpp				\
	mapsRecorder.cpp			\
	motionDetector.cpp			\
	motionMapper.cpp			\
	netStatus.cpp               		\
	overlayHelper.cpp			\
	owl.cpp					\
	owlHook.cpp				\
	panorama.cpp				\
	personMapper.cpp			\
	ringBuffer.cpp				\
	shellProcess.cpp			\
	soundLocalizer.cpp			\
	soundMapper.cpp				\
	startupExp.cpp          		\
	statusFrame.cpp                 	\
	subframe.cpp				\
	sysfs.cpp				\
	systemHealth.cpp			\
	systemParams.cpp			\
	systemtest/buttonTestManager.cpp	\
	systemtest/micTestManager.cpp   	\
	systemtest/systemTest.cpp		\
	systemtest/testImgCompositor.cpp	\
	systemtest/wifiTestManager.cpp		\
	switchboard/audioParams.cpp         	\
	switchboard/jsonListReader.cpp  	\
	switchboard/jsonListWriter.cpp  	\
	switchboard/pinnedAoiArgs.cpp   	\
	switchboard/pinnedAoiReply.cpp  	\
	switchboard/setupProg.cpp           	\
	switchboard/switchboardClient.cpp	\
	switchboard/switchboardServer.cpp	\
	switchboard/meetingInfo.cpp     	\
	switchboard/uiAction.cpp        	\
	switchboard/videoParams.cpp         \
	thread.cpp				\
	uiHandler.cpp				\
	utils/colorGenerator.cpp        	\
	utils/csvParser.cpp                     \
	version.cpp				\
	virtualMic.cpp				\
	visualEditor.cpp			\
	voice.cpp				\
	voiceTracker.cpp			\
	volumeControl.cpp

##################################################
# normally, no edits should be needed below here #
##################################################

# location of NDK platforms directory
ifeq ($(APP_ABI), arm64-v8a)
	NDK_USERD = ~/Android/Sdk/ndk-bundle/platforms/$(APP_PLATFORM)/arch-arm64/usr
else
	NDK_USERD = ~/Android/Sdk/ndk-bundle/platforms/$(APP_PLATFORM)/arch-arm/usr
endif

##########

# build directories
SRCD = owlcore/src
INCD = owlcore/inc
OBJD = owlcore/obj

##########

# desktop compilation options
CC = g++
CFLAGS = -std=gnu++11 -O3 -pthread $(LINUX_COMPILE_DEFS)
LFLAGS = -ljpeg -lpng -lX11 -lEGL -lGLESv2 -lopencv_videoio -lopencv_objdetect -lopencv_imgcodecs -lopencv_imgproc -lopencv_highgui -lopencv_core -lm
IFLAGS = -I$(INCD)

##########

# source files adjusted for directory name
SRC_FILES := $(patsubst %,$(SRCD)/%,$(SRC_FILES))

# corresponding object files
OBJ_FILES := $(patsubst $(SRCD)/%.cpp,$(OBJD)/%.o,$(SRC_FILES))

##########

# extra dependencies for object file targets
EXTRA_DEPS = $(INCD)/debugSwitches.h $(INCD)/params.h $(INCD)/elements/shape.hpp
EXTRA_DEPS += $(INCD)/localParams.h

##########

# ----- options to be picked up by Android.mk -----

export JNI_APP := $(PRJ_NAME)
export JNI_APP_ABI := $(APP_ABI)
export JNI_APP_MAIN := ../../owlcore/src/main.cpp
export JNI_APP_PLATFORM := $(APP_PLATFORM)
export JNI_INCD := $(NDK_USERD)/include/
export JNI_LIBD := $(NDK_USERD)/lib/
export JNI_COMPILE_DEFS
export JNI_CFLAGS := -I../../OpenCV-android-sdk/sdk/native/jni/include/

# adjusted source file paths to account for Android.mk working directory
JNI_SRC_FILES := $(patsubst %,../../%,$(SRC_FILES))
export JNI_SRC_FILES

##########

# ----- get git hash for version reference ----
$(shell git rev-parse HEAD > gitHash.txt)
HASH_HEAD := $(shell git rev-parse HEAD)

##########

.PHONY: android linux

all: android linux

echo:
	@echo $(value JNI_APP) $(value JNI_APP_ABI)
	@echo "OBJ FILES:" $(value OBJ_FILES)
	@echo "JNI SRC FILES:" $(value JNI_SRC_FILES)
	@echo "linux compile defs:" $(value LINUX_COMPILE_DEFS)
	@echo "JNI compile defs:" $(value JNI_COMPILE_DEFS)
	@echo "Hash of HEAD saved to hash.txt: " $(value HASH_HEAD)

setup:
	adb wait-for-device root && sleep 2 && adb wait-for-device remount
	./scripts/copyTargetAssets.sh "adb push" . ""

android:
	@echo "Hash of HEAD saved to hash.txt:" $(value HASH_HEAD)
	cp -n owlcore/inc/localParams.h.template owlcore/inc/localParams.h	
	cd android && ndk-build

linux: localParams linux-build
	@echo "Hash of HEAD saved to hash.txt:" $(value HASH_HEAD)

# NOTE: There is a known bug when the -j (jobs) option is used with
# the linux target and when localParams.h does not yet exist. For
# example, "make linux -j2" will fail the first time because one job
# will create the missing file but not before the other job detects
# that the file is missing. Running "make linux -j2" again will then
# work as desired.

localParams:
	cp -n owlcore/inc/localParams.h.template owlcore/inc/localParams.h

linux-build: $(OBJ_FILES)
	$(CC) $(CFLAGS) $^ $(LFLAGS) $(IFLAGS) -o $(PRJ_NAME)

$(OBJD)/%.o: $(SRCD)/%.cpp $(INCD)/%.hpp $(EXTRA_DEPS)
	$(CC) -c $(CFLAGS) $(IFLAGS) $< -o $@

install:
	adb push android/libs/$(JNI_APP_ABI)/$(JNI_APP) $(DEVICED)/$(JNI_APP)
	adb push ./gitHash.txt /system/media/OwlLabs/

install-debug:
	adb push android/obj/local/$(JNI_APP_ABI)/$(JNI_APP) $(DEVICED)/$(JNI_APP)

prep:
	adb wait-for-device root && sleep 2 && adb wait-for-device remount

run:
	@echo ""
	@echo "     To trap Ctrl-C, instead use 'adb shell' to run" $(value DEVICED)"/"$(value JNI_APP)
	@echo ""
	adb shell "cd $(DEVICED) && ./$(JNI_APP)"

clean:
	cd android && ndk-build clean
	rm -rf android/libs/* android/obj/* android/build/images/*.img
	find . -name "*~" -type f -delete
	rm -f $(PRJ_NAME)
	find $(OBJD) -name "*.o" -type f -delete

injected:
	@echo ""
	@echo "#### Please note: you must have already built the correct version of Android ####"
	@echo "#### on this system! If you have not, please ctrl+C now before things get messy! ####"
	@echo ""
	@echo "Building Speex..."
	@echo ""
	cd speexdsp_owl && ./build-lib.sh
	@echo ""
	@echo "Building libOwlPG..."
	@echo ""
	cd libOwlPG && ./build-lib.sh
	cp -n owlcore/inc/localParams.h.template owlcore/inc/localParams.h
	@echo ""
	@echo "Building meetingOwl..."
	@echo ""
	@echo "Hash of HEAD saved to hash.txt: " $(value HASH_HEAD)
	@echo ""
	cd scripts && ./setDebugSwitches.sh
	cd android && ndk-build
	@echo ""
	@echo "Building injected Android images..."
	@echo ""
	cd scripts && ./build-injected-image.sh
	@echo ""
	@echo "Injected image build complete: android/build/images/"
