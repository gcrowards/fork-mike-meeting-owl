version: 2.2.0.0
date: 2018-02-09
notes: Production release

version: 2.2.0.1
date: 2018-02-06
notes: BETA - Fine A Free Owl
       Video stream is always processed and number of people in the room is 
       sent to the Barn in real-time where it can be viewed via the web 
       console.
warnings: There is a bug in the startup experience that can cause an
	  application crash.  This is also on develop but is very unlikely to
	  happen.  This release is more sensitive to the bug because the video
	  is always being processed.  The work around is that this release
	  has a delay whenever a meeting starts that prevents the startup
	  experience from running in the way that is likely to cause the crash.

version: 2.2.0.2
date: 2018-03-05
notes: BETA - Voice Camera Lock
       Say, "Meeting Owl"..."Look At Me" to enable Camera Lock.
       Say, "Meeting Owl"..."Stop" to disable Camera Lock.
warnings: If the Attention System doesn't already know about a person being on
	  the map then you have to keep talking after "Look At Me" until you're
	  established on the map.  Also, command model is not well trained, so
	  expect false some positives.

version: 2.2.0.3
date: 2018-03-08
notes: 2.2.0.0 equiavalent (effectively reverts betas to 2.2.0.0 code).

version: 2.2.1.0
date: 2018-03-16
notes: Production release
       - Enabled gain control by double-talk detector (now Owl is much more 
         full duplex!)
       - Fixed software version number display (to accomodate 4-field format)
       - Fixed some bugs to improve the Camera Lock control experience
       - Cleaned up flashing scripts to reduce confusion in the factory
       - Added access to radio bin file version number, for the factory

version: 2.2.1.1
date: 2018-04-04
notes: BETA - Whiteboard Enhance
       Frame rate is quite slow and latency is high.
warnings: Contains major changes to the video pipeline and is therefore risky
           to release to too broad an audience without further testing.

version: 2.2.1.3
date: 2018-04-04
notes: BETA - Voice Camera Lock
       Say, "Alexa"..."Look At Me" to enable Camera Lock.
       Say, "Alexa"..."Stop" to disable Camera Lock.
warnings: There are no beeps or other indications that the Owl heard anything.
          There will still be some false positives.

version: 2.2.1.5
date: 2018-04-04
notes: BETA - Find A Free Owl
       This is the same as version 2.2.0.1, it's just a new build to increase
       the version number/date of the build.

version: 2.2.1.6
date: 2018-04-04
notes: 2.2.1.0 equiavalent (effectively reverts betas to 2.2.1.0 code).

version: 2.2.1.7
date: 2018-04-10
notes: BETA - Find A Free Owl
       This is the same as version 2.2.0.1, it's just a new build to increase
       the version number/date of the build.

version: 2.2.1.9
date: 2018-04-10
notes: BETA - Voice Camera Lock
       Rebased off of develop, so it is up-to-date, especially with regards to
       including the double-talk detetor (which is turns out has issues with
       S4B, which means this release also does not work well with S4B (bad
       audio quality & poor attention system performance).

version: 2.2.1.11
date: 2018-04-10
notes: BETA - Whiteboard Enhance
       This is the same as version 2.2.1.1, it's just a new build to increase
       the version number/date of the build.

version: 2.2.1.12
date: 2018-04-10
notes: 2.2.1.0 equiavalent (effectively reverts betas to 2.2.1.0 code).

version: 2.2.1.14
date: 2018-04-17
notes: 2.2.0.0 equiavalent (effectively reverts betas to 2.2.0.0 code).

version: 2.2.1.15
date: 2018-04-17
notes: BETA - S4B Audio Fix 
       Fixes audio issues (and attention system switching speed) with S4B
       that were introduced in 2.2.1.0.  Testing on a few customers before
       doing a general release.
follow-up: Helped audio issues, but didn't completely solve them. Didn't
           solve sound-source localization issue.

version: 2.2.1.17
date: 2018-04-24
notes: BETA - S4B Audio Fix II 
       Fixes audio issues (and attention system switching speed) with S4B
       that were introduced in 2.2.1.0.  Testing on a few customers before
       doing a general release.
follow-up: Helped audio issues, but there are still sometimes drop-outs and
           sometimes a bit of echo.  Sound-source localization seems to be
           working well.

version: 2.3.0.0
date: 2018-04-25
notes: S4B Audio Work-around (disabled DTD gain control)
       Disables gain control by the DTD to effectively set perfomance back to
       version 2.2.0.0.  This are some audio changes in here (things that were
       discovered that were wrong are now fixed), so this is really 2.2.0.0
       plus fixing some things that were definitely wrong.

version: 2.3.0.1
date: 2018-05-15
notes: BETA - Find A Free Owl
       This is the same as version 2.2.0.1, it's just a new build to increase
       the version number/date of the build.

version: 2.3.0.2
date: 2018-05-15
notes: 2.3.0.0 equiavalent (effectively reverts betas to 2.3.0.0 code).

version: 2.4.1.0
date: 2018-06-13
notes: Production release
       - Owl "administrators" can now use a pin number to secure the settings 
         section of the app to prevent normal users of the Owl from adjusting 
         settings
       - Now you can update your Owl's software via the app (by going to the 
         help section and  tapping 6 times on the app version number)
       - Owl does a special hoot after it reboots from successfully installing 
         new software

version: 2.5.3.0
date: 2018-08-03
notes: Production release
       - Fixed bug that caused noise suppression to remain somewhat active, 
         even when the user had disabled it via the app.
       - Fixed bug that could cause crackling on mics for some Owl-computer 
         combinations (this bug had been around for a long time, but was not 
         particularly noticeable because the crackling was low in volume and 
         would not be present for all Owls/computer setups).
       - Improved mic mixer to use all eight mics (provides better sound pickup
         quality in small, reverberant rooms).
       - Optimized and automated the setting of the minimum loudspeaker active
         level that locks out the sound-source localizer (as a result, the 
         attention system is more responsive for S4B users).
warnings: As of this release, the manual slider in the app that was used to 
          adjust the minimum loudspeaker sound-source localizer lockout no 
          longer does anything.  The slider's setting won't effect the 
          performance of the Owl in any way.  This slider will be removed from
          the app once 2.5.3.0 is in full release.

version: 2.6.1.0
date: 2018-08-14
notes: Production release
       - Smart Stage Share: When a new meeting starts the Attention System will
         fill the Stage with as many people as it can find and fit on the 
	 display.  It has 30 seconds after the first voice-triggered person is
	 found to fill the stage with visually-only-triggered people.

version: 2.7.1.0
date: 2018-12-11
notes: Production release
       - Full Duplex: DTD detector now has the ability to control the 
         microphone gain to create a more full-duplex experience.  By default
	 this is on.  The user may switch this feature off via the mobile
	 app, if it's not working well in their particular set-up.
       - Fix potential crash bug in StartCount file access.
       - Internal Use Only: Now it's possible to switch the camera into ful-
         frame mode (3456 x 3456).  At build, the software can also be enabled
	 to allow a button sequence to trigger the Owl to take a full-res
	 fisheye snapshot and save it to disk.

version: 2.8.0.0
date: 2018-12-14
notes: Production release
       - Enables mobile app to show update progress when manually triggered.

version: 2.9.0.0
date: 2019-03-13
notes: Production release
       - Adds HID control; now Owl's volume controls are synchronized with
         the host computer (e.g. pressing the volume up button on the Owl
	 will result in the host computer showing the volume going up a click
	 on the screen (in OSX).
       - Note: this doesn't work quite right on Ubuntu so there's a bit of a
         hack in there to make it work as well as possible.  We believe the
	 issue lies in Ubuntu and not in the Owl.
